init-if-undefined $STi7200ResetDelay = 20
keep-variable $STi7200ResetDelay

init-if-undefined $_sti7200jtag_debugreset = 1
keep-variable $_sti7200jtag_debugreset

init-if-undefined $_sti7200jtag_printids = 0
keep-variable $_sti7200jtag_printids

##{{{  sti7200_bypass_setup
define sti7200_bypass_setup
  set $_sti7200_bypass_setup_argc = $argc

  enable_jtag

  set $_stmmxmode = 0

  ## Print TMC device identifiers
  if ($_sti7200jtag_printids)
    ## Manual control of JTAG (manual TCK)
    jtag mode=manual

    ## Reset TapMux to bypass to TMC
    jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

    source jtagtmcid.cmd
    tmcPrintIds STi7200
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset STi7200 leaving ST40 in reset hold
  if ($_sti7200jtag_debugreset)
    jtag ntrst=0
    _jtagSleep $STi7200ResetDelay
    jtag nrst=0
    _jtagSleep $STi7200ResetDelay
    jtag asebrk=0
    _jtagSleep $STi7200ResetDelay
    jtag nrst=1
    _jtagSleep $STi7200ResetDelay
    jtag asebrk=1
    _jtagSleep $STi7200ResetDelay
    jtag ntrst=1
  else
    jtag nrst=0
    _jtagSleep $STi7200ResetDelay
    jtag asebrk=0
    _jtagSleep $STi7200ResetDelay
    jtag nrst=1
    _jtagSleep $STi7200ResetDelay
    jtag asebrk=1
  end

  ## Reset TapMux to bypass to TMC
  jtag tck=00 ntrst=01 tms=00 tdo=00

  if ($_sti7200_bypass_setup_argc > 0)
    $arg0
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux and then bypass to ST40
  jtag tck=01010 ntrst=00011 tms=00000 tdo=01111

  ## Normal control of JTAG
  jtag mode=normal
end

document sti7200_bypass_setup
Configure the STi7200 for a direct connection to the ST40 CPU
Usage: sti7200_bypass_setup [<command>]
where <command> is an optional command to be invoked before connecting to the ST40 CPU
end
##}}}

##{{{  sti7200_bypass_setup_attach
define sti7200_bypass_setup_attach
  set $_sti7200_bypass_setup_attach_argc = $argc

  enable_jtag

  set $_stmmxmode = 0

  ## Print TMC device identifiers
  if ($_sti7200jtag_printids)
    ## Manual control of JTAG (manual TCK)
    jtag mode=manual

    ## Reset TapMux to bypass to TMC
    jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

    source jtagtmcid.cmd
    tmcPrintIds STi7200
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux to bypass to TMC
  jtag tck=00 ntrst=01 tms=00 tdo=00

  if ($_sti7200_bypass_setup_attach_argc > 0)
    $arg0
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux and then bypass to ST40
  jtag tck=01010 ntrst=00011 tms=00000 tdo=01111

  ## Normal control of JTAG
  jtag mode=normal
end

document sti7200_bypass_setup_attach
Configure the STi7200 for a direct connection to the ST40 CPU (STi7200 not reset)
Usage: sti7200_bypass_setup_attach [<command>]
where <command> is an optional command to be invoked before connecting to the ST40 CPU
end
##}}}

##{{{  sti7200_stmmx_setup
define sti7200_stmmx_setup
  set $_sti7200_stmmx_setup_argc = $argc

  enable_jtag

  set $_stmmxmode = 1

  ## Maximum TapMux link speed is 5MHz
  linkspeed 2.5MHz

  ## Manual control of JTAG (singleshot TCK)
  jtag mode=singleshot

  stmmxInitialState

  ## Disable STMMX TAP multiplexor
  stmmxWriteIR_TAPMUX
  stmmxWriteDR 1 0x00

  ## Set ST40 port as STMMX master channel
  stmmxWriteIR_BYPASS
  stmmxWriteDR 4 0x02
  stmmxWriteIR_RESET
  stmmxWriteDR 4 0x02

  ## Set STMMX clock to 47MHz
  stmmxWriteICS9161 0x0077f4a5

  ## Allocate channel slots
  stmmxWriteIR_ALLOC_WR
  ## Channels 1, 2, 3 ([1, 2], [1, 3])*
  stmmxWriteDR 32 0xd9d9d9d9

  ## Print TMC device identifiers
  if ($_sti7200jtag_printids)
    ## Manual control of JTAG (manual TCK)
    jtag mode=manual

    ## Reset TapMux to bypass to TMC
    jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

    source jtagtmcid.cmd
    tmcPrintIds STi7200
  end

  ## Reset STi7200 leaving ST40 in reset hold
  if ($_sti7200jtag_debugreset)
    jtag stmmx=1 ntrst=0
    _jtagSleep $STi7200ResetDelay
    jtag stmmx=1 nrst=0
    _jtagSleep $STi7200ResetDelay
    stmmxWriteIR_ASEBRK
    stmmxWriteDR 3 0x04
    _jtagSleep $STi7200ResetDelay
    jtag stmmx=1 nrst=1
    _jtagSleep $STi7200ResetDelay
    stmmxWriteIR_ASEBRK
    stmmxWriteDR 3 0x05
    _jtagSleep $STi7200ResetDelay
    jtag stmmx=1 ntrst=1
  else
    jtag stmmx=1 nrst=0
    _jtagSleep $STi7200ResetDelay
    stmmxWriteIR_ASEBRK
    stmmxWriteDR 3 0x04
    _jtagSleep $STi7200ResetDelay
    jtag stmmx=1 nrst=1
    _jtagSleep $STi7200ResetDelay
    stmmxWriteIR_ASEBRK
    stmmxWriteDR 3 0x05
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux to bypass to TMC
  jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

  if ($_sti7200_stmmx_setup_argc > 0)
    $arg0
  end

  ## Manual control of JTAG (singleshot TCK)
  jtag mode=singleshot

  ## Enable STMMX TAP multiplexor
  stmmxWriteIR_TAPMUX
  stmmxWriteDR 1 0x01

  ## Bypass STMMX to STi7200
  jtag stmmx=1

  ## Normal control of JTAG
  jtag mode=normal
end

document sti7200_stmmx_setup
Configure the STi7200 for a ST MultiCore/Mux connection to the ST40 CPU
Usage: sti7200_stmmx_setup [<command>]
where <command> is an optional command to be invoked before connecting to the ST40 CPU
end
##}}}

##{{{  sti7200_stmmx_setup_attach
define sti7200_stmmx_setup_attach
  set $_sti7200_stmmx_setup_attach_argc = $argc

  enable_jtag

  set $_stmmxmode = 1

  ## Maximum TapMux link speed is 5MHz
  linkspeed 2.5MHz

  ## Manual control of JTAG (singleshot TCK)
  jtag mode=singleshot

  stmmxInitialState

  ## Disable STMMX TAP multiplexor
  stmmxWriteIR_TAPMUX
  stmmxWriteDR 1 0x00

  ## Set ST40 port as STMMX master channel
  stmmxWriteIR_BYPASS
  stmmxWriteDR 4 0x02
  stmmxWriteIR_RESET
  stmmxWriteDR 4 0x02

  ## Set STMMX clock to 47MHz
  stmmxWriteICS9161 0x0077f4a5

  ## Allocate channel slots
  stmmxWriteIR_ALLOC_WR
  ## Channels 1, 2, 3 ([1, 2], [1, 3])*
  stmmxWriteDR 32 0xd9d9d9d9

  ## Print TMC device identifiers
  if ($_sti7200jtag_printids)
    ## Manual control of JTAG (manual TCK)
    jtag mode=manual

    ## Reset TapMux to bypass to TMC
    jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

    source jtagtmcid.cmd
    tmcPrintIds STi7200
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux to bypass to TMC
  jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

  if ($_sti7200_stmmx_setup_attach_argc > 0)
    $arg0
  end

  ## Manual control of JTAG (singleshot TCK)
  jtag mode=singleshot

  ## Enable STMMX TAP multiplexor
  stmmxWriteIR_TAPMUX
  stmmxWriteDR 1 0x01

  ## Bypass STMMX to STi7200
  jtag stmmx=1

  ## Normal control of JTAG
  jtag mode=normal
end

document sti7200_stmmx_setup_attach
Configure the STi7200 for a ST MultiCore/Mux connection to the ST40 CPU (STi7200 not reset)
Usage: sti7200_stmmx_setup_attach [<command>]
where <command> is an optional command to be invoked before connecting to the ST40 CPU
end
##}}}
