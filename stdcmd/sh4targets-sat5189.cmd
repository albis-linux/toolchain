################################################################################

define sat5189sim
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat5189_fsim_setup sat5189sim_setup $arg0
  else
    connectsh4simle sat5189_fsim_setup sat5189sim_setup ""
  end
end

document sat5189sim
Connect to and configure a simulated STi5189-SAT board
Usage: sat5189sim
end

define sat5189simse
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat5189se_fsim_setup sat5189simse_setup $arg0
  else
    connectsh4simle sat5189se_fsim_setup sat5189simse_setup ""
  end
end

document sat5189simse
Connect to and configure a simulated STi5189-SAT board (STi5189 in 32-bit SE mode)
Usage: sat5189simse
end

define sat5189simseuc
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat5189se_fsim_setup sat5189simseuc_setup $arg0
  else
    connectsh4simle sat5189se_fsim_setup sat5189simseuc_setup ""
  end
end

document sat5189simseuc
Connect to and configure a simulated STi5189-SAT board (STi5189 in 32-bit SE mode with uncached mappings)
Usage: sat5189simseuc
end

define sat5189simse29
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat5189se_fsim_setup sat5189simse29_setup $arg0
  else
    connectsh4simle sat5189se_fsim_setup sat5189simse29_setup ""
  end
end

document sat5189simse29
Connect to and configure a simulated STi5189-SAT board (STi5189 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sat5189simse29
end

################################################################################

define sat5189psim
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat5189_psim_setup sat5189sim_setup $arg0
  else
    connectsh4psimle sat5189_psim_setup sat5189sim_setup ""
  end
end

document sat5189psim
Connect to and configure a simulated STi5189-SAT board
Usage: sat5189psim
end

define sat5189psimse
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat5189se_psim_setup sat5189simse_setup $arg0
  else
    connectsh4psimle sat5189se_psim_setup sat5189simse_setup ""
  end
end

document sat5189psimse
Connect to and configure a simulated STi5189-SAT board (STi5189 in 32-bit SE mode)
Usage: sat5189psimse
end

define sat5189psimseuc
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat5189se_psim_setup sat5189simseuc_setup $arg0
  else
    connectsh4psimle sat5189se_psim_setup sat5189simseuc_setup ""
  end
end

document sat5189psimseuc
Connect to and configure a simulated STi5189-SAT board (STi5189 in 32-bit SE mode with uncached mappings)
Usage: sat5189psimseuc
end

define sat5189psimse29
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source sat5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat5189se_psim_setup sat5189simse29_setup $arg0
  else
    connectsh4psimle sat5189se_psim_setup sat5189simse29_setup ""
  end
end

document sat5189psimse29
Connect to and configure a simulated STi5189-SAT board (STi5189 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sat5189psimse29
end

################################################################################
