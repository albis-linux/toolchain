##------------------------------------------------------------------------------
## db479.cmd - Asynchronous FLASH STEM Module DB479 (32Mb)
##------------------------------------------------------------------------------

define db479_emi_configure
  set *$EMI_BANK$arg0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK$arg0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK$arg0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK$arg0_EMICONFIGDATA3 = 0x00000000
end
