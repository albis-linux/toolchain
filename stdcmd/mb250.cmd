##------------------------------------------------------------------------------
## mb250.cmd - SH7750 High Definition 3D Graphics and Video Evaluation Board MB250
##------------------------------------------------------------------------------

##{{{  MB250 Configuration
define mb250_configure
##------------------------------------------------------------------------------
## Phase 1: Configure FRQCR register
##------------------------------------------------------------------------------
  set *$CPG_WTCNT = 0x5a00
  set *$CPG_WTCSR = 0xa567
  set *$CPG_FRQCR = 0x0e1a

##------------------------------------------------------------------------------
## Phase 2: Configure rest...
##------------------------------------------------------------------------------
  set *$INTC_ICR = 0x0080

  set *$BSC_BCR1 = 0x000a0008
  set *$BSC_BCR2 = 0xff3c
  set *$BSC_WCR1 = 0x37773774
  set *$BSC_WCR2 = 0xfffe6fef
  set *$BSC_WCR3 = 0x07777775
  set *$BSC_MCR = 0x080a6214
  set *($BSC_SDMR3+(0x0190/sizeof(*$BSC_SDMR3))) = 0x0
  set *$BSC_RTCNT = 0xa500
  set *$BSC_RFCR = 0xa400
  set *$BSC_RTCSR = 0xa508
  set *$BSC_MCR = 0x480a6214
  set *($BSC_SDMR3+(0x0190/sizeof(*$BSC_SDMR3))) = 0x0
  set *$BSC_RTCNT = 0xa500
  set *$BSC_RTCOR = 0xa5fa
  set *$BSC_RFCR = 0xa400
  set *$BSC_RTCSR = 0xa508
end
##}}}

##{{{  MB250 Memory
define mb250_memory_define
  memory-add Flash 0x00000000 16 ROM
  memory-add SDRAM 0x0c000000 32 RAM
end

define mb250_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x0c000000 32 RAM
  sim_addmemory 0xff000000 16 DEV
end
##}}}

define mb250_setup
  sh7750_define
  mb250_memory_define

  sh7750_si_regs

  mb250_configure

  set *$CCN_CCR = 0x0000090d
end

document mb250_setup
Configure an ST407750 Orion HD1 board
Usage: mb250_setup
end

define mb250sim_setup
  sh7750_define
  mb250_memory_define

  sh7750_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb250sim_setup
Configure a simulated ST407750 Orion HD1 board
Usage: mb250sim_setup
end

define mb250_fsim_setup
  sh7750_fsim_core_setup
  mb250_sim_memory_define
end

document mb250_fsim_setup
Configure functional simulator for ST407750 Orion HD1 board
Usage: mb250_fsim_setup
end

define mb250_psim_setup
  sh7750_psim_core_setup
  mb250_sim_memory_define
end

document mb250_psim_setup
Configure performance simulator for ST407750 Orion HD1 board
Usage: mb250_psim_setup
end

define mb250_display_registers
  sh7750_display_si_regs
end

document mb250_display_registers
Display the ST407750 configuration registers
Usage: mb250_display_registers
end
