##------------------------------------------------------------------------------
## g2tmm.cmd - ST40GX1+ST5512 TMM Canal+ G2 Platform
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  G2TMM EMI Configuration
define g2tmm_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000c
  set *$EMI_BANK4_BASEADDRESS = 0x00000010
  set *$EMI_BANK5_BASEADDRESS = 0x00000014

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x000026d1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x8b001000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x8b010003
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000008

##------------------------------------------------------------------------------
## Bank 1 - Unknown Device
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for unknown device 16Mb"

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x000016d1
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x0a000000
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x0a000000
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000008

##------------------------------------------------------------------------------
## Bank 2 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 3 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 4 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 5 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Undefined 47Mb"

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/1 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000000

  if ((*$SYSCONF_SYS_STAT2_0 & 0x00000007) == 0x00000001)
    ## _ST_display (_procname) "EMI MPX CLOCK @ 1/2 bus clock"
    set *$EMI_MPXCLKSEL = 0x00000001
  else
    ## _ST_display (_procname) "EMI MPX CLOCK @ 1/1 bus clock"
    set *$EMI_MPXCLKSEL = 0x00000000
  end

  set *$EMI_CLKENABLE = 0x00000001
end
##}}}

##{{{  G2TMM LMI Configuration
define g2tmm_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM"

##------------------------------------------------------------------------------
## Program clock generator registers
##------------------------------------------------------------------------------

  if ((*$SYSCONF_SYS_STAT2_0 & 0x00000007) == 0x00000001)
    ## _ST_display (_procname) "SDRAM CLOCK @ 2/3 processor clock"
    set *$CLOCKGENA_CLK4CR = 0x00000008
    set *$CLOCKGENA_CLK4CR = (0x00000003|0x00000008)
    set *$CLOCKGENA_CLK4CR = 0x00000003
  end

  ## _ST_display (_procname) "SDRAM CLOCK @ 88MHz (ClockGen B)"
  st40_set_clockgen_b_pll1 0x14 0x82 0x1
  set *$CLOCKGENB_CPG_BYPASS = 0x00000001
  set *$CLOCKGENB_CLK1CR = 0x00000000
  set *$CLOCKGENB_CLK_SELCR = 0x00000001

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  ## NOTE:
  ##   bit 8 = 0 for SSTL (DDR), 1 for LVTTL (PC-SDRAM)
  ##   bit 9 = 0 for internal ref voltage, 1 for external ref voltage
  ##   bit 10 = 1 to by pass ECLK180 retime
  ##   bit 11 = 1 for PC-SDRAM, 0 for DDR notLMICOMP25_EN
  ##   bit 12 = 1 for PC-SDRAM, 0 for DDR LMICOMP33_EN
  ##   bits [13,14] = LVTTL drive strength for data & data strobe pads
  ##   bits [15,16] = LVTTL drive strength for address & control pads

  set *$SYSCONF_SYS_CON2_0 = 0x00000000

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM = 0x01570243

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR = 0x00001108

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0 = 0x0c001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1 = 0x0e001500

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR = 0x00000003
  set *$LMI_SCR = 0x00000001
  set *$LMI_SCR = 0x00000002
  set *($LMI_SDMR0+(0x00002000/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00002000/sizeof(*$LMI_SDMR1))) = 0x0
  set *($LMI_SDMR0+(0x00000918/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000918/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR = 0x00000002
  set *$LMI_SCR = 0x00000004
  set *$LMI_SCR = 0x00000004
  set *$LMI_SCR = 0x00000004
  set *($LMI_SDMR0+(0x00000118/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000118/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR = 0x00000000
end
##}}}

##{{{  ST40GX1 IO Configuration
define st40gx1_io_configure
  ## _ST_display (_procname) "Configuring access to IO block"

  ## Control memory bridge 4 - I/O block initiator:
  ## - asynchronous, one cycle FIFO latency, software reset disabled, strobe required to latch the data
  set *$SYSCONF_SYS_CON1_0 = 0x8e000000

  ## Control memory bridge 5 - I/O block target:
  ## - asynchronous, one cycle FIFO latency, software reset disabled, strobe required to latch the data
  set *$SYSCONF_SYS_CON1_1 = 0x00000047
end
##}}}

##{{{  G2TMM EMPI Configuration
define g2tmm_empi_map_regions_01
  ## _ST_display (_procname) "EMPI REGION 0, Bypass : GX1 internal devices : 5514 base address = 78000000 to 7bffffff"
  set *$EMPI_RBAR0 = 0x18000000
  set *$EMPI_RSR0 = 0x03ff0019
  set *$EMPI_RLAR0 = 0x18000000

  ## _ST_display (_procname) "EMPI REGION 1, Bypass : Shared DDR Memory 64MB : 5514 base address = 7c000000 to 7fffffff"
  set *$EMPI_RBAR1 = 0x1c000000
  set *$EMPI_RSR1 = 0x03ff0019
  set *$EMPI_RLAR1 = 0x08000000

  ## EMPI REGION 2 to N : Undefined

  set *$EMPI_MPXCFG = 0x00000002
end

define g2tmm_empi_map_regions_01a
  ## _ST_display (_procname) "EMPI REGION 0, Bypass : GX1 internal devices : 5514 base address = 78000000 to 7bfffffc"
  set *$EMPI_RBAR0 = 0x18000000
  set *$EMPI_RSR0 = 0x03ff0019
  set *$EMPI_RLAR0 = 0x18000000

  ## _ST_display (_procname) "EMPI REGION 1, FIFO 2 : Shared DDR Memory 64MB : 5514 base address = 7c000000 to 7ffffffc"
  set *$EMPI_RBAR1 = 0x1c000000
  set *$EMPI_RSR1 = 0x03ff0005
  set *$EMPI_RLAR1 = 0x08000000
  set *$EMPI_DMACFG2 = 0x00000008

  ## EMPI REGION 2 to N : Undefined

  set *$EMPI_MPXCFG = 0x00000002
end

define g2tmm_empi_map_regions_01234
  ## _ST_display (_procname) "EMPI REGION 0, Bypass : GX1 internal devices : 5514 base address = 78000000 to 7bfffffc"
  set *$EMPI_RBAR0 = 0x18000000
  set *$EMPI_RSR0 = 0x03ff0019
  set *$EMPI_RLAR0 = 0x18000000

  ## _ST_display (_procname) "EMPI REGION 1, FIFO 0 : Shared DDR Memory 16MB : 5514 base address = 7c000000 to 7cffffff"
  set *$EMPI_RBAR1 = 0x1c000000
  set *$EMPI_RSR1 = 0x00ff0001
  set *$EMPI_RLAR1 = 0x08000000
  set *$EMPI_DMACFG0 = 0x00000000

  ## _ST_display (_procname) "EMPI REGION 2, FIFO 1 : Shared DDR Memory 16MB : 5514 base address = 7d000000 to 7dffffff"
  set *$EMPI_RBAR2 = 0x1d000000
  set *$EMPI_RSR2 = 0x00ff0003
  set *$EMPI_RLAR2 = 0x09000000
  set *$EMPI_DMACFG1 = 0x00000008

  ## _ST_display (_procname) "EMPI REGION 3, FIFO 2 : Shared DDR Memory 16MB : 5514 base address = 7e000000 to 7effffff"
  set *$EMPI_RBAR3 = 0x1e000000
  set *$EMPI_RSR3 = 0x00ff0005
  set *$EMPI_RLAR3 = 0x0a000000
  set *$EMPI_DMACFG2 = 0x00000008

  ## _ST_display (_procname) "EMPI REGION 4, FIFO 3 : Shared DDR Memory 16MB : 5514 base address = 7f000000 to 7fffffff"
  set *$EMPI_RBAR4 = 0x1f000000
  set *$EMPI_RSR4 = 0x00ff0007
  set *$EMPI_RLAR4 = 0x0b000000
  set *$EMPI_DMACFG3 = 0x00000008

  ## EMPI REGION 5 to N : Undefined

  set *$EMPI_MPXCFG = 0x00000002
end

define g2tmm_empi_configure
  g2tmm_empi_map_regions_01
end
##}}}

##{{{  G2TMM MPX Configuration
define g2tmm_mpx_configure
  if ((*$MPXARB_CONTROL & 0x00000001) == 0x00000001)
    ## _ST_display (_procname) "MASTER-MASTER mode detected, MPX bus parked to STi5514"
    set *$MPXARB_CONTROL = 0x000000d4
  else
    ## _ST_display (_procname) "MASTER-SLAVE mode detected, STi5514 access authorized"
    set *$MPXARB_CONTROL = 0x00000006
  end
end
##}}}

##{{{  G2TMM PIO Configuration
define g2tmm_pio_configure
  set *$PIO1_CLEAR_PC0 = 0x00000010
  set *$PIO1_SET_PC1 = 0x00000010
  set *$PIO1_CLEAR_PC2 = 0x00000010
  set *$PIO1_SET_POUT = 0x00000010
end
##}}}

##{{{  G2TMM Memory
define g2tmm_memory_define
  memory-add Flash     0x00000000 16 ROM
  memory-add LMI_SDRAM 0x08000000 96 RAM
end

define g2tmm_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x08000000 96 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define g2tmm_setup
  st40gx1_define
  g2tmm_memory_define

  st40gx1_si_regs

  g2tmm_emi_configure
  g2tmm_lmi_configure

  st40gx1_io_configure

  g2tmm_empi_configure
  g2tmm_mpx_configure
  g2tmm_pio_configure

  set *$CCN_CCR = 0x0000090d
end

document g2tmm_setup
Configure a TMM Canal+ G2 board
Usage: g2tmm_setup
end

define g2tmmsim_setup
  st40gx1_define
  g2tmm_memory_define

  st40gx1_si_regs

  set *$CCN_CCR = 0x0000090d
end

document g2tmmsim_setup
Configure a simulated TMM Canal+ G2 board
Usage: g2tmmsim_setup
end

define g2tmm_fsim_setup
  st40gx1_fsim_core_setup
  g2tmm_sim_memory_define
end

document g2tmm_fsim_setup
Configure functional simulator for TMM Canal+ G2 board
Usage: g2tmm_fsim_setup
end

define g2tmm_psim_setup
  st40gx1_psim_core_setup
  g2tmm_sim_memory_define
end

document g2tmm_psim_setup
Configure performance simulator for TMM Canal+ G2 board
Usage: g2tmm_psim_setup
end

define g2tmm_display_registers
  st40gx1_display_si_regs
end

document g2tmm_display_registers
Display the ST40GX1 configuration registers
Usage: g2tmm_display_registers
end
