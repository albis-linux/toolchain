################################################################################

define mb618sim
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb618_fsim_setup mb618sim_setup $arg0
  else
    connectsh4simle mb618_fsim_setup mb618sim_setup ""
  end
end

document mb618sim
Connect to and configure a simulated STi7111-Mboard board
Usage: mb618sim
end

define mb618simse
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb618se_fsim_setup mb618simse_setup $arg0
  else
    connectsh4simle mb618se_fsim_setup mb618simse_setup ""
  end
end

document mb618simse
Connect to and configure a simulated STi7111-Mboard board (STi7111 in 32-bit SE mode)
Usage: mb618simse
end

define mb618simseuc
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb618se_fsim_setup mb618simseuc_setup $arg0
  else
    connectsh4simle mb618se_fsim_setup mb618simseuc_setup ""
  end
end

document mb618simseuc
Connect to and configure a simulated STi7111-Mboard board (STi7111 in 32-bit SE mode with uncached mappings)
Usage: mb618simseuc
end

define mb618simse29
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb618se_fsim_setup mb618simse29_setup $arg0
  else
    connectsh4simle mb618se_fsim_setup mb618simse29_setup ""
  end
end

document mb618simse29
Connect to and configure a simulated STi7111-Mboard board (STi7111 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb618simse29
end

################################################################################

define mb618psim
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb618_psim_setup mb618sim_setup $arg0
  else
    connectsh4psimle mb618_psim_setup mb618sim_setup ""
  end
end

document mb618psim
Connect to and configure a simulated STi7111-Mboard board
Usage: mb618psim
end

define mb618psimse
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb618se_psim_setup mb618simse_setup $arg0
  else
    connectsh4psimle mb618se_psim_setup mb618simse_setup ""
  end
end

document mb618psimse
Connect to and configure a simulated STi7111-Mboard board (STi7111 in 32-bit SE mode)
Usage: mb618psimse
end

define mb618psimseuc
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb618se_psim_setup mb618simseuc_setup $arg0
  else
    connectsh4psimle mb618se_psim_setup mb618simseuc_setup ""
  end
end

document mb618psimseuc
Connect to and configure a simulated STi7111-Mboard board (STi7111 in 32-bit SE mode with uncached mappings)
Usage: mb618psimseuc
end

define mb618psimse29
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source mb618.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb618se_psim_setup mb618simse29_setup $arg0
  else
    connectsh4psimle mb618se_psim_setup mb618simse29_setup ""
  end
end

document mb618psimse29
Connect to and configure a simulated STi7111-Mboard board (STi7111 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb618psimse29
end

################################################################################
