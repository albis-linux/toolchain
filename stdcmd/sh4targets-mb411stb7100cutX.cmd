################################################################################

define mb411stb7100cutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb411stb7100cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb411stb7100_setup $arg2
  else
    connectsh4le $arg1 mb411stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7100cutX
Connect to and configure an STb7100-Mboard board
Usage: mb411stb7100cutX <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7100cutXbypass
  if ($argc > 2)
    mb411stb7100cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7100cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7100bypass_setup"
  end
end

document mb411stb7100cutXbypass
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411stb7100cutXbypass <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7100cutXstmmx
  if ($argc > 2)
    mb411stb7100cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7100cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7100stmmx_setup"
  end
end

document mb411stb7100cutXstmmx
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7100cutXstmmx <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb411cutX
  if ($argc > 2)
    mb411stb7100cutX $arg0 $arg1 $arg2
  else
    mb411stb7100cutX $arg0 $arg1
  end
end

document mb411cutX
Connect to and configure an STb7100-Mboard board
Usage: mb411cutX <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411cutXbypass
  if ($argc > 2)
    mb411stb7100cutXbypass $arg0 $arg1 $arg2
  else
    mb411stb7100cutXbypass $arg0 $arg1
  end
end

document mb411cutXbypass
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411cutXbypass <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411cutXstmmx
  if ($argc > 2)
    mb411stb7100cutXstmmx $arg0 $arg1 $arg2
  else
    mb411stb7100cutXstmmx $arg0 $arg1
  end
end

document mb411cutXstmmx
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411cutXstmmx <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb411stb7100cutXusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb411stb7100cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb411stb7100_setup $arg2
  else
    connectsh4usble $arg1 mb411stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7100cutXusb
Connect to and configure an STb7100-Mboard board
Usage: mb411stb7100cutXusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7100cutXbypassusb
  if ($argc > 2)
    mb411stb7100cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7100cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7100bypass_setup"
  end
end

document mb411stb7100cutXbypassusb
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411stb7100cutXbypassusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7100cutXstmmxusb
  if ($argc > 2)
    mb411stb7100cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7100cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7100stmmx_setup"
  end
end

document mb411stb7100cutXstmmxusb
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7100cutXstmmxusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

################################################################################

define mb411cutXusb
  if ($argc > 2)
    mb411stb7100cutXusb $arg0 $arg1 $arg2
  else
    mb411stb7100cutXusb $arg0 $arg1
  end
end

document mb411cutXusb
Connect to and configure an STb7100-Mboard board
Usage: mb411cutXusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411cutXbypassusb
  if ($argc > 2)
    mb411stb7100cutXbypassusb $arg0 $arg1 $arg2
  else
    mb411stb7100cutXbypassusb $arg0 $arg1
  end
end

document mb411cutXbypassusb
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411cutXbypassusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411cutXstmmxusb
  if ($argc > 2)
    mb411stb7100cutXstmmxusb $arg0 $arg1 $arg2
  else
    mb411stb7100cutXstmmxusb $arg0 $arg1
  end
end

document mb411cutXstmmxusb
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411cutXstmmxusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

################################################################################
