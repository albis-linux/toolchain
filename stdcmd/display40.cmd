################################################################################
## Display register
################################################################################

##{{{  SHx
define __shx_display_reg
  if (sizeof(*$arg0) == 1)
    printf "$arg0 (0x%08x) = 0x%02x\n", $arg0, *$arg0
  end
  if (sizeof(*$arg0) == 2)
    printf "$arg0 (0x%08x) = 0x%04x\n", $arg0, *$arg0
  end
  if (sizeof(*$arg0) == 4)
    printf "$arg0 (0x%08x) = 0x%08x\n", $arg0, *$arg0
  end
  if (sizeof(*$arg0) == 8)
    printf "$arg0 (0x%08x) = 0x%08x%08x\n", $arg0, *$arg0_1, *$arg0_0
  end
end
##}}}

################################################################################
## Core SH4 control registers
################################################################################

##{{{  SH4 CCN
## Core control registers (common to all SH4 variants)
define sh4_display_ccn_regs
  __shx_display_reg $CCN_PTEH
  __shx_display_reg $CCN_PTEL
  __shx_display_reg $CCN_TTB
  __shx_display_reg $CCN_TEA
  __shx_display_reg $CCN_MMUCR
  __shx_display_reg $CCN_BASRA
  __shx_display_reg $CCN_BASRB
  __shx_display_reg $CCN_CCR
  __shx_display_reg $CCN_TRA
  __shx_display_reg $CCN_EXPEVT
  __shx_display_reg $CCN_INTEVT
  __shx_display_reg $CCN_PVR
  __shx_display_reg $CCN_PTEA
  __shx_display_reg $CCN_QACR0
  __shx_display_reg $CCN_QACR1
  __shx_display_reg $CCN_CVR
  __shx_display_reg $CCN_PRR
end
##}}}

##{{{  SH4 UBC
## User Break Controller control registers (common to all SH4)
define sh4_display_ubc_regs
  __shx_display_reg $UBC_BARA
  __shx_display_reg $UBC_BAMRA
  __shx_display_reg $UBC_BBRA
  __shx_display_reg $UBC_BASRA
  __shx_display_reg $UBC_BARB
  __shx_display_reg $UBC_BAMRB
  __shx_display_reg $UBC_BBRB
  __shx_display_reg $UBC_BASRB
  __shx_display_reg $UBC_BDRB
  __shx_display_reg $UBC_BDMRB
  __shx_display_reg $UBC_BRCR
end
##}}}

##{{{  SH4 UDI
## User Debug Interface control registers (common to all SH4)
define sh4_display_udi_regs
  __shx_display_reg $UDI_SDIR
  __shx_display_reg $UDI_SDDR
  __shx_display_reg $UDI_SDDRH
  __shx_display_reg $UDI_SDDRL
  __shx_display_reg $UDI_SDINT
end
##}}}

##{{{  SH4 AUD
## Advanced User Debugger control registers (common to all SH4)
define sh4_display_aud_regs
  __shx_display_reg $AUD_AUCSR
  __shx_display_reg $AUD_AUWASR
  __shx_display_reg $AUD_AUWAER
  __shx_display_reg $AUD_AUWBSR
  __shx_display_reg $AUD_AUWBER
end
##}}}

################################################################################
## Generic SH4 control registers
################################################################################

##{{{  SH4 TMU
## Timer Unit control registers (common to all SH4 variants)
define sh4_display_tmu_regs
  __shx_display_reg $TMU_TOCR
  __shx_display_reg $TMU_TSTR
  __shx_display_reg $TMU_TCOR0
  __shx_display_reg $TMU_TCNT0
  __shx_display_reg $TMU_TCR0
  __shx_display_reg $TMU_TCOR1
  __shx_display_reg $TMU_TCNT1
  __shx_display_reg $TMU_TCR1
  __shx_display_reg $TMU_TCOR2
  __shx_display_reg $TMU_TCNT2
  __shx_display_reg $TMU_TCR2
  __shx_display_reg $TMU_TCPR2
end
##}}}

##{{{  SH4 RTC
## Real Time Clock control registers (common to all SH4 variants)
define sh4_display_rtc_regs
  __shx_display_reg $RTC_R64CNT
  __shx_display_reg $RTC_RSECCNT
  __shx_display_reg $RTC_RMINCNT
  __shx_display_reg $RTC_RHRCNT
  __shx_display_reg $RTC_RWKCNT
  __shx_display_reg $RTC_RDAYCNT
  __shx_display_reg $RTC_RMONCNT
  __shx_display_reg $RTC_RYRCNT
  __shx_display_reg $RTC_RSECAR
  __shx_display_reg $RTC_RMINAR
  __shx_display_reg $RTC_RHRAR
  __shx_display_reg $RTC_RWKAR
  __shx_display_reg $RTC_RDAYAR
  __shx_display_reg $RTC_RMONAR
  __shx_display_reg $RTC_RCR1
  __shx_display_reg $RTC_RCR2
end
##}}}

##{{{  SH4 INTC
## Interrupt Controller control registers
define sh4_display_intc_regs
  __shx_display_reg $INTC_ICR
  __shx_display_reg $INTC_IPRA
  __shx_display_reg $INTC_IPRB
  __shx_display_reg $INTC_IPRC
  __shx_display_reg $INTC_IPRD
end
##}}}

##{{{  SH4 CPG
## Clock Pulse Generator control registers
define sh4_display_cpg_regs
  __shx_display_reg $CPG_FRQCR
  __shx_display_reg $CPG_STBCR
  __shx_display_reg $CPG_WTCNT_R
  __shx_display_reg $CPG_WTCSR_R
  __shx_display_reg $CPG_STBCR2
end
##}}}

################################################################################
## SH775x control registers
################################################################################

##{{{  SH775x DMAC
## Direct Memory Access Controller control registers (all SH775x variants)
define sh775x_display_dmac_regs
  __shx_display_reg $DMAC_SAR0
  __shx_display_reg $DMAC_DAR0
  __shx_display_reg $DMAC_DMATCR0
  __shx_display_reg $DMAC_CHCR0
  __shx_display_reg $DMAC_SAR1
  __shx_display_reg $DMAC_DAR1
  __shx_display_reg $DMAC_DMATCR1
  __shx_display_reg $DMAC_CHCR1
  __shx_display_reg $DMAC_SAR2
  __shx_display_reg $DMAC_DAR2
  __shx_display_reg $DMAC_DMATCR2
  __shx_display_reg $DMAC_CHCR2
  __shx_display_reg $DMAC_SAR3
  __shx_display_reg $DMAC_DAR3
  __shx_display_reg $DMAC_DMATCR3
  __shx_display_reg $DMAC_CHCR3
  __shx_display_reg $DMAC_DMAOR
end
##}}}

##{{{  SH775x BSC
## Bus State Controller control registers (all SH775x variants)
define sh775x_display_bsc_regs
  __shx_display_reg $BSC_BCR1
  __shx_display_reg $BSC_BCR2
  __shx_display_reg $BSC_WCR1
  __shx_display_reg $BSC_WCR2
  __shx_display_reg $BSC_WCR3
  __shx_display_reg $BSC_MCR
  __shx_display_reg $BSC_PCR
  __shx_display_reg $BSC_RTCSR
  __shx_display_reg $BSC_RTCNT
  __shx_display_reg $BSC_RTCOR
  __shx_display_reg $BSC_RFCR
  __shx_display_reg $BSC_PCTRA
  __shx_display_reg $BSC_PDTRA
  __shx_display_reg $BSC_PCTRB
  __shx_display_reg $BSC_PDTRB
  __shx_display_reg $BSC_GPIOIC
  __shx_display_reg $BSC_SDMR2
  __shx_display_reg $BSC_SDMR3
end
##}}}

##{{{  SH775x SCI
## Serial Communication Interface control registers (all SH775x variants)
define sh775x_display_sci_regs
  __shx_display_reg $SCI_SCSMR1
  __shx_display_reg $SCI_SCBRR1
  __shx_display_reg $SCI_SCSCR1
  __shx_display_reg $SCI_SCTDR1
  __shx_display_reg $SCI_SCSSR1
  __shx_display_reg $SCI_SCRDR1
  __shx_display_reg $SCI_SCSCMR1
  __shx_display_reg $SCI_SCSPTR1
end
##}}}

##{{{  SH775x SCIF
## Serial Communication Interface with FIFO control registers (all SH775x variants)
define sh775x_display_scif_regs
  __shx_display_reg $SCIF_SCSMR2
  __shx_display_reg $SCIF_SCBRR2
  __shx_display_reg $SCIF_SCSCR2
  __shx_display_reg $SCIF_SCFTDR2
  __shx_display_reg $SCIF_SCFSR2
  __shx_display_reg $SCIF_SCFRDR2
  __shx_display_reg $SCIF_SCFCR2
  __shx_display_reg $SCIF_SCFDR2
  __shx_display_reg $SCIF_SCSPTR2
  __shx_display_reg $SCIF_SCLSR2
end
##}}}

################################################################################
## SH7750 control registers
################################################################################

##{{{  SH7750 INTC
## Interrupt Controller control registers (all SH7750 variants)
define sh7750_display_intc_regs
  __shx_display_reg $INTC_ICR
  __shx_display_reg $INTC_IPRA
  __shx_display_reg $INTC_IPRB
  __shx_display_reg $INTC_IPRC
end
##}}}

################################################################################
## SH7751 control registers
################################################################################

##{{{  SH7751 CPG
## Clock Pulse Generator control registers (all SH7751 variants)
define sh7751_display_cpg_regs
  __shx_display_reg $CPG_CLKSTP00
  __shx_display_reg $CPG_CLKSTPCLR00
end
##}}}

##{{{  SH7751 INTC
## Interrupt Controller control registers (all SH7751 variants)
define sh7751_display_intc_regs
  __shx_display_reg $INTC_INTPRI00
  __shx_display_reg $INTC_INTREQ00
  __shx_display_reg $INTC_INTMSK00
  __shx_display_reg $INTC_INTMSKCLR00
end
##}}}

##{{{  SH7751 TMU
## Timer Unit control registers (all SH7751 variants)
define sh7751_display_tmu_regs
  sh4_display_tmu_regs

  __shx_display_reg $TMU_TSTR2
  __shx_display_reg $TMU_TCOR3
  __shx_display_reg $TMU_TCNT3
  __shx_display_reg $TMU_TCR3
  __shx_display_reg $TMU_TCOR4
  __shx_display_reg $TMU_TCNT4
  __shx_display_reg $TMU_TCR4
end
##}}}

################################################################################
## SH42xx control registers
################################################################################

##{{{  SH42xx CPG2
## Clock Pulse Generator control registers
define sh42xx_display_cpg2_regs
  __shx_display_reg $CPG2_CLKSTP00
  __shx_display_reg $CPG2_CLKSTPCLR00
  __shx_display_reg $CPG2_CLKSTPACK00
  __shx_display_reg $CPG2_FRQCR3
end
##}}}

##{{{  SH42xx DMAC
## Direct Memory Access Controller control registers
define sh42xx_display_dmac_regs
  __shx_display_reg $DMAC_VCR_L
  __shx_display_reg $DMAC_VCR_H
  __shx_display_reg $DMAC_COMMON
  __shx_display_reg $DMAC_SAR0
  __shx_display_reg $DMAC_DAR0
  __shx_display_reg $DMAC_COUNT0
  __shx_display_reg $DMAC_CTRL0
  __shx_display_reg $DMAC_STATUS0
  __shx_display_reg $DMAC_SAR1
  __shx_display_reg $DMAC_DAR1
  __shx_display_reg $DMAC_COUNT1
  __shx_display_reg $DMAC_CTRL1
  __shx_display_reg $DMAC_STATUS1
  __shx_display_reg $DMAC_SAR2
  __shx_display_reg $DMAC_DAR2
  __shx_display_reg $DMAC_COUNT2
  __shx_display_reg $DMAC_CTRL2
  __shx_display_reg $DMAC_DMAEXG
end
##}}}

##{{{  SH42xx INTC2
## Interrupt Controller control registers
define sh42xx_display_intc2_regs
  __shx_display_reg $INTC2_INTPRI00
  __shx_display_reg $INTC2_INTREQ00
  __shx_display_reg $INTC2_INTMSK00
  __shx_display_reg $INTC2_INTMSKCLR00
end
##}}}

##{{{  SH42xx SCIF
## Serial Communication Interfaces control registers
define sh42xx_display_scif_regs
  __shx_display_reg $SCIF_SCSMR
  __shx_display_reg $SCIF_SCBRR
  __shx_display_reg $SCIF_SCSCR
  __shx_display_reg $SCIF_SCFTDR
  __shx_display_reg $SCIF_SCFSR
  __shx_display_reg $SCIF_SCFRDR
  __shx_display_reg $SCIF_SCFCR
  __shx_display_reg $SCIF_SCFDR
  __shx_display_reg $SCIF_SCSPTR
  __shx_display_reg $SCIF_SCLSR
end
##}}}

##{{{  SH42xx EMI
## External Memory Interface control registers
define sh42xx_display_emi_regs
  __shx_display_reg $EMI_VCR_L
  __shx_display_reg $EMI_VCR_H
  __shx_display_reg $EMI_MIM
  __shx_display_reg $EMI_SCR
  __shx_display_reg $EMI_STR
  __shx_display_reg $EMI_COC
  __shx_display_reg $EMI_SDRA0
  __shx_display_reg $EMI_SDRA1
  __shx_display_reg $EMI_SDMR0
  __shx_display_reg $EMI_SDMR1
end
##}}}

##{{{  SH42xx FEMI
## Flash External Memory Interface control registers
define sh42xx_display_femi_regs
  __shx_display_reg $FEMI_VCR_L
  __shx_display_reg $FEMI_VCR_H
  __shx_display_reg $FEMI_MDCR
  __shx_display_reg $FEMI_A0MCR
  __shx_display_reg $FEMI_A1MCR
  __shx_display_reg $FEMI_A2MCR
  __shx_display_reg $FEMI_A3MCR
  __shx_display_reg $FEMI_A4MCR
end
##}}}

################################################################################
## ST40 control registers
################################################################################

##{{{  ST40 INTC2
## Interrupt Controller control registers (all ST40 variants)
define st40_display_intc2_regs
  __shx_display_reg $INTC2_INTPRI00
  __shx_display_reg $INTC2_INTPRI04
  __shx_display_reg $INTC2_INTPRI08
  __shx_display_reg $INTC2_INTREQ00
  __shx_display_reg $INTC2_INTREQ04
  __shx_display_reg $INTC2_INTREQ08
  __shx_display_reg $INTC2_INTMSK00
  __shx_display_reg $INTC2_INTMSK04
  __shx_display_reg $INTC2_INTMSK08
  __shx_display_reg $INTC2_INTMSKCLR00
  __shx_display_reg $INTC2_INTMSKCLR04
  __shx_display_reg $INTC2_INTMSKCLR08
  __shx_display_reg $INTC2_INTC2MODE
end
##}}}

##{{{  ST40 ILC
## Interrupt Level Controller control registers (all ST40 variants)
define st40_display_ilc_x_regs
  __shx_display_reg $ILC_INPUT_INTERRUPT_$arg0
  __shx_display_reg $ILC_STATUS_$arg0
  __shx_display_reg $ILC_CLEAR_STATUS_$arg0
  __shx_display_reg $ILC_ENABLE_$arg0
  __shx_display_reg $ILC_CLEAR_ENABLE_$arg0
  __shx_display_reg $ILC_SET_ENABLE_$arg0
  __shx_display_reg $ILC_WAKEUP_ENABLE_$arg0
  __shx_display_reg $ILC_WAKEUP_ACTIVE_LEVEL_$arg0
end

define st40_display_ilc_regs
  st40_display_ilc_x_regs 0
  st40_display_ilc_x_regs 1
  st40_display_ilc_x_regs 2
  st40_display_ilc_x_regs 3
  st40_display_ilc_x_regs 4
  st40_display_ilc_x_regs 5
  st40_display_ilc_x_regs 6
  st40_display_ilc_x_regs 7
  st40_display_ilc_x_regs 8
  st40_display_ilc_x_regs 9
  st40_display_ilc_x_regs 10
  st40_display_ilc_x_regs 11
  st40_display_ilc_x_regs 12
  st40_display_ilc_x_regs 13
  st40_display_ilc_x_regs 14
  st40_display_ilc_x_regs 15
  st40_display_ilc_x_regs 16
  st40_display_ilc_x_regs 17
  st40_display_ilc_x_regs 18
  st40_display_ilc_x_regs 19
  st40_display_ilc_x_regs 20
  st40_display_ilc_x_regs 21
  st40_display_ilc_x_regs 22
  st40_display_ilc_x_regs 23
  st40_display_ilc_x_regs 24
  st40_display_ilc_x_regs 25
  st40_display_ilc_x_regs 26
  st40_display_ilc_x_regs 27
  st40_display_ilc_x_regs 28
  st40_display_ilc_x_regs 29
  st40_display_ilc_x_regs 30
  st40_display_ilc_x_regs 31
end
##}}}

##{{{  ST40 SCIF
## Serial Communication Interfaces control registers (all ST40 variants)
define st40_display_scif_regs
  __shx_display_reg $SCIF$arg0_SCSMR
  __shx_display_reg $SCIF$arg0_SCBRR
  __shx_display_reg $SCIF$arg0_SCSCR
  __shx_display_reg $SCIF$arg0_SCFTDR
  __shx_display_reg $SCIF$arg0_SCFSR
  __shx_display_reg $SCIF$arg0_SCFRDR
  __shx_display_reg $SCIF$arg0_SCFCR
  __shx_display_reg $SCIF$arg0_SCFDR
  __shx_display_reg $SCIF$arg0_SCSPTR
  __shx_display_reg $SCIF$arg0_SCLSR
end
##}}}

##{{{  ST40 CLOCKGEN
## Clock Generator control registers (all ST40 variants)
define st40_display_clockgen_regs
  __shx_display_reg $CLOCKGEN$arg0_PLL1CR1
  __shx_display_reg $CLOCKGEN$arg0_PLL1CR2
  __shx_display_reg $CLOCKGEN$arg0_PLL2CR
  __shx_display_reg $CLOCKGEN$arg0_STBREQCR
  __shx_display_reg $CLOCKGEN$arg0_STBREQCR_SET
  __shx_display_reg $CLOCKGEN$arg0_STBREQCR_CLR
  __shx_display_reg $CLOCKGEN$arg0_STBACKCR
  __shx_display_reg $CLOCKGEN$arg0_CLK4CR
  __shx_display_reg $CLOCKGEN$arg0_CPG_BYPASS
  __shx_display_reg $CLOCKGEN$arg0_PLL2_MUXCR
  __shx_display_reg $CLOCKGEN$arg0_CLK1CR
  __shx_display_reg $CLOCKGEN$arg0_CLK2CR
  __shx_display_reg $CLOCKGEN$arg0_CLK3CR
  __shx_display_reg $CLOCKGEN$arg0_CLK_SELCR
end
##}}}

##{{{  ST40 DMAC
## Direct Memeory Access Controller control registers (all ST40 variants)
define st40_display_dmac_chan_0_regs
  __shx_display_reg $DMAC_CHAN0_IDENTITY
  __shx_display_reg $DMAC_CHAN0_ENABLE
  __shx_display_reg $DMAC_CHAN0_DISABLE
  __shx_display_reg $DMAC_CHAN0_STATUS
  __shx_display_reg $DMAC_CHAN0_ACTION
  __shx_display_reg $DMAC_CHAN0_POINTER
  __shx_display_reg $DMAC_CHAN0_SUBBASE
  __shx_display_reg $DMAC_CHAN0_SUBENABLE
  __shx_display_reg $DMAC_CHAN0_SUBDISABLE
  __shx_display_reg $DMAC_CHAN0_SUBINT_ENB
  __shx_display_reg $DMAC_CHAN0_SUBINT_DIS
  __shx_display_reg $DMAC_CHAN0_SUBINT_STAT
  __shx_display_reg $DMAC_CHAN0_SUNINT_ACT
  __shx_display_reg $DMAC_CHAN0_CONTROL
  __shx_display_reg $DMAC_CHAN0_COUNT
  __shx_display_reg $DMAC_CHAN0_SAR
  __shx_display_reg $DMAC_CHAN0_DAR
end

define st40_display_dmac_chan_x_regs
  __shx_display_reg $DMAC_CHAN$arg0_IDENTITY
  __shx_display_reg $DMAC_CHAN$arg0_ENABLE
  __shx_display_reg $DMAC_CHAN$arg0_DISABLE
  __shx_display_reg $DMAC_CHAN$arg0_STATUS
  __shx_display_reg $DMAC_CHAN$arg0_ACTION
  __shx_display_reg $DMAC_CHAN$arg0_POINTER
  __shx_display_reg $DMAC_CHAN$arg0_REQUEST
  __shx_display_reg $DMAC_CHAN$arg0_CONTROL
  __shx_display_reg $DMAC_CHAN$arg0_COUNT
  __shx_display_reg $DMAC_CHAN$arg0_SAR
  __shx_display_reg $DMAC_CHAN$arg0_DAR
  __shx_display_reg $DMAC_CHAN$arg0_NEXT_PTR
  __shx_display_reg $DMAC_CHAN$arg0_SRC_LENGTH
  __shx_display_reg $DMAC_CHAN$arg0_SRC_STRIDE
  __shx_display_reg $DMAC_CHAN$arg0_DST_LENGTH
  __shx_display_reg $DMAC_CHAN$arg0_DST_STRIDE
end

define st40_display_dmac_regs
  __shx_display_reg $DMAC_VCR_STATUS
  __shx_display_reg $DMAC_VCR_VERSION
  __shx_display_reg $DMAC_ENABLE
  __shx_display_reg $DMAC_DISABLE
  __shx_display_reg $DMAC_STATUS
  __shx_display_reg $DMAC_INTERRUPT
  __shx_display_reg $DMAC_ERROR
  __shx_display_reg $DMAC_DEFINED
  __shx_display_reg $DMAC_HANDSHAKE

  st40_display_dmac_chan_0_regs
  st40_display_dmac_chan_x_regs 1
  st40_display_dmac_chan_x_regs 2
  st40_display_dmac_chan_x_regs 3
  st40_display_dmac_chan_x_regs 4
end
##}}}

##{{{  ST40 PIO
## Parallel I/O control registers (all ST40 variants)
define st40_display_pio_regs
  __shx_display_reg $PIO$arg0_POUT
  __shx_display_reg $PIO$arg0_PIN
  __shx_display_reg $PIO$arg0_PC0
  __shx_display_reg $PIO$arg0_PC1
  __shx_display_reg $PIO$arg0_PC2
  __shx_display_reg $PIO$arg0_PCOMP
  __shx_display_reg $PIO$arg0_PMASK

  ## PIO pseudo registers
  __shx_display_reg $PIO$arg0_SET_POUT
  __shx_display_reg $PIO$arg0_CLEAR_POUT
  __shx_display_reg $PIO$arg0_SET_PC0
  __shx_display_reg $PIO$arg0_CLEAR_PC0
  __shx_display_reg $PIO$arg0_SET_PC1
  __shx_display_reg $PIO$arg0_CLEAR_PC1
  __shx_display_reg $PIO$arg0_SET_PC2
  __shx_display_reg $PIO$arg0_CLEAR_PC2
  __shx_display_reg $PIO$arg0_SET_PCOMP
  __shx_display_reg $PIO$arg0_CLEAR_PCOMP
  __shx_display_reg $PIO$arg0_SET_PMASK
  __shx_display_reg $PIO$arg0_CLEAR_PMASK
end
##}}}

##{{{  ST40 LMI
## Local Memory Interface control registers (all ST40 variants)
define st40_display_lmi_regs
  if ($argc < 1)
    __shx_display_reg $LMI_VCR
    __shx_display_reg $LMI_MIM
    __shx_display_reg $LMI_SCR
    __shx_display_reg $LMI_STR
    __shx_display_reg $LMI_PBS
    __shx_display_reg $LMI_COC
    __shx_display_reg $LMI_SDRA0
    __shx_display_reg $LMI_SDRA1
    __shx_display_reg $LMI_CIC
    __shx_display_reg $LMI_SDMR0
    __shx_display_reg $LMI_SDMR1
  else
    __shx_display_reg $LMI$arg0_VCR
    __shx_display_reg $LMI$arg0_MIM
    __shx_display_reg $LMI$arg0_SCR
    __shx_display_reg $LMI$arg0_STR
    __shx_display_reg $LMI$arg0_PBS
    __shx_display_reg $LMI$arg0_COC
    __shx_display_reg $LMI$arg0_SDRA0
    __shx_display_reg $LMI$arg0_SDRA1
    __shx_display_reg $LMI$arg0_CIC
    __shx_display_reg $LMI$arg0_SDMR0
    __shx_display_reg $LMI$arg0_SDMR1
  end
end
##}}}

##{{{  ST40 LMIGP
define st40_display_lmigp_regs
  if ($argc < 1)
    __shx_display_reg $LMI_VCR
    __shx_display_reg $LMI_MIM
    __shx_display_reg $LMI_SCR
    __shx_display_reg $LMI_STR
    __shx_display_reg $LMI_GCC
    __shx_display_reg $LMI_SDRA0
    __shx_display_reg $LMI_SDRA1
    __shx_display_reg $LMI_CCO
    __shx_display_reg $LMI_SDMR0
    __shx_display_reg $LMI_SDMR1
  else
    __shx_display_reg $LMI$arg0_VCR
    __shx_display_reg $LMI$arg0_MIM
    __shx_display_reg $LMI$arg0_SCR
    __shx_display_reg $LMI$arg0_STR
    __shx_display_reg $LMI$arg0_GCC
    __shx_display_reg $LMI$arg0_SDRA0
    __shx_display_reg $LMI$arg0_SDRA1
    __shx_display_reg $LMI$arg0_CCO
    __shx_display_reg $LMI$arg0_SDMR0
    __shx_display_reg $LMI$arg0_SDMR1
  end
end
##}}}

##{{{  ST40 FMI (HCMOS7)
## Flash Memory Interface control registers (all ST40 variants)
define st40_display_fmi_hcmos7_regs
  __shx_display_reg $FMI_VCR
  __shx_display_reg $FMI_STATUSCFG
  __shx_display_reg $FMI_STATUSLOCK
  __shx_display_reg $FMI_LOCK
  __shx_display_reg $FMI_GENCFG
  __shx_display_reg $FMI_SDRAMNOPGEN
  __shx_display_reg $FMI_SDRAMMODEREG
  __shx_display_reg $FMI_SDRAMINIT
  __shx_display_reg $FMI_REFRESHINIT
  __shx_display_reg $FMI_FLASHCLKSEL
  __shx_display_reg $FMI_SDRAMCLKSEL
  __shx_display_reg $FMI_MPXCLKSEL
  __shx_display_reg $FMI_CLKENABLE
  __shx_display_reg $FMI_BANK0_FMICONFIGDATA0
  __shx_display_reg $FMI_BANK0_FMICONFIGDATA1
  __shx_display_reg $FMI_BANK0_FMICONFIGDATA2
  __shx_display_reg $FMI_BANK0_FMICONFIGDATA3
  __shx_display_reg $FMI_BANK1_FMICONFIGDATA0
  __shx_display_reg $FMI_BANK1_FMICONFIGDATA1
  __shx_display_reg $FMI_BANK1_FMICONFIGDATA2
  __shx_display_reg $FMI_BANK1_FMICONFIGDATA3
  __shx_display_reg $FMI_BANK2_FMICONFIGDATA0
  __shx_display_reg $FMI_BANK2_FMICONFIGDATA1
  __shx_display_reg $FMI_BANK2_FMICONFIGDATA2
  __shx_display_reg $FMI_BANK2_FMICONFIGDATA3
  __shx_display_reg $FMI_BANK3_FMICONFIGDATA0
  __shx_display_reg $FMI_BANK3_FMICONFIGDATA1
  __shx_display_reg $FMI_BANK3_FMICONFIGDATA2
  __shx_display_reg $FMI_BANK3_FMICONFIGDATA3
  __shx_display_reg $FMI_BANK4_FMICONFIGDATA0
  __shx_display_reg $FMI_BANK4_FMICONFIGDATA1
  __shx_display_reg $FMI_BANK4_FMICONFIGDATA2
  __shx_display_reg $FMI_BANK4_FMICONFIGDATA3
  __shx_display_reg $FMI_BANK5_FMICONFIGDATA0
  __shx_display_reg $FMI_BANK5_FMICONFIGDATA1
  __shx_display_reg $FMI_BANK5_FMICONFIGDATA2
  __shx_display_reg $FMI_BANK5_FMICONFIGDATA3

  __shx_display_reg $FMI_BANK0_BASEADDRESS
  __shx_display_reg $FMI_BANK1_BASEADDRESS
  __shx_display_reg $FMI_BANK2_BASEADDRESS
  __shx_display_reg $FMI_BANK3_BASEADDRESS
  __shx_display_reg $FMI_BANK4_BASEADDRESS
  __shx_display_reg $FMI_BANK5_BASEADDRESS
  __shx_display_reg $FMI_BANK_ENABLE
end
##}}}

##{{{  ST40 EMI
## Enhanced Flash Memory Interface control registers (all ST40 variants)
define st40_display_emi_regs
  __shx_display_reg $EMI_VCR
  __shx_display_reg $EMI_STATUSCFG
  __shx_display_reg $EMI_STATUSLOCK
  __shx_display_reg $EMI_LOCK
  __shx_display_reg $EMI_GENCFG
  __shx_display_reg $EMI_SDRAMNOPGEN
  __shx_display_reg $EMI_SDRAMMODEREG
  __shx_display_reg $EMI_SDRAMINIT
  __shx_display_reg $EMI_REFRESHINIT
  __shx_display_reg $EMI_FLASHCLKSEL
  __shx_display_reg $EMI_SDRAMCLKSEL
  __shx_display_reg $EMI_MPXCLKSEL
  __shx_display_reg $EMI_CLKENABLE
  __shx_display_reg $EMI_BANK0_EMICONFIGDATA0
  __shx_display_reg $EMI_BANK0_EMICONFIGDATA1
  __shx_display_reg $EMI_BANK0_EMICONFIGDATA2
  __shx_display_reg $EMI_BANK0_EMICONFIGDATA3
  __shx_display_reg $EMI_BANK1_EMICONFIGDATA0
  __shx_display_reg $EMI_BANK1_EMICONFIGDATA1
  __shx_display_reg $EMI_BANK1_EMICONFIGDATA2
  __shx_display_reg $EMI_BANK1_EMICONFIGDATA3
  __shx_display_reg $EMI_BANK2_EMICONFIGDATA0
  __shx_display_reg $EMI_BANK2_EMICONFIGDATA1
  __shx_display_reg $EMI_BANK2_EMICONFIGDATA2
  __shx_display_reg $EMI_BANK2_EMICONFIGDATA3
  __shx_display_reg $EMI_BANK3_EMICONFIGDATA0
  __shx_display_reg $EMI_BANK3_EMICONFIGDATA1
  __shx_display_reg $EMI_BANK3_EMICONFIGDATA2
  __shx_display_reg $EMI_BANK3_EMICONFIGDATA3
  __shx_display_reg $EMI_BANK4_EMICONFIGDATA0
  __shx_display_reg $EMI_BANK4_EMICONFIGDATA1
  __shx_display_reg $EMI_BANK4_EMICONFIGDATA2
  __shx_display_reg $EMI_BANK4_EMICONFIGDATA3
  __shx_display_reg $EMI_BANK5_EMICONFIGDATA0
  __shx_display_reg $EMI_BANK5_EMICONFIGDATA1
  __shx_display_reg $EMI_BANK5_EMICONFIGDATA2
  __shx_display_reg $EMI_BANK5_EMICONFIGDATA3

  __shx_display_reg $EMI_BANK0_BASEADDRESS
  __shx_display_reg $EMI_BANK1_BASEADDRESS
  __shx_display_reg $EMI_BANK2_BASEADDRESS
  __shx_display_reg $EMI_BANK3_BASEADDRESS
  __shx_display_reg $EMI_BANK4_BASEADDRESS
  __shx_display_reg $EMI_BANK5_BASEADDRESS
  __shx_display_reg $EMI_BANK_ENABLE
end
##}}}

##{{{  ST40 PCI (HCMOS7)
## Peripheral Component Interconnect control registers (all ST40 variants)
define st40_display_pci_hcmos7_regs
  ## PCI Local Registers
  __shx_display_reg $PCI_VCR_STATUS
  __shx_display_reg $PCI_VCR_VERSION
  __shx_display_reg $PCI_CR
  __shx_display_reg $PCI_LSR0
  __shx_display_reg $PCI_LAR0
  __shx_display_reg $PCI_INT
  __shx_display_reg $PCI_INTM
  __shx_display_reg $PCI_AIR
  __shx_display_reg $PCI_CIR
  __shx_display_reg $PCI_AINT
  __shx_display_reg $PCI_AINTM
  __shx_display_reg $PCI_BMIR
  __shx_display_reg $PCI_PAR
  __shx_display_reg $PCI_MBR
  __shx_display_reg $PCI_IOBR
  __shx_display_reg $PCI_PINT
  __shx_display_reg $PCI_PINTM
  __shx_display_reg $PCI_MBMR
  __shx_display_reg $PCI_IOBMR
  __shx_display_reg $PCI_PDR

  ## PCI Configuration Space Registers (CSR)
  __shx_display_reg $PCI_VID
  __shx_display_reg $PCI_DID
  __shx_display_reg $PCI_CMD
  __shx_display_reg $PCI_STATUS
  __shx_display_reg $PCI_RID_CLASS
  __shx_display_reg $PCI_CLS
  __shx_display_reg $PCI_MLT
  __shx_display_reg $PCI_HDR
  __shx_display_reg $PCI_BIST
  __shx_display_reg $PCI_MBAR0
  __shx_display_reg $PCI_IBAR
  __shx_display_reg $PCI_SVID
  __shx_display_reg $PCI_SID
  __shx_display_reg $PCI_CP
  __shx_display_reg $PCI_INTLINE
  __shx_display_reg $PCI_INTPIN
  __shx_display_reg $PCI_MINGNT
  __shx_display_reg $PCI_MAXLAT
  __shx_display_reg $PCI_TRDYTIME
  __shx_display_reg $PCI_RETRYTIME
  __shx_display_reg $PCI_CID
  __shx_display_reg $PCI_NIP
  __shx_display_reg $PCI_PMC
  __shx_display_reg $PCI_PMCSR
  __shx_display_reg $PCI_PMCSR_BSE
  __shx_display_reg $PCI_PCDD
end
##}}}

##{{{  ST40 PCI
## Peripheral Component Interconnect control registers (all ST40 variants)
define st40_display_pci_regs
  ## PCI Local Registers
  __shx_display_reg $PCI_VCR_STATUS
  __shx_display_reg $PCI_VCR_VERSION
  __shx_display_reg $PCI_CR
  __shx_display_reg $PCI_LSR0
  __shx_display_reg $PCI_LAR0
  __shx_display_reg $PCI_INT
  __shx_display_reg $PCI_INTM
  __shx_display_reg $PCI_AIR
  __shx_display_reg $PCI_CIR
  __shx_display_reg $PCI_AINT
  __shx_display_reg $PCI_AINTM
  __shx_display_reg $PCI_BMIR
  __shx_display_reg $PCI_PAR
  __shx_display_reg $PCI_MBR
  __shx_display_reg $PCI_IOBR
  __shx_display_reg $PCI_PINT
  __shx_display_reg $PCI_PINTM
  __shx_display_reg $PCI_MBMR
  __shx_display_reg $PCI_IOBMR

  ## PCI Local Configuration Registers
  __shx_display_reg $PCI_WCBAR
  __shx_display_reg $PCI_LOCCFG_UNLOCK
  __shx_display_reg $PCI_RBARR0
  __shx_display_reg $PCI_RSR0
  __shx_display_reg $PCI_RLAR0
  __shx_display_reg $PCI_RBARR1
  __shx_display_reg $PCI_RSR1
  __shx_display_reg $PCI_RLAR1
  __shx_display_reg $PCI_RBARR2
  __shx_display_reg $PCI_RSR2
  __shx_display_reg $PCI_RLAR2
  __shx_display_reg $PCI_RBARR3
  __shx_display_reg $PCI_RSR3
  __shx_display_reg $PCI_RLAR3
  __shx_display_reg $PCI_RBARR4
  __shx_display_reg $PCI_RSR4
  __shx_display_reg $PCI_RLAR4
  __shx_display_reg $PCI_RBARR5
  __shx_display_reg $PCI_RSR5
  __shx_display_reg $PCI_RLAR5
  __shx_display_reg $PCI_RBARR6
  __shx_display_reg $PCI_RSR6
  __shx_display_reg $PCI_RLAR6
  __shx_display_reg $PCI_RBARR7
  __shx_display_reg $PCI_RSR7
  __shx_display_reg $PCI_RLAR7

  ## PCI Configuration Space Registers (CSR)
  __shx_display_reg $PCI_VID
  __shx_display_reg $PCI_DID
  __shx_display_reg $PCI_CMD
  __shx_display_reg $PCI_STATUS
  __shx_display_reg $PCI_RID_CLASS
  __shx_display_reg $PCI_CLS
  __shx_display_reg $PCI_MLT
  __shx_display_reg $PCI_HDR
  __shx_display_reg $PCI_BIST
  __shx_display_reg $PCI_MBAR0
  __shx_display_reg $PCI_IBAR
  __shx_display_reg $PCI_SVID
  __shx_display_reg $PCI_SID
  __shx_display_reg $PCI_CP
  __shx_display_reg $PCI_INTLINE
  __shx_display_reg $PCI_INTPIN
  __shx_display_reg $PCI_MINGNT
  __shx_display_reg $PCI_MAXLAT
  __shx_display_reg $PCI_TRDYTIME
  __shx_display_reg $PCI_RETRYTIME
  __shx_display_reg $PCI_CID
  __shx_display_reg $PCI_NIP
  __shx_display_reg $PCI_PMC
  __shx_display_reg $PCI_PMCSR
  __shx_display_reg $PCI_PMCSR_BSE
  __shx_display_reg $PCI_PCDD
end
##}}}

##{{{  ST40 EMPI
## External MicroProcessor Interface control registers (all ST40 variants)
define st40_display_empi_regs
  __shx_display_reg $EMPI_VCR_STATUS
  __shx_display_reg $EMPI_VCR_VERSION
  __shx_display_reg $EMPI_SYSTEM
  __shx_display_reg $EMPI_ISTATUS
  __shx_display_reg $EMPI_IMASK
  __shx_display_reg $EMPI_MPXCFG
  __shx_display_reg $EMPI_DMAINV
  __shx_display_reg $EMPI_DMACFG0
  __shx_display_reg $EMPI_DMACFG1
  __shx_display_reg $EMPI_DMACFG2
  __shx_display_reg $EMPI_DMACFG3
  __shx_display_reg $EMPI_DSTATUS0
  __shx_display_reg $EMPI_DSTATUS1
  __shx_display_reg $EMPI_DSTATUS2
  __shx_display_reg $EMPI_DSTATUS3
  __shx_display_reg $EMPI_RBAR0
  __shx_display_reg $EMPI_RSR0
  __shx_display_reg $EMPI_RLAR0
  __shx_display_reg $EMPI_RBAR1
  __shx_display_reg $EMPI_RSR1
  __shx_display_reg $EMPI_RLAR1
  __shx_display_reg $EMPI_RBAR2
  __shx_display_reg $EMPI_RSR2
  __shx_display_reg $EMPI_RLAR2
  __shx_display_reg $EMPI_RBAR3
  __shx_display_reg $EMPI_RSR3
  __shx_display_reg $EMPI_RLAR3
  __shx_display_reg $EMPI_RBAR4
  __shx_display_reg $EMPI_RSR4
  __shx_display_reg $EMPI_RLAR4
  __shx_display_reg $EMPI_RBAR5
  __shx_display_reg $EMPI_RSR5
  __shx_display_reg $EMPI_RLAR5
  __shx_display_reg $EMPI_RBAR6
  __shx_display_reg $EMPI_RSR6
  __shx_display_reg $EMPI_RLAR6
  __shx_display_reg $EMPI_RBAR7
  __shx_display_reg $EMPI_RSR7
  __shx_display_reg $EMPI_RLAR7
end
##}}}

##{{{  ST40 MPXARB
## MPX Arbiter control registers (all ST40 variants)
define st40_display_mpxarb_regs
  __shx_display_reg $MPXARB_VCR
  __shx_display_reg $MPXARB_CONTROL
  __shx_display_reg $MPXARB_DLLCONTROL
  __shx_display_reg $MPXARB_DLLSTATUS
end
##}}}

##{{{  ST40 MAILBOX
## Mailbox control registers (all ST40 variants)
define st40_display_mailbox_regs
  if ($argc < 3)
    __shx_display_reg $MAILBOX_ID_VER
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG1
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG2
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG3
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG4
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG1_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG2_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG3_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG4_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG1_CLR
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG2_CLR
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG3_CLR
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_STATUS_REG4_CLR
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG1
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG2
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG3
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG4
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG1_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG2_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG3_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG4_SET
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG1_CLR
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG2_CLR
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG3_CLR
    __shx_display_reg $MAILBOX_$arg0_INTERRUPT_ENABLE_REG4_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG1
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG2
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG3
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG4
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG1_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG2_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG3_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG4_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG1_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG2_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG3_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_STATUS_REG4_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG1
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG2
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG3
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG4
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG1_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG2_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG3_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG4_SET
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG1_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG2_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG3_CLR
    __shx_display_reg $MAILBOX_$arg1_INTERRUPT_ENABLE_REG4_CLR
    __shx_display_reg $MAILBOX_LOCK0
    __shx_display_reg $MAILBOX_LOCK1
    __shx_display_reg $MAILBOX_LOCK2
    __shx_display_reg $MAILBOX_LOCK3
    __shx_display_reg $MAILBOX_LOCK4
    __shx_display_reg $MAILBOX_LOCK5
    __shx_display_reg $MAILBOX_LOCK6
    __shx_display_reg $MAILBOX_LOCK7
    __shx_display_reg $MAILBOX_LOCK8
    __shx_display_reg $MAILBOX_LOCK9
    __shx_display_reg $MAILBOX_LOCK10
    __shx_display_reg $MAILBOX_LOCK11
    __shx_display_reg $MAILBOX_LOCK12
    __shx_display_reg $MAILBOX_LOCK13
    __shx_display_reg $MAILBOX_LOCK14
    __shx_display_reg $MAILBOX_LOCK15
    __shx_display_reg $MAILBOX_LOCK16
    __shx_display_reg $MAILBOX_LOCK17
    __shx_display_reg $MAILBOX_LOCK18
    __shx_display_reg $MAILBOX_LOCK19
    __shx_display_reg $MAILBOX_LOCK20
    __shx_display_reg $MAILBOX_LOCK21
    __shx_display_reg $MAILBOX_LOCK22
    __shx_display_reg $MAILBOX_LOCK23
    __shx_display_reg $MAILBOX_LOCK24
    __shx_display_reg $MAILBOX_LOCK25
    __shx_display_reg $MAILBOX_LOCK26
    __shx_display_reg $MAILBOX_LOCK27
    __shx_display_reg $MAILBOX_LOCK28
    __shx_display_reg $MAILBOX_LOCK29
    __shx_display_reg $MAILBOX_LOCK30
    __shx_display_reg $MAILBOX_LOCK31
  else
    __shx_display_reg $MAILBOX$arg2_ID_VER
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG1
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG2
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG3
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG4
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG1_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG2_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG3_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG4_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG1_CLR
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG2_CLR
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG3_CLR
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_STATUS_REG4_CLR
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG1
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG2
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG3
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG4
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG1_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG2_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG3_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG4_SET
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG1_CLR
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG2_CLR
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG3_CLR
    __shx_display_reg $MAILBOX$arg2_$arg0_INTERRUPT_ENABLE_REG4_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG1
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG2
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG3
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG4
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG1_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG2_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG3_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG4_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG1_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG2_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG3_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_STATUS_REG4_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG1
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG2
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG3
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG4
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG1_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG2_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG3_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG4_SET
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG1_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG2_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG3_CLR
    __shx_display_reg $MAILBOX$arg2_$arg1_INTERRUPT_ENABLE_REG4_CLR
    __shx_display_reg $MAILBOX$arg2_LOCK0
    __shx_display_reg $MAILBOX$arg2_LOCK1
    __shx_display_reg $MAILBOX$arg2_LOCK2
    __shx_display_reg $MAILBOX$arg2_LOCK3
    __shx_display_reg $MAILBOX$arg2_LOCK4
    __shx_display_reg $MAILBOX$arg2_LOCK5
    __shx_display_reg $MAILBOX$arg2_LOCK6
    __shx_display_reg $MAILBOX$arg2_LOCK7
    __shx_display_reg $MAILBOX$arg2_LOCK8
    __shx_display_reg $MAILBOX$arg2_LOCK9
    __shx_display_reg $MAILBOX$arg2_LOCK10
    __shx_display_reg $MAILBOX$arg2_LOCK11
    __shx_display_reg $MAILBOX$arg2_LOCK12
    __shx_display_reg $MAILBOX$arg2_LOCK13
    __shx_display_reg $MAILBOX$arg2_LOCK14
    __shx_display_reg $MAILBOX$arg2_LOCK15
    __shx_display_reg $MAILBOX$arg2_LOCK16
    __shx_display_reg $MAILBOX$arg2_LOCK17
    __shx_display_reg $MAILBOX$arg2_LOCK18
    __shx_display_reg $MAILBOX$arg2_LOCK19
    __shx_display_reg $MAILBOX$arg2_LOCK20
    __shx_display_reg $MAILBOX$arg2_LOCK21
    __shx_display_reg $MAILBOX$arg2_LOCK22
    __shx_display_reg $MAILBOX$arg2_LOCK23
    __shx_display_reg $MAILBOX$arg2_LOCK24
    __shx_display_reg $MAILBOX$arg2_LOCK25
    __shx_display_reg $MAILBOX$arg2_LOCK26
    __shx_display_reg $MAILBOX$arg2_LOCK27
    __shx_display_reg $MAILBOX$arg2_LOCK28
    __shx_display_reg $MAILBOX$arg2_LOCK29
    __shx_display_reg $MAILBOX$arg2_LOCK30
    __shx_display_reg $MAILBOX$arg2_LOCK31
  end
end
##}}}

##{{{  ST40 SYSCONF
## System configuration registers (all ST40 variants)
define st40_display_sysconf_regs
  __shx_display_reg $SYSCONF_VCR
  __shx_display_reg $SYSCONF_SYS_CON1
  __shx_display_reg $SYSCONF_SYS_CON2
  __shx_display_reg $SYSCONF_CNV_STATUS
  __shx_display_reg $SYSCONF_CNV_SET
  __shx_display_reg $SYSCONF_CNV_CLEAR
  __shx_display_reg $SYSCONF_CNV_CONTROL
  __shx_display_reg $SYSCONF_SYS_STAT1
  __shx_display_reg $SYSCONF_SYS_STAT2
end
##}}}

##{{{  ST40 SSC
## Synchronous Serial Controller control registers (all ST40 variants)
define st40_display_ssc_regs
  __shx_display_reg $SSC$arg0_BRG
  __shx_display_reg $SSC$arg0_TBUF
  __shx_display_reg $SSC$arg0_RBUF
  __shx_display_reg $SSC$arg0_CTL
  __shx_display_reg $SSC$arg0_IEN
  __shx_display_reg $SSC$arg0_STA
  __shx_display_reg $SSC$arg0_I2C
  __shx_display_reg $SSC$arg0_SLAD
  __shx_display_reg $SSC$arg0_REP_START_HOLD
  __shx_display_reg $SSC$arg0_START_HOLD
  __shx_display_reg $SSC$arg0_REP_START_SETUP
  __shx_display_reg $SSC$arg0_DATA_SETUP
  __shx_display_reg $SSC$arg0_STOP_SETUP
  __shx_display_reg $SSC$arg0_BUS_FREE
  __shx_display_reg $SSC$arg0_CLR_STA
  __shx_display_reg $SSC$arg0_AGFR
  __shx_display_reg $SSC$arg0_PRSC
end
##}}}

##{{{  ST40 ASC
## Asynchronous Serial Controller control registers (all ST40 variants)
define st40_display_asc_regs
  __shx_display_reg $ASC$arg0_BAUDRATE
  __shx_display_reg $ASC$arg0_TXBUFFER
  __shx_display_reg $ASC$arg0_RXBUFFER
  __shx_display_reg $ASC$arg0_CONTROL
  __shx_display_reg $ASC$arg0_INTENABLE
  __shx_display_reg $ASC$arg0_STATUS
  __shx_display_reg $ASC$arg0_GUARDTIME
  __shx_display_reg $ASC$arg0_TIMEOUT
  __shx_display_reg $ASC$arg0_TXRESET
  __shx_display_reg $ASC$arg0_RXRESET
  __shx_display_reg $ASC$arg0_RETRIES
end
##}}}

##{{{  ST40 L2
## L2 cache control registers (all ST40 variants)
define st40_display_l2_regs
  __shx_display_reg $L2_VCR
  __shx_display_reg $L2_CFG
  __shx_display_reg $L2_CCR
  __shx_display_reg $L2_SYNC
  __shx_display_reg $L2_IA
  __shx_display_reg $L2_FA
  __shx_display_reg $L2_PA
  __shx_display_reg $L2_IE
  __shx_display_reg $L2_FE
  __shx_display_reg $L2_PE
  __shx_display_reg $L2_IS
  __shx_display_reg $L2_AES
  __shx_display_reg $L2_ATA
  __shx_display_reg $L2_PFR
  __shx_display_reg $L2_STS
  __shx_display_reg $L2_PMC
  __shx_display_reg $L2_ECO
  __shx_display_reg $L2_CCO
  __shx_display_reg $L2_ECI
  __shx_display_reg $L2_CCI
  __shx_display_reg $L2_ADA
  __shx_display_reg $L2_ECA
  __shx_display_reg $L2_CCA
end
##}}}

################################################################################
## ST403xx control registers
################################################################################

##{{{  ST403xx CCN
## Core control registers
define st403xx_display_ccn_regs
  sh4_display_ccn_regs

  __shx_display_reg $CCN_PASCR
  __shx_display_reg $CCN_RAMCR
  __shx_display_reg $CCN_IRMCR
end
##}}}

##{{{  ST403xx CPG
## Clock Pulse Generator control registers
define st403xx_display_cpg_regs
  __shx_display_reg $CPG_STBCR
  __shx_display_reg $CPG_WTCNT_R
  __shx_display_reg $CPG_WTCSR_R
  __shx_display_reg $CPG_STBCR2
  __shx_display_reg $CPG_WTCSR2_R
end
##}}}

################################################################################
## STm8000 control registers
################################################################################

##{{{  STm8000 CLOCKGEN
## Clock Generator control registers (all STm8000 variants)
define stm8000_display_clockgen_regs
  __shx_display_reg $CLOCKGEN$arg0_PLL1CR1
  __shx_display_reg $CLOCKGEN$arg0_PLL1CR2
  __shx_display_reg $CLOCKGEN$arg0_PLL2CR
  __shx_display_reg $CLOCKGEN$arg0_STBREQCR
  __shx_display_reg $CLOCKGEN$arg0_STBREQCR_SET
  __shx_display_reg $CLOCKGEN$arg0_STBREQCR_CLR
  __shx_display_reg $CLOCKGEN$arg0_STBACKCR
  __shx_display_reg $CLOCKGEN$arg0_CLK4CR
  __shx_display_reg $CLOCKGEN$arg0_CPG_BYPASS
  __shx_display_reg $CLOCKGEN$arg0_CLK_RATIO
  __shx_display_reg $CLOCKGEN$arg0_CLK1CR
  __shx_display_reg $CLOCKGEN$arg0_CLK2CR
  __shx_display_reg $CLOCKGEN$arg0_CLK3CR
  __shx_display_reg $CLOCKGEN$arg0_CLKDDRCR
end
##}}}

##{{{  STm8000 FS
## Frequency Synthesiser control registers (all STm8000 variants)
define stm8000_display_fs_regs
  __shx_display_reg $FS$arg0_CONFIG_GENERIC_INFO
  __shx_display_reg $FS$arg0_CONFIG_CLK_1
  __shx_display_reg $FS$arg0_CONFIG_CLK_2
  __shx_display_reg $FS$arg0_CONFIG_CLK_3
  __shx_display_reg $FS$arg0_CONFIG_CLK_4
end
##}}}

##{{{  STm8000 LX GLUE
## Lx Glue control registers (all STm8000 variants)
define stm8000_display_lx_glue_regs
  __shx_display_reg $LX_GLUE_VCR_STATUS
  __shx_display_reg $LX_GLUE_VCR_VERSION
  __shx_display_reg $LX_GLUE_CONTROL_REQ
  __shx_display_reg $LX_GLUE_CONTROL_ACK
  __shx_display_reg $LX_GLUE_IRQ_STATUS
  __shx_display_reg $LX_GLUE_IRQ_CLEAR
  __shx_display_reg $LX_GLUE_CH1_THRESHOLD
  __shx_display_reg $LX_GLUE_CH2_THRESHOLD
  __shx_display_reg $LX_GLUE_CH3_THRESHOLD
end
##}}}

##{{{  STm8000 SHE
## SHE control registers (all STm8000 variants)
define stm8000_display_she_regs
  __shx_display_reg $SHE_VCR_STATUS
  __shx_display_reg $SHE_VCR_VERSION
  __shx_display_reg $SHE_RESET_PD_REQ
  __shx_display_reg $SHE_RESET_PD_ACK
  __shx_display_reg $SHE_HOR_SIZE
  __shx_display_reg $SHE_ADDR_PREFIX
  __shx_display_reg $SHE_CURR_CHROMA
  __shx_display_reg $SHE_FWD_CHROMA
  __shx_display_reg $SHE_BKW_CHROMA
  __shx_display_reg $SHE_MAE_OFFS_FIELD
  __shx_display_reg $SHE_MAE_OFFS_INTERP
  __shx_display_reg $SHE_MAD_THRES_INTRA
  __shx_display_reg $SHE_MAD_OFFS_INTRA
  __shx_display_reg $SHE_MAD_THRES_MC
  __shx_display_reg $SHE_MAD_OFFS_MC
  __shx_display_reg $SHE_CURR_COARSE
  __shx_display_reg $SHE_FWD_COARSE
  __shx_display_reg $SHE_CURR_FINE
  __shx_display_reg $SHE_FWD_FINE
  __shx_display_reg $SHE_BKW_FINE
  __shx_display_reg $SHE_CAP_COARSE
  __shx_display_reg $SHE_CAP_FINE
  __shx_display_reg $SHE_T1_DMA_CONTROL
  __shx_display_reg $SHE_T1_DMA_BASE
  __shx_display_reg $SHE_T1_DMA_CURRENT
  __shx_display_reg $SHE_HOR_SIZE
  __shx_display_reg $SHE_ADDR_PREFIX
end
##}}}

################################################################################
## STi5528 control registers
################################################################################

##{{{  STi5528 CLOCKGEN
## Clock Generator control registers (STi5528 variant)
define sti5528_display_clockgen_regs
  __shx_display_reg $CLOCKGEN_MD_STATUS
  __shx_display_reg $CLOCKGEN_PLL1_CTRL
  __shx_display_reg $CLOCKGEN_PLL1_CTRL2
  __shx_display_reg $CLOCKGEN_PLL1_CLK1_CTRL
  __shx_display_reg $CLOCKGEN_PLL1_CLK2_CTRL
  __shx_display_reg $CLOCKGEN_PLL1_CLK3_CTRL
  __shx_display_reg $CLOCKGEN_PLL1_CLK4_CTRL
  __shx_display_reg $CLOCKGEN_PLL1_CLK5_CTRL
  __shx_display_reg $CLOCKGEN_PLL1_CLK6_CTRL
  __shx_display_reg $CLOCKGEN_CPG_BYPASS
  __shx_display_reg $CLOCKGEN_QSYNTH1_CTRL
  __shx_display_reg $CLOCKGEN_QSYNTH1_CLK1_CTRL
  __shx_display_reg $CLOCKGEN_QSYNTH1_CLK2_CTRL
  __shx_display_reg $CLOCKGEN_IRDA_CTRL
  __shx_display_reg $CLOCKGEN_PLL2_CTRL
  __shx_display_reg $CLOCKGEN_QSYNTH2_CTRL
  __shx_display_reg $CLOCKGEN_QSYNTH2_CLK1_CTRL
  __shx_display_reg $CLOCKGEN_QSYNTH2_CLK2_CTRL
  __shx_display_reg $CLOCKGEN_QSYNTH2_CLK3_CTRL
  __shx_display_reg $CLOCKGEN_QSYNTH2_CLK4_CTRL
  __shx_display_reg $CLOCKGEN_ST20_CLK_CTRL
  __shx_display_reg $CLOCKGEN_ST20_TICK_CTRL
  __shx_display_reg $CLOCKGEN_AUDBIT_CLK_CTRL
  __shx_display_reg $CLOCKGEN_LP_CLK_CTRL
  __shx_display_reg $CLOCKGEN_AUD_CLK_REF_CTRL
  __shx_display_reg $CLOCKGEN_PIX_CLK_CTRL
  __shx_display_reg $CLOCKGEN_DVP_CLK_CTRL
  __shx_display_reg $CLOCKGEN_PWM_CLK_CTRL
  __shx_display_reg $CLOCKGEN_CKG_LOCK
  __shx_display_reg $CLOCKGEN_CKOUT_CTRL
  __shx_display_reg $CLOCKGEN_STB_REQ
  __shx_display_reg $CLOCKGEN_STB_ACK
end
##}}}

##{{{  STi5528 SYSCONF
## System configuration registers (STi5528 variant)
define sti5528_display_sysconf_regs
  __shx_display_reg $SYSCONF_DEVICEID
  __shx_display_reg $SYSCONF_SYS_STA00
  __shx_display_reg $SYSCONF_SYS_STA01
  __shx_display_reg $SYSCONF_SYS_CFG00
  __shx_display_reg $SYSCONF_SYS_CFG01
  __shx_display_reg $SYSCONF_SYS_CFG02
  __shx_display_reg $SYSCONF_SYS_CFG03
  __shx_display_reg $SYSCONF_SYS_CFG04
  __shx_display_reg $SYSCONF_SYS_CFG05
  __shx_display_reg $SYSCONF_SYS_CFG06
  __shx_display_reg $SYSCONF_SYS_CFG07
  __shx_display_reg $SYSCONF_SYS_CFG08
  __shx_display_reg $SYSCONF_SYS_CFG09
  __shx_display_reg $SYSCONF_SYS_CFG10
  __shx_display_reg $SYSCONF_SYS_CFG11
  __shx_display_reg $SYSCONF_SYS_CFG12
  __shx_display_reg $SYSCONF_SYS_CFG13
end
##}}}

##{{{  STi5528 STBUS
## STBUS control registers (STi5528 variant)
define sti5528_display_stbus_regs
 __shx_display_reg $STBUS_GDP0_DISP_PRIORITY
 __shx_display_reg $STBUS_GDP0_LL_PRIORITY
 __shx_display_reg $STBUS_GDP1_DISP_PRIORITY
 __shx_display_reg $STBUS_GDP1_LL_PRIORITY
 __shx_display_reg $STBUS_GDP2_DISP_PRIORITY
 __shx_display_reg $STBUS_GDP2_LL_PRIORITY
 __shx_display_reg $STBUS_GDP3_DISP_PRIORITY
 __shx_display_reg $STBUS_GDP3_LL_PRIORITY
 __shx_display_reg $STBUS_N04_TARGET1_EMI_PRIORITY
 __shx_display_reg $STBUS_N04_TARGET2_LMI_PRIORITY
 __shx_display_reg $STBUS_DISP0_IC_LUMA_PRIORITY
 __shx_display_reg $STBUS_DISP0_IC_CHROMA_PRIORITY
 __shx_display_reg $STBUS_DISP1_IC_LUMA_PRIORITY
 __shx_display_reg $STBUS_DISP1_IC_CHROMA_PRIORITY
 __shx_display_reg $STBUS_DVP_IC_PIX_PRIORITY
 __shx_display_reg $STBUS_DVP_IC_ANC_PRIORITY
 __shx_display_reg $STBUS_N05_TARGET1_EMI_PRIORITY
 __shx_display_reg $STBUS_N05_TARGET2_LMI_PRIORITY
 __shx_display_reg $STBUS_ALP_DISP_PRIORITY
 __shx_display_reg $STBUS_ALP_LL_PRIORITY
 __shx_display_reg $STBUS_CUR_PRIORITY
 __shx_display_reg $STBUS_AUD_IC_RMEM_PRIORITY
 __shx_display_reg $STBUS_AUD_IC_PMEM_PRIORITY
 __shx_display_reg $STBUS_VID_IC_CD_PRIORITY
 __shx_display_reg $STBUS_VID_IC_VD_PRIORITY
 __shx_display_reg $STBUS_N04_EMI_PRIORITY
 __shx_display_reg $STBUS_N04_LMI_PRIORITY
 __shx_display_reg $STBUS_N05_EMI_PRIORITY
 __shx_display_reg $STBUS_N05_LMI_PRIORITY
 __shx_display_reg $STBUS_N06_PRIORITY
 __shx_display_reg $STBUS_N07_PRIORITY
 __shx_display_reg $STBUS_BLT_IC_PRIORITY
 __shx_display_reg $STBUS_N08_TARGET1_EMI_PRIORITY
 __shx_display_reg $STBUS_N08_TARGET2_LMI_PRIORITY
 __shx_display_reg $STBUS_CPUPLUG_IC_PRIORITY
 __shx_display_reg $STBUS_ST20_IC_ID_PRIORITY
 __shx_display_reg $STBUS_N02_PRIORITY
 __shx_display_reg $STBUS_N03_PRIORITY
 __shx_display_reg $STBUS_N08_EMI_PRIORITY
 __shx_display_reg $STBUS_N08_LMI_PRIORITY
 __shx_display_reg $STBUS_N09_PRIORITY
 __shx_display_reg $STBUS_CPUPLUG_IC_LATENCY
 __shx_display_reg $STBUS_ST20_IC_ID_LATENCY
 __shx_display_reg $STBUS_N02_LATENCY
 __shx_display_reg $STBUS_N03_LATENCY
 __shx_display_reg $STBUS_N08_EMI_LATENCY
 __shx_display_reg $STBUS_N08_LMI_LATENCY
 __shx_display_reg $STBUS_N09_LATENCY
 __shx_display_reg $STBUS_N10_TARGET_1_N11_PRIORITY
 __shx_display_reg $STBUS_N10_TARGET_2_EMI_PRIORITY
 __shx_display_reg $STBUS_N10_TARGET_3_LMI_PRIORITY
 __shx_display_reg $STBUS_N10_TARGET_4_N12_PRIORITY
end
##}}}

################################################################################
## STb710x control registers
################################################################################

##{{{  STb710x CLOCKGENA
## Clock Generator A control registers (STb710x variant)
define stb710x_display_clockgena_regs
  __shx_display_reg $CLOCKGENA_LOCK
  __shx_display_reg $CLOCKGENA_MD_STATUS
  __shx_display_reg $CLOCKGENA_PLL0_CFG
  __shx_display_reg $CLOCKGENA_PLL0_STATUS
  __shx_display_reg $CLOCKGENA_PLL0_CLK1_CTRL
  __shx_display_reg $CLOCKGENA_PLL0_CLK2_CTRL
  __shx_display_reg $CLOCKGENA_PLL0_CLK3_CTRL
  __shx_display_reg $CLOCKGENA_PLL0_CLK4_CTRL
  __shx_display_reg $CLOCKGENA_PLL1_CFG
  __shx_display_reg $CLOCKGENA_PLL1_STATUS
  __shx_display_reg $CLOCKGENA_CLK_DIV
  __shx_display_reg $CLOCKGENA_CLOCK_ENABLE
  __shx_display_reg $CLOCKGENA_OUT_CTRL
  __shx_display_reg $CLOCKGENA_PLL1_BYPASS
end
##}}}

##{{{  STb710x SYSCONF
## System configuration registers (STb710x variant)
define stb710x_display_sysconf_regs
  __shx_display_reg $SYSCONF_DEVICEID
  __shx_display_reg $SYSCONF_SYS_STA00
  __shx_display_reg $SYSCONF_SYS_STA01
  __shx_display_reg $SYSCONF_SYS_STA02
  __shx_display_reg $SYSCONF_SYS_STA03
  __shx_display_reg $SYSCONF_SYS_STA04
  __shx_display_reg $SYSCONF_SYS_STA05
  __shx_display_reg $SYSCONF_SYS_STA06
  __shx_display_reg $SYSCONF_SYS_STA07
  __shx_display_reg $SYSCONF_SYS_STA08
  __shx_display_reg $SYSCONF_SYS_STA09
  __shx_display_reg $SYSCONF_SYS_STA10
  __shx_display_reg $SYSCONF_SYS_STA11
  __shx_display_reg $SYSCONF_SYS_STA12
  __shx_display_reg $SYSCONF_SYS_STA13
  __shx_display_reg $SYSCONF_SYS_STA14
  __shx_display_reg $SYSCONF_SYS_STA15
  __shx_display_reg $SYSCONF_SYS_CFG00
  __shx_display_reg $SYSCONF_SYS_CFG01
  __shx_display_reg $SYSCONF_SYS_CFG02
  __shx_display_reg $SYSCONF_SYS_CFG03
  __shx_display_reg $SYSCONF_SYS_CFG04
  __shx_display_reg $SYSCONF_SYS_CFG05
  __shx_display_reg $SYSCONF_SYS_CFG06
  __shx_display_reg $SYSCONF_SYS_CFG07
  __shx_display_reg $SYSCONF_SYS_CFG08
  __shx_display_reg $SYSCONF_SYS_CFG09
  __shx_display_reg $SYSCONF_SYS_CFG10
  __shx_display_reg $SYSCONF_SYS_CFG11
  __shx_display_reg $SYSCONF_SYS_CFG12
  __shx_display_reg $SYSCONF_SYS_CFG13
  __shx_display_reg $SYSCONF_SYS_CFG14
  __shx_display_reg $SYSCONF_SYS_CFG15
  __shx_display_reg $SYSCONF_SYS_CFG16
  __shx_display_reg $SYSCONF_SYS_CFG17
  __shx_display_reg $SYSCONF_SYS_CFG18
  __shx_display_reg $SYSCONF_SYS_CFG19
  __shx_display_reg $SYSCONF_SYS_CFG20
  __shx_display_reg $SYSCONF_SYS_CFG21
  __shx_display_reg $SYSCONF_SYS_CFG22
  __shx_display_reg $SYSCONF_SYS_CFG23
  __shx_display_reg $SYSCONF_SYS_CFG24
  __shx_display_reg $SYSCONF_SYS_CFG25
  __shx_display_reg $SYSCONF_SYS_CFG26
  __shx_display_reg $SYSCONF_SYS_CFG27
  __shx_display_reg $SYSCONF_SYS_CFG28
  __shx_display_reg $SYSCONF_SYS_CFG29
  __shx_display_reg $SYSCONF_SYS_CFG30
  __shx_display_reg $SYSCONF_SYS_CFG31
  __shx_display_reg $SYSCONF_SYS_CFG32
  __shx_display_reg $SYSCONF_SYS_CFG33
  __shx_display_reg $SYSCONF_SYS_CFG34
  __shx_display_reg $SYSCONF_SYS_CFG35
  __shx_display_reg $SYSCONF_SYS_CFG36
  __shx_display_reg $SYSCONF_SYS_CFG37
  __shx_display_reg $SYSCONF_SYS_CFG38
  __shx_display_reg $SYSCONF_SYS_CFG39
end
##}}}

##{{{  STb710x STBUS
## STBUS control registers (STb710x variant)
define stb710x_display_stbus_regs
  __shx_display_reg $STBUS_NODE01_SH4_I_PRIORITY
  __shx_display_reg $STBUS_NODE01_LX_DH_I_PRIORITY
  __shx_display_reg $STBUS_NODE01_LX_AUD_I_PRIORITY
  __shx_display_reg $STBUS_NODE01_SH4_I_LIMIT
  __shx_display_reg $STBUS_NODE01_LX_DH_I_LIMIT
  __shx_display_reg $STBUS_NODE01_LX_AUD_I_LIMIT
  __shx_display_reg $STBUS_NODE01_N01_TARG_1_LMIVID_PRIORITY
  __shx_display_reg $STBUS_NODE01_N01_TARG_2_LMISYS_PRIORITY
  __shx_display_reg $STBUS_NODE01_N01_TARG_3_N04_PRIORITY

  __shx_display_reg $STBUS_NODE04_N01_PRIORITY
  __shx_display_reg $STBUS_NODE04_N02_PRIORITY
  __shx_display_reg $STBUS_NODE04_DH_I_0_PRIORITY
  __shx_display_reg $STBUS_NODE04_DH_I_1_PRIORITY
  __shx_display_reg $STBUS_NODE04_N03_PRIORITY
  __shx_display_reg $STBUS_NODE04_N04_TARG_1_LMIVID_PRIORITY
  __shx_display_reg $STBUS_NODE04_N04_TARG_2_N11_PRIORITY

  __shx_display_reg $STBUS_NODE05_COMPO_I_GDP1LL_PRIORITY
  __shx_display_reg $STBUS_NODE05_COMPO_I_GDP1DISP_PRIORITY
  __shx_display_reg $STBUS_NODE05_COMPO_I_GDP2LL_PRIORITY
  __shx_display_reg $STBUS_NODE05_COMPO_I_GDP2DISP_PRIORITY
  __shx_display_reg $STBUS_NODE05_COMPO_I_ALPLL_PRIORITY
  __shx_display_reg $STBUS_NODE05_COMPO_I_ALPDISP_PRIORITY
  __shx_display_reg $STBUS_NODE05_COMPO_I_CUR_PRIORITY

  __shx_display_reg $STBUS_NODE09_N08_PRIORITY
  __shx_display_reg $STBUS_NODE09_DIRT_I_PRIORITY
  __shx_display_reg $STBUS_NODE09_COMMS_I_PRIORITY
  __shx_display_reg $STBUS_NODE09_SATA_I_PRIORITY
  __shx_display_reg $STBUS_NODE09_USB_I_PRIORITY

  __shx_display_reg $STBUS_NODE11_N05_PRIORITY
  __shx_display_reg $STBUS_NODE11_N06_PRIORITY
  __shx_display_reg $STBUS_NODE11_N07_PRIORITY
  __shx_display_reg $STBUS_NODE11_N09_PRIORITY
  __shx_display_reg $STBUS_NODE11_N04_PRIORITY
  __shx_display_reg $STBUS_NODE11_N11_TARG_1_N13_PRIORITY
  __shx_display_reg $STBUS_NODE11_N11_TARG_2_N12_PRIORITY
  __shx_display_reg $STBUS_NODE11_N11_TARG_3_AUDIO_PRIORITY
  __shx_display_reg $STBUS_NODE11_N11_TARG_4_CFGREG_PRIORITY
end
##}}}

################################################################################
## STi7200 control registers
################################################################################

##{{{  STi7200 CLOCKGENA
## Clock Generator A control registers (STi7200 variant)
define sti7200_display_clockgena_regs
  __shx_display_reg $CLOCKGENA_PLL0_CFG
  __shx_display_reg $CLOCKGENA_PLL1_CFG
  __shx_display_reg $CLOCKGENA_PLL2_CFG
  __shx_display_reg $CLOCKGENA_MUX_CFG
  __shx_display_reg $CLOCKGENA_DIV_CFG
  __shx_display_reg $CLOCKGENA_DIV2_CFG
  __shx_display_reg $CLOCKGENA_CLKOBS_MUX_CFG
  __shx_display_reg $CLOCKGENA_POWER_CFG
  __shx_display_reg $CLOCKGENA_CLKOBS_MAX
  __shx_display_reg $CLOCKGENA_CLKOBS_RESULT
  __shx_display_reg $CLOCKGENA_CLKOBS_CTRL
end
##}}}

##{{{  STi7200 CLOCKGENB
## Clock Generator B control registers (STi7200 variant)
define sti7200_display_clockgenb_regs
  __shx_display_reg $CLOCKGENB_FS0_SETUP
  __shx_display_reg $CLOCKGENB_FS1_SETUP
  __shx_display_reg $CLOCKGENB_FS2_SETUP
  __shx_display_reg $CLOCKGENB_FS0_CLK1_CFG
  __shx_display_reg $CLOCKGENB_FS0_CLK2_CFG
  __shx_display_reg $CLOCKGENB_FS0_CLK3_CFG
  __shx_display_reg $CLOCKGENB_FS0_CLK4_CFG
  __shx_display_reg $CLOCKGENB_FS1_CLK1_CFG
  __shx_display_reg $CLOCKGENB_FS1_CLK2_CFG
  __shx_display_reg $CLOCKGENB_FS1_CLK3_CFG
  __shx_display_reg $CLOCKGENB_FS1_CLK4_CFG
  __shx_display_reg $CLOCKGENB_FS2_CLK1_CFG
  __shx_display_reg $CLOCKGENB_FS2_CLK2_CFG
  __shx_display_reg $CLOCKGENB_FS2_CLK3_CFG
  __shx_display_reg $CLOCKGENB_FS2_CLK4_CFG
  __shx_display_reg $CLOCKGENB_PLL0_CFG
  __shx_display_reg $CLOCKGENB_CLKRCV_CFG
  __shx_display_reg $CLOCKGENB_IN_MUX_CFG
  __shx_display_reg $CLOCKGENB_OUT_MUX_CFG
  __shx_display_reg $CLOCKGENB_DIV_CFG
  __shx_display_reg $CLOCKGENB_DIV2_CFG
  __shx_display_reg $CLOCKGENB_CLKOBS_MUX_CFG
  __shx_display_reg $CLOCKGENB_POWER_CFG
  __shx_display_reg $CLOCKGENB_CLKOBS_MAX
  __shx_display_reg $CLOCKGENB_CLKOBS_RESULT
  __shx_display_reg $CLOCKGENB_CLKOBS_CTRL
end
##}}}

##{{{  STi7200 SYSCONF
## System configuration registers (STi7200 variant)
define sti7200_display_sysconf_regs
  __shx_display_reg $SYSCONF_DEVICEID
  __shx_display_reg $SYSCONF_SYS_STA00
  __shx_display_reg $SYSCONF_SYS_STA01
  __shx_display_reg $SYSCONF_SYS_STA02
  __shx_display_reg $SYSCONF_SYS_STA03
  __shx_display_reg $SYSCONF_SYS_STA04
  __shx_display_reg $SYSCONF_SYS_STA05
  __shx_display_reg $SYSCONF_SYS_STA06
  __shx_display_reg $SYSCONF_SYS_STA07
  __shx_display_reg $SYSCONF_SYS_STA08
  __shx_display_reg $SYSCONF_SYS_STA09
  __shx_display_reg $SYSCONF_SYS_STA10
  __shx_display_reg $SYSCONF_SYS_STA11
  __shx_display_reg $SYSCONF_SYS_STA12
  __shx_display_reg $SYSCONF_SYS_STA13
  __shx_display_reg $SYSCONF_SYS_STA14
  __shx_display_reg $SYSCONF_SYS_STA15
  __shx_display_reg $SYSCONF_SYS_STA16
  __shx_display_reg $SYSCONF_SYS_CFG00
  __shx_display_reg $SYSCONF_SYS_CFG01
  __shx_display_reg $SYSCONF_SYS_CFG02
  __shx_display_reg $SYSCONF_SYS_CFG03
  __shx_display_reg $SYSCONF_SYS_CFG04
  __shx_display_reg $SYSCONF_SYS_CFG05
  __shx_display_reg $SYSCONF_SYS_CFG06
  __shx_display_reg $SYSCONF_SYS_CFG07
  __shx_display_reg $SYSCONF_SYS_CFG08
  __shx_display_reg $SYSCONF_SYS_CFG09
  __shx_display_reg $SYSCONF_SYS_CFG10
  __shx_display_reg $SYSCONF_SYS_CFG11
  __shx_display_reg $SYSCONF_SYS_CFG12
  __shx_display_reg $SYSCONF_SYS_CFG13
  __shx_display_reg $SYSCONF_SYS_CFG14
  __shx_display_reg $SYSCONF_SYS_CFG15
  __shx_display_reg $SYSCONF_SYS_CFG16
  __shx_display_reg $SYSCONF_SYS_CFG17
  __shx_display_reg $SYSCONF_SYS_CFG18
  __shx_display_reg $SYSCONF_SYS_CFG19
  __shx_display_reg $SYSCONF_SYS_CFG20
  __shx_display_reg $SYSCONF_SYS_CFG21
  __shx_display_reg $SYSCONF_SYS_CFG22
  __shx_display_reg $SYSCONF_SYS_CFG23
  __shx_display_reg $SYSCONF_SYS_CFG24
  __shx_display_reg $SYSCONF_SYS_CFG25
  __shx_display_reg $SYSCONF_SYS_CFG26
  __shx_display_reg $SYSCONF_SYS_CFG27
  __shx_display_reg $SYSCONF_SYS_CFG28
  __shx_display_reg $SYSCONF_SYS_CFG29
  __shx_display_reg $SYSCONF_SYS_CFG30
  __shx_display_reg $SYSCONF_SYS_CFG31
  __shx_display_reg $SYSCONF_SYS_CFG32
  __shx_display_reg $SYSCONF_SYS_CFG33
  __shx_display_reg $SYSCONF_SYS_CFG34
  __shx_display_reg $SYSCONF_SYS_CFG35
  __shx_display_reg $SYSCONF_SYS_CFG36
  __shx_display_reg $SYSCONF_SYS_CFG37
  __shx_display_reg $SYSCONF_SYS_CFG38
  __shx_display_reg $SYSCONF_SYS_CFG39
  __shx_display_reg $SYSCONF_SYS_CFG40
  __shx_display_reg $SYSCONF_SYS_CFG41
  __shx_display_reg $SYSCONF_SYS_CFG42
  __shx_display_reg $SYSCONF_SYS_CFG43
  __shx_display_reg $SYSCONF_SYS_CFG44
  __shx_display_reg $SYSCONF_SYS_CFG45
  __shx_display_reg $SYSCONF_SYS_CFG46
  __shx_display_reg $SYSCONF_SYS_CFG47
  __shx_display_reg $SYSCONF_SYS_CFG48
  __shx_display_reg $SYSCONF_SYS_CFG49
  __shx_display_reg $SYSCONF_SYS_CFG50
  __shx_display_reg $SYSCONF_SYS_CFG51
  __shx_display_reg $SYSCONF_SYS_CFG52
  __shx_display_reg $SYSCONF_SYS_CFG53
  __shx_display_reg $SYSCONF_SYS_CFG54
  __shx_display_reg $SYSCONF_SYS_CFG55
  __shx_display_reg $SYSCONF_SYS_CFG56
  __shx_display_reg $SYSCONF_SYS_CFG57
  __shx_display_reg $SYSCONF_SYS_CFG58
  __shx_display_reg $SYSCONF_SYS_CFG59
  __shx_display_reg $SYSCONF_SYS_CFG60
  __shx_display_reg $SYSCONF_SYS_CFG61
end
##}}}

################################################################################
## STd1000 control registers
################################################################################

##{{{  STd1000 MIXER
## Mixer control registers (STd1000 variant)
define std1000_display_mixer_regs
  __shx_display_reg $MIXER_GENC
  __shx_display_reg $MIXER_INTMASK
  __shx_display_reg $MIXER_INT
  __shx_display_reg $MIXER_BANKC
  __shx_display_reg $MIXER_ROWADDRMSK
  __shx_display_reg $MIXER_LMIBADDR
  __shx_display_reg $MIXER_DDRP1
  __shx_display_reg $MIXER_BDLCK
  __shx_display_reg $MIXER_BDLAT
  __shx_display_reg $MIXER_BDCRIS
  __shx_display_reg $MIXER_BDHPP
  __shx_display_reg $MIXER_BDQTHD
  __shx_display_reg $MIXER_BDLDINEF
  __shx_display_reg $MIXER_BDSTINEF
  __shx_display_reg $MIXER_INFLWREG
  __shx_display_reg $MIXER_ARBFLWREG
  __shx_display_reg $MIXER_BKFLWREG
  __shx_display_reg $MIXER_HPPFLWREG
  __shx_display_reg $MIXER_REQMEM
  __shx_display_reg $MIXER_RESMEM

  __shx_display_reg $MIXER_DBGROPC
  __shx_display_reg $MIXER_DBGLPADDR
  __shx_display_reg $MIXER_DBGHPADDR
  __shx_display_reg $MIXER_DBGAERR
  __shx_display_reg $MIXER_SPSRCREG_0
  __shx_display_reg $MIXER_SPSRCREG_1
  __shx_display_reg $MIXER_SPSRCREG_2
  __shx_display_reg $MIXER_SPSRCREG_3
  __shx_display_reg $MIXER_SPSRCREG_4
  __shx_display_reg $MIXER_SPSRCREG_5
  __shx_display_reg $MIXER_SPSRCREG_6
  __shx_display_reg $MIXER_SPSRCREG_7
  __shx_display_reg $MIXER_SPSRCREG_8
  __shx_display_reg $MIXER_SPSRCREG_9
  __shx_display_reg $MIXER_SPSRCREG_10
  __shx_display_reg $MIXER_SPSRCREG_11
  __shx_display_reg $MIXER_SPSRCREG_12
  __shx_display_reg $MIXER_SPSRCREG_13
  __shx_display_reg $MIXER_SPSRCREG_14
  __shx_display_reg $MIXER_SPSRCREG_15
  __shx_display_reg $MIXER_SPSRCREG_16
  __shx_display_reg $MIXER_SPSRCREG_17
  __shx_display_reg $MIXER_SPSRCREG_18
  __shx_display_reg $MIXER_SPSRCREG_19
  __shx_display_reg $MIXER_SPSRCREG_20
  __shx_display_reg $MIXER_SPSRCREG_21
  __shx_display_reg $MIXER_SPSRCREG_22
  __shx_display_reg $MIXER_SPSRCREG_23
  __shx_display_reg $MIXER_SPSRCREG_24
  __shx_display_reg $MIXER_SPSRCREG_25
  __shx_display_reg $MIXER_SPSRCREG_26
  __shx_display_reg $MIXER_SPSRCREG_27
  __shx_display_reg $MIXER_SPSRCREG_28
  __shx_display_reg $MIXER_SPSRCREG_29
  __shx_display_reg $MIXER_SPSRCREG_30
  __shx_display_reg $MIXER_SPSRCREG_31
  __shx_display_reg $MIXER_SPSRCREG_32
  __shx_display_reg $MIXER_SPSRCREG_33
  __shx_display_reg $MIXER_SPSRCREG_34
  __shx_display_reg $MIXER_SPSRCREG_35
  __shx_display_reg $MIXER_SPSRCREG_36
  __shx_display_reg $MIXER_SPSRCREG_37
  __shx_display_reg $MIXER_SPSRCREG_38
  __shx_display_reg $MIXER_SPSRCREG_39
  __shx_display_reg $MIXER_SPSRCREG_40
  __shx_display_reg $MIXER_SPSRCREG_41
  __shx_display_reg $MIXER_SPSRCREG_42
  __shx_display_reg $MIXER_SPSRCREG_43
  __shx_display_reg $MIXER_SPSRCREG_44
  __shx_display_reg $MIXER_SPSRCREG_45
  __shx_display_reg $MIXER_SPSRCREG_46
  __shx_display_reg $MIXER_SPSRCREG_47
  __shx_display_reg $MIXER_SPSRCREG_48
  __shx_display_reg $MIXER_SPSRCREG_49
  __shx_display_reg $MIXER_SPSRCREG_50
  __shx_display_reg $MIXER_SPSRCREG_51
  __shx_display_reg $MIXER_SPSRCREG_52
  __shx_display_reg $MIXER_SPSRCREG_53
  __shx_display_reg $MIXER_SPSRCREG_54
  __shx_display_reg $MIXER_SPSRCREG_55
  __shx_display_reg $MIXER_SPSRCREG_56
  __shx_display_reg $MIXER_SPSRCREG_57
  __shx_display_reg $MIXER_SPSRCREG_58
  __shx_display_reg $MIXER_SPSRCREG_59
  __shx_display_reg $MIXER_SPSRCREG_60
  __shx_display_reg $MIXER_SPSRCREG_61
  __shx_display_reg $MIXER_SPSRCREG_62
  __shx_display_reg $MIXER_SPSRCREG_63
end
##}}}

##{{{  STd1000 CLOCKGEN
## Clock Generator control registers (STd1000 variant)
define std1000_display_clockgen_regs
  __shx_display_reg $CLOCKGEN_CTL_SEL
  __shx_display_reg $CLOCKGEN_CTL_CKG_ANA
  __shx_display_reg $CLOCKGEN_CTL_EN
  __shx_display_reg $CLOCKGEN_CTL_RATIO
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_1
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_2
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_3
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_4
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_1
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_2
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_3
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_4
  __shx_display_reg $CLOCKGEN_CTL_PLL1_FREQ
  __shx_display_reg $CLOCKGEN_CTL_PLL1_SSC
  __shx_display_reg $CLOCKGEN_CTL_PLL2_FREQ
  __shx_display_reg $CLOCKGEN_CTL_PLL2_SSC
  __shx_display_reg $CLOCKGEN_CTL_PLL3
  __shx_display_reg $CLOCKGEN_CTL_PLL_USBPHY
  __shx_display_reg $CLOCKGEN_STATUS_PLL
  __shx_display_reg $CLOCKGEN_CTL_FREQ_MEAS_NORTH
  __shx_display_reg $CLOCKGEN_RES_FREQ_MEAS_NORTH
  __shx_display_reg $CLOCKGEN_CTL_FREQ_MEAS_SOUTH
  __shx_display_reg $CLOCKGEN_RES_FREQ_MEAS_SOUTH
  __shx_display_reg $CLOCKGEN_RES_JITTER_MEAS_NORTH
  __shx_display_reg $CLOCKGEN_RES_JITTER_MEAS_SOUTH
end
##}}}

##{{{  STd1000 GENCONF
## General configuration registers (STd1000 variant)
define std1000_display_genconf_regs
  __shx_display_reg $GENCONF_PIO_CONF
  __shx_display_reg $GENCONF_SYS_CONF
  __shx_display_reg $GENCONF_STBY_CMD
  __shx_display_reg $GENCONF_STBY_STATUS
  __shx_display_reg $GENCONF_LMI_DLL1_CTL
  __shx_display_reg $GENCONF_LMI_PDL4_PDL5_CTL
  __shx_display_reg $GENCONF_LMI_DLL2_CTL
  __shx_display_reg $GENCONF_LMI_PDL_0_3_MSB_CTL
  __shx_display_reg $GENCONF_LMI_PDL_0_3_LSB_CTL
  __shx_display_reg $GENCONF_LMI_STATUS
  __shx_display_reg $GENCONF_LMI_PL_CTL
  __shx_display_reg $GENCONF_IO_DDR2_CONF
  __shx_display_reg $GENCONF_COMP_EMI_CONF_3V3
  __shx_display_reg $GENCONF_COMP_FPD_CONF_3V3
  __shx_display_reg $GENCONF_COMP_PIO1_CONF_3V3
  __shx_display_reg $GENCONF_COMP_PIO2_CONF_3V3
  __shx_display_reg $GENCONF_COMP_DDR2_CONF_1V8
  __shx_display_reg $GENCONF_COMP_EMI_STATUS
  __shx_display_reg $GENCONF_COMP_FPD_STATUS
  __shx_display_reg $GENCONF_COMP_PIO1_STATUS
  __shx_display_reg $GENCONF_COMP_PIO2_STATUS
  __shx_display_reg $GENCONF_COMP_DDR2_STATUS
  __shx_display_reg $GENCONF_ANTIFUSE_ENGI_STATUS
  __shx_display_reg $GENCONF_ANTIFUSE_CUSTOM_STATUS
  __shx_display_reg $GENCONF_GENERAL_PURPOSE
end
##}}}

################################################################################
## STd2000 control registers
################################################################################

##{{{  STd2000 MIXER
## Mixer control registers (STd2000 variant)
define std2000_display_mixer_regs
  __shx_display_reg $MIXER$arg0_MODC
  __shx_display_reg $MIXER$arg0_ATC
  __shx_display_reg $MIXER$arg0_LMIAC
  __shx_display_reg $MIXER$arg0_FILC
  __shx_display_reg $MIXER$arg0_BTAC
  __shx_display_reg $MIXER$arg0_HPC
  __shx_display_reg $MIXER$arg0_RDBSC
  __shx_display_reg $MIXER$arg0_WDBSC
  __shx_display_reg $MIXER$arg0_PMC
  __shx_display_reg $MIXER$arg0_OBCLC
  __shx_display_reg $MIXER$arg0_BKCLC
  __shx_display_reg $MIXER$arg0_LPBWC
  __shx_display_reg $MIXER$arg0_STAC
  __shx_display_reg $MIXER$arg0_FILS
  __shx_display_reg $MIXER$arg0_BWLDSTS
  __shx_display_reg $MIXER$arg0_BWARBS
  __shx_display_reg $MIXER$arg0_LLDSTS
  __shx_display_reg $MIXER$arg0_LARBS
  __shx_display_reg $MIXER$arg0_LPOBS
  __shx_display_reg $MIXER$arg0_LPPNBS
  __shx_display_reg $MIXER$arg0_BK01S
  __shx_display_reg $MIXER$arg0_BK23S
  __shx_display_reg $MIXER$arg0_HBTAS
  __shx_display_reg $MIXER$arg0_PBMS
  __shx_display_reg $MIXER$arg0_ISRCFC
  __shx_display_reg $MIXER$arg0_ISRCFS
  __shx_display_reg $MIXER$arg0_GENPC0
  __shx_display_reg $MIXER$arg0_GENPS0
  __shx_display_reg $MIXER$arg0_GENPC1
  __shx_display_reg $MIXER$arg0_GENPS1
  __shx_display_reg $MIXER$arg0_DTBSAC
  __shx_display_reg $MIXER$arg0_DTBEAC
  __shx_display_reg $MIXER$arg0_DTAC
  __shx_display_reg $MIXER$arg0_DTAMC
  __shx_display_reg $MIXER$arg0_DTSC
  __shx_display_reg $MIXER$arg0_DFSC0
  __shx_display_reg $MIXER$arg0_DFSC1
  __shx_display_reg $MIXER$arg0_DS
end
##}}}

##{{{  STd2000 CLOCKGEN
## Clock Generator control registers (STd2000 variant)
define std2000_display_clockgen_regs
  __shx_display_reg $CLOCKGEN_CTL_SEL
  __shx_display_reg $CLOCKGEN_CTL_EN
  __shx_display_reg $CLOCKGEN_CTL_RATIO
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_1
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_2
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_3
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_1_4
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_1
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_2
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_3
  __shx_display_reg $CLOCKGEN_CTL_SYNTH4X_2_4
  __shx_display_reg $CLOCKGEN_CTL_PLL1
  __shx_display_reg $CLOCKGEN_CTL_PLL2
  __shx_display_reg $CLOCKGEN_STATUS_PLL
end
##}}}

##{{{  STd2000 GENCONF
## General configuration registers (STd2000 variant)
define std2000_display_genconf_regs
  __shx_display_reg $GENCONF_PIO_CONF
  __shx_display_reg $GENCONF_SYS_CONF
  __shx_display_reg $GENCONF_STBY_CMD
  __shx_display_reg $GENCONF_STBY_STATUS
  __shx_display_reg $GENCONF_LMI1_DLL1_CTL
  __shx_display_reg $GENCONF_LMI1_PDL4_CTL
  __shx_display_reg $GENCONF_LMI1_DLL2_CTL
  __shx_display_reg $GENCONF_LMI1_PDL_0_3_MSB_CTL
  __shx_display_reg $GENCONF_LMI1_PDL_0_3_LSB_CTL
  __shx_display_reg $GENCONF_LMI1_STATUS
  __shx_display_reg $GENCONF_LMI2_DLL1_CTL
  __shx_display_reg $GENCONF_LMI2_PDL4_CTL
  __shx_display_reg $GENCONF_LMI2_DLL2_CTL
  __shx_display_reg $GENCONF_LMI2_PDL_0_3_MSB_CTL
  __shx_display_reg $GENCONF_LMI2_PDL_0_3_LSB_CTL
  __shx_display_reg $GENCONF_LMI2_STATUS
  __shx_display_reg $GENCONF_DDR_CONF
  __shx_display_reg $GENCONF_COMPENSATION_CONF_3V3_A
  __shx_display_reg $GENCONF_COMPENSATION_CONF_3V3_B
  __shx_display_reg $GENCONF_COMPENSATION_CONF_2V5
  __shx_display_reg $GENCONF_COMPENSATION_CONF_1V8
  __shx_display_reg $GENCONF_COMPENSATION_STATUS_3V3_A
  __shx_display_reg $GENCONF_COMPENSATION_STATUS_3V3_B
  __shx_display_reg $GENCONF_COMPENSATION_STATUS_2V5
  __shx_display_reg $GENCONF_COMPENSATION_STATUS_1V8
end
##}}}

################################################################################
## STV0498 control registers
################################################################################

##{{{  STV0498 CLOCKGEN
## Clock Generator control registers (STV0498 variant)
define stv0498_display_clockgen_regs
  __shx_display_reg $CLOCKGEN_LOCK
  __shx_display_reg $CLOCKGEN_PLLXN_LOCK_STATUS
  __shx_display_reg $CLOCKGEN_REG_LOCK_STATUS
  __shx_display_reg $CLOCKGEN_MODE_CONTROL
  __shx_display_reg $CLOCKGEN_CLOCK_STANDBY
  __shx_display_reg $CLOCKGEN_REDUCED_PWR_CTRL
  __shx_display_reg $CLOCKGEN_PLLXN_CONFIG0
  __shx_display_reg $CLOCKGEN_PLLXN_CONFIG1
  __shx_display_reg $CLOCKGEN_CLOCK_FE_DIV
  __shx_display_reg $CLOCKGEN_CLOCK_SELECT_CFG
  __shx_display_reg $CLOCKGEN_DIVIDER_FORCE_CFG
  __shx_display_reg $CLOCKGEN_CLOCK_OBSERV_CFG
  __shx_display_reg $CLOCKGEN_CLKDIV0_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV0_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV0_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKDIV1_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV1_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV1_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKDIV2_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV2_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV2_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKDIV3_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV3_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV3_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKDIV4_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV4_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV4_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKDIV5_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV5_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV5_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKDIV6_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV6_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV6_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKDIV7_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKDIV7_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKDIV7_CONFIG2
  __shx_display_reg $CLOCKGEN_CLKSDIV_CONFIG0
  __shx_display_reg $CLOCKGEN_CLKSDIV_CONFIG1
  __shx_display_reg $CLOCKGEN_CLKSDIV_CONFIG2
  __shx_display_reg $CLOCKGEN_RTC_DIV_CONFIG
end
##}}}

##{{{  STV0498 SYSCONF
## System configuration registers (STV0498 variant)
define stv0498_display_sysconf_regs
  __shx_display_reg $SYSCONF_DEVICEID
  __shx_display_reg $SYSCONF_SYS_STA00
  __shx_display_reg $SYSCONF_SYS_STA01
  __shx_display_reg $SYSCONF_SYS_CFG00
  __shx_display_reg $SYSCONF_SYS_CFG01
  __shx_display_reg $SYSCONF_SYS_CFG02
  __shx_display_reg $SYSCONF_SYS_CFG03
  __shx_display_reg $SYSCONF_SYS_CFG04
  __shx_display_reg $SYSCONF_SYS_CFG05
  __shx_display_reg $SYSCONF_SYS_CFG06
  __shx_display_reg $SYSCONF_SYS_CFG07
  __shx_display_reg $SYSCONF_SYS_CFG08
  __shx_display_reg $SYSCONF_SYS_CFG09
  __shx_display_reg $SYSCONF_SYS_CFG10
  __shx_display_reg $SYSCONF_SYS_CFG11
  __shx_display_reg $SYSCONF_SYS_CFG12
  __shx_display_reg $SYSCONF_SYS_CFG13
  __shx_display_reg $SYSCONF_SYS_CFG14
  __shx_display_reg $SYSCONF_SYS_CFG15
  __shx_display_reg $SYSCONF_SYS_CFG16
  __shx_display_reg $SYSCONF_SYS_CFG17
  __shx_display_reg $SYSCONF_SYS_CFG18
end
##}}}

################################################################################
## SH4/ST40 cores
################################################################################

##{{{  SH4/ST40-100
## SH4-100 core control registers
define sh4100_display_core_si_regs
  ## Core SH4 control registers
  sh4_display_ccn_regs
  sh4_display_ubc_regs
  sh4_display_udi_regs
  sh4_display_aud_regs

  ## Generic SH4 control registers
  sh4_display_tmu_regs
  sh4_display_rtc_regs
  sh4_display_intc_regs
  sh4_display_cpg_regs
end

## ST40-100 core control registers
define st40100_display_core_si_regs
  sh4100_display_core_si_regs

  ## Common ST40 control registers
  st40_display_scif_regs 1
  st40_display_scif_regs 2

  st40_display_intc2_regs
end
##}}}

##{{{  SH4/ST40-200
## SH4-200 core control registers
define sh4200_display_core_si_regs
  sh4100_display_core_si_regs
end

## ST40-200 core control registers
define st40200_display_core_si_regs
  sh4200_display_core_si_regs

  ## Common ST40 control registers
  st40_display_scif_regs 2
end
##}}}

##{{{  SH4/ST40-400
## SH4-400 core control registers
define sh4400_display_core_si_regs
  sh4200_display_core_si_regs
end

## ST40-400 core control registers
define st40400_display_core_si_regs
  st40200_display_core_si_regs
end
##}}}

##{{{  SH4/ST40-500
## SH4-500 core control registers
define sh4500_display_core_si_regs
  sh4200_display_core_si_regs
end

## ST40-500 core control registers
define st40500_display_core_si_regs
  st40200_display_core_si_regs
end
##}}}

##{{{  ST40-300
## ST40-300 control registers
define st40300_display_core_si_regs
  ## Core ST403xx control registers
  st403xx_display_ccn_regs

  ## Core SH4 control registers
  sh4_display_ubc_regs
  sh4_display_udi_regs
  sh4_display_aud_regs

  ## Generic SH4 control registers
  sh4_display_tmu_regs
  sh4_display_intc_regs

  ## Common ST403xx control registers
  st403xx_display_cpg_regs
end
##}}}

################################################################################
## SH4/ST40 SoCs
################################################################################

##{{{  SH7750
## SH7750 control registers
define sh7750_display_si_regs
  ## Core SH4 control registers
  sh4_display_ccn_regs
  sh4_display_ubc_regs
  sh4_display_udi_regs
  sh4_display_aud_regs

  ## Generic SH4 control registers
  sh4_display_tmu_regs
  sh4_display_rtc_regs
  sh4_display_cpg_regs

  ## SH775x and SH7750 control registers
  sh7750_display_intc_regs

  sh775x_display_bsc_regs
  sh775x_display_dmac_regs
  sh775x_display_sci_regs
  sh775x_display_scif_regs
end
##}}}

##{{{  SH7751
## SH7751 control registers
define sh7751_display_si_regs
  sh4100_display_core_si_regs

  ## SH775x and SH7751 control registers
  sh7751_display_tmu_regs
  sh7751_display_intc_regs
  sh7751_display_cpg_regs

  sh775x_display_bsc_regs
  sh775x_display_dmac_regs
  sh775x_display_sci_regs
  sh775x_display_scif_regs
end
##}}}

##{{{  SH4202
## SH4202 control registers
define sh4202_display_si_regs
  sh4200_display_core_si_regs

  ## Common SH42xx control registers
  sh42xx_display_cpg2_regs
  sh42xx_display_dmac_regs
  sh42xx_display_intc2_regs
  sh42xx_display_scif_regs

  ## Memory control registers
  sh42xx_display_emi_regs
  sh42xx_display_femi_regs
end
##}}}

##{{{  ST40STB1 (HCMOS7)
## ST40STB1 control registers
define st40stb1_hcmos7_display_si_regs
  st40100_display_core_si_regs

  ## System Architecture Volume 1: System
  st40_display_dmac_regs
  st40_display_clockgen_regs A

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs
  st40_display_fmi_hcmos7_regs
  st40_display_pci_hcmos7_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
end
##}}}

##{{{  ST40STB1
## ST40STB1 control registers
define st40stb1_display_si_regs
  st40100_display_core_si_regs

  ## System Architecture Volume 1: System
  st40_display_dmac_regs
  st40_display_clockgen_regs A
  st40_display_clockgen_regs B

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs
  st40_display_emi_regs
  st40_display_pci_regs
  st40_display_empi_regs
  st40_display_mpxarb_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_mailbox_regs ST20 ST40
  st40_display_sysconf_regs
end
##}}}

##{{{  ST40RA
## ST40RA control registers
define st40ra_display_si_regs
  st40stb1_display_si_regs
end
##}}}

##{{{  ST40GX1
## ST40GX1 control registers
define st40gx1_display_si_regs
  st40100_display_core_si_regs

  ## System Architecture Volume 1: System
  st40_display_dmac_regs
  st40_display_clockgen_regs A
  st40_display_clockgen_regs B

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs
  st40_display_emi_regs
  st40_display_pci_regs
  st40_display_empi_regs
  st40_display_mpxarb_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_mailbox_regs ST20 ST40
  st40_display_sysconf_regs
end
##}}}

##{{{  STm8000
## STm8000 control registers
define stm8000_display_si_regs
  st40100_display_core_si_regs

  ## DVD Platform Architecture Volume 1: Cores
  st40_display_ilc_regs
  st40_display_mailbox_regs ST40 ST200 0
  st40_display_mailbox_regs ST40 ST200 1

  ## DVD Platform Architecture Volume 2: System Services
  st40_display_dmac_regs
  stm8000_display_clockgen_regs A
  stm8000_display_fs_regs A
  stm8000_display_fs_regs B
  st40_display_lmi_regs
  st40_display_emi_regs
  st40_display_sysconf_regs

  ## DVD Platform Architecture Volume 4: I/O Devices
  st40_display_pio_regs 0
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_pio_regs 5
  st40_display_pio_regs 6
  st40_display_pio_regs 7
  st40_display_asc_regs 0
  st40_display_asc_regs 1
  st40_display_asc_regs 2
  st40_display_asc_regs 3
  st40_display_asc_regs 4
  st40_display_ssc_regs 0
  st40_display_ssc_regs 1

  ## DVD Platform Architecture Volume 6: Video/Audio Encoding
  stm8000_display_lx_glue_regs
  stm8000_display_she_regs
end
##}}}

##{{{  STi5528
## STi5528 control registers
define sti5528_display_si_regs
  st40100_display_core_si_regs

  ## STi5528 control registers
  sti5528_display_sysconf_regs
  sti5528_display_clockgen_regs
  sti5528_display_stbus_regs

  ## System Architecture Volume 1: System
  st40_display_dmac_regs

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs
  st40_display_emi_regs
  st40_display_pci_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_mailbox_regs ST20 ST40
  st40_display_pio_regs 0
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_pio_regs 5
  st40_display_pio_regs 6
  st40_display_pio_regs 7
  st40_display_asc_regs 0
  st40_display_asc_regs 1
  st40_display_asc_regs 2
  st40_display_asc_regs 3
  st40_display_asc_regs 4
  st40_display_ssc_regs 0
  st40_display_ssc_regs 1
end
##}}}

##{{{  STd1000
## STd1000 control registers
define std1000_display_si_regs
  st40200_display_core_si_regs

  ## Common ST40 control registers
  st40_display_ilc_regs

  ## STd1000 control registers
  std1000_display_genconf_regs
  std1000_display_mixer_regs
  std1000_display_clockgen_regs

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmigp_regs
  st40_display_emi_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_pio_regs 5
  st40_display_pio_regs 6
  st40_display_pio_regs 7
  st40_display_pio_regs 8
  st40_display_pio_regs 9
  st40_display_pio_regs 10
  st40_display_pio_regs 11
  st40_display_pio_regs 12
end
##}}}

##{{{  STd2000
## STd2000 control registers
define std2000_display_si_regs
  st40200_display_core_si_regs

  ## Common ST40 control registers
  st40_display_ilc_regs

  ## STd2000 control registers
  std2000_display_genconf_regs
  std2000_display_mixer_regs 1
  std2000_display_mixer_regs 2
  std2000_display_clockgen_regs

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs 1
  st40_display_lmi_regs 2
  st40_display_emi_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_pio_regs 5
  st40_display_pio_regs 6
  st40_display_pio_regs 7
  st40_display_pio_regs 8
  st40_display_pio_regs 9
  st40_display_pio_regs 10
  st40_display_pio_regs 11
  st40_display_pio_regs 12
end
##}}}

##{{{  STb7100
## STb7100 control registers
define stb7100_display_si_regs
  st40200_display_core_si_regs

  ## Common ST40 control registers
  st40_display_ilc_regs
  st40_display_intc2_regs

  ## STb710x control registers
  stb710x_display_sysconf_regs
  stb710x_display_clockgena_regs
  stb710x_display_stbus_regs

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs SYS
  st40_display_lmi_regs VID
  st40_display_emi_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_mailbox_regs ST231 ST40 0
  st40_display_mailbox_regs ST231 ST40 1
  st40_display_pio_regs 0
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_pio_regs 5
  st40_display_asc_regs 0
  st40_display_asc_regs 1
  st40_display_asc_regs 2
  st40_display_asc_regs 3
  st40_display_ssc_regs 0
  st40_display_ssc_regs 1
  st40_display_ssc_regs 2
end
##}}}

##{{{  STb7109
## STb7109 control registers
define stb7109_display_si_regs
  stb7100_display_si_regs
end
##}}}

##{{{  STi5202
## STi5202 control registers
define sti5202_display_si_regs
  st40200_display_core_si_regs

  ## Common ST40 control registers
  st40_display_ilc_regs
  st40_display_intc2_regs

  ## STb710x control registers
  stb710x_display_sysconf_regs
  stb710x_display_clockgena_regs
  stb710x_display_stbus_regs

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs
  st40_display_emi_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_mailbox_regs ST231 ST40 0
  st40_display_mailbox_regs ST231 ST40 1
  st40_display_pio_regs 0
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_pio_regs 5
  st40_display_asc_regs 0
  st40_display_asc_regs 1
  st40_display_asc_regs 2
  st40_display_asc_regs 3
  st40_display_ssc_regs 0
  st40_display_ssc_regs 1
  st40_display_ssc_regs 2
end
##}}}

##{{{  STi7200
## STi7200 control registers
define sti7200_display_si_regs
  if ($argc > 0)
    $arg0_display_core_si_regs
  else
    st40200_display_core_si_regs
  end

  ## Common ST40 control registers
  st40_display_ilc_regs

  ## STb710x control registers
  sti7200_display_sysconf_regs
  sti7200_display_clockgena_regs
  sti7200_display_clockgenb_regs

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmigp_regs 0
  st40_display_lmigp_regs 1
  st40_display_emi_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_mailbox_regs ST231_AUD0 ST40 0
  st40_display_mailbox_regs ST231_VID0 ST40 1
  st40_display_mailbox_regs ST231_AUD1 ST40 2
  st40_display_mailbox_regs ST231_VID1 ST40 3
  st40_display_pio_regs 0
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_pio_regs 5
  st40_display_pio_regs 6
  st40_display_pio_regs 7
  st40_display_asc_regs 0
  st40_display_asc_regs 1
  st40_display_asc_regs 2
  st40_display_asc_regs 3
  st40_display_ssc_regs 0
  st40_display_ssc_regs 1
  st40_display_ssc_regs 2
  st40_display_ssc_regs 3
  st40_display_ssc_regs 4
end
##}}}

##{{{  STV0498
## STV0498 control registers
define stv0498_display_si_regs
  st40400_display_core_si_regs

  ## Common ST40 control registers
  st40_display_ilc_regs
  st40_display_intc2_regs

  ## STV0498 control registers
  stv0498_display_sysconf_regs
  stv0498_display_clockgen_regs

  ## System Architecture Volume 2: Bus Interfaces
  st40_display_lmi_regs
  st40_display_emi_regs

  ## System Architecture Volume 4: I/O Devices
  st40_display_pio_regs 0
  st40_display_pio_regs 1
  st40_display_pio_regs 2
  st40_display_pio_regs 3
  st40_display_pio_regs 4
  st40_display_asc_regs 0
  st40_display_ssc_regs 0
  st40_display_ssc_regs 1
  st40_display_ssc_regs 2
  st40_display_ssc_regs 3
  st40_display_ssc_regs 4
  st40_display_ssc_regs 5
end
##}}}
