################################################################################

define mb796sti5206sim
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb796sti5206_fsim_setup mb796sti5206sim_setup $arg0
  else
    connectsh4simle mb796sti5206_fsim_setup mb796sti5206sim_setup ""
  end
end

document mb796sti5206sim
Connect to and configure a simulated STi5206-Mboard board
Usage: mb796sti5206sim
end

define mb796sti5206simse
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb796sti5206se_fsim_setup mb796sti5206simse_setup $arg0
  else
    connectsh4simle mb796sti5206se_fsim_setup mb796sti5206simse_setup ""
  end
end

document mb796sti5206simse
Connect to and configure a simulated STi5206-Mboard board (STi5206 in 32-bit SE mode)
Usage: mb796sti5206simse
end

define mb796sti5206simseuc
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb796sti5206se_fsim_setup mb796sti5206simseuc_setup $arg0
  else
    connectsh4simle mb796sti5206se_fsim_setup mb796sti5206simseuc_setup ""
  end
end

document mb796sti5206simseuc
Connect to and configure a simulated STi5206-Mboard board (STi5206 in 32-bit SE mode with uncached mappings)
Usage: mb796sti5206simseuc
end

define mb796sti5206simse29
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb796sti5206se_fsim_setup mb796sti5206simse29_setup $arg0
  else
    connectsh4simle mb796sti5206se_fsim_setup mb796sti5206simse29_setup ""
  end
end

document mb796sti5206simse29
Connect to and configure a simulated STi5206-Mboard board (STi5206 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb796sti5206simse29
end

################################################################################

define mb796sti5206psim
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb796sti5206_psim_setup mb796sti5206sim_setup $arg0
  else
    connectsh4psimle mb796sti5206_psim_setup mb796sti5206sim_setup ""
  end
end

document mb796sti5206psim
Connect to and configure a simulated STi5206-Mboard board
Usage: mb796sti5206psim
end

define mb796sti5206psimse
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb796sti5206se_psim_setup mb796sti5206simse_setup $arg0
  else
    connectsh4psimle mb796sti5206se_psim_setup mb796sti5206simse_setup ""
  end
end

document mb796sti5206psimse
Connect to and configure a simulated STi5206-Mboard board (STi5206 in 32-bit SE mode)
Usage: mb796sti5206psimse
end

define mb796sti5206psimseuc
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb796sti5206se_psim_setup mb796sti5206simseuc_setup $arg0
  else
    connectsh4psimle mb796sti5206se_psim_setup mb796sti5206simseuc_setup ""
  end
end

document mb796sti5206psimseuc
Connect to and configure a simulated STi5206-Mboard board (STi5206 in 32-bit SE mode with uncached mappings)
Usage: mb796sti5206psimseuc
end

define mb796sti5206psimse29
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source mb796sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb796sti5206se_psim_setup mb796sti5206simse29_setup $arg0
  else
    connectsh4psimle mb796sti5206se_psim_setup mb796sti5206simse29_setup ""
  end
end

document mb796sti5206psimse29
Connect to and configure a simulated STi5206-Mboard board (STi5206 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb796sti5206psimse29
end

################################################################################
