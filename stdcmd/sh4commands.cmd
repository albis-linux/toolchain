################################################################################

define _ST_display
  init-if-undefined $_ST_display = 0
  keep-variable $_ST_display

  if ($argc < 2)
    help _ST_display
  else
    if ($arg0 < $_ST_display)
      if ($argc == 2)
        printf $arg1
      end
      if ($argc == 3)
        printf $arg1, $arg2
      end
      if ($argc == 4)
        printf $arg1, $arg2, $arg3
      end
      if ($argc == 5)
        printf $arg1, $arg2, $arg3, $arg4
      end
      if ($argc == 6)
        printf $arg1, $arg2, $arg3, $arg4, $arg5
      end
      if ($argc == 7)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6
      end
      if ($argc == 8)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7
      end
      if ($argc == 9)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8
      end
      if ($argc == 10)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9
      end
      printf "\n"
    end
  end
end

document _ST_display
Display a message depending on the trace level (default: 0)
Usage: _ST_display <level> <format> [<argument>]*
end

################################################################################

define posixconsole
  if ($arg0)
    console on
  else
    console off
  end
end

document posixconsole
Enable or disable the creation of a separate POSIX console (default: 1)
Usage: posixconsole <mode>
where <mode> may be 0 (off) or 1 (on)
end

################################################################################

define memory-add-gdb
  set $_loaddr = $arg0
  set $_hiaddr = $_loaddr + ((($arg1 * 1024) * 1024) - 1)
  mem $_loaddr $_hiaddr nocache
end

define memory-add
  memory-add-gdb $arg1 $arg2
end

document memory-add
Define a memory region in GDB
Usage: memory-add <id> <physical-base-address> <size in Mb> <type>
where <id> is the name of the memory region
+     <type> may be RAM, ROM or DEV
end

################################################################################

define stmcconfigure
  monitor SuperH-Configure:$arg0
end

document stmcconfigure
Specify an ST Micro Connect configuration command
Usage: stmcconfigure <option>[=<value>]
end

################################################################################

define use-watchpoint-access-size
  stmcconfigure useaccesssize=$arg0
end

document use-watchpoint-access-size
Enable or disable use of access size in hardware watchpoints (default: on)
Usage: use-watchpoint-access-size <mode>
where <mode> may be an access size (1, 2, 4, or 8), on or off
end

################################################################################

define linkspeed
  stmcconfigure linkspeed=$arg0
end

document linkspeed
Set the debug link speed (default: 10MHz)
Usage: linkspeed <speed>
where <speed> may be
25MHz 20MHz 12.5MHz 10MHz 6.25MHz 5MHz 3.125MHz 2.5MHz
1.5625MHz 1.25MHz 781.25KHz 625KHz 390.625KHz 312.5KHz
195.312KHz 156.25KHz 97.656KHz 78.125KHz 48.828KHz 39.062KHz
24.414KHz 19.531KHz 12.207KHz 9.765KHz 6.103KHz 4.882KHz
3.051KHz 2.441KHz 1.525KHz 1.220KHz 762Hz 610Hz
end

define linktimeout
  stmcconfigure linktimeout=$arg0
end

document linktimeout
Set the debug link timeout period (default: 1 second)
Usage: linktimeout <timeout>[s|ms]
where <timeout> is the timeout period in seconds or milliseconds
end

################################################################################

define stmcmsglevel
  stmcconfigure msglevel=$arg0
end

document stmcmsglevel
Set the ST Micro Connect message level (default: none)
Usage: stmcmsglevel <level>
where <level> may be all, alllvl, warning, info, debug or none
end

################################################################################

define ondisconnect
  stmcconfigure ondisconnect=$arg0
end

document ondisconnect
Set the action on target disconnect (default: none)
Usage: ondisconnect <action>
where <action> may be none, reset or restart
end

################################################################################
