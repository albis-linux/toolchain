################################################################################

define mb704sim
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb704_fsim_setup mb704sim_setup $arg0
  else
    connectsh4simle mb704_fsim_setup mb704sim_setup ""
  end
end

document mb704sim
Connect to and configure a simulated STi5197-Mboard board
Usage: mb704sim
end

define mb704simse
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb704se_fsim_setup mb704simse_setup $arg0
  else
    connectsh4simle mb704se_fsim_setup mb704simse_setup ""
  end
end

document mb704simse
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode)
Usage: mb704simse
end

define mb704simseuc
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb704se_fsim_setup mb704simseuc_setup $arg0
  else
    connectsh4simle mb704se_fsim_setup mb704simseuc_setup ""
  end
end

document mb704simseuc
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with uncached mappings)
Usage: mb704simseuc
end

define mb704simse29
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb704se_fsim_setup mb704simse29_setup $arg0
  else
    connectsh4simle mb704se_fsim_setup mb704simse29_setup ""
  end
end

document mb704simse29
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb704simse29
end

################################################################################

define mb704psim
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb704_psim_setup mb704sim_setup $arg0
  else
    connectsh4psimle mb704_psim_setup mb704sim_setup ""
  end
end

document mb704psim
Connect to and configure a simulated STi5197-Mboard board
Usage: mb704psim
end

define mb704psimse
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb704se_psim_setup mb704simse_setup $arg0
  else
    connectsh4psimle mb704se_psim_setup mb704simse_setup ""
  end
end

document mb704psimse
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode)
Usage: mb704psimse
end

define mb704psimseuc
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb704se_psim_setup mb704simseuc_setup $arg0
  else
    connectsh4psimle mb704se_psim_setup mb704simseuc_setup ""
  end
end

document mb704psimseuc
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with uncached mappings)
Usage: mb704psimseuc
end

define mb704psimse29
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb704.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb704se_psim_setup mb704simse29_setup $arg0
  else
    connectsh4psimle mb704se_psim_setup mb704simse29_setup ""
  end
end

document mb704psimse29
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb704psimse29
end

################################################################################
