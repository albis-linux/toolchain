##------------------------------------------------------------------------------
## db641.cmd - SMSC9118 Ethernet STEM Module DB641
##------------------------------------------------------------------------------

define db641_emi_configure
  if ($arg1 == 16)
    set *$EMI_BANK$arg0_EMICONFIGDATA0 = 0x041086f1
    set *$EMI_BANK$arg0_EMICONFIGDATA1 = 0x0e024400
    set *$EMI_BANK$arg0_EMICONFIGDATA2 = 0x0e024400
    set *$EMI_BANK$arg0_EMICONFIGDATA3 = 0x00000000
  end
  if ($arg1 == 32)
    set *$EMI_BANK$arg0_EMICONFIGDATA0 = 0x041086e9
    set *$EMI_BANK$arg0_EMICONFIGDATA1 = 0x0e024400
    set *$EMI_BANK$arg0_EMICONFIGDATA2 = 0x0e024400
    set *$EMI_BANK$arg0_EMICONFIGDATA3 = 0x00000000
  end
end
