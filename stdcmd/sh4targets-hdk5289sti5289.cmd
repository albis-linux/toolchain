################################################################################

define hdk5289sti5289sim
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5289_fsim_setup hdk5289sti5289sim_setup $arg0
  else
    connectsh4simle hdk5289sti5289_fsim_setup hdk5289sti5289sim_setup ""
  end
end

document hdk5289sti5289sim
Connect to and configure a simulated STi5289-HDK board
Usage: hdk5289sti5289sim
end

define hdk5289sti5289simse
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5289se_fsim_setup hdk5289sti5289simse_setup $arg0
  else
    connectsh4simle hdk5289sti5289se_fsim_setup hdk5289sti5289simse_setup ""
  end
end

document hdk5289sti5289simse
Connect to and configure a simulated STi5289-HDK board (STi5289 in 32-bit SE mode)
Usage: hdk5289sti5289simse
end

define hdk5289sti5289simseuc
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5289se_fsim_setup hdk5289sti5289simseuc_setup $arg0
  else
    connectsh4simle hdk5289sti5289se_fsim_setup hdk5289sti5289simseuc_setup ""
  end
end

document hdk5289sti5289simseuc
Connect to and configure a simulated STi5289-HDK board (STi5289 in 32-bit SE mode with uncached mappings)
Usage: hdk5289sti5289simseuc
end

define hdk5289sti5289simse29
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5289se_fsim_setup hdk5289sti5289simse29_setup $arg0
  else
    connectsh4simle hdk5289sti5289se_fsim_setup hdk5289sti5289simse29_setup ""
  end
end

document hdk5289sti5289simse29
Connect to and configure a simulated STi5289-HDK board (STi5289 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk5289sti5289simse29
end

################################################################################

define hdk5289sti5289psim
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5289_psim_setup hdk5289sti5289sim_setup $arg0
  else
    connectsh4psimle hdk5289sti5289_psim_setup hdk5289sti5289sim_setup ""
  end
end

document hdk5289sti5289psim
Connect to and configure a simulated STi5289-HDK board
Usage: hdk5289sti5289psim
end

define hdk5289sti5289psimse
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5289se_psim_setup hdk5289sti5289simse_setup $arg0
  else
    connectsh4psimle hdk5289sti5289se_psim_setup hdk5289sti5289simse_setup ""
  end
end

document hdk5289sti5289psimse
Connect to and configure a simulated STi5289-HDK board (STi5289 in 32-bit SE mode)
Usage: hdk5289sti5289psimse
end

define hdk5289sti5289psimseuc
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5289se_psim_setup hdk5289sti5289simseuc_setup $arg0
  else
    connectsh4psimle hdk5289sti5289se_psim_setup hdk5289sti5289simseuc_setup ""
  end
end

document hdk5289sti5289psimseuc
Connect to and configure a simulated STi5289-HDK board (STi5289 in 32-bit SE mode with uncached mappings)
Usage: hdk5289sti5289psimseuc
end

define hdk5289sti5289psimse29
  source register40.cmd
  source display40.cmd
  source sti5289.cmd
  source hdk5289sti5289.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5289se_psim_setup hdk5289sti5289simse29_setup $arg0
  else
    connectsh4psimle hdk5289sti5289se_psim_setup hdk5289sti5289simse29_setup ""
  end
end

document hdk5289sti5289psimse29
Connect to and configure a simulated STi5289-HDK board (STi5289 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk5289sti5289psimse29
end

################################################################################
