################################################################################

define mb519
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb519_setup $arg1
  else
    connectsh4le $arg0 mb519_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519
Connect to and configure an STi7200-Mboard board
Usage: mb519 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519se
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb519se_setup $arg1
  else
    connectsh4le $arg0 mb519se_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519se
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb519se <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519seuc
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb519seuc_setup $arg1
  else
    connectsh4le $arg0 mb519seuc_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519seuc
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519seuc <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519se29
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb519se29_setup $arg1
  else
    connectsh4le $arg0 mb519se29_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519se29
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519se29 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519bypass
  if ($argc > 1)
    mb519 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519bypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40
Usage: mb519bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519sebypass
  if ($argc > 1)
    mb519se $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519se $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519sebypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode)
Usage: mb519sebypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519seucbypass
  if ($argc > 1)
    mb519seuc $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519seuc $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519seucbypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519seucbypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519se29bypass
  if ($argc > 1)
    mb519se29 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519se29 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519se29bypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519se29bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519stmmx
  if ($argc > 1)
    mb519 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519stmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux
Usage: mb519stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519sestmmx
  if ($argc > 1)
    mb519se $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519se $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519sestmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode)
Usage: mb519sestmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519seucstmmx
  if ($argc > 1)
    mb519seuc $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519seuc $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519seucstmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519seucstmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb519se29stmmx
  if ($argc > 1)
    mb519se29 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519se29 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519se29stmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519se29stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb519usb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb519_setup $arg1
  else
    connectsh4usble $arg0 mb519_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519usb
Connect to and configure an STi7200-Mboard board
Usage: mb519usb <target>
where <target> is an ST Micro Connect USB name
end

define mb519seusb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb519se_setup $arg1
  else
    connectsh4usble $arg0 mb519se_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519seusb
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb519seusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519seucusb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb519seuc_setup $arg1
  else
    connectsh4usble $arg0 mb519seuc_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519seucusb
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519seucusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519se29usb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb519se29_setup $arg1
  else
    connectsh4usble $arg0 mb519se29_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb519se29usb
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519se29usb <target>
where <target> is an ST Micro Connect USB name
end

define mb519bypassusb
  if ($argc > 1)
    mb519usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519bypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40
Usage: mb519bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519sebypassusb
  if ($argc > 1)
    mb519seusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519seusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519sebypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode)
Usage: mb519sebypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519seucbypassusb
  if ($argc > 1)
    mb519seucusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519seucusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519seucbypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519seucbypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519se29bypassusb
  if ($argc > 1)
    mb519se29usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519se29usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

document mb519se29bypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519se29bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519stmmxusb
  if ($argc > 1)
    mb519usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519stmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux
Usage: mb519stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519sestmmxusb
  if ($argc > 1)
    mb519seusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519seusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519sestmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode)
Usage: mb519sestmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519seucstmmxusb
  if ($argc > 1)
    mb519seucusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519seucusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519seucstmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519seucstmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb519se29stmmxusb
  if ($argc > 1)
    mb519se29usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519se29usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

document mb519se29stmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519se29stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb519sim
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb519_fsim_setup mb519sim_setup $arg0
  else
    connectsh4simle mb519_fsim_setup mb519sim_setup ""
  end
end

document mb519sim
Connect to and configure a simulated STi7200-Mboard board
Usage: mb519sim
end

define mb519simse
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb519se_fsim_setup mb519simse_setup $arg0
  else
    connectsh4simle mb519se_fsim_setup mb519simse_setup ""
  end
end

document mb519simse
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb519simse
end

define mb519simseuc
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb519se_fsim_setup mb519simseuc_setup $arg0
  else
    connectsh4simle mb519se_fsim_setup mb519simseuc_setup ""
  end
end

document mb519simseuc
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519simseuc
end

define mb519simse29
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb519se_fsim_setup mb519simse29_setup $arg0
  else
    connectsh4simle mb519se_fsim_setup mb519simse29_setup ""
  end
end

document mb519simse29
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519simse29
end

################################################################################

define mb519psim
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb519_psim_setup mb519sim_setup $arg0
  else
    connectsh4psimle mb519_psim_setup mb519sim_setup ""
  end
end

document mb519psim
Connect to and configure a simulated STi7200-Mboard board
Usage: mb519psim
end

define mb519psimse
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb519se_psim_setup mb519simse_setup $arg0
  else
    connectsh4psimle mb519se_psim_setup mb519simse_setup ""
  end
end

document mb519psimse
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb519psimse
end

define mb519psimseuc
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb519se_psim_setup mb519simseuc_setup $arg0
  else
    connectsh4psimle mb519se_psim_setup mb519simseuc_setup ""
  end
end

document mb519psimseuc
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb519psimseuc
end

define mb519psimse29
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb519.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb519se_psim_setup mb519simse29_setup $arg0
  else
    connectsh4psimle mb519se_psim_setup mb519simse29_setup ""
  end
end

document mb519psimse29
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb519psimse29
end

################################################################################

define mb519slowckgbpll0bypass
  set $_mb519slowckgbpll0 = 1
  if ($argc > 1)
    mb519 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

define mb519slowckgbpll0stmmx
  set $_mb519slowckgbpll0 = 1
  if ($argc > 1)
    mb519 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

define mb519seucslowckgbpll0bypass
  set $_mb519slowckgbpll0 = 1
  if ($argc > 1)
    mb519seuc $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb519seuc $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb519bypass_setup"
  end
end

define mb519seucslowckgbpll0stmmx
  set $_mb519slowckgbpll0 = 1
  if ($argc > 1)
    mb519seuc $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb519seuc $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb519stmmx_setup"
  end
end

## Backwards compatibility
define mb519seucbypassslowckgbpll0
  if ($argc > 1)
    mb519seucslowckgbpll0bypass $arg0 $arg1
  else
    mb519seucslowckgbpll0bypass $arg0
  end
end
