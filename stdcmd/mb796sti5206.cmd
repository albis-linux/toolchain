##------------------------------------------------------------------------------
## mb796sti5206.cmd - STi5206-Mboard Validation Platform MB796
##------------------------------------------------------------------------------

##{{{  MB796 (STi5206) PMB Configuration
define mb796sti5206se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x48 128

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb796sti5206seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  sh4_set_pmb 1 0x88 0x48 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb796sti5206se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x48 128
  sh4_set_pmb 2 0xa0 0x40 128 0 0 1
  sh4_set_pmb 3 0xa8 0x48 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB796 (STi5206) Memory
define mb796sti5206_memory_define
  memory-add Flash     0x00000000 32 ROM
  memory-add LMI_SDRAM 0x0c000000 256 RAM
end

define mb796sti5206_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x0c000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define mb796sti5206se_memory_define
  memory-add Flash     0x00000000 32 ROM
  memory-add LMI_SDRAM 0x40000000 256 RAM
end

define mb796sti5206se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x40000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb796sti5206sim_setup
  sti5206_define
  mb796sti5206_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb796sti5206sim_setup
Configure a simulated STi5206-Mboard board
Usage: mb796sti5206sim_setup
end

define mb796sti5206simse_setup
  sti5206_define
  mb796sti5206se_memory_define

  st40300_core_si_regs

  mb796sti5206se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb796sti5206simse_setup
Configure a simulated STi5206-Mboard board with the STi5206 in 32-bit SE mode
Usage: mb796sti5206simse_setup
end

define mb796sti5206simseuc_setup
  sti5206_define
  mb796sti5206se_memory_define

  st40300_core_si_regs

  mb796sti5206seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb796sti5206simseuc_setup
Configure a simulated STi5206-Mboard board with the STi5206 in 32-bit SE mode with uncached RAM mappings
Usage: mb796sti5206simseuc_setup
end

define mb796sti5206simse29_setup
  sti5206_define
  mb796sti5206se_memory_define

  st40300_core_si_regs

  mb796sti5206se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb796sti5206simse29_setup
Configure a simulated STi5206-Mboard board with the STi5206 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb796sti5206simse29_setup
end

define mb796sti5206_fsim_setup
  sti5206_fsim_core_setup
  mb796sti5206_sim_memory_define
end

document mb796sti5206_fsim_setup
Configure functional simulator for STi5206-Mboard board
Usage: mb796sti5206_fsim_setup
end

define mb796sti5206se_fsim_setup
  sti5206_fsim_core_setup
  mb796sti5206se_sim_memory_define
end

document mb796sti5206se_fsim_setup
Configure functional simulator for STi5206-Mboard board with the STi5206 in 32-bit SE mode
Usage: mb796sti5206se_fsim_setup
end

define mb796sti5206_psim_setup
  sti5206_psim_core_setup
  mb796sti5206_sim_memory_define
end

document mb796sti5206_psim_setup
Configure performance simulator for STi5206-Mboard board
Usage: mb796sti5206_psim_setup
end

define mb796sti5206se_psim_setup
  sti5206_psim_core_setup
  mb796sti5206se_sim_memory_define
end

document mb796sti5206se_psim_setup
Configure performance simulator for STi5206-Mboard board with the STi5206 in 32-bit SE mode
Usage: mb796sti5206se_psim_setup
end

define mb796sti5206_display_registers
  st40300_display_core_si_regs
end

document mb796sti5206_display_registers
Display the STi5206 configuration registers
Usage: mb796sti5206_display_registers
end
