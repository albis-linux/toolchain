##
## Configure and display clocks for STm8000
##

##{{{  stm8000_set_clockgen_a_pll1
define stm8000_set_clockgen_a_pll1
  ## Stop CLOCKGENA_PLL1...
  set *$CLOCKGENA_PLL1CR1 = *$CLOCKGENA_PLL1CR1 | 0x20000000

  ## Configure CLOCKGENA_PLL1...
  set *$CLOCKGENA_PLL1CR1 = (*$CLOCKGENA_PLL1CR1 & 0xe0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | (0x1cd << 19)

  ## Restart CLOCKGENA_PLL1...
  set *$CLOCKGENA_PLL1CR1 = *$CLOCKGENA_PLL1CR1 & 0xdfffffff

  ## Wait for CLOCKGENA_PLL1 to lock...
  while ((*$CLOCKGENA_PLL1CR1 & 0x80000000) == 0)
  end
end
##}}}

##{{{  stm8000_set_clockgen_a_pll2
define stm8000_set_clockgen_a_pll2
  ## Stop CLOCKGENA_PLL2...
  set *$CLOCKGENA_PLL2CR = *$CLOCKGENA_PLL2CR | 0x20000000

  ## Configure CLOCKGENA_PLL2...
  set *$CLOCKGENA_PLL2CR = (*$CLOCKGENA_PLL2CR & 0xe0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | (0x1cb << 19)

  ## Restart CLOCKGENA_PLL2...
  set *$CLOCKGENA_PLL2CR = *$CLOCKGENA_PLL2CR & 0xdfffffff

  ## Wait for CLOCKGENA_PLL2 to lock...
  while ((*$CLOCKGENA_PLL2CR & 0x80000000) == 0)
  end
end
##}}}

##{{{  stm8000_set_clockgen_a_mode
define stm8000_set_clockgen_a_mode
  ## Switch to external clock source and set ratios...
  set *$CLOCKGENA_CLK_RATIO = (*$CLOCKGENA_CLK_RATIO & 0xffffffb8) | ($arg0)

  ## Switch to CLOCKGENA_PLL1 clock source...
  set *$CLOCKGENA_CLK_RATIO = *$CLOCKGENA_CLK_RATIO | 0x00000040
end
##}}}

##{{{  stm8000_set_clockgen_a_ddr
define stm8000_set_clockgen_a_ddr
  set *$CLOCKGENA_CLK_RATIO = (*$CLOCKGENA_CLK_RATIO & 0xffffffc7) | ($arg0 << 3)
end
##}}}

##{{{  stm8000_set_fs*_clk*
##{{{  stm8000_set_fs_x_clk_x
define stm8000_set_fs_x_clk_x
  ## Stop FSn_CONFIG_CLK_n...
  set *$FS$arg0_CONFIG_CLK_$arg1 = *$FS$arg0_CONFIG_CLK_$arg1 | 0x04000000

  ## Disable FSn_CONFIG_CLK_n...
  set *$FS$arg0_CONFIG_CLK_$arg1 = *$FS$arg0_CONFIG_CLK_$arg1 & 0xffdfffff

  ## Configure FSn_CONFIG_CLK_n...
  set *$FS$arg0_CONFIG_CLK_$arg1 = (*$FS$arg0_CONFIG_CLK_$arg1 & 0xfe200000) | ($arg2 << 5) | ($arg3) | ($arg4 << 22)

  ## Enable FSn_CONFIG_CLK_n...
  set *$FS$arg0_CONFIG_CLK_$arg1 = *$FS$arg0_CONFIG_CLK_$arg1 | 0x00200000

  ## Start FSn_CONFIG_CLK_n...
  set *$FS$arg0_CONFIG_CLK_$arg1 = *$FS$arg0_CONFIG_CLK_$arg1 & 0xfbffffff
end
##}}}

##{{{  stm8000_set_fsa_clk1
define stm8000_set_fsa_clk1
  stm8000_set_fs_x_clk_x A 1 $arg0 $arg1 $arg2
end
##}}}

##{{{  stm8000_set_fsb_clk1
define stm8000_set_fsb_clk1
  stm8000_set_fs_x_clk_x B 1 $arg0 $arg1 $arg2
end
##}}}

##{{{  stm8000_set_fsb_clk2
define stm8000_set_fsb_clk2
  stm8000_set_fs_x_clk_x B 2 $arg0 $arg1 $arg2
end
##}}}

##{{{  stm8000_set_fsb_clk3
define stm8000_set_fsb_clk3
  stm8000_set_fs_x_clk_x B 3 $arg0 $arg1 $arg2
end
##}}}

##{{{  stm8000_set_fsb_clk4
define stm8000_set_fsb_clk4
  stm8000_set_fs_x_clk_x B 4 $arg0 $arg1 $arg2
end
##}}}
##}}}

##{{{  stm8000_displayclocks
define stm8000_get_clockgen_x_pll_x_frq
  if ($arg1 == 1)
    set $_data = *$CLOCKGEN$arg0_PLL$arg1CR1
  else
    set $_data = *$CLOCKGEN$arg0_PLL$arg1CR
  end

  set $_mdiv = (double) (($_data & 0xff) + 1)
  set $_ndiv = (double) ((($_data >> 8) & 0xff) + 1)
  set $_pdiv = (double) (1 << (($_data >> 16) & 0x7))

  set $arg3 = ((2.0 * $arg2 * $_ndiv) / $_mdiv) / $_pdiv
end

define stm8000_get_fs_x_clk_x_frq
  set $_data = *$FS$arg0_CONFIG_CLK_$arg1

  set $_pe = (double) (($_data >> 5) & 0xffff)
  set $_md = (double) (((signed int) ($_data & 0x1f)) - 32)
  set $_sdiv = (double) (2 << (($_data >> 22) & 0x7))

  set $arg2 = (32768.0 * 216.0) / ($_sdiv * (($_pe * (1.0 + ($_md / 32.0))) - (($_pe - 32768.0) * (1.0 + (($_md + 1.0) / 32.0)))))
end

define stm8000_displayclocks
  if ($argc > 0)
    set $_extfrq = $arg0
  else
    set $_extfrq = 27.0
  end

  stm8000_get_clockgen_x_pll_x_frq A 1 $_extfrq $_pll1frq
  stm8000_get_clockgen_x_pll_x_frq A 2 $_extfrq $_pll2frq

  set $_data = *$CLOCKGENA_CLK_RATIO

  if ((($_data >> 6) & 0x1) == 0x1)
    set $_main1frq = $_pll1frq
    set $_main2frq = $_pll2frq
  else
    set $_main1frq = $_extfrq
    set $_main2frq = $_extfrq
  end

  if (($_data & 0x7) == 0x0)
    set $_st40frq = $_main1frq
    set $_st220frq = $_main1frq
    set $_busfrq = $_main1frq
    set $_perfrq = $_main1frq
  end
  if (($_data & 0x7) == 0x1)
    set $_st40frq = $_main1frq / 2.0
    set $_st220frq = $_main1frq
    set $_busfrq = $_main1frq / 3.0
    set $_perfrq = $_main1frq / 6.0
  end
  if (($_data & 0x7) == 0x2)
    set $_st40frq = $_main1frq / 4.0
    set $_st220frq = $_main1frq / 2.0
    set $_busfrq = $_main1frq / 6.0
    set $_perfrq = $_main1frq / 12.0
  end
  if (($_data & 0x7) == 0x3)
    set $_st40frq = $_main1frq / 2.0
    set $_st220frq = $_main1frq
    set $_busfrq = $_main1frq / 2.0
    set $_perfrq = $_main1frq / 6.0
  end
  if (($_data & 0x7) == 0x4)
    set $_st40frq = $_main1frq / 4.0
    set $_st220frq = $_main1frq
    set $_busfrq = $_main1frq / 6.0
    set $_perfrq = $_main1frq / 12.0
  end
  if (($_data & 0x7) == 0x5)
    set $_st40frq = $_main1frq / 2.0
    set $_st220frq = $_main1frq / 2.0
    set $_busfrq = $_main1frq / 4.0
    set $_perfrq = $_main1frq / 12.0
  end
  if (($_data & 0x7) == 0x6)
    set $_st40frq = $_main1frq / 4.0
    set $_st220frq = $_main1frq / 2.0
    set $_busfrq = $_main1frq / 4.0
    set $_perfrq = $_main1frq / 12.0
  end
  if (($_data & 0x7) == 0x7)
    set $_st40frq = $_main1frq / 4.0
    set $_st220frq = $_main1frq / 2.0
    set $_busfrq = $_main1frq / 6.0
    set $_perfrq = $_main1frq / 8.0
  end

  if ((($_data >> 3) & 0x7) == 0x0)
    set $_lmifrq = $_main2frq / 2.0
  end
  if ((($_data >> 3) & 0x7) == 0x1)
    set $_lmifrq = $_st220frq
  end
  if ((($_data >> 3) & 0x7) == 0x2)
    set $_lmifrq = $_st40frq
  end
  if ((($_data >> 3) & 0x7) == 0x3)
    set $_lmifrq = $_busfrq
  end
  if ((($_data >> 3) & 0x7) == 0x4)
    set $_lmifrq = $_perfrq
  end

  set $_data = *$EMI_FLASHCLKSEL & 0x3

  if ($_data == 0x0)
    set $_emiflashfrq = $_busfrq
  end
  if ($_data == 0x1)
    set $_emiflashfrq = $_busfrq / 2.0
  end
  if ($_data == 0x2)
    set $_emiflashfrq = $_busfrq / 3.0
  end

  set $_data = *$EMI_SDRAMCLKSEL & 0x3

  if ($_data == 0x0)
    set $_emisdramfrq = $_busfrq
  end
  if ($_data == 0x1)
    set $_emisdramfrq = $_busfrq / 2.0
  end
  if ($_data == 0x2)
    set $_emisdramfrq = $_busfrq / 3.0
  end

  stm8000_get_fs_x_clk_x_frq A 1 $_fsack1frq
  stm8000_get_fs_x_clk_x_frq B 1 $_fsbck1frq
  stm8000_get_fs_x_clk_x_frq B 2 $_fsbck2frq
  stm8000_get_fs_x_clk_x_frq B 3 $_fsbck3frq
  stm8000_get_fs_x_clk_x_frq B 4 $_fsbck4frq

  printf "Clock settings:\n"
  printf "\n"
  printf "  CLOCKGENA_PLL1CR1       = 0x%08x\n", *$CLOCKGENA_PLL1CR1
  printf "  CLOCKGENA_PLL2CR        = 0x%08x\n", *$CLOCKGENA_PLL2CR
  printf "  CLOCKGENA_CLK_RATIO     = 0x%08x\n", *$CLOCKGENA_CLK_RATIO
  printf "\n"
  printf "  FSA_CONFIG_GENERIC_INFO = 0x%08x\n", *$FSA_CONFIG_GENERIC_INFO
  printf "  FSA_CONFIG_CLK_1        = 0x%08x\n", *$FSA_CONFIG_CLK_1
  printf "\n"
  printf "  FSB_CONFIG_GENERIC_INFO = 0x%08x\n", *$FSB_CONFIG_GENERIC_INFO
  printf "  FSB_CONFIG_CLK_1        = 0x%08x\n", *$FSB_CONFIG_CLK_1
  printf "  FSB_CONFIG_CLK_2        = 0x%08x\n", *$FSB_CONFIG_CLK_2
  printf "  FSB_CONFIG_CLK_3        = 0x%08x\n", *$FSB_CONFIG_CLK_3
  printf "  FSB_CONFIG_CLK_4        = 0x%08x\n", *$FSB_CONFIG_CLK_4
  printf "\n"
  printf "Clock frequencies\n"
  printf "\n"
  printf "  PLL1        = %.1f MHz\n", $_pll1frq
  printf "  PLL2        = %.1f MHz\n", $_pll2frq
  printf "\n"
  printf "  FSA CLK1    = %.1f MHz\n", $_fsack1frq
  printf "\n"
  printf "  FSB CLK1    = %.1f MHz\n", $_fsbck1frq
  printf "  FSB CLK2    = %.1f MHz\n", $_fsbck2frq
  printf "  FSB CLK3    = %.1f MHz\n", $_fsbck3frq
  printf "  FSB CLK4    = %.1f MHz\n", $_fsbck4frq
  printf "\n"
  printf "  ST40 CPU    = %.1f MHz\n", $_st40frq
  printf "  ST220 CPU   = %.1f MHz\n", $_st220frq
  printf "  ST40 PER    = %.1f MHz\n", $_perfrq
  printf "  ST BUS      = %.1f MHz\n", $_busfrq
  printf "  EMI         = %.1f MHz\n", $_busfrq
  printf "  EMI (FLASH) = %.1f MHz\n", $_emiflashfrq
  printf "  EMI (SDRAM) = %.1f MHz\n", $_emisdramfrq
  printf "  LMI         = %.1f MHz\n", $_lmifrq
end

document stm8000_displayclocks
Display the STm8000 clock frequencies
Usage: stm8000_displayclocks [<frequency>]
where <frequency> is optional and is the external clock frequency (default: 27MHz)
end
##}}}
