##------------------------------------------------------------------------------
## mb548.cmd - DTV150 Development Platforms MB548
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB548 CLOCKGEN Configuration
define mb548_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  ## Setup PLL2/LMI clock path
  set *$CLOCKGEN_CTL_PLL2_FREQ = *$CLOCKGEN_CTL_PLL2_FREQ & 0xfffffffe
  while ((*$CLOCKGEN_STATUS_PLL & 0x00000002) == 0)
  end

  ## Enable clk_lmi_2x
  set *$CLOCKGEN_CTL_EN = *$CLOCKGEN_CTL_EN | 1

  ## Wait for LMI DLL1 to lock
  while ((*$GENCONF_LMI_STATUS & 0x00000001) == 0)
  end

  ## Wait for LMI DLL2 to lock
  while ((*$GENCONF_LMI_STATUS & 0x00000002) == 0)
  end
end
##}}}

##{{{  MB548 EMI Configuration
define mb548_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x04001691
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x0a000000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x0e004400
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000
end
##}}}

##{{{  MB548 LMI Configuration
##{{{  MB548 MIXER Configuration
define mb548_mixer_bypass_configure
  ## Define both memory and register base addresses
  set *$MIXER_LMIBADDR = 0x00000010 | 0x00190000

  set *$MIXER_GENC = 0x00000001
end

define mb548_mixer_standard_configure
  ## Reset low-pri source register file
  set *$MIXER_GENC = 0x00000001

  ## - Clear low-pri source register file reset bit
  ## - Enable flow regulations (input+arbiter+bank)
  ## - Set burst boundary = 32B
  ## - Enable wp, store & forward, page interrupt
  ## - Enable interrupt
  set *$MIXER_GENC = 0x100b1710

  ## - Unmask interrupts
  set *$MIXER_INTMASK = 0x00030003

  ## - Bank bit 8, 9
  set *$MIXER_BANKC = 0x00000908

  ## - Row mask : 0xffffff00
  set *$MIXER_ROWADDRMSK = 0xffffff00

  ## Define both memory and register base addresses
  set *$MIXER_LMIBADDR = 0x00000010 | 0x00190000

  set *$MIXER_BDCRIS = 0x00301010
  set *$MIXER_BDHPP = 0x00001010

  set *$MIXER_BDLAT = 0x60604030
  set *$MIXER_BDQTHD = 0x00000022
  set *$MIXER_BDLDINEF = 0x0210834f
  set *$MIXER_BDSTINEF = 0x0210834f

  ## - 0/1 fifo  : 64
  ## - 2   fifos : 32
  ## - 3   fifos : 16
  ## - 4   fifos : 10
  set *$MIXER_INFLWREG = 0x0a102040

  ## - Set asolute mode
  ## - Enable fifo full mode
  ## - Low thresh hold  = 20
  ## - High thresh hold = 30
  set *$MIXER_ARBFLWREG = 0x1e140003

  ## - Default setting
  set *$MIXER_BKFLWREG = 0x0e0c4032

  ## High priority port flow regulation
  ##   frame size=0xc0, slot=0x02, time out=0x3f
  ## CPU bandwith limitation (but to be removed when cache write-back functional)
  set *$MIXER_HPPFLWREG = 0x003f013f

  ## - pkt_mem     = 64
  ## - st_cell_mem = 128
  set *$MIXER_REQMEM = 0x00800040

  ## - r_pkt_mem = 128
  set *$MIXER_RESMEM = 0x01280080

  set *$MIXER_SPSRCREG_47 = 0x0f0f070f
  set *$MIXER_SPSRCREG_46 = 0x0f0f0f0f
end
##}}}

##{{{  MB548 LMI Utility Commands
define set_mb548_lmi_scr_nop
  set $_lminop = $arg0

  ## LMI.SCR_L
  ##   001=sms (Mode select)=nop
  ##   0=reserved
  ##   xxx=brfsh (Burst refresh)
  ##   000000000=reserved
  ##   xxxxxxxxxxxx=cst (Clock stabilisation time)
  ##   0000=reserved
  ##
  ## LMI.SCR_H
  ##   00000000000000000000000000000000=reserved

  while ($_lminop--)
    set *$LMI_SCR_L = 0x00010011
  end
end
##}}}

##{{{  MB548 LMI (DTV150DB) Configuration
define mb548_lmi_configure
  ## _ST_display (_procname) "Configuring LMI & DDR2 for DTV150DB board"

##------------------------------------------------------------------------------
## Program LMI MIXER registers
##------------------------------------------------------------------------------

  mb548_mixer_standard_configure

  set *$MIXER_DDRP1 = 0x042043c4

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## LMI.MIM_L
  ##   1=dce (DRAM controller)=enable
  ##   1=dt (DRAM type)=DDR2
  ##   0=ret (Retiming stage)
  ##   1=comb16=disabled
  ##   1=comb8=disabled
  ##   1=dqs_recov (DQS recovery mechanism)=enabled
  ##   01=bw (Bus width)=32bits
  ##   0=reserved
  ##   0=dre (DRAM refresh)=disable
  ##   0=reserved
  ##   0=by32ap (MA8 pin)=NOT used for PRE/PALL
  ##   0000=reserved
  ##   xxxxxxxxxxxxx=dri (DRAM refresh interval)
  ##   000=reserved
  ##
  ## LMI.MIM_H
  ##   000=reserved
  ##   00000000=frame=0=inactive
  ##   00000=limit=?
  ##   00000000=load_timeout=inactive
  ##   00000000=store_timeout=inactive

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_L = 0x07a1007b
  set *$LMI_MIM_H = 0x00000000

  ## LMI.STR_L
  ##   xxx=trp (RAS precharge to ACT command)
  ##   xxx=trcdr (RAS to read CAS delay)
  ##   xxxx=trc (RAS cycle time)
  ##   xxxx=tras (RAS active time)
  ##   xx=trrd (RAS to RAS active delay)
  ##   011=twr (Write recovery time)=4 clks
  ##   0100=cl (CAS latency)=4 clks
  ##   10000=trfc (Auto refresh RAS cycle time)=28 clks
  ##   0=disable_twtr (Write to read interruption)=enable
  ##   01=twtr (Write to read interruption)=2 clks
  ##   0=tx1rwl (Read to write extra cycle)=disable
  ##
  ## LMI.STR_H
  ##   0=reserved
  ##   011=trcdw (RAS to write CAS delay)
  ##   xxxx=txsr (Exit self-refresh to next command)
  ##   xx=trtp (Read to precharge command delay)
  ##   xxxx=tfaw (Four bank active time)
  ##   000=al (Additive latency)=0 clks
  ##   000000000000000=reserved

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_L = 0x282b5edb
  set *$LMI_STR_H = 0x000101d6

  ## LMI.SDRAx_L
  ##   0=enable_ba0 (Bank remapping for BA0)=disable
  ##   0=enable_ba1 (Bank remapping for BA1)=disable
  ##   0=enable_ba2 (Bank remapping for BA2)=disable
  ##   0=swp_add_13_10 (Swap between bit 13 and bit 10 of STBUS address)=disable
  ##   0=swp_add_13_11 (Swap between bit 13 and bit 11 of STBUS address)=disable
  ##   000=reserved
  ##   1010=split=13x10
  ##   0=bank (Bank number)=4
  ##   00000000=reserved
  ##   00011000000=uba (Array upper bound address)=0x18000000 (128 MBytes of DDR2 at base address 0x10000000)

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_L = 0x18000a03

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_L = 0x18000a03

  ## LMI.SCR_L
  ##   011=sms (Mode select)=clock enable
  ##   0=reserved
  ##   xxx=brfsh (Burst refresh)
  ##   000000000=reserved
  ##   xxxxxxxxxxxx=cst (Clock stabilisation time)
  ##   0000=reserved
  ##
  ## LMI.SCR_H
  ##   00000000000000000000000000000000=reserved

  ## _ST_display (_procname) "SDRAM Control Register"

  ##------
  ## ->CKE
  ## ->NOP
  ##------

  set *$LMI_SCR_L = 0x00010013
  ##----------------------------------------------------
  ## 8 NOP : Minimum 400ns between CKE and precharge all
  ##----------------------------------------------------
  set_mb548_lmi_scr_nop 16

  ##----------------
  ## ->Precharge all
  ## ->NOP
  ##----------------

  set *$LMI_SCR_L = 0x00010012
  set_mb548_lmi_scr_nop 16

  ##----------
  ## EMR2->0's
  ## EMR3->0's
  ##----------

  set *$LMI_SDMR0 = 0x00000800
  set *$LMI_SDMR0 = 0x00000c00

  ##----------------------------------------------
  ## EMRS: DLL enable
  ##   EMRS val=01 000 0 0 0 000 0 000 0 0 0
  ##   STBUS   =000 0 0 0 01 000 0 000 0 0 0=0x400
  ##----------------------------------------------

  set *$LMI_SDMR0 = 0x00000426

  ##--------------------------------------------
  ## MRS: DLL reset
  ##   MRS dec=BA       WR      CL    BL
  ##   MRS val=00 000 0 011 1 0 100 0 011
  ##   STBUS  =000 0 01 001 1 0 100 0 011=0x1343
  ##--------------------------------------------

  set *$LMI_SDMR0 = 0x00001353

  ##------------------------------------------------
  ## 8 NOP : 200 clock cycles needed to lock the DLL
  ##------------------------------------------------
  set_mb548_lmi_scr_nop 32

  ##------------------------
  ## ->Precharge all
  ## ->2x CBR (auto refresh)
  ##------------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010014
  set *$LMI_SCR_L = 0x00010014

  ##------------------------------------------
  ## MRS device initialisation
  ##   MRS dec=BA     WR      CL    BL
  ##   MRS val=00 0 0 011 0 0 100 0 011
  ##   STBUS  =0 0 01 001 0 0 100 0 011=0x1243
  ##------------------------------------------

  set *$LMI_SDMR0 = 0x00001253

  ## Should wait at least 200 clks after "step 8"

  ##----------------------------------------------
  ## OCD calibration to default
  ##   EMRS val=01 000 0 0 0 111 0 000 0 0 0
  ##   STBUS   =000 0 0 0 01 111 0 000 0 0 0=0x780
  ##----------------------------------------------

  set *$LMI_SDMR0 = 0x000007a6

  ##----------------------------------------
  ## OCD calibration mode exit
  ##   Desc    =BA      OCD   AL
  ##   EMRS val=001 000 000 1 000 0 00
  ##   STBUS   =000 001 000 1 000 0 00=0x440
  ##----------------------------------------

  set *$LMI_SDMR0 = 0x00000426

  ##-------------------
  ## ->2x Precharge all
  ##-------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010012

  ## Enable refresh
  set *$LMI_MIM_L |= 0x00000200

  ## _ST_display (_procname) "LMI Delay Registers"

  ## Set output impedance of DDR pads
  set *$GENCONF_IO_DDR2_CONF = 0x00000142

  ## Bypass PDL5 in dqs_en generation
  set *$GENCONF_LMI_PL_CTL = 0x1001d002

  ## Delay setting
  set *$GENCONF_LMI_PDL_0_3_MSB_CTL = 0x06010105
end
##}}}

##{{{  MB548 LMI (DTV150SSB - Europe) Configuration
define mb548ssbe_lmi_configure
  ## _ST_display (_procname) "Configuring LMI & DDR2 for DTV150SSB (Europe) board"

##------------------------------------------------------------------------------
## Program LMI MIXER registers
##------------------------------------------------------------------------------

  mb548_mixer_standard_configure

  set *$MIXER_DDRP1 = 0x042453c4

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_L = 0x07a1007b
  set *$LMI_MIM_H = 0x00000000

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_L = 0x46235e9b
  set *$LMI_STR_H = 0x000102d6

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_L = 0x14000903

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_L = 0x14000903

  ## _ST_display (_procname) "SDRAM Control Register"

  ##------
  ## ->CKE
  ## ->NOP
  ##------

  set *$LMI_SCR_L = 0x00010013
  set_mb548_lmi_scr_nop 16

  ##----------------
  ## ->Precharge all
  ## ->NOP
  ##----------------

  set *$LMI_SCR_L = 0x00010012
  set_mb548_lmi_scr_nop 16

  ##----------
  ## EMR2->0's
  ## EMR3->0's
  ##----------

  set *$LMI_SDMR0 = 0x00000800
  set *$LMI_SDMR0 = 0x00000c00

  set *$LMI_SDMR0 = 0x00000426
  set *$LMI_SDMR0 = 0x00001343

  ##------------------------------------------------
  ## 8 NOP : 200 clock cycles needed to lock the DLL
  ##------------------------------------------------

  set_mb548_lmi_scr_nop 32

  ##------------------------
  ## ->Precharge all
  ## ->2x CBR (auto refresh)
  ##------------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010014
  set *$LMI_SCR_L = 0x00010014

  set *$LMI_SDMR0 = 0x00001243
  set *$LMI_SDMR0 = 0x000007a6
  set *$LMI_SDMR0 = 0x00000426

  ##-------------------
  ## ->2x Precharge all
  ##-------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010012

  ## Enable refresh
  set *$LMI_MIM_L |= 0x00000200

  ## _ST_display (_procname) "LMI Delay Registers"

  ## Set output impedance of DDR pads
  set *$GENCONF_IO_DDR2_CONF = 0x00000142

  ## Bypass PDL5 in dqs_en generation
  set *$GENCONF_LMI_PL_CTL = 0x1001d002

  ## Delay setting
  set *$GENCONF_LMI_PDL_0_3_MSB_CTL = 0x0a0a0a0a
end
##}}}

##{{{  MB548 LMI (DTV150SSB - US) Configuration
define mb548ssbu_lmi_configure
  ## _ST_display (_procname) "Configuring LMI & DDR2 for DTV150SSB (US) board"

##------------------------------------------------------------------------------
## Program LMI MIXER registers
##------------------------------------------------------------------------------

  mb548_mixer_standard_configure

  set *$MIXER_DDRP1 = 0x042043c4

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_L = 0x07a1007b
  set *$LMI_MIM_H = 0x00000000

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_L = 0x282b5edb
  set *$LMI_STR_H = 0x000101d6

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_L = 0x14000903

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_L = 0x14000903

  ## _ST_display (_procname) "SDRAM Control Register"

  ##------
  ## ->CKE
  ## ->NOP
  ##------

  set *$LMI_SCR_L = 0x00010013
  set_mb548_lmi_scr_nop 16

  ##----------------
  ## ->Precharge all
  ## ->NOP
  ##----------------

  set *$LMI_SCR_L = 0x00010012
  set_mb548_lmi_scr_nop 16

  ##----------
  ## EMR2->0's
  ## EMR3->0's
  ##----------

  set *$LMI_SDMR0 = 0x00000800
  set *$LMI_SDMR0 = 0x00000c00

  set *$LMI_SDMR0 = 0x00000426
  set *$LMI_SDMR0 = 0x00001353

  ##------------------------------------------------
  ## 8 NOP : 200 clock cycles needed to lock the DLL
  ##------------------------------------------------

  set_mb548_lmi_scr_nop 32

  ##------------------------
  ## ->Precharge all
  ## ->2x CBR (auto refresh)
  ##------------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010014
  set *$LMI_SCR_L = 0x00010014

  set *$LMI_SDMR0 = 0x00001253
  set *$LMI_SDMR0 = 0x000007a6
  set *$LMI_SDMR0 = 0x00000426

  ##-------------------
  ## ->2x Precharge all
  ##-------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010012

  ## Enable refresh
  set *$LMI_MIM_L |= 0x00000200

  ## _ST_display (_procname) "LMI Delay Registers"

  ## Set output impedance of DDR pads
  set *$GENCONF_IO_DDR2_CONF = 0x00000142

  ## Bypass PDL5 in dqs_en generation
  set *$GENCONF_LMI_PL_CTL = 0x1001d002

  ## Delay setting
  set *$GENCONF_LMI_PDL_0_3_MSB_CTL = 0x06010105
end
##}}}

##{{{  MB548 LMI (DTV150EVAL) Configuration
define mb548eval_lmi_configure
  ## _ST_display (_procname) "Configuring LMI & DDR2 for DTV150EVAL board"

##------------------------------------------------------------------------------
## Program LMI MIXER registers
##------------------------------------------------------------------------------

  mb548_mixer_bypass_configure

  set *$MIXER_DDRP1 = 0x042043c4

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## LMI.MIM_L
  ##   1=dce (DRAM controller)=enable
  ##   1=dt (DRAM type)=DDR2
  ##   0=ret (Retiming stage)
  ##   1=comb16=disabled
  ##   1=comb8=disabled
  ##   1=dqs_recov (DQS recovery mechanism)=enabled
  ##   01=bw (Bus width)=32bits
  ##   0=reserved
  ##   0=dre (DRAM refresh)=disable
  ##   0=reserved
  ##   0=by32ap (MA8 pin)=NOT used for PRE/PALL
  ##   0000=reserved
  ##   xxxxxxxxxxxxx=dri (DRAM refresh interval)
  ##   000=reserved
  ##
  ## LMI.MIM_H
  ##   000=reserved
  ##   00000000=frame=0=inactive
  ##   00000=limit=?
  ##   00000000=load_timeout=inactive
  ##   00000000=store_timeout=inactive

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_L = 0x07a1007b
  set *$LMI_MIM_H = 0x00000000

  ## LMI.STR_L
  ##   xxx=trp (RAS precharge to ACT command)
  ##   xxx=trcdr (RAS to read CAS delay)
  ##   xxxx=trc (RAS cycle time)
  ##   xxxx=tras (RAS active time)
  ##   xx=trrd (RAS to RAS active delay)
  ##   xxx=twr (Write recovery time)=4 clks
  ##   xxxx=cl (CAS latency)=4 clks
  ##   xxxxx=trfc (Auto refresh RAS cycle time)=28 clks
  ##   x=disable_twtr (Write to read interruption)=enable
  ##   xx=twtr (Write to read interruption)=2 clks
  ##   x=tx1rwl (Read to write extra cycle)=disable
  ##
  ## LMI.STR_H
  ##   0=reserved
  ##   xxx=trcdw (RAS to write CAS delay)
  ##   xxxx=txsr (Exit self-refresh to next command)
  ##   xx=trtp (Read to precharge command delay)
  ##   xxxx=tfaw (Four bank active time)
  ##   xxx=al (Additive latency)=0 clks
  ##   000000000000000=reserved

  ## Timing parameters for HY5PS121821BFP-C4 (1 clock cycle = 4ns @ 250MHz)

  ## LMI.STR_L
  ##  trp=20ns (min 15ns)
  ##  trcdr=20ns (min 15ns)
  ##  trc=68ns (min 60ns)
  ##  tras=48ns (min 45ns)
  ##  trrd=12ns (min 10ns)
  ##  twr=16ns (min 15ns)
  ##  cl=4 clks
  ##  trfc=112ns (min 105ns)=25 clks
  ##  disable_twtr=0
  ##  twtr=8ns (min 7.5ns)
  ##  tx1rwl=0
  ## LMI.STR_H
  ##  trcd=20ns (min 15ns)
  ##  txsr=68ns (min 200ns)
  ##  trtp=8ns (min 7.5ns)
  ##  tfawc=52ns (min 50ns)
  ##  al=4 clks

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_L = (((20 / 4) - 2) << 0) | \
                    (((20 / 4) - 2) << 3) | \
                    (((68 / 4) - 6) << 6) | \
                    (((48 / 4) - 5) << 10) | \
                    (((12 / 4) - 2) << 14) | \
                    (((16 / 4) - 1) << 16) | \
                    (4 << 19) | \
                    (16 << 23) | \
                    (0 << 28) | \
                    (((8 / 4) - 1) << 29) | \
                    (0 << 31)
  set *$LMI_STR_H = (((20 / 4) - 2) << 1) | \
                    ((68 / 4) << 4) | \
                    (((8 / 4) - 1) << 8) | \
                    (((52 / 4) - 7) << 10) | \
                    (4 << 14)

  ## LMI.SDRAx_L
  ##   0=enable_ba0 (Bank remapping for BA0)=disable
  ##   0=enable_ba1 (Bank remapping for BA1)=disable
  ##   0=enable_ba2 (Bank remapping for BA2)=disable
  ##   0=swp_add_13_10 (Swap between bit 13 and bit 10 of STBUS address)=disable
  ##   0=swp_add_13_11 (Swap between bit 13 and bit 11 of STBUS address)=disable
  ##   000=reserved
  ##   1010=split=13x10
  ##   0=bank (Bank number)=4
  ##   00000000=reserved
  ##   00011000000=uba (Array upper bound address)=0x18000000 (128 MBytes of DDR2 at base address 0x10000000)

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_L = 0x18000a03

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_L = 0x18000a03

  ## LMI.SCR_L
  ##   011=sms (Mode select)=clock enable
  ##   0=reserved
  ##   xxx=brfsh (Burst refresh)
  ##   000000000=reserved
  ##   xxxxxxxxxxxx=cst (Clock stabilisation time)
  ##   0000=reserved
  ##
  ## LMI.SCR_H
  ##   00000000000000000000000000000000=reserved

  ## _ST_display (_procname) "SDRAM Control Register"

  ##------
  ## ->CKE
  ## ->NOP
  ##------

  set *$LMI_SCR_L = 0x00010013
  ##----------------------------------------------------
  ## 8 NOP : Minimum 400ns between CKE and precharge all
  ##----------------------------------------------------
  set_mb548_lmi_scr_nop 16

  ##----------------
  ## ->Precharge all
  ## ->NOP
  ##----------------

  set *$LMI_SCR_L = 0x00010012
  set_mb548_lmi_scr_nop 16

  ##----------
  ## EMR2->0's
  ## EMR3->0's
  ##----------

  set *$LMI_SDMR0 = 0x00000800
  set *$LMI_SDMR0 = 0x00000c00

  ##------------------------------------------------------
  ## EMRS: DLL enable + ODT enabled 75ohms + DQSN disabled
  ##   EMRS val=01 000 0 0 0 000 0 000 0 0 0
  ##   STBUS   =000 0 0 0 01 000 0 000 0 0 0=0x400
  ##------------------------------------------------------

  set *$LMI_SDMR0 = 0x00001426

  ##--------------------------------------------
  ## MRS: DLL reset
  ##   MRS dec=BA       WR      CL    BL
  ##   MRS val=00 000 0 011 1 0 100 0 011
  ##   STBUS  =000 0 01 001 1 0 100 0 011=0x1343
  ##--------------------------------------------

  set *$LMI_SDMR0 = 0x00001343

  ##------------------------------------------------
  ## 8 NOP : 200 clock cycles needed to lock the DLL
  ##------------------------------------------------
  set_mb548_lmi_scr_nop 32

  ##------------------------
  ## ->Precharge all
  ## ->2x CBR (auto refresh)
  ##------------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010014
  set *$LMI_SCR_L = 0x00010014

  ##------------------------------------------
  ## MRS device initialisation
  ##   MRS dec=BA     WR      CL    BL
  ##   MRS val=00 0 0 011 0 0 100 0 011
  ##   STBUS  =0 0 01 001 0 0 100 0 011=0x1243
  ##------------------------------------------

  set *$LMI_SDMR0 = 0x00001243

  ## Should wait at least 200 clks after "step 8"

  ##----------------------------------------------
  ## OCD calibration to default
  ##   EMRS val=01 000 0 0 0 111 0 000 0 0 0
  ##   STBUS   =000 0 0 0 01 111 0 000 0 0 0=0x780
  ##----------------------------------------------

  set *$LMI_SDMR0 = 0x000007a6

  ##----------------------------------------
  ## OCD calibration mode exit
  ##   Desc    =BA      OCD   AL
  ##   EMRS val=001 000 000 1 000 0 00
  ##   STBUS   =000 001 000 1 000 0 00=0x440
  ##----------------------------------------

  set *$LMI_SDMR0 = 0x00001426

  ##-------------------
  ## ->2x Precharge all
  ##-------------------

  set *$LMI_SCR_L = 0x00010012
  set *$LMI_SCR_L = 0x00010012

  ## Enable refresh
  set *$LMI_MIM_L |= 0x00000200

  ## _ST_display (_procname) "LMI Delay Registers"

  ## Bypass PDL5 in dqs_en generation
  set *$GENCONF_LMI_PL_CTL = 0x1001d002

  ## Delay setting
  set *$GENCONF_LMI_PDL_0_3_MSB_CTL = 0x120f0f10
  set *$GENCONF_LMI_PDL_0_3_LSB_CTL = 0x00000000

  ## Use external VREF reference voltage
  set *$GENCONF_IO_DDR2_CONF |= 0x00000010

  ## Set output impedance of DDR pads
  set *$GENCONF_IO_DDR2_CONF &= ~0x00000020
end
##}}}
##}}}

##{{{  MB548 Memory
define mb548_memory_define
  memory-add Flash     0x01000000 8 ROM
  memory-add LMI_SDRAM 0x10000000 128 RAM
end

define mb548ssb_memory_define
  memory-add Flash     0x01000000 4 ROM
  memory-add LMI_SDRAM 0x10000000 64 RAM
end

define mb548_sim_memory_define
  sim_addmemory 0x01000000 8 ROM
  sim_addmemory 0x10000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define mb548ssb_sim_memory_define
  sim_addmemory 0x01000000 4 ROM
  sim_addmemory 0x10000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb548_setup
  std1000_define
  mb548_memory_define

  std1000_si_regs

  mb548_clockgen_configure
  mb548_emi_configure
  mb548_lmi_configure

  set *$CCN_CCR = 0x8000090b
end

document mb548_setup
Configure a DTV150DB board
Usage: mb548_setup
end

define mb548sim_setup
  std1000_define
  mb548_memory_define

  std1000_si_regs

  set *$CCN_CCR = 0x8000090b
end

document mb548sim_setup
Configure a simulated DTV150DB board
Usage: mb548sim_setup
end

define mb548_fsim_setup
  std1000_fsim_core_setup
  mb548_sim_memory_define
end

document mb548_fsim_setup
Configure functional simulator for DTV150DB board
Usage: mb548_fsim_setup
end

define mb548_psim_setup
  std1000_psim_core_setup
  mb548_sim_memory_define
end

document mb548_psim_setup
Configure performance simulator for DTV150DB board
Usage: mb548_psim_setup
end

##------------------------------------------------------------------------------

define mb548ssbe_setup
  std1000_define
  mb548ssb_memory_define

  std1000_si_regs

  mb548_clockgen_configure
  mb548_emi_configure
  mb548ssbe_lmi_configure

  set *$CCN_CCR = 0x8000090b
end

document mb548ssbe_setup
Configure a DTV150SSB (Europe) board
Usage: mb548ssbe_setup
end

define mb548ssbesim_setup
  std1000_define
  mb548ssb_memory_define

  std1000_si_regs

  set *$CCN_CCR = 0x8000090b
end

document mb548ssbesim_setup
Configure a simulated DTV150SSB (Europe) board
Usage: mb548ssbesim_setup
end

define mb548ssbe_fsim_setup
  std1000_fsim_core_setup
  mb548ssb_sim_memory_define
end

document mb548ssbe_fsim_setup
Configure functional simulator for DTV150SSB (Europe) board
Usage: mb548ssbe_fsim_setup
end

define mb548ssbe_psim_setup
  std1000_psim_core_setup
  mb548ssb_sim_memory_define
end

document mb548ssbe_psim_setup
Configure performance simulator for DTV150SSB (Europe) board
Usage: mb548ssbe_psim_setup
end

##------------------------------------------------------------------------------

define mb548ssbu_setup
  std1000_define
  mb548ssb_memory_define

  std1000_si_regs

  mb548_clockgen_configure
  mb548_emi_configure
  mb548ssbu_lmi_configure

  set *$CCN_CCR = 0x8000090b
end

document mb548ssbu_setup
Configure a DTV150SSB (US) board
Usage: mb548ssbu_setup
end

define mb548ssbusim_setup
  std1000_define
  mb548ssb_memory_define

  std1000_si_regs

  set *$CCN_CCR = 0x8000090b
end

document mb548ssbusim_setup
Configure a simulated DTV150SSB (US) board
Usage: mb548ssbusim_setup
end

define mb548ssbu_fsim_setup
  std1000_fsim_core_setup
  mb548ssb_sim_memory_define
end

document mb548ssbu_fsim_setup
Configure functional simulator for DTV150SSB (US) board
Usage: mb548ssbu_fsim_setup
end

define mb548ssbu_psim_setup
  std1000_psim_core_setup
  mb548ssb_sim_memory_define
end

document mb548ssbu_psim_setup
Configure performance simulator for DTV150SSB (US) board
Usage: mb548ssbu_psim_setup
end

##------------------------------------------------------------------------------

define mb548eval_setup
  std1000_define
  mb548_memory_define

  std1000_si_regs

  mb548_clockgen_configure
  mb548_emi_configure
  mb548eval_lmi_configure

  set *$CCN_CCR = 0x8000090b
end

document mb548eval_setup
Configure a DTV150EVAL board
Usage: mb548eval_setup
end

define mb548evalsim_setup
  std1000_define
  mb548_memory_define

  std1000_si_regs

  set *$CCN_CCR = 0x8000090b
end

document mb548evalsim_setup
Configure a simulated DTV150DB board
Usage: mb548evalsim_setup
end

define mb548eval_fsim_setup
  std1000_fsim_core_setup
  mb548_sim_memory_define
end

document mb548eval_fsim_setup
Configure functional simulator for DTV150DB board
Usage: mb548eval_fsim_setup
end

define mb548eval_psim_setup
  std1000_psim_core_setup
  mb548_sim_memory_define
end

document mb548eval_psim_setup
Configure performance simulator for DTV150DB board
Usage: mb548eval_psim_setup
end

##------------------------------------------------------------------------------

define mb548_display_registers
  std1000_display_si_regs
end

document mb548_display_registers
Display the STd1000 configuration registers
Usage: mb548_display_registers
end
