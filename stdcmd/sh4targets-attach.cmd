################################################################################

define attach-sh4be
  set shtdi rtos-initialize-hook attach
  sh4be $arg0 "breakreset ondisconnect=restart"
end

document attach-sh4be
Attach to a running SH4 target (big endian)
Usage: attach-sh4be <target>
where <target> is an ST Micro Connect name or IP address
end

define attach-sh4le
  set shtdi rtos-initialize-hook attach
  sh4le $arg0 "breakreset ondisconnect=restart"
end

document attach-sh4le
Attach to a running SH4 target (little endian)
Usage: attach-sh4le <target>
where <target> is an ST Micro Connect name or IP address
end

define attach-sh4
  set shtdi rtos-initialize-hook attach
  sh4 $arg0 "breakreset ondisconnect=restart"
end

document attach-sh4
Attach to a running SH4 target
Usage: attach-sh4 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define attach-st40300be
  set shtdi rtos-initialize-hook attach
  st40300be $arg0 "breakreset ondisconnect=restart"
end

document attach-st40300be
Attach to a running ST40-300 target (big endian)
Usage: attach-st40300be <target>
where <target> is an ST Micro Connect name or IP address
end

define attach-st40300le
  set shtdi rtos-initialize-hook attach
  st40300le $arg0 "breakreset ondisconnect=restart"
end

document attach-st40300le
Attach to a running ST40-300 target (little endian)
Usage: attach-st40300le <target>
where <target> is an ST Micro Connect name or IP address
end

define attach-st40300
  set shtdi rtos-initialize-hook attach
  st40300 $arg0 "breakreset ondisconnect=restart"
end

document attach-st40300
Attach to a running ST40-300 target
Usage: attach-st40300 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define attach-sh4usbbe
  set shtdi rtos-initialize-hook attach
  sh4usbbe $arg0 "breakreset ondisconnect=restart"
end

document attach-sh4usbbe
Attach to a running SH4 target (big endian)
Usage: attach-sh4usbbe <target>
where <target> is an ST Micro Connect USB name
end

define attach-sh4usble
  set shtdi rtos-initialize-hook attach
  sh4usble $arg0 "breakreset ondisconnect=restart"
end

document attach-sh4usble
Attach to a running SH4 target (little endian)
Usage: attach-sh4usble <target>
where <target> is an ST Micro Connect USB name
end

define attach-sh4usb
  set shtdi rtos-initialize-hook attach
  sh4usb $arg0 "breakreset ondisconnect=restart"
end

document attach-sh4usb
Attach to a running SH4 target
Usage: attach-sh4usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define attach-st40300usbbe
  set shtdi rtos-initialize-hook attach
  st40300usbbe $arg0 "breakreset ondisconnect=restart"
end

document attach-st40300usbbe
Attach to a running ST40-300 target (big endian)
Usage: attach-st40300usbbe <target>
where <target> is an ST Micro Connect USB name
end

define attach-st40300usble
  set shtdi rtos-initialize-hook attach
  st40300usble $arg0 "breakreset ondisconnect=restart"
end

document attach-st40300usble
Attach to a running ST40-300 target (little endian)
Usage: attach-st40300usble <target>
where <target> is an ST Micro Connect USB name
end

define attach-st40300usb
  set shtdi rtos-initialize-hook attach
  st40300usb $arg0 "breakreset ondisconnect=restart"
end

document attach-st40300usb
Attach to a running ST40-300 target
Usage: attach-st40300usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define attach-stb7100-bypass
  source stb7100jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand $arg1"
  else
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand stb7100_bypass_setup_attach"
  end
end

document attach-stb7100-bypass
Attach to a running STb7100 target bypassing to the ST40
Usage: attach-stb7100-bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define attach-stb7100-stmmx
  source stb7100jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand $arg1"
  else
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand stb7100_stmmx_setup_attach"
  end
end

document attach-stb7100-stmmx
Attach to a running STb7100 target via an ST MultiCore/Mux
Usage: attach-stb7100-stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define attach-sti7200-bypass
  source sti7200jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand $arg1"
  else
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand sti7200_bypass_setup_attach"
  end
end

document attach-sti7200-bypass
Attach to a running STi7200 target bypassing to the ST40
Usage: attach-sti7200-bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define attach-sti7200-stmmx
  source sti7200jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand $arg1"
  else
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand sti7200_stmmx_setup_attach"
  end
end

document attach-sti7200-stmmx
Attach to a running STi7200 target via an ST MultiCore/Mux
Usage: attach-sti7200-stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define attach-stv0498-bypass
  source stv0498jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand $arg1"
  else
    sh4le $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand stv0498_bypass_setup_attach"
  end
end

document attach-stv0498-bypass
Attach to a running STV0498 target bypassing to the ST40
Usage: attach-stv0498-bypass <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define attach-stb7100usb-bypass
  source stb7100jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand $arg1"
  else
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand stb7100_bypass_setup_attach"
  end
end

document attach-stb7100usb-bypass
Attach to a running STb7100 target bypassing to the ST40
Usage: attach-stb7100usb-bypass <target>
where <target> is an ST Micro Connect USB name
end

define attach-stb7100usb-stmmx
  source stb7100jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand $arg1"
  else
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand stb7100_stmmx_setup_attach"
  end
end

document attach-stb7100usb-stmmx
Attach to a running STb7100 target via an ST MultiCore/Mux
Usage: attach-stb7100usb-stmmx <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define attach-sti7200usb-bypass
  source sti7200jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand $arg1"
  else
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand sti7200_bypass_setup_attach"
  end
end

document attach-sti7200usb-bypass
Attach to a running STi7200 target bypassing to the ST40
Usage: attach-sti7200usb-bypass <target>
where <target> is an ST Micro Connect USB name
end

define attach-sti7200usb-stmmx
  source sti7200jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand $arg1"
  else
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=stmmx tdidelay=1 -inicommand sti7200_stmmx_setup_attach"
  end
end

document attach-sti7200usb-stmmx
Attach to a running STi7200 target via an ST MultiCore/Mux
Usage: attach-sti7200usb-stmmx <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define attach-stv0498usb-bypass
  source stv0498jtag.cmd
  set shtdi rtos-initialize-hook attach
  if ($argc > 1)
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand $arg1"
  else
    sh4usble $arg0 "breakreset ondisconnect=restart jtagpinout=st40 -inicommand stv0498_bypass_setup_attach"
  end
end

document attach-stv0498usb-bypass
Attach to a running STV0498 target bypassing to the ST40
Usage: attach-stv0498usb-bypass <target>
where <target> is an ST Micro Connect USB name
end

################################################################################
