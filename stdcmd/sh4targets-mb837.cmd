################################################################################

define mb837simse
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source mb837.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb837se_fsim_setup mb837simse_setup $arg0
  else
    connectsh4simle mb837se_fsim_setup mb837simse_setup ""
  end
end

document mb837simse
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode)
Usage: mb837simse
end

define mb837simseuc
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source mb837.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb837se_fsim_setup mb837simseuc_setup $arg0
  else
    connectsh4simle mb837se_fsim_setup mb837simseuc_setup ""
  end
end

document mb837simseuc
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode with uncached mappings)
Usage: mb837simseuc
end

define mb837simse29
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source mb837.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb837se_fsim_setup mb837simse29_setup $arg0
  else
    connectsh4simle mb837se_fsim_setup mb837simse29_setup ""
  end
end

document mb837simse29
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb837simse29
end

################################################################################

define mb837sim
  if ($argc > 0)
    mb837simse $arg0
  else
    mb837simse
  end
end

document mb837sim
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode)
Usage: mb837sim
end

################################################################################

define mb837psimse
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source mb837.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb837se_psim_setup mb837simse_setup $arg0
  else
    connectsh4psimle mb837se_psim_setup mb837simse_setup ""
  end
end

document mb837psimse
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode)
Usage: mb837psimse
end

define mb837psimseuc
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source mb837.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb837se_psim_setup mb837simseuc_setup $arg0
  else
    connectsh4psimle mb837se_psim_setup mb837simseuc_setup ""
  end
end

document mb837psimseuc
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode with uncached mappings)
Usage: mb837psimseuc
end

define mb837psimse29
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source mb837.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb837se_psim_setup mb837simse29_setup $arg0
  else
    connectsh4psimle mb837se_psim_setup mb837simse29_setup ""
  end
end

document mb837psimse29
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb837psimse29
end

################################################################################

define mb837psim
  if ($argc > 0)
    mb837psimse $arg0
  else
    mb837psimse
  end
end

document mb837psim
Connect to and configure a simulated STi7108-Mboard board (STi7108 in 32-bit SE mode)
Usage: mb837psim
end

################################################################################
