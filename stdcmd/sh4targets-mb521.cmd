################################################################################

define mb521
  source register40.cmd
  source display40.cmd
  source stv0498jtag.cmd
  source stv0498.cmd
  source mb521.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb521_setup $arg1
  else
    connectsh4le $arg0 mb521_setup "jtagpinout=st40 hardreset"
  end
end

document mb521
Connect to and configure an STV0498-Mboard board
Usage: mb521 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb521bypass
  if ($argc > 1)
    mb521 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb521 $arg0 "jtagpinout=st40 jtagreset -inicommand mb521bypass_setup"
  end
end

document mb521bypass
Connect to and configure an STV0498-Mboard board bypassing to the ST40
Usage: mb521bypass <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb521usb
  source register40.cmd
  source display40.cmd
  source stv0498jtag.cmd
  source stv0498.cmd
  source mb521.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb521_setup $arg1
  else
    connectsh4usble $arg0 mb521_setup "jtagpinout=st40 hardreset"
  end
end

document mb521usb
Connect to and configure an STV0498-Mboard board
Usage: mb521usb <target>
where <target> is an ST Micro Connect USB name
end

define mb521bypassusb
  if ($argc > 1)
    mb521usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb521usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb521bypass_setup"
  end
end

document mb521bypassusb
Connect to and configure an STV0498-Mboard board bypassing to the ST40
Usage: mb521bypassusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb521sim
  source register40.cmd
  source display40.cmd
  source stv0498.cmd
  source mb521.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb521_fsim_setup mb521sim_setup $arg0
  else
    connectsh4simle mb521_fsim_setup mb521sim_setup ""
  end
end

document mb521sim
Connect to and configure a simulated STV0498-Mboard board
Usage: mb521sim
end

################################################################################

define mb521psim
  source register40.cmd
  source display40.cmd
  source stv0498.cmd
  source mb521.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb521_psim_setup mb521sim_setup $arg0
  else
    connectsh4psimle mb521_psim_setup mb521sim_setup ""
  end
end

document mb521psim
Connect to and configure a simulated STV0498-Mboard board
Usage: mb521psim
end

################################################################################
