################################################################################

define hdk7106sim
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7106_fsim_setup hdk7106sim_setup $arg0
  else
    connectsh4simle hdk7106_fsim_setup hdk7106sim_setup ""
  end
end

document hdk7106sim
Connect to and configure a simulated STi7106-HDK board
Usage: hdk7106sim
end

define hdk7106simse
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7106se_fsim_setup hdk7106simse_setup $arg0
  else
    connectsh4simle hdk7106se_fsim_setup hdk7106simse_setup ""
  end
end

document hdk7106simse
Connect to and configure a simulated STi7106-HDK board (STi7106 in 32-bit SE mode)
Usage: hdk7106simse
end

define hdk7106simseuc
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7106se_fsim_setup hdk7106simseuc_setup $arg0
  else
    connectsh4simle hdk7106se_fsim_setup hdk7106simseuc_setup ""
  end
end

document hdk7106simseuc
Connect to and configure a simulated STi7106-HDK board (STi7106 in 32-bit SE mode with uncached mappings)
Usage: hdk7106simseuc
end

define hdk7106simse29
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7106se_fsim_setup hdk7106simse29_setup $arg0
  else
    connectsh4simle hdk7106se_fsim_setup hdk7106simse29_setup ""
  end
end

document hdk7106simse29
Connect to and configure a simulated STi7106-HDK board (STi7106 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk7106simse29
end

################################################################################

define hdk7106psim
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7106_psim_setup hdk7106sim_setup $arg0
  else
    connectsh4psimle hdk7106_psim_setup hdk7106sim_setup ""
  end
end

document hdk7106psim
Connect to and configure a simulated STi7106-HDK board
Usage: hdk7106psim
end

define hdk7106psimse
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7106se_psim_setup hdk7106simse_setup $arg0
  else
    connectsh4psimle hdk7106se_psim_setup hdk7106simse_setup ""
  end
end

document hdk7106psimse
Connect to and configure a simulated STi7106-HDK board (STi7106 in 32-bit SE mode)
Usage: hdk7106psimse
end

define hdk7106psimseuc
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7106se_psim_setup hdk7106simseuc_setup $arg0
  else
    connectsh4psimle hdk7106se_psim_setup hdk7106simseuc_setup ""
  end
end

document hdk7106psimseuc
Connect to and configure a simulated STi7106-HDK board (STi7106 in 32-bit SE mode with uncached mappings)
Usage: hdk7106psimseuc
end

define hdk7106psimse29
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source hdk7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7106se_psim_setup hdk7106simse29_setup $arg0
  else
    connectsh4psimle hdk7106se_psim_setup hdk7106simse29_setup ""
  end
end

document hdk7106psimse29
Connect to and configure a simulated STi7106-HDK board (STi7106 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk7106psimse29
end

################################################################################
