################################################################################

define mb411stb7109cutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb411stb7109_setup $arg2
  else
    connectsh4le $arg1 mb411stb7109_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutX
Connect to and configure an STb7109-Mboard board
Usage: mb411stb7109cutX <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXse
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb411stb7109se_setup $arg2
  else
    connectsh4le $arg1 mb411stb7109se_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutXse
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode)
Usage: mb411stb7109cutXse <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXseuc
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb411stb7109seuc_setup $arg2
  else
    connectsh4le $arg1 mb411stb7109seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutXseuc
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109cutXseuc <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXse29
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb411stb7109se29_setup $arg2
  else
    connectsh4le $arg1 mb411stb7109se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutXse29
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109cutXse29 <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXbypass
  if ($argc > 2)
    mb411stb7109cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXbypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40
Usage: mb411stb7109cutXbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXsebypass
  if ($argc > 2)
    mb411stb7109cutXse $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutXse $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXsebypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb411stb7109cutXsebypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXseucbypass
  if ($argc > 2)
    mb411stb7109cutXseuc $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutXseuc $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXseucbypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109cutXseucbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXse29bypass
  if ($argc > 2)
    mb411stb7109cutXse29 $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutXse29 $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXse29bypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109cutXse29bypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXstmmx
  if ($argc > 2)
    mb411stb7109cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXstmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7109cutXstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXsestmmx
  if ($argc > 2)
    mb411stb7109cutXse $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutXse $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXsestmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb411stb7109cutXsestmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXseucstmmx
  if ($argc > 2)
    mb411stb7109cutXseuc $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutXseuc $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXseucstmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109cutXseucstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb411stb7109cutXse29stmmx
  if ($argc > 2)
    mb411stb7109cutXse29 $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutXse29 $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXse29stmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109cutXse29stmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb411stb7109cutXusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb411stb7109_setup $arg2
  else
    connectsh4usble $arg1 mb411stb7109_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutXusb
Connect to and configure an STb7109-Mboard board
Usage: mb411stb7109cutXusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXseusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb411stb7109se_setup $arg2
  else
    connectsh4usble $arg1 mb411stb7109se_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutXseusb
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode)
Usage: mb411stb7109cutXseusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXseucusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb411stb7109seuc_setup $arg2
  else
    connectsh4usble $arg1 mb411stb7109seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutXseucusb
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109cutXseucusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXse29usb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb411stb7109se29_setup $arg2
  else
    connectsh4usble $arg1 mb411stb7109se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109cutXse29usb
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109cutXse29usb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXbypassusb
  if ($argc > 2)
    mb411stb7109cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXbypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40
Usage: mb411stb7109cutXbypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXsebypassusb
  if ($argc > 2)
    mb411stb7109cutXseusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutXseusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXsebypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb411stb7109cutXsebypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXseucbypassusb
  if ($argc > 2)
    mb411stb7109cutXseucusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutXseucusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXseucbypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109cutXseucbypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXse29bypassusb
  if ($argc > 2)
    mb411stb7109cutXse29usb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb411stb7109cutXse29usb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109cutXse29bypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109cutXse29bypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXstmmxusb
  if ($argc > 2)
    mb411stb7109cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXstmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7109cutXstmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXsestmmxusb
  if ($argc > 2)
    mb411stb7109cutXseusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutXseusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXsestmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb411stb7109cutXsestmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXseucstmmxusb
  if ($argc > 2)
    mb411stb7109cutXseucusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutXseucusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXseucstmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109cutXseucstmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb411stb7109cutXse29stmmxusb
  if ($argc > 2)
    mb411stb7109cutXse29usb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb411stb7109cutXse29usb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109cutXse29stmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109cutXse29stmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

################################################################################
