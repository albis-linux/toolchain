################################################################################

define _jtagSleep
  sleep ($arg0/1000) (($arg0%1000)*1000)
end

################################################################################

define _jtagCopySignal
  set $_n = ((($arg0_n + 7) / 8) + 3) / 4

  set $arg1_n = $arg0_n

  if ($_n > 0)
    set $arg1_0 = $arg0_0
  end
  if ($_n > 1)
    set $arg1_1 = $arg0_1
  end
  if ($_n > 2)
    set $arg1_2 = $arg0_2
  end
  if ($_n > 3)
    set $arg1_3 = $arg0_3
  end
  if ($_n > 4)
    set $arg1_4 = $arg0_4
  end
  if ($_n > 5)
    set $arg1_5 = $arg0_5
  end
  if ($_n > 6)
    set $arg1_6 = $arg0_6
  end
  if ($_n > 7)
    set $arg1_7 = $arg0_7
  end
end

################################################################################

define _jtagPrintSignalBits
  set $_size = 0
  set $_mask = 0x80000000
  printf "$arg0 = "
  while ($_size++ < 32)
    if ($arg0 & $_mask)
      printf "1"
    else
      printf "0"
    end
    set $_mask >>= 1
  end
  printf "\n"
end

define _jtagPrintSignal
  set $_n = ((($arg0_n + 7) / 8) + 3) / 4

  printf "$arg0_n = %u\n", $arg0_n

  if ($_n > 0)
    printf "$arg0_0 = 0x%08x\n", $arg0_0
    _jtagPrintSignalBits $arg0_0
  end
  if ($_n > 1)
    printf "$arg0_1 = 0x%08x\n", $arg0_1
    _jtagPrintSignalBits $arg0_1
  end
  if ($_n > 2)
    printf "$arg0_2 = 0x%08x\n", $arg0_2
    _jtagPrintSignalBits $arg0_2
  end
  if ($_n > 3)
    printf "$arg0_3 = 0x%08x\n", $arg0_3
    _jtagPrintSignalBits $arg0_3
  end
  if ($_n > 4)
    printf "$arg0_4 = 0x%08x\n", $arg0_4
    _jtagPrintSignalBits $arg0_4
  end
  if ($_n > 5)
    printf "$arg0_5 = 0x%08x\n", $arg0_5
    _jtagPrintSignalBits $arg0_5
  end
  if ($_n > 6)
    printf "$arg0_6 = 0x%08x\n", $arg0_6
    _jtagPrintSignalBits $arg0_6
  end
  if ($_n > 7)
    printf "$arg0_7 = 0x%08x\n", $arg0_7
    _jtagPrintSignalBits $arg0_7
  end
end

################################################################################

define jtagSpin
  jtag mode=continuous
  _jtagSleep $arg0
  jtag mode=singleshot
end

################################################################################
