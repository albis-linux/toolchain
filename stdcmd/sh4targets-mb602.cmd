################################################################################

define mb602
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb602_setup $arg1
  else
    connectsh4le $arg0 mb602_setup "jtagpinout=st40 hardreset"
  end
end

document mb602
Connect to and configure an STi5202-Mboard board
Usage: mb602 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602se
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb602se_setup $arg1
  else
    connectsh4le $arg0 mb602se_setup "jtagpinout=st40 hardreset"
  end
end

document mb602se
Connect to and configure an STi5202-Mboard board (STi5202 in 32-bit SE mode)
Usage: mb602se <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602seuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb602seuc_setup $arg1
  else
    connectsh4le $arg0 mb602seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb602seuc
Connect to and configure an STi5202-Mboard board (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602seuc <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602se29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb602se29_setup $arg1
  else
    connectsh4le $arg0 mb602se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb602se29
Connect to and configure an STi5202-Mboard board (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602se29 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602bypass
  if ($argc > 1)
    mb602 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602 $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602bypass
Connect to and configure an STi5202-Mboard board bypassing to the ST40
Usage: mb602bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602sebypass
  if ($argc > 1)
    mb602se $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602se $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602sebypass
Connect to and configure an STi5202-Mboard board bypassing to the ST40 (STi5202 in 32-bit SE mode)
Usage: mb602sebypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602seucbypass
  if ($argc > 1)
    mb602seuc $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602seuc $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602seucbypass
Connect to and configure an STi5202-Mboard board bypassing to the ST40 (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602seucbypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602se29bypass
  if ($argc > 1)
    mb602se29 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602se29 $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602se29bypass
Connect to and configure an STi5202-Mboard board bypassing to the ST40 (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602se29bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602stmmx
  if ($argc > 1)
    mb602 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602stmmx
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux
Usage: mb602stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602sestmmx
  if ($argc > 1)
    mb602se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602sestmmx
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux (STi5202 in 32-bit SE mode)
Usage: mb602sestmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602seucstmmx
  if ($argc > 1)
    mb602seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602seucstmmx
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602seucstmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb602se29stmmx
  if ($argc > 1)
    mb602se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602se29stmmx
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602se29stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb602usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb602_setup $arg1
  else
    connectsh4usble $arg0 mb602_setup "jtagpinout=st40 hardreset"
  end
end

document mb602usb
Connect to and configure an STi5202-Mboard board
Usage: mb602usb <target>
where <target> is an ST Micro Connect USB name
end

define mb602seusb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb602se_setup $arg1
  else
    connectsh4usble $arg0 mb602se_setup "jtagpinout=st40 hardreset"
  end
end

document mb602seusb
Connect to and configure an STi5202-Mboard board (STi5202 in 32-bit SE mode)
Usage: mb602seusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602seucusb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb602seuc_setup $arg1
  else
    connectsh4usble $arg0 mb602seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb602seucusb
Connect to and configure an STi5202-Mboard board (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602seucusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602se29usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb602se29_setup $arg1
  else
    connectsh4usble $arg0 mb602se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb602se29usb
Connect to and configure an STi5202-Mboard board (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602se29usb <target>
where <target> is an ST Micro Connect USB name
end

define mb602bypassusb
  if ($argc > 1)
    mb602usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602bypassusb
Connect to and configure an STi5202-Mboard board bypassing to the ST40
Usage: mb602bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602sebypassusb
  if ($argc > 1)
    mb602seusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602seusb $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602sebypassusb
Connect to and configure an STi5202-Mboard board bypassing to the ST40 (STi5202 in 32-bit SE mode)
Usage: mb602sebypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602seucbypassusb
  if ($argc > 1)
    mb602seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602seucbypassusb
Connect to and configure an STi5202-Mboard board bypassing to the ST40 (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602seucbypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602se29bypassusb
  if ($argc > 1)
    mb602se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb602se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb602bypass_setup"
  end
end

document mb602se29bypassusb
Connect to and configure an STi5202-Mboard board bypassing to the ST40 (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602se29bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602stmmxusb
  if ($argc > 1)
    mb602usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602stmmxusb
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux
Usage: mb602stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602sestmmxusb
  if ($argc > 1)
    mb602seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602sestmmxusb
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux (STi5202 in 32-bit SE mode)
Usage: mb602sestmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602seucstmmxusb
  if ($argc > 1)
    mb602seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602seucstmmxusb
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602seucstmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb602se29stmmxusb
  if ($argc > 1)
    mb602se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb602se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb602stmmx_setup"
  end
end

document mb602se29stmmxusb
Connect to and configure an STi5202-Mboard board via an ST MultiCore/Mux (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602se29stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb602sim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb602_fsim_setup mb602sim_setup $arg0
  else
    connectsh4simle mb602_fsim_setup mb602sim_setup ""
  end
end

document mb602sim
Connect to and configure a simulated STi5202-Mboard board
Usage: mb602sim
end

define mb602simse
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb602se_fsim_setup mb602simse_setup $arg0
  else
    connectsh4simle mb602se_fsim_setup mb602simse_setup ""
  end
end

document mb602simse
Connect to and configure a simulated STi5202-Mboard board (STi5202 in 32-bit SE mode)
Usage: mb602simse
end

define mb602simseuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb602se_fsim_setup mb602simseuc_setup $arg0
  else
    connectsh4simle mb602se_fsim_setup mb602simseuc_setup ""
  end
end

document mb602simseuc
Connect to and configure a simulated STi5202-Mboard board (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602simseuc
end

define mb602simse29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb602se_fsim_setup mb602simse29_setup $arg0
  else
    connectsh4simle mb602se_fsim_setup mb602simse29_setup ""
  end
end

document mb602simse29
Connect to and configure a simulated STi5202-Mboard board (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602simse29
end

################################################################################

define mb602psim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb602_psim_setup mb602sim_setup $arg0
  else
    connectsh4psimle mb602_psim_setup mb602sim_setup ""
  end
end

document mb602psim
Connect to and configure a simulated STi5202-Mboard board
Usage: mb602psim
end

define mb602psimse
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb602se_psim_setup mb602simse_setup $arg0
  else
    connectsh4psimle mb602se_psim_setup mb602simse_setup ""
  end
end

document mb602psimse
Connect to and configure a simulated STi5202-Mboard board (STi5202 in 32-bit SE mode)
Usage: mb602psimse
end

define mb602psimseuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb602se_psim_setup mb602simseuc_setup $arg0
  else
    connectsh4psimle mb602se_psim_setup mb602simseuc_setup ""
  end
end

document mb602psimseuc
Connect to and configure a simulated STi5202-Mboard board (STi5202 in 32-bit SE mode with uncached mappings)
Usage: mb602psimseuc
end

define mb602psimse29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source sti5202.cmd
  source mb602.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb602se_psim_setup mb602simse29_setup $arg0
  else
    connectsh4psimle mb602se_psim_setup mb602simse29_setup ""
  end
end

document mb602psimse29
Connect to and configure a simulated STi5202-Mboard board (STi5202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb602psimse29
end

################################################################################
