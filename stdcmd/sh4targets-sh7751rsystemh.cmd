################################################################################

define sh7751rsystemhbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh7751rsystemh_setup $arg1
  else
    connectsh4be $arg0 sh7751rsystemh_setup ""
  end
end

document sh7751rsystemhbe
Connect to and configure an SH7751R Solution Engine for SystemH board (big endian)
Usage: sh7751rsystemhbe <target>
where <target> is an ST Micro Connect name or IP address
end

define sh7751rsystemhle
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh7751rsystemh_setup $arg1
  else
    connectsh4le $arg0 sh7751rsystemh_setup ""
  end
end

document sh7751rsystemhle
Connect to and configure an SH7751R Solution Engine for SystemH board (little endian)
Usage: sh7751rsystemhle <target>
where <target> is an ST Micro Connect name or IP address
end

define sh7751rsystemh
  if ($argc > 1)
    sh7751rsystemhle $arg0 $arg1
  else
    sh7751rsystemhle $arg0
  end
end

document sh7751rsystemh
Connect to and configure an SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemh <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh7751rsystemhusbbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh7751rsystemh_setup $arg1
  else
    connectsh4usbbe $arg0 sh7751rsystemh_setup ""
  end
end

document sh7751rsystemhusbbe
Connect to and configure an SH7751R Solution Engine for SystemH board (big endian)
Usage: sh7751rsystemhusbbe <target>
where <target> is an ST Micro Connect USB name
end

define sh7751rsystemhusble
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh7751rsystemh_setup $arg1
  else
    connectsh4usble $arg0 sh7751rsystemh_setup ""
  end
end

document sh7751rsystemhusble
Connect to and configure an SH7751R Solution Engine for SystemH board (little endian)
Usage: sh7751rsystemhusble <target>
where <target> is an ST Micro Connect USB name
end

define sh7751rsystemhusb
  if ($argc > 1)
    sh7751rsystemhusble $arg0 $arg1
  else
    sh7751rsystemhusble $arg0
  end
end

document sh7751rsystemhusb
Connect to and configure an SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemhusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh7751rsystemhsimbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh7751rsystemh_fsim_setup sh7751rsystemhsim_setup $arg0
  else
    connectsh4simbe sh7751rsystemh_fsim_setup sh7751rsystemhsim_setup ""
  end
end

document sh7751rsystemhsimbe
Connect to and configure a simulated SH7751R Solution Engine for SystemH board (big endian)
Usage: sh7751rsystemhsimbe
end

define sh7751rsystemhsimle
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh7751rsystemh_fsim_setup sh7751rsystemhsim_setup $arg0
  else
    connectsh4simle sh7751rsystemh_fsim_setup sh7751rsystemhsim_setup ""
  end
end

document sh7751rsystemhsimle
Connect to and configure a simulated SH7751R Solution Engine for SystemH board (little endian)
Usage: sh7751rsystemhsimle
end

define sh7751rsystemhsim
  if ($argc > 0)
    sh7751rsystemhsimle $arg0
  else
    sh7751rsystemhsimle
  end
end

document sh7751rsystemhsim
Connect to and configure a simulated SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemhsim
end

################################################################################

define sh7751rsystemhpsimbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh7751rsystemh_psim_setup sh7751rsystemhsim_setup $arg0
  else
    connectsh4psimbe sh7751rsystemh_psim_setup sh7751rsystemhsim_setup ""
  end
end

document sh7751rsystemhpsimbe
Connect to and configure a simulated SH7751R Solution Engine for SystemH board (big endian)
Usage: sh7751rsystemhpsimbe
end

define sh7751rsystemhpsimle
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751rsystemh.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh7751rsystemh_psim_setup sh7751rsystemhsim_setup $arg0
  else
    connectsh4psimle sh7751rsystemh_psim_setup sh7751rsystemhsim_setup ""
  end
end

document sh7751rsystemhpsimle
Connect to and configure a simulated SH7751R Solution Engine for SystemH board (little endian)
Usage: sh7751rsystemhpsimle
end

define sh7751rsystemhpsim
  if ($argc > 0)
    sh7751rsystemhpsimle $arg0
  else
    sh7751rsystemhpsimle
  end
end

document sh7751rsystemhpsim
Connect to and configure a simulated SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemhpsim
end

################################################################################
