##
## SH4-202 device description (on-chip memory and registers)
##

define sh4202_memory_define
  ## Configuration registers in P4
  memory-add STOREQ_data   0xe0000000 64 DEV
  memory-add ICACHE_addr   0xf0000000 16 DEV
  memory-add ICACHE_data   0xf1000000 16 DEV
  memory-add ITLB_addr     0xf2000000 16 DEV
  memory-add ITLB_data     0xf3000000 16 DEV
  memory-add OCACHE_addr   0xf4000000 16 DEV
  memory-add OCACHE_data   0xf5000000 16 DEV
  memory-add UTLB_PMB_addr 0xf6000000 16 DEV
  memory-add UTLB_PMB_data 0xf7000000 16 DEV
  memory-add CORE_regs     0xfc000000 64 DEV
end

define sh4202_define
  ## processor SH4
  ## chip SH4202

  sh4202_memory_define
end

define sh4202_fsim_core_setup
  sim_command "config +cpu.sh4_family=fpu"
  sim_command "config +cpu.sh7751_features=false"
  sim_command "config +cpu.icache.nr_partitions=2"
  sim_command "config +cpu.dcache.nr_partitions=2"
  sim_reset
end

define sh4202_psim_core_setup
  sim_command "config +cpu.cache_store_on_fill=true"
  sim_command "config +use_tobu=false"
  sh4202_fsim_core_setup
end
