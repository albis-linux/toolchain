################################################################################

source sh4targets-mb411stb7100.cmd

################################################################################

define mb411stb7100bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100bypass $arg0 $arg1
  else
    mb411stb7100bypass $arg0
  end
end

define mb411stb7100stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100stmmx $arg0 $arg1
  else
    mb411stb7100stmmx $arg0
  end
end

################################################################################

define mb411bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411bypass $arg0 $arg1
  else
    mb411bypass $arg0
  end
end

define mb411stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stmmx $arg0 $arg1
  else
    mb411stmmx $arg0
  end
end

################################################################################

define mb411stb7100bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100bypass $arg0 $arg1
  else
    mb411stb7100bypass $arg0
  end
end

define mb411stb7100stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100stmmx $arg0 $arg1
  else
    mb411stb7100stmmx $arg0
  end
end

################################################################################

define mb411bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411bypass $arg0 $arg1
  else
    mb411bypass $arg0
  end
end

define mb411stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stmmx $arg0 $arg1
  else
    mb411stmmx $arg0
  end
end

################################################################################

source sh4targets-mb411stb7100cutX.cmd

################################################################################

define mb411stb7100cut11bypass
  if ($argc > 1)
    mb411stb7100cutXbypass 11 $arg0 $arg1
  else
    mb411stb7100cutXbypass 11 $arg0
  end
end

define mb411stb7100cut11stmmx
  if ($argc > 1)
    mb411stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 11 $arg0
  end
end

define mb411stb7100cut13bypass
  if ($argc > 1)
    mb411stb7100cutXbypass 13 $arg0 $arg1
  else
    mb411stb7100cutXbypass 13 $arg0
  end
end

define mb411stb7100cut13stmmx
  if ($argc > 1)
    mb411stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 13 $arg0
  end
end

define mb411stb7100cut20bypass
  if ($argc > 1)
    mb411stb7100cutXbypass 20 $arg0 $arg1
  else
    mb411stb7100cutXbypass 20 $arg0
  end
end

define mb411stb7100cut20stmmx
  if ($argc > 1)
    mb411stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 20 $arg0
  end
end

define mb411stb7100cut31bypass
  if ($argc > 1)
    mb411stb7100cutXbypass 31 $arg0 $arg1
  else
    mb411stb7100cutXbypass 31 $arg0
  end
end

define mb411stb7100cut31stmmx
  if ($argc > 1)
    mb411stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb411cut11bypass
  if ($argc > 1)
    mb411cutXbypass 11 $arg0 $arg1
  else
    mb411cutXbypass 11 $arg0
  end
end

define mb411cut11stmmx
  if ($argc > 1)
    mb411cutXstmmx 11 $arg0 $arg1
  else
    mb411cutXstmmx 11 $arg0
  end
end

define mb411cut13bypass
  if ($argc > 1)
    mb411cutXbypass 13 $arg0 $arg1
  else
    mb411cutXbypass 13 $arg0
  end
end

define mb411cut13stmmx
  if ($argc > 1)
    mb411cutXstmmx 13 $arg0 $arg1
  else
    mb411cutXstmmx 13 $arg0
  end
end

define mb411cut20bypass
  if ($argc > 1)
    mb411cutXbypass 20 $arg0 $arg1
  else
    mb411cutXbypass 20 $arg0
  end
end

define mb411cut20stmmx
  if ($argc > 1)
    mb411cutXstmmx 20 $arg0 $arg1
  else
    mb411cutXstmmx 20 $arg0
  end
end

define mb411cut31bypass
  if ($argc > 1)
    mb411cutXbypass 31 $arg0 $arg1
  else
    mb411cutXbypass 31 $arg0
  end
end

define mb411cut31stmmx
  if ($argc > 1)
    mb411cutXstmmx 31 $arg0 $arg1
  else
    mb411cutXstmmx 31 $arg0
  end
end

################################################################################

define mb411stb7100cut11bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXbypass 11 $arg0 $arg1
  else
    mb411stb7100cutXbypass 11 $arg0
  end
end

define mb411stb7100cut11stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 11 $arg0
  end
end

define mb411stb7100cut13bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXbypass 13 $arg0 $arg1
  else
    mb411stb7100cutXbypass 13 $arg0
  end
end

define mb411stb7100cut13stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 13 $arg0
  end
end

define mb411stb7100cut20bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXbypass 20 $arg0 $arg1
  else
    mb411stb7100cutXbypass 20 $arg0
  end
end

define mb411stb7100cut20stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 20 $arg0
  end
end

define mb411stb7100cut31bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXbypass 31 $arg0 $arg1
  else
    mb411stb7100cutXbypass 31 $arg0
  end
end

define mb411stb7100cut31stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb411cut11bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXbypass 11 $arg0 $arg1
  else
    mb411cutXbypass 11 $arg0
  end
end

define mb411cut11stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXstmmx 11 $arg0 $arg1
  else
    mb411cutXstmmx 11 $arg0
  end
end

define mb411cut13bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXbypass 13 $arg0 $arg1
  else
    mb411cutXbypass 13 $arg0
  end
end

define mb411cut13stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXstmmx 13 $arg0 $arg1
  else
    mb411cutXstmmx 13 $arg0
  end
end

define mb411cut20bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXbypass 20 $arg0 $arg1
  else
    mb411cutXbypass 20 $arg0
  end
end

define mb411cut20stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXstmmx 20 $arg0 $arg1
  else
    mb411cutXstmmx 20 $arg0
  end
end

define mb411cut31bypass27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXbypass 31 $arg0 $arg1
  else
    mb411cutXbypass 31 $arg0
  end
end

define mb411cut31stmmx27MHz
  set $_mb411stb7100extclk = 27
  if ($argc > 1)
    mb411cutXstmmx 31 $arg0 $arg1
  else
    mb411cutXstmmx 31 $arg0
  end
end

################################################################################

define mb411stb7100cut11bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXbypass 11 $arg0 $arg1
  else
    mb411stb7100cutXbypass 11 $arg0
  end
end

define mb411stb7100cut11stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 11 $arg0
  end
end

define mb411stb7100cut13bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXbypass 13 $arg0 $arg1
  else
    mb411stb7100cutXbypass 13 $arg0
  end
end

define mb411stb7100cut13stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 13 $arg0
  end
end

define mb411stb7100cut20bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXbypass 20 $arg0 $arg1
  else
    mb411stb7100cutXbypass 20 $arg0
  end
end

define mb411stb7100cut20stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 20 $arg0
  end
end

define mb411stb7100cut31bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXbypass 31 $arg0 $arg1
  else
    mb411stb7100cutXbypass 31 $arg0
  end
end

define mb411stb7100cut31stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb411stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb411cut11bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXbypass 11 $arg0 $arg1
  else
    mb411cutXbypass 11 $arg0
  end
end

define mb411cut11stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXstmmx 11 $arg0 $arg1
  else
    mb411cutXstmmx 11 $arg0
  end
end

define mb411cut13bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXbypass 13 $arg0 $arg1
  else
    mb411cutXbypass 13 $arg0
  end
end

define mb411cut13stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXstmmx 13 $arg0 $arg1
  else
    mb411cutXstmmx 13 $arg0
  end
end

define mb411cut20bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXbypass 20 $arg0 $arg1
  else
    mb411cutXbypass 20 $arg0
  end
end

define mb411cut20stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXstmmx 20 $arg0 $arg1
  else
    mb411cutXstmmx 20 $arg0
  end
end

define mb411cut31bypass30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXbypass 31 $arg0 $arg1
  else
    mb411cutXbypass 31 $arg0
  end
end

define mb411cut31stmmx30MHz
  set $_mb411stb7100extclk = 30
  if ($argc > 1)
    mb411cutXstmmx 31 $arg0 $arg1
  else
    mb411cutXstmmx 31 $arg0
  end
end

################################################################################
