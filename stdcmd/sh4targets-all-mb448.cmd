################################################################################

source sh4targets-mb448.cmd

################################################################################

define mb448bypass27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448bypass $arg0 $arg1
  else
    mb448bypass $arg0
  end
end

define mb448stmmx27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448stmmx $arg0 $arg1
  else
    mb448stmmx $arg0
  end
end

################################################################################

define mb448bypass30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448bypass $arg0 $arg1
  else
    mb448bypass $arg0
  end
end

define mb448stmmx30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448stmmx $arg0 $arg1
  else
    mb448stmmx $arg0
  end
end

################################################################################

source sh4targets-mb448cutX.cmd

################################################################################

define mb448cut11bypass
  if ($argc > 1)
    mb448cutXbypass 11 $arg0 $arg1
  else
    mb448cutXbypass 11 $arg0
  end
end

define mb448cut11stmmx
  if ($argc > 1)
    mb448cutXstmmx 11 $arg0 $arg1
  else
    mb448cutXstmmx 11 $arg0
  end
end

define mb448cut20bypass
  if ($argc > 1)
    mb448cutXbypass 20 $arg0 $arg1
  else
    mb448cutXbypass 20 $arg0
  end
end

define mb448cut20stmmx
  if ($argc > 1)
    mb448cutXstmmx 20 $arg0 $arg1
  else
    mb448cutXstmmx 20 $arg0
  end
end

define mb448cut30bypass
  if ($argc > 1)
    mb448cutXbypass 30 $arg0 $arg1
  else
    mb448cutXbypass 30 $arg0
  end
end

define mb448cut30stmmx
  if ($argc > 1)
    mb448cutXstmmx 30 $arg0 $arg1
  else
    mb448cutXstmmx 30 $arg0
  end
end

################################################################################

define mb448cut11bypass27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448cutXbypass 11 $arg0 $arg1
  else
    mb448cutXbypass 11 $arg0
  end
end

define mb448cut11stmmx27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448cutXstmmx 11 $arg0 $arg1
  else
    mb448cutXstmmx 11 $arg0
  end
end

define mb448cut20bypass27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448cutXbypass 20 $arg0 $arg1
  else
    mb448cutXbypass 20 $arg0
  end
end

define mb448cut20stmmx27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448cutXstmmx 20 $arg0 $arg1
  else
    mb448cutXstmmx 20 $arg0
  end
end

define mb448cut30bypass27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448cutXbypass 30 $arg0 $arg1
  else
    mb448cutXbypass 30 $arg0
  end
end

define mb448cut30stmmx27MHz
  set $_mb448extclk = 27
  if ($argc > 1)
    mb448cutXstmmx 30 $arg0 $arg1
  else
    mb448cutXstmmx 30 $arg0
  end
end

################################################################################

define mb448cut11bypass30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448cutXbypass 11 $arg0 $arg1
  else
    mb448cutXbypass 11 $arg0
  end
end

define mb448cut11stmmx30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448cutXstmmx 11 $arg0 $arg1
  else
    mb448cutXstmmx 11 $arg0
  end
end

define mb448cut20bypass30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448cutXbypass 20 $arg0 $arg1
  else
    mb448cutXbypass 20 $arg0
  end
end

define mb448cut20stmmx30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448cutXstmmx 20 $arg0 $arg1
  else
    mb448cutXstmmx 20 $arg0
  end
end

define mb448cut30bypass30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448cutXbypass 30 $arg0 $arg1
  else
    mb448cutXbypass 30 $arg0
  end
end

define mb448cut30stmmx30MHz
  set $_mb448extclk = 30
  if ($argc > 1)
    mb448cutXstmmx 30 $arg0 $arg1
  else
    mb448cutXstmmx 30 $arg0
  end
end

################################################################################
