################################################################################

define hdk5289sti5206sim
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5206_fsim_setup hdk5289sti5206sim_setup $arg0
  else
    connectsh4simle hdk5289sti5206_fsim_setup hdk5289sti5206sim_setup ""
  end
end

document hdk5289sti5206sim
Connect to and configure a simulated STi5206-HDK board
Usage: hdk5289sti5206sim
end

define hdk5289sti5206simse
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5206se_fsim_setup hdk5289sti5206simse_setup $arg0
  else
    connectsh4simle hdk5289sti5206se_fsim_setup hdk5289sti5206simse_setup ""
  end
end

document hdk5289sti5206simse
Connect to and configure a simulated STi5206-HDK board (STi5206 in 32-bit SE mode)
Usage: hdk5289sti5206simse
end

define hdk5289sti5206simseuc
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5206se_fsim_setup hdk5289sti5206simseuc_setup $arg0
  else
    connectsh4simle hdk5289sti5206se_fsim_setup hdk5289sti5206simseuc_setup ""
  end
end

document hdk5289sti5206simseuc
Connect to and configure a simulated STi5206-HDK board (STi5206 in 32-bit SE mode with uncached mappings)
Usage: hdk5289sti5206simseuc
end

define hdk5289sti5206simse29
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5289sti5206se_fsim_setup hdk5289sti5206simse29_setup $arg0
  else
    connectsh4simle hdk5289sti5206se_fsim_setup hdk5289sti5206simse29_setup ""
  end
end

document hdk5289sti5206simse29
Connect to and configure a simulated STi5206-HDK board (STi5206 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk5289sti5206simse29
end

################################################################################

define hdk5289sti5206psim
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5206_psim_setup hdk5289sti5206sim_setup $arg0
  else
    connectsh4psimle hdk5289sti5206_psim_setup hdk5289sti5206sim_setup ""
  end
end

document hdk5289sti5206psim
Connect to and configure a simulated STi5206-HDK board
Usage: hdk5289sti5206psim
end

define hdk5289sti5206psimse
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5206se_psim_setup hdk5289sti5206simse_setup $arg0
  else
    connectsh4psimle hdk5289sti5206se_psim_setup hdk5289sti5206simse_setup ""
  end
end

document hdk5289sti5206psimse
Connect to and configure a simulated STi5206-HDK board (STi5206 in 32-bit SE mode)
Usage: hdk5289sti5206psimse
end

define hdk5289sti5206psimseuc
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5206se_psim_setup hdk5289sti5206simseuc_setup $arg0
  else
    connectsh4psimle hdk5289sti5206se_psim_setup hdk5289sti5206simseuc_setup ""
  end
end

document hdk5289sti5206psimseuc
Connect to and configure a simulated STi5206-HDK board (STi5206 in 32-bit SE mode with uncached mappings)
Usage: hdk5289sti5206psimseuc
end

define hdk5289sti5206psimse29
  source register40.cmd
  source display40.cmd
  source sti5206.cmd
  source hdk5289sti5206.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5289sti5206se_psim_setup hdk5289sti5206simse29_setup $arg0
  else
    connectsh4psimle hdk5289sti5206se_psim_setup hdk5289sti5206simse29_setup ""
  end
end

document hdk5289sti5206psimse29
Connect to and configure a simulated STi5206-HDK board (STi5206 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk5289sti5206psimse29
end

################################################################################
