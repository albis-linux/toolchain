##------------------------------------------------------------------------------
## hms1stb7100cut31.cmd - Simple Devices HMS1
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

init-if-undefined $_hms1stb7100extclk = 27
keep-variable $_hms1stb7100extclk

init-if-undefined $_hms1stb7100sys128 = 0
keep-variable $_hms1stb7100sys128

##{{{  HMS1 (STb7100) CLOCKGEN Configuration
define hms1stb7100_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  linkspeed 1.25MHz

  if ($_hms1stb7100extclk == 30)
    ## Set PLL0 to 531MHz
    stb7100_set_clockgen_a_pll0 0x14 0xb1 0x0
    ## Set PLL1 to 400MHz
    stb7100_set_clockgen_a_pll1 0x1e 0xc8 0x0
  else
  if ($_hms1stb7100extclk == 27)
    ## Set PLL0 to 531MHz
    stb7100_set_clockgen_a_pll0 0x06 0x3b 0x0
    ## Set PLL1 to 400MHz
    stb7100_set_clockgen_a_pll1 0x1b 0xc8 0x0
  else
    ## Unsupported external clock frequency
  end
  end

  ## Restore default link speed (5MHz maximum for TapMux)
  if ($_stmmxmode)
    linkspeed 5MHz
  else
    linkspeed 10MHz
  end
end
##}}}

##{{{  HMS1 (STb7100) SYSCONF Configuration
define hms1stb7100_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  set *$SYSCONF_SYS_CFG11 = 0x080780c0

  while ((*$SYSCONF_SYS_STA12 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  while ((*$SYSCONF_SYS_STA13 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  set *$SYSCONF_SYS_CFG12 = 0x4000000f | (0xf << 12) | (0xf << 23)
  set *$SYSCONF_SYS_CFG13 = 0x4000000f | (0xf << 12) | (0xf << 23)
end
##}}}

##{{{  HMS1 (STb7100) EMI Configuration
define hms1stb7100_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000a
  set *$EMI_BANK4_BASEADDRESS = 0x0000000c

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016d1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - SMSC 9117 on HMS1
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for SMSC9118 STEM card 16Mb"

##  set *$EMI_BANK1_EMICONFIGDATA0 = 0x041086f1
##  set *$EMI_BANK1_EMICONFIGDATA1 = 0x0e024400
##  set *$EMI_BANK1_EMICONFIGDATA2 = 0x0e024400
##  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x041086f1
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x93001111
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x91001111
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000


##------------------------------------------------------------------------------
## Bank 2 - DVBCI
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for DVBCI 8Mb"

##  set *$EMI_BANK2_EMICONFIGDATA0 = 0x002046f9
##  set *$EMI_BANK2_EMICONFIGDATA1 = 0xa5a00000
##  set *$EMI_BANK2_EMICONFIGDATA2 = 0xa5a20000
##  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - ATAPI
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for ATAPI 8Mb"

  set *$EMI_BANK3_EMICONFIGDATA0 = 0x00200791
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - EPLD Registers and LAN91C111
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for EPLD Registers and LAN91C111 16Mb"

##  set *$EMI_BANK4_EMICONFIGDATA0 = 0x042086f1
##  set *$EMI_BANK4_EMICONFIGDATA1 = 0x88112111
##  set *$EMI_BANK4_EMICONFIGDATA2 = 0x88112211
##  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000050
end
##}}}

##{{{  HMS1 (STb7100) LMI Configuration
define hms1stb7100_lmisys_configure
  ## _ST_display (_procname) "Configuring LMI-SYS for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMISYS_MIM_0 = 0x861a0247
  set *$LMISYS_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMISYS_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMISYS_SDRA0_0 = $_hms1stb7100sys128 ? 0x0c001a00 : 0x08001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMISYS_SDRA1_0 = $_hms1stb7100sys128 ? 0x0c001a00 : 0x08001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000003
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SDMR0 = 0x00000402
  set *$LMISYS_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SDMR0 = 0x00000033
  set *$LMISYS_SCR_0 = 0x00000000
end

define hms1stb7100_lmivid_configure
  ## _ST_display (_procname) "Configuring LMI-VID for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMIVID_MIM_0 = 0x861a0247
  set *$LMIVID_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMIVID_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMIVID_SDRA0_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMIVID_SDRA1_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000003
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SDMR0 = 0x00000402
  set *$LMIVID_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SDMR0 = 0x00000033
  set *$LMIVID_SCR_0 = 0x00000000
end
##}}}

##{{{  HMS1 (STb7100) Memory
define hms1stb7100_memory_define
  memory-add Flash        0x00000000 8 ROM
  if ($_hms1stb7100sys128)
  memory-add LMISYS_SDRAM 0x04000000 128 RAM
  else
  memory-add LMISYS_SDRAM 0x04000000 64 RAM
  end
  memory-add LMIVID_SDRAM 0x10000000 64 RAM
end

define hms1stb7100_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  if ($_hms1stb7100sys128)
  sim_addmemory 0x04000000 128 RAM
  else
  sim_addmemory 0x04000000 64 RAM
  end
  sim_addmemory 0x10000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  HMS1 (STb7100) Bypass Configuration
define hms1stb7100bypass_setup
  if ($argc > 0)
  stb7100_bypass_setup $arg0
  else
  stb7100_bypass_setup
  end
end

define hms1stb7100bypass_setup_attach
  if ($argc > 0)
  stb7100_bypass_setup_attach $arg0
  else
  stb7100_bypass_setup_attach
  end
end
##}}}

##{{{  HMS1 (STb7100) STMMX Configuration
define hms1stb7100stmmx_setup
  if ($argc > 0)
  stb7100_stmmx_setup $arg0
  else
  stb7100_stmmx_setup
  end
end
##}}}

define hms1stb7100_setup
  stb7100_define
  hms1stb7100_memory_define

  printf "Setup for STi7100 cut version 3.1\n"

  stb7100_si_regs

  hms1stb7100_clockgen_configure
  hms1stb7100_sysconf_configure
  hms1stb7100_emi_configure
  hms1stb7100_lmisys_configure
  hms1stb7100_lmivid_configure

  set *$CCN_CCR = 0x8000090d
end

document hms1stb7100_setup
Configure an STb7100-Mboard board
Usage: hms1stb7100_setup
end

define hms1stb7100sim_setup
  stb7100_define
  hms1stb7100_memory_define

  stb7100_si_regs

  set *$CCN_CCR = 0x8000090d
end

document hms1stb7100sim_setup
Configure a simulated STb7100-Mboard board
Usage: hms1stb7100sim_setup
end

define hms1stb7100_fsim_setup
  stb7100_fsim_core_setup
  hms1stb7100_sim_memory_define
end

document hms1stb7100_fsim_setup
Configure functional simulator for STb7100-Mboard board
Usage: hms1stb7100_fsim_setup
end

define hms1stb7100_psim_setup
  stb7100_psim_core_setup
  hms1stb7100_sim_memory_define
end

document hms1stb7100_psim_setup
Configure performance simulator for STb7100-Mboard board
Usage: hms1stb7100_psim_setup
end

define hms1stb7100_display_registers
  stb7100_display_si_regs
end

document hms1stb7100_display_registers
Display the STb7100 configuration registers
Usage: hms1stb7100_display_registers
end

