set $targetpack_enabled = 0
keep-variable $targetpack_enabled

define targetpack
  if (!$targetpack_enabled)
    printf "TargetPack plugin not installed\n"
  else
    if ($argc == 0)
      callplugin targetpack
    end
    if ($argc == 1)
      callplugin targetpack $arg0
    end
    if ($argc == 2)
      callplugin targetpack $arg0 $arg1
    end
    if ($argc > 2)
      callplugin targetpack help
    end
  end
end

document targetpack
Use "targetpack help"
end

define enable_targetpack
  if (!$targetpack_enabled)
    installplugin targetpack libsh4targetpack.so libstmc1.so
    set $targetpack_enabled = 1
  else
    printf "TargetPack plugin already enabled\n"
  end
end

document enable_targetpack
Enable the TargetPack plugin
Usage: enable_targetpack
end
