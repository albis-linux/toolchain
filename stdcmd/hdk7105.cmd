##------------------------------------------------------------------------------
## hdk7105.cmd - STi7105-HDK Reference Platform
##------------------------------------------------------------------------------

##{{{  HDK7105 PMB Configuration
define hdk7105se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk7105seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk7105se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512
  sh4_set_pmb 1 0xa0 0x40 512 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  HDK7105 Memory
define hdk7105_memory_define
  memory-add Flash     0x00000000 128 ROM
  memory-add LMI_SDRAM 0x0c000000 256 RAM
end

define hdk7105_sim_memory_define
  sim_addmemory 0x00000000 128 ROM
  sim_addmemory 0x0c000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define hdk7105se_memory_define
  memory-add Flash     0x00000000 128 ROM
  memory-add LMI_SDRAM 0x40000000 512 RAM
end

define hdk7105se_sim_memory_define
  sim_addmemory 0x00000000 128 ROM
  sim_addmemory 0x40000000 512 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define hdk7105sim_setup
  sti7105_define
  hdk7105_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document hdk7105sim_setup
Configure a simulated STi7105-HDK board
Usage: hdk7105sim_setup
end

define hdk7105simse_setup
  sti7105_define
  hdk7105se_memory_define

  st40300_core_si_regs

  hdk7105se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk7105simse_setup
Configure a simulated STi7105-HDK board with the STi7105 in 32-bit SE mode
Usage: hdk7105simse_setup
end

define hdk7105simseuc_setup
  sti7105_define
  hdk7105se_memory_define

  st40300_core_si_regs

  hdk7105seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk7105simseuc_setup
Configure a simulated STi7105-HDK board with the STi7105 in 32-bit SE mode with uncached RAM mappings
Usage: hdk7105simseuc_setup
end

define hdk7105simse29_setup
  sti7105_define
  hdk7105se_memory_define

  st40300_core_si_regs

  hdk7105se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk7105simse29_setup
Configure a simulated STi7105-HDK board with the STi7105 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: hdk7105simse29_setup
end

define hdk7105_fsim_setup
  sti7105_fsim_core_setup
  hdk7105_sim_memory_define
end

document hdk7105_fsim_setup
Configure functional simulator for STi7105-HDK board
Usage: hdk7105_fsim_setup
end

define hdk7105se_fsim_setup
  sti7105_fsim_core_setup
  hdk7105se_sim_memory_define
end

document hdk7105se_fsim_setup
Configure functional simulator for STi7105-HDK board with the STi7105 in 32-bit SE mode
Usage: hdk7105se_fsim_setup
end

define hdk7105_psim_setup
  sti7105_psim_core_setup
  hdk7105_sim_memory_define
end

document hdk7105_psim_setup
Configure performance simulator for STi7105-HDK board
Usage: hdk7105_psim_setup
end

define hdk7105se_psim_setup
  sti7105_psim_core_setup
  hdk7105se_sim_memory_define
end

document hdk7105se_psim_setup
Configure performance simulator for STi7105-HDK board with the STi7105 in 32-bit SE mode
Usage: hdk7105se_psim_setup
end

define hdk7105_display_registers
  st40300_display_core_si_regs
end

document hdk7105_display_registers
Display the STi7105 configuration registers
Usage: hdk7105_display_registers
end
