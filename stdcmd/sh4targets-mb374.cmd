################################################################################

define mb374
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb374.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb374_setup $arg1
  else
    connectsh4le $arg0 mb374_setup "hardreset"
  end
end

document mb374
Connect to and configure an ST40RA-Starter board
Usage: mb374 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb374usb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb374.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb374_setup $arg1
  else
    connectsh4usble $arg0 mb374_setup "hardreset"
  end
end

document mb374usb
Connect to and configure an ST40RA-Starter board
Usage: mb374usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb374sim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb374.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb374_fsim_setup mb374sim_setup $arg0
  else
    connectsh4simle mb374_fsim_setup mb374sim_setup ""
  end
end

document mb374sim
Connect to and configure a simulated ST40RA-Starter board
Usage: mb374sim
end

################################################################################

define mb374psim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb374.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb374_psim_setup mb374sim_setup $arg0
  else
    connectsh4psimle mb374_psim_setup mb374sim_setup ""
  end
end

document mb374psim
Connect to and configure a simulated ST40RA-Starter board
Usage: mb374psim
end

################################################################################
