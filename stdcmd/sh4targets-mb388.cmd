################################################################################

define mb388be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb388_setup $arg1
  else
    connectst40300be $arg0 mb388_setup "jtagpinout=st40 hardreset"
  end
end

document mb388be
Connect to and configure an ST40-300 FPGA board (big endian)
Usage: mb388be <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb388_setup $arg1
  else
    connectst40300le $arg0 mb388_setup "jtagpinout=st40 hardreset"
  end
end

document mb388le
Connect to and configure an ST40-300 FPGA board (little endian)
Usage: mb388le <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388
  if ($argc > 1)
    mb388le $arg0 $arg1
  else
    mb388le $arg0
  end
end

document mb388
Connect to and configure an ST40-300 FPGA board
Usage: mb388 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb388sebe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb388se_setup $arg1
  else
    connectst40300be $arg0 mb388se_setup "jtagpinout=st40 hardreset"
  end
end

document mb388sebe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb388sebe <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388sele
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb388se_setup $arg1
  else
    connectst40300le $arg0 mb388se_setup "jtagpinout=st40 hardreset"
  end
end

document mb388sele
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb388sele <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388se
  if ($argc > 1)
    mb388sele $arg0 $arg1
  else
    mb388sele $arg0
  end
end

document mb388se
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb388se <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb388seucbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb388seuc_setup $arg1
  else
    connectst40300be $arg0 mb388seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb388seucbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388seucbe <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388seucle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb388seuc_setup $arg1
  else
    connectst40300le $arg0 mb388seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb388seucle
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388seucle <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388seuc
  if ($argc > 1)
    mb388seucle $arg0 $arg1
  else
    mb388seucle $arg0
  end
end

document mb388seuc
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388seuc <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb388se29be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb388se29_setup $arg1
  else
    connectst40300be $arg0 mb388se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb388se29be
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388se29be <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388se29le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb388se29_setup $arg1
  else
    connectst40300le $arg0 mb388se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb388se29le
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388se29le <target>
where <target> is an ST Micro Connect name or IP address
end

define mb388se29
  if ($argc > 1)
    mb388se29le $arg0 $arg1
  else
    mb388se29le $arg0
  end
end

document mb388se29
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388se29 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb388usbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb388_setup $arg1
  else
    connectst40300usbbe $arg0 mb388_setup "jtagpinout=st40 hardreset"
  end
end

document mb388usbbe
Connect to and configure an ST40-300 FPGA board (big endian)
Usage: mb388usbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb388usble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb388_setup $arg1
  else
    connectst40300usble $arg0 mb388_setup "jtagpinout=st40 hardreset"
  end
end

document mb388usble
Connect to and configure an ST40-300 FPGA board (little endian)
Usage: mb388usble <target>
where <target> is an ST Micro Connect USB name
end

define mb388usb
  if ($argc > 1)
    mb388usble $arg0 $arg1
  else
    mb388usble $arg0
  end
end

document mb388usb
Connect to and configure an ST40-300 FPGA board
Usage: mb388usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb388seusbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb388se_setup $arg1
  else
    connectst40300usbbe $arg0 mb388se_setup "jtagpinout=st40 hardreset"
  end
end

document mb388seusbbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb388seusbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb388seusble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb388se_setup $arg1
  else
    connectst40300usble $arg0 mb388se_setup "jtagpinout=st40 hardreset"
  end
end

document mb388seusble
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb388seusble <target>
where <target> is an ST Micro Connect USB name
end

define mb388seusb
  if ($argc > 1)
    mb388seusble $arg0 $arg1
  else
    mb388seusble $arg0
  end
end

document mb388seusb
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb388seusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb388seucusbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb388seuc_setup $arg1
  else
    connectst40300usbbe $arg0 mb388seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb388seucusbbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388seucusbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb388seucusble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb388seuc_setup $arg1
  else
    connectst40300usble $arg0 mb388seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb388seucusble
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388seucusble <target>
where <target> is an ST Micro Connect USB name
end

define mb388seucusb
  if ($argc > 1)
    mb388seucusble $arg0 $arg1
  else
    mb388seucusble $arg0
  end
end

document mb388seucusb
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388seucusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb388se29usbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb388se29_setup $arg1
  else
    connectst40300usbbe $arg0 mb388se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb388se29usbbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388se29usbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb388se29usble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb388se29_setup $arg1
  else
    connectst40300usble $arg0 mb388se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb388se29usble
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388se29usble <target>
where <target> is an ST Micro Connect USB name
end

define mb388se29usb
  if ($argc > 1)
    mb388se29usble $arg0 $arg1
  else
    mb388se29usble $arg0
  end
end

document mb388se29usb
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388se29usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb388simbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb388_fsim_setup mb388sim_setup $arg0
  else
    connectst40300simbe mb388_fsim_setup mb388sim_setup ""
  end
end

document mb388simbe
Connect to and configure a simulated ST40-300 FPGA board (big endian)
Usage: mb388simbe
end

define mb388simle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb388_fsim_setup mb388sim_setup $arg0
  else
    connectst40300simle mb388_fsim_setup mb388sim_setup ""
  end
end

document mb388simle
Connect to and configure a simulated ST40-300 FPGA board (little endian)
Usage: mb388simle
end

define mb388sim
  if ($argc > 0)
    mb388simle $arg0
  else
    mb388simle
  end
end

document mb388sim
Connect to and configure a simulated ST40-300 FPGA board
Usage: mb388sim
end

################################################################################

define mb388simsebe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb388_fsim_setup mb388simse_setup $arg0
  else
    connectst40300simbe mb388_fsim_setup mb388simse_setup ""
  end
end

document mb388simsebe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb388simsebe
end

define mb388simsele
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb388_fsim_setup mb388simse_setup $arg0
  else
    connectst40300simle mb388_fsim_setup mb388simse_setup ""
  end
end

document mb388simsele
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb388simsele
end

define mb388simse
  if ($argc > 0)
    mb388simsele $arg0
  else
    mb388simsele
  end
end

document mb388simse
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb388simse
end

################################################################################

define mb388simseucbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb388_fsim_setup mb388simseuc_setup $arg0
  else
    connectst40300simbe mb388_fsim_setup mb388simseuc_setup ""
  end
end

document mb388simseucbe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388simseucbe
end

define mb388simseucle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb388_fsim_setup mb388simseuc_setup $arg0
  else
    connectst40300simle mb388_fsim_setup mb388simseuc_setup ""
  end
end

document mb388simseucle
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388simseucle
end

define mb388simseuc
  if ($argc > 0)
    mb388simseucle $arg0
  else
    mb388simseucle
  end
end

document mb388simseuc
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388simseuc
end

################################################################################

define mb388simse29be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb388_fsim_setup mb388simse29_setup $arg0
  else
    connectst40300simbe mb388_fsim_setup mb388simse29_setup ""
  end
end

document mb388simse29be
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388simse29be
end

define mb388simse29le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb388_fsim_setup mb388simse29_setup $arg0
  else
    connectst40300simle mb388_fsim_setup mb388simse29_setup ""
  end
end

document mb388simse29le
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388simse29le
end

define mb388simse29
  if ($argc > 0)
    mb388simse29le $arg0
  else
    mb388simse29le
  end
end

document mb388simse29
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388simse29
end

################################################################################

define mb388psimbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb388_psim_setup mb388sim_setup $arg0
  else
    connectst40300psimbe mb388_psim_setup mb388sim_setup ""
  end
end

document mb388psimbe
Connect to and configure a simulated ST40-300 FPGA board (big endian)
Usage: mb388psimbe
end

define mb388psimle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb388_psim_setup mb388sim_setup $arg0
  else
    connectst40300psimle mb388_psim_setup mb388sim_setup ""
  end
end

document mb388psimle
Connect to and configure a simulated ST40-300 FPGA board (little endian)
Usage: mb388psimle
end

define mb388psim
  if ($argc > 0)
    mb388psimle $arg0
  else
    mb388psimle
  end
end

document mb388psim
Connect to and configure a simulated ST40-300 FPGA board
Usage: mb388psim
end

################################################################################

define mb388psimsebe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb388_psim_setup mb388simse_setup $arg0
  else
    connectst40300psimbe mb388_psim_setup mb388simse_setup ""
  end
end

document mb388psimsebe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb388psimsebe
end

define mb388psimsele
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb388_psim_setup mb388simse_setup $arg0
  else
    connectst40300psimle mb388_psim_setup mb388simse_setup ""
  end
end

document mb388psimsele
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb388psimsele
end

define mb388psimse
  if ($argc > 0)
    mb388psimsele $arg0
  else
    mb388psimsele
  end
end

document mb388psimse
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb388psimse
end

################################################################################

define mb388psimseucbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb388_psim_setup mb388simseuc_setup $arg0
  else
    connectst40300psimbe mb388_psim_setup mb388simseuc_setup ""
  end
end

document mb388psimseucbe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388psimseucbe
end

define mb388psimseucle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb388_psim_setup mb388simseuc_setup $arg0
  else
    connectst40300psimle mb388_psim_setup mb388simseuc_setup ""
  end
end

document mb388psimseucle
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388psimseucle
end

define mb388psimseuc
  if ($argc > 0)
    mb388psimseucle $arg0
  else
    mb388psimseucle
  end
end

document mb388psimseuc
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb388psimseuc
end

################################################################################

define mb388psimse29be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb388_psim_setup mb388simse29_setup $arg0
  else
    connectst40300psimbe mb388_psim_setup mb388simse29_setup ""
  end
end

document mb388psimse29be
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388psimse29be
end

define mb388psimse29le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb388.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb388_psim_setup mb388simse29_setup $arg0
  else
    connectst40300psimle mb388_psim_setup mb388simse29_setup ""
  end
end

document mb388psimse29le
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388psimse29le
end

define mb388psimse29
  if ($argc > 0)
    mb388psimse29le $arg0
  else
    mb388psimse29le
  end
end

document mb388psimse29
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb388psimse29
end

################################################################################
