################################################################################

define hdk5189sim
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5189_fsim_setup hdk5189sim_setup $arg0
  else
    connectsh4simle hdk5189_fsim_setup hdk5189sim_setup ""
  end
end

document hdk5189sim
Connect to and configure a simulated STi5189-HDK board
Usage: hdk5189sim
end

define hdk5189simse
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5189se_fsim_setup hdk5189simse_setup $arg0
  else
    connectsh4simle hdk5189se_fsim_setup hdk5189simse_setup ""
  end
end

document hdk5189simse
Connect to and configure a simulated STi5189-HDK board (STi5189 in 32-bit SE mode)
Usage: hdk5189simse
end

define hdk5189simseuc
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5189se_fsim_setup hdk5189simseuc_setup $arg0
  else
    connectsh4simle hdk5189se_fsim_setup hdk5189simseuc_setup ""
  end
end

document hdk5189simseuc
Connect to and configure a simulated STi5189-HDK board (STi5189 in 32-bit SE mode with uncached mappings)
Usage: hdk5189simseuc
end

define hdk5189simse29
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk5189se_fsim_setup hdk5189simse29_setup $arg0
  else
    connectsh4simle hdk5189se_fsim_setup hdk5189simse29_setup ""
  end
end

document hdk5189simse29
Connect to and configure a simulated STi5189-HDK board (STi5189 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk5189simse29
end

################################################################################

define hdk5189psim
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5189_psim_setup hdk5189sim_setup $arg0
  else
    connectsh4psimle hdk5189_psim_setup hdk5189sim_setup ""
  end
end

document hdk5189psim
Connect to and configure a simulated STi5189-HDK board
Usage: hdk5189psim
end

define hdk5189psimse
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5189se_psim_setup hdk5189simse_setup $arg0
  else
    connectsh4psimle hdk5189se_psim_setup hdk5189simse_setup ""
  end
end

document hdk5189psimse
Connect to and configure a simulated STi5189-HDK board (STi5189 in 32-bit SE mode)
Usage: hdk5189psimse
end

define hdk5189psimseuc
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5189se_psim_setup hdk5189simseuc_setup $arg0
  else
    connectsh4psimle hdk5189se_psim_setup hdk5189simseuc_setup ""
  end
end

document hdk5189psimseuc
Connect to and configure a simulated STi5189-HDK board (STi5189 in 32-bit SE mode with uncached mappings)
Usage: hdk5189psimseuc
end

define hdk5189psimse29
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source hdk5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk5189se_psim_setup hdk5189simse29_setup $arg0
  else
    connectsh4psimle hdk5189se_psim_setup hdk5189simse29_setup ""
  end
end

document hdk5189psimse29
Connect to and configure a simulated STi5189-HDK board (STi5189 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk5189psimse29
end

################################################################################
