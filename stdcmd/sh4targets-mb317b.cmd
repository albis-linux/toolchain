################################################################################

define mb317b
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb317b_setup $arg1
  else
    connectsh4le $arg0 mb317b_setup "hardreset"
  end
end

document mb317b
Connect to and configure an ST40GX1 Evaluation board, revision B
Usage: mb317b <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb317busb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb317b_setup $arg1
  else
    connectsh4usble $arg0 mb317b_setup "hardreset"
  end
end

document mb317busb
Connect to and configure an ST40GX1 Evaluation board, revision B
Usage: mb317busb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb317bsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb317b_fsim_setup mb317bsim_setup $arg0
  else
    connectsh4simle mb317b_fsim_setup mb317bsim_setup ""
  end
end

document mb317bsim
Connect to and configure a simulated ST40GX1 Evaluation board, revision B
Usage: mb317bsim
end

################################################################################

define mb317bpsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb317b_psim_setup mb317bsim_setup $arg0
  else
    connectsh4psimle mb317b_psim_setup mb317bsim_setup ""
  end
end

document mb317bpsim
Connect to and configure a simulated ST40GX1 Evaluation board, revision B
Usage: mb317bpsim
end

################################################################################
