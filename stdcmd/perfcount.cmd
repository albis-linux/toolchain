set $performance_counters_enabled = 0
keep-variable $performance_counters_enabled

define perfcount
  if (!$performance_counters_enabled)
    printf "Hardware performance counters plugin not installed\n"
  else
    if ($argc == 0)
      callplugin perf
    end
    if ($argc == 1)
      callplugin perf $arg0
    end
    if ($argc == 2)
      callplugin perf $arg0 $arg1
    end
    if ($argc == 3)
      callplugin perf $arg0 $arg1 $arg2
    end
    if ($argc == 4)
      callplugin perf $arg0 $arg1 $arg2 $arg3
    end
    if ($argc > 4)
      callplugin perf help
    end
  end
end

document perfcount
Use "perfcount help"
end

define enable_performance_counters
  if (!$performance_counters_enabled)
    installplugin perf libsh4perfcount.so
    set $performance_counters_enabled = 1
  else
    printf "Hardware performance counters plugin already enabled\n"
  end
end

document enable_performance_counters
Enable the hardware performance counters plugin
Usage: enable_performance_counters
end
