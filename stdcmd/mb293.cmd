##------------------------------------------------------------------------------
## mb293.cmd - ST40RA Software Development Boards MB293 and MB350
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB293 EMI Configuration
define mb293_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000c
  set *$EMI_BANK4_BASEADDRESS = 0x00000010
  set *$EMI_BANK5_BASEADDRESS = 0x00000018

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 2 - Hitachi Peripherals (MPX mode)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for HD664465 MPX device 16Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x040006db
  set *$EMI_BANK2_EMICONFIGDATA1 = 0x00000000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0x00000000
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - On-board SDRAM
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for on-board SDRAM 16Mb"

  ## Strobe on rising edge, bus release width 1 cycle, 1 sub-bank of 128Mb, column address width 8, port size 32 bit
  set *$EMI_BANK3_EMICONFIGDATA0 = 0x0000032a

  ## Page address mask
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x0001fffc

  ## Pre-charge time 2 cycles
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x00000020

  ## ModeSetDelay 3 cycles, RefreshTime 2 cycles, ActivtoRD 3 cycles, ActivtoWR 3 cycles, CAS Latency 3 cycles, 4 banks, WRRecovTime 1 cycle
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00002929

##------------------------------------------------------------------------------
## Bank 4 - PCMIA 0
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for PCMIA 0 32Mb (16 bit)"

  set *$EMI_BANK4_EMICONFIGDATA0 = 0x007017f1
  set *$EMI_BANK4_EMICONFIGDATA1 = 0xad224411
  set *$EMI_BANK4_EMICONFIGDATA2 = 0xbd224411
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 5 - PCMIA 1
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for PCMIA 1 31Mb (16 bit)"

  set *$EMI_BANK5_EMICONFIGDATA0 = 0x007017f1
  set *$EMI_BANK5_EMICONFIGDATA1 = 0xad224411
  set *$EMI_BANK5_EMICONFIGDATA2 = 0xbd224411
  set *$EMI_BANK5_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000008

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  ## _ST_display (_procname) "EMI SDRAM CLOCK @ 1/2 bus clock"
  set *$EMI_SDRAMCLKSEL = 0x00000001

  ## _ST_display (_procname) "EMI MPX CLOCK @ 1/2 bus clock"
  set *$EMI_MPXCLKSEL = 0x00000001

  set *$EMI_CLKENABLE = 0x00000001
  set *$EMI_SDRAMNOPGEN = 0x00000001
  set *$EMI_REFRESHINIT = 0x000003e8
  set *$EMI_SDRAMMODEREG = 0x00000032
  ## _ST_cfg_sleep 1000
  set *$EMI_SDRAMINIT = 0x00000001
end
##}}}

##{{{  MB293 LMI Configuration
define mb293_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for 64 bit SDRAM burst length 4"

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  ## NOTE:
  ##   bit 8 = 0 for SSTL (DDR), 1 for LVTTL (PC-SDRAM)
  ##   bit 9 = 0 for internal ref voltage, 1 for external ref voltage
  ##   bit 10 = 1 to by pass ECLK180 retime
  ##   bit 11 = 1 for PC-SDRAM, 0 for DDR notLMICOMP25_EN
  ##   bit 12 = 1 for PC-SDRAM, 0 for DDR LMICOMP33_EN
  ##   bits [13,14] = LVTTL drive strength for data & data strobe pads
  ##   bits [15,16] = LVTTL drive strength for address & control pads

  set *$SYSCONF_SYS_CON2_0 = 0x0000b900

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x01000281

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x0000126f

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0a001400

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0a001400

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00000190/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000190/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB293 Memory
define mb293_memory_define
  memory-add Flash               0x00000000 8 ROM
  memory-add EPLD_regs           0x00800000 4 RAM
  memory-add Mezzanine           0x00c00000 4 RAM
  memory-add Hitachi_Peripherals 0x02000000 16 RAM
  memory-add EMI_SDRAM           0x03000000 16 RAM
  memory-add PCMIA_0             0x04000000 32 RAM
  memory-add PCMIA_1             0x06000000 31 RAM
  memory-add LMI_SDRAM           0x08000000 32 RAM
end

define mb293_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x03000000 16 RAM
  sim_addmemory 0x08000000 32 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb293_setup
  st40ra_define
  mb293_memory_define

  st40ra_si_regs

  mb293_emi_configure
  mb293_lmi_configure

  set *$CCN_CCR = 0x0000090d
end

document mb293_setup
Configure an ST40RA HARP board
Usage: mb293_setup
end

define mb293sim_setup
  st40ra_define
  mb293_memory_define

  st40ra_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb293sim_setup
Configure a simulated ST40RA HARP board
Usage: mb293sim_setup
end

define mb293_fsim_setup
  st40ra_fsim_core_setup
  mb293_sim_memory_define
end

document mb293_fsim_setup
Configure functional simulator for ST40RA HARP board
Usage: mb293_fsim_setup
end

define mb293_psim_setup
  st40ra_psim_core_setup
  mb293_sim_memory_define
end

document mb293_psim_setup
Configure performance simulator for ST40RA HARP board
Usage: mb293_psim_setup
end

define mb293_display_registers
  st40ra_display_si_regs
end

document mb293_display_registers
Display the ST40RA configuration registers
Usage: mb293_display_registers
end

define mb350_setup
  mb293_setup
end

document mb350_setup
Configure an ST40RA Extended HARP board
Usage: mb350_setup
end

define mb350sim_setup
  mb293sim_setup
end

document mb350sim_setup
Configure a simulated ST40RA Extended HARP board
Usage: mb350sim_setup
end

define mb350_fsim_setup
  mb293_fsim_setup
end

document mb350_fsim_setup
Configure functional simulator for ST40RA Extended HARP board
Usage: mb350_fsim_setup
end

define mb350_psim_setup
  mb293_psim_setup
end

document mb350_psim_setup
Configure performance simulator for ST40RA Extended HARP board
Usage: mb350_psim_setup
end

define mb350_display_registers
  mb293_display_registers
end

document mb350_display_registers
Display the ST40RA configuration registers
Usage: mb350_display_registers
end
