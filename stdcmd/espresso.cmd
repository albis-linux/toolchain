##------------------------------------------------------------------------------
## espresso.cmd - STi5528 Espresso Reference Platform
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  Espresso SYSCONF Configuration
define espresso_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  set *$SYSCONF_SYS_CFG11 = 0x000000ff
  set *$SYSCONF_SYS_CFG12 = 0x00000005
  set *$SYSCONF_SYS_CFG13 = 0x00000000
end
##}}}

##{{{  Espresso EMI Configuration
define espresso_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Ensure EMI control registers are unlocked
##------------------------------------------------------------------------------

  set *$EMI_LOCK = 0x00000000

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000a
  set *$EMI_BANK4_BASEADDRESS = 0x0000000c
  set *$EMI_BANK5_BASEADDRESS = 0x0000000e

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 2 - STV0701
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for STV0701 8Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x021016f9
  set *$EMI_BANK2_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - STi4629
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for STi4629 8Mb"

  set *$EMI_BANK3_EMICONFIGDATA0 = 0x002046f9
  set *$EMI_BANK3_EMICONFIGDATA1 = 0xa5a00000
  set *$EMI_BANK3_EMICONFIGDATA2 = 0xa5a00000
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - EMI Connector
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for EMI Connector 8Mb"

  set *$EMI_BANK4_EMICONFIGDATA0 = 0x021016e9
  set *$EMI_BANK4_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK4_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 5 - LAN91C111
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for LAN91C111 8Mb"

  set *$EMI_BANK5_EMICONFIGDATA0 = 0x041086f1
  set *$EMI_BANK5_EMICONFIGDATA1 = 0x86001100
  set *$EMI_BANK5_EMICONFIGDATA2 = 0x86001100
  set *$EMI_BANK5_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  ## Pre cut 2.0
  ##set *$EMI_GENCFG = 0x00000680
  ## Post cut 2.0
  set *$EMI_GENCFG = 0x00000750

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  set *$EMI_CLKENABLE = 0x00000001
end
##}}}

##{{{  Espresso LMI Configuration
define espresso_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x05100243

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x00ee7c5a

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0c001900

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00002000/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00002000/sizeof(*$LMI_SDMR1))) = 0x0
  set *($LMI_SDMR0+(0x00000998/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000998/sizeof(*$LMI_SDMR1))) = 0x0
  ## _ST_cfg_sleep 1000
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *($LMI_SDMR0+(0x00000198/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000198/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  Espresso Memory
define espresso_memory_define
  memory-add Flash     0x00000000 16 ROM
  memory-add LMI_SDRAM 0x04000000 128 RAM
end

define espresso_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x04000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define espresso_setup
  sti5528_define
  espresso_memory_define

  sti5528_si_regs

  ## Unlock CLOCKGEN, bypass ST40 CPGL, EMI ratio 1/3, LMI @ 166MHz, lock CLOCKGEN
  set *$CLOCKGEN_CKG_LOCK = 0xc0de
  set *$CLOCKGEN_CPG_BYPASS = 0x1
  set *$CLOCKGEN_PLL1_CLK4_CTRL = 0x2
  sti5528_set_clockgen_pll2 0x1a 0xa5 0x0 0x1c8
  set *$CLOCKGEN_CKG_LOCK = 0x0

  espresso_sysconf_configure
  espresso_emi_configure
  espresso_lmi_configure

  set *$CCN_CCR = 0x0000090d
end

document espresso_setup
Configure an STEspresso-Demo board
Usage: espresso_setup
end

define espressosim_setup
  sti5528_define
  espresso_memory_define

  sti5528_si_regs

  set *$CCN_CCR = 0x0000090d
end

document espressosim_setup
Configure a simulated STEspresso-Demo board
Usage: espressosim_setup
end

define espresso_fsim_setup
  sti5528_fsim_core_setup
  espresso_sim_memory_define
end

document espresso_fsim_setup
Configure functional simulator for STEspresso-Demo board
Usage: espresso_fsim_setup
end

define espresso_psim_setup
  sti5528_psim_core_setup
  espresso_sim_memory_define
end

document espresso_psim_setup
Configure performance simulator for STEspresso-Demo board
Usage: espresso_psim_setup
end

define espresso_display_registers
  sti5528_display_si_regs
end

document espresso_display_registers
Display the STi5528 configuration registers
Usage: espresso_display_registers
end
