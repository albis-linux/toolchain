##
## Configure and display clocks for STb7100
##

##{{{  stb7100_set_clockgen_a_pll0
define stb7100_set_clockgen_a_pll0
  ## Unlock CLOCKGENA...
  set *$CLOCKGENA_LOCK = 0xc0de

  ## Set CLOCKGENA PLL0 into BYPASS...
  set *$CLOCKGENA_PLL0_CFG = *$CLOCKGENA_PLL0_CFG | 0x00100000

  ## Disable CLOCKGENA PLL0...
  set *$CLOCKGENA_PLL0_CFG = *$CLOCKGENA_PLL0_CFG & 0xfff7ffff

  ## Configure CLOCKGENA PLL0...
  set *$CLOCKGENA_PLL0_CFG = (*$CLOCKGENA_PLL0_CFG & 0xfff80000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16)

  ## Enable CLOCKGENA PLL0...
  set *$CLOCKGENA_PLL0_CFG = *$CLOCKGENA_PLL0_CFG | 0x00080000

  ## Wait for CLOCKGENA PLL0 to lock...
  while ((*$CLOCKGENA_PLL0_STATUS & 0x00000001) == 0)
  end

  ## Clear CLOCKGENA PLL0 from BYPASS...
  set *$CLOCKGENA_PLL0_CFG = *$CLOCKGENA_PLL0_CFG & 0xffefffff

  ## Lock CLOCKGENA...
  set *$CLOCKGENA_LOCK = 0x0
end
##}}}

##{{{  stb7100_set_clockgen_a_pll1
define stb7100_set_clockgen_a_pll1
  ## Unlock CLOCKGENA...
  set *$CLOCKGENA_LOCK = 0xc0de

  ## Set CLOCKGENA PLL1 into BYPASS...
  set *$CLOCKGENA_PLL1_BYPASS = 0x00000002

  ## Disable CLOCKGENA PLL1...
  set *$CLOCKGENA_PLL1_CFG = *$CLOCKGENA_PLL1_CFG & 0xfff7ffff

  ## Configure CLOCKGENA PLL1...
  set *$CLOCKGENA_PLL1_CFG = (*$CLOCKGENA_PLL1_CFG & 0xfff80000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16)

  ## Enable CLOCKGENA PLL1...
  set *$CLOCKGENA_PLL1_CFG = *$CLOCKGENA_PLL1_CFG | 0x00080000

  ## Wait for CLOCKGENA PLL1 to lock...
  while ((*$CLOCKGENA_PLL1_STATUS & 0x00000001) == 0)
  end

  ## Clear CLOCKGENA PLL1 from BYPASS...
  set *$CLOCKGENA_PLL1_BYPASS = 0x00000000

  ## Lock CLOCKGENA...
  set *$CLOCKGENA_LOCK = 0x0
end
##}}}

##{{{  stb7100_displayclocks
define stb7100_get_clockgen_x_pll_x_frq
  set $_data = *$CLOCKGEN$arg0_PLL$arg1_CFG

  set $_mdiv = (double) ($_data & 0xff)
  set $_ndiv = (double) (($_data >> 8) & 0xff)
  set $_pdiv = (double) (1 << (($_data >> 16) & 0x7))

  set $arg3 = ((2.0 * $arg2 * $_ndiv) / $_mdiv) / $_pdiv
end

define stb7100_displayclocks
  if ($argc > 0)
    set $_extfrq = $arg0
  else
    set $_extfrq = 27.0
  end

  stb7100_get_clockgen_x_pll_x_frq A 0 $_extfrq $_pll0frq
  stb7100_get_clockgen_x_pll_x_frq A 1 $_extfrq $_pll1frq

  set $_mainfrq = $_pll0frq / 2.0

  set $_data = *$CLOCKGENA_PLL0_CLK1_CTRL & 0x7

  if ($_data == 0x0)
    set $_st40cpufrq = $_mainfrq
  end
  if ($_data == 0x1)
    set $_st40cpufrq = $_mainfrq / 2.0
  end
  if ($_data == 0x2)
    set $_st40cpufrq = $_mainfrq / 3.0
  end
  if ($_data == 0x3)
    set $_st40cpufrq = $_mainfrq / 4.0
  end
  if ($_data == 0x4)
    set $_st40cpufrq = $_mainfrq / 6.0
  end
  if ($_data == 0x5)
    set $_st40cpufrq = $_mainfrq / 8.0
  end
  if ($_data == 0x6)
    set $_st40cpufrq = $_mainfrq
  end
  if ($_data == 0x7)
    set $_st40cpufrq = $_mainfrq
  end

  set $_data = *$CLOCKGENA_PLL0_CLK2_CTRL & 0x7

  if ($_data == 0x0)
    set $_st40busfrq = $_mainfrq
  end
  if ($_data == 0x1)
    set $_st40busfrq = $_mainfrq / 2.0
  end
  if ($_data == 0x2)
    set $_st40busfrq = $_mainfrq / 3.0
  end
  if ($_data == 0x3)
    set $_st40busfrq = $_mainfrq / 4.0
  end
  if ($_data == 0x4)
    set $_st40busfrq = $_mainfrq / 6.0
  end
  if ($_data == 0x5)
    set $_st40busfrq = $_mainfrq / 8.0
  end
  if ($_data == 0x6)
    set $_st40busfrq = $_mainfrq / 2.0
  end
  if ($_data == 0x7)
    set $_st40busfrq = $_mainfrq / 2.0
  end

  set $_data = *$CLOCKGENA_PLL0_CLK3_CTRL & 0x7

  if ($_data == 0x0)
    set $_st40perfrq = $_mainfrq / 4.0
  end
  if ($_data == 0x1)
    set $_st40perfrq = $_mainfrq / 2.0
  end
  if ($_data == 0x2)
    set $_st40perfrq = $_mainfrq / 4.0
  end
  if ($_data == 0x3)
    set $_st40perfrq = $_mainfrq / 4.0
  end
  if ($_data == 0x4)
    set $_st40perfrq = $_mainfrq / 6.0
  end
  if ($_data == 0x5)
    set $_st40perfrq = $_mainfrq / 8.0
  end
  if ($_data == 0x6)
    set $_st40perfrq = $_mainfrq / 4.0
  end
  if ($_data == 0x7)
    set $_st40perfrq = $_mainfrq / 4.0
  end

  set $_data = *$CLOCKGENA_PLL0_CLK4_CTRL & 0x7

  if ($_data == 0x0)
    set $_slimfrq = $_mainfrq
  end
  if ($_data == 0x1)
    set $_slimfrq = $_mainfrq / 2.0
  end
  if ($_data == 0x2)
    set $_slimfrq = $_mainfrq / 3.0
  end
  if ($_data == 0x3)
    set $_slimfrq = $_mainfrq / 4.0
  end
  if ($_data == 0x4)
    set $_slimfrq = $_mainfrq / 6.0
  end
  if ($_data == 0x5)
    set $_slimfrq = $_mainfrq / 8.0
  end
  if ($_data == 0x6)
    set $_slimfrq = $_mainfrq / 3.0
  end
  if ($_data == 0x7)
    set $_slimfrq = $_mainfrq / 3.0
  end

  printf "Clock settings:\n"
  printf "\n"
  printf "  CLOCKGENA_PLL0_CFG       = 0x%08x\n", *$CLOCKGENA_PLL0_CFG
  printf "  CLOCKGENA_PLL0_CLK1_CTRL = 0x%08x\n", *$CLOCKGENA_PLL0_CLK1_CTRL
  printf "  CLOCKGENA_PLL0_CLK2_CTRL = 0x%08x\n", *$CLOCKGENA_PLL0_CLK2_CTRL
  printf "  CLOCKGENA_PLL0_CLK3_CTRL = 0x%08x\n", *$CLOCKGENA_PLL0_CLK3_CTRL
  printf "  CLOCKGENA_PLL0_CLK4_CTRL = 0x%08x\n", *$CLOCKGENA_PLL0_CLK4_CTRL
  printf "  CLOCKGENA_PLL1_CFG       = 0x%08x\n", *$CLOCKGENA_PLL1_CFG
  printf "\n"
  printf "Clock frequencies\n"
  printf "\n"
  printf "  PLL0      = %.1f MHz\n", $_pll0frq
  printf "  PLL1      = %.1f MHz\n", $_pll1frq
  printf "  ST40 CPU  = %.1f MHz\n", $_st40cpufrq
  printf "  ST40 BUS  = %.1f MHz\n", $_st40busfrq
  printf "  ST40 PER  = %.1f MHz\n", $_st40perfrq
  printf "  ST231 CPU = %.1f MHz\n", $_pll1frq
  printf "  SLIM CPU  = %.1f MHz\n", $_slimfrq
  printf "  ST BUS    = %.1f MHz\n", $_pll1frq / 2.0
  printf "  EMI       = %.1f MHz\n", $_pll1frq / 4.0
  printf "  LMI       = %.1f MHz\n", $_pll1frq / 2.0
end

document stb7100_displayclocks
Display the STb7100 clock frequencies
Usage: stb7100_displayclocks [<frequency>]
where <frequency> is optional and is the external clock frequency (default: 27MHz)
end
##}}}
