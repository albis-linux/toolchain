##------------------------------------------------------------------------------
## cab5197.cmd - STi5197-CAB Reference Platform
##------------------------------------------------------------------------------

##{{{  CAB5197 PMB Configuration
define cab5197se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define cab5197seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define cab5197se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64
  sh4_set_pmb 1 0xa0 0x40 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  CAB5197 Memory
define cab5197_memory_define
  memory-add Flash     0x00000000 16 ROM
  memory-add LMI_SDRAM 0x0c000000 64 RAM
end

define cab5197_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x0c000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define cab5197se_memory_define
  memory-add Flash     0x00000000 16 ROM
  memory-add LMI_SDRAM 0x40000000 64 RAM
end

define cab5197se_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x40000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define cab5197sim_setup
  sti5197_define
  cab5197_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document cab5197sim_setup
Configure a simulated STi5197-CAB board
Usage: cab5197sim_setup
end

define cab5197simse_setup
  sti5197_define
  cab5197se_memory_define

  st40300_core_si_regs

  cab5197se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document cab5197simse_setup
Configure a simulated STi5197-CAB board with the STi5197 in 32-bit SE mode
Usage: cab5197simse_setup
end

define cab5197simseuc_setup
  sti5197_define
  cab5197se_memory_define

  st40300_core_si_regs

  cab5197seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document cab5197simseuc_setup
Configure a simulated STi5197-CAB board with the STi5197 in 32-bit SE mode with uncached RAM mappings
Usage: cab5197simseuc_setup
end

define cab5197simse29_setup
  sti5197_define
  cab5197se_memory_define

  st40300_core_si_regs

  cab5197se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document cab5197simse29_setup
Configure a simulated STi5197-CAB board with the STi5197 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: cab5197simse29_setup
end

define cab5197_fsim_setup
  sti5197_fsim_core_setup
  cab5197_sim_memory_define
end

document cab5197_fsim_setup
Configure functional simulator for STi5197-CAB board
Usage: cab5197_fsim_setup
end

define cab5197se_fsim_setup
  sti5197_fsim_core_setup
  cab5197se_sim_memory_define
end

document cab5197se_fsim_setup
Configure functional simulator for STi5197-CAB board with the STi5197 in 32-bit SE mode
Usage: cab5197se_fsim_setup
end

define cab5197_psim_setup
  sti5197_psim_core_setup
  cab5197_sim_memory_define
end

document cab5197_psim_setup
Configure performance simulator for STi5197-CAB board
Usage: cab5197_psim_setup
end

define cab5197se_psim_setup
  sti5197_psim_core_setup
  cab5197se_sim_memory_define
end

document cab5197se_psim_setup
Configure performance simulator for STi5197-CAB board with the STi5197 in 32-bit SE mode
Usage: cab5197se_psim_setup
end

define cab5197_display_registers
  st40300_display_core_si_regs
end

document cab5197_display_registers
Display the STi5197 configuration registers
Usage: cab5197_display_registers
end
