################################################################################

define sat7111sim
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat7111_fsim_setup sat7111sim_setup $arg0
  else
    connectsh4simle sat7111_fsim_setup sat7111sim_setup ""
  end
end

document sat7111sim
Connect to and configure a simulated STi7111-SAT board
Usage: sat7111sim
end

define sat7111simse
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat7111se_fsim_setup sat7111simse_setup $arg0
  else
    connectsh4simle sat7111se_fsim_setup sat7111simse_setup ""
  end
end

document sat7111simse
Connect to and configure a simulated STi7111-SAT board (STi7111 in 32-bit SE mode)
Usage: sat7111simse
end

define sat7111simseuc
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat7111se_fsim_setup sat7111simseuc_setup $arg0
  else
    connectsh4simle sat7111se_fsim_setup sat7111simseuc_setup ""
  end
end

document sat7111simseuc
Connect to and configure a simulated STi7111-SAT board (STi7111 in 32-bit SE mode with uncached mappings)
Usage: sat7111simseuc
end

define sat7111simse29
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sat7111se_fsim_setup sat7111simse29_setup $arg0
  else
    connectsh4simle sat7111se_fsim_setup sat7111simse29_setup ""
  end
end

document sat7111simse29
Connect to and configure a simulated STi7111-SAT board (STi7111 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sat7111simse29
end

################################################################################

define sat7111psim
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat7111_psim_setup sat7111sim_setup $arg0
  else
    connectsh4psimle sat7111_psim_setup sat7111sim_setup ""
  end
end

document sat7111psim
Connect to and configure a simulated STi7111-SAT board
Usage: sat7111psim
end

define sat7111psimse
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat7111se_psim_setup sat7111simse_setup $arg0
  else
    connectsh4psimle sat7111se_psim_setup sat7111simse_setup ""
  end
end

document sat7111psimse
Connect to and configure a simulated STi7111-SAT board (STi7111 in 32-bit SE mode)
Usage: sat7111psimse
end

define sat7111psimseuc
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat7111se_psim_setup sat7111simseuc_setup $arg0
  else
    connectsh4psimle sat7111se_psim_setup sat7111simseuc_setup ""
  end
end

document sat7111psimseuc
Connect to and configure a simulated STi7111-SAT board (STi7111 in 32-bit SE mode with uncached mappings)
Usage: sat7111psimseuc
end

define sat7111psimse29
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source sat7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sat7111se_psim_setup sat7111simse29_setup $arg0
  else
    connectsh4psimle sat7111se_psim_setup sat7111simse29_setup ""
  end
end

document sat7111psimse29
Connect to and configure a simulated STi7111-SAT board (STi7111 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sat7111psimse29
end

################################################################################
