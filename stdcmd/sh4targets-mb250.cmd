################################################################################

define mb250
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source mb250.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb250_setup $arg1
  else
    connectsh4le $arg0 mb250_setup ""
  end
end

document mb250
Connect to and configure an ST407750 Orion HD1 board
Usage: mb250 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb250usb
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source mb250.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb250_setup $arg1
  else
    connectsh4usble $arg0 mb250_setup ""
  end
end

document mb250usb
Connect to and configure an ST407750 Orion HD1 board
Usage: mb250usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb250sim
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source mb250.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb250_fsim_setup mb250sim_setup $arg0
  else
    connectsh4simle mb250_fsim_setup mb250sim_setup ""
  end
end

document mb250sim
Connect to and configure a simulated ST407750 Orion HD1 board
Usage: mb250sim
end

################################################################################

define mb250psim
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source mb250.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb250_psim_setup mb250sim_setup $arg0
  else
    connectsh4psimle mb250_psim_setup mb250sim_setup ""
  end
end

document mb250psim
Connect to and configure a simulated ST407750 Orion HD1 board
Usage: mb250psim
end

################################################################################
