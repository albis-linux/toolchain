init-if-undefined $STV0498ResetDelay = 20
keep-variable $STV0498ResetDelay

init-if-undefined $_stv0498jtag_debugreset = 1
keep-variable $_stv0498jtag_debugreset

init-if-undefined $_stv0498jtag_printids = 0
keep-variable $_stv0498jtag_printids

##{{{  stv0498_bypass_setup
define stv0498_bypass_setup
  set $_stv0498_bypass_setup_argc = $argc

  enable_jtag

  ## Print TMC device identifiers
  if ($_stv0498jtag_printids)
    ## Manual control of JTAG (manual TCK)
    jtag mode=manual

    ## Reset TapMux to bypass to TMC
    jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

    source jtagtmcid.cmd
    tmcPrintIds STV0498
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset STV0498 leaving ST40 in reset hold
  if ($_stv0498jtag_debugreset)
    jtag ntrst=0
    _jtagSleep $STV0498ResetDelay
    jtag nrst=0
    _jtagSleep $STV0498ResetDelay
    jtag asebrk=0
    _jtagSleep $STV0498ResetDelay
    jtag nrst=1
    _jtagSleep $STV0498ResetDelay
    jtag asebrk=1
    _jtagSleep $STV0498ResetDelay
    jtag ntrst=1
  else
    jtag nrst=0
    _jtagSleep $STV0498ResetDelay
    jtag asebrk=0
    _jtagSleep $STV0498ResetDelay
    jtag nrst=1
    _jtagSleep $STV0498ResetDelay
    jtag asebrk=1
  end

  ## Reset TapMux to bypass to TMC
  jtag tck=00 ntrst=01 tms=00 tdo=00

  if ($_stv0498_bypass_setup_argc > 0)
    $arg0
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux and then bypass to ST40
  jtag tck=01010 ntrst=00011 tms=00000 tdo=01111

  ## Normal control of JTAG
  jtag mode=normal
end

document stv0498_bypass_setup
Configure the STV0498 for a direct connection to the ST40 CPU
Usage: stv0498_bypass_setup [<command>]
where <command> is an optional command to be invoked before connecting to the ST40 CPU
end
##}}}

##{{{  stv0498_bypass_setup_attach
define stv0498_bypass_setup_attach
  set $_stv0498_bypass_setup_attach_argc = $argc

  enable_jtag

  ## Print TMC device identifiers
  if ($_stv0498jtag_printids)
    ## Manual control of JTAG (manual TCK)
    jtag mode=manual

    ## Reset TapMux to bypass to TMC
    jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

    source jtagtmcid.cmd
    tmcPrintIds STV0498
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux to bypass to TMC
  jtag tck=00 ntrst=01 tms=00 tdo=00

  if ($_stv0498_bypass_setup_attach_argc > 0)
    $arg0
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TapMux and then bypass to ST40
  jtag tck=01010 ntrst=00011 tms=00000 tdo=01111

  ## Normal control of JTAG
  jtag mode=normal
end

document stv0498_bypass_setup_attach
Configure the STV0498 for a direct connection to the ST40 CPU (STV0498 not reset)
Usage: stv0498_bypass_setup_attach [<command>]
where <command> is an optional command to be invoked before connecting to the ST40 CPU
end
##}}}
