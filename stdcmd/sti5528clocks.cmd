##
## Configure and display clocks for STi5528
##

##{{{  sti5528_set_clockgen_pll1
define sti5528_set_clockgen_pll1_shmode
  ## Unlock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  ## Move CLOCKGEN_PLL1 to SH_RUN_SLOW...
  set *$CPG_FRQCR = *$CPG_FRQCR & 0xf7ff

  ## Configure CLOCKGEN_PLL1...
  set *$CLOCKGEN_PLL1_CTRL = (*$CLOCKGEN_PLL1_CTRL & 0xf0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | ($arg3 << 19)

  ## Move CLOCKGEN_PLL1 to SH_RUN_FAST...
  set *$CPG_FRQCR = *$CPG_FRQCR | 0x0800

  ## Lock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0x0
end

define sti5528_set_clockgen_pll1_stmode
  ## Unlock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  ## Move CLOCKGEN_PLL1 to ST_RUN_SLOW...
  set *$CLOCKGEN_PLL1_CTRL2 = *$CLOCKGEN_PLL1_CTRL2 & 0xfffffffa

  ## Configure CLOCKGEN_PLL1...
  set *$CLOCKGEN_PLL1_CTRL = (*$CLOCKGEN_PLL1_CTRL & 0xf0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | ($arg3 << 19)

  ## Move CLOCKGEN_PLL1 to ST_LEAVING_SLOW...
  set *$CLOCKGEN_PLL1_CTRL2 = *$CLOCKGEN_PLL1_CTRL2 | 0x00000001

  ## Wait for CLOCKGEN_PLL1 to lock...
  while ((*$CLOCKGEN_PLL1_CTRL & 0x80000000) == 0)
  end

  ## Move CLOCKGEN_PLL1 to ST_RUN_FAST...
  set *$CLOCKGEN_PLL1_CTRL2 = *$CLOCKGEN_PLL1_CTRL2 | 0x00000004

  ## Lock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0x0
end

define sti5528_set_clockgen_pll1
  if ((*$CLOCKGEN_PLL1_CTRL2 & 0x0000000f) == 0x0000000f)
    sti5528_set_clockgen_pll1_stmode $arg0 $arg1 $arg2 $arg3
  else
  if (((*$CLOCKGEN_PLL1_CTRL2 & 0x0000000a) == 0x00000000) && ((*$CLOCKGEN_CPG_BYPASS & 0x00000001) == 0x00000000) && ((*$CPG_FRQCR & 0x0800) == 0x0800))
    sti5528_set_clockgen_pll1_shmode $arg0 $arg1 $arg2 $arg3
  else
    printf "unknown PLL1 configuration mode\n"
  end
  end
end

define sti5528_set_clockgen_pll1_st2sh_mode
  ## Unlock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  if ((*$CLOCKGEN_PLL1_CTRL2 & 0x0000000f) == 0x0000000a)
    ## Move CLOCKGEN_PLL1 from ST_RUN_SLOW to ST2SH_SLOW
    set *$CLOCKGEN_PLL1_CTRL2 = (*$CLOCKGEN_PLL1_CTRL2 & 0xfffffff0) | 0x00000008
    set *$CLOCKGEN_CPG_BYPASS = *$CLOCKGEN_CPG_BYPASS & 0xfffffffe
    set *$CPG_FRQCR = *$CPG_FRQCR & 0xf7ff

    ## Move CLOCKGEN_PLL1 from ST2SH_SLOW to SH_RUN_SLOW
    set *$CLOCKGEN_PLL1_CTRL2 = *$CLOCKGEN_PLL1_CTRL2 & 0xfffffff0
  else
  if ((*$CLOCKGEN_PLL1_CTRL2 & 0x0000000f) == 0x0000000f)
    ## Move CLOCKGEN_PLL1 from ST_RUN_FAST to ST2SH_FAST
    set *$CLOCKGEN_PLL1_CTRL2 = (*$CLOCKGEN_PLL1_CTRL2 & 0xfffffff0) | 0x0000000d
    set *$CLOCKGEN_CPG_BYPASS = *$CLOCKGEN_CPG_BYPASS & 0xfffffffe
    set *$CPG_FRQCR = *$CPG_FRQCR | 0x0800

    ## Move CLOCKGEN_PLL1 from ST2SH_FAST to SH_RUN_FAST
    set *$CLOCKGEN_PLL1_CTRL2 = *$CLOCKGEN_PLL1_CTRL2 & 0xfffffff0
  else
    printf "unknown PLL1 configuration mode\n"
  end
  end

  ## Lock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0x0
end

define sti5528_set_clockgen_pll1_sh2st_mode
  ## Unlock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  if (((*$CLOCKGEN_PLL1_CTRL2 & 0x0000000a) == 0x00000000) && ((*$CLOCKGEN_CPG_BYPASS & 0x00000001) == 0x00000000) && ((*$CPG_FRQCR & 0x0800) == 0x0000))
    ## Move CLOCKGEN_PLL1 from SH_RUN_SLOW to SH2ST_SLOW
    set *$CLOCKGEN_PLL1_CTRL2 = (*$CLOCKGEN_PLL1_CTRL2 & 0xfffffff0) | 0x00000002

    ## Move CLOCKGEN_PLL1 from SH2ST_SLOW to ST_RUN_SLOW
    set *$CLOCKGEN_CPG_BYPASS = *$CLOCKGEN_CPG_BYPASS | 0x00000001
    set *$CLOCKGEN_PLL1_CTRL2 = *$CLOCKGEN_PLL1_CTRL2 | 0x00000008
  else
  if (((*$CLOCKGEN_PLL1_CTRL2 & 0x0000000a) == 0x00000000) && ((*$CLOCKGEN_CPG_BYPASS & 0x00000001) == 0x00000000) && ((*$CPG_FRQCR & 0x0800) == 0x0800))
    ## Move CLOCKGEN_PLL1 from SH_RUN_FAST to SH2ST_FAST
    set *$CLOCKGEN_PLL1_CTRL2 = (*$CLOCKGEN_PLL1_CTRL2 & 0xfffffff0) | 0x00000007

    ## Move CLOCKGEN_PLL1 from SH2ST_FAST to ST_RUN_FAST
    set *$CLOCKGEN_CPG_BYPASS = *$CLOCKGEN_CPG_BYPASS | 0x00000001
    set *$CLOCKGEN_PLL1_CTRL2 = *$CLOCKGEN_PLL1_CTRL2 | 0x00000008
  else
    printf "unknown PLL1 configuration mode\n"
  end
  end

  ## Lock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0x0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk*
##{{{  sti5528_set_clockgen_pll1_clk_x
define sti5528_set_clockgen_pll1_clk_x
  ## Unlock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  ## Configure CLOCKGEN_PLL1_CLKn...
  set *$CLOCKGEN_PLL1_CLK$arg0_CTRL = $arg1

  ## Lock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0x0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk1
define sti5528_set_clockgen_pll1_clk1
  sti5528_set_clockgen_pll1_clk_x 1 $arg0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk2
define sti5528_set_clockgen_pll1_clk1
  sti5528_set_clockgen_pll1_clk_x 2 $arg0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk3
define sti5528_set_clockgen_pll1_clk1
  sti5528_set_clockgen_pll1_clk_x 3 $arg0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk4
define sti5528_set_clockgen_pll1_clk1
  sti5528_set_clockgen_pll1_clk_x 4 $arg0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk5
define sti5528_set_clockgen_pll1_clk1
  sti5528_set_clockgen_pll1_clk_x 5 $arg0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk6
define sti5528_set_clockgen_pll1_clk1
  sti5528_set_clockgen_pll1_clk_x 6 $arg0
end
##}}}

##{{{  sti5528_set_clockgen_pll1_clk
define sti5528_set_clockgen_pll1_clk
  sti5528_set_clockgen_pll1_clk_x 1 $arg0
  sti5528_set_clockgen_pll1_clk_x 2 $arg1
  sti5528_set_clockgen_pll1_clk_x 3 $arg2
  sti5528_set_clockgen_pll1_clk_x 4 $arg3
  sti5528_set_clockgen_pll1_clk_x 5 $arg4
  sti5528_set_clockgen_pll1_clk_x 6 $arg5
end
##}}}
##}}}

##{{{  sti5528_set_clockgen_pll2
define sti5528_set_clockgen_pll2
  ## Unlock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  ## Disable CLOCKGEN_PLL2...
  set *$CLOCKGEN_PLL2_CTRL = *$CLOCKGEN_PLL2_CTRL & 0xbfffffff

  ## Stop CLOCKGEN_PLL2...
  set *$CLOCKGEN_PLL2_CTRL = *$CLOCKGEN_PLL2_CTRL | 0x20000000

  ## Configure CLOCKGEN_PLL2...
  set *$CLOCKGEN_PLL2_CTRL = (*$CLOCKGEN_PLL2_CTRL & 0xf0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | ($arg3 << 19)

  ## Restart CLOCKGEN_PLL2...
  set *$CLOCKGEN_PLL2_CTRL = *$CLOCKGEN_PLL2_CTRL & 0xdfffffff

  ## Wait for CLOCKGEN_PLL2 to lock...
  while ((*$CLOCKGEN_PLL2_CTRL & 0x80000000) == 0)
  end

  ## Enable CLOCKGEN_PLL2...
  set *$CLOCKGEN_PLL2_CTRL = *$CLOCKGEN_PLL2_CTRL | 0x40000000

  ## Lock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0x0
end
##}}}

##{{{  sti5528_set_clockgen_qsynth*_clk*
##{{{  sti5528_set_clockgen_qsynth_x_clk_x
define sti5528_set_clockgen_qsynth_x_clk_x
  ## Unlock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  ## Stop CLOCKGEN_QSYNTHn_CLKn...
  set *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL = *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL | 0x08000000

  ## Disable CLOCKGEN_QSYNTHn_CLKn...
  set *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL = *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL & 0xfeffffff

  ## Configure CLOCKGEN_QSYNTHn_CLKn...
  set *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL = (*$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL & 0xff000000) | ($arg2) | ($arg3 << 16) | ($arg4 << 21)

  ## Enable CLOCKGEN_QSYNTHn_CLKn...
  set *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL = *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL | 0x01000000

  ## Start CLOCKGEN_QSYNTHn_CLKn...
  set *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL = *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL & 0xf7ffffff

  ## Lock CLOCKGEN...
  set *$CLOCKGEN_CKG_LOCK = 0x0
end
##}}}

##{{{  sti5528_set_clockgen_qsynth1_clk1
define sti5528_set_clockgen_qsynth1_clk1
  sti5528_set_clockgen_qsynth_x_clk_x 1 1 $arg0 $arg1 $arg2
end
##}}}

##{{{  sti5528_set_clockgen_qsynth1_clk2
define sti5528_set_clockgen_qsynth1_clk2
  sti5528_set_clockgen_qsynth_x_clk_x 1 2 $arg0 $arg1 $arg2
end
##}}}

##{{{  sti5528_set_clockgen_qsynth2_clk1
define sti5528_set_clockgen_qsynth2_clk1
  sti5528_set_clockgen_qsynth_x_clk_x 2 1 $arg0 $arg1 $arg2
end
##}}}

##{{{  sti5528_set_clockgen_qsynth2_clk2
define sti5528_set_clockgen_qsynth2_clk2
  sti5528_set_clockgen_qsynth_x_clk_x 2 2 $arg0 $arg1 $arg2
end
##}}}

##{{{  sti5528_set_clockgen_qsynth2_clk3
define sti5528_set_clockgen_qsynth2_clk3
  sti5528_set_clockgen_qsynth_x_clk_x 2 3 $arg0 $arg1 $arg2
end
##}}}

##{{{  sti5528_set_clockgen_qsynth2_clk4
define sti5528_set_clockgen_qsynth2_clk4
  sti5528_set_clockgen_qsynth_x_clk_x 2 4 $arg0 $arg1 $arg2
end
##}}}
##}}}

##{{{  sti5528_displayclocks
define sti5528_get_clockgen_pll_x_frq
  set $_data = *$CLOCKGEN_PLL$arg0_CTRL

  set $_mdiv = (double) (($_data & 0xff) + 1)
  set $_ndiv = (double) ((($_data >> 8) & 0xff) + 1)
  set $_pdiv = (double) (1 << (($_data >> 16) & 0x7))

  set $arg2 = ((2.0 * $arg1 * $_ndiv) / $_mdiv) / $_pdiv
end

define sti5528_get_clockgen_qsynth_x_clk_x_frq
  set $_data = *$CLOCKGEN_QSYNTH$arg0_CLK$arg1_CTRL

  set $_pe = (double) ($_data & 0xffff)
  set $_md = (double) (((signed int) (($_data >> 16) & 0x1f)) - 32)
  set $_sdiv = (double) (2 << (($_data >> 21) & 0x7))

  set $arg2 = (32768.0 * 216.0) / ($_sdiv * (($_pe * (1.0 + ($_md / 32.0))) - (($_pe - 32768.0) * (1.0 + (($_md + 1.0) / 32.0)))))
end

define sti5528_displayclocks
  if ($argc > 0)
    set $_extfrq = $arg0
  else
    set $_extfrq = 27.0
  end

  sti5528_get_clockgen_pll_x_frq 1 $_extfrq $_pll1frq
  sti5528_get_clockgen_pll_x_frq 2 $_extfrq $_pll2frq

  set $_mainfrq = $_pll1frq / 2.0
  set $_lmifrq = $_pll2frq / 2.0

  set $_cpgbypass = *$CLOCKGEN_CPG_BYPASS & 0x1

  if ($_cpgbypass == 0x0)
    set $_st40frq = 0.0

    set $_data = (*$CPG_FRQCR >> 6) & 0x7

    if ($_data == 0x0)
      set $_st40frq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_st40frq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_st40frq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_st40frq = $_mainfrq / 4.0
    end
    if ($_data == 0x4)
      set $_st40frq = $_mainfrq / 6.0
    end
    if ($_data == 0x5)
      set $_st40frq = $_mainfrq / 8.0
    end
    if ($_st40frq == 0.0)
      printf "unknown value for CPG_FRQCR\n"
    end
  else
    set $_data = *$CLOCKGEN_PLL1_CLK1_CTRL & 0x7

    if ($_data == 0x0)
      set $_st40frq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_st40frq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_st40frq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_st40frq = $_mainfrq * 2.0 / 3.0
    end
    if ($_data == 0x4)
      set $_st40frq = $_mainfrq / 4.0
    end
    if ($_data == 0x5)
      set $_st40frq = $_mainfrq / 6.0
    end
    if ($_data == 0x6)
      set $_st40frq = $_mainfrq / 8.0
    end
    if ($_data == 0x7)
      set $_st40frq = $_mainfrq / 8.0
    end
  end

  if ($_cpgbypass == 0x0)
    set $_busfrq = 0.0

    set $_data = (*$CPG_FRQCR >> 3) & 0x7

    if ($_data == 0x0)
      set $_busfrq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_busfrq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_busfrq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_busfrq = $_mainfrq / 4.0
    end
    if ($_data == 0x4)
      set $_busfrq = $_mainfrq / 6.0
    end
    if ($_data == 0x5)
      set $_busfrq = $_mainfrq / 8.0
    end
    if ($_busfrq == 0.0)
      printf "unknown value for CPG_FRQCR\n"
    end
  else
    set $_data = *$CLOCKGEN_PLL1_CLK2_CTRL & 0x7

    if ($_data == 0x0)
      set $_busfrq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_busfrq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_busfrq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_busfrq = $_mainfrq * 2.0 / 3.0
    end
    if ($_data == 0x4)
      set $_busfrq = $_mainfrq / 4.0
    end
    if ($_data == 0x5)
      set $_busfrq = $_mainfrq / 6.0
    end
    if ($_data == 0x6)
      set $_busfrq = $_mainfrq / 8.0
    end
    if ($_data == 0x7)
      set $_busfrq = $_mainfrq / 8.0
    end
  end

  if ($_cpgbypass == 0x0)
    set $_perfrq = 0.0

    set $_data = *$CPG_FRQCR & 0x7

    if ($_data == 0x0)
      set $_perfrq = $_mainfrq / 2.0
    end
    if ($_data == 0x1)
      set $_perfrq = $_mainfrq / 3.0
    end
    if ($_data == 0x2)
      set $_perfrq = $_mainfrq / 4.0
    end
    if ($_data == 0x3)
      set $_perfrq = $_mainfrq / 6.0
    end
    if ($_data == 0x4)
      set $_perfrq = $_mainfrq / 8.0
    end
    if ($_perfrq == 0.0)
      printf "unknown value for CPG_FRQCR\n"
    end
  else
    set $_data = *$CLOCKGEN_PLL1_CLK3_CTRL & 0x7

    if ($_data == 0x0)
      set $_perfrq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_perfrq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_perfrq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_perfrq = $_mainfrq * 2.0 / 3.0
    end
    if ($_data == 0x4)
      set $_perfrq = $_mainfrq / 4.0
    end
    if ($_data == 0x5)
      set $_perfrq = $_mainfrq / 6.0
    end
    if ($_data == 0x6)
      set $_perfrq = $_mainfrq / 8.0
    end
    if ($_data == 0x7)
      set $_perfrq = $_mainfrq / 8.0
    end
  end

  set $_data = *$CLOCKGEN_PLL1_CLK4_CTRL & 0x7

  if ($_data == 0x0)
    set $_emifrq = $_mainfrq
  end
  if ($_data == 0x1)
    set $_emifrq = $_mainfrq / 2.0
  end
  if ($_data == 0x2)
    set $_emifrq = $_mainfrq / 3.0
  end
  if ($_data == 0x3)
    set $_emifrq = $_mainfrq * 2.0 / 3.0
  end
  if ($_data == 0x4)
    set $_emifrq = $_mainfrq / 4.0
  end
  if ($_data == 0x5)
    set $_emifrq = $_mainfrq / 6.0
  end
  if ($_data == 0x6)
    set $_emifrq = $_mainfrq / 8.0
  end
  if ($_data == 0x7)
    set $_emifrq = $_mainfrq / 8.0
  end

  set $_data = *$EMI_FLASHCLKSEL & 0x3

  if ($_data == 0x0)
    set $_emiflashfrq = $_emifrq
  end
  if ($_data == 0x1)
    set $_emiflashfrq = $_emifrq / 2.0
  end
  if ($_data == 0x2)
    set $_emiflashfrq = $_emifrq / 3.0
  end

  set $_data = *$EMI_SDRAMCLKSEL & 0x3

  if ($_data == 0x0)
    set $_emisdramfrq = $_emifrq
  end
  if ($_data == 0x1)
    set $_emisdramfrq = $_emifrq / 2.0
  end
  if ($_data == 0x2)
    set $_emisdramfrq = $_emifrq / 3.0
  end

  set $_data = *$CLOCKGEN_PLL1_CLK5_CTRL & 0x7

  if ($_data == 0x0)
    set $_vdefrq = $_mainfrq
  end
  if ($_data == 0x1)
    set $_vdefrq = $_mainfrq / 2.0
  end
  if ($_data == 0x2)
    set $_vdefrq = $_mainfrq / 3.0
  end
  if ($_data == 0x3)
    set $_vdefrq = $_mainfrq * 2.0 / 3.0
  end
  if ($_data == 0x4)
    set $_vdefrq = $_mainfrq / 4.0
  end
  if ($_data == 0x5)
    set $_vdefrq = $_mainfrq / 6.0
  end
  if ($_data == 0x6)
    set $_vdefrq = $_mainfrq / 8.0
  end
  if ($_data == 0x7)
    set $_vdefrq = $_mainfrq / 8.0
  end

  set $_data = *$CLOCKGEN_PLL1_CLK6_CTRL & 0x7

  if ($_data == 0x0)
    set $_adefrq = $_mainfrq
  end
  if ($_data == 0x1)
    set $_adefrq = $_mainfrq / 2.0
  end
  if ($_data == 0x2)
    set $_adefrq = $_mainfrq / 3.0
  end
  if ($_data == 0x3)
    set $_adefrq = $_mainfrq * 2.0 / 3.0
  end
  if ($_data == 0x4)
    set $_adefrq = $_mainfrq / 4.0
  end
  if ($_data == 0x5)
    set $_adefrq = $_mainfrq / 6.0
  end
  if ($_data == 0x6)
    set $_adefrq = $_mainfrq / 8.0
  end
  if ($_data == 0x7)
    set $_adefrq = $_mainfrq / 8.0
  end

  sti5528_get_clockgen_qsynth_x_clk_x_frq 1 1 $_qs1ck1frq
  sti5528_get_clockgen_qsynth_x_clk_x_frq 1 2 $_qs1ck2frq
  sti5528_get_clockgen_qsynth_x_clk_x_frq 2 1 $_qs2ck1frq
  sti5528_get_clockgen_qsynth_x_clk_x_frq 2 2 $_qs2ck2frq
  sti5528_get_clockgen_qsynth_x_clk_x_frq 2 3 $_qs2ck3frq
  sti5528_get_clockgen_qsynth_x_clk_x_frq 2 4 $_qs2ck4frq

  set $_pcifrq = $_qs1ck1frq

  set $_usbfrq = $_qs1ck2frq

  set $_data = *$CLOCKGEN_ST20_CLK_CTRL & 0x1

  if ($_data == 0x0)
    set $_st20frq = $_adefrq
  end
  if ($_data == 0x1)
    set $_st20frq = $_qs2ck1frq
  end

  set $_hddfrq = $_qs2ck2frq

  set $_scfrq = $_qs2ck3frq

  set $_data = *$CLOCKGEN_PIX_CLK_CTRL & 0x3

  if ($_data == 0x0)
    set $_pixfrq = 27.0
  end
  if ($_data == 0x1)
    set $_pixfrq = 27.0
  end
  if ($_data == 0x2)
    set $_pixfrq = 0.0
  end
  if ($_data == 0x3)
    set $_pixfrq = $_qs2ck4frq
  end

  printf "Clock settings:\n"
  printf "\n"
  printf "  CPG_FRQCR                  = 0x%04x\n", *$CPG_FRQCR
  printf "\n"
  printf "  CLOCKGEN_MD_STATUS         = 0x%08x\n", *$CLOCKGEN_MD_STATUS
  printf "\n"
  printf "  CLOCKGEN_CPG_BYPASS        = 0x%08x\n", *$CLOCKGEN_CPG_BYPASS
  printf "\n"
  printf "  CLOCKGEN_PLL1_CTRL         = 0x%08x\n", *$CLOCKGEN_PLL1_CTRL
  printf "  CLOCKGEN_PLL1_CTRL2        = 0x%08x\n", *$CLOCKGEN_PLL1_CTRL2
  printf "  CLOCKGEN_PLL2_CTRL         = 0x%08x\n", *$CLOCKGEN_PLL2_CTRL
  printf "  CLOCKGEN_PLL1_CLK1_CTRL    = 0x%08x\n", *$CLOCKGEN_PLL1_CLK1_CTRL
  printf "  CLOCKGEN_PLL1_CLK2_CTRL    = 0x%08x\n", *$CLOCKGEN_PLL1_CLK2_CTRL
  printf "  CLOCKGEN_PLL1_CLK3_CTRL    = 0x%08x\n", *$CLOCKGEN_PLL1_CLK3_CTRL
  printf "  CLOCKGEN_PLL1_CLK4_CTRL    = 0x%08x\n", *$CLOCKGEN_PLL1_CLK4_CTRL
  printf "  CLOCKGEN_PLL1_CLK5_CTRL    = 0x%08x\n", *$CLOCKGEN_PLL1_CLK5_CTRL
  printf "  CLOCKGEN_PLL1_CLK6_CTRL    = 0x%08x\n", *$CLOCKGEN_PLL1_CLK6_CTRL
  printf "\n"
  printf "  CLOCKGEN_QSYNTH1_CTRL      = 0x%08x\n", *$CLOCKGEN_QSYNTH1_CTRL
  printf "  CLOCKGEN_QSYNTH2_CTRL      = 0x%08x\n", *$CLOCKGEN_QSYNTH2_CTRL
  printf "  CLOCKGEN_QSYNTH1_CLK1_CTRL = 0x%08x\n", *$CLOCKGEN_QSYNTH1_CLK1_CTRL
  printf "  CLOCKGEN_QSYNTH1_CLK2_CTRL = 0x%08x\n", *$CLOCKGEN_QSYNTH1_CLK2_CTRL
  printf "  CLOCKGEN_QSYNTH2_CLK1_CTRL = 0x%08x\n", *$CLOCKGEN_QSYNTH2_CLK1_CTRL
  printf "  CLOCKGEN_QSYNTH2_CLK2_CTRL = 0x%08x\n", *$CLOCKGEN_QSYNTH2_CLK2_CTRL
  printf "  CLOCKGEN_QSYNTH2_CLK3_CTRL = 0x%08x\n", *$CLOCKGEN_QSYNTH2_CLK3_CTRL
  printf "  CLOCKGEN_QSYNTH2_CLK4_CTRL = 0x%08x\n", *$CLOCKGEN_QSYNTH2_CLK4_CTRL
  printf "\n"
  printf "Clock frequencies\n"
  printf "\n"
  printf "  PLL1         = %.1f MHz\n", $_pll1frq
  printf "  PLL2         = %.1f MHz\n", $_pll2frq
  printf "\n"
  printf "  QSYNTH1 CLK1 = %.1f MHz\n", $_qs1ck1frq
  printf "  QSYNTH1 CLK2 = %.1f MHz\n", $_qs1ck2frq
  printf "\n"
  printf "  QSYNTH2 CLK1 = %.1f MHz\n", $_qs2ck1frq
  printf "  QSYNTH2 CLK2 = %.1f MHz\n", $_qs2ck2frq
  printf "  QSYNTH2 CLK3 = %.1f MHz\n", $_qs2ck3frq
  printf "  QSYNTH2 CLK4 = %.1f MHz\n", $_qs2ck4frq
  printf "\n"
  printf "  ST20 CPU     = %.1f MHz\n", $_st20frq
  printf "  ST40 CPU     = %.1f MHz\n", $_st40frq
  printf "  ST40 PER     = %.1f MHz\n", $_perfrq
  printf "  ST BUS       = %.1f MHz\n", $_busfrq
  printf "  EMI          = %.1f MHz\n", $_emifrq
  printf "  EMI (FLASH)  = %.1f MHz\n", $_emiflashfrq
  printf "  EMI (SDRAM)  = %.1f MHz\n", $_emisdramfrq
  printf "  LMI          = %.1f MHz\n", $_lmifrq
  printf "  PCI          = %.1f MHz\n", $_pcifrq
  printf "  USB          = %.1f MHz\n", $_usbfrq
  printf "\n"
  printf "  HDD          = %.1f MHz\n", $_hddfrq
  printf "  PIX          = %.1f MHz\n", $_pixfrq
  printf "  SC           = %.1f MHz\n", $_scfrq
  printf "  VDE          = %.1f MHz\n", $_vdefrq
  printf "  ADE          = %.1f MHz\n", $_adefrq
end

document sti5528_displayclocks
Display the STi5528 clock frequencies
Usage: sti5528_displayclocks [<frequency>]
where <frequency> is optional and is the external clock frequency (default: 27MHz)
end
##}}}
