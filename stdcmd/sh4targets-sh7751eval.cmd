################################################################################

define sh7751evalbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh7751eval_setup $arg1
  else
    connectsh4be $arg0 sh7751eval_setup ""
  end
end

document sh7751evalbe
Connect to and configure an SH7751 Evaluation board (big endian)
Usage: sh7751evalbe <target>
where <target> is an ST Micro Connect name or IP address
end

define sh7751evalle
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh7751eval_setup $arg1
  else
    connectsh4le $arg0 sh7751eval_setup ""
  end
end

document sh7751evalle
Connect to and configure an SH7751 Evaluation board (little endian)
Usage: sh7751evalle <target>
where <target> is an ST Micro Connect name or IP address
end

define sh7751eval
  if ($argc > 1)
    sh7751evalle $arg0 $arg1
  else
    sh7751evalle $arg0
  end
end

document sh7751eval
Connect to and configure an SH7751 Evaluation board
Usage: sh7751eval <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh7751evalusbbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh7751eval_setup $arg1
  else
    connectsh4usbbe $arg0 sh7751eval_setup ""
  end
end

document sh7751evalusbbe
Connect to and configure an SH7751 Evaluation board (big endian)
Usage: sh7751evalusbbe <target>
where <target> is an ST Micro Connect USB name
end

define sh7751evalusble
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh7751eval_setup $arg1
  else
    connectsh4usble $arg0 sh7751eval_setup ""
  end
end

document sh7751evalusble
Connect to and configure an SH7751 Evaluation board (little endian)
Usage: sh7751evalusble <target>
where <target> is an ST Micro Connect USB name
end

define sh7751evalusb
  if ($argc > 1)
    sh7751evalusble $arg0 $arg1
  else
    sh7751evalusble $arg0
  end
end

document sh7751evalusb
Connect to and configure an SH7751 Evaluation board
Usage: sh7751evalusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh7751evalsimbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh7751eval_fsim_setup sh7751evalsim_setup $arg0
  else
    connectsh4simbe sh7751eval_fsim_setup sh7751evalsim_setup ""
  end
end

document sh7751evalsimbe
Connect to and configure a simulated SH7751 Evaluation board (big endian)
Usage: sh7751evalsimbe
end

define sh7751evalsimle
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh7751eval_fsim_setup sh7751evalsim_setup $arg0
  else
    connectsh4simle sh7751eval_fsim_setup sh7751evalsim_setup ""
  end
end

document sh7751evalsimle
Connect to and configure a simulated SH7751 Evaluation board (little endian)
Usage: sh7751evalsimle
end

define sh7751evalsim
  if ($argc > 0)
    sh7751evalsimle $arg0
  else
    sh7751evalsimle
  end
end

document sh7751evalsim
Connect to and configure a simulated SH7751 Evaluation board
Usage: sh7751evalsim
end

################################################################################

define sh7751evalpsimbe
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh7751eval_psim_setup sh7751evalsim_setup $arg0
  else
    connectsh4psimbe sh7751eval_psim_setup sh7751evalsim_setup ""
  end
end

document sh7751evalpsimbe
Connect to and configure a simulated SH7751 Evaluation board (big endian)
Usage: sh7751evalpsimbe
end

define sh7751evalpsimle
  source register40.cmd
  source display40.cmd
  source sh7751.cmd
  source sh7751eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh7751eval_psim_setup sh7751evalsim_setup $arg0
  else
    connectsh4psimle sh7751eval_psim_setup sh7751evalsim_setup ""
  end
end

document sh7751evalpsimle
Connect to and configure a simulated SH7751 Evaluation board (little endian)
Usage: sh7751evalpsimle
end

define sh7751evalpsim
  if ($argc > 0)
    sh7751evalpsimle $arg0
  else
    sh7751evalpsimle
  end
end

document sh7751evalpsim
Connect to and configure a simulated SH7751 Evaluation board
Usage: sh7751evalpsim
end

################################################################################
