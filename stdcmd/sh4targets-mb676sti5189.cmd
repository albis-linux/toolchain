################################################################################

define mb676sti5189sim
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5189_fsim_setup mb676sti5189sim_setup $arg0
  else
    connectsh4simle mb676sti5189_fsim_setup mb676sti5189sim_setup ""
  end
end

document mb676sti5189sim
Connect to and configure a simulated STi5189-Mboard board
Usage: mb676sti5189sim
end

define mb676sti5189simse
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5189se_fsim_setup mb676sti5189simse_setup $arg0
  else
    connectsh4simle mb676sti5189se_fsim_setup mb676sti5189simse_setup ""
  end
end

document mb676sti5189simse
Connect to and configure a simulated STi5189-Mboard board (STi5189 in 32-bit SE mode)
Usage: mb676sti5189simse
end

define mb676sti5189simseuc
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5189se_fsim_setup mb676sti5189simseuc_setup $arg0
  else
    connectsh4simle mb676sti5189se_fsim_setup mb676sti5189simseuc_setup ""
  end
end

document mb676sti5189simseuc
Connect to and configure a simulated STi5189-Mboard board (STi5189 in 32-bit SE mode with uncached mappings)
Usage: mb676sti5189simseuc
end

define mb676sti5189simse29
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5189se_fsim_setup mb676sti5189simse29_setup $arg0
  else
    connectsh4simle mb676sti5189se_fsim_setup mb676sti5189simse29_setup ""
  end
end

document mb676sti5189simse29
Connect to and configure a simulated STi5189-Mboard board (STi5189 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb676sti5189simse29
end

################################################################################

define mb676sti5189psim
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5189_psim_setup mb676sti5189sim_setup $arg0
  else
    connectsh4psimle mb676sti5189_psim_setup mb676sti5189sim_setup ""
  end
end

document mb676sti5189psim
Connect to and configure a simulated STi5189-Mboard board
Usage: mb676sti5189psim
end

define mb676sti5189psimse
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5189se_psim_setup mb676sti5189simse_setup $arg0
  else
    connectsh4psimle mb676sti5189se_psim_setup mb676sti5189simse_setup ""
  end
end

document mb676sti5189psimse
Connect to and configure a simulated STi5189-Mboard board (STi5189 in 32-bit SE mode)
Usage: mb676sti5189psimse
end

define mb676sti5189psimseuc
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5189se_psim_setup mb676sti5189simseuc_setup $arg0
  else
    connectsh4psimle mb676sti5189se_psim_setup mb676sti5189simseuc_setup ""
  end
end

document mb676sti5189psimseuc
Connect to and configure a simulated STi5189-Mboard board (STi5189 in 32-bit SE mode with uncached mappings)
Usage: mb676sti5189psimseuc
end

define mb676sti5189psimse29
  source register40.cmd
  source display40.cmd
  source sti5189.cmd
  source mb676sti5189.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5189se_psim_setup mb676sti5189simse29_setup $arg0
  else
    connectsh4psimle mb676sti5189se_psim_setup mb676sti5189simse29_setup ""
  end
end

document mb676sti5189psimse29
Connect to and configure a simulated STi5189-Mboard board (STi5189 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb676sti5189psimse29
end

################################################################################
