################################################################################
#
# STi7200 ST231 Boot Commands
#
################################################################################

init-if-undefined $_sti7200_st231_audio0_boot = 1
keep-variable $_sti7200_st231_audio0_boot

init-if-undefined $_sti7200_st231_audio1_boot = 1
keep-variable $_sti7200_st231_audio1_boot

init-if-undefined $_sti7200_st231_video0_boot = 1
keep-variable $_sti7200_st231_video0_boot

init-if-undefined $_sti7200_st231_video1_boot = 1
keep-variable $_sti7200_st231_video1_boot

define sti7200_st231_audio0_boot
  set *$SYSCONF_SYS_CFG09 |= 0x08000000
  set *$SYSCONF_SYS_CFG05 = (*$SYSCONF_SYS_CFG05 & ~0x00000004) & ~0x00000002
  set *$SYSCONF_SYS_CFG26 = ((*$SYSCONF_SYS_CFG27 & 0x00001ffe) << 19) | 0x00004001
  set *$SYSCONF_SYS_CFG27 |= 0x00000001
  set *$SYSCONF_SYS_CFG27 &= ~0x00000001
end

define sti7200_st231_audio1_boot
  set *$SYSCONF_SYS_CFG09 |= 0x08000000
  set *$SYSCONF_SYS_CFG05 = (*$SYSCONF_SYS_CFG05 & ~0x00000004) | 0x00000002
  set *$SYSCONF_SYS_CFG34 = ((*$SYSCONF_SYS_CFG35 & 0x00001ffe) << 19) | 0x00004001
  set *$SYSCONF_SYS_CFG35 |= 0x00000001
  set *$SYSCONF_SYS_CFG35 &= ~0x00000001
end

define sti7200_st231_video0_boot
  set *$SYSCONF_SYS_CFG09 |= 0x08000000
  set *$SYSCONF_SYS_CFG05 = *$SYSCONF_SYS_CFG05 & ~0x00000001
  set *$SYSCONF_SYS_CFG28 = ((*$SYSCONF_SYS_CFG29 & 0x00001ffe) << 19) | 0x00004001
  set *$SYSCONF_SYS_CFG29 |= 0x00000001
  set *$SYSCONF_SYS_CFG29 &= ~0x00000001
end

define sti7200_st231_video1_boot
  set *$SYSCONF_SYS_CFG09 |= 0x08000000
  set *$SYSCONF_SYS_CFG05 = *$SYSCONF_SYS_CFG05 | 0x00000001
  set *$SYSCONF_SYS_CFG36 = ((*$SYSCONF_SYS_CFG37 & 0x00001ffe) << 19) | 0x00004001
  set *$SYSCONF_SYS_CFG37 |= 0x00000001
  set *$SYSCONF_SYS_CFG37 &= ~0x00000001
end

define sti7200_st231_boot
  if ($_sti7200_st231_audio0_boot)
    sti7200_st231_audio0_boot
  end

  if ($_sti7200_st231_audio1_boot)
    sti7200_st231_audio1_boot
  end

  if ($_sti7200_st231_video0_boot)
    sti7200_st231_video0_boot
  end

  if ($_sti7200_st231_video1_boot)
    sti7200_st231_video1_boot
  end
end

################################################################################
