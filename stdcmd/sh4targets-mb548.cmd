################################################################################

define mb548
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb548_setup $arg1
  else
    connectsh4le $arg0 mb548_setup "jtagpinout=st40 hardreset"
  end
end

document mb548
Connect to and configure a DTV150DB board
Usage: mb548 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb548ssbe
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb548ssbe_setup $arg1
  else
    connectsh4le $arg0 mb548ssbe_setup "jtagpinout=st40 hardreset"
  end
end

document mb548ssbe
Connect to and configure a DTV150SSB (Europe) board
Usage: mb548ssbe <target>
where <target> is an ST Micro Connect name or IP address
end

define mb548ssbu
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb548ssbu_setup $arg1
  else
    connectsh4le $arg0 mb548ssbu_setup "jtagpinout=st40 hardreset"
  end
end

document mb548ssbu
Connect to and configure a DTV150SSB (US) board
Usage: mb548ssbu <target>
where <target> is an ST Micro Connect name or IP address
end

define mb548eval
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb548eval_setup $arg1
  else
    connectsh4le $arg0 mb548eval_setup "jtagpinout=st40 hardreset"
  end
end

document mb548eval
Connect to and configure a DTV150EVAL board
Usage: mb548eval <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb548usb
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb548_setup $arg1
  else
    connectsh4usble $arg0 mb548_setup "jtagpinout=st40 hardreset"
  end
end

document mb548usb
Connect to and configure a DTV150DB board
Usage: mb548usb <target>
where <target> is an ST Micro Connect USB name
end

define mb548ssbeusb
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb548ssbe_setup $arg1
  else
    connectsh4usble $arg0 mb548ssbe_setup "jtagpinout=st40 hardreset"
  end
end

document mb548ssbeusb
Connect to and configure a DTV150SSB (Europe) board
Usage: mb548ssbeusb <target>
where <target> is an ST Micro Connect USB name
end

define mb548ssbuusb
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb548ssbu_setup $arg1
  else
    connectsh4usble $arg0 mb548ssbu_setup "jtagpinout=st40 hardreset"
  end
end

document mb548ssbuusb
Connect to and configure a DTV150SSB (US) board
Usage: mb548ssbuusb <target>
where <target> is an ST Micro Connect USB name
end

define mb548evalusb
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb548eval_setup $arg1
  else
    connectsh4usble $arg0 mb548eval_setup "jtagpinout=st40 hardreset"
  end
end

document mb548evalusb
Connect to and configure a DTV150EVAL board
Usage: mb548evalusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb548sim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb548_fsim_setup mb548sim_setup $arg0
  else
    connectsh4simle mb548_fsim_setup mb548sim_setup ""
  end
end

document mb548sim
Connect to and configure a simulated DTV150DB board
Usage: mb548sim
end

define mb548ssbesim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb548ssbe_fsim_setup mb548ssbesim_setup $arg0
  else
    connectsh4simle mb548ssbe_fsim_setup mb548ssbesim_setup ""
  end
end

document mb548ssbesim
Connect to and configure a simulated DTV150SSB (Europe) board
Usage: mb548ssbesim
end

define mb548ssbusim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb548ssbu_fsim_setup mb548ssbusim_setup $arg0
  else
    connectsh4simle mb548ssbu_fsim_setup mb548ssbusim_setup ""
  end
end

document mb548ssbusim
Connect to and configure a simulated DTV150SSB (US) board
Usage: mb548ssbusim
end

define mb548evalsim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb548eval_fsim_setup mb548evalsim_setup $arg0
  else
    connectsh4simle mb548eval_fsim_setup mb548evalsim_setup ""
  end
end

document mb548evalsim
Connect to and configure a simulated DTV150EVAL board
Usage: mb548evalsim
end

################################################################################

define mb548psim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb548_psim_setup mb548sim_setup $arg0
  else
    connectsh4psimle mb548_psim_setup mb548sim_setup ""
  end
end

document mb548psim
Connect to and configure a simulated DTV150DB board
Usage: mb548psim
end

define mb548ssbepsim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb548ssbe_psim_setup mb548ssbesim_setup $arg0
  else
    connectsh4psimle mb548ssbe_psim_setup mb548ssbesim_setup ""
  end
end

document mb548ssbepsim
Connect to and configure a simulated DTV150SSB (Europe) board
Usage: mb548ssbepsim
end

define mb548ssbupsim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb548ssbu_psim_setup mb548ssbusim_setup $arg0
  else
    connectsh4psimle mb548ssbu_psim_setup mb548ssbusim_setup ""
  end
end

document mb548ssbupsim
Connect to and configure a simulated DTV150SSB (US) board
Usage: mb548ssbupsim
end

define mb548evalpsim
  source register40.cmd
  source display40.cmd
  source std1000.cmd
  source mb548.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb548eval_psim_setup mb548evalsim_setup $arg0
  else
    connectsh4psimle mb548eval_psim_setup mb548evalsim_setup ""
  end
end

document mb548evalpsim
Connect to and configure a simulated DTV150EVAL board
Usage: mb548evalpsim
end

################################################################################
