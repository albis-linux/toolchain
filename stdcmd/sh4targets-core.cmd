################################################################################

define sh4be
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 echo $arg1
  else
    connectsh4be $arg0 echo ""
  end
end

document sh4be
Connect to an SH4 target without configuring (big endian)
Usage: sh4be <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4le
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 echo $arg1
  else
    connectsh4le $arg0 echo ""
  end
end

document sh4le
Connect to an SH4 target without configuring (little endian)
Usage: sh4le <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4
  if ($argc > 1)
    sh4le $arg0 $arg1
  else
    sh4le $arg0
  end
end

document sh4
Connect to an SH4 target without configuring
Usage: sh4 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define st40300be
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 echo $arg1
  else
    connectst40300be $arg0 echo ""
  end
end

document st40300be
Connect to an ST40-300 target without configuring (big endian)
Usage: st40300be <target>
where <target> is an ST Micro Connect name or IP address
end

define st40300le
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 echo $arg1
  else
    connectst40300le $arg0 echo ""
  end
end

document st40300le
Connect to an ST40-300 target without configuring (little endian)
Usage: st40300le <target>
where <target> is an ST Micro Connect name or IP address
end

define st40300
  if ($argc > 1)
    st40300le $arg0 $arg1
  else
    st40300le $arg0
  end
end

document st40300
Connect to an ST40-300 target without configuring
Usage: st40300 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh4usbbe
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 echo $arg1
  else
    connectsh4usbbe $arg0 echo ""
  end
end

document sh4usbbe
Connect to an SH4 target without configuring (big endian)
Usage: sh4usbbe <target>
where <target> is an ST Micro Connect USB name
end

define sh4usble
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 echo $arg1
  else
    connectsh4usble $arg0 echo ""
  end
end

document sh4usble
Connect to an SH4 target without configuring (little endian)
Usage: sh4usble <target>
where <target> is an ST Micro Connect USB name
end

define sh4usb
  if ($argc > 1)
    sh4usble $arg0 $arg1
  else
    sh4usble $arg0
  end
end

document sh4usb
Connect to an SH4 target without configuring
Usage: sh4usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define st40300usbbe
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 echo $arg1
  else
    connectst40300usbbe $arg0 echo ""
  end
end

document st40300usbbe
Connect to an ST40-300 target without configuring (big endian)
Usage: st40300usbbe <target>
where <target> is an ST Micro Connect USB name
end

define st40300usble
  source register40.cmd
  source display40.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 echo $arg1
  else
    connectst40300usble $arg0 echo ""
  end
end

document st40300usble
Connect to an ST40-300 target without configuring (little endian)
Usage: st40300usble <target>
where <target> is an ST Micro Connect USB name
end

define st40300usb
  if ($argc > 1)
    st40300usble $arg0 $arg1
  else
    st40300usble $arg0
  end
end

document st40300usb
Connect to an ST40-300 target without configuring
Usage: st40300usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################
