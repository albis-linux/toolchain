################################################################################

define mb411stb7100
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb411stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb411stb7100_setup $arg1
  else
    connectsh4le $arg0 mb411stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7100
Connect to and configure an STb7100-Mboard board
Usage: mb411stb7100 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7100bypass
  if ($argc > 1)
    mb411stb7100 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7100 $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7100bypass_setup"
  end
end

document mb411stb7100bypass
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411stb7100bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7100stmmx
  if ($argc > 1)
    mb411stb7100 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7100 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7100stmmx_setup"
  end
end

document mb411stb7100stmmx
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7100stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb411
  if ($argc > 1)
    mb411stb7100 $arg0 $arg1
  else
    mb411stb7100 $arg0
  end
end

document mb411
Connect to and configure an STb7100-Mboard board
Usage: mb411 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411bypass
  if ($argc > 1)
    mb411stb7100bypass $arg0 $arg1
  else
    mb411stb7100bypass $arg0
  end
end

document mb411bypass
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stmmx
  if ($argc > 1)
    mb411stb7100stmmx $arg0 $arg1
  else
    mb411stb7100stmmx $arg0
  end
end

document mb411stmmx
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb411stb7100usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb411stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb411stb7100_setup $arg1
  else
    connectsh4usble $arg0 mb411stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7100usb
Connect to and configure an STb7100-Mboard board
Usage: mb411stb7100usb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7100bypassusb
  if ($argc > 1)
    mb411stb7100usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7100usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7100bypass_setup"
  end
end

document mb411stb7100bypassusb
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411stb7100bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7100stmmxusb
  if ($argc > 1)
    mb411stb7100usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7100usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7100stmmx_setup"
  end
end

document mb411stb7100stmmxusb
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7100stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb411usb
  if ($argc > 1)
    mb411stb7100usb $arg0 $arg1
  else
    mb411stb7100usb $arg0
  end
end

document mb411usb
Connect to and configure an STb7100-Mboard board
Usage: mb411usb <target>
where <target> is an ST Micro Connect USB name
end

define mb411bypassusb
  if ($argc > 1)
    mb411stb7100bypassusb $arg0 $arg1
  else
    mb411stb7100bypassusb $arg0
  end
end

document mb411bypassusb
Connect to and configure an STb7100-Mboard board bypassing to the ST40
Usage: mb411bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stmmxusb
  if ($argc > 1)
    mb411stb7100stmmxusb $arg0 $arg1
  else
    mb411stb7100stmmxusb $arg0
  end
end

document mb411stmmxusb
Connect to and configure an STb7100-Mboard board via an ST MultiCore/Mux
Usage: mb411stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb411stb7100sim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source mb411stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb411stb7100_fsim_setup mb411stb7100sim_setup $arg0
  else
    connectsh4simle mb411stb7100_fsim_setup mb411stb7100sim_setup ""
  end
end

document mb411stb7100sim
Connect to and configure a simulated STb7100-Mboard board
Usage: mb411stb7100sim
end

################################################################################

define mb411sim
  if ($argc > 0)
    mb411stb7100sim $arg0
  else
    mb411stb7100sim
  end
end

document mb411sim
Connect to and configure a simulated STb7100-Mboard board
Usage: mb411sim
end

################################################################################

define mb411stb7100psim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source mb411stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb411stb7100_psim_setup mb411stb7100sim_setup $arg0
  else
    connectsh4psimle mb411stb7100_psim_setup mb411stb7100sim_setup ""
  end
end

document mb411stb7100psim
Connect to and configure a simulated STb7100-Mboard board
Usage: mb411stb7100psim
end

################################################################################

define mb411psim
  if ($argc > 0)
    mb411stb7100psim $arg0
  else
    mb411stb7100psim
  end
end

document mb411psim
Connect to and configure a simulated STb7100-Mboard board
Usage: mb411psim
end

################################################################################
