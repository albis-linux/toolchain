##------------------------------------------------------------------------------
## eud7141.cmd - STi7141-EUD Reference Platform
##------------------------------------------------------------------------------

##{{{  EUD7141 PMB Configuration
define eud7141se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x48 128
  sh4_set_pmb 2 0xa0 0x80 128
  sh4_set_pmb 3 0xa8 0x88 128

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define eud7141seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  sh4_set_pmb 1 0x88 0x48 128 0 0 1
  sh4_set_pmb 2 0xa0 0x80 128 0 0 1
  sh4_set_pmb 3 0xa8 0x88 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define eud7141se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x48 128
  sh4_set_pmb 2 0x90 0x80 128
  sh4_set_pmb 3 0x98 0x88 128
  sh4_set_pmb 4 0xa0 0x40 128 0 0 1
  sh4_set_pmb 5 0xa8 0x48 128 0 0 1
  sh4_set_pmb 6 0xb0 0x80 128 0 0 1
  sh4_set_pmb 7 0xb8 0x88 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  EUD7141 Memory
define eud7141_memory_define
  memory-add Flash      0x00000000 128 ROM
  memory-add LMI0_SDRAM 0x0c000000 128 RAM
  memory-add LMI1_SDRAM 0x14000000 128 RAM
end

define eud7141_sim_memory_define
  sim_addmemory 0x00000000 128 ROM
  sim_addmemory 0x0c000000 128 RAM
  sim_addmemory 0x14000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define eud7141se_memory_define
  memory-add Flash      0x00000000 128 ROM
  memory-add LMI0_SDRAM 0x40000000 256 RAM
  memory-add LMI1_SDRAM 0x80000000 256 RAM
end

define eud7141se_sim_memory_define
  sim_addmemory 0x00000000 128 ROM
  sim_addmemory 0x40000000 256 RAM
  sim_addmemory 0x80000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define eud7141sim_setup
  sti7141_define
  eud7141_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document eud7141sim_setup
Configure a simulated STi7141-EUD board
Usage: eud7141sim_setup
end

define eud7141simse_setup
  sti7141_define
  eud7141se_memory_define

  st40300_core_si_regs

  eud7141se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document eud7141simse_setup
Configure a simulated STi7141-EUD board with the STi7141 in 32-bit SE mode
Usage: eud7141simse_setup
end

define eud7141simseuc_setup
  sti7141_define
  eud7141se_memory_define

  st40300_core_si_regs

  eud7141seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document eud7141simseuc_setup
Configure a simulated STi7141-EUD board with the STi7141 in 32-bit SE mode with uncached RAM mappings
Usage: eud7141simseuc_setup
end

define eud7141simse29_setup
  sti7141_define
  eud7141se_memory_define

  st40300_core_si_regs

  eud7141se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document eud7141simse29_setup
Configure a simulated STi7141-EUD board with the STi7141 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: eud7141simse29_setup
end

define eud7141_fsim_setup
  sti7141_fsim_core_setup
  eud7141_sim_memory_define
end

document eud7141_fsim_setup
Configure functional simulator for STi7141-EUD board
Usage: eud7141_fsim_setup
end

define eud7141se_fsim_setup
  sti7141_fsim_core_setup
  eud7141se_sim_memory_define
end

document eud7141se_fsim_setup
Configure functional simulator for STi7141-EUD board with the STi7141 in 32-bit SE mode
Usage: eud7141se_fsim_setup
end

define eud7141_psim_setup
  sti7141_psim_core_setup
  eud7141_sim_memory_define
end

document eud7141_psim_setup
Configure performance simulator for STi7141-EUD board
Usage: eud7141_psim_setup
end

define eud7141se_psim_setup
  sti7141_psim_core_setup
  eud7141se_sim_memory_define
end

document eud7141se_psim_setup
Configure performance simulator for STi7141-EUD board with the STi7141 in 32-bit SE mode
Usage: eud7141se_psim_setup
end

define eud7141_display_registers
  st40300_display_core_si_regs
end

document eud7141_display_registers
Display the STi7141 configuration registers
Usage: eud7141_display_registers
end
