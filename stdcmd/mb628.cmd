##------------------------------------------------------------------------------
## mb628.cmd - STi7141-Mboard Validation Platform MB628
##------------------------------------------------------------------------------

##{{{  MB628 PMB Configuration
define mb628se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0xa0 0x80 128

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb628seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  sh4_set_pmb 1 0xa0 0x80 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb628se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x80 128
  sh4_set_pmb 2 0xa0 0x40 128 0 0 1
  sh4_set_pmb 3 0xa8 0x80 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB628 Memory
define mb628_memory_define
  memory-add Flash      0x00000000 32 ROM
  memory-add LMI0_SDRAM 0x0c000000 128 RAM
  memory-add LMI1_SDRAM 0x14000000 128 RAM
end

define mb628_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x0c000000 128 RAM
  sim_addmemory 0x14000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define mb628se_memory_define
  memory-add Flash      0x00000000 32 ROM
  memory-add LMI0_SDRAM 0x40000000 128 RAM
  memory-add LMI1_SDRAM 0x80000000 128 RAM
end

define mb628se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x40000000 128 RAM
  sim_addmemory 0x80000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb628sim_setup
  sti7141_define
  mb628_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb628sim_setup
Configure a simulated STi7141-Mboard board
Usage: mb628sim_setup
end

define mb628simse_setup
  sti7141_define
  mb628se_memory_define

  st40300_core_si_regs

  mb628se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb628simse_setup
Configure a simulated STi7141-Mboard board with the STi7141 in 32-bit SE mode
Usage: mb628simse_setup
end

define mb628simseuc_setup
  sti7141_define
  mb628se_memory_define

  st40300_core_si_regs

  mb628seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb628simseuc_setup
Configure a simulated STi7141-Mboard board with the STi7141 in 32-bit SE mode with uncached RAM mappings
Usage: mb628simseuc_setup
end

define mb628simse29_setup
  sti7141_define
  mb628se_memory_define

  st40300_core_si_regs

  mb628se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb628simse29_setup
Configure a simulated STi7141-Mboard board with the STi7141 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb628simse29_setup
end

define mb628_fsim_setup
  sti7141_fsim_core_setup
  mb628_sim_memory_define
end

document mb628_fsim_setup
Configure functional simulator for STi7141-Mboard board
Usage: mb628_fsim_setup
end

define mb628se_fsim_setup
  sti7141_fsim_core_setup
  mb628se_sim_memory_define
end

document mb628se_fsim_setup
Configure functional simulator for STi7141-Mboard board with the STi7141 in 32-bit SE mode
Usage: mb628se_fsim_setup
end

define mb628_psim_setup
  sti7141_psim_core_setup
  mb628_sim_memory_define
end

document mb628_psim_setup
Configure performance simulator for STi7141-Mboard board
Usage: mb628_psim_setup
end

define mb628se_psim_setup
  sti7141_psim_core_setup
  mb628se_sim_memory_define
end

document mb628se_psim_setup
Configure performance simulator for STi7141-Mboard board with the STi7141 in 32-bit SE mode
Usage: mb628se_psim_setup
end

define mb628_display_registers
  st40300_display_core_si_regs
end

document mb628_display_registers
Display the STi7141 configuration registers
Usage: mb628_display_registers
end
