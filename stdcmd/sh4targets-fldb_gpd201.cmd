################################################################################

define fldb_gpd201simse
  source register40.cmd
  source display40.cmd
  source fli7510.cmd
  source fldb_gpd201.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle fldb_gpd201se_fsim_setup fldb_gpd201simse_setup $arg0
  else
    connectsh4simle fldb_gpd201se_fsim_setup fldb_gpd201simse_setup ""
  end
end

document fldb_gpd201simse
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode)
Usage: fldb_gpd201simse
end

define fldb_gpd201simseuc
  source register40.cmd
  source display40.cmd
  source fli7510.cmd
  source fldb_gpd201.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle fldb_gpd201se_fsim_setup fldb_gpd201simseuc_setup $arg0
  else
    connectsh4simle fldb_gpd201se_fsim_setup fldb_gpd201simseuc_setup ""
  end
end

document fldb_gpd201simseuc
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode with uncached mappings)
Usage: fldb_gpd201simseuc
end

define fldb_gpd201simse29
  source register40.cmd
  source display40.cmd
  source fli7510.cmd
  source fldb_gpd201.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle fldb_gpd201se_fsim_setup fldb_gpd201simse29_setup $arg0
  else
    connectsh4simle fldb_gpd201se_fsim_setup fldb_gpd201simse29_setup ""
  end
end

document fldb_gpd201simse29
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: fldb_gpd201simse29
end

################################################################################

define fldb_gpd201sim
  if ($argc > 0)
    fldb_gpd201simse $arg0
  else
    fldb_gpd201simse
  end
end

document fldb_gpd201sim
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode)
Usage: fldb_gpd201sim
end

################################################################################

define fldb_gpd201psimse
  source register40.cmd
  source display40.cmd
  source fli7510.cmd
  source fldb_gpd201.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle fldb_gpd201se_psim_setup fldb_gpd201simse_setup $arg0
  else
    connectsh4psimle fldb_gpd201se_psim_setup fldb_gpd201simse_setup ""
  end
end

document fldb_gpd201psimse
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode)
Usage: fldb_gpd201psimse
end

define fldb_gpd201psimseuc
  source register40.cmd
  source display40.cmd
  source fli7510.cmd
  source fldb_gpd201.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle fldb_gpd201se_psim_setup fldb_gpd201simseuc_setup $arg0
  else
    connectsh4psimle fldb_gpd201se_psim_setup fldb_gpd201simseuc_setup ""
  end
end

document fldb_gpd201psimseuc
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode with uncached mappings)
Usage: fldb_gpd201psimseuc
end

define fldb_gpd201psimse29
  source register40.cmd
  source display40.cmd
  source fli7510.cmd
  source fldb_gpd201.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle fldb_gpd201se_psim_setup fldb_gpd201simse29_setup $arg0
  else
    connectsh4psimle fldb_gpd201se_psim_setup fldb_gpd201simse29_setup ""
  end
end

document fldb_gpd201psimse29
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: fldb_gpd201psimse29
end

################################################################################

define fldb_gpd201psim
  if ($argc > 0)
    fldb_gpd201psimse $arg0
  else
    fldb_gpd201psimse
  end
end

document fldb_gpd201psim
Connect to and configure a simulated FLi7510 development board (FLi7510 in 32-bit SE mode)
Usage: fldb_gpd201psim
end

################################################################################
