################################################################################

define mb422
  source register40.cmd
  source display40.cmd
  source std2000.cmd
  source mb422.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb422_setup $arg1
  else
    connectsh4le $arg0 mb422_setup "hardreset"
  end
end

document mb422
Connect to and configure a DTV100-DB board
Usage: mb422 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb422usb
  source register40.cmd
  source display40.cmd
  source std2000.cmd
  source mb422.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb422_setup $arg1
  else
    connectsh4usble $arg0 mb422_setup "hardreset"
  end
end

document mb422usb
Connect to and configure a DTV100-DB board
Usage: mb422usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb422sim
  source register40.cmd
  source display40.cmd
  source std2000.cmd
  source mb422.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb422_fsim_setup mb422sim_setup $arg0
  else
    connectsh4simle mb422_fsim_setup mb422sim_setup ""
  end
end

document mb422sim
Connect to and configure a simulated DTV100-DB board
Usage: mb422sim
end

################################################################################

define mb422psim
  source register40.cmd
  source display40.cmd
  source std2000.cmd
  source mb422.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb422_psim_setup mb422sim_setup $arg0
  else
    connectsh4psimle mb422_psim_setup mb422sim_setup ""
  end
end

document mb422psim
Connect to and configure a simulated DTV100-DB board
Usage: mb422psim
end

################################################################################
