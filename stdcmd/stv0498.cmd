##
## STV0498 device description (on-chip memory and registers)
##

define stv0498_memory_define
  ## Configuration registers on SuperHyway (via P2)
  memory-add LMI_regs    0x1b000000 16 DEV

  ## Configuration registers in P4
  memory-add STOREQ_data 0xe0000000 64 DEV
  memory-add ICACHE_addr 0xf0000000 16 DEV
  memory-add ICACHE_data 0xf1000000 16 DEV
  memory-add OCACHE_addr 0xf4000000 16 DEV
  memory-add OCACHE_data 0xf5000000 16 DEV
  memory-add CORE_regs   0xfc000000 64 DEV
end

define stv0498_define
  ## processor ST40
  ## chip STV0498

  stv0498_memory_define
end

define stv0498_fsim_core_setup
  sim_command "config +cpu.sh4_family=mcu"
  sim_command "config +cpu.sh7751_features=false"
  sim_command "config +cpu.icache.nr_partitions=2"
  sim_command "config +cpu.dcache.nr_partitions=2"
  sim_reset
end

define stv0498_psim_core_setup
  sim_command "config +cpu.cache_store_on_fill=true"
  sim_command "config +use_tobu=false"
  stv0498_fsim_core_setup
end
