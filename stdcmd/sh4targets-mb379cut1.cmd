################################################################################

define mb379cut1
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb379cut1.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb379_setup $arg1
  else
    connectsh4le $arg0 mb379_setup "hardreset"
  end
end

document mb379cut1
Connect to and configure an STm8000-Demo board
Usage: mb379cut1 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb379cut1usb
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb379cut1.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb379_setup $arg1
  else
    connectsh4usble $arg0 mb379_setup "hardreset"
  end
end

document mb379cut1usb
Connect to and configure an STm8000-Demo board
Usage: mb379cut1 <target>
where <target> is an ST Micro Connect USB name
end

################################################################################
