##------------------------------------------------------------------------------
## mb625.cmd - STV0498 Reference Platform MB625
##------------------------------------------------------------------------------
## Note that apart from the LMI configuration registers (in P2), all the other
## configuration registers must be accessed through P4.
##------------------------------------------------------------------------------

##{{{  MB625 CLOCKGEN Configuration
define mb625_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"
end
##}}}

##{{{  MB625 SYSCONF Configuration
define mb625_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  set *$SYSCONF_SYS_CFG11 = 0x0000026f
  set *$SYSCONF_SYS_CFG12 = 0x0000026f
  set *$SYSCONF_SYS_CFG08 = 0x40000003
  set *$SYSCONF_SYS_CFG09 = 0x00000000
  set *$SYSCONF_SYS_CFG07 = 0x00004080
  set *$SYSCONF_SYS_CFG07 = 0x000040c0
end
##}}}

##{{{  MB625 EMI Configuration
define mb625_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000002
  set *$EMI_BANK2_BASEADDRESS = 0x00000006
  set *$EMI_BANK3_BASEADDRESS = 0x0000000a
  set *$EMI_BANK4_BASEADDRESS = 0x0000000e

##------------------------------------------------------------------------------
## Bank 0 - On board Flash M28W640FSU 8MB
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 8Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016d1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - Cable card
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for cable card 16Mb"

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x001016d1
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 2 - On board FPGA
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for on-board FPGA 16Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x04310ef1
  set *$EMI_BANK2_EMICONFIGDATA1 = 0x8a222000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0x8a224200
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 4 - STi7200/STb7109
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for STi7200/STb7109 72Mb"
end
##}}}

##{{{  MB625 LMI Configuration
define mb625_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM (16 bit, CAS 2.5 @ 133MHz-150MHz)"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## SDRAM Mode Register
  ## Set Refresh Interval, Enable Refresh, 16-bit bus,
  ## Grouping Disabled, DDR-SDRAM, Enable.
  ## Bits[27:16]: Refresh Interval = 7.8 microseconds (8K/64ms)
  ## @  50MHz =  390 clk cycles -> 0x186
  ## @  75MHz =  585 clk cycles -> 0x249
  ## @ 100MHz =  780 clk cycles -> 0x30c
  ## @ 125MHz =  975 clk cycles -> 0x3cf
  ## @ 133MHz = 1040 clk cycles -> 0x410  <--
  ## @ 166MHz = 1300 clk cycles -> 0x514

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x04100203

  ## For 133MHz (7.5ns) to 150MHz (6.6nS) operation:
  ## 3 clks RAS_precharge, Trp;
  ## 3 clks RAS_to_CAS_delay, Trcd-r;
  ## 9 clks RAS cycle time, Trc; 
  ## 6 clks RAS Active time, Tras; 
  ## 2 clks RAS_to_RAS_Active_delay, Trrd;
  ## 3 clks Last write to PRE/PALL period SDRAM, Twr; 
  ## 2.5 clks CAS Latency; 
  ## 11 clks Auto Refresh RAS cycle time, Trfc; 
  ## Enable Write to Read interruption;
  ## 1 clk  Write to Read interruption, Twtr;
  ## 3 clks RAS_to_CAS_delay, Trcd-w;
  ## (200/16)=3 clks Exit self-refresh to next command, Tsxsr;

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x352b4235

  ## UBA = 64MB + Base Adr, Quad-bank, Shape 13x10,
  ## Bank Remapping Disabled
  ##
  ## LMI base address 0x0c000000
  ## Memory size 64MB 0x04000000
  ## Row UBA value    0x1000

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x10001a00

  ## One row connected to cs0, so UBA1 = UBA0

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x10001a00

##------------------------------------------------------------------------------
## Initialisation Sequence for LMI & DDR-SDRAM devices
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Control Register"

  ## NOP Enable
  set *$LMI_SCR_0 = 0x00000001
  ## Clock Enable
  set *$LMI_SCR_0 = 0x00000003
  ## NOP Enable
  set *$LMI_SCR_0 = 0x00000001
  ## Precharge all banks
  set *$LMI_SCR_0 = 0x00000002

  ## EMRS Row 0 & 1: Weak Drive : Enable DLL
  set *$LMI_SDMR0 = 0x00000402
  set *$LMI_SDMR1 = 0x00000402
  sleep 0 200

  ## MRS Row 0 & 1 : Reset DLL - /CAS = 2.5, Mode Sequential, Burst Length 8
  set *$LMI_SDMR0 = 0x00000163 
  set *$LMI_SDMR1 = 0x00000163 

  ## Precharge all banks
  set *$LMI_SCR_0 = 0x00000002

  ## CBR enable (auto-refresh)
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004

  ## MRS Row 0 & 1 : Normal - /CAS = 2.5, Mode Sequential, Burst Length 8
  set *$LMI_SDMR0 = 0x00000063
  set *$LMI_SDMR1 = 0x00000063

  ## Normal SDRAM operation, No burst refresh after standby
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB625 Memory
define mb625_memory_define
  memory-add Flash     0x00000000 8 ROM
  memory-add LMI_SDRAM 0x0c000000 64 RAM
end

define mb625_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x0c000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  MB625 Bypass Configuration
define mb625bypass_setup
  if ($argc > 0)
    stv0498_bypass_setup $arg0
  else
    stv0498_bypass_setup
  end
end

define mb625bypass_setup_attach
  if ($argc > 0)
    stv0498_bypass_setup_attach $arg0
  else
    stv0498_bypass_setup_attach
  end
end
##}}}

define mb625_setup
  stv0498_define
  mb625_memory_define

  stv0498_si_regs

  mb625_clockgen_configure
  mb625_sysconf_configure
  mb625_emi_configure
  mb625_lmi_configure

  set *$CCN_CCR = 0x0000090d
end

document mb625_setup
Configure an STV0498-Ref board
Usage: mb625_setup
end

define mb625sim_setup
  stv0498_define
  mb625_memory_define

  stv0498_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb625sim_setup
Configure a simulated STV0498-Ref board
Usage: mb625sim_setup
end

define mb625_fsim_setup
  stv0498_fsim_core_setup
  mb625_sim_memory_define
end

document mb625_fsim_setup
Configure functional simulator for STV0498-Ref board
Usage: mb625_fsim_setup
end

define mb625_psim_setup
  stv0498_psim_core_setup
  mb625_sim_memory_define
end

document mb625_psim_setup
Configure performance simulator for STV0498-Ref board
Usage: mb625_psim_setup
end

define mb625_display_registers
  stv0498_display_si_regs
end

document mb625_display_registers
Display the STV0498 configuration registers
Usage: mb625_display_registers
end
