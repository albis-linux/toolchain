################################################################################

define hms1cutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source hms1stb7100cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
  connectsh4le $arg1 hms1stb7100_setup $arg2
  else
  connectsh4le $arg1 hms1stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document hms1cutX
Connect to and configure a Simple Devices HMS1 board
Usage: hms1cutX <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1cutXbypass
  if ($argc > 2)
  hms1cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
  hms1cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand hms1stb7100bypass_setup"
  end
end

document hms1cutXbypass
Connect to and configure a Simple Devices HMS1 board bypassing to the ST40
Usage: hms1cutXbypass <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1cutXstmmx
  if ($argc > 2)
  hms1cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
  hms1cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hms1stb7100stmmx_setup"
  end
end

document hms1cutXstmmx
Connect to and configure a Simple Devices HMS1 board via an ST MultiCore/Mux
Usage: hms1cutXstmmx <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################
