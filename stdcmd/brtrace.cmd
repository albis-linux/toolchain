set $branch_trace_enabled = 0
keep-variable $branch_trace_enabled

define branchtrace
  if (!$branch_trace_enabled)
    printf "Hardware branch trace plugin not installed\n"
  else
    if ($argc == 0)
      callplugin branch
    end
    if ($argc == 1)
      callplugin branch $arg0
    end
    if ($argc == 2)
      callplugin branch $arg0 $arg1
    end
    if ($argc == 3)
      callplugin branch $arg0 $arg1 $arg2
    end
    if ($argc > 3)
      callplugin branch help
    end
  end
end

document branchtrace
Use "branchtrace help"
end

define brt
  if ($argc == 0)
    branchtrace
  end
  if ($argc == 1)
    branchtrace $arg0
  end
  if ($argc == 2)
    branchtrace $arg0 $arg1
  end
  if ($argc == 3)
    branchtrace $arg0 $arg1 $arg2
  end
  if ($argc > 3)
    branchtrace help
  end
end

document brt
Use "brt help"
end

define enable_branch_trace
  if (!$branch_trace_enabled)
    installplugin branch libsh4brtrace.so
    set $branch_trace_enabled = 1
  else
    printf "Hardware branch trace plugin already enabled\n"
  end
end

document enable_branch_trace
Enable the hardware branch trace plugin
Usage: enable_branch_trace
end
