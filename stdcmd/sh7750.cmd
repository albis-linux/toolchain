##
## SH7750 device description (on-chip memory and registers)
##

define sh7750_memory_define
  ## Configuration registers in P4
  memory-add STOREQ_data 0xe0000000 64 DEV
  memory-add ICACHE_addr 0xf0000000 16 DEV
  memory-add ICACHE_data 0xf1000000 16 DEV
  memory-add ITLB_addr   0xf2000000 16 DEV
  memory-add ITLB_data1  0xf3000000  8 DEV
  memory-add ITLB_data2  0xf3800000  8 DEV
  memory-add OCACHE_addr 0xf4000000 16 DEV
  memory-add OCACHE_data 0xf5000000 16 DEV
  memory-add UTLB_addr   0xf6000000 16 DEV
  memory-add UTLB_data1  0xf7000000  8 DEV
  memory-add UTLB_data2  0xf7800000  8 DEV
  memory-add CORE_regs   0xff000000 16 DEV
end

define sh7750_define
  ## processor SH4
  ## chip SH7750

  sh7750_memory_define
end

define sh7750_fsim_core_setup
  sim_command "config +cpu.sh4_family=fpu"
  sim_command "config +cpu.sh7751_features=true"
  sim_command "config +cpu.icache.nr_partitions=1"
  sim_command "config +cpu.dcache.nr_partitions=1"
  sim_reset
end

define sh7750_psim_core_setup
  sim_command "config +cpu.cache_store_on_fill=false"
  sh7750_fsim_core_setup
end
