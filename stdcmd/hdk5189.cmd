##------------------------------------------------------------------------------
## hdk5189.cmd - STi5189-HDK Reference Platform
##------------------------------------------------------------------------------

##{{{  HDK5189 PMB Configuration
define hdk5189se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk5189seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk5189se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64
  sh4_set_pmb 1 0xa0 0x40 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  HDK5189 Memory
define hdk5189_memory_define
  memory-add Flash     0x00000000 8 ROM
  memory-add LMI_SDRAM 0x0c000000 64 RAM
end

define hdk5189_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x0c000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define hdk5189se_memory_define
  memory-add Flash     0x00000000 8 ROM
  memory-add LMI_SDRAM 0x40000000 64 RAM
end

define hdk5189se_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x40000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define hdk5189sim_setup
  sti5189_define
  hdk5189_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document hdk5189sim_setup
Configure a simulated STi5189-HDK board
Usage: hdk5189sim_setup
end

define hdk5189simse_setup
  sti5189_define
  hdk5189se_memory_define

  st40300_core_si_regs

  hdk5189se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk5189simse_setup
Configure a simulated STi5189-HDK board with the STi5189 in 32-bit SE mode
Usage: hdk5189simse_setup
end

define hdk5189simseuc_setup
  sti5189_define
  hdk5189se_memory_define

  st40300_core_si_regs

  hdk5189seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk5189simseuc_setup
Configure a simulated STi5189-HDK board with the STi5189 in 32-bit SE mode with uncached RAM mappings
Usage: hdk5189simseuc_setup
end

define hdk5189simse29_setup
  sti5189_define
  hdk5189se_memory_define

  st40300_core_si_regs

  hdk5189se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk5189simse29_setup
Configure a simulated STi5189-HDK board with the STi5189 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: hdk5189simse29_setup
end

define hdk5189_fsim_setup
  sti5189_fsim_core_setup
  hdk5189_sim_memory_define
end

document hdk5189_fsim_setup
Configure functional simulator for STi5189-HDK board
Usage: hdk5189_fsim_setup
end

define hdk5189se_fsim_setup
  sti5189_fsim_core_setup
  hdk5189se_sim_memory_define
end

document hdk5189se_fsim_setup
Configure functional simulator for STi5189-HDK board with the STi5189 in 32-bit SE mode
Usage: hdk5189se_fsim_setup
end

define hdk5189_psim_setup
  sti5189_psim_core_setup
  hdk5189_sim_memory_define
end

document hdk5189_psim_setup
Configure performance simulator for STi5189-HDK board
Usage: hdk5189_psim_setup
end

define hdk5189se_psim_setup
  sti5189_psim_core_setup
  hdk5189se_sim_memory_define
end

document hdk5189se_psim_setup
Configure performance simulator for STi5189-HDK board with the STi5189 in 32-bit SE mode
Usage: hdk5189se_psim_setup
end

define hdk5189_display_registers
  st40300_display_core_si_regs
end

document hdk5189_display_registers
Display the STi5189 configuration registers
Usage: hdk5189_display_registers
end
