################################################################################

define mb628sim
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb628_fsim_setup mb628sim_setup $arg0
  else
    connectsh4simle mb628_fsim_setup mb628sim_setup ""
  end
end

document mb628sim
Connect to and configure a simulated STi7141-Mboard board
Usage: mb628sim
end

define mb628simse
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb628se_fsim_setup mb628simse_setup $arg0
  else
    connectsh4simle mb628se_fsim_setup mb628simse_setup ""
  end
end

document mb628simse
Connect to and configure a simulated STi7141-Mboard board (STi7141 in 32-bit SE mode)
Usage: mb628simse
end

define mb628simseuc
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb628se_fsim_setup mb628simseuc_setup $arg0
  else
    connectsh4simle mb628se_fsim_setup mb628simseuc_setup ""
  end
end

document mb628simseuc
Connect to and configure a simulated STi7141-Mboard board (STi7141 in 32-bit SE mode with uncached mappings)
Usage: mb628simseuc
end

define mb628simse29
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb628se_fsim_setup mb628simse29_setup $arg0
  else
    connectsh4simle mb628se_fsim_setup mb628simse29_setup ""
  end
end

document mb628simse29
Connect to and configure a simulated STi7141-Mboard board (STi7141 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb628simse29
end

################################################################################

define mb628psim
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb628_psim_setup mb628sim_setup $arg0
  else
    connectsh4psimle mb628_psim_setup mb628sim_setup ""
  end
end

document mb628psim
Connect to and configure a simulated STi7141-Mboard board
Usage: mb628psim
end

define mb628psimse
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb628se_psim_setup mb628simse_setup $arg0
  else
    connectsh4psimle mb628se_psim_setup mb628simse_setup ""
  end
end

document mb628psimse
Connect to and configure a simulated STi7141-Mboard board (STi7141 in 32-bit SE mode)
Usage: mb628psimse
end

define mb628psimseuc
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb628se_psim_setup mb628simseuc_setup $arg0
  else
    connectsh4psimle mb628se_psim_setup mb628simseuc_setup ""
  end
end

document mb628psimseuc
Connect to and configure a simulated STi7141-Mboard board (STi7141 in 32-bit SE mode with uncached mappings)
Usage: mb628psimseuc
end

define mb628psimse29
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source mb628.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb628se_psim_setup mb628simse29_setup $arg0
  else
    connectsh4psimle mb628se_psim_setup mb628simse29_setup ""
  end
end

document mb628psimse29
Connect to and configure a simulated STi7141-Mboard board (STi7141 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb628psimse29
end

################################################################################
