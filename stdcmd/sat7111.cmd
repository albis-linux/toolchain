##------------------------------------------------------------------------------
## sat7111.cmd - STi7111-SAT Reference Platform
##------------------------------------------------------------------------------

##{{{  SAT7111 PMB Configuration
define sat7111se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define sat7111seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define sat7111se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512
  sh4_set_pmb 1 0xa0 0x40 512 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  SAT7111 Memory
define sat7111_memory_define
  memory-add Flash     0x00000000 64 ROM
  memory-add LMI_SDRAM 0x0c000000 256 RAM
end

define sat7111_sim_memory_define
  sim_addmemory 0x00000000 64 ROM
  sim_addmemory 0x0c000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define sat7111se_memory_define
  memory-add Flash     0x00000000 64 ROM
  memory-add LMI_SDRAM 0x40000000 512 RAM
end

define sat7111se_sim_memory_define
  sim_addmemory 0x00000000 64 ROM
  sim_addmemory 0x40000000 512 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define sat7111sim_setup
  sti7111_define
  sat7111_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document sat7111sim_setup
Configure a simulated STi7111-SAT board
Usage: sat7111sim_setup
end

define sat7111simse_setup
  sti7111_define
  sat7111se_memory_define

  st40300_core_si_regs

  sat7111se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sat7111simse_setup
Configure a simulated STi7111-SAT board with the STi7111 in 32-bit SE mode
Usage: sat7111simse_setup
end

define sat7111simseuc_setup
  sti7111_define
  sat7111se_memory_define

  st40300_core_si_regs

  sat7111seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sat7111simseuc_setup
Configure a simulated STi7111-SAT board with the STi7111 in 32-bit SE mode with uncached RAM mappings
Usage: sat7111simseuc_setup
end

define sat7111simse29_setup
  sti7111_define
  sat7111se_memory_define

  st40300_core_si_regs

  sat7111se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sat7111simse29_setup
Configure a simulated STi7111-SAT board with the STi7111 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: sat7111simse29_setup
end

define sat7111_fsim_setup
  sti7111_fsim_core_setup
  sat7111_sim_memory_define
end

document sat7111_fsim_setup
Configure functional simulator for STi7111-SAT board
Usage: sat7111_fsim_setup
end

define sat7111se_fsim_setup
  sti7111_fsim_core_setup
  sat7111se_sim_memory_define
end

document sat7111se_fsim_setup
Configure functional simulator for STi7111-SAT board with the STi7111 in 32-bit SE mode
Usage: sat7111se_fsim_setup
end

define sat7111_psim_setup
  sti7111_psim_core_setup
  sat7111_sim_memory_define
end

document sat7111_psim_setup
Configure performance simulator for STi7111-SAT board
Usage: sat7111_psim_setup
end

define sat7111se_psim_setup
  sti7111_psim_core_setup
  sat7111se_sim_memory_define
end

document sat7111se_psim_setup
Configure performance simulator for STi7111-SAT board with the STi7111 in 32-bit SE mode
Usage: sat7111se_psim_setup
end

define sat7111_display_registers
  st40300_display_core_si_regs
end

document sat7111_display_registers
Display the STi7111 configuration registers
Usage: sat7111_display_registers
end
