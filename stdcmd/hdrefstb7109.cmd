##------------------------------------------------------------------------------
## hdrefstb7109.cmd - SST HDRef STb7109 Reference Platform 
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

init-if-undefined $_hdrefstb7109extclk = 27
keep-variable $_hdrefstb7109extclk

##{{{  HDREF (STb7109) CLOCKGEN Configuration
define hdrefstb7109_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  linkspeed 1.25MHz

  ##
  ## PLL equations for STb710x
  ##
  ## if   stb7100_set_clockgen_a_pllx A B C
  ## then Fout = (2 * Fpll * B) / (A * 2^C)
  ##
  ## e.g.
  ##
  ## let Fpll = 30 MHz
  ## let    A = 20
  ## let    B = 111
  ## let    C = 0
  ##
  ##     Fout = 2 * 30 * 111 / 20 * 1 = 333MHz
  ##
  
  if ($_hdrefstb7109extclk == 30)
    ## Set PLL0 to 531MHz
    stb7100_set_clockgen_a_pll0 0x14 0xb1 0x0
    ## Set PLL1 to 400MHz
    stb7100_set_clockgen_a_pll1 0x1e 0xc8 0x0
    ## Set PLL1 to 333MHz
    ## stb7100_set_clockgen_a_pll1 0x14 0x6f 0x0
  else
  if ($_hdrefstb7109extclk == 27)
    ## Set PLL0 to 531MHz
    stb7100_set_clockgen_a_pll0 0x06 0x3b 0x0
    ## Set PLL1 to 400MHz
    stb7100_set_clockgen_a_pll1 0x1b 0xc8 0x0
    ## Set PLL1 to 333MHz
    ## stb7100_set_clockgen_a_pll1 0x03 0x25 0x1
  else
    ## Unsupported external clock frequency
  end
  end

  ## stb7100_displayclocks $_hdrefstb7109extclk

  ## Restore default link speed (5MHz maximum for TapMux)
  if ($_stmmxmode)
    linkspeed 5MHz
  else
    linkspeed 10MHz
  end
end
##}}}

##{{{  HDREF (STb7109) SYSCONF Configuration
define hdrefstb7109_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  set *$SYSCONF_SYS_CFG11 = $_stb7109movelmiregs ? 0xad7fd4ea : 0x0d7fd4ea

  while ((*$SYSCONF_SYS_STA12 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  while ((*$SYSCONF_SYS_STA13 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  set *$SYSCONF_SYS_CFG12 = 0x4000000f | (0xf << 12) | (0xf << 23)
  set *$SYSCONF_SYS_CFG13 = 0x4000000f | (0xf << 12) | (0xf << 23)
end

define hdrefstb7109se_sysconf_configure
  hdrefstb7109_sysconf_configure

  # Move LMI SYS and VID base addresses to their space enhanced mode addresses
  set *$SYSCONF_SYS_CFG36 = (*$SYSCONF_SYS_CFG36 & 0xff00ff00) | 0x00600040

  #Signal that full 128MBin bank2 is available in 32 bit mode 
  set $_32bit_bank2 = 1

end
##}}}

##{{{  HDREF (STb7109) EMI Configuration
define hdrefstb7109_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000a
  set *$EMI_BANK4_BASEADDRESS = 0x0000000c

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016d1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 2 - LAN91C111
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for LAN91C111 8Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x042086f1
  set *$EMI_BANK2_EMICONFIGDATA1 = 0x88112111
  set *$EMI_BANK2_EMICONFIGDATA2 = 0x88112211
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - ATAPI
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for ATAPI 8Mb"

  set *$EMI_BANK3_EMICONFIGDATA0 = 0x00200791
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 16Mb"

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000010
end
##}}}

##{{{  HDREF (STb7109) LMI Configuration
define hdrefstb7109_lmisys_configure
  ## _ST_display (_procname) "Configuring LMI-SYS for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMISYS_MIM_0 = 0x861a0247
  set *$LMISYS_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMISYS_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMISYS_SDRA0_0 = (((*$SYSCONF_SYS_CFG36 & 0xff) + 0x08) << 24) | 0x00001a00

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMISYS_SDRA1_0 = (((*$SYSCONF_SYS_CFG36 & 0xff) + ($_32bit_bank2? 0x10 : 0x0c)) << 24) | 0x00001a00

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000003
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SDMR0 = 0x00000402
  set *$LMISYS_SDMR0 = 0x00000133
  set *$LMISYS_SDMR1 = 0x00000402
  set *$LMISYS_SDMR1 = 0x00000133
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SDMR0 = 0x00000033
  set *$LMISYS_SDMR1 = 0x00000033
  set *$LMISYS_SCR_0 = 0x00000000
end

define hdrefstb7109_lmivid_configure
  ## _ST_display (_procname) "Configuring LMI-VID for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMIVID_MIM_0 = 0x861a025f
  set *$LMIVID_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMIVID_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMIVID_SDRA0_0 = ((((*$SYSCONF_SYS_CFG36 >> 16) & 0xff) + 0x08) << 24) | 0x00001a00

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMIVID_SDRA1_0 = ((((*$SYSCONF_SYS_CFG36 >> 16) & 0xff) + 0x08) << 24) | 0x00001a00

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000003
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SDMR0 = 0x00000402
  set *$LMIVID_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SDMR0 = 0x00000033
  set *$LMIVID_SCR_0 = 0x00000000
end
##}}}

##{{{  HDREF (STb7109) PMB Configuration
define hdrefstb7109se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 256
  sh4_set_pmb 1 0xa0 0x60 128
  sh4_set_pmb 2 0xb8 0x18 64 0 1 1
  if (!$_stb7109movelmiregs)
  sh4_set_pmb 3 0xaf 0x0f 16 0 1 1
  sh4_set_pmb 4 0xb7 0x17 16 0 1 1
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdrefstb7109seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 256 0 0 1
  sh4_set_pmb 1 0xa0 0x60 128 0 0 1
  sh4_set_pmb 2 0xb8 0x18 64 0 1 1
  if (!$_stb7109movelmiregs)
  sh4_set_pmb 3 0xaf 0x0f 16 0 1 1
  sh4_set_pmb 4 0xb7 0x17 16 0 1 1
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

##{{{  HDREF (STb7109) Memory
define hdrefstb7109_memory_define
  memory-add Flash        0x00000000 8 ROM
  memory-add LMISYS_SDRAM 0x04000000 192 RAM
  memory-add LMIVID_SDRAM 0x10000000 128 RAM
end

define hdrefstb7109_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x04000000 192 RAM
  sim_addmemory 0x10000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define hdrefstb7109se_memory_define
  memory-add Flash        0x00000000 8 ROM
  memory-add LMISYS_SDRAM 0x40000000 256 RAM
  memory-add LMIVID_SDRAM 0x60000000 128 RAM
end

define hdrefstb7109se_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x40000000 256 RAM
  sim_addmemory 0x60000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  HDREF (STb7109) Bypass Configuration
define hdrefstb7109bypass_setup
  if ($argc > 0)
    stb7100_bypass_setup $arg0
  else
    stb7100_bypass_setup
  end
end

define hdrefstb7109bypass_setup_attach
  if ($argc > 0)
    stb7100_bypass_setup_attach $arg0
  else
    stb7100_bypass_setup_attach
  end
end
##}}}

##{{{  HDREF (STb7109) STMMX Configuration
define hdrefstb7109stmmx_setup
  if ($argc > 0)
    stb7100_stmmx_setup $arg0
  else
    stb7100_stmmx_setup
  end
end
##}}}

define hdrefstb7109_setup
  stb7109_define
  hdrefstb7109_memory_define

  stb7109_si_regs

  hdrefstb7109_clockgen_configure
  hdrefstb7109_sysconf_configure
  hdrefstb7109_emi_configure
  hdrefstb7109_lmisys_configure
  hdrefstb7109_lmivid_configure

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109_setup
Configure an STb7109-Ref board
Usage: hdrefstb7109_setup
end

define hdrefstb7109se_setup
  stb7109_define
  hdrefstb7109se_memory_define

  stb7109_si_regs

  hdrefstb7109_clockgen_configure
  hdrefstb7109se_sysconf_configure
  hdrefstb7109_emi_configure
  hdrefstb7109_lmisys_configure
  hdrefstb7109_lmivid_configure

  hdrefstb7109se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109se_setup
Configure an STb7109-Ref board with the STb7109 in 32-bit SE mode
Usage: hdrefstb7109se_setup
end

define hdrefstb7109seuc_setup
  stb7109_define
  hdrefstb7109se_memory_define

  stb7109_si_regs

  hdrefstb7109_clockgen_configure
  hdrefstb7109se_sysconf_configure
  hdrefstb7109_emi_configure
  hdrefstb7109_lmisys_configure
  hdrefstb7109_lmivid_configure

  hdrefstb7109seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109seuc_setup
Configure an STb7109-Ref board with the STb7109 in 32-bit SE mode with uncached RAM mappings
Usage: hdrefstb7109seuc_setup
end

define hdrefstb7109se29_setup
  stb7109_define
  hdrefstb7109se_memory_define

  stb7109_si_regs

  hdrefstb7109_clockgen_configure
  hdrefstb7109se_sysconf_configure
  hdrefstb7109_emi_configure
  hdrefstb7109_lmisys_configure
  hdrefstb7109_lmivid_configure

  hdrefstb7109se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109se29_setup
Configure an STb7109-Ref board with the STb7109 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: hdrefstb7109se29_setup
end

define hdrefstb7109sim_setup
  stb7109_define
  hdrefstb7109_memory_define

  stb7109_si_regs

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109sim_setup
Configure a simulated STb7109-Ref board
Usage: hdrefstb7109sim_setup
end

define hdrefstb7109simse_setup
  stb7109_define
  hdrefstb7109se_memory_define

  stb7109_si_regs

  hdrefstb7109se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109simse_setup
Configure a simulated STb7109-Ref board with the STb7109 in 32-bit SE mode
Usage: hdrefstb7109simse_setup
end

define hdrefstb7109simseuc_setup
  stb7109_define
  hdrefstb7109se_memory_define

  stb7109_si_regs

  hdrefstb7109seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109simseuc_setup
Configure a simulated STb7109-Ref board with the STb7109 in 32-bit SE mode with uncached RAM mappings
Usage: hdrefstb7109simseuc_setup
end

define hdrefstb7109simse29_setup
  stb7109_define
  hdrefstb7109se_memory_define

  stb7109_si_regs

  hdrefstb7109se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdrefstb7109simse29_setup
Configure a simulated STb7109-Ref board with the STb7109 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: hdrefstb7109simse29_setup
end

define hdrefstb7109_fsim_setup
  stb7109_fsim_core_setup
  hdrefstb7109_sim_memory_define
end

document hdrefstb7109_fsim_setup
Configure functional simulator for STb7109-Ref board
Usage: hdrefstb7109_fsim_setup
end

define hdrefstb7109se_fsim_setup
  stb7109_fsim_core_setup
  hdrefstb7109se_sim_memory_define
end

document hdrefstb7109se_fsim_setup
Configure functional simulator for STb7109-Ref board with the STb7109 in 32-bit SE mode
Usage: hdrefstb7109se_fsim_setup
end

define hdrefstb7109_psim_setup
  stb7109_psim_core_setup
  hdrefstb7109_sim_memory_define
end

document hdrefstb7109_psim_setup
Configure performance simulator for STb7109-Ref board
Usage: hdrefstb7109_psim_setup
end

define hdrefstb7109se_psim_setup
  stb7109_psim_core_setup
  hdrefstb7109se_sim_memory_define
end

document hdrefstb7109se_psim_setup
Configure performance simulator for STb7109-Ref board with the STb7109 in 32-bit SE mode
Usage: hdrefstb7109se_psim_setup
end

define hdrefstb7109_display_registers
  stb7109_display_si_regs
end

document hdrefstb7109_display_registers
Display the STb7109 configuration registers
Usage: hdrefstb7109_display_registers
end
