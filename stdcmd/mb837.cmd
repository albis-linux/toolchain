##------------------------------------------------------------------------------
## mb837.cmd - STi7108-Mboard Validation Platform MB837
##------------------------------------------------------------------------------

##{{{  MB837 PMB Configuration
define mb837se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x48 128
  sh4_set_pmb 2 0xa0 0x80 128
  sh4_set_pmb 3 0xa8 0x88 128

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb837seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  sh4_set_pmb 1 0x88 0x48 128 0 0 1
  sh4_set_pmb 2 0xa0 0x80 128 0 0 1
  sh4_set_pmb 3 0xa8 0x88 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb837se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x48 128
  sh4_set_pmb 2 0x90 0x80 128
  sh4_set_pmb 3 0x98 0x88 128
  sh4_set_pmb 4 0xa0 0x40 128 0 0 1
  sh4_set_pmb 5 0xa8 0x48 128 0 0 1
  sh4_set_pmb 6 0xb0 0x80 128 0 0 1
  sh4_set_pmb 7 0xb8 0x88 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB837 Memory
define mb837se_memory_define
  memory-add Flash      0x00000000 32 ROM
  memory-add LMI0_SDRAM 0x40000000 256 RAM
  memory-add LMI1_SDRAM 0x80000000 256 RAM
end

define mb837se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x40000000 256 RAM
  sim_addmemory 0x80000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb837simse_setup
  sti7108_define
  mb837se_memory_define

  st40300_core_si_regs

  mb837se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb837simse_setup
Configure a simulated STi7108-Mboard board with the STi7108 in 32-bit SE mode
Usage: mb837simse_setup
end

define mb837simseuc_setup
  sti7108_define
  mb837se_memory_define

  st40300_core_si_regs

  mb837seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb837simseuc_setup
Configure a simulated STi7108-Mboard board with the STi7108 in 32-bit SE mode with uncached RAM mappings
Usage: mb837simseuc_setup
end

define mb837simse29_setup
  sti7108_define
  mb837se_memory_define

  st40300_core_si_regs

  mb837se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb837simse29_setup
Configure a simulated STi7108-Mboard board with the STi7108 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb837simse29_setup
end

define mb837se_fsim_setup
  sti7108_fsim_core_setup
  mb837se_sim_memory_define
end

document mb837se_fsim_setup
Configure functional simulator for STi7108-Mboard board with the STi7108 in 32-bit SE mode
Usage: mb837se_fsim_setup
end

define mb837se_psim_setup
  sti7108_psim_core_setup
  mb837se_sim_memory_define
end

document mb837se_psim_setup
Configure performance simulator for STi7108-Mboard board with the STi7108 in 32-bit SE mode
Usage: mb837se_psim_setup
end

define mb837_display_registers
  st40300_display_core_si_regs
end

document mb837_display_registers
Display the STi7108 configuration registers
Usage: mb837_display_registers
end
