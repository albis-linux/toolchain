##
## STd2000 device description (on-chip memory and registers)
##

define std2000_memory_define
  ## Configuration registers on SuperHyway (via P2)
  memory-add EMI_regs    0x01000000  8 DEV
  memory-add LMI1_regs   0x08000000 16 DEV
  memory-add LMI2_regs   0x17000000 16 DEV
  memory-add ST_regs     0x18000000 64 DEV

  ## Configuration registers in P4
  memory-add STOREQ_data   0xe0000000 64 DEV
  memory-add ICACHE_addr   0xf0000000 16 DEV
  memory-add ICACHE_data   0xf1000000 16 DEV
  memory-add ITLB_addr     0xf2000000 16 DEV
  memory-add ITLB_data     0xf3000000 16 DEV
  memory-add OCACHE_addr   0xf4000000 16 DEV
  memory-add OCACHE_data   0xf5000000 16 DEV
  memory-add UTLB_PMB_addr 0xf6000000 16 DEV
  memory-add UTLB_PMB_data 0xf7000000 16 DEV
  memory-add CORE_regs     0xfc000000 64 DEV
end

define std2000_define
  ## processor ST40
  ## chip STd2000

  std2000_memory_define
end

define std2000_fsim_core_setup
  sim_command "config +cpu.sh4_family=fpu"
  sim_command "config +cpu.sh7751_features=false"
  sim_command "config +cpu.icache.nr_partitions=2"
  sim_command "config +cpu.dcache.nr_partitions=2"
  sim_reset
end

define std2000_psim_core_setup
  sim_command "config +cpu.cache_store_on_fill=true"
  sim_command "config +use_tobu=false"
  std2000_fsim_core_setup
end
