################################################################################

source sh4targets-mb442stb7109.cmd

################################################################################

define mb442stb7109bypass128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109bypass $arg0 $arg1
  else
    mb442stb7109bypass $arg0
  end
end

define mb442stb7109stmmx128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109stmmx $arg0 $arg1
  else
    mb442stb7109stmmx $arg0
  end
end

################################################################################

define mb442stb7109bypass128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109bypass $arg0 $arg1
  else
    mb442stb7109bypass $arg0
  end
end

define mb442stb7109stmmx128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109stmmx $arg0 $arg1
  else
    mb442stb7109stmmx $arg0
  end
end

################################################################################

define mb442stb7109bypass128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109bypass $arg0 $arg1
  else
    mb442stb7109bypass $arg0
  end
end

define mb442stb7109stmmx128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109stmmx $arg0 $arg1
  else
    mb442stb7109stmmx $arg0
  end
end

################################################################################

source sh4targets-mb442stb7109cutX.cmd

################################################################################

define mb442stb7109cut11bypass128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109cutXbypass 11 $arg0 $arg1
  else
    mb442stb7109cutXbypass 11 $arg0
  end
end

define mb442stb7109cut11stmmx128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 11 $arg0
  end
end

define mb442stb7109cut20bypass128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109cutXbypass 20 $arg0 $arg1
  else
    mb442stb7109cutXbypass 20 $arg0
  end
end

define mb442stb7109cut20stmmx128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 20 $arg0
  end
end

define mb442stb7109cut30bypass128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109cutXbypass 30 $arg0 $arg1
  else
    mb442stb7109cutXbypass 30 $arg0
  end
end

define mb442stb7109cut30stmmx128Mb
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7109cutXstmmx 30 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 30 $arg0
  end
end

################################################################################

define mb442stb7109cut11bypass128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109cutXbypass 11 $arg0 $arg1
  else
    mb442stb7109cutXbypass 11 $arg0
  end
end

define mb442stb7109cut11stmmx128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 11 $arg0
  end
end

define mb442stb7109cut20bypass128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109cutXbypass 20 $arg0 $arg1
  else
    mb442stb7109cutXbypass 20 $arg0
  end
end

define mb442stb7109cut20stmmx128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 20 $arg0
  end
end

define mb442stb7109cut30bypass128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109cutXbypass 30 $arg0 $arg1
  else
    mb442stb7109cutXbypass 30 $arg0
  end
end

define mb442stb7109cut30stmmx128Mb27MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 27
  if ($argc > 1)
    mb442stb7109cutXstmmx 30 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 30 $arg0
  end
end

################################################################################

define mb442stb7109cut11bypass128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109cutXbypass 11 $arg0 $arg1
  else
    mb442stb7109cutXbypass 11 $arg0
  end
end

define mb442stb7109cut11stmmx128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 11 $arg0
  end
end

define mb442stb7109cut20bypass128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109cutXbypass 20 $arg0 $arg1
  else
    mb442stb7109cutXbypass 20 $arg0
  end
end

define mb442stb7109cut20stmmx128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 20 $arg0
  end
end

define mb442stb7109cut30bypass128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109cutXbypass 30 $arg0 $arg1
  else
    mb442stb7109cutXbypass 30 $arg0
  end
end

define mb442stb7109cut30stmmx128Mb30MHz
  set $_mb442stb7109sys128 = 1
  set $_mb442stb7109extclk = 30
  if ($argc > 1)
    mb442stb7109cutXstmmx 30 $arg0 $arg1
  else
    mb442stb7109cutXstmmx 30 $arg0
  end
end

################################################################################
