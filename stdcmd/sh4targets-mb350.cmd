################################################################################

define mb350
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb350_setup $arg1
  else
    connectsh4le $arg0 mb350_setup "hardreset"
  end
end

document mb350
Connect to and configure an ST40RA Extended HARP board
Usage: mb350 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb350usb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb350_setup $arg1
  else
    connectsh4usble $arg0 mb350_setup "hardreset"
  end
end

document mb350usb
Connect to and configure an ST40RA Extended HARP board
Usage: mb350usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb350sim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb350_fsim_setup mb350sim_setup $arg0
  else
    connectsh4simle mb350_fsim_setup mb350sim_setup ""
  end
end

document mb350sim
Connect to and configure a simulated ST40RA Extended HARP board
Usage: mb350sim
end

################################################################################

define mb350psim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb350_psim_setup mb350sim_setup $arg0
  else
    connectsh4psimle mb350_psim_setup mb350sim_setup ""
  end
end

document mb350psim
Connect to and configure a simulated ST40RA Extended HARP board
Usage: mb350psim
end

################################################################################
