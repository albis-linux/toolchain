##------------------------------------------------------------------------------
## mb376.cmd - STi5528-Mboard Validation Platform MB376
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB376 CLOCKGEN Configuration
define mb376_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  linkspeed 1.25MHz

  ## Set PLL1 to ST mode
  sti5528_set_clockgen_pll1_sh2st_mode
  ## Set PLL1 to 200 MHz
  sti5528_set_clockgen_pll1 0x1a 0xc7 0x0 0x1c8
  ## Set PLL1 ratio modes (ST40=200MHz STBUS=133MHz ST40PER=66MHz EMI=66MHz VDE=100MHz ADE=100MHz)
  sti5528_set_clockgen_pll1_clk 0x0 0x3 0x2 0x2 0x1 0x1

  ## Set PLL2 to 266 MHz (LMI=133MHz)
  sti5528_set_clockgen_pll2 0x1a 0x84 0x0 0x1c8

  ## Set QSYNTH1 CLK1 (PCI=33MHz)
  sti5528_set_clockgen_qsynth1_clk1 0x68ba 0x01a 0x2
  ## Set QSYNTH1 CLK2 (USB=48MHz)
  sti5528_set_clockgen_qsynth1_clk2 0x0000 0x011 0x2

  ## Set QSYNTH2 CLK1 (ST20=166MHz)
  sti5528_set_clockgen_qsynth2_clk1 0x1722 0x014 0x0
  ## Set QSYNTH2 CLK2 (HDD=?MHz)
  sti5528_set_clockgen_qsynth2_clk2 0x01ed 0x019 0x0
  ## Set QSYNTH2 CLK3 (SC=27MHz)
  sti5528_set_clockgen_qsynth2_clk3 0x0000 0x01f 0x2
  ## Set QSYNTH2 CLK4 (PIX=29.5MHz)
  sti5528_set_clockgen_qsynth2_clk4 0x5b1e 0x01d 0x2

  ## Unlock CLOCKGEN
  set *$CLOCKGEN_CKG_LOCK = 0xc0de

  ## Set ST20 clock source to QSYNTH2 CLK1
  set *$CLOCKGEN_ST20_CLK_CTRL = (*$CLOCKGEN_ST20_CLK_CTRL & 0xfffffffe) | 0x00000001

  ## Set ST20 timer clock to 1MHz (SYSACLKIN source)
  set *$CLOCKGEN_ST20_TICK_CTRL |= 0x00000040
  set *$CLOCKGEN_ST20_TICK_CTRL = (*$CLOCKGEN_ST20_TICK_CTRL & 0xffffffc0) | 0x0000001b
  set *$CLOCKGEN_ST20_TICK_CTRL &= 0xffffffbf

  ## Set audio bitstream clock to 27MHz (SYSACLKIN source)
  set *$CLOCKGEN_AUDBIT_CLK_CTRL |= 0x00000040
  set *$CLOCKGEN_AUDBIT_CLK_CTRL = (*$CLOCKGEN_AUDBIT_CLK_CTRL & 0xffffffc0) | 0x00000001
  set *$CLOCKGEN_AUDBIT_CLK_CTRL &= 0xffffffbf

  ## Set low-power clock source to RTCCLK (32KHz ratio)
  set *$CLOCKGEN_LP_CLK_CTRL |= 0x00000040
  set *$CLOCKGEN_LP_CLK_CTRL = (*$CLOCKGEN_LP_CLK_CTRL & 0xffffff40) | 0x00000b38
  set *$CLOCKGEN_LP_CLK_CTRL &= 0xffffffbf

  ## Set audio frequency clocks sources to SYSACLKIN (audio decoder & S/PDIF)
  set *$CLOCKGEN_AUD_CLK_REF_CTRL = (*$CLOCKGEN_AUD_CLK_REF_CTRL & 0xffffffe4) | 0x00000000

  ## Set pixel clock source to SYSACLKIN
  set *$CLOCKGEN_PIX_CLK_CTRL |= 0x00000004
  set *$CLOCKGEN_PIX_CLK_CTRL = (*$CLOCKGEN_PIX_CLK_CTRL & 0xfffffffc) | 0x00000000
  set *$CLOCKGEN_PIX_CLK_CTRL &= 0xfffffffb

  ## Set DVP clock source to VIDINCLKIN
  set *$CLOCKGEN_DVP_CLK_CTRL |= 0x00000002
  set *$CLOCKGEN_DVP_CLK_CTRL = (*$CLOCKGEN_DVP_CLK_CTRL & 0xfffffffe) | 0x00000000
  set *$CLOCKGEN_DVP_CLK_CTRL &= 0xfffffffd

  ## Start PWM clock
  set *$CLOCKGEN_PWM_CLK_CTRL = (*$CLOCKGEN_PWM_CLK_CTRL & 0xfffffffe) | 0x00000000

  ## Set general purpose clock source to QSYNTH2 CLK1 (stopped)
  set *$CLOCKGEN_CKOUT_CTRL = (*$CLOCKGEN_CKOUT_CTRL & 0xfffffff8) | 0x00000004

  ## Lock CLOCKGEN
  set *$CLOCKGEN_CKG_LOCK = 0x0

  linkspeed 10MHz
end
##}}}

##{{{  MB376 SYSCONF Configuration
define mb376_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  set *$SYSCONF_SYS_CFG11 = 0x000000ff
  set *$SYSCONF_SYS_CFG12 = 0x00000005
  set *$SYSCONF_SYS_CFG13 = 0x00000000
end
##}}}

##{{{  MB376 EMI Configuration
define mb376_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Ensure EMI control registers are unlocked
##------------------------------------------------------------------------------

  set *$EMI_LOCK = 0x00000000

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000c
  set *$EMI_BANK4_BASEADDRESS = 0x0000000d
  set *$EMI_BANK5_BASEADDRESS = 0x0000000e

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for on-board Flash 16Mb"

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 2 - EPLD Registers and STi4629
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for EPLD Registers and STi4629 16Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x002046f9
  set *$EMI_BANK2_EMICONFIGDATA1 = 0xa5a00000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0xa5a20000
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - STEM (SMC91C111 card)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: STEM (SMC91C111) 32Mb"
  set *$EMI_BANK3_EMICONFIGDATA0 = 0x041086e9
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x0e024400
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x0e024400
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000


##------------------------------------------------------------------------------
## Bank 4 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 32Mb (4Mb accessible by ST40)"

##------------------------------------------------------------------------------
## Bank 5 - On-board SDRAM
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for on-board SDRAM 32Mb (8Mb accessible by ST40)"

  ## Strobe on falling edge, bus release width 1 cycle, 1 sub-bank of 256Mb, port size 32 bit
  set *$EMI_BANK5_EMICONFIGDATA0 = 0x0400044a
  set *$EMI_BANK5_EMICONFIGDATA1 = 0x007ffff0
  set *$EMI_BANK5_EMICONFIGDATA2 = 0x00000010
  set *$EMI_BANK5_EMICONFIGDATA3 = 0x00005fba

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  ## Pre cut 2.0
  ##set *$EMI_GENCFG = 0x00000118
  ## Post cut 2.0
  set *$EMI_GENCFG = 0x00000098

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  ## _ST_display (_procname) "EMI SDRAM CLOCK @ bus clock"
  set *$EMI_SDRAMCLKSEL = 0x00000000

  set *$EMI_CLKENABLE = 0x00000001
  set *$EMI_SDRAMNOPGEN = 0x00000001
  set *$EMI_REFRESHINIT = 0x000003e8
  set *$EMI_SDRAMMODEREG = 0x00000032
  ## _ST_cfg_sleep 1000
  set *$EMI_SDRAMINIT = 0x00000001
end
##}}}

##{{{  MB376 LMI Configuration
define mb376_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x05100243

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x00ef5c5a

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0c001a00

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0c001a00

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00002000/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00002000/sizeof(*$LMI_SDMR1))) = 0x0
  set *($LMI_SDMR0+(0x00000b18/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000b18/sizeof(*$LMI_SDMR1))) = 0x0
  ## _ST_cfg_sleep 1000
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *($LMI_SDMR0+(0x00000318/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000318/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB376 Memory
define mb376_memory_define
  memory-add Flash     0x00000000 16 ROM
  memory-add Flash     0x01000000 16 ROM
  memory-add EPLD_regs 0x02000000 16 RAM
  memory-add EMI_SDRAM 0x03800000 8 RAM
  memory-add LMI_SDRAM 0x04000000 128 RAM
end

define mb376_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x01000000 16 ROM
  sim_addmemory 0x03800000 8 RAM
  sim_addmemory 0x04000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb376_setup
  sti5528_define
  mb376_memory_define

  sti5528_si_regs

  mb376_clockgen_configure
  mb376_sysconf_configure
  mb376_emi_configure
  mb376_lmi_configure

  set *$CCN_CCR = 0x0000090d
end

document mb376_setup
Configure an STi5528-Mboard board
Usage: mb376_setup
end

define mb376sim_setup
  sti5528_define
  mb376_memory_define

  sti5528_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb376sim_setup
Configure a simulated STi5528-Mboard board
Usage: mb376sim_setup
end

define mb376_fsim_setup
  sti5528_fsim_core_setup
  mb376_sim_memory_define
end

document mb376_fsim_setup
Configure functional simulator for STi5528-Mboard board
Usage: mb376_fsim_setup
end

define mb376_psim_setup
  sti5528_psim_core_setup
  mb376_sim_memory_define
end

document mb376_psim_setup
Configure performance simulator for STi5528-Mboard board
Usage: mb376_psim_setup
end

define mb376_display_registers
  sti5528_display_si_regs
end

document mb376_display_registers
Display the STi5528 configuration registers
Usage: mb376_display_registers
end
