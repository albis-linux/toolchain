##------------------------------------------------------------------------------
## sh7751eval.cmd - SH7751 Evaluation Board
##------------------------------------------------------------------------------

##{{{  SH7751EVAL Configure
define sh7751eval_configure
##------------------------------------------------------------------------------
## Phase 1: Configure FRQCR register
##------------------------------------------------------------------------------
  set *$CPG_WTCNT = 0x5a00
  set *$CPG_WTCSR = 0xa567
  set *$CPG_FRQCR = 0x0e0a

##------------------------------------------------------------------------------
## Phase 2: Configure rest...
##------------------------------------------------------------------------------
  set *$BSC_BCR1 = 0x00080008
  set *$BSC_BCR2 = 0xbff8
  set *$BSC_WCR1 = 0x77771724
  set *$BSC_WCR2 = 0xfffe6e77
  set *$BSC_WCR3 = 0x07777000
  set *$BSC_MCR = 0x103103dc
  set *($BSC_SDMR3+(0x00cc/sizeof(*$BSC_SDMR3))) = 0x0
  set *$BSC_RTCNT = 0xa500
  set *$BSC_RFCR = 0xa400
  set *$BSC_RTCSR = 0xa510
  set *$BSC_MCR = 0x103103dc
  set *($BSC_SDMR3+(0x00cc/sizeof(*$BSC_SDMR3))) = 0x0
  set *$BSC_RTCNT = 0xa500
  set *$BSC_RTCOR = 0xa530
  set *$BSC_RFCR = 0xa400
  set *$BSC_RTCSR = 0xa510
end
##}}}

##{{{  SH7751EVAL Memory
define sh7751eval_memory_define
  memory-add Flash 0x00000000 32 ROM
  memory-add RAM   0x0c000000 32 RAM
end

define sh7751eval_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x0c000000 32 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define sh7751eval_setup
  sh7751_define
  sh7751eval_memory_define

  sh7751_si_regs

  sh7751eval_configure

  set *$CCN_CCR = 0x0000090d
end

document sh7751eval_setup
Configure an SH7751 Evaluation board
Usage: sh7751eval_setup
end

define sh7751evalsim_setup
  sh7751_define
  sh7751eval_memory_define

  sh7751_si_regs

  set *$CCN_CCR = 0x0000090d
end

document sh7751evalsim_setup
Configure a simulated SH7751 Evaluation board
Usage: sh7751evalsim_setup
end

define sh7751eval_fsim_setup
  sh7751_fsim_core_setup
  sh7751eval_sim_memory_define
end

document sh7751eval_fsim_setup
Configure functional simulator for SH7751 Evaluation board
Usage: sh7751eval_fsim_setup
end

define sh7751eval_psim_setup
  sh7751_psim_core_setup
  sh7751eval_sim_memory_define
end

document sh7751eval_psim_setup
Configure performance simulator for SH7751 Evaluation board
Usage: sh7751eval_psim_setup
end

define sh7751eval_display_registers
  sh7751_display_si_regs
end

document sh7751eval_display_registers
Display the SH7751 configuration registers
Usage: sh7751eval_display_registers
end
