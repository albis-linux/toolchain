################################################################################

define mb625
  source register40.cmd
  source display40.cmd
  source stv0498jtag.cmd
  source stv0498.cmd
  source mb625.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb625_setup $arg1
  else
    connectsh4le $arg0 mb625_setup "jtagpinout=st40 hardreset"
  end
end

document mb625
Connect to and configure an STV0498-Ref board
Usage: mb625 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb625bypass
  if ($argc > 1)
    mb625 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb625 $arg0 "jtagpinout=st40 jtagreset -inicommand mb625bypass_setup"
  end
end

document mb625bypass
Connect to and configure an STV0498-Ref board bypassing to the ST40
Usage: mb625bypass <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb625usb
  source register40.cmd
  source display40.cmd
  source stv0498jtag.cmd
  source stv0498.cmd
  source mb625.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb625_setup $arg1
  else
    connectsh4usble $arg0 mb625_setup "jtagpinout=st40 hardreset"
  end
end

document mb625usb
Connect to and configure an STV0498-Ref board
Usage: mb625usb <target>
where <target> is an ST Micro Connect USB name
end

define mb625bypassusb
  if ($argc > 1)
    mb625usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb625usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb625bypass_setup"
  end
end

document mb625bypassusb
Connect to and configure an STV0498-Ref board bypassing to the ST40
Usage: mb625bypassusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb625sim
  source register40.cmd
  source display40.cmd
  source stv0498.cmd
  source mb625.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb625_fsim_setup mb625sim_setup $arg0
  else
    connectsh4simle mb625_fsim_setup mb625sim_setup ""
  end
end

document mb625sim
Connect to and configure a simulated STV0498-Ref board
Usage: mb625sim
end

################################################################################

define mb625psim
  source register40.cmd
  source display40.cmd
  source stv0498.cmd
  source mb625.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb625_psim_setup mb625sim_setup $arg0
  else
    connectsh4psimle mb625_psim_setup mb625sim_setup ""
  end
end

document mb625psim
Connect to and configure a simulated STV0498-Ref board
Usage: mb625psim
end

################################################################################
