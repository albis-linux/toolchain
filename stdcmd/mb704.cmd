##------------------------------------------------------------------------------
## mb704.cmd - STi5197-Mboard Validation Platform MB704
##------------------------------------------------------------------------------

##{{{  MB704 PMB Configuration
define mb704se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb704seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb704se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 64
  sh4_set_pmb 1 0xa0 0x40 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB704 Memory
define mb704_memory_define
  memory-add Flash     0x00000000 32 ROM
  memory-add LMI_SDRAM 0x0c000000 64 RAM
end

define mb704_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x0c000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define mb704se_memory_define
  memory-add Flash     0x00000000 32 ROM
  memory-add LMI_SDRAM 0x40000000 64 RAM
end

define mb704se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x40000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb704sim_setup
  sti5197_define
  mb704_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb704sim_setup
Configure a simulated STi5197-Mboard board
Usage: mb704sim_setup
end

define mb704simse_setup
  sti5197_define
  mb704se_memory_define

  st40300_core_si_regs

  mb704se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb704simse_setup
Configure a simulated STi5197-Mboard board with the STi5197 in 32-bit SE mode
Usage: mb704simse_setup
end

define mb704simseuc_setup
  sti5197_define
  mb704se_memory_define

  st40300_core_si_regs

  mb704seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb704simseuc_setup
Configure a simulated STi5197-Mboard board with the STi5197 in 32-bit SE mode with uncached RAM mappings
Usage: mb704simseuc_setup
end

define mb704simse29_setup
  sti5197_define
  mb704se_memory_define

  st40300_core_si_regs

  mb704se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb704simse29_setup
Configure a simulated STi5197-Mboard board with the STi5197 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb704simse29_setup
end

define mb704_fsim_setup
  sti5197_fsim_core_setup
  mb704_sim_memory_define
end

document mb704_fsim_setup
Configure functional simulator for STi5197-Mboard board
Usage: mb704_fsim_setup
end

define mb704se_fsim_setup
  sti5197_fsim_core_setup
  mb704se_sim_memory_define
end

document mb704se_fsim_setup
Configure functional simulator for STi5197-Mboard board with the STi5197 in 32-bit SE mode
Usage: mb704se_fsim_setup
end

define mb704_psim_setup
  sti5197_psim_core_setup
  mb704_sim_memory_define
end

document mb704_psim_setup
Configure performance simulator for STi5197-Mboard board
Usage: mb704_psim_setup
end

define mb704se_psim_setup
  sti5197_psim_core_setup
  mb704se_sim_memory_define
end

document mb704se_psim_setup
Configure performance simulator for STi5197-Mboard board with the STi5197 in 32-bit SE mode
Usage: mb704se_psim_setup
end

define mb704_display_registers
  st40300_display_core_si_regs
end

document mb704_display_registers
Display the STi5197 configuration registers
Usage: mb704_display_registers
end
