################################################################################

define db412
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source db412.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 db412_setup $arg1
  else
    connectsh4le $arg0 db412_setup ""
  end
end

document db412
Connect to and configure an ST407750 Overdrive board
Usage: db412 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define db412usb
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source db412.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 db412_setup $arg1
  else
    connectsh4usble $arg0 db412_setup ""
  end
end

document db412usb
Connect to and configure an ST407750 Overdrive board
Usage: db412usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define db412sim
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source db412.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle db412_fsim_setup db412sim_setup $arg0
  else
    connectsh4simle db412_fsim_setup db412sim_setup ""
  end
end

document db412sim
Connect to and configure a simulated ST407750 Overdrive board
Usage: db412sim
end

################################################################################

define db412psim
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source db412.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle db412_psim_setup db412sim_setup $arg0
  else
    connectsh4psimle db412_psim_setup db412sim_setup ""
  end
end

document db412psim
Connect to and configure a simulated ST407750 Overdrive board
Usage: db412psim
end

################################################################################
