################################################################################

source sh4targets-mb411stb7109.cmd

################################################################################

define mb411stb7109bypass27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109bypass $arg0 $arg1
  else
    mb411stb7109bypass $arg0
  end
end

define mb411stb7109stmmx27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109stmmx $arg0 $arg1
  else
    mb411stb7109stmmx $arg0
  end
end

################################################################################

define mb411stb7109bypass30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109bypass $arg0 $arg1
  else
    mb411stb7109bypass $arg0
  end
end

define mb411stb7109stmmx30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109stmmx $arg0 $arg1
  else
    mb411stb7109stmmx $arg0
  end
end

################################################################################

source sh4targets-mb411stb7109cutX.cmd

################################################################################

define mb411stb7109cut11bypass
  if ($argc > 1)
    mb411stb7109cutXbypass 11 $arg0 $arg1
  else
    mb411stb7109cutXbypass 11 $arg0
  end
end

define mb411stb7109cut11stmmx
  if ($argc > 1)
    mb411stb7109cutXstmmx 11 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 11 $arg0
  end
end

define mb411stb7109cut20bypass
  if ($argc > 1)
    mb411stb7109cutXbypass 20 $arg0 $arg1
  else
    mb411stb7109cutXbypass 20 $arg0
  end
end

define mb411stb7109cut20stmmx
  if ($argc > 1)
    mb411stb7109cutXstmmx 20 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 20 $arg0
  end
end

define mb411stb7109cut30bypass
  if ($argc > 1)
    mb411stb7109cutXbypass 30 $arg0 $arg1
  else
    mb411stb7109cutXbypass 30 $arg0
  end
end

define mb411stb7109cut30stmmx
  if ($argc > 1)
    mb411stb7109cutXstmmx 30 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 30 $arg0
  end
end

################################################################################

define mb411stb7109cut11bypass27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109cutXbypass 11 $arg0 $arg1
  else
    mb411stb7109cutXbypass 11 $arg0
  end
end

define mb411stb7109cut11stmmx27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109cutXstmmx 11 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 11 $arg0
  end
end

define mb411stb7109cut20bypass27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109cutXbypass 20 $arg0 $arg1
  else
    mb411stb7109cutXbypass 20 $arg0
  end
end

define mb411stb7109cut20stmmx27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109cutXstmmx 20 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 20 $arg0
  end
end

define mb411stb7109cut30bypass27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109cutXbypass 30 $arg0 $arg1
  else
    mb411stb7109cutXbypass 30 $arg0
  end
end

define mb411stb7109cut30stmmx27MHz
  set $_mb411stb7109extclk = 27
  if ($argc > 1)
    mb411stb7109cutXstmmx 30 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 30 $arg0
  end
end

################################################################################

define mb411stb7109cut11bypass30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109cutXbypass 11 $arg0 $arg1
  else
    mb411stb7109cutXbypass 11 $arg0
  end
end

define mb411stb7109cut11stmmx30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109cutXstmmx 11 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 11 $arg0
  end
end

define mb411stb7109cut20bypass30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109cutXbypass 20 $arg0 $arg1
  else
    mb411stb7109cutXbypass 20 $arg0
  end
end

define mb411stb7109cut20stmmx30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109cutXstmmx 20 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 20 $arg0
  end
end

define mb411stb7109cut30bypass30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109cutXbypass 30 $arg0 $arg1
  else
    mb411stb7109cutXbypass 30 $arg0
  end
end

define mb411stb7109cut30stmmx30MHz
  set $_mb411stb7109extclk = 30
  if ($argc > 1)
    mb411stb7109cutXstmmx 30 $arg0 $arg1
  else
    mb411stb7109cutXstmmx 30 $arg0
  end
end

################################################################################
