##------------------------------------------------------------------------------
## mb317.cmd - ST40GX1 Evaluation Board MB317
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB317 EMI Configuration
define mb317_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x00000010
  set *$EMI_BANK4_BASEADDRESS = 0x00000018
  set *$EMI_BANK5_BASEADDRESS = 0x0000001c

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - On-board SDRAM
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for on-board SDRAM 16Mb"

  ## Strobe on rising edge, bus release width 1 cycle, 1 sub-bank of 128Mb, column address width 8, port size 32 bit
  set *$EMI_BANK1_EMICONFIGDATA0 = 0x0000032a

  ## Page address mask
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x0001fffc

  ## Pre-charge time 2 cycles
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x00000020

  ## ModeSetDelay 3 cycles, RefreshTime 2 cycles, ActivtoRD 3 cycles, ActivtoWR 3 cycles, CAS Latency 3 cycles, 4 banks, WRRecovTime 1 cycle
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00002929

##------------------------------------------------------------------------------
## Bank 2 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Undefined 32Mb"

##------------------------------------------------------------------------------
## Bank 3 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Undefined 32Mb"

##------------------------------------------------------------------------------
## Bank 4 - MPX Mode (unused)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for MPX Mode 16Mb (unused)"

  set *$EMI_BANK4_EMICONFIGDATA0 = 0x0400064b
  set *$EMI_BANK4_EMICONFIGDATA1 = 0x00000000
  set *$EMI_BANK4_EMICONFIGDATA2 = 0x00000000
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 5 - EPLD Registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for EPLD Registers 15Mb"

  set *$EMI_BANK5_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK5_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK5_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK5_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000004

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  ## _ST_display (_procname) "EMI SDRAM CLOCK @ 1/1 bus clock"
  set *$EMI_SDRAMCLKSEL = 0x00000000

  ## _ST_display (_procname) "EMI MPX CLOCK @ 1/1 bus clock"
  set *$EMI_MPXCLKSEL = 0x00000000

  set *$EMI_CLKENABLE = 0x00000001
  set *$EMI_SDRAMNOPGEN = 0x00000001
  set *$EMI_REFRESHINIT = 0x000003e8
  set *$EMI_SDRAMMODEREG = 0x00000032
  ## _ST_cfg_sleep 1000
  set *$EMI_SDRAMINIT = 0x00000001
end
##}}}

##{{{  MB317 LMI Configuration
##{{{  MB317 LMI DDR Configuration
define mb317_lmi_ddr_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM from 4 off 8Mb x 16 x 4 bank devices"

##------------------------------------------------------------------------------
## Program clock generator registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM CLOCK @ 2/3 processor clock"
  set *$CLOCKGENA_CLK4CR = 0x00000008
  set *$CLOCKGENA_CLK4CR = (0x00000003|0x00000008)
  set *$CLOCKGENA_CLK4CR = 0x00000003

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  ## NOTE:
  ##   bit 8 = 0 for SSTL (DDR), 1 for LVTTL (PC-SDRAM)
  ##   bit 9 = 0 for internal ref voltage, 1 for external ref voltage
  ##   bit 10 = 1 to by pass ECLK180 retime
  ##   bit 11 = 1 for PC-SDRAM, 0 for DDR notLMICOMP25_EN
  ##   bit 12 = 1 for PC-SDRAM, 0 for DDR LMICOMP33_EN
  ##   bits [13,14] = LVTTL drive strength for data & data strobe pads
  ##   bits [15,16] = LVTTL drive strength for address & control pads

  set *$SYSCONF_SYS_CON2_0 = 0x00000000

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x01000283

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x00001128

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0c001500

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0c001500

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00002000/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00002000/sizeof(*$LMI_SDMR1))) = 0x0
  set *($LMI_SDMR0+(0x00000910/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000910/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *($LMI_SDMR0+(0x00000110/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000110/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB317 LMI Configuration
define mb317_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for 64 bit SDRAM from 4 off 2Mb x 16 x 4 bank devices"

##------------------------------------------------------------------------------
## Program clock generator registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM CLOCK @ 2/3 processor clock"
  set *$CLOCKGENA_CLK4CR = 0x00000008
  set *$CLOCKGENA_CLK4CR = (0x00000003|0x00000008)
  set *$CLOCKGENA_CLK4CR = 0x00000003

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  ## NOTE:
  ##   bit 8 = 0 for SSTL (DDR), 1 for LVTTL (PC-SDRAM)
  ##   bit 9 = 0 for internal ref voltage, 1 for external ref voltage
  ##   bit 10 = 1 to by pass ECLK180 retime
  ##   bit 11 = 1 for PC-SDRAM, 0 for DDR notLMICOMP25_EN
  ##   bit 12 = 1 for PC-SDRAM, 0 for DDR LMICOMP33_EN
  ##   bits [13,14] = LVTTL drive strength for data & data strobe pads
  ##   bits [15,16] = LVTTL drive strength for address & control pads

  set *$SYSCONF_SYS_CON2_0 = 0x00001900

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x01000281

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x0000026f

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0c001500

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0c001500

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00000190/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000190/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000000
end
##}}}
##}}}

##{{{  MB317 PCI Configuration
define mb317_pci_configure
  ## _ST_display (_procname) "Configuring PCI to 33MHz"

  set *$CLOCKGENA_PLL2_MUXCR = 0x1
end
##}}}

##{{{  ST40GX1 IO Configuration
define st40gx1_io_configure
  ## _ST_display (_procname) "Configuring access to IO block"

  ## Control memory bridge 4 - I/O block initiator:
  ## - asynchronous, one cycle FIFO latency, software reset disabled, strobe required to latch the data
  set *$SYSCONF_SYS_CON1_0 = 0x8e000000

  ## Control memory bridge 5 - I/O block target:
  ## - asynchronous, one cycle FIFO latency, software reset disabled, strobe required to latch the data
  set *$SYSCONF_SYS_CON1_1 = 0x00000047
end
##}}}

##{{{  MB317 Memory
define mb317_memory_define
  memory-add Flash        0x00000000 16 ROM
  memory-add EMI_SDRAM    0x01000000 16 RAM
  memory-add MPX_unused   0x06000000 16 RAM
  memory-add EPLD_regs    0x07000000 15 RAM
  memory-add LMI_SDRAM    0x08000000 64 RAM
end

define mb317_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x01000000 16 RAM
  sim_addmemory 0x08000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  MB317A Setup
define mb317a_pci_configure
  ## _ST_display (_procname) "Configuring PCI for notPREQ problem"

  set *$PIO3_PC0 = 0x00
  set *$PIO3_PC1 = 0x55
  set *$PIO3_PC2 = 0x00
  set *$PIO3_POUT = 0x55
end

define mb317a_setup
  st40gx1_define
  mb317_memory_define

  st40gx1_si_regs

  mb317_emi_configure
  mb317_lmi_configure
  mb317_pci_configure
  st40gx1_io_configure

  mb317a_pci_configure

  set *$CCN_CCR = 0x0000090d
end

document mb317a_setup
Configure an ST40GX1 Evaluation board, revision A
Usage: mb317a_setup
end

define mb317asim_setup
  st40gx1_define
  mb317_memory_define

  st40gx1_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb317asim_setup
Configure a simulated ST40GX1 Evaluation board, revision A
Usage: mb317asim_setup
end

define mb317a_fsim_setup
  st40gx1_fsim_core_setup
  mb317_sim_memory_define
end

document mb317a_fsim_setup
Configure functional simulator for ST40GX1 Evaluation board
Usage: mb317a_fsim_setup
end

define mb317a_psim_setup
  st40gx1_psim_core_setup
  mb317_sim_memory_define
end

document mb317a_psim_setup
Configure performance simulator for ST40GX1 Evaluation board
Usage: mb317a_psim_setup
end

define mb317a_display_registers
  st40gx1_display_si_regs
end

document mb317a_display_registers
Display the ST40GX1 configuration registers
Usage: mb317a_display_registers
end
##}}}

##{{{  MB317B Setup
define mb317b_setup
  st40gx1_define
  mb317_memory_define

  st40gx1_si_regs

  mb317_emi_configure
  mb317_lmi_ddr_configure
  mb317_pci_configure
  st40gx1_io_configure

  set *$CCN_CCR = 0x0000090d
end

document mb317b_setup
Configure an ST40GX1 Evaluation board, revision B
Usage: mb317b_setup
end

define mb317bsim_setup
  st40gx1_define
  mb317_memory_define

  st40gx1_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb317bsim_setup
Configure a simulated ST40GX1 Evaluation board, revision B
Usage: mb317bsim_setup
end

define mb317b_fsim_setup
  st40gx1_fsim_core_setup
  mb317_sim_memory_define
end

document mb317b_fsim_setup
Configure functional simulator for ST40GX1 Evaluation board
Usage: mb317b_fsim_setup
end

define mb317b_psim_setup
  st40gx1_psim_core_setup
  mb317_sim_memory_define
end

document mb317b_psim_setup
Configure performance simulator for ST40GX1 Evaluation board
Usage: mb317b_psim_setup
end

define mb317b_display_registers
  st40gx1_display_si_regs
end

document mb317b_display_registers
Display the ST40GX1 configuration registers
Usage: mb317b_display_registers
end
##}}}
