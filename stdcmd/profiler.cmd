set $profiler_enabled = 0
keep-variable $profiler_enabled

define profiler
  if (!$profiler_enabled)
    printf "Profiler plugin not installed\n"
  else
    if ($argc == 0)
      callplugin profiler
    end
    if ($argc == 1)
      callplugin profiler $arg0
    end
    if ($argc == 2)
      callplugin profiler $arg0 $arg1
    end
    if ($argc == 3)
      callplugin profiler $arg0 $arg1 $arg2
    end
    if ($argc == 4)
      callplugin profiler $arg0 $arg1 $arg2 $arg3
    end
    if ($argc > 4)
      callplugin profiler help
    end
  end
end

document profiler
Use "profiler help"
end

define enable_profiler
  if (!$profiler_enabled)
    installplugin profiler libsh4profiler.so
    set $profiler_enabled = 1
  else
    printf "Profiler plugin already enabled\n"
  end
end

document enable_profiler
Enable the profiler plugin
Usage: enable_profiler
end
