##------------------------------------------------------------------------------
## hdk7106.cmd - STi7106-HDK Reference Platform
##------------------------------------------------------------------------------

##{{{  HDK7106 PMB Configuration
define hdk7106se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk7106seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk7106se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512
  sh4_set_pmb 1 0xa0 0x40 512 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  HDK7106 Memory
define hdk7106_memory_define
  memory-add Flash     0x00000000 128 ROM
  memory-add LMI_SDRAM 0x0c000000 256 RAM
end

define hdk7106_sim_memory_define
  sim_addmemory 0x00000000 128 ROM
  sim_addmemory 0x0c000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define hdk7106se_memory_define
  memory-add Flash     0x00000000 128 ROM
  memory-add LMI_SDRAM 0x40000000 512 RAM
end

define hdk7106se_sim_memory_define
  sim_addmemory 0x00000000 128 ROM
  sim_addmemory 0x40000000 512 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define hdk7106sim_setup
  sti7106_define
  hdk7106_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document hdk7106sim_setup
Configure a simulated STi7106-HDK board
Usage: hdk7106sim_setup
end

define hdk7106simse_setup
  sti7106_define
  hdk7106se_memory_define

  st40300_core_si_regs

  hdk7106se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk7106simse_setup
Configure a simulated STi7106-HDK board with the STi7106 in 32-bit SE mode
Usage: hdk7106simse_setup
end

define hdk7106simseuc_setup
  sti7106_define
  hdk7106se_memory_define

  st40300_core_si_regs

  hdk7106seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk7106simseuc_setup
Configure a simulated STi7106-HDK board with the STi7106 in 32-bit SE mode with uncached RAM mappings
Usage: hdk7106simseuc_setup
end

define hdk7106simse29_setup
  sti7106_define
  hdk7106se_memory_define

  st40300_core_si_regs

  hdk7106se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk7106simse29_setup
Configure a simulated STi7106-HDK board with the STi7106 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: hdk7106simse29_setup
end

define hdk7106_fsim_setup
  sti7106_fsim_core_setup
  hdk7106_sim_memory_define
end

document hdk7106_fsim_setup
Configure functional simulator for STi7106-HDK board
Usage: hdk7106_fsim_setup
end

define hdk7106se_fsim_setup
  sti7106_fsim_core_setup
  hdk7106se_sim_memory_define
end

document hdk7106se_fsim_setup
Configure functional simulator for STi7106-HDK board with the STi7106 in 32-bit SE mode
Usage: hdk7106se_fsim_setup
end

define hdk7106_psim_setup
  sti7106_psim_core_setup
  hdk7106_sim_memory_define
end

document hdk7106_psim_setup
Configure performance simulator for STi7106-HDK board
Usage: hdk7106_psim_setup
end

define hdk7106se_psim_setup
  sti7106_psim_core_setup
  hdk7106se_sim_memory_define
end

document hdk7106se_psim_setup
Configure performance simulator for STi7106-HDK board with the STi7106 in 32-bit SE mode
Usage: hdk7106se_psim_setup
end

define hdk7106_display_registers
  st40300_display_core_si_regs
end

document hdk7106_display_registers
Display the STi7106 configuration registers
Usage: hdk7106_display_registers
end
