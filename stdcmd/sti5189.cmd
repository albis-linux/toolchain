##
## STi5189 device description (on-chip memory and registers)
##

define sti5189_memory_define
  ## Configuration registers in P4
  memory-add STOREQ_data   0xe0000000 64 DEV
  memory-add ICACHE_addr   0xf0000000 16 DEV
  memory-add ICACHE_data   0xf1000000 16 DEV
  memory-add ITLB_addr     0xf2000000 16 DEV
  memory-add ITLB_data     0xf3000000 16 DEV
  memory-add OCACHE_addr   0xf4000000 16 DEV
  memory-add OCACHE_data   0xf5000000 16 DEV
  memory-add UTLB_PMB_addr 0xf6000000 16 DEV
  memory-add UTLB_PMB_data 0xf7000000 16 DEV
  memory-add CORE_regs     0xfc000000 64 DEV
end

define sti5189_define
  ## processor ST40
  ## chip STi5189

  sti5189_memory_define
end

define sti5189_fsim_core_setup
  sim_command "config +cpu.sh4_family=fpu"
  sim_command "config +cpu.sh7751_features=false"
  sim_command "config +cpu.enable_sh4_300_isa=true"
  sim_command "config +cpu.icache.nr_partitions=2"
  sim_command "config +cpu.icache.lines_per_part=512"
  sim_command "config +cpu.icache.cache_line_size=32"
  sim_command "config +cpu.dcache.nr_partitions=2"
  sim_command "config +cpu.dcache.lines_per_part=512"
  sim_command "config +cpu.dcache.cache_line_size=32"
  sim_reset
end

define sti5189_psim_core_setup
  sim_command "config +cpu.cache_store_on_fill=true"
  sim_command "config +cpu.use_long_pipe=true"
  sim_command "config +cpu.fetch.fetch_queue=8"
  sim_command "config +cpu.fetch.fetch_width=8"
  sim_command "config +cpu.fetch.fetch_delay=2"
  sim_command "config +cpu.force_single_issue=false"
  sim_command "config +cpu.store_writeback_delay=2"
  sim_command "config +cpu.one_cyc_reg_move=false"
  sim_command "config +cpu.one_cyc_const_move=false"
  sim_command "config +cpu.take_br_class_branches_in_e1=true"
  sim_command "config +cpu.serialise_pr_load=false"
  sim_command "config +cpu.macl_forwarding_stage=execute3"
  sim_command "config +cpu.store_depcheck_in_e3=true"
  sim_command "config +cpu.use_branch_cache=true"
  sim_command "config +cpu.branch_cache.entries=512"
  sim_command "config +cpu.branch_cache.associativity=1"
  sim_command "config +cpu.branch_cache.replacement=lru"
  sim_command "config +cpu.branch_cache.cache_type=tri"
  sim_command "config +cpu.return_stack=4"
  sim_command "config +use_tobu=false"
  sti5189_fsim_core_setup
end
