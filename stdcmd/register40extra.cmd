################################################################################
## Core SH4 control registers
################################################################################

##{{{  SH4 CCN
## Core control registers (common to all SH4 variants)
define sh4_ccn_x_regs
  __shx_reg_8  $CCN_ASEEVT 0xff000080
  __shx_reg_8  $CCN_BASRS 0xff000090
  __shx_reg_8  $CCN_BASRT 0xff000094
  __shx_reg_8  $CCN_BASRU 0xff000098
  __shx_reg_8  $CCN_BASRV 0xff00009c
  __shx_reg_32 $CCN_IOBAR 0xff0000a0
  __shx_reg_32 $CCN_IOBDR 0xff0000a4

  __shx_reg_16 $CCN_PMCR1 0xff000084
  __shx_reg_16 $CCN_PMCR2 0xff000088
  __shx_reg_32 $CCN_PMCTR1H 0xff100004
  __shx_reg_32 $CCN_PMCTR1L 0xff100008
  __shx_reg_32 $CCN_PMCTR2H 0xff10000c
  __shx_reg_32 $CCN_PMCTR2L 0xff100010

  __shx_reg_32 $CCN_TRMCR 0xff2000bc
  __shx_reg_32 $CCN_TRMBA 0xff100014
  __shx_reg_32 $CCN_TRMCTR 0xff100018
end
##}}}

##{{{  SH4 UBC
## User Break Controller control registers (common to all SH4 variants)
define sh4_ubc_x_regs
  __shx_reg_16 $UBC_BRCRA 0xff2000c0
  __shx_reg_16 $UBC_BRCRS 0xff2000b8
  __shx_reg_32 $UBC_BARS 0xff200080
  __shx_reg_8  $UBC_BAMRS 0xff200084
  __shx_reg_16 $UBC_BBRS 0xff200088
  __shx_reg_8  $UBC_BASRS 0xff000090
  __shx_reg_32 $UBC_BART 0xff20008c
  __shx_reg_8  $UBC_BAMRT 0xff200090
  __shx_reg_16 $UBC_BBRT 0xff200094
  __shx_reg_8  $UBC_BASRT 0xff000094
  __shx_reg_32 $UBC_BARU 0xff200098
  __shx_reg_8  $UBC_BAMRU 0xff20009c
  __shx_reg_16 $UBC_BBRU 0xff2000a0
  __shx_reg_8  $UBC_BASRU 0xff000098
  __shx_reg_32 $UBC_BARV 0xff2000a4
  __shx_reg_8  $UBC_BAMRV 0xff2000a8
  __shx_reg_16 $UBC_BBRV 0xff2000ac
  __shx_reg_8  $UBC_BASRV 0xff00009c
  __shx_reg_32 $UBC_BDRV 0xff2000b0
  __shx_reg_32 $UBC_BDMRV 0xff2000b4
end
##}}}

##{{{  SH4 UDI
## User Debug Interface control registers (common to all SH4 variants)
define sh4_udi_x_regs
  __shx_reg_16 $UDI_SDSR 0xfff00004
  __shx_reg_16 $UDI_SDAR 0xfff0000c
  __shx_reg_16 $UDI_SDARE 0xfff00010
end
##}}}

################################################################################

define sh4_x_regs
  sh4_ccn_x_regs
  sh4_ubc_x_regs
  sh4_udi_x_regs
end
