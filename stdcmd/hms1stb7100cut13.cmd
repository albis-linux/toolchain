##------------------------------------------------------------------------------
## hms1stb7100cut13.cmd - Simple Devices HMS1
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  HMS1 (STb7100) CLOCKGEN Configuration
define hms1stb7100_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  linkspeed 1.25MHz

  ## Set PLL0 to 531MHz
  stb7100_set_clockgen_a_pll0 0x06 0x3b 0x0
  ## Set PLL1 to 400MHz
  stb7100_set_clockgen_a_pll1 0x1b 0xc8 0x0

  ## Restore default link speed (5MHz maximum for TapMux)
  if ($_stmmxmode)
    linkspeed 5MHz
  else
    linkspeed 10MHz
  end
end
##}}}

##{{{  HMS1 (STb7100) SYSCONF Configuration
define hms1stb7100_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  set *$SYSCONF_SYS_CFG11 = 0x080780c0

  while ((*$SYSCONF_SYS_STA12 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  while ((*$SYSCONF_SYS_STA13 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  set *$SYSCONF_SYS_CFG12 = 0x4000000f | (0xf << 12) | (0xf << 23)
  set *$SYSCONF_SYS_CFG13 = 0x4000000f | (0xf << 12) | (0xf << 23)

  set *$SYSCONF_SYS_CFG14 = (1 << 18) | (0x50 << 20)
  set *$SYSCONF_SYS_CFG15 = (1 << 19) | (0x40 << 20)

  set *$SYSCONF_SYS_CFG20 = (1 << 18) | (0x50 << 20)
  set *$SYSCONF_SYS_CFG21 = (1 << 19) | (0x40 << 20)
end
##}}}

##{{{  HMS1 (STb7100) EMI Configuration
define hms1stb7100_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000a
  set *$EMI_BANK4_BASEADDRESS = 0x0000000c

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016d1
  ## set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d000000
  ## set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d000000
  ## Modified for Simple Devices HMS1 hardware
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - SMSC 91C117 (Simple Devices HMS1 hardware)
##------------------------------------------------------------------------------

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x041086f1
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x93001111
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x91001111
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 2 - DVBCI
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for DVBCI 8Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x002046f9
  set *$EMI_BANK2_EMICONFIGDATA1 = 0xa5a00000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0xa5a20000
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - ATAPI
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for ATAPI 8Mb"

  ##set *$EMI_BANK3_EMICONFIGDATA0 = 0x00021791
  ##set *$EMI_BANK3_EMICONFIGDATA1 = 0x08004141
  ##set *$EMI_BANK3_EMICONFIGDATA2 = 0x08004141
  ##set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

  # Run the ATAPI interface within it's timing specification!
  set *$EMI_BANK3_EMICONFIGDATA0 = 0x00200791
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - EPLD Registers and LAN91C111
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for EPLD Registers and LAN91C111 16Mb"

  set *$EMI_BANK4_EMICONFIGDATA0 = 0x042086f1
  set *$EMI_BANK4_EMICONFIGDATA1 = 0x88112111
  set *$EMI_BANK4_EMICONFIGDATA2 = 0x88112211
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  ## Unlock Flash on HMS1 board

  set *(0xB8022020) = 0x00
  set *(0xB8022030) = 0x20
  set *(0xB8022040) = 0x00
  set *(0xB8022004) = 0x20

  set *$EMI_GENCFG = 0x00000010
end
##}}}

##{{{  HMS1 (STb7100) LMI Configuration
define hms1stb7100_lmisys_configure
  ## _ST_display (_procname) "Configuring LMI-SYS for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMISYS_MIM_0 = 0x861a0247
  set *$LMISYS_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMISYS_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMISYS_SDRA0_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMISYS_SDRA1_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000003
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SDMR0 = 0x00000400
  set *$LMISYS_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SDMR0 = 0x00000033
  set *$LMISYS_SCR_0 = 0x00000000
end

define hms1stb7100_lmivid_configure
  ## _ST_display (_procname) "Configuring LMI-VID for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMIVID_MIM_0 = 0x861a0247
  set *$LMIVID_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMIVID_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMIVID_SDRA0_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMIVID_SDRA1_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000003
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SDMR0 = 0x00000400
  set *$LMIVID_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SDMR0 = 0x00000033
  set *$LMIVID_SCR_0 = 0x00000000
end
##}}}

##{{{  HMS1 (STb7100) Memory
define hms1stb7100_memory_define
  memory-add Flash        0x00000000 8 ROM
  memory-add LMISYS_SDRAM 0x04000000 64 RAM
  memory-add LMIVID_SDRAM 0x10000000 64 RAM
end

define hms1stb7100_sim_memory_define
  sim_addmemory 0x04000000 64 RAM
  sim_addmemory 0x10000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  HMS1 (STb7100) Bypass Configuration
define hms1stb7100bypass_setup
  set $_hms1stb7100bypass_setup_argc = $argc

  enable_jtag

  set $_stmmxmode = 0

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset STb7100 leaving ST40 in reset hold
  jtag ntrst=0
  _jtagSleep 20
  jtag nrst=0
  _jtagSleep 20
  jtag asebrk=0
  _jtagSleep 20
  jtag nrst=1
  _jtagSleep 20
  jtag asebrk=1
  _jtagSleep 20
  jtag ntrst=1

  ## Reset TMC to bypass to TMC
  jtag tck=00 ntrst=01 tms=00 tdo=00

  if ($_hms1stb7100bypass_setup_argc > 0)
  $arg0
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TMC and then bypass to ST40
  jtag tck=01010 ntrst=00011 tms=00000 tdo=01111

  ## Normal control of JTAG
  jtag mode=normal
end

define hms1stb7100bypass_setup_attach
  set $_hms1stb7100bypass_setup_attach_argc = $argc

  enable_jtag

  set $_stmmxmode = 0

  if ($_hms1stb7100bypass_setup_attach_argc > 0)
  $arg0
  end

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TMC and then bypass to ST40
  jtag tck=01010 ntrst=00011 tms=00000 tdo=01111

  ## Normal control of JTAG
  jtag mode=normal
end
##}}}

##{{{  HMS1 (STb7100) STMMX Configuration
define hms1stb7100stmmx_setup
  set $_hms1stb7100stmmx_setup_argc = $argc

  enable_jtag

  set $_stmmxmode = 1

  ## Maximum TapMux link speed is 5MHz
  linkspeed 5MHz

  ## Manual control of JTAG (singleshot TCK)
  jtag mode=singleshot

  stmmxInitialState

  ## Disable STMMX TAP multiplexor
  stmmxWriteIR_TAPMUX
  stmmxWriteDR 1 0x00

  ## Set ST40 port as STMMX master channel
  stmmxWriteIR_BYPASS
  stmmxWriteDR 4 0x02
  stmmxWriteIR_RESET
  stmmxWriteDR 4 0x02

  ## Set STMMX clock to 40MHz
  ##stmmxWriteICS9161 0x007380a1
  ## Set STMMX clock to 47MHz
  stmmxWriteICS9161 0x0077f4a5

  ## Allocate channel slots
  stmmxWriteIR_ALLOC_WR
  ## Channels 1, 2 ([1, 2])*
  ##stmmxWriteDR 32 0x99999999
  ## Channels 1, 2, 3 ([1, 2], [3, 1], [2, 3])* (NB: last slot cannot be same as first)
  ##stmmxWriteDR 32 0xb9e79e79
  ## Channels 1, 2, 3 ([1, 2], [1, 3])*
  stmmxWriteDR 32 0xd9d9d9d9
  ## Channels 0, 1, 2, 3
  ##stmmxWriteDR 32 0xe4e4e4e4

  ## Reset STb7100 leaving ST40 in reset hold
  jtag stmmx=1 ntrst=0
  _jtagSleep 20
  jtag stmmx=1 nrst=0
  _jtagSleep 20
  stmmxWriteIR_ASEBRK
  stmmxWriteDR 3 0x04
  _jtagSleep 20
  jtag stmmx=1 nrst=1
  _jtagSleep 20
  stmmxWriteIR_ASEBRK
  stmmxWriteDR 3 0x05
  _jtagSleep 20
  jtag stmmx=1 ntrst=1

  ## Manual control of JTAG (manual TCK)
  jtag mode=manual

  ## Reset TMC to bypass to TMC
  jtag stmmx=1 tck=00 ntrst=01 tms=00 tdo=00

  if ($_hms1stb7100stmmx_setup_argc > 0)
  $arg0
  end

  ## Manual control of JTAG (singleshot TCK)
  jtag mode=singleshot

  ## Enable STMMX TAP multiplexor
  stmmxWriteIR_TAPMUX
  stmmxWriteDR 1 0x01

  ## Bypass STMMX to STb7100
  jtag stmmx=1

  ## Normal control of JTAG
  jtag mode=normal
end
##}}}

define hms1stb7100_setup
  stb7100_define
  hms1stb7100_memory_define

  printf "Local setup for STi7100 cut version 1.3\n"

  stb7100_si_regs

  hms1stb7100_clockgen_configure
  hms1stb7100_sysconf_configure
  hms1stb7100_emi_configure
  hms1stb7100_lmisys_configure
  hms1stb7100_lmivid_configure

  set *$CCN_CCR = 0x8000090b
end

document hms1stb7100_setup
Configure an STb7100-Mboard board
Usage: hms1stb7100_setup
end

define hms1stb7100sim_setup
  stb7100_define
  hms1stb7100_memory_define

  stb7100_si_regs

  set *$CCN_CCR = 0x8000090b
end

document hms1stb7100sim_setup
Configure a simulated STb7100-Mboard board
Usage: hms1stb7100sim_setup
end

define hms1stb7100_fsim_setup
  stb7100_fsim_core_setup
  hms1stb7100_sim_memory_define
end

document hms1stb7100_fsim_setup
Configure functional simulator for STb7100-Mboard board
Usage: hms1stb7100_fsim_setup
end

define hms1stb7100_psim_setup
  stb7100_psim_core_setup
  hms1stb7100_sim_memory_define
end

document hms1stb7100_psim_setup
Configure performance simulator for STb7100-Mboard board
Usage: hms1stb7100_psim_setup
end

define hms1stb7100_display_registers
  stb7100_display_si_regs
end

document hms1stb7100_display_registers
Display the STb7100 configuration registers
Usage: hms1stb7100_display_registers
end

