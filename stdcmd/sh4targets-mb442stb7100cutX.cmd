################################################################################

define mb442stb7100cutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb442stb7100cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb442stb7100_setup $arg2
  else
    connectsh4le $arg1 mb442stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb442stb7100cutX
Connect to and configure an STb7100-Ref board
Usage: mb442stb7100cutX <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb442stb7100cutXbypass
  if ($argc > 2)
    mb442stb7100cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb442stb7100cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb442stb7100bypass_setup"
  end
end

document mb442stb7100cutXbypass
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442stb7100cutXbypass <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb442stb7100cutXstmmx
  if ($argc > 2)
    mb442stb7100cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb442stb7100cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb442stb7100stmmx_setup"
  end
end

document mb442stb7100cutXstmmx
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442stb7100cutXstmmx <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb442cutX
  if ($argc > 2)
    mb442stb7100cutX $arg0 $arg1 $arg2
  else
    mb442stb7100cutX $arg0 $arg1
  end
end

document mb442cutX
Connect to and configure an STb7100-Ref board
Usage: mb442cutX <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb442cutXbypass
  if ($argc > 2)
    mb442stb7100cutXbypass $arg0 $arg1 $arg2
  else
    mb442stb7100cutXbypass $arg0 $arg1
  end
end

document mb442cutXbypass
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442cutXbypass <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb442cutXstmmx
  if ($argc > 2)
    mb442stb7100cutXstmmx $arg0 $arg1 $arg2
  else
    mb442stb7100cutXstmmx $arg0 $arg1
  end
end

document mb442cutXstmmx
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442cutXstmmx <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################

define stb7100refcutX
  if ($argc > 2)
    mb442stb7100cutX $arg0 $arg1 $arg2
  else
    mb442stb7100cutX $arg0 $arg1
  end
end

document stb7100refcutX
Connect to and configure an STb7100-Ref board
Usage: stb7100refcutX <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define stb7100refcutXbypass
  if ($argc > 2)
    mb442stb7100cutXbypass $arg0 $arg1 $arg2
  else
    mb442stb7100cutXbypass $arg0 $arg1
  end
end

document stb7100refcutXbypass
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: stb7100refcutXbypass <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define stb7100refcutXstmmx
  if ($argc > 2)
    mb442stb7100cutXstmmx $arg0 $arg1 $arg2
  else
    mb442stb7100cutXstmmx $arg0 $arg1
  end
end

document stb7100refcutXstmmx
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: stb7100refcutXstmmx <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb442stb7100cutXusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb442stb7100cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb442stb7100_setup $arg2
  else
    connectsh4usble $arg1 mb442stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb442stb7100cutXusb
Connect to and configure an STb7100-Ref board
Usage: mb442stb7100cutXusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb442stb7100cutXbypassusb
  if ($argc > 2)
    mb442stb7100cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb442stb7100cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb442stb7100bypass_setup"
  end
end

document mb442stb7100cutXbypassusb
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442stb7100cutXbypassusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb442stb7100cutXstmmxusb
  if ($argc > 2)
    mb442stb7100cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb442stb7100cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb442stb7100stmmx_setup"
  end
end

document mb442stb7100cutXstmmxusb
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442stb7100cutXstmmxusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

################################################################################

define mb442cutXusb
  if ($argc > 2)
    mb442stb7100cutXusb $arg0 $arg1 $arg2
  else
    mb442stb7100cutXusb $arg0 $arg1
  end
end

document mb442cutXusb
Connect to and configure an STb7100-Ref board
Usage: mb442cutXusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb442cutXbypassusb
  if ($argc > 2)
    mb442stb7100cutXbypassusb $arg0 $arg1 $arg2
  else
    mb442stb7100cutXbypassusb $arg0 $arg1
  end
end

document mb442cutXbypassusb
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442cutXbypassusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb442cutXstmmxusb
  if ($argc > 2)
    mb442stb7100cutXstmmxusb $arg0 $arg1 $arg2
  else
    mb442stb7100cutXstmmxusb $arg0 $arg1
  end
end

document mb442cutXstmmxusb
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442cutXstmmxusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

################################################################################

define stb7100refcutXusb
  if ($argc > 2)
    mb442stb7100cutXusb $arg0 $arg1 $arg2
  else
    mb442stb7100cutXusb $arg0 $arg1
  end
end

document stb7100refcutXusb
Connect to and configure an STb7100-Ref board
Usage: stb7100refcutXusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define stb7100refcutXbypassusb
  if ($argc > 2)
    mb442stb7100cutXbypassusb $arg0 $arg1 $arg2
  else
    mb442stb7100cutXbypassusb $arg0 $arg1
  end
end

document stb7100refcutXbypassusb
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: stb7100refcutXbypassusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define stb7100refcutXstmmxusb
  if ($argc > 2)
    mb442stb7100cutXstmmxusb $arg0 $arg1 $arg2
  else
    mb442stb7100cutXstmmxusb $arg0 $arg1
  end
end

document stb7100refcutXstmmxusb
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: stb7100refcutXstmmxusb <cut> <target>
where <cut> is the STb7100 silicon cut version
+     <target> is an ST Micro Connect USB name
end

################################################################################
