##------------------------------------------------------------------------------
## db436.cmd - FPGA STEM Module DB436
##------------------------------------------------------------------------------

define db436_emi_configure
  set *$EMI_BANK$arg0_EMICONFIGDATA0 = 0x002016c9
  set *$EMI_BANK$arg0_EMICONFIGDATA1 = 0x9d211111
  set *$EMI_BANK$arg0_EMICONFIGDATA2 = 0x9d112222
  set *$EMI_BANK$arg0_EMICONFIGDATA3 = 0x00000000
end
