init-if-undefined $SHTDIWriteSize = (2 * 1024 * 1024)
keep-variable $SHTDIWriteSize

init-if-undefined $RTOSType = 0
keep-variable $RTOSType

init-if-undefined $_sh_simulator_target = 0
keep-variable $_sh_simulator_target

################################################################################
# SHx silicon connect commands
################################################################################

# arg0 = SDI library type
# arg1 = architecture type
# arg2 = endian type
# arg3 = target definition
# arg4 = configuration command
# arg5 = configuration options

define _connectshx
  set endian $arg2
  set $_sh_simulator_target = 0

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi libsh4sdi-$arg0.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -target $arg3 $arg5 endian=$arg2
  else
    target shtdi libsh4shdebug.so -sdi libsh4sdi-$arg0.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -target $arg3 $arg5 endian=$arg2
  end

  _init_all_plugins

  $arg4
end

define _connectshxstmc1-ethmp
  set download-write-size $SHTDIWriteSize
  _connectshx ethmp $arg0 $arg1 $arg2 $arg3 $arg4
end

define _connectshxstmc1-usbmp
  printf "Connection to target via USB is not supported on this host\n"
end

################################################################################

# arg0 = SDI library type
# arg1 = architecture type
# arg2 = endian type
# arg3 = target definition
# arg4 = configuration options

define _connectshxstmc1tp
  set endian $arg2
  set $_sh_simulator_target = 0
  set download-write-size $SHTDIWriteSize

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi libsh4sdi-$arg0.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -stmc libstmc1.so -target $arg3 -targetpack jtaginitialise=off $arg4 endian=$arg2
  else
    target shtdi libsh4shdebug.so -sdi libsh4sdi-$arg0.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -stmc libstmc1.so -target $arg3 -targetpack jtaginitialise=off $arg4 endian=$arg2
  end

  _init_all_plugins

  enable_targetpack
  targetpack import $arg3
end

define _connectshxstmc1tp-ethmp
  _connectshxstmc1tp ethmp $arg0 $arg1 $arg2 $arg3
end

define _connectshxstmc1tp-usbmp
  printf "Connection to target via USB is not supported on this host\n"
end

################################################################################

# arg0 = architecture type
# arg1 = endian type
# arg2 = target definition
# arg3 = configuration options

define _connectshxstmc2tp
  set endian $arg1
  set $_sh_simulator_target = 0
  set download-write-size ($SHTDIWriteSize - 20)

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi libsh4sdi-stmc2.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -target $arg2 -targetpack jtaginitialise=off $arg3 endian=$arg1
  else
    target shtdi libsh4shdebug.so -sdi libsh4sdi-stmc2.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -target $arg2 -targetpack jtaginitialise=off $arg3 endian=$arg1
  end

  _init_all_plugins

  enable_targetpack
  targetpack import $arg2
end

################################################################################

# arg0 = architecture type
# arg1 = endian type
# arg2 = target definition
# arg3 = configuration options

define _connectshxautotp
  set endian $arg1
  set $_sh_simulator_target = 0
  set download-write-size ($SHTDIWriteSize - 20)

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi-auto stmc1@eth=libsh4sdi-ethmp.so -sdi-auto stmc2=libsh4sdi-stmc2.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -stmc libstmc1.so -target $arg2 -targetpack jtaginitialise=off $arg3 endian=$arg1
  else
    target shtdi libsh4shdebug.so -sdi-auto stmc1@eth=libsh4sdi-ethmp.so -sdi-auto stmc2=libsh4sdi-stmc2.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -stmc libstmc1.so -target $arg2 -targetpack jtaginitialise=off $arg3 endian=$arg1
  end

  _init_all_plugins

  enable_targetpack
  targetpack import $arg2
end

################################################################################
# SH4 little endian silicon connection wrapper commands
################################################################################

define connectsh4le
  _connectshxstmc1-ethmp sh4 little $arg0 $arg1 $arg2
end

document connectsh4le
Connect to and configure a little endian SH4 silicon target
Usage: connectsh4le <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 name or IP address
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectsh4usble
  _connectshxstmc1-usbmp sh4 little $arg0 $arg1 $arg2
end

document connectsh4usble
Connect to and configure a little endian SH4 silicon target
Usage: connectsh4usble <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 USB name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectsh4stmc1tple
  _connectshxstmc1tp-ethmp sh4 little $arg0 $arg1
end

document connectsh4stmc1tple
Connect to and configure a little endian SH4 silicon target
Usage: connectsh4stmc1tple <target> <control-commands>
where <target> is an ST Micro Connect 1 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectsh4stmc1tpusble
  _connectshxstmc1tp-usbmp sh4 little $arg0 $arg1
end

document connectsh4stmc1tpusble
Connect to and configure a little endian SH4 silicon target
Usage: connectsh4stmc1tpusble <target> <control-commands>
where <target> is an ST Micro Connect 1 USB target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectsh4stmc2tple
  _connectshxstmc2tp sh4 little $arg0 $arg1
end

document connectsh4stmc2tple
Connect to and configure a little endian SH4 silicon target
Usage: connectsh4stmc2tple <target> <control-commands>
where <target> is an ST Micro Connect 2 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectsh4autotple
  _connectshxautotp sh4 little $arg0 $arg1
end

document connectsh4autotple
Connect to and configure a little endian SH4 silicon target (auto detection of ST Micro Connect type)
Usage: connectsh4autotple <target> <control-commands>
where <target> is an ST Micro Connect target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################
# ST40-300 little endian silicon connection wrapper commands
################################################################################

define connectst40300le
  _connectshxstmc1-ethmp st40-300 little $arg0 $arg1 $arg2
end

document connectst40300le
Connect to and configure a little endian ST40-300 silicon target
Usage: connectst40300le <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 name or IP address
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectst40300usble
  _connectshxstmc1-usbmp st40-300 little $arg0 $arg1 $arg2
end

document connectst40300usble
Connect to and configure a little endian ST40-300 silicon target
Usage: connectst40300usble <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 USB name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectst40300stmc1tple
  _connectshxstmc1tp-ethmp st40-300 little $arg0 $arg1
end

document connectst40300stmc1tple
Connect to and configure a little endian ST40-300 silicon target
Usage: connectst40300stmc1tple <target> <control-commands>
where <target> is an ST Micro Connect 1 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectst40300stmc1tpusble
  _connectshxstmc1tp-usbmp st40-300 little $arg0 $arg1
end

document connectst40300stmc1tpusble
Connect to and configure a little endian ST40-300 silicon target
Usage: connectst40300stmc1tpusble <target> <control-commands>
where <target> is an ST Micro Connect 1 USB target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectst40300stmc2tple
  _connectshxstmc2tp st40-300 little $arg0 $arg1
end

document connectst40300stmc2tple
Connect to and configure a little endian ST40-300 silicon target
Usage: connectst40300stmc2tple <target> <control-commands>
where <target> is an ST Micro Connect 2 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectst40300autotple
  _connectshxautotp st40-300 little $arg0 $arg1
end

document connectst40300autotple
Connect to and configure a little endian ST40-300 silicon target (auto detection of ST Micro Connect type)
Usage: connectst40300autotple <target> <control-commands>
where <target> is an ST Micro Connect target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################
# SH4 big endian silicon connection wrapper commands
################################################################################

define connectsh4be
  _connectshxstmc1-ethmp sh4 big $arg0 $arg1 $arg2
end

document connectsh4be
Connect to and configure a big endian SH4 silicon target
Usage: connectsh4be <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 name or IP address
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectsh4usbbe
  _connectshxstmc1-usbmp sh4 big $arg0 $arg1 $arg2
end

document connectsh4usbbe
Connect to and configure a big endian SH4 silicon target
Usage: connectsh4usbbe <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 USB name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectsh4stmc1tpbe
  _connectshxstmc1tp-ethmp sh4 big $arg0 $arg1
end

document connectsh4stmc1tpbe
Connect to and configure a big endian SH4 silicon target
Usage: connectsh4stmc1tpbe <target> <control-commands>
where <target> is an ST Micro Connect 1 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectsh4stmc1tpusbbe
  _connectshxstmc1tp-usbmp sh4 big $arg0 $arg1
end

document connectsh4stmc1tpusbbe
Connect to and configure a big endian SH4 silicon target
Usage: connectsh4stmc1tpusbbe <target> <control-commands>
where <target> is an ST Micro Connect 1 USB target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectsh4stmc2tpbe
  _connectshxstmc2tp sh4 big $arg0 $arg1
end

document connectsh4stmc2tpbe
Connect to and configure a big endian SH4 silicon target
Usage: connectsh4stmc2tpbe <target> <control-commands>
where <target> is an ST Micro Connect 2 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectsh4autotpbe
  _connectshxautotp sh4 big $arg0 $arg1
end

document connectsh4autotpbe
Connect to and configure a big endian SH4 silicon target (auto detection of ST Micro Connect type)
Usage: connectsh4autotpbe <target> <control-commands>
where <target> is an ST Micro Connect target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################
# ST40-300 big endian silicon connection wrapper commands
################################################################################

define connectst40300be
  _connectshxstmc1-ethmp st40-300 big $arg0 $arg1 $arg2
end

document connectst40300be
Connect to and configure a big endian ST40-300 silicon target
Usage: connectst40300be <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 name or IP address
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectst40300usbbe
  _connectshxstmc1-usbmp st40-300 big $arg0 $arg1 $arg2
end

document connectst40300usbbe
Connect to and configure a big endian ST40-300 silicon target
Usage: connectst40300usbbe <target> <setup-command> <control-commands>
where <target> is an ST Micro Connect 1 USB name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectst40300stmc1tpbe
  _connectshxstmc1tp-ethmp st40-300 big $arg0 $arg1
end

document connectst40300stmc1tpbe
Connect to and configure a big endian ST40-300 silicon target
Usage: connectst40300stmc1tpbe <target> <control-commands>
where <target> is an ST Micro Connect 1 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

define connectst40300stmc1tpusbbe
  _connectshxstmc1tp-usbmp st40-300 big $arg0 $arg1
end

document connectst40300stmc1tpusbbe
Connect to and configure a big endian ST40-300 silicon target
Usage: connectst40300stmc1tpusbbe <target> <control-commands>
where <target> is an ST Micro Connect 1 USB target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectst40300stmc2tpbe
  _connectshxstmc2tp st40-300 big $arg0 $arg1
end

document connectst40300stmc2tpbe
Connect to and configure a big endian ST40-300 silicon target
Usage: connectst40300stmc2tpbe <target> <control-commands>
where <target> is an ST Micro Connect 2 target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectst40300autotpbe
  _connectshxautotp st40-300 big $arg0 $arg1
end

document connectst40300autotpbe
Connect to and configure a big endian ST40-300 silicon target (auto detection of ST Micro Connect type)
Usage: connectst40300autotpbe <target> <control-commands>
where <target> is an ST Micro Connect target definition
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################
# SHx simulation connect command
################################################################################

# arg0 = simulator type
# arg1 = architecture type
# arg2 = endian type
# arg3 = simulator configuration command
# arg4 = configuration command
# arg5 = configuration options

define _connectshxsim
  set endian $arg2
  set $_sh_simulator_target = 1
  set download-write-size 0

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi libsh4sdi-sim.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -sim -inicommand $arg3 $arg0 $arg2 nomemory cache chessargs $arg5
  else
    target shtdi libsh4shdebug.so -sdi libsh4sdi-sim.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -sim -inicommand $arg3 $arg0 $arg2 nomemory cache chessargs $arg5
  end

  $arg4
end

################################################################################
# SH4 little endian simulation connection wrapper commands
################################################################################

define connectsh4simle
  _connectshxsim instr sh4 little $arg0 $arg1 $arg2
end

document connectsh4simle
Connect to and configure a little endian SH4 simulated target
Usage: connectsh4simle <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

define connectsh4psimle
  _connectshxsim cycle sh4 little $arg0 $arg1 $arg2
end

document connectsh4psimle
Connect to and configure a cycle accurate, little endian SH4 simulated target
Usage: connectsh4psimle <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

################################################################################
# ST40-300 little endian simulation connection wrapper commands
################################################################################

define connectst40300simle
  _connectshxsim instr st40-300 little $arg0 $arg1 $arg2
end

document connectst40300simle
Connect to and configure a little endian ST40-300 simulated target
Usage: connectst40300simle <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

define connectst40300psimle
  _connectshxsim cycle st40-300 little $arg0 $arg1 $arg2
end

document connectst40300psimle
Connect to and configure a cycle accurate, little endian ST40-300 simulated target
Usage: connectst40300psimle <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

################################################################################
# SH4 big endian simulation connection wrapper commands
################################################################################

define connectsh4simbe
  _connectshxsim instr sh4 big $arg0 $arg1 $arg2
end

document connectsh4simbe
Connect to and configure a big endian SH4 simulated target
Usage: connectsh4simbe <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

define connectsh4psimbe
  _connectshxsim cycle sh4 big $arg0 $arg1 $arg2
end

document connectsh4psimbe
Connect to and configure a cycle accurate, big endian SH4 simulated target
Usage: connectsh4psimbe <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

################################################################################
# ST40-300 big endian simulation connection wrapper commands
################################################################################

define connectst40300simbe
  _connectshxsim instr st40-300 big $arg0 $arg1 $arg2
end

document connectst40300simbe
Connect to and configure a big endian ST40-300 simulated target
Usage: connectst40300simbe <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

define connectst40300psimbe
  _connectshxsim cycle st40-300 big $arg0 $arg1 $arg2
end

document connectst40300psimbe
Connect to and configure a cycle accurate, big endian ST40-300 simulated target
Usage: connectst40300psimbe <simulator-command> <setup-command> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the simulator
end

################################################################################
# SHx co-simulation connect command
################################################################################

# arg0 = simulator type
# arg1 = architecture type
# arg2 = endian type
# arg3 = simulator configuration command
# arg4 = configuration command
# arg5 = shared memory handle
# arg6 = host event handle
# arg7 = target event handle
# arg8 = configuration options

define _connectshxcwsim
  set endian $arg2
  set $_sh_simulator_target = 1
  set download-write-size 0

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi libsh4sdi-sim.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -sim -inicommand $arg3 $arg0 $arg2 nomemory cache coware $arg5 $arg6 $arg7 chessargs $arg8
  else
    target shtdi libsh4shdebug.so -sdi libsh4sdi-sim.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -sim -inicommand $arg3 $arg0 $arg2 nomemory cache coware $arg5 $arg6 $arg7 chessargs $arg8
  end

  $arg4
end

################################################################################
# SH4 little endian co-simulation connection wrapper commands
################################################################################

define connectsh4cwsimle
  _connectshxcwsim instr sh4 little $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectsh4cwsimle
Connect to and configure a little endian SH4 simulated target
Usage: connectsh4cwsimle <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

define connectsh4cwpsimle
  _connectshxcwsim cycle sh4 little $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectsh4cwpsimle
Connect to and configure a cycle accurate, little endian SH4 simulated target
Usage: connectsh4cwpsimle <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

################################################################################
# ST40-300 little endian co-simulation connection wrapper commands
################################################################################

define connectst40300cwsimle
  _connectshxcwsim instr st40-300 little $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectst40300cwsimle
Connect to and configure a little endian ST40-300 simulated target
Usage: connectst40300cwsimle <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

define connectst40300cwpsimle
  _connectshxcwsim cycle st40-300 little $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectst40300cwpsimle
Connect to and configure a cycle accurate, little endian ST40-300 simulated target
Usage: connectst40300cwpsimle <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

################################################################################
# SH4 big endian co-simulation connection wrapper commands
################################################################################

define connectsh4cwsimbe
  _connectshxcwsim instr sh4 big $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectsh4cwsimbe
Connect to and configure a big endian SH4 simulated target
Usage: connectsh4cwsimbe <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

define connectsh4cwpsimbe
  _connectshxcwsim cycle sh4 big $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectsh4cwpsimbe
Connect to and configure a cycle accurate, big endian SH4 simulated target
Usage: connectsh4cwpsimbe <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

################################################################################
# ST40-300 big endian co-simulation connection wrapper commands
################################################################################

define connectst40300cwsimbe
  _connectshxcwsim instr st40-300 big $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectst40300cwsimbe
Connect to and configure a big endian ST40-300 simulated target
Usage: connectst40300cwsimbe <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

define connectst40300cwpsimbe
  _connectshxcwsim cycle st40-300 big $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
end

document connectst40300cwpsimbe
Connect to and configure a cycle accurate, big endian ST40-300 simulated target
Usage: connectst40300cwpsimbe <simulator-command> <setup-command> <shared-memory> <host-event> <target-event> <control-commands>
where <simulator-command> is a command to configure the simulator
+     <setup-command> is a command to configure the target
+     <shared-memory> is a shared memory handle identifier
+     <host-event> is a host event handle identifier
+     <target-event> is a target event handle identifier
+     <control-commands> are commands to configure the simulator
end

################################################################################

init-if-undefined $_STCheapTap = 0
keep-variable $_STCheapTap

if ($_STCheapTap)
  source sh4cheaptap.cmd
end

################################################################################
