##------------------------------------------------------------------------------
## sh7750eval.cmd - SH7750 Evaluation Board
##------------------------------------------------------------------------------

##{{{  SH7750EVAL Configure
define sh7750eval_configure
##------------------------------------------------------------------------------
## Phase 1: Configure FRQCR register
##------------------------------------------------------------------------------
  set *$CPG_WTCNT = 0x5a00
  set *$CPG_WTCSR = 0xa567
  set *$CPG_FRQCR = 0x0e0a

##------------------------------------------------------------------------------
## Phase 2: Configure rest...
##------------------------------------------------------------------------------
  set *$BSC_BCR1 = 0x00080008
  set *$BSC_BCR2 = 0xc000
  set *$BSC_WCR1 = 0x37773773
  set *$BSC_WCR2 = 0xfffe6fef
  set *$BSC_WCR3 = 0x07777774
  set *$BSC_MCR = 0x080a6214
  set *($BSC_SDMR3+(0x0190/sizeof(*$BSC_SDMR3))) = 0x0
  set *$BSC_RTCNT = 0xa500
  set *$BSC_RFCR = 0xa400
  set *$BSC_RTCSR = 0xa508
  set *$BSC_MCR = 0x480a6214
  set *($BSC_SDMR3+(0x0190/sizeof(*$BSC_SDMR3))) = 0x0
  set *$BSC_RTCNT = 0xa500
  set *$BSC_RTCOR = 0xa5fa
  set *$BSC_RFCR = 0xa400
  set *$BSC_RTCSR = 0xa508
end
##}}}

##{{{  SH7750EVAL Memory
define sh7750eval_memory_define
  memory-add Flash 0x00000000 1 ROM
  memory-add RAM   0x0c000000 32 RAM
end

define sh7750eval_sim_memory_define
  sim_addmemory 0x00000000 1 ROM
  sim_addmemory 0x0c000000 32 RAM
  sim_addmemory 0xff000000 16 DEV
end
##}}}

define sh7750eval_setup
  sh7750_define
  sh7750eval_memory_define

  sh7750_si_regs

  sh7750eval_configure

  set *$CCN_CCR = 0x0000090d
end

document sh7750eval_setup
Configure an SH7750 Evaluation board
Usage: sh7750eval_setup
end

define sh7750evalsim_setup
  sh7750_define
  sh7750eval_memory_define

  sh7750_si_regs

  set *$CCN_CCR = 0x0000090d
end

document sh7750evalsim_setup
Configure a simulated SH7750 Evaluation board
Usage: sh7750evalsim_setup
end

define sh7750eval_fsim_setup
  sh7750_fsim_core_setup
  sh7750eval_sim_memory_define
end

document sh7750eval_fsim_setup
Configure functional simulator for SH7750 Evaluation board
Usage: sh7750eval_fsim_setup
end

define sh7750eval_psim_setup
  sh7750_psim_core_setup
  sh7750eval_sim_memory_define
end

document sh7750eval_psim_setup
Configure performance simulator for SH7750 Evaluation board
Usage: sh7750eval_psim_setup
end

define sh7750eval_display_registers
  sh7750_display_si_regs
end

document sh7750eval_display_registers
Display the SH7750 configuration registers
Usage: sh7750eval_display_registers
end
