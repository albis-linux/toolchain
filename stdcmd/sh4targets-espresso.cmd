################################################################################

define espresso
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source espresso.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 espresso_setup $arg1
  else
    connectsh4le $arg0 espresso_setup "hardreset"
  end
end

document espresso
Connect to and configure an STEspresso-Demo board
Usage: espresso <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define espressousb
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source espresso.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 espresso_setup $arg1
  else
    connectsh4usble $arg0 espresso_setup "hardreset"
  end
end

document espressousb
Connect to and configure an STEspresso-Demo board
Usage: espressousb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define espressosim
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source espresso.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle espresso_fsim_setup espressosim_setup $arg0
  else
    connectsh4simle espresso_fsim_setup espressosim_setup ""
  end
end

document espressosim
Connect to and configure a simulated STEspresso-Demo board
Usage: espressosim
end

################################################################################

define espressopsim
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source espresso.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle espresso_psim_setup espressosim_setup $arg0
  else
    connectsh4psimle espresso_psim_setup espressosim_setup ""
  end
end

document espressopsim
Connect to and configure a simulated STEspresso-Demo board
Usage: espressopsim
end

################################################################################
