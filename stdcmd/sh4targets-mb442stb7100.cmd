################################################################################

define mb442stb7100
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb442stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb442stb7100_setup $arg1
  else
    connectsh4le $arg0 mb442stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb442stb7100
Connect to and configure an STb7100-Ref board
Usage: mb442stb7100 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb442stb7100bypass
  if ($argc > 1)
    mb442stb7100 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb442stb7100 $arg0 "jtagpinout=st40 jtagreset -inicommand mb442stb7100bypass_setup"
  end
end

document mb442stb7100bypass
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442stb7100bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb442stb7100stmmx
  if ($argc > 1)
    mb442stb7100 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb442stb7100 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb442stb7100stmmx_setup"
  end
end

document mb442stb7100stmmx
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442stb7100stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb442
  if ($argc > 1)
    mb442stb7100 $arg0 $arg1
  else
    mb442stb7100 $arg0
  end
end

document mb442
Connect to and configure an STb7100-Ref board
Usage: mb442 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb442bypass
  if ($argc > 1)
    mb442stb7100bypass $arg0 $arg1
  else
    mb442stb7100bypass $arg0
  end
end

document mb442bypass
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb442stmmx
  if ($argc > 1)
    mb442stb7100stmmx $arg0 $arg1
  else
    mb442stb7100stmmx $arg0
  end
end

document mb442stmmx
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define stb7100ref
  if ($argc > 1)
    mb442stb7100 $arg0 $arg1
  else
    mb442stb7100 $arg0
  end
end

document stb7100ref
Connect to and configure an STb7100-Ref board
Usage: stb7100ref <target>
where <target> is an ST Micro Connect name or IP address
end

define stb7100refbypass
  if ($argc > 1)
    mb442stb7100bypass $arg0 $arg1
  else
    mb442stb7100bypass $arg0
  end
end

document stb7100refbypass
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: stb7100refbypass <target>
where <target> is an ST Micro Connect name or IP address
end

define stb7100refstmmx
  if ($argc > 1)
    mb442stb7100stmmx $arg0 $arg1
  else
    mb442stb7100stmmx $arg0
  end
end

document stb7100refstmmx
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: stb7100refstmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb442stb7100usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mb442stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb442stb7100_setup $arg1
  else
    connectsh4usble $arg0 mb442stb7100_setup "jtagpinout=st40 hardreset"
  end
end

document mb442stb7100usb
Connect to and configure an STb7100-Ref board
Usage: mb442stb7100usb <target>
where <target> is an ST Micro Connect USB name
end

define mb442stb7100bypassusb
  if ($argc > 1)
    mb442stb7100usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb442stb7100usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb442stb7100bypass_setup"
  end
end

document mb442stb7100bypassusb
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442stb7100bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb442stb7100stmmxusb
  if ($argc > 1)
    mb442stb7100usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb442stb7100usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb442stb7100stmmx_setup"
  end
end

document mb442stb7100stmmxusb
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442stb7100stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb442usb
  if ($argc > 1)
    mb442stb7100usb $arg0 $arg1
  else
    mb442stb7100usb $arg0
  end
end

document mb442usb
Connect to and configure an STb7100-Ref board
Usage: mb442usb <target>
where <target> is an ST Micro Connect USB name
end

define mb442bypassusb
  if ($argc > 1)
    mb442stb7100bypassusb $arg0 $arg1
  else
    mb442stb7100bypassusb $arg0
  end
end

document mb442bypassusb
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: mb442bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb442stmmxusb
  if ($argc > 1)
    mb442stb7100stmmxusb $arg0 $arg1
  else
    mb442stb7100stmmxusb $arg0
  end
end

document mb442stmmxusb
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: mb442stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define stb7100refusb
  if ($argc > 1)
    mb442stb7100usb $arg0 $arg1
  else
    mb442stb7100usb $arg0
  end
end

document stb7100refusb
Connect to and configure an STb7100-Ref board
Usage: stb7100refusb <target>
where <target> is an ST Micro Connect USB name
end

define stb7100refbypassusb
  if ($argc > 1)
    mb442stb7100bypassusb $arg0 $arg1
  else
    mb442stb7100bypassusb $arg0
  end
end

document stb7100refbypassusb
Connect to and configure an STb7100-Ref board bypassing to the ST40
Usage: stb7100refbypassusb <target>
where <target> is an ST Micro Connect USB name
end

define stb7100refstmmxusb
  if ($argc > 1)
    mb442stb7100stmmxusb $arg0 $arg1
  else
    mb442stb7100stmmxusb $arg0
  end
end

document stb7100refstmmxusb
Connect to and configure an STb7100-Ref board via an ST MultiCore/Mux
Usage: stb7100refstmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb442stb7100sim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source mb442stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb442stb7100_fsim_setup mb442stb7100sim_setup $arg0
  else
    connectsh4simle mb442stb7100_fsim_setup mb442stb7100sim_setup ""
  end
end

document mb442stb7100sim
Connect to and configure a simulated STb7100-Ref board
Usage: mb442stb7100sim
end

################################################################################

define mb442sim
  if ($argc > 0)
    mb442stb7100sim $arg0
  else
    mb442stb7100sim
  end
end

document mb442sim
Connect to and configure a simulated STb7100-Ref board
Usage: mb442sim
end

################################################################################

define stb7100refsim
  if ($argc > 0)
    mb442stb7100sim $arg0
  else
    mb442stb7100sim
  end
end

document stb7100refsim
Connect to and configure a simulated STb7100-Ref board
Usage: stb7100refsim
end

################################################################################

define mb442stb7100psim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source mb442stb7100.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb442stb7100_psim_setup mb442stb7100sim_setup $arg0
  else
    connectsh4psimle mb442stb7100_psim_setup mb442stb7100sim_setup ""
  end
end

document mb442stb7100psim
Connect to and configure a simulated STb7100-Ref board
Usage: mb442stb7100psim
end

################################################################################

define mb442psim
  if ($argc > 0)
    mb442stb7100psim $arg0
  else
    mb442stb7100psim
  end
end

document mb442psim
Connect to and configure a simulated STb7100-Ref board
Usage: mb442psim
end

################################################################################

define stb7100refpsim
  if ($argc > 0)
    mb442stb7100psim $arg0
  else
    mb442stb7100psim
  end
end

document stb7100refpsim
Connect to and configure a simulated STb7100-Ref board
Usage: stb7100refpsim
end

################################################################################
