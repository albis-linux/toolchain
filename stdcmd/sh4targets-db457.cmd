################################################################################

define db457
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source db457.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 db457_setup $arg1
  else
    connectsh4le $arg0 db457_setup "hardreset"
  end
end

document db457
Connect to and configure an ST40STB1 Overdrive board
Usage: db457 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define db457usb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source db457.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 db457_setup $arg1
  else
    connectsh4usble $arg0 db457_setup "hardreset"
  end
end

document db457usb
Connect to and configure an ST40STB1 Overdrive board
Usage: db457usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define db457sim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source db457.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle db457_fsim_setup db457sim_setup $arg0
  else
    connectsh4simle db457_fsim_setup db457sim_setup ""
  end
end

document db457sim
Connect to and configure a simulated ST40STB1 Overdrive board
Usage: db457sim
end

################################################################################

define db457psim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source db457.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle db457_psim_setup db457sim_setup $arg0
  else
    connectsh4psimle db457_psim_setup db457sim_setup ""
  end
end

document db457psim
Connect to and configure a simulated ST40STB1 Overdrive board
Usage: db457psim
end

################################################################################
