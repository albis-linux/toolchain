################################################################################

define mb680sti7105sim
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7105_fsim_setup mb680sti7105sim_setup $arg0
  else
    connectsh4simle mb680sti7105_fsim_setup mb680sti7105sim_setup ""
  end
end

document mb680sti7105sim
Connect to and configure a simulated STi7105-Mboard board
Usage: mb680sti7105sim
end

define mb680sti7105simse
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7105se_fsim_setup mb680sti7105simse_setup $arg0
  else
    connectsh4simle mb680sti7105se_fsim_setup mb680sti7105simse_setup ""
  end
end

document mb680sti7105simse
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode)
Usage: mb680sti7105simse
end

define mb680sti7105simseuc
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7105se_fsim_setup mb680sti7105simseuc_setup $arg0
  else
    connectsh4simle mb680sti7105se_fsim_setup mb680sti7105simseuc_setup ""
  end
end

document mb680sti7105simseuc
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: mb680sti7105simseuc
end

define mb680sti7105simse29
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7105se_fsim_setup mb680sti7105simse29_setup $arg0
  else
    connectsh4simle mb680sti7105se_fsim_setup mb680sti7105simse29_setup ""
  end
end

document mb680sti7105simse29
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb680sti7105simse29
end

################################################################################

define mb680sim
  if ($argc > 0)
    mb680sti7105sim $arg0
  else
    mb680sti7105sim
  end
end

document mb680sim
Connect to and configure a simulated STi7105-Mboard board
Usage: mb680sim
end

define mb680simse
  if ($argc > 0)
    mb680sti7105simse $arg0
  else
    mb680sti7105simse
  end
end

document mb680simse
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode)
Usage: mb680simse
end

define mb680simseuc
  if ($argc > 0)
    mb680sti7105simseuc $arg0
  else
    mb680sti7105simseuc
  end
end

document mb680simseuc
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: mb680simseuc
end

define mb680simse29
  if ($argc > 0)
    mb680sti7105simse29 $arg0
  else
    mb680sti7105simse29
  end
end

document mb680simse29
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb680simse29
end

################################################################################

define mb680sti7105psim
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7105_psim_setup mb680sti7105sim_setup $arg0
  else
    connectsh4psimle mb680sti7105_psim_setup mb680sti7105sim_setup ""
  end
end

document mb680sti7105psim
Connect to and configure a simulated STi7105-Mboard board
Usage: mb680sti7105psim
end

define mb680sti7105psimse
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7105se_psim_setup mb680sti7105simse_setup $arg0
  else
    connectsh4psimle mb680sti7105se_psim_setup mb680sti7105simse_setup ""
  end
end

document mb680sti7105psimse
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode)
Usage: mb680sti7105psimse
end

define mb680sti7105psimseuc
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7105se_psim_setup mb680sti7105simseuc_setup $arg0
  else
    connectsh4psimle mb680sti7105se_psim_setup mb680sti7105simseuc_setup ""
  end
end

document mb680sti7105psimseuc
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: mb680sti7105psimseuc
end

define mb680sti7105psimse29
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source mb680sti7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7105se_psim_setup mb680sti7105simse29_setup $arg0
  else
    connectsh4psimle mb680sti7105se_psim_setup mb680sti7105simse29_setup ""
  end
end

document mb680sti7105psimse29
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb680sti7105psimse29
end

################################################################################

define mb680psim
  if ($argc > 0)
    mb680sti7105psim $arg0
  else
    mb680sti7105psim
  end
end

document mb680psim
Connect to and configure a simulated STi7105-Mboard board
Usage: mb680psim
end

define mb680psimse
  if ($argc > 0)
    mb680sti7105psimse $arg0
  else
    mb680sti7105psimse
  end
end

document mb680psimse
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode)
Usage: mb680psimse
end

define mb680psimseuc
  if ($argc > 0)
    mb680sti7105psimseuc $arg0
  else
    mb680sti7105psimseuc
  end
end

document mb680psimseuc
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: mb680psimseuc
end

define mb680psimse29
  if ($argc > 0)
    mb680sti7105psimse29 $arg0
  else
    mb680sti7105psimse29
  end
end

document mb680psimse29
Connect to and configure a simulated STi7105-Mboard board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb680psimse29
end

################################################################################
