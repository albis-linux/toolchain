################################################################################

define tmmlr2
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr2.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 tmmlr2_setup $arg1
  else
    connectsh4le $arg0 tmmlr2_setup "hardreset"
  end
end

document tmmlr2
Connect to and configure an STm8000-TMM board
Usage: tmmlr2 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define tmmlr2usb
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr2.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 tmmlr2_setup $arg1
  else
    connectsh4usble $arg0 tmmlr2_setup "hardreset"
  end
end

document tmmlr2usb
Connect to and configure an STm8000-TMM board
Usage: tmmlr2usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define tmmlr2sim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr2.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle tmmlr2_fsim_setup tmmlr2sim_setup $arg0
  else
    connectsh4simle tmmlr2_fsim_setup tmmlr2sim_setup ""
  end
end

document tmmlr2sim
Connect to and configure a simulated STm8000-TMM board
Usage: tmmlr2sim
end

################################################################################

define tmmlr2psim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr2.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle tmmlr2_psim_setup tmmlr2sim_setup $arg0
  else
    connectsh4psimle tmmlr2_psim_setup tmmlr2sim_setup ""
  end
end

document tmmlr2psim
Connect to and configure a simulated STm8000-TMM board
Usage: tmmlr2psim
end

################################################################################
