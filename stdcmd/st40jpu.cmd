init-if-undefined $_st40unlocktimeout = 0
keep-variable $_st40unlocktimeout

define _bitreverse32
  set $_b = $arg0

  set $_b = (($_b >> 16) & 0x0000ffff) | (($_b << 16) & 0xffff0000)
  set $_b = (($_b >> 8) & 0x00ff00ff) | (($_b << 8) & 0xff00ff00)
  set $_b = (($_b >> 4) & 0x0f0f0f0f) | (($_b << 4) & 0xf0f0f0f0)
  set $_b = (($_b >> 2) & 0x33333333) | (($_b << 2) & 0xcccccccc)
  set $_b = (($_b >> 1) & 0x55555555) | (($_b << 1) & 0xaaaaaaaa)

  set $arg1 = $_b
end

define _bitreverse64
  set $_b = $arg0

  set $_b = (($_b >> 32) & 0x00000000ffffffffLL) | (($_b << 32) & 0xffffffff00000000LL)
  set $_b = (($_b >> 16) & 0x0000ffff0000ffffLL) | (($_b << 16) & 0xffff0000ffff0000LL)
  set $_b = (($_b >> 8) & 0x00ff00ff00ff00ffLL) | (($_b << 8) & 0xff00ff00ff00ff00LL)
  set $_b = (($_b >> 4) & 0x0f0f0f0f0f0f0f0fLL) | (($_b << 4) & 0xf0f0f0f0f0f0f0f0LL)
  set $_b = (($_b >> 2) & 0x3333333333333333LL) | (($_b << 2) & 0xccccccccccccccccLL)
  set $_b = (($_b >> 1) & 0x5555555555555555LL) | (($_b << 1) & 0xaaaaaaaaaaaaaaaaLL)

  set $arg1 = $_b
end

define _st40unlockprintstatus
  if ($arg1 == 1)
    printf "$arg0: device successfully unlocked\n"
  end
  if ($arg1 == 0)
    printf "$arg0: device is not keyed\n"
  end
  if ($arg1 == -1)
    printf "$arg0: device cannot be unlocked\n"
  end
  if ($arg1 == -2)
    printf "$arg0: device failed to unlock with secret ID\n"
  end
  if ($arg1 == -3)
    printf "$arg0: device is not the expected device ID\n"
  end
  if ($arg1 == -4)
    printf "$arg0: device is not the expected chip ID\n"
  end
end

define _st40getdeviceid
  ## Select specified device ID
  tmcSelectDeviceId $arg0

  ## Read device ID
  tmcWriteIR_IDCODE
  tmcReadDR 32 $_id

  set $arg1 = $_id_0

  ## Restore device ID to default
  tmcSelectDeviceId 0
end

define _st40getchipid
  ## Read chip ID (via security instruction)
  tmcWriteIR 0x4
  tmcReadDR 32 $_id

  ## Reverse bits (format is MSB to LSB)
  _bitreverse32 $_id_0 $arg0
end

define _st40setsecretid32
  ## Reverse bits (format is MSB to LSB)
  _bitreverse32 $arg0 $_id

  ## Set secret ID (via security instruction)
  tmcWriteIR 0x4
  tmcWriteDR 32 $_id
end

define _st40setsecretid64
  ## Reverse bits (format is MSB to LSB)
  _bitreverse64 $arg0 $_id

  ## Set secret ID (via security instruction)
  tmcWriteIR 0x4
  tmcWriteDR 64 $_id
end

define _st40setsecretid
  if (sizeof($arg0) == 4)
    _st40setsecretid32 $arg0
  else
  if (sizeof($arg0) == 8)
    _st40setsecretid64 $arg0
  end
  end
end

define _st40unlock
  _st40getdeviceid 4 $tmcprivateid

  ## Check if device is keyed and not locked (bits 23 and 24 of private ID)
  if (($tmcprivateid & 0x01800000) == 0x00800000)
    _st40setsecretid $arg0

    ## Wait for unlock timer to expire (2^24 TCK cycles)
    jtag mode=continuous
    sleep $_st40unlocktimeout
    jtag mode=singleshot

    _st40getdeviceid 4 $tmcprivateid

    ## Check if device is unlocked (bit 25 of private ID)
    if (($tmcprivateid & 0x02000000) == 0x02000000)
      set $arg1 = 1
    else
      set $arg1 = -2
    end
  else
    if (($tmcprivateid & 0x01000000) == 0x00000000)
      set $arg1 = 0
    else
      set $arg1 = -1
    end
  end
end

define _st40unlocklookup
  _st40getdeviceid 4 $tmcprivateid

  ## Check if device is keyed and not locked (bits 23 and 24 of private ID)
  if (($tmcprivateid & 0x01800000) == 0x00800000)
    _st40getdeviceid 0 $tmcdeviceid
    _st40getchipid $tmcchipid

    ## Lookup secret ID from device ID and chip ID
    $arg0 $tmcdeviceid $tmcchipid $tmcsecretid $arg1

    ## Check if recognised device ID and chip ID
    if ($arg1 == 0)
      _st40setsecretid $tmcsecretid

      ## Wait for unlock timer to expire (2^24 TCK cycles)
      jtag mode=continuous
      sleep $_st40unlocktimeout
      jtag mode=singleshot

      _st40getdeviceid 4 $tmcprivateid

      ## Check if device is unlocked (bit 25 of private ID)
      if (($tmcprivateid & 0x02000000) == 0x02000000)
        set $arg1 = 1
      else
        set $arg1 = -2
      end
    end
  else
    if (($tmcprivateid & 0x01000000) == 0x00000000)
      set $arg1 = 0
    else
      set $arg1 = -1
    end
  end
end

define _st40unlockvalidate
  _st40getdeviceid 0 $tmcdeviceid

  if ($arg0 == $tmcdeviceid)
    _st40getdeviceid 4 $tmcprivateid

    ## Check if device is keyed and not locked (bits 23 and 24 of private ID)
    if (($tmcprivateid & 0x01800000) == 0x00800000)
      _st40getchipid $tmcchipid

      ## Check if expected chip ID
      if ($arg1 == $tmcchipid)
        _st40setsecretid $arg2

        ## Wait for unlock timer to expire (2^24 TCK cycles)
        jtag mode=continuous
        sleep $_st40unlocktimeout
        jtag mode=singleshot

        _st40getdeviceid 4 $tmcprivateid

        ## Check if device is unlocked (bit 25 of private ID)
        if (($tmcprivateid & 0x02000000) == 0x02000000)
          set $arg3 = 1
        else
          set $arg3 = -2
        end
      else
        set $arg3 = -4
      end
    else
      if (($tmcprivateid & 0x01000000) == 0x00000000)
        set $arg3 = 0
      else
        set $arg3 = -1
      end
    end
  else
    set $arg3 = -3
  end
end

define st40deviceid
  tmcInitialState
  _st40getdeviceid 0 $arg0
end

define st40privateid
  tmcInitialState
  _st40getdeviceid 4 $arg0
end

define st40optionid
  tmcInitialState
  _st40getdeviceid 2 $arg0
end

define st40extraid
  tmcInitialState
  _st40getdeviceid 6 $arg0
end

define st40chipid
  tmcInitialState
  _st40getchipid $arg0
end

define st40unlock
  tmcInitialState
  _st40unlock $arg0 $tmcerrno
  if ($tmcerrno != 1)
    _st40unlockprintstatus st40unlock $tmcerrno
  end
end

define st40unlocklookup
  tmcInitialState
  _st40unlocklookup $arg0 $tmcerrno
  if ($tmcerrno != 1)
    _st40unlockprintstatus st40unlocklookup $tmcerrno
  end
end

define st40unlockvalidate
  tmcInitialState
  _st40unlockvalidate $arg0 $arg1 $arg2 $tmcerrno
  if ($tmcerrno != 1)
    _st40unlockprintstatus st40unlockvalidate $tmcerrno
  end
end
