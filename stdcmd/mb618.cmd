##------------------------------------------------------------------------------
## mb618.cmd - STi7111-Mboard Validation Platform MB618
##------------------------------------------------------------------------------

init-if-undefined $_mb618lmi256 = 0
keep-variable $_mb618lmi256

##{{{  MB618 PMB Configuration
define mb618se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  if ($_mb618lmi256)
  sh4_set_pmb 1 0x88 0x48 128
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb618seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  if ($_mb618lmi256)
  sh4_set_pmb 1 0x88 0x48 128 0 0 1
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb618se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  if ($_mb618lmi256)
  sh4_set_pmb 1 0x88 0x48 128
  end
  sh4_set_pmb 2 0xa0 0x40 128 0 0 1
  if ($_mb618lmi256)
  sh4_set_pmb 3 0xa8 0x48 128 0 0 1
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB618 Memory
define mb618_memory_define
  memory-add Flash     0x00000000 32 ROM
  if ($_mb618lmi256)
  memory-add LMI_SDRAM 0x0c000000 256 RAM
  else
  memory-add LMI_SDRAM 0x0c000000 128 RAM
  end
end

define mb618_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  if ($_mb618lmi256)
  sim_addmemory 0x0c000000 256 RAM
  else
  sim_addmemory 0x0c000000 128 RAM
  end
  sim_addmemory 0xfc000000 64 DEV
end

define mb618se_memory_define
  memory-add Flash     0x00000000 32 ROM
  if ($_mb618lmi256)
  memory-add LMI_SDRAM 0x40000000 256 RAM
  else
  memory-add LMI_SDRAM 0x40000000 128 RAM
  end
end

define mb618se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  if ($_mb618lmi256)
  sim_addmemory 0x40000000 256 RAM
  else
  sim_addmemory 0x40000000 128 RAM
  end
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb618sim_setup
  sti7111_define
  mb618_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb618sim_setup
Configure a simulated STi7111-Mboard board
Usage: mb618sim_setup
end

define mb618simse_setup
  sti7111_define
  mb618se_memory_define

  st40300_core_si_regs

  mb618se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb618simse_setup
Configure a simulated STi7111-Mboard board with the STi7111 in 32-bit SE mode
Usage: mb618simse_setup
end

define mb618simseuc_setup
  sti7111_define
  mb618se_memory_define

  st40300_core_si_regs

  mb618seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb618simseuc_setup
Configure a simulated STi7111-Mboard board with the STi7111 in 32-bit SE mode with uncached RAM mappings
Usage: mb618simseuc_setup
end

define mb618simse29_setup
  sti7111_define
  mb618se_memory_define

  st40300_core_si_regs

  mb618se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb618simse29_setup
Configure a simulated STi7111-Mboard board with the STi7111 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb618simse29_setup
end

define mb618_fsim_setup
  sti7111_fsim_core_setup
  mb618_sim_memory_define
end

document mb618_fsim_setup
Configure functional simulator for STi7111-Mboard board
Usage: mb618_fsim_setup
end

define mb618se_fsim_setup
  sti7111_fsim_core_setup
  mb618se_sim_memory_define
end

document mb618se_fsim_setup
Configure functional simulator for STi7111-Mboard board with the STi7111 in 32-bit SE mode
Usage: mb618se_fsim_setup
end

define mb618_psim_setup
  sti7111_psim_core_setup
  mb618_sim_memory_define
end

document mb618_psim_setup
Configure performance simulator for STi7111-Mboard board
Usage: mb618_psim_setup
end

define mb618se_psim_setup
  sti7111_psim_core_setup
  mb618se_sim_memory_define
end

document mb618se_psim_setup
Configure performance simulator for STi7111-Mboard board with the STi7111 in 32-bit SE mode
Usage: mb618se_psim_setup
end

define mb618_display_registers
  st40300_display_core_si_regs
end

document mb618_display_registers
Display the STi7111 configuration registers
Usage: mb618_display_registers
end
