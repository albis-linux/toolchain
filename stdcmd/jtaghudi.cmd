source jtagcommon.cmd

################################################################################

## ST MultiCore/Mux state
init-if-undefined $_stmmxmode = 0
keep-variable $_stmmxmode

################################################################################

## Initial JTAG state
set $hudi_sdmode = 1
keep-variable $hudi_sdmode
set $hudi_sdmode_saved = 1
keep-variable $hudi_sdmode_saved
set $hudi_sdmode_locked = 1
keep-variable $hudi_sdmode_locked

################################################################################

define hudiTestLogicReset
  ## Move to Test-Logic-Reset
  jtag tms=(1*5)

  ## Update JTAG state
  set $hudi_sdmode_locked = 1
  set $hudi_sdmode = 1
end

################################################################################

define hudiRunTestIdle
  ## Move to Run-Test-Idle
  hudiTestLogicReset
  jtag tms=0
end

################################################################################

define _hudiReadSDIR
  ## Read SDIR register (Run-Test-Idle initial and final state)
  if ($_stmmxmode)
    jtag tms=1100 tdo=1
    jtag tms=(0*31)1 tdo=1 tdi=$arg0
    jtag tms=10
  else
    jtag tms=110
    jtag tms=(0*32) tdo=1 tdi=$arg0
    jtag tms=110
  end

  ## Update JTAG state
  set $hudi_sdmode_locked = 1
  set $hudi_sdmode = 1
end

define hudiReadSDIR
  if ($argc > 0)
    _hudiReadSDIR $arg0
  else
    _hudiReadSDIR $_sdir
  end
end

################################################################################

define hudiWriteSDIR
  ## Write SDIR register (Run-Test-Idle initial and final state)
  set $_size = 8
  set $_data = $arg0
  jtag tms=1100
  while ($_size-- > 1)
    if ($_data & 1)
      jtag tms=0 tdo=1
    else
      jtag tms=0 tdo=0
    end
    set $_data >>= 1
  end
  if ($_data & 1)
    jtag tms=1 tdo=1
  else
    jtag tms=1 tdo=0
  end
  jtag tms=10
end

################################################################################

define hudiWriteSDIR_Extest
  hudiWriteSDIR 0x00

  ## Update JTAG state
  set $hudi_sdmode_locked = 1
  set $hudi_sdmode = 1
end

define hudiWriteSDIR_SamplePreLoad
  hudiWriteSDIR 0x04

  ## Update JTAG state
  set $hudi_sdmode_locked = 1
  set $hudi_sdmode = 1
end

define hudiWriteSDIR_InternalStatusReadStart
  hudiWriteSDIR 0x20

  ## Update JTAG state
  set $hudi_sdmode_locked = 1
  set $hudi_sdmode_saved = $hudi_sdmode
  set $hudi_sdmode = 1
end

define hudiWriteSDIR_InternalStatusReadEnd
  hudiWriteSDIR 0x21

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
  set $hudi_sdmode = $hudi_sdmode_saved
end

define hudiWriteSDIR_TDOTimingChange
  hudiWriteSDIR 0x22

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
end

define hudiWriteSDIR_WaitCancel
  hudiWriteSDIR 0x23

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
end

define hudiWriteSDIR_AseramWrite
  hudiWriteSDIR 0x50

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
end

define hudiWriteSDIR_ResetNegate
  hudiWriteSDIR 0x60

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
end

define hudiWriteSDIR_ResetAssert
  hudiWriteSDIR 0x70

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
end

define hudiWriteSDIR_Boot
  hudiWriteSDIR 0x80

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
end

define hudiWriteSDIR_Interrupt
  hudiWriteSDIR 0xa0

  ## Update JTAG state
  set $hudi_sdmode_locked = 1
  set $hudi_sdmode = 1
end

define hudiWriteSDIR_Break
  hudiWriteSDIR 0xc0

  ## Update JTAG state
  set $hudi_sdmode_locked = 0
end

define hudiWriteSDIR_Bypass
  hudiWriteSDIR 0xff

  ## Update JTAG state
  set $hudi_sdmode_locked = 1
  set $hudi_sdmode = 1
end

################################################################################

define hudiPrintSDIR
  hudiReadSDIR $sdir
  _jtagPrintSignal $sdir
end

################################################################################

define _hudiReadSDDRorSDSR
  ## Read SDDR or SDSR register (Run-Test-Idle initial and final state)
  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=0 tdi=$arg0
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=0 tdi=$arg0
    jtag tms=110
  end

  ## Update JTAG state
  if ($hudi_sdmode_locked == 0)
    if ($hudi_sdmode == 1)
      set $hudi_sdmode = 0
    else
      set $hudi_sdmode = $arg0_0 & 0x1
    end
  end
end

define hudiReadSDDRorSDSR
  if ($argc > 0)
    _hudiReadSDDRorSDSR $arg0
  else
    _hudiReadSDDRorSDSR $_sddr
  end
end

################################################################################

define hudiWriteSDDRorSDSR
  ## Write SDDR or SDSR register (Run-Test-Idle initial and final state)
  set $_size = 32
  set $_data = $arg0
  jtag tms=100
  while ($_size-- > 1)
    if ($_data & 1)
      jtag tms=0 tdo=1
    else
      jtag tms=0 tdo=0
    end
    set $_data >>= 1
  end
  if ($_data & 1)
    jtag tms=1 tdo=1
  else
    jtag tms=1 tdo=0
  end
  jtag tms=10

  ## Update JTAG state
  if ($hudi_sdmode_locked == 0)
    if ($hudi_sdmode == 1)
      set $hudi_sdmode = 0
    else
      set $hudi_sdmode = $_sddr_0 & 0x1
    end
  end
end

################################################################################

define hudiReadSDDR
  ## Read SDDR register
  if ($hudi_sdmode == 0)
    hudiInitialState
  end
  hudiReadSDDRorSDSR $arg0
end

define hudiReadSDSR
  ## Read SDSR register
  if ($hudi_sdmode == 1)
    hudiInitialState
    hudiReadSDDRorSDSR
  end
  hudiReadSDDRorSDSR $arg0
end

################################################################################

define hudiWriteSDDR
  ## Read SDDR register
  if ($hudi_sdmode == 0)
    hudiInitialState
  end
  hudiWriteSDDRorSDSR $arg0
end

################################################################################

define hudiPrintSDDR
  hudiReadSDDR $sddr
  _jtagPrintSignal $sddr
end

define hudiPrintSDSR
  hudiReadSDSR $sdsr
  _jtagPrintSignal $sdsr
end

################################################################################

define hudiInitialState
  jtag mode=singleshot
  hudiRunTestIdle
  hudiWriteSDIR_ResetNegate
end

################################################################################

define hudiHardReset
  jtag nrst=0
  _jtagSleep 20
  jtag nrst=1
end

################################################################################

define hudiSoftReset
  hudiWriteSDIR_ResetAssert
  _jtagSleep 20
  hudiWriteSDIR_ResetNegate
end

################################################################################

define hudiBootHardReset
  ## Hard reset for boot sequence (Reset-Hold final state)
  jtag ntrst=0
  _jtagSleep 20
  jtag nrst=0
  _jtagSleep 20
  jtag asebrk=0
  _jtagSleep 20
  jtag nrst=1
  _jtagSleep 20
  jtag asebrk=1
  _jtagSleep 20
  jtag ntrst=1
end

################################################################################

define hudiBootSoftReset
  ## Soft reset for boot sequence
  hudiWriteSDIR_ResetAssert
  _jtagSleep 20
end

################################################################################

define hudiReadInternalStatus
  set $hudi_status_SR = -1
  set $hudi_status_FPSCR = -1
  set $hudi_status_CCR = -1
  set $hudi_status_FRQCR = -1
  set $hudi_status_EXPEVT = -1
  set $hudi_status_INTEVT = -1
  set $hudi_status_EBUS = -1
  set $hudi_status_IBUS = -1
  set $hudi_status_SBUS = -1
  set $hudi_status_EBTYPE = -1
  set $hudi_status_SBTYPE = -1
  set $hudi_status_CMF = -1
  set $hudi_status_SCMF = -1
  set $hudi_status_MMUCRAT = -1
  set $hudi_status_PTEH = -1
  set $hudi_status_STATUS = -1

  hudiWriteSDIR_InternalStatusReadStart

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)1000(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)1000(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_IBUS = $_sddr_0

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)0100(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)0100(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_SR = $_sddr_0

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)1100(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)1100(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_FPSCR = $_sddr_0

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)0010(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)0010(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_CMF = $_sddr_0 & 0x3f
  set $hudi_status_SCMF = ($_sddr_0 >> 6) & 0xf

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)1010(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)1010(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_PTEH = $_sddr_0 & 0xff
  set $hudi_status_EXPEVT = ($_sddr_0 >> 8) & 0xfff
  set $hudi_status_INTEVT = (($_sddr_0 >> 20) & 0xfff) << 2

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)0110(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)0110(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_CCR = $_sddr_0 & 0x7f
  set $hudi_status_MMUCRAT = ($_sddr_0 >> 7) & 0x1

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)1110(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)1110(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_SBTYPE = $_sddr_0 & 0xf
  set $hudi_status_EBTYPE = ($_sddr_0 >> 4) & 0x7f

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)0001(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)0001(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_SBUS = $_sddr_0

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)1001(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)1001(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_EBUS = $_sddr_0 & 0x1fffffff

  if ($_stmmxmode)
    jtag tms=100 tdo=0
    jtag tms=(0*31)1 tdo=(0*24)0000(0*4) tdi=$_sddr
    jtag tms=10
  else
    jtag tms=10
    jtag tms=(0*32) tdo=(0*25)0000(0*3) tdi=$_sddr
    jtag tms=110
  end

  set $hudi_status_FRQCR = $_sddr_0 & 0xfff
  set $hudi_status_STATUS = ($_sddr_0 >> 12) & 0x3

  hudiWriteSDIR_InternalStatusReadEnd
end

################################################################################

define hudiPrintInternalStatus
  hudiReadInternalStatus
  printf "========================================\n"
  printf "HUDI Internal Status\n"
  printf "----------------------------------------\n"
  printf "SR       = 0x%08x\n", $hudi_status_SR
  printf "FPSCR    = 0x%08x\n\n", $hudi_status_FPSCR
  printf "CCR      = 0x%02x\n", $hudi_status_CCR
  printf "FRQCR    = 0x%04x\n", $hudi_status_FRQCR
  printf "EXPEVT   = 0x%04x\n", $hudi_status_EXPEVT
  printf "INTEVT   = 0x%04x\n\n", $hudi_status_INTEVT
  printf "EBUS     = 0x%08x\n", $hudi_status_EBUS
  printf "IBUS     = 0x%08x\n", $hudi_status_IBUS
  printf "SBUS     = 0x%08x\n", $hudi_status_SBUS
  printf "EBTYPE   = 0x%02x\n", $hudi_status_EBTYPE
  printf "SBTYPE   = 0x%02x\n\n", $hudi_status_SBTYPE
  printf "CMF      = 0x%02x\n", $hudi_status_CMF
  printf "SCMF     = 0x%02x\n", $hudi_status_SCMF
  printf "MMUCR.AT = 0x%02x\n", $hudi_status_MMUCRAT
  printf "PTEH     = 0x%02x\n\n", $hudi_status_PTEH
  printf "STATUS   = 0x%02x\n", $hudi_status_STATUS
  printf "========================================\n"
end

################################################################################
