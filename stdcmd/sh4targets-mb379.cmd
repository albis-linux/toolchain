################################################################################

define mb379
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb379.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb379_setup $arg1
  else
    connectsh4le $arg0 mb379_setup "hardreset"
  end
end

document mb379
Connect to and configure an STm8000-Demo board
Usage: mb379 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb379usb
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb379.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb379_setup $arg1
  else
    connectsh4usble $arg0 mb379_setup "hardreset"
  end
end

document mb379usb
Connect to and configure an STm8000-Demo board
Usage: mb379usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb379sim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb379.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb379_fsim_setup mb379sim_setup $arg0
  else
    connectsh4simle mb379_fsim_setup mb379sim_setup ""
  end
end

document mb379sim
Connect to and configure a simulated STm8000-Demo board
Usage: mb379sim
end

################################################################################

define mb379psim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb379.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb379_psim_setup mb379sim_setup $arg0
  else
    connectsh4psimle mb379_psim_setup mb379sim_setup ""
  end
end

document mb379psim
Connect to and configure a simulated STm8000-Demo board
Usage: mb379psim
end

################################################################################
