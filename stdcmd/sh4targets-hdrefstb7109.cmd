################################################################################

define hdrefstb7109
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 hdrefstb7109_setup $arg1
  else
    connectsh4le $arg0 hdrefstb7109_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109
Connect to and configure an STb7109-Ref board
Usage: hdrefstb7109 <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109se
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 hdrefstb7109se_setup $arg1
  else
    connectsh4le $arg0 hdrefstb7109se_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109se
Connect to and configure an STb7109-Ref board (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109se <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109seuc
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 hdrefstb7109seuc_setup $arg1
  else
    connectsh4le $arg0 hdrefstb7109seuc_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109seuc
Connect to and configure an STb7109-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109seuc <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109se29
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 hdrefstb7109se29_setup $arg1
  else
    connectsh4le $arg0 hdrefstb7109se29_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109se29
Connect to and configure an STb7109-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109se29 <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109bypass
  if ($argc > 1)
    hdrefstb7109 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109 $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109bypass
Connect to and configure an STb7109-Ref board bypassing to the ST40
Usage: hdrefstb7109bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109sebypass
  if ($argc > 1)
    hdrefstb7109se $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109se $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109sebypass
Connect to and configure an STb7109-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109sebypass <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109seucbypass
  if ($argc > 1)
    hdrefstb7109seuc $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109seuc $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109seucbypass
Connect to and configure an STb7109-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109seucbypass <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109se29bypass
  if ($argc > 1)
    hdrefstb7109se29 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109se29 $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109se29bypass
Connect to and configure an STb7109-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109se29bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109stmmx
  if ($argc > 1)
    hdrefstb7109 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109stmmx
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux
Usage: hdrefstb7109stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109sestmmx
  if ($argc > 1)
    hdrefstb7109se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109sestmmx
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109sestmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109seucstmmx
  if ($argc > 1)
    hdrefstb7109seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109seucstmmx
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109seucstmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define hdrefstb7109se29stmmx
  if ($argc > 1)
    hdrefstb7109se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109se29stmmx
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109se29stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define hdrefstb7109usb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 hdrefstb7109_setup $arg1
  else
    connectsh4usble $arg0 hdrefstb7109_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109usb
Connect to and configure an STb7109-Ref board
Usage: hdrefstb7109usb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109seusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 hdrefstb7109se_setup $arg1
  else
    connectsh4usble $arg0 hdrefstb7109se_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109seusb
Connect to and configure an STb7109-Ref board (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109seusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109seucusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 hdrefstb7109seuc_setup $arg1
  else
    connectsh4usble $arg0 hdrefstb7109seuc_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109seucusb
Connect to and configure an STb7109-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109seucusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109se29usb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 hdrefstb7109se29_setup $arg1
  else
    connectsh4usble $arg0 hdrefstb7109se29_setup "jtagpinout=st40 hardreset"
  end
end

document hdrefstb7109se29usb
Connect to and configure an STb7109-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109se29usb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109bypassusb
  if ($argc > 1)
    hdrefstb7109usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109usb $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109bypassusb
Connect to and configure an STb7109-Ref board bypassing to the ST40
Usage: hdrefstb7109bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109sebypassusb
  if ($argc > 1)
    hdrefstb7109seusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109seusb $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109sebypassusb
Connect to and configure an STb7109-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109sebypassusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109seucbypassusb
  if ($argc > 1)
    hdrefstb7109seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109seucbypassusb
Connect to and configure an STb7109-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109seucbypassusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109se29bypassusb
  if ($argc > 1)
    hdrefstb7109se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hdrefstb7109se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand hdrefstb7109bypass_setup"
  end
end

document hdrefstb7109se29bypassusb
Connect to and configure an STb7109-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109se29bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109stmmxusb
  if ($argc > 1)
    hdrefstb7109usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109stmmxusb
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux
Usage: hdrefstb7109stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109sestmmxusb
  if ($argc > 1)
    hdrefstb7109seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109sestmmxusb
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109sestmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109seucstmmxusb
  if ($argc > 1)
    hdrefstb7109seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109seucstmmxusb
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109seucstmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define hdrefstb7109se29stmmxusb
  if ($argc > 1)
    hdrefstb7109se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hdrefstb7109se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hdrefstb7109stmmx_setup"
  end
end

document hdrefstb7109se29stmmxusb
Connect to and configure an STb7109-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109se29stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define hdrefstb7109sim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdrefstb7109_fsim_setup hdrefstb7109sim_setup $arg0
  else
    connectsh4simle hdrefstb7109_fsim_setup hdrefstb7109sim_setup ""
  end
end

document hdrefstb7109sim
Connect to and configure a simulated STb7109-Ref board
Usage: hdrefstb7109sim
end

define hdrefstb7109simse
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdrefstb7109se_fsim_setup hdrefstb7109simse_setup $arg0
  else
    connectsh4simle hdrefstb7109se_fsim_setup hdrefstb7109simse_setup ""
  end
end

document hdrefstb7109simse
Connect to and configure a simulated STb7109-Ref board (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109simse
end

define hdrefstb7109simseuc
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdrefstb7109se_fsim_setup hdrefstb7109simseuc_setup $arg0
  else
    connectsh4simle hdrefstb7109se_fsim_setup hdrefstb7109simseuc_setup ""
  end
end

document hdrefstb7109simseuc
Connect to and configure a simulated STb7109-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109simseuc
end

define hdrefstb7109simse29
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdrefstb7109se_fsim_setup hdrefstb7109simse29_setup $arg0
  else
    connectsh4simle hdrefstb7109se_fsim_setup hdrefstb7109simse29_setup ""
  end
end

document hdrefstb7109simse29
Connect to and configure a simulated STb7109-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109simse29
end

################################################################################

define hdrefstb7109psim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdrefstb7109_psim_setup hdrefstb7109sim_setup $arg0
  else
    connectsh4psimle hdrefstb7109_psim_setup hdrefstb7109sim_setup ""
  end
end

document hdrefstb7109psim
Connect to and configure a simulated STb7109-Ref board
Usage: hdrefstb7109psim
end

define hdrefstb7109psimse
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdrefstb7109se_psim_setup hdrefstb7109simse_setup $arg0
  else
    connectsh4psimle hdrefstb7109se_psim_setup hdrefstb7109simse_setup ""
  end
end

document hdrefstb7109psimse
Connect to and configure a simulated STb7109-Ref board (STb7109 in 32-bit SE mode)
Usage: hdrefstb7109psimse
end

define hdrefstb7109psimseuc
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdrefstb7109se_psim_setup hdrefstb7109simseuc_setup $arg0
  else
    connectsh4psimle hdrefstb7109se_psim_setup hdrefstb7109simseuc_setup ""
  end
end

document hdrefstb7109psimseuc
Connect to and configure a simulated STb7109-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hdrefstb7109psimseuc
end

define hdrefstb7109psimse29
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source hdrefstb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdrefstb7109se_psim_setup hdrefstb7109simse29_setup $arg0
  else
    connectsh4psimle hdrefstb7109se_psim_setup hdrefstb7109simse29_setup ""
  end
end

document hdrefstb7109psimse29
Connect to and configure a simulated STb7109-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdrefstb7109psimse29
end

################################################################################
