################################################################################
## Define register
################################################################################

##{{{  SHx
define __shx_reg
  set $arg0 = $arg1
  keep-variable $arg0
end

define __shx_reg_8
  set $arg0 = (unsigned char *) $arg1
  keep-variable $arg0
end

define __shx_reg_16
  set $arg0 = (unsigned short *) $arg1
  keep-variable $arg0
end

define __shx_reg_32
  set $arg0 = (unsigned int *) $arg1
  keep-variable $arg0
end

define __shx_reg_64
  set $arg0_0 = (unsigned int *) $arg1
  keep-variable $arg0_0

  __shx_reg $arg0_L $arg0_0

  set $arg0_1 = (unsigned int *) ($arg1+4)
  keep-variable $arg0_1

  __shx_reg $arg0_H $arg0_1

  set $arg0 = (unsigned long long int *) $arg1
  keep-variable $arg0
end
##}}}

################################################################################
## Core SH4 control registers
################################################################################

##{{{  SH4 CCN
## Core control registers (common to all SH4 variants)
define sh4_ccn_regs
  __shx_reg_32 $CCN_PTEH 0xff000000
  __shx_reg_32 $CCN_PTEL 0xff000004
  __shx_reg_32 $CCN_TTB 0xff000008
  __shx_reg_32 $CCN_TEA 0xff00000c
  __shx_reg_32 $CCN_MMUCR 0xff000010
  __shx_reg_8  $CCN_BASRA 0xff000014
  __shx_reg_8  $CCN_BASRB 0xff000018
  __shx_reg_32 $CCN_CCR 0xff00001c
  __shx_reg_32 $CCN_TRA 0xff000020
  __shx_reg_32 $CCN_EXPEVT 0xff000024
  __shx_reg_32 $CCN_INTEVT 0xff000028
  __shx_reg_32 $CCN_PVR 0xff000030
  __shx_reg_32 $CCN_PTEA 0xff000034
  __shx_reg_32 $CCN_QACR0 0xff000038
  __shx_reg_32 $CCN_QACR1 0xff00003c
  __shx_reg_32 $CCN_CVR 0xff000040
  __shx_reg_32 $CCN_PRR 0xff000044
end
##}}}

##{{{  SH4 UBC
## User Break Controller control registers (common to all SH4)
define sh4_ubc_regs
  __shx_reg_32 $UBC_BARA 0xff200000
  __shx_reg_8  $UBC_BAMRA 0xff200004
  __shx_reg_16 $UBC_BBRA 0xff200008
  __shx_reg_8  $UBC_BASRA 0xff000014
  __shx_reg_32 $UBC_BARB 0xff20000c
  __shx_reg_8  $UBC_BAMRB 0xff200010
  __shx_reg_16 $UBC_BBRB 0xff200014
  __shx_reg_8  $UBC_BASRB 0xff000018
  __shx_reg_32 $UBC_BDRB 0xff200018
  __shx_reg_32 $UBC_BDMRB 0xff20001c
  __shx_reg_16 $UBC_BRCR 0xff200020
end
##}}}

##{{{  SH4 UDI
## User Debug Interface control registers (common to all SH4)
define sh4_udi_regs
  __shx_reg_16 $UDI_SDIR 0xfff00000
  __shx_reg_32 $UDI_SDDR 0xfff00008
  __shx_reg_16 $UDI_SDDRH 0xfff00008
  __shx_reg_16 $UDI_SDDRL 0xfff0000a
  __shx_reg_16 $UDI_SDINT 0xfff00014
end
##}}}

##{{{  SH4 AUD
## Advanced User Debugger control registers (common to all SH4)
define sh4_aud_regs
  __shx_reg_16 $AUD_AUCSR 0xff2000cc
  __shx_reg_32 $AUD_AUWASR 0xff2000d0
  __shx_reg_32 $AUD_AUWAER 0xff2000d4
  __shx_reg_32 $AUD_AUWBSR 0xff2000d8
  __shx_reg_32 $AUD_AUWBER 0xff2000dc
end
##}}}

################################################################################
## Generic SH4 control registers
################################################################################

##{{{  SH4 TMU
## Timer Unit control registers (common to all SH4 variants)
define sh4_tmu_regs
  __shx_reg_8  $TMU_TOCR ($arg0+0x00)
  __shx_reg_8  $TMU_TSTR ($arg0+0x04)
  __shx_reg_32 $TMU_TCOR0 ($arg0+0x08)
  __shx_reg_32 $TMU_TCNT0 ($arg0+0x0c)
  __shx_reg_16 $TMU_TCR0 ($arg0+0x10)
  __shx_reg_32 $TMU_TCOR1 ($arg0+0x14)
  __shx_reg_32 $TMU_TCNT1 ($arg0+0x18)
  __shx_reg_16 $TMU_TCR1 ($arg0+0x1c)
  __shx_reg_32 $TMU_TCOR2 ($arg0+0x20)
  __shx_reg_32 $TMU_TCNT2 ($arg0+0x24)
  __shx_reg_16 $TMU_TCR2 ($arg0+0x28)
  __shx_reg_32 $TMU_TCPR2 ($arg0+0x2c)
end
##}}}

##{{{  SH4 RTC
## Real Time Clock control registers (common to all SH4 variants)
define sh4_rtc_regs
  __shx_reg_8  $RTC_R64CNT ($arg0+0x00)
  __shx_reg_8  $RTC_RSECCNT ($arg0+0x04)
  __shx_reg_8  $RTC_RMINCNT ($arg0+0x08)
  __shx_reg_8  $RTC_RHRCNT ($arg0+0x0c)
  __shx_reg_8  $RTC_RWKCNT ($arg0+0x10)
  __shx_reg_8  $RTC_RDAYCNT ($arg0+0x14)
  __shx_reg_8  $RTC_RMONCNT ($arg0+0x18)
  __shx_reg_16 $RTC_RYRCNT ($arg0+0x1c)
  __shx_reg_8  $RTC_RSECAR ($arg0+0x20)
  __shx_reg_8  $RTC_RMINAR ($arg0+0x24)
  __shx_reg_8  $RTC_RHRAR ($arg0+0x28)
  __shx_reg_8  $RTC_RWKAR ($arg0+0x2c)
  __shx_reg_8  $RTC_RDAYAR ($arg0+0x30)
  __shx_reg_8  $RTC_RMONAR ($arg0+0x34)
  __shx_reg_8  $RTC_RCR1 ($arg0+0x38)
  __shx_reg_8  $RTC_RCR2 ($arg0+0x3c)
end
##}}}

##{{{  SH4 INTC
## Interrupt Controller control registers
define sh4_intc_regs
  __shx_reg_16 $INTC_ICR ($arg0+0x00)
  __shx_reg_16 $INTC_IPRA ($arg0+0x04)
  __shx_reg_16 $INTC_IPRB ($arg0+0x08)
  __shx_reg_16 $INTC_IPRC ($arg0+0x0c)
  __shx_reg_16 $INTC_IPRD ($arg0+0x10)
end
##}}}

##{{{  SH4 CPG
## Clock Pulse Generator control registers
define sh4_cpg_regs
  __shx_reg_16 $CPG_FRQCR ($arg0+0x00)
  __shx_reg_8  $CPG_STBCR ($arg0+0x04)
  __shx_reg_16 $CPG_WTCNT ($arg0+0x08)
  __shx_reg_8  $CPG_WTCNT_R ($arg0+0x08)
  __shx_reg_16 $CPG_WTCSR ($arg0+0x0c)
  __shx_reg_8  $CPG_WTCSR_R ($arg0+0x0c)
  __shx_reg_8  $CPG_STBCR2 ($arg0+0x10)
end
##}}}

################################################################################
## SH775x control registers
################################################################################

##{{{  SH775x DMAC
## Direct Memory Access Controller control registers (all SH775x variants)
define sh775x_dmac_regs
  __shx_reg_32 $DMAC_SAR0 0xffa00000
  __shx_reg_32 $DMAC_DAR0 0xffa00004
  __shx_reg_32 $DMAC_DMATCR0 0xffa00008
  __shx_reg_32 $DMAC_CHCR0 0xffa0000c
  __shx_reg_32 $DMAC_SAR1 0xffa00010
  __shx_reg_32 $DMAC_DAR1 0xffa00014
  __shx_reg_32 $DMAC_DMATCR1 0xffa00018
  __shx_reg_32 $DMAC_CHCR1 0xffa0001c
  __shx_reg_32 $DMAC_SAR2 0xffa00020
  __shx_reg_32 $DMAC_DAR2 0xffa00024
  __shx_reg_32 $DMAC_DMATCR2 0xffa00028
  __shx_reg_32 $DMAC_CHCR2 0xffa0002c
  __shx_reg_32 $DMAC_SAR3 0xffa00030
  __shx_reg_32 $DMAC_DAR3 0xffa00034
  __shx_reg_32 $DMAC_DMATCR3 0xffa00038
  __shx_reg_32 $DMAC_CHCR3 0xffa0003c
  __shx_reg_32 $DMAC_DMAOR 0xffa00040
end
##}}}

##{{{  SH775x BSC
## Bus State Controller control registers (all SH775x variants)
define sh775x_bsc_regs
  __shx_reg_32 $BSC_BCR1 0xff800000
  __shx_reg_16 $BSC_BCR2 0xff800004
  __shx_reg_32 $BSC_WCR1 0xff800008
  __shx_reg_32 $BSC_WCR2 0xff80000c
  __shx_reg_32 $BSC_WCR3 0xff800010
  __shx_reg_32 $BSC_MCR 0xff800014
  __shx_reg_16 $BSC_PCR 0xff800018
  __shx_reg_16 $BSC_RTCSR 0xff80001c
  __shx_reg_16 $BSC_RTCNT 0xff800020
  __shx_reg_16 $BSC_RTCOR 0xff800024
  __shx_reg_16 $BSC_RFCR 0xff800028
  __shx_reg_32 $BSC_PCTRA 0xff80002c
  __shx_reg_16 $BSC_PDTRA 0xff800030
  __shx_reg_32 $BSC_PCTRB 0xff800040
  __shx_reg_16 $BSC_PDTRB 0xff800044
  __shx_reg_16 $BSC_GPIOIC 0xff800048
  __shx_reg_8  $BSC_SDMR2 0xff900000
  __shx_reg_8  $BSC_SDMR3 0xff940000
end
##}}}

##{{{  SH775x SCI
## Serial Communication Interface control registers (all SH775x variants)
define sh775x_sci_regs
  __shx_reg_8  $SCI_SCSMR1 0xffe00000
  __shx_reg_8  $SCI_SCBRR1 0xffe00004
  __shx_reg_8  $SCI_SCSCR1 0xffe00008
  __shx_reg_8  $SCI_SCTDR1 0xffe0000c
  __shx_reg_8  $SCI_SCSSR1 0xffe00010
  __shx_reg_8  $SCI_SCRDR1 0xffe00014
  __shx_reg_8  $SCI_SCSCMR1 0xffe00018
  __shx_reg_8  $SCI_SCSPTR1 0xffe0001c
end
##}}}

##{{{  SH775x SCIF
## Serial Communication Interface with FIFO control registers (all SH775x variants)
define sh775x_scif_regs
  __shx_reg_16 $SCIF_SCSMR2 0xffe80000
  __shx_reg_8  $SCIF_SCBRR2 0xffe80004
  __shx_reg_16 $SCIF_SCSCR2 0xffe80008
  __shx_reg_8  $SCIF_SCFTDR2 0xffe8000c
  __shx_reg_16 $SCIF_SCFSR2 0xffe80010
  __shx_reg_8  $SCIF_SCFRDR2 0xffe80014
  __shx_reg_16 $SCIF_SCFCR2 0xffe80018
  __shx_reg_16 $SCIF_SCFDR2 0xffe8001c
  __shx_reg_16 $SCIF_SCSPTR2 0xffe80020
  __shx_reg_16 $SCIF_SCLSR2 0xffe80024
end
##}}}

################################################################################
## SH7750 control registers
################################################################################

##{{{  SH7750 INTC
## Interrupt Controller control registers (all SH7750 variants)
define sh7750_intc_regs
  __shx_reg_16 $INTC_ICR 0xffd00000
  __shx_reg_16 $INTC_IPRA 0xffd00004
  __shx_reg_16 $INTC_IPRB 0xffd00008
  __shx_reg_16 $INTC_IPRC 0xffd0000c
end
##}}}

################################################################################
## SH7751 control registers
################################################################################

##{{{  SH7751 CPG
## Clock Pulse Generator control registers (all SH7751 variants)
define sh7751_cpg_regs
  __shx_reg_32 $CPG_CLKSTP00 0xfe0a0000
  __shx_reg_32 $CPG_CLKSTPCLR00 0xfe0a0008
end
##}}}

##{{{  SH7751 INTC
## Interrupt Controller control registers (all SH7751 variants)
define sh7751_intc_regs
  __shx_reg_32 $INTC_INTPRI00 0xfe080000
  __shx_reg_32 $INTC_INTREQ00 0xfe080020
  __shx_reg_32 $INTC_INTMSK00 0xfe080040
  __shx_reg_32 $INTC_INTMSKCLR00 0xfe080060
end
##}}}

##{{{  SH7751 TMU
## Timer Unit control registers (all SH7751 variants)
define sh7751_tmu_regs
  __shx_reg_8  $TMU_TSTR2 0xfe100004
  __shx_reg_32 $TMU_TCOR3 0xfe100008
  __shx_reg_32 $TMU_TCNT3 0xfe10000c
  __shx_reg_16 $TMU_TCR3 0xfe100010
  __shx_reg_32 $TMU_TCOR4 0xfe100014
  __shx_reg_32 $TMU_TCNT4 0xfe100018
  __shx_reg_16 $TMU_TCR4 0xfe10001c
end
##}}}

################################################################################
## SH42xx control registers
################################################################################

##{{{  SH42xx CPG2
## Clock Pulse Generator control registers
define sh42xx_cpg2_regs
  __shx_reg_32 $CPG2_CLKSTP00 ($arg0+0x00)
  __shx_reg_32 $CPG2_CLKSTPCLR00 ($arg0+0x08)
  __shx_reg_32 $CPG2_CLKSTPACK00 ($arg0+0x10)
  __shx_reg_32 $CPG2_FRQCR3 ($arg0+0x18)
end
##}}}

##{{{  SH42xx DMAC
## Direct Memory Access Controller control registers
define sh42xx_dmac_regs
  __shx_reg_32 $DMAC_VCR_L ($arg0+0x00)
  __shx_reg_32 $DMAC_VCR_H ($arg0+0x04)
  __shx_reg_32 $DMAC_COMMON ($arg0+0x0c)
  __shx_reg_32 $DMAC_SAR0 ($arg0+0x14)
  __shx_reg_32 $DMAC_DAR0 ($arg0+0x1c)
  __shx_reg_32 $DMAC_COUNT0 ($arg0+0x24)
  __shx_reg_32 $DMAC_CTRL0 ($arg0+0x2c)
  __shx_reg_32 $DMAC_STATUS0 ($arg0+0x34)
  __shx_reg_32 $DMAC_SAR1 ($arg0+0x3c)
  __shx_reg_32 $DMAC_DAR1 ($arg0+0x44)
  __shx_reg_32 $DMAC_COUNT1 ($arg0+0x4c)
  __shx_reg_32 $DMAC_CTRL1 ($arg0+0x54)
  __shx_reg_32 $DMAC_STATUS1 ($arg0+0x5c)
  __shx_reg_32 $DMAC_SAR2 ($arg0+0x64)
  __shx_reg_32 $DMAC_DAR2 ($arg0+0x6c)
  __shx_reg_32 $DMAC_COUNT2 ($arg0+0x74)
  __shx_reg_32 $DMAC_CTRL2 ($arg0+0x7c)
  __shx_reg_32 $DMAC_DMAEXG ($arg0+0xc4)
end
##}}}

##{{{  SH42xx INTC2
## Interrupt Controller control registers
define sh42xx_intc2_regs
  __shx_reg_32 $INTC2_INTPRI00 ($arg0+0x00)
  __shx_reg_32 $INTC2_INTREQ00 ($arg0+0x20)
  __shx_reg_32 $INTC2_INTMSK00 ($arg0+0x40)
  __shx_reg_32 $INTC2_INTMSKCLR00 ($arg0+0x60)
end
##}}}

##{{{  SH42xx SCIF
## Serial Communication Interfaces control registers
define sh42xx_scif_regs
  __shx_reg_16 $SCIF_SCSMR ($arg0+0x00)
  __shx_reg_8  $SCIF_SCBRR ($arg0+0x04)
  __shx_reg_16 $SCIF_SCSCR ($arg0+0x08)
  __shx_reg_8  $SCIF_SCFTDR ($arg0+0x0c)
  __shx_reg_16 $SCIF_SCFSR ($arg0+0x10)
  __shx_reg_8  $SCIF_SCFRDR ($arg0+0x14)
  __shx_reg_16 $SCIF_SCFCR ($arg0+0x18)
  __shx_reg_16 $SCIF_SCFDR ($arg0+0x1c)
  __shx_reg_16 $SCIF_SCSPTR ($arg0+0x20)
  __shx_reg_16 $SCIF_SCLSR ($arg0+0x24)
end
##}}}

##{{{  SH42xx EMI
## External Memory Interface control registers
define sh42xx_emi_regs
  __shx_reg_32 $EMI_VCR_L ($arg0+0x000000)
  __shx_reg_32 $EMI_VCR_H ($arg0+0x000004)
  __shx_reg_32 $EMI_MIM ($arg0+0x00000c)
  __shx_reg_32 $EMI_SCR ($arg0+0x000014)
  __shx_reg_32 $EMI_STR ($arg0+0x00001c)
  __shx_reg_32 $EMI_COC ($arg0+0x00002c)
  __shx_reg_32 $EMI_SDRA0 ($arg0+0x000034)
  __shx_reg_32 $EMI_SDRA1 ($arg0+0x00003c)
  __shx_reg_32 $EMI_SDMR0 ($arg0+0x100000)
  __shx_reg_32 $EMI_SDMR1 ($arg0+0x200000)
end
##}}}

##{{{  SH42xx FEMI
## Flash External Memory Interface control registers
define sh42xx_femi_regs
  __shx_reg_32 $FEMI_VCR_L ($arg0+0x00)
  __shx_reg_32 $FEMI_VCR_H ($arg0+0x04)
  __shx_reg_32 $FEMI_MDCR ($arg0+0x0c)
  __shx_reg_32 $FEMI_A0MCR ($arg0+0x14)
  __shx_reg_32 $FEMI_A1MCR ($arg0+0x1c)
  __shx_reg_32 $FEMI_A2MCR ($arg0+0x24)
  __shx_reg_32 $FEMI_A3MCR ($arg0+0x2c)
  __shx_reg_32 $FEMI_A4MCR ($arg0+0x34)
end
##}}}

################################################################################
## ST40 control registers
################################################################################

##{{{  ST40 INTC2
## Interrupt Controller control registers (all ST40 variants)
define st40_intc2_regs
  __shx_reg_32 $INTC2_INTPRI00 ($arg0+0x00)
  __shx_reg_32 $INTC2_INTPRI04 ($arg0+0x04)
  __shx_reg_32 $INTC2_INTPRI08 ($arg0+0x08)
  __shx_reg_32 $INTC2_INTREQ00 ($arg0+0x20)
  __shx_reg_32 $INTC2_INTREQ04 ($arg0+0x24)
  __shx_reg_32 $INTC2_INTREQ08 ($arg0+0x28)
  __shx_reg_32 $INTC2_INTMSK00 ($arg0+0x40)
  __shx_reg_32 $INTC2_INTMSK04 ($arg0+0x44)
  __shx_reg_32 $INTC2_INTMSK08 ($arg0+0x48)
  __shx_reg_32 $INTC2_INTMSKCLR00 ($arg0+0x60)
  __shx_reg_32 $INTC2_INTMSKCLR04 ($arg0+0x64)
  __shx_reg_32 $INTC2_INTMSKCLR08 ($arg0+0x68)
  __shx_reg_32 $INTC2_INTC2MODE ($arg0+0x80)
end
##}}}

##{{{  ST40 ILC
## Interrupt Level Controller control registers (all ST40 variants)
define st40_ilc_x_regs
  __shx_reg_32 $ILC_INPUT_INTERRUPT_$arg1 ($arg0+(0x0080+($arg1*4)))
  __shx_reg_32 $ILC_STATUS_$arg1 ($arg0+(0x0200+($arg1*4)))
  __shx_reg_32 $ILC_CLEAR_STATUS_$arg1 ($arg0+(0x0280+($arg1*4)))
  __shx_reg_32 $ILC_ENABLE_$arg1 ($arg0+(0x0400+($arg1*4)))
  __shx_reg_32 $ILC_CLEAR_ENABLE_$arg1 ($arg0+(0x0480+($arg1*4)))
  __shx_reg_32 $ILC_SET_ENABLE_$arg1 ($arg0+(0x0500+($arg1*4)))
  __shx_reg_32 $ILC_WAKEUP_ENABLE_$arg1 ($arg0+(0x0600+($arg1*4)))
  __shx_reg_32 $ILC_WAKEUP_ACTIVE_LEVEL_$arg1 ($arg0+(0x0680+($arg1*4)))
end

define st40_ilc_regs
  st40_ilc_x_regs $arg0 0
  st40_ilc_x_regs $arg0 1
  st40_ilc_x_regs $arg0 2
  st40_ilc_x_regs $arg0 3
  st40_ilc_x_regs $arg0 4
  st40_ilc_x_regs $arg0 5
  st40_ilc_x_regs $arg0 6
  st40_ilc_x_regs $arg0 7
  st40_ilc_x_regs $arg0 8
  st40_ilc_x_regs $arg0 9
  st40_ilc_x_regs $arg0 10
  st40_ilc_x_regs $arg0 11
  st40_ilc_x_regs $arg0 12
  st40_ilc_x_regs $arg0 13
  st40_ilc_x_regs $arg0 14
  st40_ilc_x_regs $arg0 15
  st40_ilc_x_regs $arg0 16
  st40_ilc_x_regs $arg0 17
  st40_ilc_x_regs $arg0 18
  st40_ilc_x_regs $arg0 19
  st40_ilc_x_regs $arg0 20
  st40_ilc_x_regs $arg0 21
  st40_ilc_x_regs $arg0 22
  st40_ilc_x_regs $arg0 23
  st40_ilc_x_regs $arg0 24
  st40_ilc_x_regs $arg0 25
  st40_ilc_x_regs $arg0 26
  st40_ilc_x_regs $arg0 27
  st40_ilc_x_regs $arg0 28
  st40_ilc_x_regs $arg0 29
  st40_ilc_x_regs $arg0 30
  st40_ilc_x_regs $arg0 31
end
##}}}

##{{{  ST40 SCIF
## Serial Communication Interfaces control registers (all ST40 variants)
define st40_scif_regs
  __shx_reg_16 $SCIF$arg1_SCSMR ($arg0+0x00)
  __shx_reg_8  $SCIF$arg1_SCBRR ($arg0+0x04)
  __shx_reg_16 $SCIF$arg1_SCSCR ($arg0+0x08)
  __shx_reg_8  $SCIF$arg1_SCFTDR ($arg0+0x0c)
  __shx_reg_16 $SCIF$arg1_SCFSR ($arg0+0x10)
  __shx_reg_8  $SCIF$arg1_SCFRDR ($arg0+0x14)
  __shx_reg_16 $SCIF$arg1_SCFCR ($arg0+0x18)
  __shx_reg_16 $SCIF$arg1_SCFDR ($arg0+0x1c)
  __shx_reg_16 $SCIF$arg1_SCSPTR ($arg0+0x20)
  __shx_reg_16 $SCIF$arg1_SCLSR ($arg0+0x24)
end
##}}}

##{{{  ST40 CLOCKGEN
## Clock Generator control registers (all ST40 variants)
define st40_clockgen_regs
  __shx_reg_32 $CLOCKGEN$arg1_PLL1CR1 ($arg0+0x00)
  __shx_reg_32 $CLOCKGEN$arg1_PLL1CR2 ($arg0+0x08)
  __shx_reg_32 $CLOCKGEN$arg1_PLL2CR ($arg0+0x10)
  __shx_reg_32 $CLOCKGEN$arg1_STBREQCR ($arg0+0x18)
  __shx_reg_32 $CLOCKGEN$arg1_STBREQCR_SET ($arg0+0x20)
  __shx_reg_32 $CLOCKGEN$arg1_STBREQCR_CLR ($arg0+0x28)
  __shx_reg_32 $CLOCKGEN$arg1_STBACKCR ($arg0+0x30)
  __shx_reg_32 $CLOCKGEN$arg1_CLK4CR ($arg0+0x38)
  __shx_reg_32 $CLOCKGEN$arg1_CPG_BYPASS ($arg0+0x40)
  __shx_reg_32 $CLOCKGEN$arg1_PLL2_MUXCR ($arg0+0x48)
  __shx_reg_32 $CLOCKGEN$arg1_CLK1CR ($arg0+0x50)
  __shx_reg_32 $CLOCKGEN$arg1_CLK2CR ($arg0+0x58)
  __shx_reg_32 $CLOCKGEN$arg1_CLK3CR ($arg0+0x60)
  __shx_reg_32 $CLOCKGEN$arg1_CLK_SELCR ($arg0+0x68)
end
##}}}

##{{{  ST40 DMAC
## Direct Memeory Access Controller control registers (all ST40 variants)
define st40_dmac_chan_0_regs
  __shx_reg_32 $DMAC_CHAN0_IDENTITY ($arg0+0x00)
  __shx_reg_32 $DMAC_CHAN0_ENABLE ($arg0+0x08)
  __shx_reg_32 $DMAC_CHAN0_DISABLE ($arg0+0x10)
  __shx_reg_32 $DMAC_CHAN0_STATUS ($arg0+0x18)
  __shx_reg_32 $DMAC_CHAN0_ACTION ($arg0+0x20)
  __shx_reg_32 $DMAC_CHAN0_POINTER ($arg0+0x28)
  __shx_reg_32 $DMAC_CHAN0_SUBBASE ($arg0+0x30)
  __shx_reg_32 $DMAC_CHAN0_SUBENABLE ($arg0+0x38)
  __shx_reg_32 $DMAC_CHAN0_SUBDISABLE ($arg0+0x40)
  __shx_reg_32 $DMAC_CHAN0_SUBINT_ENB ($arg0+0x48)
  __shx_reg_32 $DMAC_CHAN0_SUBINT_DIS ($arg0+0x50)
  __shx_reg_32 $DMAC_CHAN0_SUBINT_STAT ($arg0+0x58)
  __shx_reg_32 $DMAC_CHAN0_SUNINT_ACT ($arg0+0x60)
  __shx_reg_32 $DMAC_CHAN0_CONTROL ($arg0+0x80)
  __shx_reg_32 $DMAC_CHAN0_COUNT ($arg0+0x88)
  __shx_reg_32 $DMAC_CHAN0_SAR ($arg0+0x90)
  __shx_reg_32 $DMAC_CHAN0_DAR ($arg0+0x98)
end

define st40_dmac_chan_x_regs
  __shx_reg_32 $DMAC_CHAN$arg1_IDENTITY ($arg0+0x00)
  __shx_reg_32 $DMAC_CHAN$arg1_ENABLE ($arg0+0x08)
  __shx_reg_32 $DMAC_CHAN$arg1_DISABLE ($arg0+0x10)
  __shx_reg_32 $DMAC_CHAN$arg1_STATUS ($arg0+0x18)
  __shx_reg_32 $DMAC_CHAN$arg1_ACTION ($arg0+0x20)
  __shx_reg_32 $DMAC_CHAN$arg1_POINTER ($arg0+0x28)
  __shx_reg_32 $DMAC_CHAN$arg1_REQUEST ($arg0+0x30)
  __shx_reg_32 $DMAC_CHAN$arg1_CONTROL ($arg0+0x80)
  __shx_reg_32 $DMAC_CHAN$arg1_COUNT ($arg0+0x88)
  __shx_reg_32 $DMAC_CHAN$arg1_SAR ($arg0+0x90)
  __shx_reg_32 $DMAC_CHAN$arg1_DAR ($arg0+0x98)
  __shx_reg_32 $DMAC_CHAN$arg1_NEXT_PTR ($arg0+0xa0)
  __shx_reg_32 $DMAC_CHAN$arg1_SRC_LENGTH ($arg0+0xa8)
  __shx_reg_32 $DMAC_CHAN$arg1_SRC_STRIDE ($arg0+0xb0)
  __shx_reg_32 $DMAC_CHAN$arg1_DST_LENGTH ($arg0+0xb8)
  __shx_reg_32 $DMAC_CHAN$arg1_DST_STRIDE ($arg0+0xc0)
end

define st40_dmac_regs
  __shx_reg_32 $DMAC_VCR_STATUS ($arg0+0x00)
  __shx_reg_32 $DMAC_VCR_VERSION ($arg0+0x08)
  __shx_reg_32 $DMAC_ENABLE ($arg0+0x10)
  __shx_reg_32 $DMAC_DISABLE ($arg0+0x18)
  __shx_reg_32 $DMAC_STATUS ($arg0+0x20)
  __shx_reg_32 $DMAC_INTERRUPT ($arg0+0x28)
  __shx_reg_32 $DMAC_ERROR ($arg0+0x30)
  __shx_reg_32 $DMAC_DEFINED ($arg0+0x38)
  __shx_reg_32 $DMAC_HANDSHAKE ($arg0+0x40)

  st40_dmac_chan_0_regs ($arg0+0x100)
  st40_dmac_chan_x_regs ($arg0+0x200) 1
  st40_dmac_chan_x_regs ($arg0+0x300) 2
  st40_dmac_chan_x_regs ($arg0+0x400) 3
  st40_dmac_chan_x_regs ($arg0+0x500) 4
end
##}}}

##{{{  ST40 PIO
## Parallel I/O control registers (all ST40 variants)
define st40_pio_regs
  __shx_reg_32 $PIO$arg1_POUT ($arg0+0x00)
  __shx_reg_32 $PIO$arg1_PIN ($arg0+0x10)
  __shx_reg_32 $PIO$arg1_PC0 ($arg0+0x20)
  __shx_reg_32 $PIO$arg1_PC1 ($arg0+0x30)
  __shx_reg_32 $PIO$arg1_PC2 ($arg0+0x40)
  __shx_reg_32 $PIO$arg1_PCOMP ($arg0+0x50)
  __shx_reg_32 $PIO$arg1_PMASK ($arg0+0x60)

  ## PIO pseudo registers
  __shx_reg_32 $PIO$arg1_SET_POUT ($arg0+0x04)
  __shx_reg_32 $PIO$arg1_CLEAR_POUT ($arg0+0x08)
  __shx_reg_32 $PIO$arg1_SET_PC0 ($arg0+0x24)
  __shx_reg_32 $PIO$arg1_CLEAR_PC0 ($arg0+0x28)
  __shx_reg_32 $PIO$arg1_SET_PC1 ($arg0+0x34)
  __shx_reg_32 $PIO$arg1_CLEAR_PC1 ($arg0+0x38)
  __shx_reg_32 $PIO$arg1_SET_PC2 ($arg0+0x44)
  __shx_reg_32 $PIO$arg1_CLEAR_PC2 ($arg0+0x48)
  __shx_reg_32 $PIO$arg1_SET_PCOMP ($arg0+0x54)
  __shx_reg_32 $PIO$arg1_CLEAR_PCOMP ($arg0+0x58)
  __shx_reg_32 $PIO$arg1_SET_PMASK ($arg0+0x64)
  __shx_reg_32 $PIO$arg1_CLEAR_PMASK ($arg0+0x68)
end
##}}}

##{{{  ST40 LMI
## Local Memory Interface control registers (all ST40 variants)
define st40_lmi1_regs
  if ($argc < 2)
    __shx_reg_64 $LMI_VCR ($arg0+0x000000)
    __shx_reg_64 $LMI_MIM ($arg0+0x000008)
    __shx_reg_64 $LMI_SCR ($arg0+0x000010)
    __shx_reg_64 $LMI_STR ($arg0+0x000018)
    __shx_reg_64 $LMI_PBS ($arg0+0x000020)
    __shx_reg_64 $LMI_COC ($arg0+0x000028)
    __shx_reg_64 $LMI_SDRA0 ($arg0+0x000030)
    __shx_reg_64 $LMI_SDRA1 ($arg0+0x000038)
    __shx_reg_64 $LMI_CIC ($arg0+0x000040)
    __shx_reg_32 $LMI_SDMR0 ($arg0+0x800000)
    __shx_reg_32 $LMI_SDMR1 ($arg0+0x900000)
  else
    __shx_reg_64 $LMI$arg1_VCR ($arg0+0x000000)
    __shx_reg_64 $LMI$arg1_MIM ($arg0+0x000008)
    __shx_reg_64 $LMI$arg1_SCR ($arg0+0x000010)
    __shx_reg_64 $LMI$arg1_STR ($arg0+0x000018)
    __shx_reg_64 $LMI$arg1_PBS ($arg0+0x000020)
    __shx_reg_64 $LMI$arg1_COC ($arg0+0x000028)
    __shx_reg_64 $LMI$arg1_SDRA0 ($arg0+0x000030)
    __shx_reg_64 $LMI$arg1_SDRA1 ($arg0+0x000038)
    __shx_reg_64 $LMI$arg1_CIC ($arg0+0x000040)
    __shx_reg_32 $LMI$arg1_SDMR0 ($arg0+0x800000)
    __shx_reg_32 $LMI$arg1_SDMR1 ($arg0+0x900000)
  end
end

define st40_lmi2_regs
  if ($argc < 2)
    st40_lmi1_regs $arg0
  else
    st40_lmi1_regs $arg0 $arg1
  end
end

define st40_lmi3_regs
  if ($argc < 2)
    __shx_reg_64 $LMI_VCR ($arg0+0x000000)
    __shx_reg_64 $LMI_MIM ($arg0+0x000008)
    __shx_reg_64 $LMI_SCR ($arg0+0x000010)
    __shx_reg_64 $LMI_STR ($arg0+0x000018)
    __shx_reg_64 $LMI_PBS ($arg0+0x000020)
    __shx_reg_64 $LMI_COC ($arg0+0x000028)
    __shx_reg_64 $LMI_SDRA0 ($arg0+0x000030)
    __shx_reg_64 $LMI_SDRA1 ($arg0+0x000038)
    __shx_reg_64 $LMI_CIC ($arg0+0x000040)
    __shx_reg_32 $LMI_SDMR0 ($arg0+0x000048)
    __shx_reg_32 $LMI_SDMR1 ($arg0+0x000050)
  else
    __shx_reg_64 $LMI$arg1_VCR ($arg0+0x000000)
    __shx_reg_64 $LMI$arg1_MIM ($arg0+0x000008)
    __shx_reg_64 $LMI$arg1_SCR ($arg0+0x000010)
    __shx_reg_64 $LMI$arg1_STR ($arg0+0x000018)
    __shx_reg_64 $LMI$arg1_PBS ($arg0+0x000020)
    __shx_reg_64 $LMI$arg1_COC ($arg0+0x000028)
    __shx_reg_64 $LMI$arg1_SDRA0 ($arg0+0x000030)
    __shx_reg_64 $LMI$arg1_SDRA1 ($arg0+0x000038)
    __shx_reg_64 $LMI$arg1_CIC ($arg0+0x000040)
    __shx_reg_32 $LMI$arg1_SDMR0 ($arg0+0x000048)
    __shx_reg_32 $LMI$arg1_SDMR1 ($arg0+0x000050)
  end
end

define st40_lmi4_regs
  if ($argc < 2)
    st40_lmi3_regs $arg0
  else
    st40_lmi3_regs $arg0 $arg1
  end
end
##}}}

##{{{  ST40 LMIGP
define st40_lmigp_regs
  if ($argc < 2)
    __shx_reg_64 $LMI_VCR ($arg0+0x00)
    __shx_reg_64 $LMI_MIM ($arg0+0x08)
    __shx_reg_64 $LMI_SCR ($arg0+0x10)
    __shx_reg_64 $LMI_STR ($arg0+0x18)
    __shx_reg_64 $LMI_GCC ($arg0+0x28)
    __shx_reg_64 $LMI_SDRA0 ($arg0+0x30)
    __shx_reg_64 $LMI_SDRA1 ($arg0+0x38)
    __shx_reg_64 $LMI_CCO ($arg0+0x40)
    __shx_reg_32 $LMI_SDMR0 ($arg0+0x48)
    __shx_reg_32 $LMI_SDMR1 ($arg0+0x50)
  else
    __shx_reg_64 $LMI$arg1_VCR ($arg0+0x00)
    __shx_reg_64 $LMI$arg1_MIM ($arg0+0x08)
    __shx_reg_64 $LMI$arg1_SCR ($arg0+0x10)
    __shx_reg_64 $LMI$arg1_STR ($arg0+0x18)
    __shx_reg_64 $LMI$arg1_GCC ($arg0+0x28)
    __shx_reg_64 $LMI$arg1_SDRA0 ($arg0+0x30)
    __shx_reg_64 $LMI$arg1_SDRA1 ($arg0+0x38)
    __shx_reg_64 $LMI$arg1_CCO ($arg0+0x40)
    __shx_reg_32 $LMI$arg1_SDMR0 ($arg0+0x48)
    __shx_reg_32 $LMI$arg1_SDMR1 ($arg0+0x50)
  end
end
##}}}

##{{{  ST40 FMI (HCMOS7)
## Flash Memory Interface control registers (all ST40 variants)
define st40_fmi_hcmos7_regs
  __shx_reg_64 $FMI_VCR ($arg0+0x0000)
  __shx_reg_32 $FMI_STATUSCFG ($arg0+0x0010)
  __shx_reg_32 $FMI_STATUSLOCK ($arg0+0x0018)
  __shx_reg_32 $FMI_LOCK ($arg0+0x0020)
  __shx_reg_32 $FMI_GENCFG ($arg0+0x0028)
  __shx_reg_32 $FMI_SDRAMNOPGEN ($arg0+0x0030)
  __shx_reg_32 $FMI_SDRAMMODEREG ($arg0+0x0038)
  __shx_reg_32 $FMI_SDRAMINIT ($arg0+0x0040)
  __shx_reg_32 $FMI_REFRESHINIT ($arg0+0x0048)
  __shx_reg_32 $FMI_FLASHCLKSEL ($arg0+0x0050)
  __shx_reg_32 $FMI_SDRAMCLKSEL ($arg0+0x0058)
  __shx_reg_32 $FMI_MPXCLKSEL ($arg0+0x0060)
  __shx_reg_32 $FMI_CLKENABLE ($arg0+0x0068)
  __shx_reg_32 $FMI_BANK0_FMICONFIGDATA0 ($arg0+0x0100)
  __shx_reg_32 $FMI_BANK0_FMICONFIGDATA1 ($arg0+0x0108)
  __shx_reg_32 $FMI_BANK0_FMICONFIGDATA2 ($arg0+0x0110)
  __shx_reg_32 $FMI_BANK0_FMICONFIGDATA3 ($arg0+0x0118)
  __shx_reg_32 $FMI_BANK1_FMICONFIGDATA0 ($arg0+0x0140)
  __shx_reg_32 $FMI_BANK1_FMICONFIGDATA1 ($arg0+0x0148)
  __shx_reg_32 $FMI_BANK1_FMICONFIGDATA2 ($arg0+0x0150)
  __shx_reg_32 $FMI_BANK1_FMICONFIGDATA3 ($arg0+0x0158)
  __shx_reg_32 $FMI_BANK2_FMICONFIGDATA0 ($arg0+0x0180)
  __shx_reg_32 $FMI_BANK2_FMICONFIGDATA1 ($arg0+0x0188)
  __shx_reg_32 $FMI_BANK2_FMICONFIGDATA2 ($arg0+0x0190)
  __shx_reg_32 $FMI_BANK2_FMICONFIGDATA3 ($arg0+0x0198)
  __shx_reg_32 $FMI_BANK3_FMICONFIGDATA0 ($arg0+0x01c0)
  __shx_reg_32 $FMI_BANK3_FMICONFIGDATA1 ($arg0+0x01c8)
  __shx_reg_32 $FMI_BANK3_FMICONFIGDATA2 ($arg0+0x01d0)
  __shx_reg_32 $FMI_BANK3_FMICONFIGDATA3 ($arg0+0x01d8)
  __shx_reg_32 $FMI_BANK4_FMICONFIGDATA0 ($arg0+0x0200)
  __shx_reg_32 $FMI_BANK4_FMICONFIGDATA1 ($arg0+0x0208)
  __shx_reg_32 $FMI_BANK4_FMICONFIGDATA2 ($arg0+0x0210)
  __shx_reg_32 $FMI_BANK4_FMICONFIGDATA3 ($arg0+0x0218)
  __shx_reg_32 $FMI_BANK5_FMICONFIGDATA0 ($arg0+0x0240)
  __shx_reg_32 $FMI_BANK5_FMICONFIGDATA1 ($arg0+0x0248)
  __shx_reg_32 $FMI_BANK5_FMICONFIGDATA2 ($arg0+0x0250)
  __shx_reg_32 $FMI_BANK5_FMICONFIGDATA3 ($arg0+0x0258)

  __shx_reg_32 $FMI_BANK0_BASEADDRESS ($arg0+0x0800)
  __shx_reg_32 $FMI_BANK1_BASEADDRESS ($arg0+0x0810)
  __shx_reg_32 $FMI_BANK2_BASEADDRESS ($arg0+0x0820)
  __shx_reg_32 $FMI_BANK3_BASEADDRESS ($arg0+0x0830)
  __shx_reg_32 $FMI_BANK4_BASEADDRESS ($arg0+0x0840)
  __shx_reg_32 $FMI_BANK5_BASEADDRESS ($arg0+0x0850)
  __shx_reg_32 $FMI_BANK_ENABLE ($arg0+0x0860)
end
##}}}

##{{{  ST40 EMI
## Enhanced Flash Memory Interface control registers (all ST40 variants)
define st40_emi_regs
  __shx_reg_64 $EMI_VCR ($arg0+0x0000)
  __shx_reg_32 $EMI_STATUSCFG ($arg0+0x0010)
  __shx_reg_32 $EMI_STATUSLOCK ($arg0+0x0018)
  __shx_reg_32 $EMI_LOCK ($arg0+0x0020)
  __shx_reg_32 $EMI_GENCFG ($arg0+0x0028)
  __shx_reg_32 $EMI_SDRAMNOPGEN ($arg0+0x0030)
  __shx_reg_32 $EMI_SDRAMMODEREG ($arg0+0x0038)
  __shx_reg_32 $EMI_SDRAMINIT ($arg0+0x0040)
  __shx_reg_32 $EMI_REFRESHINIT ($arg0+0x0048)
  __shx_reg_32 $EMI_FLASHCLKSEL ($arg0+0x0050)
  __shx_reg_32 $EMI_SDRAMCLKSEL ($arg0+0x0058)
  __shx_reg_32 $EMI_MPXCLKSEL ($arg0+0x0060)
  __shx_reg_32 $EMI_CLKENABLE ($arg0+0x0068)
  __shx_reg_32 $EMI_BANK0_EMICONFIGDATA0 ($arg0+0x0100)
  __shx_reg_32 $EMI_BANK0_EMICONFIGDATA1 ($arg0+0x0108)
  __shx_reg_32 $EMI_BANK0_EMICONFIGDATA2 ($arg0+0x0110)
  __shx_reg_32 $EMI_BANK0_EMICONFIGDATA3 ($arg0+0x0118)
  __shx_reg_32 $EMI_BANK1_EMICONFIGDATA0 ($arg0+0x0140)
  __shx_reg_32 $EMI_BANK1_EMICONFIGDATA1 ($arg0+0x0148)
  __shx_reg_32 $EMI_BANK1_EMICONFIGDATA2 ($arg0+0x0150)
  __shx_reg_32 $EMI_BANK1_EMICONFIGDATA3 ($arg0+0x0158)
  __shx_reg_32 $EMI_BANK2_EMICONFIGDATA0 ($arg0+0x0180)
  __shx_reg_32 $EMI_BANK2_EMICONFIGDATA1 ($arg0+0x0188)
  __shx_reg_32 $EMI_BANK2_EMICONFIGDATA2 ($arg0+0x0190)
  __shx_reg_32 $EMI_BANK2_EMICONFIGDATA3 ($arg0+0x0198)
  __shx_reg_32 $EMI_BANK3_EMICONFIGDATA0 ($arg0+0x01c0)
  __shx_reg_32 $EMI_BANK3_EMICONFIGDATA1 ($arg0+0x01c8)
  __shx_reg_32 $EMI_BANK3_EMICONFIGDATA2 ($arg0+0x01d0)
  __shx_reg_32 $EMI_BANK3_EMICONFIGDATA3 ($arg0+0x01d8)
  __shx_reg_32 $EMI_BANK4_EMICONFIGDATA0 ($arg0+0x0200)
  __shx_reg_32 $EMI_BANK4_EMICONFIGDATA1 ($arg0+0x0208)
  __shx_reg_32 $EMI_BANK4_EMICONFIGDATA2 ($arg0+0x0210)
  __shx_reg_32 $EMI_BANK4_EMICONFIGDATA3 ($arg0+0x0218)
  __shx_reg_32 $EMI_BANK5_EMICONFIGDATA0 ($arg0+0x0240)
  __shx_reg_32 $EMI_BANK5_EMICONFIGDATA1 ($arg0+0x0248)
  __shx_reg_32 $EMI_BANK5_EMICONFIGDATA2 ($arg0+0x0250)
  __shx_reg_32 $EMI_BANK5_EMICONFIGDATA3 ($arg0+0x0258)

  __shx_reg_32 $EMI_BANK0_BASEADDRESS ($arg0+0x0800)
  __shx_reg_32 $EMI_BANK1_BASEADDRESS ($arg0+0x0810)
  __shx_reg_32 $EMI_BANK2_BASEADDRESS ($arg0+0x0820)
  __shx_reg_32 $EMI_BANK3_BASEADDRESS ($arg0+0x0830)
  __shx_reg_32 $EMI_BANK4_BASEADDRESS ($arg0+0x0840)
  __shx_reg_32 $EMI_BANK5_BASEADDRESS ($arg0+0x0850)
  __shx_reg_32 $EMI_BANK_ENABLE ($arg0+0x0860)
end
##}}}

##{{{  ST40 PCI (HCMOS7)
## Peripheral Component Interconnect control registers (all ST40 variants)
define st40_pci_hcmos7_regs
  ## PCI Local Registers
  __shx_reg_32 $PCI_VCR_STATUS ($arg0+0x000000)
  __shx_reg_32 $PCI_VCR_VERSION ($arg0+0x000008)
  __shx_reg_32 $PCI_CR ($arg0+0x000010)
  __shx_reg_32 $PCI_LSR0 ($arg0+0x000014)
  __shx_reg_32 $PCI_LAR0 ($arg0+0x00001c)
  __shx_reg_32 $PCI_INT ($arg0+0x000024)
  __shx_reg_32 $PCI_INTM ($arg0+0x000028)
  __shx_reg_32 $PCI_AIR ($arg0+0x00002c)
  __shx_reg_32 $PCI_CIR ($arg0+0x000030)
  __shx_reg_32 $PCI_AINT ($arg0+0x000040)
  __shx_reg_32 $PCI_AINTM ($arg0+0x000044)
  __shx_reg_32 $PCI_BMIR ($arg0+0x000048)
  __shx_reg_32 $PCI_PAR ($arg0+0x00004c)
  __shx_reg_32 $PCI_MBR ($arg0+0x000050)
  __shx_reg_32 $PCI_IOBR ($arg0+0x000054)
  __shx_reg_32 $PCI_PINT ($arg0+0x000058)
  __shx_reg_32 $PCI_PINTM ($arg0+0x00005c)
  __shx_reg_32 $PCI_MBMR ($arg0+0x000070)
  __shx_reg_32 $PCI_IOBMR ($arg0+0x000074)
  __shx_reg_32 $PCI_PDR ($arg0+0x000078)

  ## PCI Configuration Space Registers (CSR)
  __shx_reg_16 $PCI_VID ($arg0+0x010000)
  __shx_reg_16 $PCI_DID ($arg0+0x010002)
  __shx_reg_16 $PCI_CMD ($arg0+0x010004)
  __shx_reg_16 $PCI_STATUS ($arg0+0x010006)
  __shx_reg_32 $PCI_RID_CLASS ($arg0+0x010008)
  __shx_reg_8  $PCI_CLS ($arg0+0x01000c)
  __shx_reg_8  $PCI_MLT ($arg0+0x01000d)
  __shx_reg_8  $PCI_HDR ($arg0+0x01000e)
  __shx_reg_8  $PCI_BIST ($arg0+0x01000f)
  __shx_reg_32 $PCI_MBAR0 ($arg0+0x010010)
  __shx_reg_32 $PCI_IBAR ($arg0+0x010018)
  __shx_reg_16 $PCI_SVID ($arg0+0x01002c)
  __shx_reg_16 $PCI_SID ($arg0+0x01002e)
  __shx_reg_8  $PCI_CP ($arg0+0x010034)
  __shx_reg_8  $PCI_INTLINE ($arg0+0x01003c)
  __shx_reg_8  $PCI_INTPIN ($arg0+0x01003d)
  __shx_reg_8  $PCI_MINGNT ($arg0+0x01003e)
  __shx_reg_8  $PCI_MAXLAT ($arg0+0x01003f)
  __shx_reg_8  $PCI_TRDYTIME ($arg0+0x010040)
  __shx_reg_8  $PCI_RETRYTIME ($arg0+0x010041)
  __shx_reg_8  $PCI_CID ($arg0+0x0100dc)
  __shx_reg_8  $PCI_NIP ($arg0+0x0100dd)
  __shx_reg_16 $PCI_PMC ($arg0+0x0100de)
  __shx_reg_16 $PCI_PMCSR ($arg0+0x0100e0)
  __shx_reg_8  $PCI_PMCSR_BSE ($arg0+0x0100e2)
  __shx_reg_8  $PCI_PCDD ($arg0+0x0100e3)
end
##}}}

##{{{  ST40 PCI
## Peripheral Component Interconnect control registers (all ST40 variants)
define st40_pci_regs
  ## PCI Local Registers
  __shx_reg_32 $PCI_VCR_STATUS ($arg0+0x000000)
  __shx_reg_32 $PCI_VCR_VERSION ($arg0+0x000008)
  __shx_reg_32 $PCI_CR ($arg0+0x000010)
  __shx_reg_32 $PCI_LSR0 ($arg0+0x000014)
  __shx_reg_32 $PCI_LAR0 ($arg0+0x00001c)
  __shx_reg_32 $PCI_INT ($arg0+0x000024)
  __shx_reg_32 $PCI_INTM ($arg0+0x000028)
  __shx_reg_32 $PCI_AIR ($arg0+0x00002c)
  __shx_reg_32 $PCI_CIR ($arg0+0x000030)
  __shx_reg_32 $PCI_AINT ($arg0+0x000040)
  __shx_reg_32 $PCI_AINTM ($arg0+0x000044)
  __shx_reg_32 $PCI_BMIR ($arg0+0x000048)
  __shx_reg_32 $PCI_PAR ($arg0+0x00004c)
  __shx_reg_32 $PCI_MBR ($arg0+0x000050)
  __shx_reg_32 $PCI_IOBR ($arg0+0x000054)
  __shx_reg_32 $PCI_PINT ($arg0+0x000058)
  __shx_reg_32 $PCI_PINTM ($arg0+0x00005c)
  __shx_reg_32 $PCI_MBMR ($arg0+0x000070)
  __shx_reg_32 $PCI_IOBMR ($arg0+0x000074)

  ## PCI Local Configuration Registers
  __shx_reg_32 $PCI_WCBAR ($arg0+0x00007c)
  __shx_reg_32 $PCI_LOCCFG_UNLOCK ($arg0+0x000034)
  __shx_reg_32 $PCI_RBARR0 ($arg0+0x000100)
  __shx_reg_32 $PCI_RSR0 ($arg0+0x000104)
  __shx_reg_32 $PCI_RLAR0 ($arg0+0x000108)
  __shx_reg_32 $PCI_RBARR1 ($arg0+0x000110)
  __shx_reg_32 $PCI_RSR1 ($arg0+0x000114)
  __shx_reg_32 $PCI_RLAR1 ($arg0+0x000118)
  __shx_reg_32 $PCI_RBARR2 ($arg0+0x000120)
  __shx_reg_32 $PCI_RSR2 ($arg0+0x000124)
  __shx_reg_32 $PCI_RLAR2 ($arg0+0x000128)
  __shx_reg_32 $PCI_RBARR3 ($arg0+0x000130)
  __shx_reg_32 $PCI_RSR3 ($arg0+0x000134)
  __shx_reg_32 $PCI_RLAR3 ($arg0+0x000138)
  __shx_reg_32 $PCI_RBARR4 ($arg0+0x000140)
  __shx_reg_32 $PCI_RSR4 ($arg0+0x000144)
  __shx_reg_32 $PCI_RLAR4 ($arg0+0x000148)
  __shx_reg_32 $PCI_RBARR5 ($arg0+0x000150)
  __shx_reg_32 $PCI_RSR5 ($arg0+0x000154)
  __shx_reg_32 $PCI_RLAR5 ($arg0+0x000158)
  __shx_reg_32 $PCI_RBARR6 ($arg0+0x000160)
  __shx_reg_32 $PCI_RSR6 ($arg0+0x000164)
  __shx_reg_32 $PCI_RLAR6 ($arg0+0x000168)
  __shx_reg_32 $PCI_RBARR7 ($arg0+0x000170)
  __shx_reg_32 $PCI_RSR7 ($arg0+0x000174)
  __shx_reg_32 $PCI_RLAR7 ($arg0+0x000178)

  ## PCI Configuration Space Registers (CSR)
  __shx_reg_16 $PCI_VID ($arg0+0x010000)
  __shx_reg_16 $PCI_DID ($arg0+0x010002)
  __shx_reg_16 $PCI_CMD ($arg0+0x010004)
  __shx_reg_16 $PCI_STATUS ($arg0+0x010006)
  __shx_reg_32 $PCI_RID_CLASS ($arg0+0x010008)
  __shx_reg_8  $PCI_CLS ($arg0+0x01000c)
  __shx_reg_8  $PCI_MLT ($arg0+0x01000d)
  __shx_reg_8  $PCI_HDR ($arg0+0x01000e)
  __shx_reg_8  $PCI_BIST ($arg0+0x01000f)
  __shx_reg_32 $PCI_MBAR0 ($arg0+0x010010)
  __shx_reg_32 $PCI_IBAR ($arg0+0x010018)
  __shx_reg_16 $PCI_SVID ($arg0+0x01002c)
  __shx_reg_16 $PCI_SID ($arg0+0x01002e)
  __shx_reg_8  $PCI_CP ($arg0+0x010034)
  __shx_reg_8  $PCI_INTLINE ($arg0+0x01003c)
  __shx_reg_8  $PCI_INTPIN ($arg0+0x01003d)
  __shx_reg_8  $PCI_MINGNT ($arg0+0x01003e)
  __shx_reg_8  $PCI_MAXLAT ($arg0+0x01003f)
  __shx_reg_8  $PCI_TRDYTIME ($arg0+0x010040)
  __shx_reg_8  $PCI_RETRYTIME ($arg0+0x010041)
  __shx_reg_8  $PCI_CID ($arg0+0x0100dc)
  __shx_reg_8  $PCI_NIP ($arg0+0x0100dd)
  __shx_reg_16 $PCI_PMC ($arg0+0x0100de)
  __shx_reg_16 $PCI_PMCSR ($arg0+0x0100e0)
  __shx_reg_8  $PCI_PMCSR_BSE ($arg0+0x0100e2)
  __shx_reg_8  $PCI_PCDD ($arg0+0x0100e3)
end
##}}}

##{{{  ST40 EMPI
## External MicroProcessor Interface control registers (all ST40 variants)
define st40_empi_regs
  __shx_reg_32 $EMPI_VCR_STATUS ($arg0+0x0000)
  __shx_reg_32 $EMPI_VCR_VERSION ($arg0+0x0008)
  __shx_reg_32 $EMPI_SYSTEM ($arg0+0x0010)
  __shx_reg_32 $EMPI_ISTATUS ($arg0+0x0018)
  __shx_reg_32 $EMPI_IMASK ($arg0+0x0020)
  __shx_reg_32 $EMPI_MPXCFG ($arg0+0x0028)
  __shx_reg_32 $EMPI_DMAINV ($arg0+0x0030)
  __shx_reg_32 $EMPI_DMACFG0 ($arg0+0x0080)
  __shx_reg_32 $EMPI_DMACFG1 ($arg0+0x0088)
  __shx_reg_32 $EMPI_DMACFG2 ($arg0+0x0090)
  __shx_reg_32 $EMPI_DMACFG3 ($arg0+0x0098)
  __shx_reg_32 $EMPI_DSTATUS0 ($arg0+0x0100)
  __shx_reg_32 $EMPI_DSTATUS1 ($arg0+0x0108)
  __shx_reg_32 $EMPI_DSTATUS2 ($arg0+0x0110)
  __shx_reg_32 $EMPI_DSTATUS3 ($arg0+0x0118)
  __shx_reg_32 $EMPI_RBAR0 ($arg0+0x0200)
  __shx_reg_32 $EMPI_RSR0 ($arg0+0x0208)
  __shx_reg_32 $EMPI_RLAR0 ($arg0+0x0210)
  __shx_reg_32 $EMPI_RBAR1 ($arg0+0x0220)
  __shx_reg_32 $EMPI_RSR1 ($arg0+0x0228)
  __shx_reg_32 $EMPI_RLAR1 ($arg0+0x0230)
  __shx_reg_32 $EMPI_RBAR2 ($arg0+0x0240)
  __shx_reg_32 $EMPI_RSR2 ($arg0+0x0248)
  __shx_reg_32 $EMPI_RLAR2 ($arg0+0x0250)
  __shx_reg_32 $EMPI_RBAR3 ($arg0+0x0260)
  __shx_reg_32 $EMPI_RSR3 ($arg0+0x0268)
  __shx_reg_32 $EMPI_RLAR3 ($arg0+0x0270)
  __shx_reg_32 $EMPI_RBAR4 ($arg0+0x0280)
  __shx_reg_32 $EMPI_RSR4 ($arg0+0x0288)
  __shx_reg_32 $EMPI_RLAR4 ($arg0+0x0290)
  __shx_reg_32 $EMPI_RBAR5 ($arg0+0x02a0)
  __shx_reg_32 $EMPI_RSR5 ($arg0+0x02a8)
  __shx_reg_32 $EMPI_RLAR5 ($arg0+0x02b0)
  __shx_reg_32 $EMPI_RBAR6 ($arg0+0x02c0)
  __shx_reg_32 $EMPI_RSR6 ($arg0+0x02c8)
  __shx_reg_32 $EMPI_RLAR6 ($arg0+0x02d0)
  __shx_reg_32 $EMPI_RBAR7 ($arg0+0x02e0)
  __shx_reg_32 $EMPI_RSR7 ($arg0+0x02e8)
  __shx_reg_32 $EMPI_RLAR7 ($arg0+0x02f0)
end
##}}}

##{{{  ST40 MPXARB
## MPX Arbiter control registers (all ST40 variants)
define st40_mpxarb_regs
  __shx_reg_32 $MPXARB_VCR ($arg0+0x0000)
  __shx_reg_32 $MPXARB_CONTROL ($arg0+0x0010)
  __shx_reg_32 $MPXARB_DLLCONTROL ($arg0+0x4000)
  __shx_reg_32 $MPXARB_DLLSTATUS ($arg0+0x4010)
end
##}}}

##{{{  ST40 MAILBOX
## Mailbox control registers (all ST40 variants)
define st40_mailbox_regs
  if ($argc < 4)
    __shx_reg_32 $MAILBOX_ID_VER ($arg2+0x0000)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG1 ($arg2+0x0004)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG2 ($arg2+0x0008)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG3 ($arg2+0x000c)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG4 ($arg2+0x0010)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG1_SET ($arg2+0x0024)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG2_SET ($arg2+0x0028)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG3_SET ($arg2+0x002c)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG4_SET ($arg2+0x0030)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG1_CLR ($arg2+0x0044)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG2_CLR ($arg2+0x0048)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG3_CLR ($arg2+0x004c)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_STATUS_REG4_CLR ($arg2+0x0050)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG1 ($arg2+0x0064)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG2 ($arg2+0x0068)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG3 ($arg2+0x006c)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG4 ($arg2+0x0070)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG1_SET ($arg2+0x0084)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG2_SET ($arg2+0x0088)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG3_SET ($arg2+0x008c)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG4_SET ($arg2+0x0090)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG1_CLR ($arg2+0x00a4)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG2_CLR ($arg2+0x00a8)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG3_CLR ($arg2+0x00ac)
    __shx_reg_32 $MAILBOX_$arg0_INTERRUPT_ENABLE_REG4_CLR ($arg2+0x00b0)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG1 ($arg2+0x0104)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG2 ($arg2+0x0108)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG3 ($arg2+0x010c)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG4 ($arg2+0x0110)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG1_SET ($arg2+0x0124)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG2_SET ($arg2+0x0128)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG3_SET ($arg2+0x012c)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG4_SET ($arg2+0x0130)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG1_CLR ($arg2+0x0144)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG2_CLR ($arg2+0x0148)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG3_CLR ($arg2+0x014c)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_STATUS_REG4_CLR ($arg2+0x0150)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG1 ($arg2+0x0164)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG2 ($arg2+0x0168)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG3 ($arg2+0x016c)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG4 ($arg2+0x0170)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG1_SET ($arg2+0x0184)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG2_SET ($arg2+0x0188)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG3_SET ($arg2+0x018c)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG4_SET ($arg2+0x0190)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG1_CLR ($arg2+0x01a4)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG2_CLR ($arg2+0x01a8)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG3_CLR ($arg2+0x01ac)
    __shx_reg_32 $MAILBOX_$arg1_INTERRUPT_ENABLE_REG4_CLR ($arg2+0x01b0)
    __shx_reg_32 $MAILBOX_LOCK0 ($arg2+0x0200)
    __shx_reg_32 $MAILBOX_LOCK1 ($arg2+0x0204)
    __shx_reg_32 $MAILBOX_LOCK2 ($arg2+0x0208)
    __shx_reg_32 $MAILBOX_LOCK3 ($arg2+0x020c)
    __shx_reg_32 $MAILBOX_LOCK4 ($arg2+0x0210)
    __shx_reg_32 $MAILBOX_LOCK5 ($arg2+0x0214)
    __shx_reg_32 $MAILBOX_LOCK6 ($arg2+0x0218)
    __shx_reg_32 $MAILBOX_LOCK7 ($arg2+0x021c)
    __shx_reg_32 $MAILBOX_LOCK8 ($arg2+0x0220)
    __shx_reg_32 $MAILBOX_LOCK9 ($arg2+0x0224)
    __shx_reg_32 $MAILBOX_LOCK10 ($arg2+0x0228)
    __shx_reg_32 $MAILBOX_LOCK11 ($arg2+0x022c)
    __shx_reg_32 $MAILBOX_LOCK12 ($arg2+0x0230)
    __shx_reg_32 $MAILBOX_LOCK13 ($arg2+0x0234)
    __shx_reg_32 $MAILBOX_LOCK14 ($arg2+0x0238)
    __shx_reg_32 $MAILBOX_LOCK15 ($arg2+0x023c)
    __shx_reg_32 $MAILBOX_LOCK16 ($arg2+0x0240)
    __shx_reg_32 $MAILBOX_LOCK17 ($arg2+0x0244)
    __shx_reg_32 $MAILBOX_LOCK18 ($arg2+0x0248)
    __shx_reg_32 $MAILBOX_LOCK19 ($arg2+0x024c)
    __shx_reg_32 $MAILBOX_LOCK20 ($arg2+0x0250)
    __shx_reg_32 $MAILBOX_LOCK21 ($arg2+0x0254)
    __shx_reg_32 $MAILBOX_LOCK22 ($arg2+0x0258)
    __shx_reg_32 $MAILBOX_LOCK23 ($arg2+0x025c)
    __shx_reg_32 $MAILBOX_LOCK24 ($arg2+0x0260)
    __shx_reg_32 $MAILBOX_LOCK25 ($arg2+0x0264)
    __shx_reg_32 $MAILBOX_LOCK26 ($arg2+0x0268)
    __shx_reg_32 $MAILBOX_LOCK27 ($arg2+0x026c)
    __shx_reg_32 $MAILBOX_LOCK28 ($arg2+0x0270)
    __shx_reg_32 $MAILBOX_LOCK29 ($arg2+0x0274)
    __shx_reg_32 $MAILBOX_LOCK30 ($arg2+0x0278)
    __shx_reg_32 $MAILBOX_LOCK31 ($arg2+0x027c)
  else
    __shx_reg_32 $MAILBOX$arg3_ID_VER ($arg2+0x0000)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG1 ($arg2+0x0004)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG2 ($arg2+0x0008)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG3 ($arg2+0x000c)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG4 ($arg2+0x0010)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG1_SET ($arg2+0x0024)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG2_SET ($arg2+0x0028)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG3_SET ($arg2+0x002c)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG4_SET ($arg2+0x0030)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG1_CLR ($arg2+0x0044)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG2_CLR ($arg2+0x0048)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG3_CLR ($arg2+0x004c)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_STATUS_REG4_CLR ($arg2+0x0050)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG1 ($arg2+0x0064)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG2 ($arg2+0x0068)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG3 ($arg2+0x006c)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG4 ($arg2+0x0070)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG1_SET ($arg2+0x0084)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG2_SET ($arg2+0x0088)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG3_SET ($arg2+0x008c)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG4_SET ($arg2+0x0090)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG1_CLR ($arg2+0x00a4)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG2_CLR ($arg2+0x00a8)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG3_CLR ($arg2+0x00ac)
    __shx_reg_32 $MAILBOX$arg3_$arg0_INTERRUPT_ENABLE_REG4_CLR ($arg2+0x00b0)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG1 ($arg2+0x0104)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG2 ($arg2+0x0108)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG3 ($arg2+0x010c)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG4 ($arg2+0x0110)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG1_SET ($arg2+0x0124)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG2_SET ($arg2+0x0128)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG3_SET ($arg2+0x012c)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG4_SET ($arg2+0x0130)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG1_CLR ($arg2+0x0144)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG2_CLR ($arg2+0x0148)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG3_CLR ($arg2+0x014c)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_STATUS_REG4_CLR ($arg2+0x0150)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG1 ($arg2+0x0164)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG2 ($arg2+0x0168)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG3 ($arg2+0x016c)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG4 ($arg2+0x0170)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG1_SET ($arg2+0x0184)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG2_SET ($arg2+0x0188)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG3_SET ($arg2+0x018c)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG4_SET ($arg2+0x0190)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG1_CLR ($arg2+0x01a4)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG2_CLR ($arg2+0x01a8)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG3_CLR ($arg2+0x01ac)
    __shx_reg_32 $MAILBOX$arg3_$arg1_INTERRUPT_ENABLE_REG4_CLR ($arg2+0x01b0)
    __shx_reg_32 $MAILBOX$arg3_LOCK0 ($arg2+0x0200)
    __shx_reg_32 $MAILBOX$arg3_LOCK1 ($arg2+0x0204)
    __shx_reg_32 $MAILBOX$arg3_LOCK2 ($arg2+0x0208)
    __shx_reg_32 $MAILBOX$arg3_LOCK3 ($arg2+0x020c)
    __shx_reg_32 $MAILBOX$arg3_LOCK4 ($arg2+0x0210)
    __shx_reg_32 $MAILBOX$arg3_LOCK5 ($arg2+0x0214)
    __shx_reg_32 $MAILBOX$arg3_LOCK6 ($arg2+0x0218)
    __shx_reg_32 $MAILBOX$arg3_LOCK7 ($arg2+0x021c)
    __shx_reg_32 $MAILBOX$arg3_LOCK8 ($arg2+0x0220)
    __shx_reg_32 $MAILBOX$arg3_LOCK9 ($arg2+0x0224)
    __shx_reg_32 $MAILBOX$arg3_LOCK10 ($arg2+0x0228)
    __shx_reg_32 $MAILBOX$arg3_LOCK11 ($arg2+0x022c)
    __shx_reg_32 $MAILBOX$arg3_LOCK12 ($arg2+0x0230)
    __shx_reg_32 $MAILBOX$arg3_LOCK13 ($arg2+0x0234)
    __shx_reg_32 $MAILBOX$arg3_LOCK14 ($arg2+0x0238)
    __shx_reg_32 $MAILBOX$arg3_LOCK15 ($arg2+0x023c)
    __shx_reg_32 $MAILBOX$arg3_LOCK16 ($arg2+0x0240)
    __shx_reg_32 $MAILBOX$arg3_LOCK17 ($arg2+0x0244)
    __shx_reg_32 $MAILBOX$arg3_LOCK18 ($arg2+0x0248)
    __shx_reg_32 $MAILBOX$arg3_LOCK19 ($arg2+0x024c)
    __shx_reg_32 $MAILBOX$arg3_LOCK20 ($arg2+0x0250)
    __shx_reg_32 $MAILBOX$arg3_LOCK21 ($arg2+0x0254)
    __shx_reg_32 $MAILBOX$arg3_LOCK22 ($arg2+0x0258)
    __shx_reg_32 $MAILBOX$arg3_LOCK23 ($arg2+0x025c)
    __shx_reg_32 $MAILBOX$arg3_LOCK24 ($arg2+0x0260)
    __shx_reg_32 $MAILBOX$arg3_LOCK25 ($arg2+0x0264)
    __shx_reg_32 $MAILBOX$arg3_LOCK26 ($arg2+0x0268)
    __shx_reg_32 $MAILBOX$arg3_LOCK27 ($arg2+0x026c)
    __shx_reg_32 $MAILBOX$arg3_LOCK28 ($arg2+0x0270)
    __shx_reg_32 $MAILBOX$arg3_LOCK29 ($arg2+0x0274)
    __shx_reg_32 $MAILBOX$arg3_LOCK30 ($arg2+0x0278)
    __shx_reg_32 $MAILBOX$arg3_LOCK31 ($arg2+0x027c)
  end
end
##}}}

##{{{  ST40 SYSCONF
## System configuration registers (all ST40 variants)
define st40_sysconf_regs
  __shx_reg_32 $SYSCONF_VCR ($arg0+0x00)
  __shx_reg_64 $SYSCONF_SYS_CON1 ($arg0+0x10)
  __shx_reg_64 $SYSCONF_SYS_CON2 ($arg0+0x18)
  __shx_reg_8  $SYSCONF_CNV_STATUS ($arg0+0x20)
  __shx_reg_8  $SYSCONF_CNV_SET ($arg0+0x28)
  __shx_reg_8  $SYSCONF_CNV_CLEAR ($arg0+0x30)
  __shx_reg_32 $SYSCONF_CNV_CONTROL ($arg0+0x38)
  __shx_reg_64 $SYSCONF_SYS_STAT1 ($arg0+0x40)
  __shx_reg_64 $SYSCONF_SYS_STAT2 ($arg0+0x48)
end
##}}}

##{{{  ST40 SSC
## Synchronous Serial Controller control registers (all ST40 variants)
define st40_ssc_regs
  __shx_reg_16 $SSC$arg1_BRG ($arg0+0x0000)
  __shx_reg_16 $SSC$arg1_TBUF ($arg0+0x0004)
  __shx_reg_16 $SSC$arg1_RBUF ($arg0+0x0008)
  __shx_reg_16 $SSC$arg1_CTL ($arg0+0x000c)
  __shx_reg_16 $SSC$arg1_IEN ($arg0+0x0010)
  __shx_reg_16 $SSC$arg1_STA ($arg0+0x0014)
  __shx_reg_16 $SSC$arg1_I2C ($arg0+0x0018)
  __shx_reg_16 $SSC$arg1_SLAD ($arg0+0x001c)
  __shx_reg_16 $SSC$arg1_REP_START_HOLD ($arg0+0x0020)
  __shx_reg_16 $SSC$arg1_START_HOLD ($arg0+0x0024)
  __shx_reg_16 $SSC$arg1_REP_START_SETUP ($arg0+0x0028)
  __shx_reg_16 $SSC$arg1_DATA_SETUP ($arg0+0x002c)
  __shx_reg_16 $SSC$arg1_STOP_SETUP ($arg0+0x0030)
  __shx_reg_16 $SSC$arg1_BUS_FREE ($arg0+0x0034)
  __shx_reg_32 $SSC$arg1_CLR_STA ($arg0+0x0080)
  __shx_reg_32 $SSC$arg1_AGFR ($arg0+0x0100)
  __shx_reg_32 $SSC$arg1_PRSC ($arg0+0x0104)
end
##}}}

##{{{  ST40 ASC
## Asynchronous Serial Controller control registers (all ST40 variants)
define st40_asc_regs
  __shx_reg_32 $ASC$arg1_BAUDRATE ($arg0+0x00)
  __shx_reg_32 $ASC$arg1_TXBUFFER ($arg0+0x04)
  __shx_reg_32 $ASC$arg1_RXBUFFER ($arg0+0x08)
  __shx_reg_32 $ASC$arg1_CONTROL ($arg0+0x0c)
  __shx_reg_32 $ASC$arg1_INTENABLE ($arg0+0x10)
  __shx_reg_32 $ASC$arg1_STATUS ($arg0+0x14)
  __shx_reg_32 $ASC$arg1_GUARDTIME ($arg0+0x18)
  __shx_reg_32 $ASC$arg1_TIMEOUT ($arg0+0x1c)
  __shx_reg_32 $ASC$arg1_TXRESET ($arg0+0x20)
  __shx_reg_32 $ASC$arg1_RXRESET ($arg0+0x24)
  __shx_reg_32 $ASC$arg1_RETRIES ($arg0+0x28)
end
##}}}

##{{{  ST40 L2
## L2 cache control registers (all ST40 variants)
define st40_l2_regs
  __shx_reg_32 $L2_VCR ($arg0+0x0000)
  __shx_reg_32 $L2_CFG ($arg0+0x0004)
  __shx_reg_32 $L2_CCR ($arg0+0x0008)
  __shx_reg_32 $L2_SYNC ($arg0+0x000c)
  __shx_reg_32 $L2_IA ($arg0+0x0010)
  __shx_reg_32 $L2_FA ($arg0+0x0014)
  __shx_reg_32 $L2_PA ($arg0+0x0018)
  __shx_reg_32 $L2_IE ($arg0+0x0020)
  __shx_reg_32 $L2_FE ($arg0+0x0024)
  __shx_reg_32 $L2_PE ($arg0+0x0028)
  __shx_reg_32 $L2_IS ($arg0+0x0030)
  __shx_reg_32 $L2_AES ($arg0+0x0040)
  __shx_reg_32 $L2_ATA ($arg0+0x0044)
  __shx_reg_32 $L2_PFR ($arg0+0x0050)
  __shx_reg_32 $L2_STS ($arg0+0x0060)
  __shx_reg_32 $L2_PMC ($arg0+0x0070)
  __shx_reg_32 $L2_ECO ($arg0+0x0074)
  __shx_reg_32 $L2_CCO ($arg0+0x0078)
  __shx_reg_32 $L2_ECI ($arg0+0x007c)
  __shx_reg_32 $L2_CCI ($arg0+0x0080)
  __shx_reg_32 $L2_ADA ($arg0+0x00a0)
  __shx_reg_32 $L2_ECA ($arg0+0x0100)
  __shx_reg_64 $L2_CCA ($arg0+0x0180)
end
##}}}

################################################################################
## ST403xx control registers
################################################################################

##{{{  ST403xx CCN
## Core control registers
define st403xx_ccn_regs
  sh4_ccn_regs

  __shx_reg_32 $CCN_PASCR 0xff000070
  __shx_reg_32 $CCN_RAMCR 0xff000074
  __shx_reg_32 $CCN_IRMCR 0xff000078
end
##}}}

##{{{  ST403xx CPG
## Clock Pulse Generator control registers
define st403xx_cpg_regs
  __shx_reg_8  $CPG_STBCR ($arg0+0x04)
  __shx_reg_16 $CPG_WTCNT ($arg0+0x08)
  __shx_reg_8  $CPG_WTCNT_R ($arg0+0x08)
  __shx_reg_16 $CPG_WTCSR ($arg0+0x0c)
  __shx_reg_8  $CPG_WTCSR_R ($arg0+0x0c)
  __shx_reg_8  $CPG_STBCR2 ($arg0+0x10)
  __shx_reg_16 $CPG_WTCSR2 ($arg0+0x1c)
  __shx_reg_8  $CPG_WTCSR2_R ($arg0+0x1c)
end
##}}}

################################################################################
## STm8000 control registers
################################################################################

##{{{  STm8000 CLOCKGEN
## Clock Generator control registers (all STm8000 variants)
define stm8000_clockgen_regs
  __shx_reg_32 $CLOCKGEN$arg1_PLL1CR1 ($arg0+0x00)
  __shx_reg_32 $CLOCKGEN$arg1_PLL1CR2 ($arg0+0x08)
  __shx_reg_32 $CLOCKGEN$arg1_PLL2CR ($arg0+0x10)
  __shx_reg_32 $CLOCKGEN$arg1_STBREQCR ($arg0+0x18)
  __shx_reg_32 $CLOCKGEN$arg1_STBREQCR_SET ($arg0+0x20)
  __shx_reg_32 $CLOCKGEN$arg1_STBREQCR_CLR ($arg0+0x28)
  __shx_reg_32 $CLOCKGEN$arg1_STBACKCR ($arg0+0x30)
  __shx_reg_32 $CLOCKGEN$arg1_CLK4CR ($arg0+0x38)
  __shx_reg_32 $CLOCKGEN$arg1_CPG_BYPASS ($arg0+0x40)
  __shx_reg_32 $CLOCKGEN$arg1_CLK_RATIO ($arg0+0x48)
  __shx_reg_32 $CLOCKGEN$arg1_CLK1CR ($arg0+0x50)
  __shx_reg_32 $CLOCKGEN$arg1_CLK2CR ($arg0+0x58)
  __shx_reg_32 $CLOCKGEN$arg1_CLK3CR ($arg0+0x60)
  __shx_reg_32 $CLOCKGEN$arg1_CLKDDRCR ($arg0+0x68)
end
##}}}

##{{{  STm8000 FS
## Frequency Synthesiser control registers (all STm8000 variants)
define stm8000_fs_regs
  __shx_reg_32 $FS$arg1_CONFIG_GENERIC_INFO ($arg0+0x00)
  __shx_reg_32 $FS$arg1_CONFIG_CLK_1 ($arg0+0x08)
  __shx_reg_32 $FS$arg1_CONFIG_CLK_2 ($arg0+0x10)
  __shx_reg_32 $FS$arg1_CONFIG_CLK_3 ($arg0+0x18)
  __shx_reg_32 $FS$arg1_CONFIG_CLK_4 ($arg0+0x20)
end
##}}}

##{{{  STm8000 LX GLUE
## Lx Glue control registers (all STm8000 variants)
define stm8000_lx_glue_regs
  __shx_reg_32 $LX_GLUE_VCR_STATUS ($arg0+0x00)
  __shx_reg_32 $LX_GLUE_VCR_VERSION ($arg0+0x08)
  __shx_reg_32 $LX_GLUE_CONTROL_REQ ($arg0+0x10)
  __shx_reg_32 $LX_GLUE_CONTROL_ACK ($arg0+0x18)
  __shx_reg_32 $LX_GLUE_IRQ_STATUS ($arg0+0x20)
  __shx_reg_32 $LX_GLUE_IRQ_CLEAR ($arg0+0x28)
  __shx_reg_32 $LX_GLUE_CH1_THRESHOLD ($arg0+0x30)
  __shx_reg_32 $LX_GLUE_CH2_THRESHOLD ($arg0+0x38)
  __shx_reg_32 $LX_GLUE_CH3_THRESHOLD ($arg0+0x40)
end
##}}}

##{{{  STm8000 SHE
## SHE control registers (all STm8000 variants)
define stm8000_she_regs
  __shx_reg_32 $SHE_VCR_STATUS ($arg0+0x0000)
  __shx_reg_32 $SHE_VCR_VERSION ($arg0+0x0008)
  __shx_reg_32 $SHE_RESET_PD_REQ ($arg0+0x0010)
  __shx_reg_32 $SHE_RESET_PD_ACK ($arg0+0x0018)
  __shx_reg_32 $SHE_HOR_SIZE ($arg0+0x0100)
  __shx_reg_32 $SHE_ADDR_PREFIX ($arg0+0x0108)
  __shx_reg_32 $SHE_CURR_CHROMA ($arg0+0x0110)
  __shx_reg_32 $SHE_FWD_CHROMA ($arg0+0x0118)
  __shx_reg_32 $SHE_BKW_CHROMA ($arg0+0x0120)
  __shx_reg_32 $SHE_MAE_OFFS_FIELD ($arg0+0x0128)
  __shx_reg_32 $SHE_MAE_OFFS_INTERP ($arg0+0x0130)
  __shx_reg_32 $SHE_MAD_THRES_INTRA ($arg0+0x0138)
  __shx_reg_32 $SHE_MAD_OFFS_INTRA ($arg0+0x0140)
  __shx_reg_32 $SHE_MAD_THRES_MC ($arg0+0x0148)
  __shx_reg_32 $SHE_MAD_OFFS_MC ($arg0+0x0150)
  __shx_reg_32 $SHE_CURR_COARSE ($arg0+0x0180)
  __shx_reg_32 $SHE_FWD_COARSE ($arg0+0x0188)
  __shx_reg_32 $SHE_CURR_FINE ($arg0+0x0190)
  __shx_reg_32 $SHE_FWD_FINE ($arg0+0x0198)
  __shx_reg_32 $SHE_BKW_FINE ($arg0+0x01a0)
  __shx_reg_32 $SHE_CAP_COARSE ($arg0+0x01a8)
  __shx_reg_32 $SHE_CAP_FINE ($arg0+0x01b0)
  __shx_reg_32 $SHE_T1_DMA_CONTROL ($arg0+0x01e8)
  __shx_reg_32 $SHE_T1_DMA_BASE ($arg0+0x01f0)
  __shx_reg_32 $SHE_T1_DMA_CURRENT ($arg0+0x01f8)
end
##}}}

################################################################################
## STi5528 control registers
################################################################################

##{{{  STi5528 CLOCKGEN
## Clock Generator control registers (STi5528 variant)
define sti5528_clockgen_regs
  __shx_reg_32 $CLOCKGEN_MD_STATUS ($arg0+0x00)
  __shx_reg_32 $CLOCKGEN_PLL1_CTRL ($arg0+0x04)
  __shx_reg_32 $CLOCKGEN_PLL1_CTRL2 ($arg0+0x08)
  __shx_reg_32 $CLOCKGEN_PLL1_CLK1_CTRL ($arg0+0x0c)
  __shx_reg_32 $CLOCKGEN_PLL1_CLK2_CTRL ($arg0+0x10)
  __shx_reg_32 $CLOCKGEN_PLL1_CLK3_CTRL ($arg0+0x14)
  __shx_reg_32 $CLOCKGEN_PLL1_CLK4_CTRL ($arg0+0x18)
  __shx_reg_32 $CLOCKGEN_PLL1_CLK5_CTRL ($arg0+0x1c)
  __shx_reg_32 $CLOCKGEN_PLL1_CLK6_CTRL ($arg0+0x20)
  __shx_reg_32 $CLOCKGEN_CPG_BYPASS ($arg0+0x24)
  __shx_reg_32 $CLOCKGEN_QSYNTH1_CTRL ($arg0+0x30)
  __shx_reg_32 $CLOCKGEN_QSYNTH1_CLK1_CTRL ($arg0+0x34)
  __shx_reg_32 $CLOCKGEN_QSYNTH1_CLK2_CTRL ($arg0+0x38)
  __shx_reg_32 $CLOCKGEN_IRDA_CTRL ($arg0+0x3c)
  __shx_reg_32 $CLOCKGEN_PLL2_CTRL ($arg0+0x40)
  __shx_reg_32 $CLOCKGEN_QSYNTH2_CTRL ($arg0+0x50)
  __shx_reg_32 $CLOCKGEN_QSYNTH2_CLK1_CTRL ($arg0+0x54)
  __shx_reg_32 $CLOCKGEN_QSYNTH2_CLK2_CTRL ($arg0+0x58)
  __shx_reg_32 $CLOCKGEN_QSYNTH2_CLK3_CTRL ($arg0+0x5c)
  __shx_reg_32 $CLOCKGEN_QSYNTH2_CLK4_CTRL ($arg0+0x60)
  __shx_reg_32 $CLOCKGEN_ST20_CLK_CTRL ($arg0+0x70)
  __shx_reg_32 $CLOCKGEN_ST20_TICK_CTRL ($arg0+0x74)
  __shx_reg_32 $CLOCKGEN_AUDBIT_CLK_CTRL ($arg0+0x78)
  __shx_reg_32 $CLOCKGEN_LP_CLK_CTRL ($arg0+0x7c)
  __shx_reg_32 $CLOCKGEN_AUD_CLK_REF_CTRL ($arg0+0x80)
  __shx_reg_32 $CLOCKGEN_PIX_CLK_CTRL ($arg0+0x84)
  __shx_reg_32 $CLOCKGEN_DVP_CLK_CTRL ($arg0+0x88)
  __shx_reg_32 $CLOCKGEN_PWM_CLK_CTRL ($arg0+0x8c)
  __shx_reg_32 $CLOCKGEN_CKG_LOCK ($arg0+0x90)
  __shx_reg_32 $CLOCKGEN_CKOUT_CTRL ($arg0+0xa0)
  __shx_reg_32 $CLOCKGEN_STB_REQ ($arg0+0xb0)
  __shx_reg_32 $CLOCKGEN_STB_ACK ($arg0+0xb4)
end
##}}}

##{{{  STi5528 SYSCONF
## System configuration registers (STi5528 variant)
define sti5528_sysconf_regs
  __shx_reg_32 $SYSCONF_DEVICEID ($arg0+0x00)
  __shx_reg_32 $SYSCONF_SYS_STA00 ($arg0+0x1c)
  __shx_reg_32 $SYSCONF_SYS_STA01 ($arg0+0x2c)
  __shx_reg_32 $SYSCONF_SYS_CFG00 ($arg0+0x30)
  __shx_reg_32 $SYSCONF_SYS_CFG01 ($arg0+0x34)
  __shx_reg_32 $SYSCONF_SYS_CFG02 ($arg0+0x38)
  __shx_reg_32 $SYSCONF_SYS_CFG03 ($arg0+0x3c)
  __shx_reg_32 $SYSCONF_SYS_CFG04 ($arg0+0x40)
  __shx_reg_32 $SYSCONF_SYS_CFG05 ($arg0+0x44)
  __shx_reg_32 $SYSCONF_SYS_CFG06 ($arg0+0x48)
  __shx_reg_32 $SYSCONF_SYS_CFG07 ($arg0+0x4c)
  __shx_reg_32 $SYSCONF_SYS_CFG08 ($arg0+0x50)
  __shx_reg_32 $SYSCONF_SYS_CFG09 ($arg0+0x54)
  __shx_reg_32 $SYSCONF_SYS_CFG10 ($arg0+0x58)
  __shx_reg_32 $SYSCONF_SYS_CFG11 ($arg0+0x5c)
  __shx_reg_32 $SYSCONF_SYS_CFG12 ($arg0+0x60)
  __shx_reg_32 $SYSCONF_SYS_CFG13 ($arg0+0x64)
end
##}}}

##{{{  STi5528 STBUS
## STBUS control registers (STi5528 variant)
define sti5528_stbus_regs
 __shx_reg_32 $STBUS_GDP0_DISP_PRIORITY ($arg0+0x0000)
 __shx_reg_32 $STBUS_GDP0_LL_PRIORITY ($arg0+0x0004)
 __shx_reg_32 $STBUS_GDP1_DISP_PRIORITY ($arg0+0x0008)
 __shx_reg_32 $STBUS_GDP1_LL_PRIORITY ($arg0+0x000c)
 __shx_reg_32 $STBUS_GDP2_DISP_PRIORITY ($arg0+0x0010)
 __shx_reg_32 $STBUS_GDP2_LL_PRIORITY ($arg0+0x0014)
 __shx_reg_32 $STBUS_GDP3_DISP_PRIORITY ($arg0+0x0018)
 __shx_reg_32 $STBUS_GDP3_LL_PRIORITY ($arg0+0x001c)
 __shx_reg_32 $STBUS_N04_TARGET1_EMI_PRIORITY ($arg0+0x0080)
 __shx_reg_32 $STBUS_N04_TARGET2_LMI_PRIORITY ($arg0+0x0084)
 __shx_reg_32 $STBUS_DISP0_IC_LUMA_PRIORITY ($arg0+0x0100)
 __shx_reg_32 $STBUS_DISP0_IC_CHROMA_PRIORITY ($arg0+0x0104)
 __shx_reg_32 $STBUS_DISP1_IC_LUMA_PRIORITY ($arg0+0x0108)
 __shx_reg_32 $STBUS_DISP1_IC_CHROMA_PRIORITY ($arg0+0x010c)
 __shx_reg_32 $STBUS_DVP_IC_PIX_PRIORITY ($arg0+0x0110)
 __shx_reg_32 $STBUS_DVP_IC_ANC_PRIORITY ($arg0+0x0114)
 __shx_reg_32 $STBUS_N05_TARGET1_EMI_PRIORITY ($arg0+0x0160)
 __shx_reg_32 $STBUS_N05_TARGET2_LMI_PRIORITY ($arg0+0x0164)
 __shx_reg_32 $STBUS_ALP_DISP_PRIORITY ($arg0+0x0200)
 __shx_reg_32 $STBUS_ALP_LL_PRIORITY ($arg0+0x0204)
 __shx_reg_32 $STBUS_CUR_PRIORITY ($arg0+0x0208)
 __shx_reg_32 $STBUS_AUD_IC_RMEM_PRIORITY ($arg0+0x0300)
 __shx_reg_32 $STBUS_AUD_IC_PMEM_PRIORITY ($arg0+0x0304)
 __shx_reg_32 $STBUS_VID_IC_CD_PRIORITY ($arg0+0x0308)
 __shx_reg_32 $STBUS_VID_IC_VD_PRIORITY ($arg0+0x030c)
 __shx_reg_32 $STBUS_N04_EMI_PRIORITY ($arg0+0x0400)
 __shx_reg_32 $STBUS_N04_LMI_PRIORITY ($arg0+0x0404)
 __shx_reg_32 $STBUS_N05_EMI_PRIORITY ($arg0+0x0408)
 __shx_reg_32 $STBUS_N05_LMI_PRIORITY ($arg0+0x040c)
 __shx_reg_32 $STBUS_N06_PRIORITY ($arg0+0x0410)
 __shx_reg_32 $STBUS_N07_PRIORITY ($arg0+0x0414)
 __shx_reg_32 $STBUS_BLT_IC_PRIORITY ($arg0+0x0418)
 __shx_reg_32 $STBUS_N08_TARGET1_EMI_PRIORITY ($arg0+0x0470)
 __shx_reg_32 $STBUS_N08_TARGET2_LMI_PRIORITY ($arg0+0x0474)
 __shx_reg_32 $STBUS_CPUPLUG_IC_PRIORITY ($arg0+0x0500)
 __shx_reg_32 $STBUS_ST20_IC_ID_PRIORITY ($arg0+0x0504)
 __shx_reg_32 $STBUS_N02_PRIORITY ($arg0+0x0508)
 __shx_reg_32 $STBUS_N03_PRIORITY ($arg0+0x050c)
 __shx_reg_32 $STBUS_N08_EMI_PRIORITY ($arg0+0x0510)
 __shx_reg_32 $STBUS_N08_LMI_PRIORITY ($arg0+0x0514)
 __shx_reg_32 $STBUS_N09_PRIORITY ($arg0+0x0518)
 __shx_reg_32 $STBUS_CPUPLUG_IC_LATENCY ($arg0+0x051c)
 __shx_reg_32 $STBUS_ST20_IC_ID_LATENCY ($arg0+0x0520)
 __shx_reg_32 $STBUS_N02_LATENCY ($arg0+0x0524)
 __shx_reg_32 $STBUS_N03_LATENCY ($arg0+0x0528)
 __shx_reg_32 $STBUS_N08_EMI_LATENCY ($arg0+0x052c)
 __shx_reg_32 $STBUS_N08_LMI_LATENCY ($arg0+0x0530)
 __shx_reg_32 $STBUS_N09_LATENCY ($arg0+0x0534)
 __shx_reg_32 $STBUS_N10_TARGET_1_N11_PRIORITY ($arg0+0x0570)
 __shx_reg_32 $STBUS_N10_TARGET_2_EMI_PRIORITY ($arg0+0x0574)
 __shx_reg_32 $STBUS_N10_TARGET_3_LMI_PRIORITY ($arg0+0x0578)
 __shx_reg_32 $STBUS_N10_TARGET_4_N12_PRIORITY ($arg0+0x057c)
end
##}}}

################################################################################
## STb710x control registers
################################################################################

##{{{  STb710x CLOCKGENA
## Clock Generator A control registers (STb710x variant)
define stb710x_clockgena_regs
  __shx_reg_32 $CLOCKGENA_LOCK ($arg0+0x00)
  __shx_reg_32 $CLOCKGENA_MD_STATUS ($arg0+0x04)
  __shx_reg_32 $CLOCKGENA_PLL0_CFG ($arg0+0x08)
  __shx_reg_32 $CLOCKGENA_PLL0_STATUS ($arg0+0x10)
  __shx_reg_32 $CLOCKGENA_PLL0_CLK1_CTRL ($arg0+0x14)
  __shx_reg_32 $CLOCKGENA_PLL0_CLK2_CTRL ($arg0+0x18)
  __shx_reg_32 $CLOCKGENA_PLL0_CLK3_CTRL ($arg0+0x1c)
  __shx_reg_32 $CLOCKGENA_PLL0_CLK4_CTRL ($arg0+0x20)
  __shx_reg_32 $CLOCKGENA_PLL1_CFG ($arg0+0x24)
  __shx_reg_32 $CLOCKGENA_PLL1_STATUS ($arg0+0x2c)
  __shx_reg_32 $CLOCKGENA_CLK_DIV ($arg0+0x30)
  __shx_reg_32 $CLOCKGENA_CLOCK_ENABLE ($arg0+0x34)
  __shx_reg_32 $CLOCKGENA_OUT_CTRL ($arg0+0x38)
  __shx_reg_32 $CLOCKGENA_PLL1_BYPASS ($arg0+0x3c)
end
##}}}

##{{{  STb710x SYSCONF
## System configuration registers (STb710x variant)
define stb710x_sysconf_regs
  __shx_reg_64 $SYSCONF_DEVICEID ($arg0+0x0000)
  __shx_reg_32 $SYSCONF_SYS_STA00 ($arg0+0x0008)
  __shx_reg_32 $SYSCONF_SYS_STA01 ($arg0+0x000c)
  __shx_reg_32 $SYSCONF_SYS_STA02 ($arg0+0x0010)
  __shx_reg_32 $SYSCONF_SYS_STA03 ($arg0+0x0014)
  __shx_reg_32 $SYSCONF_SYS_STA04 ($arg0+0x0018)
  __shx_reg_32 $SYSCONF_SYS_STA05 ($arg0+0x001c)
  __shx_reg_32 $SYSCONF_SYS_STA06 ($arg0+0x0020)
  __shx_reg_32 $SYSCONF_SYS_STA07 ($arg0+0x0024)
  __shx_reg_32 $SYSCONF_SYS_STA08 ($arg0+0x0028)
  __shx_reg_32 $SYSCONF_SYS_STA09 ($arg0+0x002c)
  __shx_reg_32 $SYSCONF_SYS_STA10 ($arg0+0x0030)
  __shx_reg_32 $SYSCONF_SYS_STA11 ($arg0+0x0034)
  __shx_reg_32 $SYSCONF_SYS_STA12 ($arg0+0x0038)
  __shx_reg_32 $SYSCONF_SYS_STA13 ($arg0+0x003c)
  __shx_reg_32 $SYSCONF_SYS_STA14 ($arg0+0x0040)
  __shx_reg_32 $SYSCONF_SYS_STA15 ($arg0+0x0044)
  __shx_reg_32 $SYSCONF_SYS_CFG00 ($arg0+0x0100)
  __shx_reg_32 $SYSCONF_SYS_CFG01 ($arg0+0x0104)
  __shx_reg_32 $SYSCONF_SYS_CFG02 ($arg0+0x0108)
  __shx_reg_32 $SYSCONF_SYS_CFG03 ($arg0+0x010c)
  __shx_reg_32 $SYSCONF_SYS_CFG04 ($arg0+0x0110)
  __shx_reg_32 $SYSCONF_SYS_CFG05 ($arg0+0x0114)
  __shx_reg_32 $SYSCONF_SYS_CFG06 ($arg0+0x0118)
  __shx_reg_32 $SYSCONF_SYS_CFG07 ($arg0+0x011c)
  __shx_reg_32 $SYSCONF_SYS_CFG08 ($arg0+0x0120)
  __shx_reg_32 $SYSCONF_SYS_CFG09 ($arg0+0x0124)
  __shx_reg_32 $SYSCONF_SYS_CFG10 ($arg0+0x0128)
  __shx_reg_32 $SYSCONF_SYS_CFG11 ($arg0+0x012c)
  __shx_reg_32 $SYSCONF_SYS_CFG12 ($arg0+0x0130)
  __shx_reg_32 $SYSCONF_SYS_CFG13 ($arg0+0x0134)
  __shx_reg_32 $SYSCONF_SYS_CFG14 ($arg0+0x0138)
  __shx_reg_32 $SYSCONF_SYS_CFG15 ($arg0+0x013c)
  __shx_reg_32 $SYSCONF_SYS_CFG16 ($arg0+0x0140)
  __shx_reg_32 $SYSCONF_SYS_CFG17 ($arg0+0x0144)
  __shx_reg_32 $SYSCONF_SYS_CFG18 ($arg0+0x0148)
  __shx_reg_32 $SYSCONF_SYS_CFG19 ($arg0+0x014c)
  __shx_reg_32 $SYSCONF_SYS_CFG20 ($arg0+0x0150)
  __shx_reg_32 $SYSCONF_SYS_CFG21 ($arg0+0x0154)
  __shx_reg_32 $SYSCONF_SYS_CFG22 ($arg0+0x0158)
  __shx_reg_32 $SYSCONF_SYS_CFG23 ($arg0+0x015c)
  __shx_reg_32 $SYSCONF_SYS_CFG24 ($arg0+0x0160)
  __shx_reg_32 $SYSCONF_SYS_CFG25 ($arg0+0x0164)
  __shx_reg_32 $SYSCONF_SYS_CFG26 ($arg0+0x0168)
  __shx_reg_32 $SYSCONF_SYS_CFG27 ($arg0+0x016c)
  __shx_reg_32 $SYSCONF_SYS_CFG28 ($arg0+0x0170)
  __shx_reg_32 $SYSCONF_SYS_CFG29 ($arg0+0x0174)
  __shx_reg_32 $SYSCONF_SYS_CFG30 ($arg0+0x0178)
  __shx_reg_32 $SYSCONF_SYS_CFG31 ($arg0+0x017c)
  __shx_reg_32 $SYSCONF_SYS_CFG32 ($arg0+0x0180)
  __shx_reg_32 $SYSCONF_SYS_CFG33 ($arg0+0x0184)
  __shx_reg_32 $SYSCONF_SYS_CFG34 ($arg0+0x0188)
  __shx_reg_32 $SYSCONF_SYS_CFG35 ($arg0+0x018c)
  __shx_reg_32 $SYSCONF_SYS_CFG36 ($arg0+0x0190)
  __shx_reg_32 $SYSCONF_SYS_CFG37 ($arg0+0x0194)
  __shx_reg_32 $SYSCONF_SYS_CFG38 ($arg0+0x0198)
  __shx_reg_32 $SYSCONF_SYS_CFG39 ($arg0+0x019c)
end
##}}}

##{{{  STb710x STBUS
## STBUS control registers (STb710x variant)
define stb710x_stbus_regs
  __shx_reg_32 $STBUS_NODE01_SH4_I_PRIORITY ($arg0+0x00)
  __shx_reg_32 $STBUS_NODE01_LX_DH_I_PRIORITY ($arg0+0x04)
  __shx_reg_32 $STBUS_NODE01_LX_AUD_I_PRIORITY ($arg0+0x08)
  __shx_reg_32 $STBUS_NODE01_SH4_I_LIMIT ($arg0+0x24)
  __shx_reg_32 $STBUS_NODE01_LX_DH_I_LIMIT ($arg0+0x28)
  __shx_reg_32 $STBUS_NODE01_LX_AUD_I_LIMIT ($arg0+0x2c)
  __shx_reg_32 $STBUS_NODE01_N01_TARG_1_LMIVID_PRIORITY ($arg0+0x30)
  __shx_reg_32 $STBUS_NODE01_N01_TARG_2_LMISYS_PRIORITY ($arg0+0x34)
  __shx_reg_32 $STBUS_NODE01_N01_TARG_3_N04_PRIORITY ($arg0+0x38)

  __shx_reg_32 $STBUS_NODE04_N01_PRIORITY ($arg1+0x00)
  __shx_reg_32 $STBUS_NODE04_N02_PRIORITY ($arg1+0x04)
  __shx_reg_32 $STBUS_NODE04_DH_I_0_PRIORITY ($arg1+0x08)
  __shx_reg_32 $STBUS_NODE04_DH_I_1_PRIORITY ($arg1+0x0c)
  __shx_reg_32 $STBUS_NODE04_N03_PRIORITY ($arg1+0x10)
  __shx_reg_32 $STBUS_NODE04_N04_TARG_1_LMIVID_PRIORITY ($arg1+0x50)
  __shx_reg_32 $STBUS_NODE04_N04_TARG_2_N11_PRIORITY ($arg1+0x54)

  __shx_reg_32 $STBUS_NODE05_COMPO_I_GDP1LL_PRIORITY ($arg2+0x00)
  __shx_reg_32 $STBUS_NODE05_COMPO_I_GDP1DISP_PRIORITY ($arg2+0x04)
  __shx_reg_32 $STBUS_NODE05_COMPO_I_GDP2LL_PRIORITY ($arg2+0x08)
  __shx_reg_32 $STBUS_NODE05_COMPO_I_GDP2DISP_PRIORITY ($arg2+0x0c)
  __shx_reg_32 $STBUS_NODE05_COMPO_I_ALPLL_PRIORITY ($arg2+0x10)
  __shx_reg_32 $STBUS_NODE05_COMPO_I_ALPDISP_PRIORITY ($arg2+0x14)
  __shx_reg_32 $STBUS_NODE05_COMPO_I_CUR_PRIORITY ($arg2+0x18)

  __shx_reg_32 $STBUS_NODE09_N08_PRIORITY ($arg3+0x00)
  __shx_reg_32 $STBUS_NODE09_DIRT_I_PRIORITY ($arg3+0x04)
  __shx_reg_32 $STBUS_NODE09_COMMS_I_PRIORITY ($arg3+0x08)
  __shx_reg_32 $STBUS_NODE09_SATA_I_PRIORITY ($arg3+0x0c)
  __shx_reg_32 $STBUS_NODE09_USB_I_PRIORITY ($arg3+0x10)

  __shx_reg_32 $STBUS_NODE11_N05_PRIORITY ($arg4+0x00)
  __shx_reg_32 $STBUS_NODE11_N06_PRIORITY ($arg4+0x04)
  __shx_reg_32 $STBUS_NODE11_N07_PRIORITY ($arg4+0x08)
  __shx_reg_32 $STBUS_NODE11_N09_PRIORITY ($arg4+0x0c)
  __shx_reg_32 $STBUS_NODE11_N04_PRIORITY ($arg4+0x10)
  __shx_reg_32 $STBUS_NODE11_N11_TARG_1_N13_PRIORITY ($arg4+0x50)
  __shx_reg_32 $STBUS_NODE11_N11_TARG_2_N12_PRIORITY ($arg4+0x54)
  __shx_reg_32 $STBUS_NODE11_N11_TARG_3_AUDIO_PRIORITY ($arg4+0x58)
  __shx_reg_32 $STBUS_NODE11_N11_TARG_4_CFGREG_PRIORITY ($arg4+0x5c)
end
##}}}

################################################################################
## STi7200 control registers
################################################################################

##{{{  STi7200 CLOCKGENA
## Clock Generator A control registers (STi7200 variant)
define sti7200_clockgena_regs
  __shx_reg_32 $CLOCKGENA_PLL0_CFG ($arg0+0x00)
  __shx_reg_32 $CLOCKGENA_PLL1_CFG ($arg0+0x04)
  __shx_reg_32 $CLOCKGENA_PLL2_CFG ($arg0+0x08)
  __shx_reg_32 $CLOCKGENA_MUX_CFG ($arg0+0x0c)
  __shx_reg_32 $CLOCKGENA_DIV_CFG ($arg0+0x10)
  __shx_reg_32 $CLOCKGENA_DIV2_CFG ($arg0+0x14)
  __shx_reg_32 $CLOCKGENA_CLKOBS_MUX_CFG ($arg0+0x18)
  __shx_reg_32 $CLOCKGENA_POWER_CFG ($arg0+0x1c)
  __shx_reg_32 $CLOCKGENA_CLKOBS_MAX ($arg0+0x20)
  __shx_reg_32 $CLOCKGENA_CLKOBS_RESULT ($arg0+0x24)
  __shx_reg_32 $CLOCKGENA_CLKOBS_CTRL ($arg0+0x28)
end
##}}}

##{{{  STi7200 CLOCKGENB
## Clock Generator B control registers (STi7200 variant)
define sti7200_clockgenb_regs
  __shx_reg_32 $CLOCKGENB_FS0_SETUP ($arg0+0x00)
  __shx_reg_32 $CLOCKGENB_FS1_SETUP ($arg0+0x04)
  __shx_reg_32 $CLOCKGENB_FS2_SETUP ($arg0+0x08)
  __shx_reg_32 $CLOCKGENB_FS0_CLK1_CFG ($arg0+0x0c)
  __shx_reg_32 $CLOCKGENB_FS0_CLK2_CFG ($arg0+0x10)
  __shx_reg_32 $CLOCKGENB_FS0_CLK3_CFG ($arg0+0x14)
  __shx_reg_32 $CLOCKGENB_FS0_CLK4_CFG ($arg0+0x18)
  __shx_reg_32 $CLOCKGENB_FS1_CLK1_CFG ($arg0+0x1c)
  __shx_reg_32 $CLOCKGENB_FS1_CLK2_CFG ($arg0+0x20)
  __shx_reg_32 $CLOCKGENB_FS1_CLK3_CFG ($arg0+0x24)
  __shx_reg_32 $CLOCKGENB_FS1_CLK4_CFG ($arg0+0x28)
  __shx_reg_32 $CLOCKGENB_FS2_CLK1_CFG ($arg0+0x2c)
  __shx_reg_32 $CLOCKGENB_FS2_CLK2_CFG ($arg0+0x30)
  __shx_reg_32 $CLOCKGENB_FS2_CLK3_CFG ($arg0+0x34)
  __shx_reg_32 $CLOCKGENB_FS2_CLK4_CFG ($arg0+0x38)
  __shx_reg_32 $CLOCKGENB_PLL0_CFG ($arg0+0x3c)
  __shx_reg_32 $CLOCKGENB_CLKRCV_CFG ($arg0+0x40)
  __shx_reg_32 $CLOCKGENB_IN_MUX_CFG ($arg0+0x44)
  __shx_reg_32 $CLOCKGENB_OUT_MUX_CFG ($arg0+0x48)
  __shx_reg_32 $CLOCKGENB_DIV_CFG ($arg0+0x4c)
  __shx_reg_32 $CLOCKGENB_DIV2_CFG ($arg0+0x50)
  __shx_reg_32 $CLOCKGENB_CLKOBS_MUX_CFG ($arg0+0x54)
  __shx_reg_32 $CLOCKGENB_POWER_CFG ($arg0+0x58)
  __shx_reg_32 $CLOCKGENB_CLKOBS_MAX ($arg0+0x5c)
  __shx_reg_32 $CLOCKGENB_CLKOBS_RESULT ($arg0+0x60)
  __shx_reg_32 $CLOCKGENB_CLKOBS_CTRL ($arg0+0x64)
end
##}}}

##{{{  STi7200 SYSCONF
## System configuration registers (STi7200 variant)
define sti7200_sysconf_regs
  __shx_reg_64 $SYSCONF_DEVICEID ($arg0+0x0000)
  __shx_reg_32 $SYSCONF_SYS_STA00 ($arg0+0x0008)
  __shx_reg_32 $SYSCONF_SYS_STA01 ($arg0+0x000c)
  __shx_reg_32 $SYSCONF_SYS_STA02 ($arg0+0x0010)
  __shx_reg_32 $SYSCONF_SYS_STA03 ($arg0+0x0014)
  __shx_reg_32 $SYSCONF_SYS_STA04 ($arg0+0x0018)
  __shx_reg_32 $SYSCONF_SYS_STA05 ($arg0+0x001c)
  __shx_reg_32 $SYSCONF_SYS_STA06 ($arg0+0x0020)
  __shx_reg_32 $SYSCONF_SYS_STA07 ($arg0+0x0024)
  __shx_reg_32 $SYSCONF_SYS_STA08 ($arg0+0x0028)
  __shx_reg_32 $SYSCONF_SYS_STA09 ($arg0+0x002c)
  __shx_reg_32 $SYSCONF_SYS_STA10 ($arg0+0x0030)
  __shx_reg_32 $SYSCONF_SYS_STA11 ($arg0+0x0034)
  __shx_reg_32 $SYSCONF_SYS_STA12 ($arg0+0x0038)
  __shx_reg_32 $SYSCONF_SYS_STA13 ($arg0+0x003c)
  __shx_reg_32 $SYSCONF_SYS_STA14 ($arg0+0x0040)
  __shx_reg_32 $SYSCONF_SYS_STA15 ($arg0+0x0044)
  __shx_reg_32 $SYSCONF_SYS_STA16 ($arg0+0x0048)
  __shx_reg_32 $SYSCONF_SYS_CFG00 ($arg0+0x0100)
  __shx_reg_32 $SYSCONF_SYS_CFG01 ($arg0+0x0104)
  __shx_reg_32 $SYSCONF_SYS_CFG02 ($arg0+0x0108)
  __shx_reg_32 $SYSCONF_SYS_CFG03 ($arg0+0x010c)
  __shx_reg_32 $SYSCONF_SYS_CFG04 ($arg0+0x0110)
  __shx_reg_32 $SYSCONF_SYS_CFG05 ($arg0+0x0114)
  __shx_reg_32 $SYSCONF_SYS_CFG06 ($arg0+0x0118)
  __shx_reg_32 $SYSCONF_SYS_CFG07 ($arg0+0x011c)
  __shx_reg_32 $SYSCONF_SYS_CFG08 ($arg0+0x0120)
  __shx_reg_32 $SYSCONF_SYS_CFG09 ($arg0+0x0124)
  __shx_reg_32 $SYSCONF_SYS_CFG10 ($arg0+0x0128)
  __shx_reg_32 $SYSCONF_SYS_CFG11 ($arg0+0x012c)
  __shx_reg_32 $SYSCONF_SYS_CFG12 ($arg0+0x0130)
  __shx_reg_32 $SYSCONF_SYS_CFG13 ($arg0+0x0134)
  __shx_reg_32 $SYSCONF_SYS_CFG14 ($arg0+0x0138)
  __shx_reg_32 $SYSCONF_SYS_CFG15 ($arg0+0x013c)
  __shx_reg_32 $SYSCONF_SYS_CFG16 ($arg0+0x0140)
  __shx_reg_32 $SYSCONF_SYS_CFG17 ($arg0+0x0144)
  __shx_reg_32 $SYSCONF_SYS_CFG18 ($arg0+0x0148)
  __shx_reg_32 $SYSCONF_SYS_CFG19 ($arg0+0x014c)
  __shx_reg_32 $SYSCONF_SYS_CFG20 ($arg0+0x0150)
  __shx_reg_32 $SYSCONF_SYS_CFG21 ($arg0+0x0154)
  __shx_reg_32 $SYSCONF_SYS_CFG22 ($arg0+0x0158)
  __shx_reg_32 $SYSCONF_SYS_CFG23 ($arg0+0x015c)
  __shx_reg_32 $SYSCONF_SYS_CFG24 ($arg0+0x0160)
  __shx_reg_32 $SYSCONF_SYS_CFG25 ($arg0+0x0164)
  __shx_reg_32 $SYSCONF_SYS_CFG26 ($arg0+0x0168)
  __shx_reg_32 $SYSCONF_SYS_CFG27 ($arg0+0x016c)
  __shx_reg_32 $SYSCONF_SYS_CFG28 ($arg0+0x0170)
  __shx_reg_32 $SYSCONF_SYS_CFG29 ($arg0+0x0174)
  __shx_reg_32 $SYSCONF_SYS_CFG30 ($arg0+0x0178)
  __shx_reg_32 $SYSCONF_SYS_CFG31 ($arg0+0x017c)
  __shx_reg_32 $SYSCONF_SYS_CFG32 ($arg0+0x0180)
  __shx_reg_32 $SYSCONF_SYS_CFG33 ($arg0+0x0184)
  __shx_reg_32 $SYSCONF_SYS_CFG34 ($arg0+0x0188)
  __shx_reg_32 $SYSCONF_SYS_CFG35 ($arg0+0x018c)
  __shx_reg_32 $SYSCONF_SYS_CFG36 ($arg0+0x0190)
  __shx_reg_32 $SYSCONF_SYS_CFG37 ($arg0+0x0194)
  __shx_reg_32 $SYSCONF_SYS_CFG38 ($arg0+0x0198)
  __shx_reg_32 $SYSCONF_SYS_CFG39 ($arg0+0x019c)
  __shx_reg_32 $SYSCONF_SYS_CFG40 ($arg0+0x01a0)
  __shx_reg_32 $SYSCONF_SYS_CFG41 ($arg0+0x01a4)
  __shx_reg_32 $SYSCONF_SYS_CFG42 ($arg0+0x01a8)
  __shx_reg_32 $SYSCONF_SYS_CFG43 ($arg0+0x01ac)
  __shx_reg_32 $SYSCONF_SYS_CFG44 ($arg0+0x01b0)
  __shx_reg_32 $SYSCONF_SYS_CFG45 ($arg0+0x01b4)
  __shx_reg_32 $SYSCONF_SYS_CFG46 ($arg0+0x01b8)
  __shx_reg_32 $SYSCONF_SYS_CFG47 ($arg0+0x01bc)
  __shx_reg_32 $SYSCONF_SYS_CFG48 ($arg0+0x01c0)
  __shx_reg_32 $SYSCONF_SYS_CFG49 ($arg0+0x01c4)
  __shx_reg_32 $SYSCONF_SYS_CFG50 ($arg0+0x01c8)
  __shx_reg_32 $SYSCONF_SYS_CFG51 ($arg0+0x01cc)
  __shx_reg_32 $SYSCONF_SYS_CFG52 ($arg0+0x01d0)
  __shx_reg_32 $SYSCONF_SYS_CFG53 ($arg0+0x01d4)
  __shx_reg_32 $SYSCONF_SYS_CFG54 ($arg0+0x01d8)
  __shx_reg_32 $SYSCONF_SYS_CFG55 ($arg0+0x01dc)
  __shx_reg_32 $SYSCONF_SYS_CFG56 ($arg0+0x01e0)
  __shx_reg_32 $SYSCONF_SYS_CFG57 ($arg0+0x01e4)
  __shx_reg_32 $SYSCONF_SYS_CFG58 ($arg0+0x01e8)
  __shx_reg_32 $SYSCONF_SYS_CFG59 ($arg0+0x01ec)
  __shx_reg_32 $SYSCONF_SYS_CFG60 ($arg0+0x01f0)
  __shx_reg_32 $SYSCONF_SYS_CFG61 ($arg0+0x01f4)
end
##}}}

################################################################################
## STd1000 control registers
################################################################################

##{{{  STd1000 MIXER
## Mixer control registers (STd1000 variant)
define std1000_mixer_regs
  __shx_reg_32 $MIXER_GENC ($arg0+0x0000)
  __shx_reg_32 $MIXER_INTMASK ($arg0+0x0010)
  __shx_reg_32 $MIXER_INT ($arg0+0x0018)
  __shx_reg_32 $MIXER_BANKC ($arg0+0x0020)
  __shx_reg_32 $MIXER_ROWADDRMSK ($arg0+0x0028)
  __shx_reg_32 $MIXER_LMIBADDR ($arg0+0x0030)
  __shx_reg_32 $MIXER_DDRP1 ($arg0+0x0038)
  __shx_reg_32 $MIXER_BDLCK ($arg0+0x0040)
  __shx_reg_32 $MIXER_BDLAT ($arg0+0x0048)
  __shx_reg_32 $MIXER_BDCRIS ($arg0+0x0050)
  __shx_reg_32 $MIXER_BDHPP ($arg0+0x0058)
  __shx_reg_32 $MIXER_BDQTHD ($arg0+0x0060)
  __shx_reg_32 $MIXER_BDLDINEF ($arg0+0x0068)
  __shx_reg_32 $MIXER_BDSTINEF ($arg0+0x0070)
  __shx_reg_32 $MIXER_INFLWREG ($arg0+0x0080)
  __shx_reg_32 $MIXER_ARBFLWREG ($arg0+0x0088)
  __shx_reg_32 $MIXER_BKFLWREG ($arg0+0x0090)
  __shx_reg_32 $MIXER_HPPFLWREG ($arg0+0x00a0)
  __shx_reg_32 $MIXER_REQMEM ($arg0+0x00b0)
  __shx_reg_32 $MIXER_RESMEM ($arg0+0x00b8)

  __shx_reg_32 $MIXER_DBGROPC ($arg0+0x0100)
  __shx_reg_32 $MIXER_DBGLPADDR ($arg0+0x0108)
  __shx_reg_32 $MIXER_DBGHPADDR ($arg0+0x010c)
  __shx_reg_32 $MIXER_DBGAERR ($arg0+0x0110)
  __shx_reg_32 $MIXER_SPSRCREG_0 ($arg0+0x0200)
  __shx_reg_32 $MIXER_SPSRCREG_1 ($arg0+0x0204)
  __shx_reg_32 $MIXER_SPSRCREG_2 ($arg0+0x0208)
  __shx_reg_32 $MIXER_SPSRCREG_3 ($arg0+0x020c)
  __shx_reg_32 $MIXER_SPSRCREG_4 ($arg0+0x0210)
  __shx_reg_32 $MIXER_SPSRCREG_5 ($arg0+0x0214)
  __shx_reg_32 $MIXER_SPSRCREG_6 ($arg0+0x0218)
  __shx_reg_32 $MIXER_SPSRCREG_7 ($arg0+0x021c)
  __shx_reg_32 $MIXER_SPSRCREG_8 ($arg0+0x0220)
  __shx_reg_32 $MIXER_SPSRCREG_9 ($arg0+0x0224)
  __shx_reg_32 $MIXER_SPSRCREG_10 ($arg0+0x0228)
  __shx_reg_32 $MIXER_SPSRCREG_11 ($arg0+0x022c)
  __shx_reg_32 $MIXER_SPSRCREG_12 ($arg0+0x0230)
  __shx_reg_32 $MIXER_SPSRCREG_13 ($arg0+0x0234)
  __shx_reg_32 $MIXER_SPSRCREG_14 ($arg0+0x0238)
  __shx_reg_32 $MIXER_SPSRCREG_15 ($arg0+0x023c)
  __shx_reg_32 $MIXER_SPSRCREG_16 ($arg0+0x0240)
  __shx_reg_32 $MIXER_SPSRCREG_17 ($arg0+0x0244)
  __shx_reg_32 $MIXER_SPSRCREG_18 ($arg0+0x0248)
  __shx_reg_32 $MIXER_SPSRCREG_19 ($arg0+0x024c)
  __shx_reg_32 $MIXER_SPSRCREG_20 ($arg0+0x0250)
  __shx_reg_32 $MIXER_SPSRCREG_21 ($arg0+0x0254)
  __shx_reg_32 $MIXER_SPSRCREG_22 ($arg0+0x0258)
  __shx_reg_32 $MIXER_SPSRCREG_23 ($arg0+0x025c)
  __shx_reg_32 $MIXER_SPSRCREG_24 ($arg0+0x0260)
  __shx_reg_32 $MIXER_SPSRCREG_25 ($arg0+0x0264)
  __shx_reg_32 $MIXER_SPSRCREG_26 ($arg0+0x0268)
  __shx_reg_32 $MIXER_SPSRCREG_27 ($arg0+0x026c)
  __shx_reg_32 $MIXER_SPSRCREG_28 ($arg0+0x0270)
  __shx_reg_32 $MIXER_SPSRCREG_29 ($arg0+0x0274)
  __shx_reg_32 $MIXER_SPSRCREG_30 ($arg0+0x0278)
  __shx_reg_32 $MIXER_SPSRCREG_31 ($arg0+0x027c)
  __shx_reg_32 $MIXER_SPSRCREG_32 ($arg0+0x0280)
  __shx_reg_32 $MIXER_SPSRCREG_33 ($arg0+0x0284)
  __shx_reg_32 $MIXER_SPSRCREG_34 ($arg0+0x0288)
  __shx_reg_32 $MIXER_SPSRCREG_35 ($arg0+0x028c)
  __shx_reg_32 $MIXER_SPSRCREG_36 ($arg0+0x0290)
  __shx_reg_32 $MIXER_SPSRCREG_37 ($arg0+0x0294)
  __shx_reg_32 $MIXER_SPSRCREG_38 ($arg0+0x0298)
  __shx_reg_32 $MIXER_SPSRCREG_39 ($arg0+0x029c)
  __shx_reg_32 $MIXER_SPSRCREG_40 ($arg0+0x02a0)
  __shx_reg_32 $MIXER_SPSRCREG_41 ($arg0+0x02a4)
  __shx_reg_32 $MIXER_SPSRCREG_42 ($arg0+0x02a8)
  __shx_reg_32 $MIXER_SPSRCREG_43 ($arg0+0x02ac)
  __shx_reg_32 $MIXER_SPSRCREG_44 ($arg0+0x02b0)
  __shx_reg_32 $MIXER_SPSRCREG_45 ($arg0+0x02b4)
  __shx_reg_32 $MIXER_SPSRCREG_46 ($arg0+0x02b8)
  __shx_reg_32 $MIXER_SPSRCREG_47 ($arg0+0x02bc)
  __shx_reg_32 $MIXER_SPSRCREG_48 ($arg0+0x02c0)
  __shx_reg_32 $MIXER_SPSRCREG_49 ($arg0+0x02c4)
  __shx_reg_32 $MIXER_SPSRCREG_50 ($arg0+0x02c8)
  __shx_reg_32 $MIXER_SPSRCREG_51 ($arg0+0x02cc)
  __shx_reg_32 $MIXER_SPSRCREG_52 ($arg0+0x02d0)
  __shx_reg_32 $MIXER_SPSRCREG_53 ($arg0+0x02d4)
  __shx_reg_32 $MIXER_SPSRCREG_54 ($arg0+0x02d8)
  __shx_reg_32 $MIXER_SPSRCREG_55 ($arg0+0x02dc)
  __shx_reg_32 $MIXER_SPSRCREG_56 ($arg0+0x02e0)
  __shx_reg_32 $MIXER_SPSRCREG_57 ($arg0+0x02e4)
  __shx_reg_32 $MIXER_SPSRCREG_58 ($arg0+0x02e8)
  __shx_reg_32 $MIXER_SPSRCREG_59 ($arg0+0x02ec)
  __shx_reg_32 $MIXER_SPSRCREG_60 ($arg0+0x02f0)
  __shx_reg_32 $MIXER_SPSRCREG_61 ($arg0+0x02f4)
  __shx_reg_32 $MIXER_SPSRCREG_62 ($arg0+0x02f8)
  __shx_reg_32 $MIXER_SPSRCREG_63 ($arg0+0x02fc)
end
##}}}

##{{{  STd1000 CLOCKGEN
## Clock Generator control registers (STd1000 variant)
define std1000_clockgen_regs
  __shx_reg_32 $CLOCKGEN_CTL_SEL ($arg0+0x00)
  __shx_reg_32 $CLOCKGEN_CTL_CKG_ANA ($arg0+0x04)
  __shx_reg_32 $CLOCKGEN_CTL_EN ($arg0+0x08)
  __shx_reg_32 $CLOCKGEN_CTL_RATIO ($arg0+0x0C)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1 ($arg0+0x10)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_1 ($arg0+0x14)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_2 ($arg0+0x18)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_3 ($arg0+0x1c)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_4 ($arg0+0x20)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2 ($arg0+0x24)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_1 ($arg0+0x28)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_2 ($arg0+0x2c)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_3 ($arg0+0x30)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_4 ($arg0+0x34)
  __shx_reg_32 $CLOCKGEN_CTL_PLL1_FREQ ($arg0+0x38)
  __shx_reg_32 $CLOCKGEN_CTL_PLL1_SSC ($arg0+0x3c)
  __shx_reg_32 $CLOCKGEN_CTL_PLL2_FREQ ($arg0+0x40)
  __shx_reg_32 $CLOCKGEN_CTL_PLL2_SSC ($arg0+0x44)
  __shx_reg_32 $CLOCKGEN_CTL_PLL3 ($arg0+0x48)
  __shx_reg_32 $CLOCKGEN_CTL_PLL_USBPHY ($arg0+0x4c)
  __shx_reg_32 $CLOCKGEN_STATUS_PLL ($arg0+0x50)
  __shx_reg_32 $CLOCKGEN_CTL_FREQ_MEAS_NORTH ($arg0+0x54)
  __shx_reg_32 $CLOCKGEN_RES_FREQ_MEAS_NORTH ($arg0+0x58)
  __shx_reg_32 $CLOCKGEN_CTL_FREQ_MEAS_SOUTH ($arg0+0x5c)
  __shx_reg_32 $CLOCKGEN_RES_FREQ_MEAS_SOUTH ($arg0+0x60)
  __shx_reg_32 $CLOCKGEN_RES_JITTER_MEAS_NORTH ($arg0+0x64)
  __shx_reg_32 $CLOCKGEN_RES_JITTER_MEAS_SOUTH ($arg0+0x68)
end
##}}}

##{{{  STd1000 GENCONF
## General configuration registers (STd1000 variant)
define std1000_genconf_regs
  __shx_reg_32 $GENCONF_PIO_CONF ($arg0+0x0000)
  __shx_reg_32 $GENCONF_SYS_CONF ($arg0+0x0004)
  __shx_reg_32 $GENCONF_STBY_CMD ($arg0+0x0008)
  __shx_reg_32 $GENCONF_STBY_STATUS ($arg0+0x000c)
  __shx_reg_32 $GENCONF_LMI_DLL1_CTL ($arg0+0x0010)
  __shx_reg_32 $GENCONF_LMI_PDL4_PDL5_CTL ($arg0+0x0014)
  __shx_reg_32 $GENCONF_LMI_DLL2_CTL ($arg0+0x0018)
  __shx_reg_32 $GENCONF_LMI_PDL_0_3_MSB_CTL ($arg0+0x001c)
  __shx_reg_32 $GENCONF_LMI_PDL_0_3_LSB_CTL ($arg0+0x0020)
  __shx_reg_32 $GENCONF_LMI_STATUS ($arg0+0x0024)
  __shx_reg_32 $GENCONF_LMI_PL_CTL ($arg0+0x0028)
  __shx_reg_32 $GENCONF_IO_DDR2_CONF ($arg0+0x002c)
  __shx_reg_32 $GENCONF_COMP_EMI_CONF_3V3 ($arg0+0x0030)
  __shx_reg_32 $GENCONF_COMP_FPD_CONF_3V3 ($arg0+0x0034)
  __shx_reg_32 $GENCONF_COMP_PIO1_CONF_3V3 ($arg0+0x0038)
  __shx_reg_32 $GENCONF_COMP_PIO2_CONF_3V3 ($arg0+0x003c)
  __shx_reg_32 $GENCONF_COMP_DDR2_CONF_1V8 ($arg0+0x0040)
  __shx_reg_32 $GENCONF_COMP_EMI_STATUS ($arg0+0x0044)
  __shx_reg_32 $GENCONF_COMP_FPD_STATUS ($arg0+0x0048)
  __shx_reg_32 $GENCONF_COMP_PIO1_STATUS ($arg0+0x004c)
  __shx_reg_32 $GENCONF_COMP_PIO2_STATUS ($arg0+0x0050)
  __shx_reg_32 $GENCONF_COMP_DDR2_STATUS ($arg0+0x0054)
  __shx_reg_32 $GENCONF_ANTIFUSE_ENGI_STATUS ($arg0+0x0058)
  __shx_reg_32 $GENCONF_ANTIFUSE_CUSTOM_STATUS ($arg0+0x005c)
  __shx_reg_32 $GENCONF_GENERAL_PURPOSE ($arg0+0x0060)
end
##}}}

################################################################################
## STd2000 control registers
################################################################################

##{{{  STd2000 MIXER
## Mixer control registers (STd2000 variant)
define std2000_mixer_regs
  __shx_reg_32 $MIXER$arg1_MODC ($arg0+0x0000)
  __shx_reg_32 $MIXER$arg1_ATC ($arg0+0x0010)
  __shx_reg_32 $MIXER$arg1_LMIAC ($arg0+0x0014)
  __shx_reg_32 $MIXER$arg1_FILC ($arg0+0x0040)
  __shx_reg_32 $MIXER$arg1_BTAC ($arg0+0x0044)
  __shx_reg_32 $MIXER$arg1_HPC ($arg0+0x0048)
  __shx_reg_32 $MIXER$arg1_RDBSC ($arg0+0x0060)
  __shx_reg_32 $MIXER$arg1_WDBSC ($arg0+0x0064)
  __shx_reg_32 $MIXER$arg1_PMC ($arg0+0x0068)
  __shx_reg_32 $MIXER$arg1_OBCLC ($arg0+0x0080)
  __shx_reg_32 $MIXER$arg1_BKCLC ($arg0+0x0084)
  __shx_reg_32 $MIXER$arg1_LPBWC ($arg0+0x0088)
  __shx_reg_32 $MIXER$arg1_STAC ($arg0+0x00c0)
  __shx_reg_32 $MIXER$arg1_FILS ($arg0+0x00c4)
  __shx_reg_32 $MIXER$arg1_BWLDSTS ($arg0+0x00c8)
  __shx_reg_32 $MIXER$arg1_BWARBS ($arg0+0x00cc)
  __shx_reg_32 $MIXER$arg1_LLDSTS ($arg0+0x00d0)
  __shx_reg_32 $MIXER$arg1_LARBS ($arg0+0x00d4)
  __shx_reg_32 $MIXER$arg1_LPOBS ($arg0+0x00d8)
  __shx_reg_32 $MIXER$arg1_LPPNBS ($arg0+0x00dc)
  __shx_reg_32 $MIXER$arg1_BK01S ($arg0+0x00e0)
  __shx_reg_32 $MIXER$arg1_BK23S ($arg0+0x00e4)
  __shx_reg_32 $MIXER$arg1_HBTAS ($arg0+0x00e8)
  __shx_reg_32 $MIXER$arg1_PBMS ($arg0+0x00ec)
  __shx_reg_32 $MIXER$arg1_ISRCFC ($arg0+0x00f0)
  __shx_reg_32 $MIXER$arg1_ISRCFS ($arg0+0x00f4)
  __shx_reg_32 $MIXER$arg1_GENPC0 ($arg0+0x0100)
  __shx_reg_32 $MIXER$arg1_GENPS0 ($arg0+0x0104)
  __shx_reg_32 $MIXER$arg1_GENPC1 ($arg0+0x0108)
  __shx_reg_32 $MIXER$arg1_GENPS1 ($arg0+0x010c)
  __shx_reg_32 $MIXER$arg1_DTBSAC ($arg0+0x0140)
  __shx_reg_32 $MIXER$arg1_DTBEAC ($arg0+0x0144)
  __shx_reg_32 $MIXER$arg1_DTAC ($arg0+0x0148)
  __shx_reg_32 $MIXER$arg1_DTAMC ($arg0+0x014c)
  __shx_reg_32 $MIXER$arg1_DTSC ($arg0+0x0150)
  __shx_reg_32 $MIXER$arg1_DFSC0 ($arg0+0x0154)
  __shx_reg_32 $MIXER$arg1_DFSC1 ($arg0+0x0158)
  __shx_reg_32 $MIXER$arg1_DS ($arg0+0x015c)
end
##}}}

##{{{  STd2000 CLOCKGEN
## Clock Generator control registers (STd2000 variant)
define std2000_clockgen_regs
  __shx_reg_32 $CLOCKGEN_CTL_SEL ($arg0+0x00)
  __shx_reg_32 $CLOCKGEN_CTL_EN ($arg0+0x04)
  __shx_reg_32 $CLOCKGEN_CTL_RATIO ($arg0+0x08)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1 ($arg0+0x0c)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_1 ($arg0+0x10)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_2 ($arg0+0x14)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_3 ($arg0+0x18)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_1_4 ($arg0+0x1c)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2 ($arg0+0x20)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_1 ($arg0+0x24)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_2 ($arg0+0x28)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_3 ($arg0+0x2c)
  __shx_reg_32 $CLOCKGEN_CTL_SYNTH4X_2_4 ($arg0+0x30)
  __shx_reg_32 $CLOCKGEN_CTL_PLL1 ($arg0+0x34)
  __shx_reg_32 $CLOCKGEN_CTL_PLL2 ($arg0+0x38)
  __shx_reg_32 $CLOCKGEN_STATUS_PLL ($arg0+0x3c)
end
##}}}

##{{{  STd2000 GENCONF
## General configuration registers (STd2000 variant)
define std2000_genconf_regs
  __shx_reg_32 $GENCONF_PIO_CONF ($arg0+0x0000)
  __shx_reg_32 $GENCONF_SYS_CONF ($arg0+0x0004)
  __shx_reg_32 $GENCONF_STBY_CMD ($arg0+0x0008)
  __shx_reg_32 $GENCONF_STBY_STATUS ($arg0+0x000c)
  __shx_reg_32 $GENCONF_LMI1_DLL1_CTL ($arg0+0x0010)
  __shx_reg_32 $GENCONF_LMI1_PDL4_CTL ($arg0+0x0014)
  __shx_reg_32 $GENCONF_LMI1_DLL2_CTL ($arg0+0x0018)
  __shx_reg_32 $GENCONF_LMI1_PDL_0_3_MSB_CTL ($arg0+0x001c)
  __shx_reg_32 $GENCONF_LMI1_PDL_0_3_LSB_CTL ($arg0+0x0020)
  __shx_reg_32 $GENCONF_LMI1_STATUS ($arg0+0x0024)
  __shx_reg_32 $GENCONF_LMI2_DLL1_CTL ($arg0+0x0028)
  __shx_reg_32 $GENCONF_LMI2_PDL4_CTL ($arg0+0x002c)
  __shx_reg_32 $GENCONF_LMI2_DLL2_CTL ($arg0+0x0030)
  __shx_reg_32 $GENCONF_LMI2_PDL_0_3_MSB_CTL ($arg0+0x0034)
  __shx_reg_32 $GENCONF_LMI2_PDL_0_3_LSB_CTL ($arg0+0x0038)
  __shx_reg_32 $GENCONF_LMI2_STATUS ($arg0+0x003c)
  __shx_reg_32 $GENCONF_DDR_CONF ($arg0+0x0040)
  __shx_reg_32 $GENCONF_COMPENSATION_CONF_3V3_A ($arg0+0x0044)
  __shx_reg_32 $GENCONF_COMPENSATION_CONF_3V3_B ($arg0+0x0048)
  __shx_reg_32 $GENCONF_COMPENSATION_CONF_2V5 ($arg0+0x004c)
  __shx_reg_32 $GENCONF_COMPENSATION_CONF_1V8 ($arg0+0x0050)
  __shx_reg_32 $GENCONF_COMPENSATION_STATUS_3V3_A ($arg0+0x0054)
  __shx_reg_32 $GENCONF_COMPENSATION_STATUS_3V3_B ($arg0+0x0058)
  __shx_reg_32 $GENCONF_COMPENSATION_STATUS_2V5 ($arg0+0x005c)
  __shx_reg_32 $GENCONF_COMPENSATION_STATUS_1V8 ($arg0+0x0060)
end
##}}}

################################################################################
## STV0498 control registers
################################################################################

##{{{  STV0498 CLOCKGEN
## Clock Generator control registers (STV0498 variant)
define stv0498_clockgen_regs
  __shx_reg_32 $CLOCKGEN_LOCK ($arg0+0x0000)
  __shx_reg_32 $CLOCKGEN_PLLXN_LOCK_STATUS ($arg0+0x0004)
  __shx_reg_32 $CLOCKGEN_REG_LOCK_STATUS ($arg0+0x0008)
  __shx_reg_32 $CLOCKGEN_MODE_CONTROL ($arg0+0x0010)
  __shx_reg_32 $CLOCKGEN_CLOCK_STANDBY ($arg0+0x0014)
  __shx_reg_32 $CLOCKGEN_REDUCED_PWR_CTRL ($arg0+0x0018)
  __shx_reg_32 $CLOCKGEN_PLLXN_CONFIG0 ($arg0+0x0020)
  __shx_reg_32 $CLOCKGEN_PLLXN_CONFIG1 ($arg0+0x0024)
  __shx_reg_32 $CLOCKGEN_CLOCK_FE_DIV ($arg0+0x0040)
  __shx_reg_32 $CLOCKGEN_CLOCK_SELECT_CFG ($arg0+0x0044)
  __shx_reg_32 $CLOCKGEN_DIVIDER_FORCE_CFG ($arg0+0x0048)
  __shx_reg_32 $CLOCKGEN_CLOCK_OBSERV_CFG ($arg0+0x004c)
  __shx_reg_32 $CLOCKGEN_CLKDIV0_CONFIG0 ($arg0+0x0060)
  __shx_reg_32 $CLOCKGEN_CLKDIV0_CONFIG1 ($arg0+0x0064)
  __shx_reg_32 $CLOCKGEN_CLKDIV0_CONFIG2 ($arg0+0x0068)
  __shx_reg_32 $CLOCKGEN_CLKDIV1_CONFIG0 ($arg0+0x0070)
  __shx_reg_32 $CLOCKGEN_CLKDIV1_CONFIG1 ($arg0+0x0074)
  __shx_reg_32 $CLOCKGEN_CLKDIV1_CONFIG2 ($arg0+0x0078)
  __shx_reg_32 $CLOCKGEN_CLKDIV2_CONFIG0 ($arg0+0x0080)
  __shx_reg_32 $CLOCKGEN_CLKDIV2_CONFIG1 ($arg0+0x0084)
  __shx_reg_32 $CLOCKGEN_CLKDIV2_CONFIG2 ($arg0+0x0088)
  __shx_reg_32 $CLOCKGEN_CLKDIV3_CONFIG0 ($arg0+0x0090)
  __shx_reg_32 $CLOCKGEN_CLKDIV3_CONFIG1 ($arg0+0x0094)
  __shx_reg_32 $CLOCKGEN_CLKDIV3_CONFIG2 ($arg0+0x0098)
  __shx_reg_32 $CLOCKGEN_CLKDIV4_CONFIG0 ($arg0+0x00a0)
  __shx_reg_32 $CLOCKGEN_CLKDIV4_CONFIG1 ($arg0+0x00a4)
  __shx_reg_32 $CLOCKGEN_CLKDIV4_CONFIG2 ($arg0+0x00a8)
  __shx_reg_32 $CLOCKGEN_CLKDIV5_CONFIG0 ($arg0+0x00b0)
  __shx_reg_32 $CLOCKGEN_CLKDIV5_CONFIG1 ($arg0+0x00b4)
  __shx_reg_32 $CLOCKGEN_CLKDIV5_CONFIG2 ($arg0+0x00b8)
  __shx_reg_32 $CLOCKGEN_CLKDIV6_CONFIG0 ($arg0+0x00c0)
  __shx_reg_32 $CLOCKGEN_CLKDIV6_CONFIG1 ($arg0+0x00c4)
  __shx_reg_32 $CLOCKGEN_CLKDIV6_CONFIG2 ($arg0+0x00c8)
  __shx_reg_32 $CLOCKGEN_CLKDIV7_CONFIG0 ($arg0+0x00d0)
  __shx_reg_32 $CLOCKGEN_CLKDIV7_CONFIG1 ($arg0+0x00d4)
  __shx_reg_32 $CLOCKGEN_CLKDIV7_CONFIG2 ($arg0+0x00d8)
  __shx_reg_32 $CLOCKGEN_CLKSDIV_CONFIG0 ($arg0+0x0100)
  __shx_reg_32 $CLOCKGEN_CLKSDIV_CONFIG1 ($arg0+0x0104)
  __shx_reg_32 $CLOCKGEN_CLKSDIV_CONFIG2 ($arg0+0x0108)
  __shx_reg_32 $CLOCKGEN_RTC_DIV_CONFIG ($arg0+0x0110)
end
##}}}

##{{{  STV0498 SYSCONF
## System configuration registers (STV0498 variant)
define stv0498_sysconf_regs
  __shx_reg_64 $SYSCONF_DEVICEID ($arg0+0x0000)
  __shx_reg_32 $SYSCONF_SYS_STA00 ($arg0+0x0008)
  __shx_reg_32 $SYSCONF_SYS_STA01 ($arg0+0x000c)
  __shx_reg_32 $SYSCONF_SYS_CFG00 ($arg0+0x0100)
  __shx_reg_32 $SYSCONF_SYS_CFG01 ($arg0+0x0104)
  __shx_reg_32 $SYSCONF_SYS_CFG02 ($arg0+0x0108)
  __shx_reg_32 $SYSCONF_SYS_CFG03 ($arg0+0x010c)
  __shx_reg_32 $SYSCONF_SYS_CFG04 ($arg0+0x0110)
  __shx_reg_32 $SYSCONF_SYS_CFG05 ($arg0+0x0114)
  __shx_reg_32 $SYSCONF_SYS_CFG06 ($arg0+0x0118)
  __shx_reg_32 $SYSCONF_SYS_CFG07 ($arg0+0x011c)
  __shx_reg_32 $SYSCONF_SYS_CFG08 ($arg0+0x0120)
  __shx_reg_32 $SYSCONF_SYS_CFG09 ($arg0+0x0124)
  __shx_reg_32 $SYSCONF_SYS_CFG10 ($arg0+0x0128)
  __shx_reg_32 $SYSCONF_SYS_CFG11 ($arg0+0x012c)
  __shx_reg_32 $SYSCONF_SYS_CFG12 ($arg0+0x0130)
  __shx_reg_32 $SYSCONF_SYS_CFG13 ($arg0+0x0134)
  __shx_reg_32 $SYSCONF_SYS_CFG14 ($arg0+0x0138)
  __shx_reg_32 $SYSCONF_SYS_CFG15 ($arg0+0x013c)
  __shx_reg_32 $SYSCONF_SYS_CFG16 ($arg0+0x0140)
  __shx_reg_32 $SYSCONF_SYS_CFG17 ($arg0+0x0144)
  __shx_reg_32 $SYSCONF_SYS_CFG18 ($arg0+0x0148)
end
##}}}

################################################################################
## SH4/ST40 cores
################################################################################

##{{{  SH4/ST40-100
## SH4-100 core control registers
define sh4100_core_si_regs
  ## Core SH4 control registers
  sh4_ccn_regs
  sh4_ubc_regs
  sh4_udi_regs
  sh4_aud_regs

  ## Generic SH4 control registers
  sh4_tmu_regs 0xffd80000
  sh4_rtc_regs 0xffc80000
  sh4_intc_regs 0xffd00000
  sh4_cpg_regs 0xffc00000
end

## ST40-100 core control registers
define st40100_core_si_regs
  sh4100_core_si_regs

  ## Common ST40 control registers
  st40_scif_regs 0xffe00000 1
  st40_scif_regs 0xffe80000 2

  st40_intc2_regs 0xfe080000
end
##}}}

##{{{  SH4/ST40-200
## SH4-200 core control registers
define sh4200_core_si_regs
  sh4100_core_si_regs
end

## ST40-200 core control registers
define st40200_core_si_regs
  sh4200_core_si_regs

  ## Common ST40 control registers
  st40_scif_regs 0xffe80000 2
end
##}}}

##{{{  SH4/ST40-400
## SH4-400 core control registers
define sh4400_core_si_regs
  sh4200_core_si_regs
end

## ST40-400 core control registers
define st40400_core_si_regs
  st40200_core_si_regs
end
##}}}

##{{{  SH4/ST40-500
## SH4-500 core control registers
define sh4500_core_si_regs
  sh4200_core_si_regs
end

## ST40-500 core control registers
define st40500_core_si_regs
  st40200_core_si_regs
end
##}}}

##{{{  ST40-300
## ST40-300 core control registers
define st40300_core_si_regs
  ## Core ST403xx control registers
  st403xx_ccn_regs

  ## Core SH4 control registers
  sh4_ubc_regs
  sh4_udi_regs
  sh4_aud_regs

  ## Generic SH4 control registers
  sh4_tmu_regs 0xffd80000
  sh4_intc_regs 0xffd00000

  ## Common ST403xx control registers
  st403xx_cpg_regs 0xffc00000
end
##}}}

################################################################################
## SH4/ST40 SoCs
################################################################################

##{{{  SH7750
## SH7750 control registers
define sh7750_si_regs
  ## Core SH4 control registers
  sh4_ccn_regs
  sh4_ubc_regs
  sh4_udi_regs
  sh4_aud_regs

  ## Generic SH4 control registers
  sh4_tmu_regs 0xffd80000
  sh4_rtc_regs 0xffc80000
  sh4_cpg_regs 0xffc00000

  ## SH775x and SH7750 control registers
  sh7750_intc_regs

  sh775x_bsc_regs
  sh775x_dmac_regs
  sh775x_sci_regs
  sh775x_scif_regs
end
##}}}

##{{{  SH7751
## SH7751 control registers
define sh7751_si_regs
  sh4100_core_si_regs

  ## SH775x and SH7751 control registers
  sh7751_tmu_regs
  sh7751_intc_regs
  sh7751_cpg_regs

  sh775x_bsc_regs
  sh775x_dmac_regs
  sh775x_sci_regs
  sh775x_scif_regs
end
##}}}

##{{{  SH4202
## SH4202 control registers
define sh4202_si_regs
  sh4200_core_si_regs

  ## Common SH42xx control registers
  sh42xx_cpg2_regs 0xfe0a0000
  sh42xx_dmac_regs 0xffa00000
  sh42xx_intc2_regs 0xfe080000
  sh42xx_scif_regs 0xffe80000

  ## Memory control registers
  sh42xx_emi_regs 0xfec00000
  sh42xx_femi_regs 0xff800000
end
##}}}

##{{{  ST40STB1 (HCMOS7)
## ST40STB1 control registers
define st40stb1_hcmos7_si_regs
  st40100_core_si_regs

  ## System Architecture Volume 1: System
  st40_dmac_regs 0xbb000000
  st40_clockgen_regs 0xbb040000 A

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmi1_regs 0xaf000000
  st40_fmi_hcmos7_regs 0xa7f00000
  st40_pci_hcmos7_regs 0xb7000000

  ## System Architecture Volume 4: I/O Devices
  st40_pio_regs 0xbb010000 1
  st40_pio_regs 0xbb020000 2
  st40_pio_regs 0xbb030000 3
end
##}}}

##{{{  ST40STB1
## ST40STB1 control registers
define st40stb1_si_regs
  st40100_core_si_regs

  ## System Architecture Volume 1: System
  st40_dmac_regs 0xbb000000
  st40_clockgen_regs 0xbb040000 A
  st40_clockgen_regs 0xbb100000 B

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmi2_regs 0xaf000000
  st40_emi_regs 0xa7f00000
  st40_pci_regs 0xb7000000
  st40_empi_regs 0xbb130000
  st40_mpxarb_regs 0xbb138000

  ## System Architecture Volume 4: I/O Devices
  st40_pio_regs 0xbb010000 1
  st40_pio_regs 0xbb020000 2
  st40_pio_regs 0xbb030000 3
  st40_mailbox_regs ST20 ST40 0xbb150000
  st40_sysconf_regs 0xbb190000
end
##}}}

##{{{  ST40RA
## ST40RA control registers
define st40ra_si_regs
  st40stb1_si_regs
end
##}}}

##{{{  ST40GX1
## ST40GX1 control registers
define st40gx1_si_regs
  st40100_core_si_regs

  ## System Architecture Volume 1: System
  st40_dmac_regs 0xbb000000
  st40_clockgen_regs 0xbb040000 A
  st40_clockgen_regs 0xbb100000 B

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmi2_regs 0xaf000000
  st40_emi_regs 0xa7f00000
  st40_pci_regs 0xb7000000
  st40_empi_regs 0xbb130000
  st40_mpxarb_regs 0xbb138000

  ## System Architecture Volume 4: I/O Devices
  st40_pio_regs 0xbb010000 1
  st40_pio_regs 0xbb020000 2
  st40_pio_regs 0xbb030000 3
  st40_mailbox_regs ST20 ST40 0xbb150000
  st40_sysconf_regs 0xbb190000
end
##}}}

##{{{  STm8000
## STm8000 control registers
define stm8000_si_regs
  st40100_core_si_regs

  ## DVD Platform Architecture Volume 1: Cores
  st40_ilc_regs 0xb8300000
  st40_mailbox_regs ST40 ST200 0xb0200000 0
  st40_mailbox_regs ST40 ST200 0xb0210000 1

  ## DVD Platform Architecture Volume 2: System Services
  st40_dmac_regs 0xb8100000
  stm8000_clockgen_regs 0xb0400000 A
  stm8000_fs_regs 0xb0410000 A
  stm8000_fs_regs 0xb0420000 B
  st40_lmi3_regs 0xaf000000
  st40_emi_regs 0xa7f00000
  st40_sysconf_regs 0xb0300000

  ## DVD Platform Architecture Volume 4: I/O Devices
  st40_pio_regs 0xb8320000 0
  st40_pio_regs 0xb8321000 1
  st40_pio_regs 0xb8322000 2
  st40_pio_regs 0xb8323000 3
  st40_pio_regs 0xb8324000 4
  st40_pio_regs 0xb8325000 5
  st40_pio_regs 0xb8326000 6
  st40_pio_regs 0xb8327000 7
  st40_asc_regs 0xb8330000 0
  st40_asc_regs 0xb8331000 1
  st40_asc_regs 0xb8332000 2
  st40_asc_regs 0xb8333000 3
  st40_asc_regs 0xb8334000 4
  st40_ssc_regs 0xb8340000 0
  st40_ssc_regs 0xb8341000 1

  ## DVD Platform Architecture Volume 6: Video/Audio Encoding
  stm8000_lx_glue_regs 0xb4112000
  stm8000_she_regs 0xb4114000
end
##}}}

##{{{  STi5528
## STi5528 control registers
define sti5528_si_regs
  st40100_core_si_regs

  ## STi5528 control registers
  sti5528_sysconf_regs 0xb9162000
  sti5528_clockgen_regs 0xb9163000
  sti5528_stbus_regs 0xb9150000

  ## System Architecture Volume 1: System
  st40_dmac_regs 0xb9161000

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmi2_regs 0xaf000000
  st40_emi_regs 0xba100000
  st40_pci_regs 0xb7000000

  ## System Architecture Volume 4: I/O Devices
  st40_mailbox_regs ST20 ST40 0xb9160000
  st40_pio_regs 0xba020000 0
  st40_pio_regs 0xba021000 1
  st40_pio_regs 0xba022000 2
  st40_pio_regs 0xba023000 3
  st40_pio_regs 0xba024000 4
  st40_pio_regs 0xba025000 5
  st40_pio_regs 0xba026000 6
  st40_pio_regs 0xba027000 7
  st40_asc_regs 0xba030000 0
  st40_asc_regs 0xba031000 1
  st40_asc_regs 0xba032000 2
  st40_asc_regs 0xba033000 3
  st40_asc_regs 0xba034000 4
  st40_ssc_regs 0xba040000 0
  st40_ssc_regs 0xba041000 1
end
##}}}

##{{{  STd1000
## STd1000 control registers
define std1000_si_regs
  st40200_core_si_regs

  ## Common ST40 control registers
  st40_ilc_regs 0xbbe80000

  ## STd1000 control registers
  std1000_genconf_regs 0xbb928000
  std1000_mixer_regs 0xbbb50000
  std1000_clockgen_regs 0xbba00000

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmigp_regs 0xb9000000
  st40_emi_regs 0xba000000

  ## System Architecture Volume 4: I/O Devices
  st40_pio_regs 0xbb934000 2
  st40_pio_regs 0xbb938000 3
  st40_pio_regs 0xbb93c000 4
  st40_pio_regs 0xbb940000 5
  st40_pio_regs 0xbb944000 6
  st40_pio_regs 0xbb948000 7
  st40_pio_regs 0xbb94c000 8
  st40_pio_regs 0xbb950000 9
  st40_pio_regs 0xbb954000 10
  st40_pio_regs 0xbb958000 11
  st40_pio_regs 0xbb95c000 12
end
##}}}

##{{{  STd2000
## STd2000 control registers
define std2000_si_regs
  st40200_core_si_regs

  ## Common ST40 control registers
  st40_ilc_regs 0xb9000000

  ## STd2000 control registers
  std2000_genconf_regs 0xb9ee0000
  std2000_mixer_regs 0xb9400000 1
  std2000_mixer_regs 0xb9420000 2
  std2000_clockgen_regs 0xb8800000

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmi3_regs 0xa8000000 1
  st40_lmi3_regs 0xb7000000 2
  st40_emi_regs 0xa1000000

  ## System Architecture Volume 4: I/O Devices
  st40_pio_regs 0xb9c80000 2
  st40_pio_regs 0xb9ca0000 3
  st40_pio_regs 0xb9cc0000 4
  st40_pio_regs 0xb9ce0000 5
  st40_pio_regs 0xb9d00000 6
  st40_pio_regs 0xb9d20000 7
  st40_pio_regs 0xb9d40000 8
  st40_pio_regs 0xb9d60000 9
  st40_pio_regs 0xb9d80000 10
  st40_pio_regs 0xb9e80000 11
  st40_pio_regs 0xb9ea0000 12
end
##}}}

##{{{  STb7100
## STb7100 control registers
define stb7100_si_regs
  st40200_core_si_regs

  ## Common ST40 control registers
  st40_ilc_regs 0xb8000000
  st40_intc2_regs 0xb9001300

  ## STb710x control registers
  stb710x_sysconf_regs 0xb9001000
  stb710x_clockgena_regs 0xb9213000
  stb710x_stbus_regs 0xb9216000 0xb9216200 0xb920d200 0xb920d000 0xb9243000

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmi3_regs 0xaf000000 SYS
  st40_lmi3_regs 0xb7000000 VID
  st40_emi_regs 0xba100000

  ## System Architecture Volume 4: I/O Devices
  st40_mailbox_regs ST231 ST40 0xb9211000 0
  st40_mailbox_regs ST231 ST40 0xb9212000 1
  st40_pio_regs 0xb8020000 0
  st40_pio_regs 0xb8021000 1
  st40_pio_regs 0xb8022000 2
  st40_pio_regs 0xb8023000 3
  st40_pio_regs 0xb8024000 4
  st40_pio_regs 0xb8025000 5
  st40_asc_regs 0xb8030000 0
  st40_asc_regs 0xb8031000 1
  st40_asc_regs 0xb8032000 2
  st40_asc_regs 0xb8033000 3
  st40_ssc_regs 0xb8040000 0
  st40_ssc_regs 0xb8041000 1
  st40_ssc_regs 0xb8042000 2
end
##}}}

##{{{  STb7109
## STb7109 control registers
define stb7109_si_regs
  init-if-undefined $_stb7109movelmiregs = 1
  keep-variable $_stb7109movelmiregs

  stb7100_si_regs

  if ($_stb7109movelmiregs)
  st40_lmi3_regs 0xba800000 SYS
  st40_lmi3_regs 0xbb000000 VID
  end
end
##}}}

##{{{  STi5202
## STi5202 control registers
define sti5202_si_regs
  init-if-undefined $_sti5202movelmiregs = 1
  keep-variable $_sti5202movelmiregs

  st40200_core_si_regs

  ## Common ST40 control registers
  st40_ilc_regs 0xb8000000
  st40_intc2_regs 0xb9001300

  ## STb710x control registers
  stb710x_sysconf_regs 0xb9001000
  stb710x_clockgena_regs 0xb9213000
  stb710x_stbus_regs 0xb9216000 0xb9216200 0xb920d200 0xb920d000 0xb9243000

  ## System Architecture Volume 2: Bus Interfaces
  if ($_sti5202movelmiregs)
  st40_lmi3_regs 0xba800000
  else
  st40_lmi3_regs 0xaf000000
  end
  st40_emi_regs 0xba100000

  ## System Architecture Volume 4: I/O Devices
  st40_mailbox_regs ST231 ST40 0xb9211000 0
  st40_mailbox_regs ST231 ST40 0xb9212000 1
  st40_pio_regs 0xb8020000 0
  st40_pio_regs 0xb8021000 1
  st40_pio_regs 0xb8022000 2
  st40_pio_regs 0xb8023000 3
  st40_pio_regs 0xb8024000 4
  st40_pio_regs 0xb8025000 5
  st40_asc_regs 0xb8030000 0
  st40_asc_regs 0xb8031000 1
  st40_asc_regs 0xb8032000 2
  st40_asc_regs 0xb8033000 3
  st40_ssc_regs 0xb8040000 0
  st40_ssc_regs 0xb8041000 1
  st40_ssc_regs 0xb8042000 2
end
##}}}

##{{{  STi7200
## STi7200 control registers
define sti7200_si_regs
  if ($argc > 0)
    $arg0_core_si_regs
  else
    st40200_core_si_regs
  end

  ## Common ST40 control registers
  st40_ilc_regs 0xfd804000

  ## STi7200 control registers
  sti7200_sysconf_regs 0xfd704000
  sti7200_clockgena_regs 0xfd700000
  sti7200_clockgenb_regs 0xfd701000

  ## STi7200 contains 2 LMIGPs and one EMI4
  st40_lmigp_regs 0xfdd18000 0
  st40_lmigp_regs 0xfdd19000 1
  st40_emi_regs 0xfdf00000

  ## Peripherals
  st40_mailbox_regs ST231_AUD0 ST40 0xfd800000 0
  st40_mailbox_regs ST231_VID0 ST40 0xfd801000 1
  st40_mailbox_regs ST231_AUD1 ST40 0xfd802000 2
  st40_mailbox_regs ST231_VID1 ST40 0xfd803000 3
  st40_pio_regs 0xfd020000 0
  st40_pio_regs 0xfd021000 1
  st40_pio_regs 0xfd022000 2
  st40_pio_regs 0xfd023000 3
  st40_pio_regs 0xfd024000 4
  st40_pio_regs 0xfd025000 5
  st40_pio_regs 0xfd026000 6
  st40_pio_regs 0xfd027000 7
  st40_asc_regs 0xfd030000 0
  st40_asc_regs 0xfd031000 1
  st40_asc_regs 0xfd032000 2
  st40_asc_regs 0xfd033000 3
  st40_ssc_regs 0xfd040000 0
  st40_ssc_regs 0xfd041000 1
  st40_ssc_regs 0xfd042000 2
  st40_ssc_regs 0xfd043000 3
  st40_ssc_regs 0xfd044000 4
end
##}}}

##{{{  STV0498
## STV0498 control registers
define stv0498_si_regs
  st40400_core_si_regs

  ## Common ST40 control registers
  st40_ilc_regs 0xfd200000
  st40_intc2_regs 0xfd301300

  ## STV0498 control registers
  stv0498_sysconf_regs 0xfd301000
  stv0498_clockgen_regs 0xfd300000

  ## System Architecture Volume 2: Bus Interfaces
  st40_lmi4_regs 0xbb000000
  st40_emi_regs 0xfd100000

  ## System Architecture Volume 4: I/O Devices
  st40_pio_regs 0xfd220000 0
  st40_pio_regs 0xfd221000 1
  st40_pio_regs 0xfd222000 2
  st40_pio_regs 0xfe220000 3
  st40_pio_regs 0xfe221000 4
  st40_asc_regs 0xfd230000 0
  st40_ssc_regs 0xfd240000 0
  st40_ssc_regs 0xfd241000 1
  st40_ssc_regs 0xfd242000 2
  st40_ssc_regs 0xfe240000 3
  st40_ssc_regs 0xfe241000 4
  st40_ssc_regs 0xfe242000 5
end
##}}}
