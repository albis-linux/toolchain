################################################################################

define mediaref
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mediaref.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mediaref_setup $arg1
  else
    connectsh4le $arg0 mediaref_setup "hardreset"
  end
end

document mediaref
Connect to and configure an STMediaRef-Demo board
Usage: mediaref <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mediarefusb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mediaref.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mediaref_setup $arg1
  else
    connectsh4usble $arg0 mediaref_setup "hardreset"
  end
end

document mediarefusb
Connect to and configure an STMediaRef-Demo board
Usage: mediarefusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mediarefsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mediaref.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mediaref_fsim_setup mediarefsim_setup $arg0
  else
    connectsh4simle mediaref_fsim_setup mediarefsim_setup ""
  end
end

document mediarefsim
Connect to and configure a simulated STMediaRef-Demo board
Usage: mediarefsim
end

################################################################################

define mediarefpsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mediaref.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mediaref_psim_setup mediarefsim_setup $arg0
  else
    connectsh4psimle mediaref_psim_setup mediarefsim_setup ""
  end
end

document mediarefpsim
Connect to and configure a simulated STMediaRef-Demo board
Usage: mediarefpsim
end

################################################################################
