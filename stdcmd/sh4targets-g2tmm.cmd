################################################################################

define g2tmm
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source g2tmm.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 g2tmm_setup $arg1
  else
    connectsh4le $arg0 g2tmm_setup "hardreset"
  end
end

document g2tmm
Connect to and configure a TMM Canal+ G2 board
Usage: g2tmm <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define g2tmmusb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source g2tmm.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 g2tmm_setup $arg1
  else
    connectsh4usble $arg0 g2tmm_setup "hardreset"
  end
end

document g2tmmusb
Connect to and configure a TMM Canal+ G2 board
Usage: g2tmm <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define g2tmmsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source g2tmm.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle g2tmm_fsim_setup g2tmmsim_setup $arg0
  else
    connectsh4simle g2tmm_fsim_setup g2tmmsim_setup ""
  end
end

document g2tmmsim
Connect to and configure a simulated TMM Canal+ G2 board
Usage: g2tmmsim
end

################################################################################

define g2tmmpsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source g2tmm.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle g2tmm_psim_setup g2tmmsim_setup $arg0
  else
    connectsh4psimle g2tmm_psim_setup g2tmmsim_setup ""
  end
end

document g2tmmpsim
Connect to and configure a simulated TMM Canal+ G2 board
Usage: g2tmmpsim
end

################################################################################
