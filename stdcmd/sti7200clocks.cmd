##
## Configure and display clocks for STi7200
##

##{{{  sti7200_version
define sti7200_version
  set $_id = *$SYSCONF_DEVICEID
  if (($_id & 0x0fffffff) == 0x0d437041)
    set $arg0 = 0xa0 + ((($_id >> 28) & 0x0f) << 4)
  else
    set $_id = (*$CCN_PVR >> 16) & 0xffff
    if ($_id  == 0x0406)
      set $arg0 = 0xa0
    else
      set $arg0 = 0xa1
    end
  end
end
##}}}

##{{{  sti7200_set_clockgen_a_pll0
define sti7200_set_clockgen_a_pll0
  ## Set CLOCKGENA PLL0 into BYPASS...
  set *$CLOCKGENA_PLL0_CFG = *$CLOCKGENA_PLL0_CFG | 0x00100000

  ## Power down CLOCKGENA PLL0...
  set *$CLOCKGENA_POWER_CFG = *$CLOCKGENA_POWER_CFG | 0x00000001

  ## Configure CLOCKGENA PLL0...
  set *$CLOCKGENA_PLL0_CFG = (*$CLOCKGENA_PLL0_CFG & 0xfff80000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16)

  ## Enable CLOCKGENA PLL0...
  set *$CLOCKGENA_POWER_CFG = *$CLOCKGENA_POWER_CFG & 0xfffffffe

  ## Wait for CLOCKGENA PLL0 to lock...
  while ((*$CLOCKGENA_PLL0_CFG & 0x80000000) == 0)
  end

  ## Clear CLOCKGENA PLL0 from BYPASS...
  set *$CLOCKGENA_PLL0_CFG = *$CLOCKGENA_PLL0_CFG & 0xffefffff
end
##}}}

##{{{  sti7200_set_clockgen_a_pll1
define sti7200_set_clockgen_a_pll1
  ## Set CLOCKGENA PLL1 into BYPASS...
  set *$CLOCKGENA_PLL1_CFG = *$CLOCKGENA_PLL1_CFG | 0x00100000

  ## Power down CLOCKGENA PLL1...
  set *$CLOCKGENA_POWER_CFG = *$CLOCKGENA_POWER_CFG | 0x00000002

  ## Configure CLOCKGENA PLL1...
  set *$CLOCKGENA_PLL1_CFG = (*$CLOCKGENA_PLL1_CFG & 0xfff80000) | ($arg0) | ($arg1 << 8)

  ## Enable CLOCKGENA PLL1...
  set *$CLOCKGENA_POWER_CFG = *$CLOCKGENA_POWER_CFG & 0xfffffffd

  ## Wait for CLOCKGENA PLL1 to lock...
  while ((*$CLOCKGENA_PLL1_CFG & 0x80000000) == 0)
  end

  ## Clear CLOCKGENA PLL1 from BYPASS...
  set *$CLOCKGENA_PLL1_CFG = *$CLOCKGENA_PLL1_CFG & 0xffefffff
end
##}}}

##{{{  sti7200_set_clockgen_a_pll2
define sti7200_set_clockgen_a_pll2
  ## Set CLOCKGENA PLL2 into BYPASS...
  set *$CLOCKGENA_PLL2_CFG = *$CLOCKGENA_PLL2_CFG | 0x00100000

  ## Power down CLOCKGENA PLL2...
  set *$CLOCKGENA_POWER_CFG = *$CLOCKGENA_POWER_CFG | 0x00000004

  ## Configure CLOCKGENA PLL2...
  set *$CLOCKGENA_PLL2_CFG = (*$CLOCKGENA_PLL2_CFG & 0xfff80000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16)

  ## Enable CLOCKGENA PLL2...
  set *$CLOCKGENA_POWER_CFG = *$CLOCKGENA_POWER_CFG & 0xfffffffb

  ## Wait for CLOCKGENA PLL2 to lock...
  while ((*$CLOCKGENA_PLL2_CFG & 0x80000000) == 0)
  end

  ## Clear CLOCKGENA PLL2 from BYPASS...
  set *$CLOCKGENA_PLL2_CFG = *$CLOCKGENA_PLL2_CFG & 0xffefffff
end
##}}}

##{{{  sti7200_set_clockgen_b_pll0
define sti7200_set_clockgen_b_pll0
  ## Set CLOCKGENB PLL0 into BYPASS...
  set *$CLOCKGENB_PLL0_CFG = *$CLOCKGENB_PLL0_CFG | 0x00100000

  ## Power down CLOCKGENB PLL0...
  set *$CLOCKGENB_POWER_CFG = *$CLOCKGENB_POWER_CFG | 0x00008000

  ## Configure CLOCKGENB PLL0...
  set *$CLOCKGENB_PLL0_CFG = (*$CLOCKGENB_PLL0_CFG & 0xfff80000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16)

  ## Enable CLOCKGENB PLL0...
  set *$CLOCKGENB_POWER_CFG = *$CLOCKGENB_POWER_CFG & 0xffff7fff

  ## Wait for CLOCKGENB PLL0 to lock...
  while ((*$CLOCKGENB_PLL0_CFG & 0x80000000) == 0)
  end

  ## Clear CLOCKGENB PLL0 from BYPASS...
  set *$CLOCKGENB_PLL0_CFG = *$CLOCKGENB_PLL0_CFG & 0xffefffff
end
##}}}

##{{{  sti7200_set_clockgen_lmi_pll
define sti7200_set_clockgen_lmi_pll_cut1
  ## Power down PLL...
  set *$SYSCONF_SYS_CFG11 = *$SYSCONF_SYS_CFG11 | 0x00001000

  ## Configure PLL...
  set *$SYSCONF_SYS_CFG11 = (*$SYSCONF_SYS_CFG11 & 0xfffff001) | (($arg0) << 9) | ($arg1 << 1)

  ## Enable CLOCKGENA PLL1...
  set *$SYSCONF_SYS_CFG11 = *$SYSCONF_SYS_CFG11 & 0xffffefff

  ## Wait for CLOCKGENA PLL1 to lock... (polarity inverted on lock bit)
  while ((*$SYSCONF_SYS_STA03 & 0x00000001) == 1)
  end
end

define sti7200_set_clockgen_lmi_pll_cut2
  ## Power down PLL...
  set *$SYSCONF_SYS_CFG11 = *$SYSCONF_SYS_CFG11 | 0x00001000

  ## Configure PLL...
  set *$SYSCONF_SYS_CFG11 = (*$SYSCONF_SYS_CFG11 & 0xfffff001) | (($arg0) << 9) | ($arg1 << 1)

  ## Enable CLOCKGENA PLL1...
  set *$SYSCONF_SYS_CFG11 = *$SYSCONF_SYS_CFG11 & 0xffffefff

  ## Wait for CLOCKGENA PLL1 to lock...
  while ((*$SYSCONF_SYS_STA03 & 0x00000001) == 0)
  end
end
##}}}

##{{{  sti7200_displayclocks
define sti7200_get_clockgen_x_pll800_x_frq
  set $_data = *$CLOCKGEN$arg0_PLL$arg1_CFG

  set $_mdiv = (double) ($_data & 0xff)
  set $_ndiv = (double) (($_data >> 8) & 0xff)
  set $_pdiv = (double) (1 << (($_data >> 16) & 0x7))

  set $arg3 = ((2.0 * $arg2 * $_ndiv) / $_mdiv) / $_pdiv
end

define sti7200_get_clockgen_x_pll1600_x_frq
  set $_data = *$CLOCKGEN$arg0_PLL$arg1_CFG

  set $_rdiv = (double) ($_data & 0x7)
  set $_ddiv = (double) (($_data >> 8) & 0xff)

  set $arg3 = ($arg2 * $_ddiv) / $_rdiv
end

define sti7200_get_clockgen_lmi_pll1600_frq
  set $_data = *$SYSCONF_SYS_CFG11

  set $_rdiv = (double) (($_data >> 9) & 0x7)
  set $_ddiv = (double) (($_data >> 1) & 0xff)

  set $arg1 = ($arg0 * $_ddiv) / $_rdiv
end

define sti7200_displayclocks
  if ($argc > 0)
    set $_extfrq = $arg0
  else
    set $_extfrq = 30.0
  end

  sti7200_version $_ver

  if ($_ver == 0xa0)
    sti7200_get_clockgen_x_pll800_x_frq A 0 $_extfrq $_pll0frq
  else
    sti7200_get_clockgen_x_pll1600_x_frq A 0 $_extfrq $_pll0frq
    ## NOTE: FVCO used (2 * PLL clock)
    set $_pll0frq = $_pll0frq * 2
  end
  sti7200_get_clockgen_x_pll1600_x_frq A 1 $_extfrq $_pll1frq
  sti7200_get_clockgen_x_pll800_x_frq A 2 $_extfrq $_pll2frq
  sti7200_get_clockgen_x_pll800_x_frq B 0 $_extfrq $_pll0bfrq
  sti7200_get_clockgen_lmi_pll1600_frq $_extfrq $_lmifrq

  set $_data = (*$CLOCKGENA_DIV_CFG >> 20) & 0x3

  if ($_data == 0x0)
    set $_mainfrq = 0
  end
  if ($_data == 0x1)
    set $_mainfrq = $_pll0frq
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_mainfrq = $_pll0frq / 2.0
  end

  if ($_ver == 0xa0)
    set $_data = (*$CLOCKGENA_DIV_CFG >> 1) & 0x7

    if ($_data == 0x0)
      set $_st40cpufrq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_st40cpufrq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_st40cpufrq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_st40cpufrq = $_mainfrq / 4.0
    end
    if ($_data == 0x4)
      set $_st40cpufrq = $_mainfrq / 6.0
    end
    if ($_data == 0x5)
      set $_st40cpufrq = $_mainfrq / 8.0
    end
    if ($_data == 0x6)
      set $_st40cpufrq = $_mainfrq / 1024.0
    end
    if ($_data == 0x7)
      set $_st40cpufrq = $_mainfrq
    end

    set $_data = (*$CLOCKGENA_DIV_CFG >> 4) & 0x7

    if ($_data == 0x0)
      set $_st40busfrq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_st40busfrq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_st40busfrq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_st40busfrq = $_mainfrq / 4.0
    end
    if ($_data == 0x4)
      set $_st40busfrq = $_mainfrq / 6.0
    end
    if ($_data == 0x5)
      set $_st40busfrq = $_mainfrq / 8.0
    end
    if ($_data == 0x6)
      set $_st40busfrq = $_mainfrq / 1024.0
    end
    if ($_data == 0x7)
      set $_st40busfrq = $_mainfrq
    end

    set $_data = (*$CLOCKGENA_DIV_CFG >> 7) & 0x7

    if ($_data == 0x0)
      set $_st40perfrq = $_mainfrq
    end
    if ($_data == 0x1)
      set $_st40perfrq = $_mainfrq / 2.0
    end
    if ($_data == 0x2)
      set $_st40perfrq = $_mainfrq / 3.0
    end
    if ($_data == 0x3)
      set $_st40perfrq = $_mainfrq / 4.0
    end
    if ($_data == 0x4)
      set $_st40perfrq = $_mainfrq / 6.0
    end
    if ($_data == 0x5)
      set $_st40perfrq = $_mainfrq / 8.0
    end
    if ($_data == 0x6)
      set $_st40perfrq = $_mainfrq / 1024.0
    end
    if ($_data == 0x7)
      set $_st40perfrq = $_mainfrq
    end

    set $_data = (*$CLOCKGENA_MUX_CFG >> 7) & 0x3

    if ($_data == 0x0)
      set $_slim0_1srcfrq = $_st40cpufrq
    end
    if ($_data == 0x1)
      set $_slim0_1srcfrq = $_pll1frq
    end
  else
    set $_data = (*$CLOCKGENA_DIV_CFG >> 1) & 0x7

    if ($_data == 0x0)
      set $_st40cpufrq = 0
    end
    if ($_data == 0x1)
      set $_st40cpufrq = $_mainfrq
    end
    if ($_data == 0x2)
      set $_st40cpufrq = $_mainfrq / 2.0
    end
    if ($_data == 0x3)
      set $_st40cpufrq = $_mainfrq / 1024.0
    end
    if (($_data == 0x4) || ($_data == 0x5) ||($_data == 0x6) ||($_data == 0x7))
      set $_st40cpufrq = $_mainfrq / 3.0
    end

    set $_data = (*$CLOCKGENA_DIV_CFG >> 4) & 0x7

    if ($_data == 0x0)
      set $_slim0_1srcfrq = 0
    end
    if ($_data == 0x1)
      set $_slim0_1srcfrq = $_mainfrq
    end
    if ($_data == 0x2)
      set $_slim0_1srcfrq = $_mainfrq / 2.0
    end
    if ($_data == 0x3)
      set $_slim0_1srcfrq = $_mainfrq / 1024.0
    end
    if (($_data == 0x4) || ($_data == 0x5) ||($_data == 0x6) ||($_data == 0x7))
      set $_slim0_1srcfrq = $_mainfrq / 3.0
    end

    set $_data = (*$CLOCKGENA_MUX_CFG >> 7) & 0x3

    if ($_data == 0x1)
      set $_slim0_1srcfrq = $_pll1frq
    end
  end

  set $_data = (*$CLOCKGENA_DIV_CFG >> 10) & 0x3

  if ($_data == 0x0)
    set $_slim0_1frq = 0
  end
  if ($_data == 0x1)
    set $_slim0_1frq = $_slim0_1srcfrq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_slim0_1frq = $_slim0_1srcfrq
  end

  set $_data = (*$CLOCKGENA_DIV_CFG >> 12) & 0x3

  if ($_data == 0x0)
    set $_st231aud0frq = 0
  end
  if ($_data == 0x1)
    set $_st231aud0frq = $_pll1frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_st231aud0frq = $_pll1frq
  end

  set $_data = (*$CLOCKGENA_DIV_CFG >> 14) & 0x3

  if ($_data == 0x0)
    set $_st231aud1frq = 0
  end
  if ($_data == 0x1)
    set $_st231aud1frq = $_pll1frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_st231aud1frq = $_pll1frq
  end

  set $_data = (*$CLOCKGENA_DIV_CFG >> 16) & 0x3

  if ($_data == 0x0)
    set $_st231vid0frq = 0
  end
  if ($_data == 0x1)
    set $_st231vid0frq = $_pll1frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_st231vid0frq = $_pll1frq
  end

  set $_data = (*$CLOCKGENA_DIV_CFG >> 18) & 0x3

  if ($_data == 0x0)
    set $_st231vid1frq = 0
  end
  if ($_data == 0x1)
    set $_st231vid1frq = $_pll1frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_st231vid1frq = $_pll1frq
  end

  set $_data = (*$CLOCKGENA_DIV2_CFG >> 5) & 0x1

  if ($_data == 0x0)
    set $_ic_266frq = _pll2frq / 1024.0
  end
  if ($_data == 0x1)
    set $_ic_266frq = $_pll2frq / 3.0
  end

  set $_data = (*$CLOCKGENB_DIV2_CFG >> 16) & 0x3

  if ($_data == 0x0)
    set $_bdisp_266frq = 0
  end
  if ($_data == 0x1)
    set $_bdisp_266frq = $_pll0bfrq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_bdisp_266frq = $_pll0bfrq / 3.0
  end

  set $_data = (*$CLOCKGENA_DIV2_CFG >> 18) & 0x3

  if ($_data == 0x0)
    set $_dmu0_266frq = 0
  end
  if ($_data == 0x1)
    set $_dmu0_266frq = $_pll2frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_dmu0_266frq = $_pll2frq / 3.0
  end

  set $_data = (*$CLOCKGENA_DIV2_CFG >> 20) & 0x3

  if ($_data == 0x0)
    set $_dmu1_266frq = 0
  end
  if ($_data == 0x1)
    set $_dmu1_266frq = $_pll2frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_dmu1_266frq = $_pll2frq / 3.0
  end

  set $_data = (*$CLOCKGENB_DIV2_CFG >> 20) & 0x3

  if ($_data == 0x0)
    set $_slim_tsg_266frq = 0
  end
  if ($_data == 0x1)
    set $_slim_tsg_266frq = $_pll0bfrq / 1024.0
  end
  if ($_data == 0x2)
    set $_slim_tsg_266frq = $_pll0bfrq / 2.0
  end
  if ($_data == 0x3)
    set $_slim_tsg_266frq = $_pll0bfrq / 3.0
  end

  set $_data = (*$CLOCKGENA_DIV2_CFG >> 22) & 0x3

  if ($_data == 0x0)
    set $_disp_266frq = 0
  end
  if ($_data == 0x1)
    set $_disp_266frq = $_pll2frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_disp_266frq = $_pll2frq / 3.0
  end

  set $_data = (*$CLOCKGENA_MUX_CFG >> 9) & 0x1

  if ($_data == 0x0)
    set $_ic_regfrq = $_pll0bfrq / 8.0
  end
  if ($_data == 0x1)
    set $_ic_regfrq = $_pll0bfrq / 6.0
  end

  set $_data = (*$CLOCKGENA_DIV2_CFG >> 6) & 0x3

  if ($_data == 0x0)
    set $_bdisp_200frq = 0
  end
  if ($_data == 0x1)
    set $_bdisp_200frq = $_pll2frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_bdisp_200frq = $_pll2frq / 4.0
  end

  set $_data = (*$CLOCKGENB_DIV2_CFG >> 8) & 0x3

  if ($_data == 0x0)
    set $_compo_200frq = 0
  end
  if ($_data == 0x1)
    set $_compo_200frq = $_pll0bfrq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_compo_200frq = $_pll0bfrq / 4.0
  end

  set $_data = (*$CLOCKGENB_DIV2_CFG >> 10) & 0x3

  if ($_data == 0x0)
    set $_disp_200frq = 0
  end
  if ($_data == 0x1)
    set $_disp_200frq = $_pll0bfrq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_disp_200frq = $_pll0bfrq / 4.0
  end

  set $_data = (*$CLOCKGENB_DIV2_CFG >> 12) & 0x3

  if ($_data == 0x0)
    set $_vdp_200frq = 0
  end
  if ($_data == 0x1)
    set $_vdp_200frq = $_pll0bfrq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_vdp_200frq = $_pll0bfrq / 4.0
  end

  set $_data = (*$CLOCKGENA_DIV2_CFG >> 14) & 0x3

  if ($_data == 0x0)
    set $_fdma_200frq = 0
  end
  if ($_data == 0x1)
    set $_fdma_200frq = $_pll2frq / 1024.0
  end
  if (($_data == 0x2) || ($_data == 0x3))
    set $_fdma_200frq = $_pll2frq / 4.0
  end

  set $_emi_masterfrq = $_pll0bfrq / 8.0

  set $_ethernetfrq = $_pll0bfrq / 8.0

  printf "Clock frequencies\n"
  printf "\n"
  printf "  CKGAPLL0        = %.1f MHz\n", $_pll0frq
  printf "  CKGAPLL1        = %.1f MHz\n", $_pll1frq
  printf "  CKGAPLL2        = %.1f MHz\n", $_pll2frq
  printf "  CKGBPLL0        = %.1f MHz\n", $_pll0bfrq
  printf "  ST40 CPU        = %.1f MHz\n", $_st40cpufrq
  if ($_ver == 0xa0)
    printf "  ST40 BUS        = %.1f MHz\n", $_st40busfrq
    printf "  ST40 PER        = %.1f MHz\n", $_st40perfrq
  end
  printf "  ST231 VID0 CPU  = %.1f MHz\n", $_st231vid0frq
  printf "  ST231 VID1 CPU  = %.1f MHz\n", $_st231vid1frq
  printf "  ST231 AUD0 CPU  = %.1f MHz\n", $_st231aud0frq
  printf "  ST231 AUD1 CPU  = %.1f MHz\n", $_st231aud1frq
  printf "  SLIM0_1FDMA CPU = %.1f MHz\n", $_slim0_1frq
  printf "  SLIM_TSGDMA CPU = %.1f MHz\n", $_slim_tsg_266frq
  printf "  XP70 CPU        = %.1f MHz\n", $_disp_266frq
  printf "  IC_266          = %.1f MHz\n", $_ic_266frq
  printf "  IC_200 (BDISP)  = %.1f MHz\n", $_bdisp_200frq
  printf "  IC_REG          = %.1f MHz\n", $_ic_regfrq
  printf "  LMI2X           = %.1f MHz\n", $_lmifrq
  printf "  EMI             = %.1f MHz\n", $_emi_masterfrq
  printf "  ETHERNET        = %.1f MHz\n", $_ethernetfrq
end

document sti7200_displayclocks
Display the STi7200 clock frequencies
Usage: sti7200_displayclocks [<frequency>]
where <frequency> is optional and is the external clock frequency (default: 30MHz)
end
##}}}
