################################################################################

source sh4targets-mb442stb7100.cmd

################################################################################

define mb442stb7100bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100bypass $arg0 $arg1
  else
    mb442stb7100bypass $arg0
  end
end

define mb442stb7100stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100stmmx $arg0 $arg1
  else
    mb442stb7100stmmx $arg0
  end
end

################################################################################

define mb442bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442bypass $arg0 $arg1
  else
    mb442bypass $arg0
  end
end

define mb442stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stmmx $arg0 $arg1
  else
    mb442stmmx $arg0
  end
end

################################################################################

define stb7100refbypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refbypass $arg0 $arg1
  else
    stb7100refbypass $arg0
  end
end

define stb7100refstmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refstmmx $arg0 $arg1
  else
    stb7100refstmmx $arg0
  end
end

################################################################################

define mb442stb7100bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100bypass $arg0 $arg1
  else
    mb442stb7100bypass $arg0
  end
end

define mb442stb7100stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100stmmx $arg0 $arg1
  else
    mb442stb7100stmmx $arg0
  end
end

################################################################################

define mb442bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442bypass $arg0 $arg1
  else
    mb442bypass $arg0
  end
end

define mb442stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stmmx $arg0 $arg1
  else
    mb442stmmx $arg0
  end
end

################################################################################

define stb7100refbypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refbypass $arg0 $arg1
  else
    stb7100refbypass $arg0
  end
end

define stb7100refstmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refstmmx $arg0 $arg1
  else
    stb7100refstmmx $arg0
  end
end

################################################################################

define mb442stb7100bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100bypass $arg0 $arg1
  else
    mb442stb7100bypass $arg0
  end
end

define mb442stb7100stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100stmmx $arg0 $arg1
  else
    mb442stb7100stmmx $arg0
  end
end

################################################################################

define mb442bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442bypass $arg0 $arg1
  else
    mb442bypass $arg0
  end
end

define mb442stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stmmx $arg0 $arg1
  else
    mb442stmmx $arg0
  end
end

################################################################################

define stb7100refbypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refbypass $arg0 $arg1
  else
    stb7100refbypass $arg0
  end
end

define stb7100refstmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refstmmx $arg0 $arg1
  else
    stb7100refstmmx $arg0
  end
end

################################################################################

source sh4targets-mb442stb7100cutX.cmd

################################################################################

define mb442stb7100cut11bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXbypass 11 $arg0 $arg1
  else
    mb442stb7100cutXbypass 11 $arg0
  end
end

define mb442stb7100cut11stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 11 $arg0
  end
end

define mb442stb7100cut13bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXbypass 13 $arg0 $arg1
  else
    mb442stb7100cutXbypass 13 $arg0
  end
end

define mb442stb7100cut13stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 13 $arg0
  end
end

define mb442stb7100cut20bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXbypass 20 $arg0 $arg1
  else
    mb442stb7100cutXbypass 20 $arg0
  end
end

define mb442stb7100cut20stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 20 $arg0
  end
end

define mb442stb7100cut31bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb442cut11bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXbypass 11 $arg0 $arg1
  else
    mb442cutXbypass 11 $arg0
  end
end

define mb442cut11stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXstmmx 11 $arg0 $arg1
  else
    mb442cutXstmmx 11 $arg0
  end
end

define mb442cut13bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXbypass 13 $arg0 $arg1
  else
    mb442cutXbypass 13 $arg0
  end
end

define mb442cut13stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXstmmx 13 $arg0 $arg1
  else
    mb442cutXstmmx 13 $arg0
  end
end

define mb442cut20bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXbypass 20 $arg0 $arg1
  else
    mb442cutXbypass 20 $arg0
  end
end

define mb442cut20stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXstmmx 20 $arg0 $arg1
  else
    mb442cutXstmmx 20 $arg0
  end
end

define mb442cut31bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXbypass 31 $arg0 $arg1
  else
    mb442cutXbypass 31 $arg0
  end
end

define mb442cut31stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    mb442cutXstmmx 31 $arg0 $arg1
  else
    mb442cutXstmmx 31 $arg0
  end
end

################################################################################

define stb7100refcut11bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXbypass 11 $arg0 $arg1
  else
    stb7100refcutXbypass 11 $arg0
  end
end

define stb7100refcut11stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXstmmx 11 $arg0 $arg1
  else
    stb7100refcutXstmmx 11 $arg0
  end
end

define stb7100refcut13bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXbypass 13 $arg0 $arg1
  else
    stb7100refcutXbypass 13 $arg0
  end
end

define stb7100refcut13stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXstmmx 13 $arg0 $arg1
  else
    stb7100refcutXstmmx 13 $arg0
  end
end

define stb7100refcut20bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXbypass 20 $arg0 $arg1
  else
    stb7100refcutXbypass 20 $arg0
  end
end

define stb7100refcut20stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXstmmx 20 $arg0 $arg1
  else
    stb7100refcutXstmmx 20 $arg0
  end
end

define stb7100refcut31bypass128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXbypass 31 $arg0 $arg1
  else
    stb7100refcutXbypass 31 $arg0
  end
end

define stb7100refcut31stmmx128Mb
  set $_mb442stb7100sys128 = 1
  if ($argc > 1)
    stb7100refcutXstmmx 31 $arg0 $arg1
  else
    stb7100refcutXstmmx 31 $arg0
  end
end

################################################################################

define mb442stb7100cut11bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 11 $arg0 $arg1
  else
    mb442stb7100cutXbypass 11 $arg0
  end
end

define mb442stb7100cut11stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 11 $arg0
  end
end

define mb442stb7100cut13bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 13 $arg0 $arg1
  else
    mb442stb7100cutXbypass 13 $arg0
  end
end

define mb442stb7100cut13stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 13 $arg0
  end
end

define mb442stb7100cut20bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 20 $arg0 $arg1
  else
    mb442stb7100cutXbypass 20 $arg0
  end
end

define mb442stb7100cut20stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 20 $arg0
  end
end

define mb442stb7100cut31bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb442cut11bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 11 $arg0 $arg1
  else
    mb442cutXbypass 11 $arg0
  end
end

define mb442cut11stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 11 $arg0 $arg1
  else
    mb442cutXstmmx 11 $arg0
  end
end

define mb442cut13bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 13 $arg0 $arg1
  else
    mb442cutXbypass 13 $arg0
  end
end

define mb442cut13stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 13 $arg0 $arg1
  else
    mb442cutXstmmx 13 $arg0
  end
end

define mb442cut20bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 20 $arg0 $arg1
  else
    mb442cutXbypass 20 $arg0
  end
end

define mb442cut20stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 20 $arg0 $arg1
  else
    mb442cutXstmmx 20 $arg0
  end
end

define mb442cut31bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 31 $arg0 $arg1
  else
    mb442cutXbypass 31 $arg0
  end
end

define mb442cut31stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 31 $arg0 $arg1
  else
    mb442cutXstmmx 31 $arg0
  end
end

################################################################################

define stb7100refcut11bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 11 $arg0 $arg1
  else
    stb7100refcutXbypass 11 $arg0
  end
end

define stb7100refcut11stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 11 $arg0 $arg1
  else
    stb7100refcutXstmmx 11 $arg0
  end
end

define stb7100refcut13bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 13 $arg0 $arg1
  else
    stb7100refcutXbypass 13 $arg0
  end
end

define stb7100refcut13stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 13 $arg0 $arg1
  else
    stb7100refcutXstmmx 13 $arg0
  end
end

define stb7100refcut20bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 20 $arg0 $arg1
  else
    stb7100refcutXbypass 20 $arg0
  end
end

define stb7100refcut20stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 20 $arg0 $arg1
  else
    stb7100refcutXstmmx 20 $arg0
  end
end

define stb7100refcut31bypass128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 31 $arg0 $arg1
  else
    stb7100refcutXbypass 31 $arg0
  end
end

define stb7100refcut31stmmx128Mb27MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 31 $arg0 $arg1
  else
    stb7100refcutXstmmx 31 $arg0
  end
end

################################################################################

define mb442stb7100cut11bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 11 $arg0 $arg1
  else
    mb442stb7100cutXbypass 11 $arg0
  end
end

define mb442stb7100cut11stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 11 $arg0
  end
end

define mb442stb7100cut13bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 13 $arg0 $arg1
  else
    mb442stb7100cutXbypass 13 $arg0
  end
end

define mb442stb7100cut13stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 13 $arg0
  end
end

define mb442stb7100cut20bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 20 $arg0 $arg1
  else
    mb442stb7100cutXbypass 20 $arg0
  end
end

define mb442stb7100cut20stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 20 $arg0
  end
end

define mb442stb7100cut31bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb442cut11bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 11 $arg0 $arg1
  else
    mb442cutXbypass 11 $arg0
  end
end

define mb442cut11stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 11 $arg0 $arg1
  else
    mb442cutXstmmx 11 $arg0
  end
end

define mb442cut13bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 13 $arg0 $arg1
  else
    mb442cutXbypass 13 $arg0
  end
end

define mb442cut13stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 13 $arg0 $arg1
  else
    mb442cutXstmmx 13 $arg0
  end
end

define mb442cut20bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 20 $arg0 $arg1
  else
    mb442cutXbypass 20 $arg0
  end
end

define mb442cut20stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 20 $arg0 $arg1
  else
    mb442cutXstmmx 20 $arg0
  end
end

define mb442cut31bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 31 $arg0 $arg1
  else
    mb442cutXbypass 31 $arg0
  end
end

define mb442cut31stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 31 $arg0 $arg1
  else
    mb442cutXstmmx 31 $arg0
  end
end

################################################################################

define stb7100refcut11bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 11 $arg0 $arg1
  else
    stb7100refcutXbypass 11 $arg0
  end
end

define stb7100refcut11stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 11 $arg0 $arg1
  else
    stb7100refcutXstmmx 11 $arg0
  end
end

define stb7100refcut13bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 13 $arg0 $arg1
  else
    stb7100refcutXbypass 13 $arg0
  end
end

define stb7100refcut13stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 13 $arg0 $arg1
  else
    stb7100refcutXstmmx 13 $arg0
  end
end

define stb7100refcut20bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 20 $arg0 $arg1
  else
    stb7100refcutXbypass 20 $arg0
  end
end

define stb7100refcut20stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 20 $arg0 $arg1
  else
    stb7100refcutXstmmx 20 $arg0
  end
end

define stb7100refcut31bypass128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 31 $arg0 $arg1
  else
    stb7100refcutXbypass 31 $arg0
  end
end

define stb7100refcut31stmmx128Mb30MHz
  set $_mb442stb7100sys128 = 1
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 31 $arg0 $arg1
  else
    stb7100refcutXstmmx 31 $arg0
  end
end

################################################################################
