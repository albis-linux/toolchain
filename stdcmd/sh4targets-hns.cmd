################################################################################

define hns
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source hns.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 hns_setup $arg1
  else
    connectsh4le $arg0 hns_setup "hardreset"
  end
end

document hns
Connect to and configure an HNS board
Usage: hns <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define hnsusb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source hns.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 hns_setup $arg1
  else
    connectsh4usble $arg0 hns_setup "hardreset"
  end
end

document hnsusb
Connect to and configure an HNS board
Usage: hnsusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define hnssim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source hns.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hns_fsim_setup hnssim_setup $arg0
  else
    connectsh4simle hns_fsim_setup hnssim_setup ""
  end
end

document hnssim
Connect to and configure a simulated HNS board
Usage: hnssim
end

################################################################################

define hnspsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source hns.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hns_psim_setup hnssim_setup $arg0
  else
    connectsh4psimle hns_psim_setup hnssim_setup ""
  end
end

document hnspsim
Connect to and configure a simulated HNS board
Usage: hnspsim
end

################################################################################
