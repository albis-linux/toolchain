################################################################################

define mb671
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb671_setup $arg1
  else
    connectsh4le $arg0 mb671_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671
Connect to and configure an STi7200-Mboard board
Usage: mb671 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671se
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb671se_setup $arg1
  else
    connectsh4le $arg0 mb671se_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671se
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb671se <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671seuc
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb671seuc_setup $arg1
  else
    connectsh4le $arg0 mb671seuc_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671seuc
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671seuc <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671se29
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb671se29_setup $arg1
  else
    connectsh4le $arg0 mb671se29_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671se29
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671se29 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671bypass
  if ($argc > 1)
    mb671 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671bypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40
Usage: mb671bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671sebypass
  if ($argc > 1)
    mb671se $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671se $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671sebypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode)
Usage: mb671sebypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671seucbypass
  if ($argc > 1)
    mb671seuc $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671seuc $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671seucbypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671seucbypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671se29bypass
  if ($argc > 1)
    mb671se29 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671se29 $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671se29bypass
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671se29bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671stmmx
  if ($argc > 1)
    mb671 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671stmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux
Usage: mb671stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671sestmmx
  if ($argc > 1)
    mb671se $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671se $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671sestmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode)
Usage: mb671sestmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671seucstmmx
  if ($argc > 1)
    mb671seuc $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671seuc $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671seucstmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671seucstmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb671se29stmmx
  if ($argc > 1)
    mb671se29 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671se29 $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671se29stmmx
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671se29stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb671usb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb671_setup $arg1
  else
    connectsh4usble $arg0 mb671_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671usb
Connect to and configure an STi7200-Mboard board
Usage: mb671usb <target>
where <target> is an ST Micro Connect USB name
end

define mb671seusb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb671se_setup $arg1
  else
    connectsh4usble $arg0 mb671se_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671seusb
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb671seusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671seucusb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb671seuc_setup $arg1
  else
    connectsh4usble $arg0 mb671seuc_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671seucusb
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671seucusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671se29usb
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200jtag.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb671se29_setup $arg1
  else
    connectsh4usble $arg0 mb671se29_setup "jtagpinout=st40 linkspeed=1.25MHz hardreset"
  end
end

document mb671se29usb
Connect to and configure an STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671se29usb <target>
where <target> is an ST Micro Connect USB name
end

define mb671bypassusb
  if ($argc > 1)
    mb671usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671bypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40
Usage: mb671bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671sebypassusb
  if ($argc > 1)
    mb671seusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671seusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671sebypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode)
Usage: mb671sebypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671seucbypassusb
  if ($argc > 1)
    mb671seucusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671seucusb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671seucbypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671seucbypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671se29bypassusb
  if ($argc > 1)
    mb671se29usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand $arg1"
  else
    mb671se29usb $arg0 "jtagpinout=st40 jtagreset linkspeed=1.25MHz -inicommand mb671bypass_setup"
  end
end

document mb671se29bypassusb
Connect to and configure an STi7200-Mboard board bypassing to the ST40 (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671se29bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671stmmxusb
  if ($argc > 1)
    mb671usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671stmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux
Usage: mb671stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671sestmmxusb
  if ($argc > 1)
    mb671seusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671seusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671sestmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode)
Usage: mb671sestmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671seucstmmxusb
  if ($argc > 1)
    mb671seucusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671seucusb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671seucstmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671seucstmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb671se29stmmxusb
  if ($argc > 1)
    mb671se29usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand $arg1"
  else
    mb671se29usb $arg0 "jtagpinout=stmmx jtagreset linkspeed=1.25MHz tdidelay=1 -inicommand mb671stmmx_setup"
  end
end

document mb671se29stmmxusb
Connect to and configure an STi7200-Mboard board via an ST MultiCore/Mux (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671se29stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb671sim
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb671_fsim_setup mb671sim_setup $arg0
  else
    connectsh4simle mb671_fsim_setup mb671sim_setup ""
  end
end

document mb671sim
Connect to and configure a simulated STi7200-Mboard board
Usage: mb671sim
end

define mb671simse
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb671se_fsim_setup mb671simse_setup $arg0
  else
    connectsh4simle mb671se_fsim_setup mb671simse_setup ""
  end
end

document mb671simse
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb671simse
end

define mb671simseuc
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb671se_fsim_setup mb671simseuc_setup $arg0
  else
    connectsh4simle mb671se_fsim_setup mb671simseuc_setup ""
  end
end

document mb671simseuc
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671simseuc
end

define mb671simse29
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb671se_fsim_setup mb671simse29_setup $arg0
  else
    connectsh4simle mb671se_fsim_setup mb671simse29_setup ""
  end
end

document mb671simse29
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671simse29
end

################################################################################

define mb671psim
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb671_psim_setup mb671sim_setup $arg0
  else
    connectsh4psimle mb671_psim_setup mb671sim_setup ""
  end
end

document mb671psim
Connect to and configure a simulated STi7200-Mboard board
Usage: mb671psim
end

define mb671psimse
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb671se_psim_setup mb671simse_setup $arg0
  else
    connectsh4psimle mb671se_psim_setup mb671simse_setup ""
  end
end

document mb671psimse
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode)
Usage: mb671psimse
end

define mb671psimseuc
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb671se_psim_setup mb671simseuc_setup $arg0
  else
    connectsh4psimle mb671se_psim_setup mb671simseuc_setup ""
  end
end

document mb671psimseuc
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with uncached mappings)
Usage: mb671psimseuc
end

define mb671psimse29
  source register40.cmd
  source display40.cmd
  source sti7200boot.cmd
  source sti7200clocks.cmd
  source sti7200.cmd
  source mb671.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb671se_psim_setup mb671simse29_setup $arg0
  else
    connectsh4psimle mb671se_psim_setup mb671simse29_setup ""
  end
end

document mb671psimse29
Connect to and configure a simulated STi7200-Mboard board (STi7200 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb671psimse29
end

################################################################################
