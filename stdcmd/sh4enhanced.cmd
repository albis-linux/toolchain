################################################################################

set $PMB_ADDR_ARRAY = (unsigned int *) 0xf6100000
set $PMB_DATA_ARRAY = (unsigned int *) 0xf7100000

################################################################################

define sh4_set_pmb
  set $_pmbdata = 0x00000000

  # PMB[n].SZ
  if ($arg3 == 16)
    set $_pmbdata |= 0x00000000
  else
  if ($arg3 == 64)
    set $_pmbdata |= 0x00000010
  else
  if ($arg3 == 128)
    set $_pmbdata |= 0x00000080
  else
  if ($arg3 == 512)
    set $_pmbdata |= 0x00000090
  else
    ## Unsupported page size
  end
  end
  end
  end

  # PMB[n].C (default is 1)
  if ($argc > 4)
    set $_pmbdata |= (($arg4 & 0x1) << 3)
  else
    set $_pmbdata |= 0x00000008
  end

  # PMB[n].WT (default is 0)
  if ($argc > 5)
    set $_pmbdata |= (($arg5 & 0x1) << 0)
  end

  # PMB[n].UB (default is 0)
  if ($argc > 6)
    set $_pmbdata |= (($arg6 & 0x1) << 9)
  end

  set $PMB_ADDR_ARRAY[$arg0 * 64] = (($arg1 & 0xff) << 24)
  set $PMB_DATA_ARRAY[$arg0 * 64] = (($arg2 & 0xff) << 24) | (1 << 8) | $_pmbdata
end

document sh4_set_pmb
Enable a PMB entry
Usage: sh4_set_pmb <index> <virtual> <physical> <size> [<cache>=1 [<wt>=0 [<ub>=0]]]]
where <index> is the PMB entry
+     <virtual> is the virtual page number
+     <physical> is the physical page number
+     <size> is the page size in MBytes
+     <cache> is optional and is the page cacheability (default: 1 [on])
+     <wt> is optional and is the page cache mode (default: 0 [copy-back])
+     <ub> is optional and is the page buffer mode (default: 0 [buffered])
end

################################################################################

define sh4_clear_pmb
  set $PMB_DATA_ARRAY[$arg0 * 64] = 0x00000000
  set $PMB_ADDR_ARRAY[$arg0 * 64] = 0x80000000
end

document sh4_clear_pmb
Disable a PMB entry
Usage: sh4_clear_pmb <index>
where <index> is the PMB entry
end

define sh4_clear_all_pmbs
  set $_pmb = 0
  while ($_pmb < 16)
    sh4_clear_pmb $_pmb
    set $_pmb++
  end
end

document sh4_clear_all_pmbs
Disable all PMB entries
Usage: sh4_clear_all_pmbs
end

################################################################################

define sh4_display_pmb
  set $_pmbaddrp = &$PMB_ADDR_ARRAY[$arg0 * 64]
  set $_pmbdatap = &$PMB_DATA_ARRAY[$arg0 * 64]

  printf "PMB[%02.2d] (0x%08x,0x%08x) = 0x%08x:0x%08x (Valid: %d)\n", $arg0, $_pmbaddrp, $_pmbdatap, *$_pmbaddrp, *$_pmbdatap, (*$_pmbaddrp >> 8) & 0x1
end

document sh4_display_pmb
Display the configuration of a PMB entry
Usage: sh4_display_pmb <index>
where <index> is the PMB entry
end

define sh4_display_all_pmbs
  set $_pmb = 0
  while ($_pmb < 16)
    sh4_display_pmb $_pmb
    set $_pmb++
  end
end

document sh4_display_all_pmbs
Display the configurations of all PMB entries
Usage: sh4_display_all_pmbs
end

################################################################################

define sh4_display_pmb_mapping
  set $_pmbaddr = $PMB_ADDR_ARRAY[$arg0 * 64]
  set $_pmbdata = $PMB_DATA_ARRAY[$arg0 * 64]

  set $_pmbvalid = ($_pmbaddr >> 8) & 0x1

  if ($_pmbvalid)
    set $_pmbwt = ($_pmbdata >> 0) & 0x1
    set $_pmbcached = ($_pmbdata >> 3) & 0x1
    set $_pmbub = ($_pmbdata >> 9) & 0x1
    set $_pmbsize = (($_pmbdata >> 6) & 0x2) | (($_pmbdata >> 4) & 0x1)

    printf "PMB[%02d]: ", $arg0
    if ($_pmbsize == 0)
      set $_pmbvpn = $_pmbaddr & 0xff000000
      set $_pmbppn = $_pmbdata & 0xff000000
      printf " 16M VIRT 0x%08x -> PHYS 0x%08x\n", $_pmbvpn, $_pmbppn
    else
    if ($_pmbsize == 1)
      set $_pmbvpn = $_pmbaddr & 0xfc000000
      set $_pmbppn = $_pmbdata & 0xfc000000
      printf " 64M VIRT 0x%08x -> PHYS 0x%08x\n", $_pmbvpn, $_pmbppn
    else
    if ($_pmbsize == 2)
      set $_pmbvpn = $_pmbaddr & 0xf8000000
      set $_pmbppn = $_pmbdata & 0xf8000000
      printf "128M VIRT 0x%08x -> PHYS 0x%08x\n", $_pmbvpn, $_pmbppn
    else
      set $_pmbvpn = $_pmbaddr & 0xe0000000
      set $_pmbppn = $_pmbdata & 0xe0000000
      printf "512M VIRT 0x%08x -> PHYS 0x%08x\n", $_pmbvpn, $_pmbppn
    end
    end
    end

    printf "("
    if ($_pmbcached)
      printf "CACHED, "
      if ($_pmbwt)
        printf "WRITE-THROUGH"
      else
        printf "COPY-BACK"
      end
    else
      printf "UNCACHED, "
      if ($_pmbub)
        printf "UNBUFFERED"
      else
        printf "BUFFERED"
      end
    end
    printf ")\n"
  end
end

document sh4_display_pmb_mapping
Display the mapping of a PMB entry if valid
Usage: sh4_display_pmb_mapping <index>
where <index> is the PMB entry
end

define sh4_display_all_pmb_mappings
  set $_pmb = 0
  while ($_pmb < 16)
    sh4_display_pmb_mapping $_pmb
    set $_pmb++
  end
end

document sh4_display_all_pmb_mappings
Display all the mappings of valid PMB entries
Usage: sh4_display_all_pmb_mappings
end

################################################################################

define sh4_enhanced_mode
  if ($argc)
    if ($CCN_PASCR)
      set *$CCN_PASCR = (*$CCN_PASCR & ~(1 << 31)) | (($arg0 & 0x1) << 31)
    else
      set *$CCN_MMUCR = (*$CCN_MMUCR & ~(1 << 4)) | (($arg0 & 0x1) << 4)
    end
  else
    if ($CCN_PASCR)
      printf "Space enhanced mode = %d\n", (*$CCN_PASCR >> 31) & 0x1
    else
      printf "Space enhanced mode = %d\n", (*$CCN_MMUCR >> 4) & 0x1
    end
  end
end

document sh4_enhanced_mode
Display or set space enhanced mode
Usage: sh4_enhanced_mode [<mode>]
where <mode> is optional and enables or disables space enhanced mode
end

################################################################################
