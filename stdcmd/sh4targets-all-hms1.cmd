################################################################################

source sh4targets-hms1cutX.cmd
source sh4targets-hms1stb7109cutX.cmd

################################################################################

define hms1cut13bypass
  if ($argc > 1)
  hms1cutXbypass 13 $arg0 $arg1
  else
  hms1cutXbypass 13 $arg0
  end
end

define hms1cut13stmmx
  if ($argc > 1)
  hms1cutXstmmx 13 $arg0 $arg1
  else
  hms1cutXstmmx 13 $arg0
  end
end

define hms1cut31bypass
  if ($argc > 1)
  hms1cutXbypass 31 $arg0 $arg1
  else
  hms1cutXbypass 31 $arg0
  end
end

define hms1cut31stmmx
  if ($argc > 1)
  hms1cutXstmmx 31 $arg0 $arg1
  else
  hms1cutXstmmx 31 $arg0
  end
end

define hms1stb7100cut31bypass128MB
  set $_hms1stb7100sys128 = 1
  if ($argc > 1)
    hms1cutXbypass 31 $arg0 $arg1
  else
    hms1cutXbypass 31 $arg0
  end
end

define hms1stb7100cut31stmmx128MB
  set $_hms1stb7100sys128 = 1
  if ($argc > 1)
    hms1cutXstmmx 31 $arg0 $arg1
  else
    hms1cutXstmmx 31 $arg0
  end
end

define hms1stb7109cut30bypass
  if ($argc > 1)
  hms1stb7109cutXbypass 30 $arg0 $arg1
  else
  hms1stb7109cutXbypass 30 $arg0
  end
end

define hms1stb7109cut30stmmx
  if ($argc > 1)
  hms1stb7109cutXstmmx 30 $arg0 $arg1
  else
  hms1stb7109cutXstmmx 30 $arg0
  end
end

define hms1stb7109cut30bypass128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109cutXbypass 30 $arg0 $arg1
  else
    hms1stb7109cutXbypass 30 $arg0
  end
end

define hms1stb7109cut30stmmx128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109cutXstmmx 30 $arg0 $arg1
  else
    hms1stb7109cutXstmmx 30 $arg0
  end
end

define hms1stb7109secut30bypass128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109secutXbypass 30 $arg0 $arg1
  else
    hms1stb7109secutXbypass 30 $arg0
  end
end

define hms1stb7109secut30stmmx128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109secutXstmmx 30 $arg0 $arg1
  else
    hms1stb7109secutXstmmx 30 $arg0
  end
end

define hms1stb7109seuccut30bypass128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109seuccutXbypass 30 $arg0 $arg1
  else
    hms1stb7109seuccutXbypass 30 $arg0
  end
end

define hms1stb7109seuccut30stmmx128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109seuccutXstmmx 30 $arg0 $arg1
  else
    hms1stb7109seuccutXstmmx 30 $arg0
  end
end

define hms1stb7109se29cut30bypass128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109se29cutXbypass 30 $arg0 $arg1
  else
    hms1stb7109se29cutXbypass 30 $arg0
  end
end

define hms1stb7109se29cut30stmmx128MB
  set $_hms1stb7109sys128 = 1
  if ($argc > 1)
    hms1stb7109se29cutXstmmx 30 $arg0 $arg1
  else
    hms1stb7109se29cutXstmmx 30 $arg0
  end
end
