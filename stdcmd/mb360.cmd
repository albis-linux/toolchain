##------------------------------------------------------------------------------
## mb360.cmd - ST40RA Evaluation Board MB360
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB360 EMI Configuration
define mb360_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x0000000c
  set *$EMI_BANK3_BASEADDRESS = 0x00000010
  set *$EMI_BANK4_BASEADDRESS = 0x00000014
  set *$EMI_BANK5_BASEADDRESS = 0x00000018

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 32Mb"

##------------------------------------------------------------------------------
## Bank 2 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 3 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 4 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 5 - EPLD Registers and LCD display
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for EPLD Registers and LCD display 31Mb"

  set *$EMI_BANK5_EMICONFIGDATA0 = 0x00a00689
  set *$EMI_BANK5_EMICONFIGDATA1 = 0xc0a80000
  set *$EMI_BANK5_EMICONFIGDATA2 = 0xc0ff0000
  set *$EMI_BANK5_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000000

##  _ST_display (_procname) "EMI FLASH CLOCK @ 1/2 system clock"
##  set *$EMI_FLASHCLKSEL = 0x00000001

  set *$EMI_CLKENABLE = 0x00000001
end
##}}}

##{{{  MB360 LMI Configuration
define mb360_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for 64-bit SDRAM burst length 4"

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  ## NOTE:
  ##   bit 8 = 0 for SSTL (DDR), 1 for LVTTL (PC-SDRAM)
  ##   bit 9 = 0 for internal ref voltage, 1 for external ref voltage
  ##   bit 10 = 1 to by pass ECLK180 retime
  ##   bit 11 = 1 for PC-SDRAM, 0 for DDR notLMICOMP25_EN
  ##   bit 12 = 1 for PC-SDRAM, 0 for DDR LMICOMP33_EN
  ##   bits [13,14] = LVTTL (DDR) drive strength for data & data strobe pads
  ##   bits [15,16] = LVTTL (DDR) drive strength for address & control pads

  set *$SYSCONF_SYS_CON2_0 = 0x0000b900

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x04000281

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x00001b4f

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0a001400

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0a001400

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00000190/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000190/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB360 Memory
define mb360_memory_define
  memory-add Flash      0x00000000 16 ROM
  memory-add EPLD_regs  0x06000000 31 RAM
  memory-add LMI_SDRAM  0x08000000 32 RAM
end

define mb360_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x08000000 32 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb360_setup
  st40ra_define
  mb360_memory_define

  st40ra_si_regs

  mb360_emi_configure
  mb360_lmi_configure

  set *$CCN_CCR = 0x0000090d
end

document mb360_setup
Configure an ST40RA-Eval board
Usage: mb360_setup
end

define mb360sim_setup
  st40ra_define
  mb360_memory_define

  st40ra_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb360sim_setup
Configure a simulated ST40RA-Eval board
Usage: mb360sim_setup
end

define mb360_fsim_setup
  st40ra_fsim_core_setup
  mb360_sim_memory_define
end

document mb360_fsim_setup
Configure functional simulator for ST40RA-Eval board
Usage: mb360_fsim_setup
end

define mb360_psim_setup
  st40ra_psim_core_setup
  mb360_sim_memory_define
end

document mb360_psim_setup
Configure performance simulator for ST40RA-Eval board
Usage: mb360_psim_setup
end

define mb360_display_registers
  st40ra_display_si_regs
end

document mb360_display_registers
Display the ST40RA configuration registers
Usage: mb360_display_registers
end
