define _tmcReadDeviceId
  ## Select specified device ID
  tmcSelectDeviceId $arg0

  ## Read device ID
  tmcWriteIR_IDCODE
  tmcReadDR 32 $_id

  set $arg1 = $_id_0

  ## Restore device ID to default
  tmcSelectDeviceId 0
end

define tmcReadDeviceId
  tmcInitialState
  _tmcReadDeviceId 0 $arg0
end

define tmcReadPrivateId
  tmcInitialState
  _tmcReadDeviceId 4 $arg0
end

define tmcReadOptionId
  tmcInitialState
  _tmcReadDeviceId 2 $arg0
end

define tmcReadExtraId
  tmcInitialState
  _tmcReadDeviceId 6 $arg0
end

define tmcPrintIds
  ## Reset TMC to bypass to TMC
  jtag mode=manual
  jtag tck=00 ntrst=01 tms=00 tdo=00
  jtag mode=normal

  tmcReadDeviceId $id
  printf "$arg0 deviceid = 0x%08x\n", $id
  tmcReadPrivateId $id
  printf "$arg0 privateid = 0x%08x\n", $id
  tmcReadOptionId $id
  printf "$arg0 optionid = 0x%08x\n", $id
  tmcReadExtraId $id
  printf "$arg0 extraid = 0x%08x\n", $id
end
