################################################################################

define mb676sti5197sim
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5197_fsim_setup mb676sti5197sim_setup $arg0
  else
    connectsh4simle mb676sti5197_fsim_setup mb676sti5197sim_setup ""
  end
end

document mb676sti5197sim
Connect to and configure a simulated STi5197-Mboard board
Usage: mb676sti5197sim
end

define mb676sti5197simse
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5197se_fsim_setup mb676sti5197simse_setup $arg0
  else
    connectsh4simle mb676sti5197se_fsim_setup mb676sti5197simse_setup ""
  end
end

document mb676sti5197simse
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode)
Usage: mb676sti5197simse
end

define mb676sti5197simseuc
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5197se_fsim_setup mb676sti5197simseuc_setup $arg0
  else
    connectsh4simle mb676sti5197se_fsim_setup mb676sti5197simseuc_setup ""
  end
end

document mb676sti5197simseuc
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with uncached mappings)
Usage: mb676sti5197simseuc
end

define mb676sti5197simse29
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb676sti5197se_fsim_setup mb676sti5197simse29_setup $arg0
  else
    connectsh4simle mb676sti5197se_fsim_setup mb676sti5197simse29_setup ""
  end
end

document mb676sti5197simse29
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb676sti5197simse29
end

################################################################################

define mb676sti5197psim
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5197_psim_setup mb676sti5197sim_setup $arg0
  else
    connectsh4psimle mb676sti5197_psim_setup mb676sti5197sim_setup ""
  end
end

document mb676sti5197psim
Connect to and configure a simulated STi5197-Mboard board
Usage: mb676sti5197psim
end

define mb676sti5197psimse
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5197se_psim_setup mb676sti5197simse_setup $arg0
  else
    connectsh4psimle mb676sti5197se_psim_setup mb676sti5197simse_setup ""
  end
end

document mb676sti5197psimse
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode)
Usage: mb676sti5197psimse
end

define mb676sti5197psimseuc
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5197se_psim_setup mb676sti5197simseuc_setup $arg0
  else
    connectsh4psimle mb676sti5197se_psim_setup mb676sti5197simseuc_setup ""
  end
end

document mb676sti5197psimseuc
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with uncached mappings)
Usage: mb676sti5197psimseuc
end

define mb676sti5197psimse29
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source mb676sti5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb676sti5197se_psim_setup mb676sti5197simse29_setup $arg0
  else
    connectsh4psimle mb676sti5197se_psim_setup mb676sti5197simse29_setup ""
  end
end

document mb676sti5197psimse29
Connect to and configure a simulated STi5197-Mboard board (STi5197 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb676sti5197psimse29
end

################################################################################
