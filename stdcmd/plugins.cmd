################################################################################

source brtrace.cmd
source jtag.cmd
source perfcount.cmd
source profiler.cmd
source targetpack.cmd
source kptrace.cmd

################################################################################

source coverif.cmd

################################################################################

define enable_all_plugins
  if (!$branch_trace_enabled)
    enable_branch_trace
  end
  if (!$jtag_enabled)
    enable_jtag
  end
  if (!$performance_counters_enabled)
    enable_performance_counters
  end
  if (!$profiler_enabled)
    enable_profiler
  end
end

document enable_all_plugins
Enable all the silicon target plugins
Usage: enable_all_plugins
end

################################################################################

define _init_all_plugins
  init-if-undefined $_enable_all_plugins = 1
  keep-variable $_enable_all_plugins

  if ($_enable_all_plugins)
    enable_all_plugins
  end

  init-if-undefined $_init_branch_trace = 1
  keep-variable $_init_branch_trace

  if ($branch_trace_enabled && $_init_branch_trace)
    branchtrace mode all
  end
end

################################################################################

source sh4enhanced.cmd
source sh4virtual.cmd

################################################################################
