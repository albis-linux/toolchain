################################################################################

source register40.cmd
source display40.cmd

################################################################################

source fli7510.cmd
source sh4202.cmd
source sh7750.cmd
source sh7751.cmd
source st40300.cmd
source st40gx1.cmd
source st40ra.cmd
source stb7100.cmd
source stb7109.cmd
source std1000.cmd
source std2000.cmd
source sti5189.cmd
source sti5197.cmd
source sti5202.cmd
source sti5206.cmd
source sti5289.cmd
source sti5528.cmd
source sti7105.cmd
source sti7106.cmd
source sti7108.cmd
source sti7111.cmd
source sti7141.cmd
source sti7200.cmd
source stm8000.cmd
source stv0498.cmd

################################################################################

source adi7105.cmd
source adi7108.cmd
source cab5197.cmd
source db412.cmd
source db457.cmd
source espresso.cmd
source eud7141.cmd
source fldb_gpd201.cmd
source hdk5189.cmd
source hdk5289sti5206.cmd
source hdk5289sti5289.cmd
source hdk7105.cmd
source hdk7106.cmd
source hdk7108.cmd
source hdk7111.cmd
source mb250.cmd
source mb293.cmd
source mb317.cmd
source mb360.cmd
source mb374.cmd
source mb376.cmd
source mb379.cmd
source mb388.cmd
source mb392.cmd
source mb411stb7100.cmd
source mb411stb7109.cmd
source mb422.cmd
source mb427.cmd
source mb442stb7100.cmd
source mb442stb7109.cmd
source mb448.cmd
source mb519.cmd
source mb521.cmd
source mb548.cmd
source mb602.cmd
source mb618.cmd
source mb625.cmd
source mb628.cmd
source mb671.cmd
source mb676sti5189.cmd
source mb676sti5197.cmd
source mb680sti7105.cmd
source mb680sti7106.cmd
source mb704.cmd
source mb796sti5206.cmd
source mb796sti5289.cmd
source mb837.cmd
source mediaref.cmd
source sat5189.cmd
source sat7111.cmd
source sh4202microdev.cmd
source sh4202romram.cmd
source sh7750eval.cmd
source sh7751eval.cmd
source sh7751rsystemh.cmd
source tmmidr04.cmd
source tmmlr1.cmd
source tmmlr2.cmd

################################################################################

source db436.cmd
source db479.cmd
source db507.cmd
source db558.cmd
source db641.cmd

################################################################################

source custom13.cmd
source g2tmm.cmd
source hms1.cmd
source hns.cmd
source mdv2.cmd

################################################################################

source sh4connect.cmd

################################################################################

source shsimcmds.cmd

################################################################################

source plugins.cmd

################################################################################

source jtagcommon.cmd
source jtaghudi.cmd
source jtagstmmx.cmd
source jtagtmc.cmd

################################################################################

source st40clocks.cmd

################################################################################

source stb7100boot.cmd
source stb7100clocks.cmd
source stb7100jtag.cmd

################################################################################

source sti5528clocks.cmd

################################################################################

source sti7200boot.cmd
source sti7200clocks.cmd
source sti7200jtag.cmd

################################################################################

source stm8000clocks.cmd

################################################################################

source stv0498jtag.cmd

################################################################################

source sh4targets.cmd

################################################################################
