################################################################################

source sh4commands.cmd

################################################################################

source sh4targets-core.cmd

################################################################################

source sh4targets-attach.cmd
source sh4targets-attach-debug.cmd

################################################################################

source sh4targets-adi7105.cmd
source sh4targets-adi7108.cmd
source sh4targets-cab5197.cmd
source sh4targets-db412.cmd
source sh4targets-db457.cmd
source sh4targets-espresso.cmd
source sh4targets-eud7141.cmd
source sh4targets-fldb_gpd201.cmd
source sh4targets-hdk5189.cmd
source sh4targets-hdk5289sti5206.cmd
source sh4targets-hdk5289sti5289.cmd
source sh4targets-hdk7105.cmd
source sh4targets-hdk7106.cmd
source sh4targets-hdk7108.cmd
source sh4targets-hdk7111.cmd
source sh4targets-mb250.cmd
source sh4targets-mb293.cmd
source sh4targets-mb317a.cmd
source sh4targets-mb317b.cmd
source sh4targets-mb350.cmd
source sh4targets-mb360.cmd
source sh4targets-mb374.cmd
source sh4targets-mb376.cmd
source sh4targets-mb379.cmd
source sh4targets-mb388.cmd
source sh4targets-mb392.cmd
source sh4targets-mb411stb7100.cmd
source sh4targets-mb411stb7109.cmd
source sh4targets-hdrefstb7109.cmd
source sh4targets-mb422.cmd
source sh4targets-mb427.cmd
source sh4targets-mb442stb7100.cmd
source sh4targets-mb442stb7109.cmd
source sh4targets-mb448.cmd
source sh4targets-mb519.cmd
source sh4targets-mb521.cmd
source sh4targets-mb548.cmd
source sh4targets-mb602.cmd
source sh4targets-mb618.cmd
source sh4targets-mb625.cmd
source sh4targets-mb628.cmd
source sh4targets-mb671.cmd
source sh4targets-mb676sti5189.cmd
source sh4targets-mb676sti5197.cmd
source sh4targets-mb680sti7105.cmd
source sh4targets-mb680sti7106.cmd
source sh4targets-mb704.cmd
source sh4targets-mb796sti5206.cmd
source sh4targets-mb796sti5289.cmd
source sh4targets-mb837.cmd
source sh4targets-mediaref.cmd
source sh4targets-sat5189.cmd
source sh4targets-sat7111.cmd
source sh4targets-sh4202microdev.cmd
source sh4targets-sh4202romram.cmd
source sh4targets-sh7750eval.cmd
source sh4targets-sh7751eval.cmd
source sh4targets-sh7751rsystemh.cmd
source sh4targets-tmmidr04.cmd
source sh4targets-tmmlr1.cmd
source sh4targets-tmmlr2.cmd

################################################################################

source sh4targets-mb379cut1.cmd

################################################################################

source sh4targets-mb411stb7100cutX.cmd
source sh4targets-mb411stb7109cutX.cmd
source sh4targets-mb442stb7100cutX.cmd
source sh4targets-mb442stb7109cutX.cmd

################################################################################

source sh4targets-all-stb710x.cmd

################################################################################

source sh4targets-custom13.cmd
source sh4targets-g2tmm.cmd
source sh4targets-hms1.cmd
source sh4targets-hns.cmd
source sh4targets-mdv2.cmd

################################################################################

source sh4targets-targetpack.cmd

################################################################################
