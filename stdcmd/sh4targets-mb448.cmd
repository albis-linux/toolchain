################################################################################

define mb448
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb448_setup $arg1
  else
    connectsh4le $arg0 mb448_setup "jtagpinout=st40 hardreset"
  end
end

document mb448
Connect to and configure an STb7109E-Ref board
Usage: mb448 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448se
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb448se_setup $arg1
  else
    connectsh4le $arg0 mb448se_setup "jtagpinout=st40 hardreset"
  end
end

document mb448se
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode)
Usage: mb448se <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448seuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb448seuc_setup $arg1
  else
    connectsh4le $arg0 mb448seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb448seuc
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448seuc <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448se29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb448se29_setup $arg1
  else
    connectsh4le $arg0 mb448se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb448se29
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448se29 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448bypass
  if ($argc > 1)
    mb448 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448 $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448bypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40
Usage: mb448bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448sebypass
  if ($argc > 1)
    mb448se $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448se $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448sebypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb448sebypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448seucbypass
  if ($argc > 1)
    mb448seuc $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448seuc $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448seucbypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448seucbypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448se29bypass
  if ($argc > 1)
    mb448se29 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448se29 $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448se29bypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448se29bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448stmmx
  if ($argc > 1)
    mb448 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448stmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux
Usage: mb448stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448sestmmx
  if ($argc > 1)
    mb448se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448sestmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb448sestmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448seucstmmx
  if ($argc > 1)
    mb448seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448seucstmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448seucstmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb448se29stmmx
  if ($argc > 1)
    mb448se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448se29stmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448se29stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb448usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb448_setup $arg1
  else
    connectsh4usble $arg0 mb448_setup "jtagpinout=st40 hardreset"
  end
end

document mb448usb
Connect to and configure an STb7109E-Ref board
Usage: mb448usb <target>
where <target> is an ST Micro Connect USB name
end

define mb448seusb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb448se_setup $arg1
  else
    connectsh4usble $arg0 mb448se_setup "jtagpinout=st40 hardreset"
  end
end

document mb448seusb
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode)
Usage: mb448seusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448seucusb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb448seuc_setup $arg1
  else
    connectsh4usble $arg0 mb448seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb448seucusb
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448seucusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448se29usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb448se29_setup $arg1
  else
    connectsh4usble $arg0 mb448se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb448se29usb
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448se29usb <target>
where <target> is an ST Micro Connect USB name
end

define mb448bypassusb
  if ($argc > 1)
    mb448usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448bypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40
Usage: mb448bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448sebypassusb
  if ($argc > 1)
    mb448seusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448seusb $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448sebypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb448sebypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448seucbypassusb
  if ($argc > 1)
    mb448seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448seucbypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448seucbypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448se29bypassusb
  if ($argc > 1)
    mb448se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb448se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448se29bypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448se29bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448stmmxusb
  if ($argc > 1)
    mb448usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448stmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux
Usage: mb448stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448sestmmxusb
  if ($argc > 1)
    mb448seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448sestmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb448sestmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448seucstmmxusb
  if ($argc > 1)
    mb448seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448seucstmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448seucstmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb448se29stmmxusb
  if ($argc > 1)
    mb448se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb448se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448se29stmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448se29stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb448sim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb448_fsim_setup mb448sim_setup $arg0
  else
    connectsh4simle mb448_fsim_setup mb448sim_setup ""
  end
end

document mb448sim
Connect to and configure a simulated STb7109E-Ref board
Usage: mb448sim
end

define mb448simse
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb448se_fsim_setup mb448simse_setup $arg0
  else
    connectsh4simle mb448se_fsim_setup mb448simse_setup ""
  end
end

document mb448simse
Connect to and configure a simulated STb7109E-Ref board (STb7109 in 32-bit SE mode)
Usage: mb448simse
end

define mb448simseuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb448se_fsim_setup mb448simseuc_setup $arg0
  else
    connectsh4simle mb448se_fsim_setup mb448simseuc_setup ""
  end
end

document mb448simseuc
Connect to and configure a simulated STb7109E-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448simseuc
end

define mb448simse29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb448se_fsim_setup mb448simse29_setup $arg0
  else
    connectsh4simle mb448se_fsim_setup mb448simse29_setup ""
  end
end

document mb448simse29
Connect to and configure a simulated STb7109E-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448simse29
end

################################################################################

define mb448psim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb448_psim_setup mb448sim_setup $arg0
  else
    connectsh4psimle mb448_psim_setup mb448sim_setup ""
  end
end

document mb448psim
Connect to and configure a simulated STb7109E-Ref board
Usage: mb448psim
end

define mb448psimse
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb448se_psim_setup mb448simse_setup $arg0
  else
    connectsh4psimle mb448se_psim_setup mb448simse_setup ""
  end
end

document mb448psimse
Connect to and configure a simulated STb7109E-Ref board (STb7109 in 32-bit SE mode)
Usage: mb448psimse
end

define mb448psimseuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb448se_psim_setup mb448simseuc_setup $arg0
  else
    connectsh4psimle mb448se_psim_setup mb448simseuc_setup ""
  end
end

document mb448psimseuc
Connect to and configure a simulated STb7109E-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448psimseuc
end

define mb448psimse29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb448.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb448se_psim_setup mb448simse29_setup $arg0
  else
    connectsh4psimle mb448se_psim_setup mb448simse29_setup ""
  end
end

document mb448psimse29
Connect to and configure a simulated STb7109E-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448psimse29
end

################################################################################
