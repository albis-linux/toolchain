##------------------------------------------------------------------------------
## mb602.cmd - STi5202-Mboard Validation Platform MB602
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

init-if-undefined $_mb602extclk = 30
keep-variable $_mb602extclk

##{{{  MB602 CLOCKGEN Configuration
define mb602_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  linkspeed 1.25MHz

  if ($_mb602extclk == 30)
    ## Set PLL0 to 531MHz
    stb7100_set_clockgen_a_pll0 0x14 0xb1 0x0
    ## Set PLL1 to 400MHz
    stb7100_set_clockgen_a_pll1 0x3 0x28 0x01
  else
  if ($_mb602extclk == 27)
    ## Set PLL0 to 531MHz
    stb7100_set_clockgen_a_pll0 0x06 0x3b 0x0
    ## Set PLL1 to 400MHz
    stb7100_set_clockgen_a_pll1 0x1b 0xc8 0x0
  else
    ## Unsupported external clock frequency
  end
  end

  ## Restore default link speed (5MHz maximum for TapMux)
  if ($_stmmxmode)
    linkspeed 5MHz
  else
    linkspeed 10MHz
  end
end
##}}}

##{{{  MB602 SYSCONF Configuration
define mb602_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  set *$SYSCONF_SYS_CFG11 = $_sti5202movelmiregs ? 0x2d7fd4ea : 0x0d7fd4ea

  while ((*$SYSCONF_SYS_STA12 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  set *$SYSCONF_SYS_CFG12 = 0x4000000f | (0xf << 12) | (0xf << 23)
end

define mb602se_sysconf_configure
  mb602_sysconf_configure

  # Move LMI base address to its space enhanced mode address
  set *$SYSCONF_SYS_CFG36 = (*$SYSCONF_SYS_CFG36 & 0xffffff00) | 0x00000040
end
##}}}

##{{{  MB602 EMI Configuration
define mb602_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000008
  set *$EMI_BANK2_BASEADDRESS = 0x0000000c
  set *$EMI_BANK3_BASEADDRESS = 0x0000000e
  set *$EMI_BANK4_BASEADDRESS = 0x00000010

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 32Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016d1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 2 - LAN91C111
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for LAN91C111 8Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x042086f1
  set *$EMI_BANK2_EMICONFIGDATA1 = 0x88112111
  set *$EMI_BANK2_EMICONFIGDATA2 = 0x88112211
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - ATAPI
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for ATAPI 8Mb"

  set *$EMI_BANK3_EMICONFIGDATA0 = 0x00200791
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x0c006700
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 8Mb"

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000010
end
##}}}

##{{{  MB602 LMI Configuration
define mb602_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x861a025f
  set *$LMI_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = (((*$SYSCONF_SYS_CFG36 & 0xff) + 0x08) << 24) | 0x00001a00

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = (((*$SYSCONF_SYS_CFG36 & 0xff) + 0x08) << 24) | 0x00001a00

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SDMR0 = 0x00000402
  set *$LMI_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SDMR0 = 0x00000033
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB602 PMB Configuration
define mb602se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0xb8 0x18 64 0 1 1
  if (!$_sti5202movelmiregs)
  sh4_set_pmb 2 0xaf 0x0f 16 0 1 1
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb602seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  sh4_set_pmb 1 0xb8 0x18 64 0 1 1
  if (!$_sti5202movelmiregs)
  sh4_set_pmb 2 0xaf 0x0f 16 0 1 1
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb602se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0xa0 0x40 128 0 0 1
  sh4_set_pmb 2 0xb8 0x18 64 0 1 1
  if (!$_sti5202movelmiregs)
  sh4_set_pmb 3 0xaf 0x0f 16 0 1 1
  end

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB602 Memory
define mb602_memory_define
  memory-add Flash     0x00000000 32 ROM
  memory-add LMI_SDRAM 0x04000000 128 RAM
end

define mb602_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x04000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define mb602se_memory_define
  memory-add Flash     0x00000000 32 ROM
  memory-add LMI_SDRAM 0x40000000 128 RAM
end

define mb602se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x40000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  MB602 Bypass Configuration
define mb602bypass_setup
  if ($argc > 0)
    stb7100_bypass_setup $arg0
  else
    stb7100_bypass_setup
  end
end

define mb602bypass_setup_attach
  if ($argc > 0)
    stb7100_bypass_setup_attach $arg0
  else
    stb7100_bypass_setup_attach
  end
end
##}}}

##{{{  MB602 STMMX Configuration
define mb602stmmx_setup
  if ($argc > 0)
    stb7100_stmmx_setup $arg0
  else
    stb7100_stmmx_setup
  end
end

define mb602stmmx_setup_attach
  if ($argc > 0)
    stb7100_stmmx_setup_attach $arg0
  else
    stb7100_stmmx_setup_attach
  end
end
##}}}

define mb602_setup
  sti5202_define
  mb602_memory_define

  sti5202_si_regs

  mb602_clockgen_configure
  mb602_sysconf_configure
  mb602_emi_configure
  mb602_lmi_configure

  set *$CCN_CCR = 0x8000090d
end

document mb602_setup
Configure an STi5202-Ref board
Usage: mb602_setup
end

define mb602se_setup
  sti5202_define
  mb602se_memory_define

  sti5202_si_regs

  mb602_clockgen_configure
  mb602se_sysconf_configure
  mb602_emi_configure
  mb602_lmi_configure

  mb602se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb602se_setup
Configure an STi5202-Ref board with the STi5202 in 32-bit SE mode
Usage: mb602se_setup
end

define mb602seuc_setup
  sti5202_define
  mb602se_memory_define

  sti5202_si_regs

  mb602_clockgen_configure
  mb602se_sysconf_configure
  mb602_emi_configure
  mb602_lmi_configure

  mb602seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb602seuc_setup
Configure an STi5202-Ref board with the STi5202 in 32-bit SE mode with uncached RAM mappings
Usage: mb602seuc_setup
end

define mb602se29_setup
  sti5202_define
  mb602se_memory_define

  sti5202_si_regs

  mb602_clockgen_configure
  mb602se_sysconf_configure
  mb602_emi_configure
  mb602_lmi_configure

  mb602se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb602se29_setup
Configure an STi5202-Ref board with the STi5202 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb602se29_setup
end

define mb602sim_setup
  sti5202_define
  mb602_memory_define

  sti5202_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb602sim_setup
Configure a simulated STi5202-Ref board
Usage: mb602sim_setup
end

define mb602simse_setup
  sti5202_define
  mb602se_memory_define

  sti5202_si_regs

  mb602se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb602simse_setup
Configure a simulated STi5202-Ref board with the STi5202 in 32-bit SE mode
Usage: mb602simse_setup
end

define mb602simseuc_setup
  sti5202_define
  mb602se_memory_define

  sti5202_si_regs

  mb602seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb602simseuc_setup
Configure a simulated STi5202-Ref board with the STi5202 in 32-bit SE mode with uncached RAM mappings
Usage: mb602simseuc_setup
end

define mb602simse29_setup
  sti5202_define
  mb602se_memory_define

  sti5202_si_regs

  mb602se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb602simse29_setup
Configure a simulated STi5202-Ref board with the STi5202 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb602simse29_setup
end

define mb602_fsim_setup
  sti5202_fsim_core_setup
  mb602_sim_memory_define
end

document mb602_fsim_setup
Configure functional simulator for STi5202-Ref board
Usage: mb602_fsim_setup
end

define mb602se_fsim_setup
  sti5202_fsim_core_setup
  mb602se_sim_memory_define
end

document mb602se_fsim_setup
Configure functional simulator for STi5202-Ref board with the STi5202 in 32-bit SE mode
Usage: mb602se_fsim_setup
end

define mb602_psim_setup
  sti5202_psim_core_setup
  mb602_sim_memory_define
end

document mb602_psim_setup
Configure performance simulator for STi5202-Ref board
Usage: mb602_psim_setup
end

define mb602se_psim_setup
  sti5202_psim_core_setup
  mb602se_sim_memory_define
end

document mb602se_psim_setup
Configure performance simulator for STi5202-Ref board with the STi5202 in 32-bit SE mode
Usage: mb602se_psim_setup
end

define mb602_display_registers
  sti5202_display_si_regs
end

document mb602_display_registers
Display the STi5202 configuration registers
Usage: mb602_display_registers
end
