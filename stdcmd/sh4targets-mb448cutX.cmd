################################################################################

define mb448cutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb448_setup $arg2
  else
    connectsh4le $arg1 mb448_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutX
Connect to and configure an STb7109E-Ref board
Usage: mb448cutX <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXse
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb448se_setup $arg2
  else
    connectsh4le $arg1 mb448se_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutXse
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode)
Usage: mb448cutXse <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXseuc
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb448seuc_setup $arg2
  else
    connectsh4le $arg1 mb448seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutXseuc
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448cutXseuc <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXse29
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4le $arg1 mb448se29_setup $arg2
  else
    connectsh4le $arg1 mb448se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutXse29
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448cutXse29 <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXbypass
  if ($argc > 2)
    mb448cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXbypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40
Usage: mb448cutXbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXsebypass
  if ($argc > 2)
    mb448cutXse $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutXse $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXsebypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb448cutXsebypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXseucbypass
  if ($argc > 2)
    mb448cutXseuc $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutXseuc $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXseucbypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448cutXseucbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXse29bypass
  if ($argc > 2)
    mb448cutXse29 $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutXse29 $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXse29bypass
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448cutXse29bypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXstmmx
  if ($argc > 2)
    mb448cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXstmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux
Usage: mb448cutXstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXsestmmx
  if ($argc > 2)
    mb448cutXse $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutXse $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXsestmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb448cutXsestmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXseucstmmx
  if ($argc > 2)
    mb448cutXseuc $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutXseuc $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXseucstmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448cutXseucstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define mb448cutXse29stmmx
  if ($argc > 2)
    mb448cutXse29 $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutXse29 $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXse29stmmx
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448cutXse29stmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb448cutXusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb448_setup $arg2
  else
    connectsh4usble $arg1 mb448_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutXusb
Connect to and configure an STb7109E-Ref board
Usage: mb448cutXusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXseusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb448se_setup $arg2
  else
    connectsh4usble $arg1 mb448se_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutXseusb
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode)
Usage: mb448cutXseusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXseucusb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb448seuc_setup $arg2
  else
    connectsh4usble $arg1 mb448seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutXseucusb
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448cutXseucusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXse29usb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb448cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
    connectsh4usble $arg1 mb448se29_setup $arg2
  else
    connectsh4usble $arg1 mb448se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb448cutXse29usb
Connect to and configure an STb7109E-Ref board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448cutXse29usb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXbypassusb
  if ($argc > 2)
    mb448cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutXusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXbypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40
Usage: mb448cutXbypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXsebypassusb
  if ($argc > 2)
    mb448cutXseusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutXseusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXsebypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb448cutXsebypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXseucbypassusb
  if ($argc > 2)
    mb448cutXseucusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutXseucusb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXseucbypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448cutXseucbypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXse29bypassusb
  if ($argc > 2)
    mb448cutXse29usb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
    mb448cutXse29usb $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand mb448bypass_setup"
  end
end

document mb448cutXse29bypassusb
Connect to and configure an STb7109E-Ref board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448cutXse29bypassusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXstmmxusb
  if ($argc > 2)
    mb448cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutXusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXstmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux
Usage: mb448cutXstmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXsestmmxusb
  if ($argc > 2)
    mb448cutXseusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutXseusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXsestmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb448cutXsestmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXseucstmmxusb
  if ($argc > 2)
    mb448cutXseucusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutXseucusb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXseucstmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb448cutXseucstmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

define mb448cutXse29stmmxusb
  if ($argc > 2)
    mb448cutXse29usb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
    mb448cutXse29usb $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb448stmmx_setup"
  end
end

document mb448cutXse29stmmxusb
Connect to and configure an STb7109E-Ref board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb448cutXse29stmmxusb <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect USB name
end

################################################################################
