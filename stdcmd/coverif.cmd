set $coverification_counters_enabled = 0
keep-variable $coverification_counters_enabled

define coverif
  if (!$coverification_counters_enabled)
    printf "Coverification counters plugin not installed\n"
  else
    if ($argc == 0)
      callplugin coverif
    end
    if ($argc == 1)
      callplugin coverif $arg0
    end
    if ($argc == 2)
      callplugin coverif $arg0 $arg1
    end
    if ($argc == 3)
      callplugin coverif $arg0 $arg1 $arg2
    end
    if ($argc > 3)
      callplugin coverif help
    end
  end
end

document coverif
Use "coverif help"
end

define enable_coverification_counters
  if (!$coverification_counters_enabled)
    installplugin coverif libsh4coverif.so
    set $coverification_counters_enabled = 1
  else
    printf "Coverification counters plugin already enabled\n"
  end
end

document enable_coverification_counters
Enable the coverification counters plugin
Usage: enable_coverification_counters
end
