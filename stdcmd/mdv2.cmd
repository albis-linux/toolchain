##------------------------------------------------------------------------------
## mdv2.cmd - Sagem STb7100 MDV-2 Platform
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MDV2 CLOCKGEN Configuration
define mdv2_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  linkspeed 1.25MHz

  ## Set PLL0 to 531MHz
  stb7100_set_clockgen_a_pll0 0x06 0x3b 0x0
  ## Set PLL1 to 400MHz
  stb7100_set_clockgen_a_pll1 0x1b 0xc8 0x0

  ## Set CLOCKGENA_OUT_CTRL to clk_emi
  set *$CLOCKGENA_LOCK = 0xc0de
  set *$CLOCKGENA_OUT_CTRL = 0xb
  set *$CLOCKGENA_LOCK = 0x0

  ## Restore default link speed (5MHz maximum for TapMux)
  if ($_stmmxmode)
    linkspeed 5MHz
  else
    linkspeed 10MHz
  end
end
##}}}

##{{{  MDV2 SYSCONF Configuration
define mdv2_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  ## Source SmartCard clock from clk_dss
  set *$SYSCONF_SYS_CFG07 = 0x00000010

  set *$SYSCONF_SYS_CFG11 = 0x080780c0

  while ((*$SYSCONF_SYS_STA12 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  while ((*$SYSCONF_SYS_STA13 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  set *$SYSCONF_SYS_CFG12 = 0x4000000f | (0xf << 12) | (0xf << 23)
  set *$SYSCONF_SYS_CFG13 = 0x4000000f | (0xf << 12) | (0xf << 23)

  set *$SYSCONF_SYS_CFG14 = (1 << 18) | (0x54 << 20)
  set *$SYSCONF_SYS_CFG15 = (1 << 19) | (0x56 << 20)

  set *$SYSCONF_SYS_CFG20 = (1 << 18) | (0x48 << 20)
  set *$SYSCONF_SYS_CFG21 = (1 << 19) | (0x50 << 20)
end
##}}}

##{{{  MDV2 EMI Configuration
define mdv2_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x00000009
  set *$EMI_BANK4_BASEADDRESS = 0x0000000a

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x00000691
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x8b100000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x8c110000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for on-board Flash 16Mb"

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x00000691
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x8b100000
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x8c110000
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 2 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Undefined 4Mb"

##------------------------------------------------------------------------------
## Bank 3 - LAN9118
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for LAN9118 4Mb"

  set *$EMI_BANK3_EMICONFIGDATA0 = 0x000196d1
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x85100000
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x86110000
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 24Mb"
end
##}}}

##{{{  MDV2 LMI Configuration
define mdv2_lmisys_configure
  ## _ST_display (_procname) "Configuring LMI-SYS for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMISYS_MIM_0 = 0x861a0247
  set *$LMISYS_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMISYS_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMISYS_SDRA0_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMISYS_SDRA1_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000003
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SDMR0 = 0x00000400
  set *$LMISYS_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SDMR0 = 0x00000033
  set *$LMISYS_SCR_0 = 0x00000000
end

define mdv2_lmivid_configure
  ## _ST_display (_procname) "Configuring LMI-VID for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMIVID_MIM_0 = 0x861a0247
  set *$LMIVID_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMIVID_STR_0 = 0x35b06455

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMIVID_SDRA0_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMIVID_SDRA1_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000003
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SDMR0 = 0x00000400
  set *$LMIVID_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SDMR0 = 0x00000033
  set *$LMIVID_SCR_0 = 0x00000000
end
##}}}

##{{{  MDV2 Memory
define mdv2_memory_define
  memory-add Flash        0x00000000 8 ROM
  memory-add LMISYS_SDRAM 0x04000000 64 RAM
  memory-add LMIVID_SDRAM 0x10000000 64 RAM
end

define mdv2_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x04000000 64 RAM
  sim_addmemory 0x10000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  MDV2 Bypass Configuration
define mdv2bypass_setup
  if ($argc > 0)
    stb7100_bypass_setup $arg0
  else
    stb7100_bypass_setup
  end
end

define mdv2bypass_setup_attach
  if ($argc > 0)
    stb7100_bypass_setup_attach $arg0
  else
    stb7100_bypass_setup_attach
  end
end
##}}}

##{{{  MDV2 STMMX Configuration
define mdv2stmmx_setup
  if ($argc > 0)
    stb7100_stmmx_setup $arg0
  else
    stb7100_stmmx_setup
  end
end

define mdv2stmmx_setup_attach
  if ($argc > 0)
    stb7100_stmmx_setup_attach $arg0
  else
    stb7100_stmmx_setup_attach
  end
end
##}}}

define mdv2_setup
  stb7100_define
  mdv2_memory_define

  stb7100_si_regs

  mdv2_clockgen_configure
  mdv2_sysconf_configure
  mdv2_emi_configure
  mdv2_lmisys_configure
  mdv2_lmivid_configure

  set *$CCN_CCR = 0x8000090d
end

document mdv2_setup
Configure a Sagem MDV-2 board
Usage: mdv2_setup
end

define mdv2sim_setup
  stb7100_define
  mdv2_memory_define

  stb7100_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mdv2sim_setup
Configure a simulated Sagem MDV-2 board
Usage: mdv2sim_setup
end

define mdv2_fsim_setup
  stb7100_fsim_core_setup
  mdv2_sim_memory_define
end

document mdv2_fsim_setup
Configure functional simulator for Sagem MDV-2 board
Usage: mdv2_fsim_setup
end

define mdv2_psim_setup
  stb7100_psim_core_setup
  mdv2_sim_memory_define
end

document mdv2_psim_setup
Configure performance simulator for Sagem MDV-2 board
Usage: mdv2_psim_setup
end

define mdv2_display_registers
  stb7100_display_si_regs
end

document mdv2_display_registers
Display the STb7100 configuration registers
Usage: mdv2_display_registers
end
