##------------------------------------------------------------------------------
## sh4202romram.cmd - SH4202 ROMRAM Evaluation Board
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  SH4202ROMRAM Configure
define sh4202romram_configure
##------------------------------------------------------------------------------
## Configure FRQCR register
##------------------------------------------------------------------------------
  set *$CPG_WTCNT = 0x5a00
  set *$CPG_WTCSR = 0xa567
  set *$CPG_FRQCR = 0x0e0a

##------------------------------------------------------------------------------
## Program FEMI registers
##------------------------------------------------------------------------------
  set *$FEMI_A1MCR = (*$FEMI_A1MCR & ~0x03707000) | 0x004041010

##------------------------------------------------------------------------------
## Program EMI registers
##------------------------------------------------------------------------------
  set *$CPG2_FRQCR3 = 0x0000016a

  set *$EMI_COC = 0x00000080
  sleep 0 10000
  set *$EMI_MIM = 0x00000081
  set *$EMI_STR = 0x0000116b
  set *$EMI_SDRA0 = 0x0c001500
  set *$EMI_SDRA1 = 0x10001500
  set *$EMI_SCR = 0x00000003
  sleep 0 10000
  set *$EMI_SCR = 0x00000002
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *($EMI_SDMR0+(0x0110/sizeof(*$EMI_SDMR0))) = 0x0
  set *($EMI_SDMR1+(0x0110/sizeof(*$EMI_SDMR1))) = 0x0
  set *$EMI_SCR = 0x00000000
  set *$EMI_MIM = 0x00900281
end
##}}}

##{{{  SH4202ROMRAM Memory
define sh4202romram_memory_define
  memory-add FEMI_RAM 0x04000000 2 RAM
  memory-add EMI_RAM  0x08000000 128 RAM
end

define sh4202romram_sim_memory_define
  sim_addmemory 0x04000000 2 RAM
  sim_addmemory 0x08000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define sh4202romram_setup
  sh4202_define
  sh4202romram_memory_define

  sh4202_si_regs

  sh4202romram_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202romram_setup
Configure an SH4202 Evaluation board
Usage: sh4202romram_setup
end

define sh4202romramsim_setup
  sh4202_define
  sh4202romram_memory_define

  sh4202_si_regs

  set *$CCN_CCR = 0x8000090d
end

document sh4202romramsim_setup
Configure a simulated SH4202 Evaluation board
Usage: sh4202romramsim_setup
end

define sh4202romram_fsim_setup
  sh4202_fsim_core_setup
  sh4202romram_sim_memory_define
end

document sh4202romram_fsim_setup
Configure functional simulator for SH4202 ROMRAM Evaluation board
Usage: sh4202romram_fsim_setup
end

define sh4202romram_psim_setup
  sh4202_psim_core_setup
  sh4202romram_sim_memory_define
end

document sh4202romram_psim_setup
Configure performance simulator for SH4202 ROMRAM Evaluation board
Usage: sh4202romram_psim_setup
end

define sh4202romram_display_registers
  sh4202_display_si_regs
end

document sh4202romram_display_registers
Display the SH4202 configuration registers
Usage: sh4202romram_display_registers
end
