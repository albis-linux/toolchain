##
## Configure and display clocks for ST40
##

##{{{  st40_set_clockgen_a_pll1
define st40_set_clockgen_a_pll1
  ## Stop CLOCKGENA_PLL1...
  set $_data = *$CLOCKGENA_PLL1CR2
  set $_data = $_data | 0x1
  set *$CLOCKGENA_PLL1CR2 = $_data
  set $_data = $_data | 0x2
  set *$CLOCKGENA_PLL1CR2 = $_data
  set $_data = $_data | 0x4
  set *$CLOCKGENA_PLL1CR2 = $_data
  set $_data = $_data | 0x8
  set *$CLOCKGENA_PLL1CR2 = $_data
  set $_data = $_data & 0xffffffffb
  set *$CLOCKGENA_PLL1CR2 = $_data
  set $_data = $_data & 0xffffffffe
  set *$CLOCKGENA_PLL1CR2 = $_data

  ## Configure CLOCKGENA_PLL1...
  set *$CLOCKGENA_PLL1CR1 = (*$CLOCKGENA_PLL1CR1 & 0xf0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | (0x127 << 19)

  ## Restart CLOCKGENA_PLL1...
  set $_data = *$CLOCKGENA_PLL1CR2
  set $_data = $_data | 0x1
  set *$CLOCKGENA_PLL1CR2 = $_data
  set $_data = $_data | 0x4
  set *$CLOCKGENA_PLL1CR2 = $_data
end
##}}}

##{{{  st40_set_clockgen_a_pll2
define st40_set_clockgen_a_pll2
  ## Stop CLOCKGENA_PLL2...
  set $_data = *$CLOCKGENA_PLL2CR
  set $_data = $_data & 0xbffffffff
  set *$CLOCKGENA_PLL2CR = $_data
  set $_data = $_data & 0xdffffffff
  set *$CLOCKGENA_PLL2CR = $_data
  set $_data = $_data & 0xeffffffff
  set *$CLOCKGENA_PLL2CR = $_data

  ## Configure CLOCKGENA_PLL2...
  set *$CLOCKGENA_PLL2CR = (*$CLOCKGENA_PLL2CR & 0xf0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | (0x127 << 19)

  ## Restart CLOCKGENA_PLL2...
  set $_data = *$CLOCKGENA_PLL2CR
  set $_data = $_data | 0x10000000
  set *$CLOCKGENA_PLL2CR = $_data
  set $_data = $_data | 0x20000000
  set *$CLOCKGENA_PLL2CR = $_data
end
##}}}

##{{{  st40_set_clockgen_a_clk4
define st40_set_clockgen_a_clk4
  ## Configure CLOCKGENA_CLK4...
  set *$CLOCKGENA_CLK4CR = (*$CLOCKGENA_CLK4CR & 0xfffffff8) | ($arg0)
end
##}}}

##{{{  st40_set_clockgen_b_pll1
define st40_set_clockgen_b_pll1
  ## Stop CLOCKGENB_PLL1...
  set $_data = *$CLOCKGENB_PLL1CR2
  set $_data = $_data | 0x1
  set *$CLOCKGENB_PLL1CR2 = $_data
  set $_data = $_data | 0x2
  set *$CLOCKGENB_PLL1CR2 = $_data
  set $_data = $_data | 0x4
  set *$CLOCKGENB_PLL1CR2 = $_data
  set $_data = $_data | 0x8
  set *$CLOCKGENB_PLL1CR2 = $_data
  set $_data = $_data & 0xffffffffb
  set *$CLOCKGENB_PLL1CR2 = $_data
  set $_data = $_data & 0xffffffffe
  set *$CLOCKGENB_PLL1CR2 = $_data

  ## Configure CLOCKGENB_PLL1...
  set *$CLOCKGENB_PLL1CR1 = (*$CLOCKGENB_PLL1CR1 & 0xf0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | (0x127 << 19)

  ## Restart CLOCKGENB_PLL1...
  set $_data = *$CLOCKGENB_PLL1CR2
  set $_data = $_data | 0x1
  set *$CLOCKGENB_PLL1CR2 = $_data
  set $_data = $_data | 0x4
  set *$CLOCKGENB_PLL1CR2 = $_data
end
##}}}

##{{{  st40_set_clockgen_b_pll2
define st40_set_clockgen_b_pll2
  ## Stop CLOCKGENB_PLL2...
  set $_data = *$CLOCKGENB_PLL2CR
  set $_data = $_data & 0xbffffffff
  set *$CLOCKGENB_PLL2CR = $_data
  set $_data = $_data & 0xdffffffff
  set *$CLOCKGENB_PLL2CR = $_data
  set $_data = $_data & 0xeffffffff
  set *$CLOCKGENB_PLL2CR = $_data

  ## Configure CLOCKGENB_PLL2...
  set *$CLOCKGENB_PLL2CR = (*$CLOCKGENB_PLL2CR & 0xf0000000) | ($arg0) | ($arg1 << 8) | ($arg2 << 16) | (0x127 << 19)

  ## Restart CLOCKGENB_PLL2...
  set $_data = *$CLOCKGENB_PLL2CR
  set $_data = $_data | 0x10000000
  set *$CLOCKGENB_PLL2CR = $_data
  set $_data = $_data | 0x20000000
  set *$CLOCKGENB_PLL2CR = $_data
end
##}}}

##{{{  st40_set_clockgen_b_clk4
define st40_set_clockgen_b_clk4
  ## Configure CLOCKGENB_CLK4...
  set *$CLOCKGENB_CLK4CR = (*$CLOCKGENB_CLK4CR & 0xfffffff8) | ($arg0)
end
##}}}

##{{{  st40_cpu*bus*mem*per*
##{{{  st40_cpu100bus50mem50per25 (mode 0)
define st40_cpu100bus50mem50per25
  set *$CPG_FRQCR = 0x0e1a
  st40_set_clockgen_a_pll1 0x12 0x86 0x1
  st40_set_clockgen_a_clk4 0x1
end

document st40_cpu100bus50mem50per25
Set the ST40 clock frequencies to reset mode 0
Usage: st40_cpu100bus50mem50per25
end
##}}}

##{{{  st40_cpu133bus88mem88per44 (mode 1)
define st40_cpu133bus88mem88per44
  set *$CPG_FRQCR = 0x0e23
  st40_set_clockgen_a_pll1 0x12 0xb1 0x1
  st40_set_clockgen_a_clk4 0x3
end

document st40_cpu133bus88mem88per44
Set the ST40 clock frequencies to reset mode 1
Usage: st40_cpu133bus88mem88per44
end
##}}}

##{{{  st40_cpu150bus100mem100per50 (mode 2)
define st40_cpu150bus100mem100per50
  set *$CPG_FRQCR = 0x0e13
  st40_set_clockgen_a_pll1 0x12 0x64 0x0
  st40_set_clockgen_a_clk4 0x3
end

document st40_cpu150bus100mem100per50
Set the ST40 clock frequencies to reset mode 2
Usage: st40_cpu150bus100mem100per50
end
##}}}

##{{{  st40_cpu166bus110mem110per55 (mode 3)
define st40_cpu166bus110mem110per55
  set *$CPG_FRQCR = 0x0e13
  st40_set_clockgen_a_pll1 0x14 0x7b 0x0
  st40_set_clockgen_a_clk4 0x3
end

document st40_cpu166bus110mem110per55
Set the ST40 clock frequencies to reset mode 3
Usage: st40_cpu166bus110mem110per55
end
##}}}

##{{{  st40_cpu200bus100mem100per50 (mode 4)
define st40_cpu200bus100mem100per50
  set *$CPG_FRQCR = 0x0e0a
  st40_set_clockgen_a_pll1 0x12 0x86 0x0
  st40_set_clockgen_a_clk4 0x1
end

document st40_cpu200bus100mem100per50
Set the ST40 clock frequencies to reset mode 4
Usage: st40_cpu200bus100mem100per50
end
##}}}

##{{{  st40_cpu250bus125mem125per62 (mode 5)
define st40_cpu250bus125mem125per62
  set *$CPG_FRQCR = 0x0e0a
  st40_set_clockgen_a_pll1 0x12 0xa7 0x0
  st40_set_clockgen_a_clk4 0x1
end

document st40_cpu250bus125mem125per62
Set the ST40 clock frequencies to reset mode 5
Usage: st40_cpu250bus125mem125per62
end
##}}}
##}}}

##{{{  st40_displayclocks
define st40_get_clockgen_x_pll_x_frq
  if ($arg1 == 1)
    set $_data = *$CLOCKGEN$arg0_PLL$arg1CR1
  else
    set $_data = *$CLOCKGEN$arg0_PLL$arg1CR
  end

  set $_mdiv = (double) ($_data & 0xff)
  set $_ndiv = (double) (($_data >> 8) & 0xff)
  set $_pdiv = (double) (1 << (($_data >> 16) & 0x7))

  set $arg3 = ((2.0 * $arg2 * $_ndiv) / $_mdiv) / $_pdiv
end

define st40_displayclocks
  if ($argc > 0)
    set $_extfrq = $arg0
  else
    set $_extfrq = 27.0
  end

  st40_get_clockgen_x_pll_x_frq A 1 $_extfrq $_pll1afrq
  st40_get_clockgen_x_pll_x_frq A 2 $_extfrq $_pll2afrq
  st40_get_clockgen_x_pll_x_frq B 1 $_extfrq $_pll1bfrq
  st40_get_clockgen_x_pll_x_frq B 2 $_extfrq $_pll2bfrq

  set $_mainafrq = $_pll1afrq / 2.0
  set $_mainbfrq = $_pll1bfrq / 2.0

  set $_cpgbypass = *$CLOCKGENA_CPG_BYPASS & 0x1

  if ($_cpgbypass == 0x1)
    set $_data = *$CLOCKGENA_CLK1CR & 0x7

    if ($_data == 0x0)
      set $_cpufrq = $_mainafrq
    end
    if ($_data == 0x1)
      set $_cpufrq = $_mainafrq / 2.0
    end
    if ($_data == 0x2)
      set $_cpufrq = $_mainafrq / 3.0
    end
    if ($_data == 0x3)
      set $_cpufrq = $_mainafrq * 2.0 / 3.0
    end
    if ($_data == 0x4)
      set $_cpufrq = $_mainafrq / 4.0
    end
    if ($_data == 0x5)
      set $_cpufrq = $_mainafrq / 6.0
    end
    if ($_data == 0x6)
      set $_cpufrq = $_mainafrq / 8.0
    end
    if ($_data == 0x7)
      set $_cpufrq = $_mainafrq / 8.0
    end

    set $_data = *$CLOCKGENA_CLK2CR & 0x7

    if ($_data == 0x0)
      set $_busfrq = $_mainafrq
    end
    if ($_data == 0x1)
      set $_busfrq = $_mainafrq / 2.0
    end
    if ($_data == 0x2)
      set $_busfrq = $_mainafrq / 3.0
    end
    if ($_data == 0x3)
      set $_busfrq = $_mainafrq * 2.0 / 3.0
    end
    if ($_data == 0x4)
      set $_busfrq = $_mainafrq / 4.0
    end
    if ($_data == 0x5)
      set $_busfrq = $_mainafrq / 6.0
    end
    if ($_data == 0x6)
      set $_busfrq = $_mainafrq / 8.0
    end
    if ($_data == 0x7)
      set $_busfrq = $_mainafrq / 8.0
    end

    set $_data = *$CLOCKGENA_CLK3CR & 0x7

    if ($_data == 0x0)
      set $_perfrq = $_mainafrq
    end
    if ($_data == 0x1)
      set $_perfrq = $_mainafrq / 2.0
    end
    if ($_data == 0x2)
      set $_perfrq = $_mainafrq / 3.0
    end
    if ($_data == 0x3)
      set $_perfrq = $_mainafrq * 2.0 / 3.0
    end
    if ($_data == 0x4)
      set $_perfrq = $_mainafrq / 4.0
    end
    if ($_data == 0x5)
      set $_perfrq = $_mainafrq / 6.0
    end
    if ($_data == 0x6)
      set $_perfrq = $_mainafrq / 8.0
    end
    if ($_data == 0x7)
      set $_perfrq = $_mainafrq / 8.0
    end
  else
    set $_data = *$CPG_FRQCR & 0x1ff

    if ($_data == 0x000)
      set $_cpufrq = $_mainafrq
      set $_busfrq = $_mainafrq
      set $_perfrq = $_mainafrq / 2.0
    else
    if ($_data == 0x008)
      set $_cpufrq = $_mainafrq
      set $_busfrq = $_mainafrq / 2.0
      set $_perfrq = $_mainafrq / 2.0
    else
    if ($_data == 0x00a)
      set $_cpufrq = $_mainafrq
      set $_busfrq = $_mainafrq / 2.0
      set $_perfrq = $_mainafrq / 4.0
    else
    if ($_data == 0x013)
      set $_cpufrq = $_mainafrq
      set $_busfrq = $_mainafrq * 2.0 / 3.0
      set $_perfrq = $_mainafrq / 3.0
    else
    if ($_data == 0x01a)
      set $_cpufrq = $_mainafrq
      set $_busfrq = $_mainafrq / 2.0
      set $_perfrq = $_mainafrq / 4.0
    else
    if ($_data == 0x023)
      set $_cpufrq = $_mainafrq
      set $_busfrq = $_mainafrq * 2.0 / 3.0
      set $_perfrq = $_mainafrq / 3.0
    else
    if ($_data == 0x048)
      set $_cpufrq = $_mainafrq / 2.0
      set $_busfrq = $_mainafrq / 2.0
      set $_perfrq = $_mainafrq / 4.0
    else
    if ($_data == 0x063)
      set $_cpufrq = $_mainafrq / 2.0
      set $_busfrq = $_mainafrq / 4.0
      set $_perfrq = $_mainafrq / 4.0
    else
    if ($_data == 0x091)
      set $_cpufrq = $_mainafrq / 3.0
      set $_busfrq = $_mainafrq / 3.0
      set $_perfrq = $_mainafrq / 6.0
    else
    if ($_data == 0x0da)
      set $_cpufrq = $_mainafrq / 4.0
      set $_busfrq = $_mainafrq / 4.0
      set $_perfrq = $_mainafrq / 8.0
    else
      printf "unknown value for CPG_FRQCR\n"
    end
    end
    end
    end
    end
    end
    end
    end
    end
    end
  end

  set $_data = *$EMI_FLASHCLKSEL & 0x3

  if ($_data == 0x0)
    set $_emiflashfrq = $_busfrq
  end
  if ($_data == 0x1)
    set $_emiflashfrq = $_busfrq / 2.0
  end
  if ($_data == 0x2)
    set $_emiflashfrq = $_busfrq / 3.0
  end

  set $_data = *$EMI_SDRAMCLKSEL & 0x3

  if ($_data == 0x0)
    set $_emisdramfrq = $_busfrq
  end
  if ($_data == 0x1)
    set $_emisdramfrq = $_busfrq / 2.0
  end
  if ($_data == 0x2)
    set $_emisdramfrq = $_busfrq / 3.0
  end

  set $_data = *$CLOCKGENA_CLK4CR & 0x7

  if ($_data == 0x0)
    set $_lmifrq = $_mainafrq
  end
  if ($_data == 0x1)
    set $_lmifrq = $_mainafrq / 2.0
  end
  if ($_data == 0x2)
    set $_lmifrq = $_mainafrq / 3.0
  end
  if ($_data == 0x3)
    set $_lmifrq = $_mainafrq * 2.0 / 3.0
  end
  if ($_data == 0x4)
    set $_lmifrq = $_mainafrq / 4.0
  end
  if ($_data == 0x5)
    set $_lmifrq = $_mainafrq / 6.0
  end
  if ($_data == 0x6)
    set $_lmifrq = $_mainafrq / 8.0
  end
  if ($_data == 0x7)
    set $_lmifrq = $_mainafrq / 8.0
  end

  set $_data = *$CLOCKGENA_PLL2_MUXCR & 0x3

  if ($_data == 0x0)
    set $_pcifrq = $_pll2afrq / 8.0
  end
  if ($_data == 0x1)
    set $_pcifrq = $_pll2afrq / 16.0
  end
  if ($_data == 0x2)
    set $_pcifrq = $_pll2afrq / 21.0
  end
  if ($_data == 0x3)
    set $_pcifrq = $_pll2afrq
  end

  printf "Clock settings:\n"
  printf "\n"
  printf "  CPG_FRQCR = 0x%04x\n", *$CPG_FRQCR
  printf "\n"
  printf "  CLOCKGENA_CPG_BYPASS = 0x%08x\n", *$CLOCKGENA_CPG_BYPASS
  printf "\n"
  printf "  CLOCKGENA_PLL1CR1    = 0x%08x\n", *$CLOCKGENA_PLL1CR1
  printf "  CLOCKGENA_PLL1CR2    = 0x%08x\n", *$CLOCKGENA_PLL1CR2
  printf "  CLOCKGENA_PLL2CR     = 0x%08x\n", *$CLOCKGENA_PLL2CR
  printf "  CLOCKGENA_CLK1CR     = 0x%08x\n", *$CLOCKGENA_CLK1CR
  printf "  CLOCKGENA_CLK2CR     = 0x%08x\n", *$CLOCKGENA_CLK2CR
  printf "  CLOCKGENA_CLK3CR     = 0x%08x\n", *$CLOCKGENA_CLK3CR
  printf "  CLOCKGENA_CLK4CR     = 0x%08x\n", *$CLOCKGENA_CLK4CR
  printf "  CLOCKGENA_PLL2_MUXCR = 0x%08x\n", *$CLOCKGENA_PLL2_MUXCR
  printf "\n"
  printf "  CLOCKGENB_CPG_BYPASS = 0x%08x\n", *$CLOCKGENB_CPG_BYPASS
  printf "\n"
  printf "  CLOCKGENB_PLL1CR1    = 0x%08x\n", *$CLOCKGENB_PLL1CR1
  printf "  CLOCKGENB_PLL1CR2    = 0x%08x\n", *$CLOCKGENB_PLL1CR2
  printf "  CLOCKGENB_PLL2CR     = 0x%08x\n", *$CLOCKGENB_PLL2CR
  printf "  CLOCKGENB_CLK1CR     = 0x%08x\n", *$CLOCKGENB_CLK1CR
  printf "  CLOCKGENB_CLK2CR     = 0x%08x\n", *$CLOCKGENB_CLK2CR
  printf "  CLOCKGENB_CLK3CR     = 0x%08x\n", *$CLOCKGENB_CLK3CR
  printf "  CLOCKGENB_CLK4CR     = 0x%08x\n", *$CLOCKGENB_CLK4CR
  printf "  CLOCKGENB_PLL2_MUXCR = 0x%08x\n", *$CLOCKGENB_PLL2_MUXCR
  printf "\n"
  printf "Clock A frequencies\n"
  printf "\n"
  printf "  PLL1        = %.1f MHz\n", $_pll1afrq
  printf "  PLL2        = %.1f MHz\n", $_pll2afrq
  printf "  ST40 CPU    = %.1f MHz\n", $_cpufrq
  printf "  ST40 PER    = %.1f MHz\n", $_perfrq
  printf "  ST BUS      = %.1f MHz\n", $_busfrq
  printf "  EMI         = %.1f MHz\n", $_busfrq
  printf "  EMI (FLASH) = %.1f MHz\n", $_emiflashfrq
  printf "  EMI (SDRAM) = %.1f MHz\n", $_emisdramfrq
  printf "  LMI         = %.1f MHz\n", $_lmifrq
  printf "  PCI         = %.1f MHz\n", $_pcifrq
  printf "\n"
  printf "Clock B frequencies\n"
  printf "\n"
  printf "  PLL1        = %.1f MHz\n", $_pll1bfrq
  printf "  PLL2        = %.1f MHz\n", $_pll2bfrq
end

document st40_displayclocks
Display the ST40 clock frequencies
Usage: st40_displayclocks [<frequency>]
where <frequency> is optional and is the external clock frequency (default: 27MHz)
end
##}}}
