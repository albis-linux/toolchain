################################################################################

define hdk7105sim
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7105_fsim_setup hdk7105sim_setup $arg0
  else
    connectsh4simle hdk7105_fsim_setup hdk7105sim_setup ""
  end
end

document hdk7105sim
Connect to and configure a simulated STi7105-HDK board
Usage: hdk7105sim
end

define hdk7105simse
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7105se_fsim_setup hdk7105simse_setup $arg0
  else
    connectsh4simle hdk7105se_fsim_setup hdk7105simse_setup ""
  end
end

document hdk7105simse
Connect to and configure a simulated STi7105-HDK board (STi7105 in 32-bit SE mode)
Usage: hdk7105simse
end

define hdk7105simseuc
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7105se_fsim_setup hdk7105simseuc_setup $arg0
  else
    connectsh4simle hdk7105se_fsim_setup hdk7105simseuc_setup ""
  end
end

document hdk7105simseuc
Connect to and configure a simulated STi7105-HDK board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: hdk7105simseuc
end

define hdk7105simse29
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7105se_fsim_setup hdk7105simse29_setup $arg0
  else
    connectsh4simle hdk7105se_fsim_setup hdk7105simse29_setup ""
  end
end

document hdk7105simse29
Connect to and configure a simulated STi7105-HDK board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk7105simse29
end

################################################################################

define hdk7105psim
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7105_psim_setup hdk7105sim_setup $arg0
  else
    connectsh4psimle hdk7105_psim_setup hdk7105sim_setup ""
  end
end

document hdk7105psim
Connect to and configure a simulated STi7105-HDK board
Usage: hdk7105psim
end

define hdk7105psimse
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7105se_psim_setup hdk7105simse_setup $arg0
  else
    connectsh4psimle hdk7105se_psim_setup hdk7105simse_setup ""
  end
end

document hdk7105psimse
Connect to and configure a simulated STi7105-HDK board (STi7105 in 32-bit SE mode)
Usage: hdk7105psimse
end

define hdk7105psimseuc
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7105se_psim_setup hdk7105simseuc_setup $arg0
  else
    connectsh4psimle hdk7105se_psim_setup hdk7105simseuc_setup ""
  end
end

document hdk7105psimseuc
Connect to and configure a simulated STi7105-HDK board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: hdk7105psimseuc
end

define hdk7105psimse29
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source hdk7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7105se_psim_setup hdk7105simse29_setup $arg0
  else
    connectsh4psimle hdk7105se_psim_setup hdk7105simse29_setup ""
  end
end

document hdk7105psimse29
Connect to and configure a simulated STi7105-HDK board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk7105psimse29
end

################################################################################
