################################################################################

define tmmidr04
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmidr04.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 tmmidr04_setup $arg1
  else
    connectsh4le $arg0 tmmidr04_setup "hardreset"
  end
end

document tmmidr04
Connect to and configure an STm8000-TMM board
Usage: tmmidr04 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define tmmidr04usb
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmidr04.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 tmmidr04_setup $arg1
  else
    connectsh4usble $arg0 tmmidr04_setup "hardreset"
  end
end

document tmmidr04usb
Connect to and configure an STm8000-TMM board
Usage: tmmidr04usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define tmmidr04sim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmidr04.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle tmmidr04_fsim_setup tmmidr04sim_setup $arg0
  else
    connectsh4simle tmmidr04_fsim_setup tmmidr04sim_setup ""
  end
end

document tmmidr04sim
Connect to and configure a simulated STm8000-TMM board
Usage: tmmidr04sim
end

################################################################################

define tmmidr04psim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmidr04.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle tmmidr04_psim_setup tmmidr04sim_setup $arg0
  else
    connectsh4psimle tmmidr04_psim_setup tmmidr04sim_setup ""
  end
end

document tmmidr04psim
Connect to and configure a simulated STm8000-TMM board
Usage: tmmidr04psim
end

################################################################################
