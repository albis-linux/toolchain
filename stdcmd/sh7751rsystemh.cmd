##------------------------------------------------------------------------------
## sh7751rsystemh.cmd - SH7751R Solution Engine for SystemH Board
##------------------------------------------------------------------------------

##{{{  SH7751RSYSTEMH Configure
define sh7751rsystemh_configure
##------------------------------------------------------------------------------
## Phase 1: Configure FRQCR register
##------------------------------------------------------------------------------
  set *$CPG_WTCNT = 0x5a00
  set *$CPG_WTCSR = 0xa567
  set *$CPG_FRQCR = 0x0e0a

##------------------------------------------------------------------------------
## Phase 2: Configure rest...
##------------------------------------------------------------------------------
  set *$BSC_BCR1 = 0x00000008
  set *$BSC_BCR2 = 0x00c0
  set *$BSC_WCR1 = 0x00000000
  set *$BSC_WCR2 = 0x00006000
  set *$BSC_RTCSR = 0xa518
  set *$BSC_RTCNT = 0xa5004
  set *$BSC_RTCOR = 0xa503
  set *$BSC_RFCR = 0xa400
  set *$BSC_MCR = 0x080921f4
  set *($BSC_SDMR3+(0x00c8/sizeof(*$BSC_SDMR3))) = 0x0
  set *$BSC_MCR = 0x480921f4
  set *($BSC_SDMR3+(0x00c8/sizeof(*$BSC_SDMR3))) = 0x0
end
##}}}

##{{{  SH7751RSYSTEMH Memory
define sh7751rsystemh_memory_define
  memory-add Flash 0x00000000 1 ROM
  memory-add RAM   0x0c000000 32 RAM
end

define sh7751rsystemh_sim_memory_define
  sim_addmemory 0x00000000 1 ROM
  sim_addmemory 0x0c000000 32 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define sh7751rsystemh_setup
  sh7751_define
  sh7751rsystemh_memory_define

  sh7751_si_regs

  sh7751rsystemh_configure

  set *$CCN_CCR = 0x8000090d
end

document sh7751rsystemh_setup
Configure an SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemh_setup
end

define sh7751rsystemhsim_setup
  sh7751_define
  sh7751rsystemh_memory_define

  sh7751_si_regs

  set *$CCN_CCR = 0x8000090d
end

document sh7751rsystemhsim_setup
Configure a simulated SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemhsim_setup
end

define sh7751rsystemh_fsim_setup
  sh7751r_fsim_core_setup
  sh7751rsystemh_sim_memory_define
end

document sh7751rsystemh_fsim_setup
Configure functional simulator for SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemh_fsim_setup
end

define sh7751rsystemh_psim_setup
  sh7751r_psim_core_setup
  sh7751rsystemh_sim_memory_define
end

document sh7751rsystemh_psim_setup
Configure performance simulator for SH7751R Solution Engine for SystemH board
Usage: sh7751rsystemh_psim_setup
end

define sh7751rsystemh_display_registers
  sh7751_display_si_regs
end

document sh7751rsystemh_display_registers
Display the SH7751R configuration registers
Usage: sh7751rsystemh_display_registers
end
