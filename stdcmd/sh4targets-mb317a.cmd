################################################################################

define mb317a
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb317a_setup $arg1
  else
    connectsh4le $arg0 mb317a_setup "hardreset"
  end
end

document mb317a
Connect to and configure an ST40GX1 Evaluation board, revision A
Usage: mb317a <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb317ausb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb317a_setup $arg1
  else
    connectsh4usble $arg0 mb317a_setup "hardreset"
  end
end

document mb317ausb
Connect to and configure an ST40GX1 Evaluation board, revision A
Usage: mb317ausb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb317asim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb317a_fsim_setup mb317asim_setup $arg0
  else
    connectsh4simle mb317a_fsim_setup mb317asim_setup ""
  end
end

document mb317asim
Connect to and configure a simulated ST40GX1 Evaluation board, revision A
Usage: mb317asim
end

################################################################################

define mb317apsim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40gx1.cmd
  source mb317.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb317a_psim_setup mb317asim_setup $arg0
  else
    connectsh4psimle mb317a_psim_setup mb317asim_setup ""
  end
end

document mb317apsim
Connect to and configure a simulated ST40GX1 Evaluation board, revision A
Usage: mb317apsim
end

################################################################################
