################################################################################

source sh4targets-mb442stb7100.cmd

################################################################################

define mb442stb7100bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100bypass $arg0 $arg1
  else
    mb442stb7100bypass $arg0
  end
end

define mb442stb7100stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100stmmx $arg0 $arg1
  else
    mb442stb7100stmmx $arg0
  end
end

################################################################################

define mb442bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442bypass $arg0 $arg1
  else
    mb442bypass $arg0
  end
end

define mb442stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stmmx $arg0 $arg1
  else
    mb442stmmx $arg0
  end
end

################################################################################

define stb7100refbypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refbypass $arg0 $arg1
  else
    stb7100refbypass $arg0
  end
end

define stb7100refstmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refstmmx $arg0 $arg1
  else
    stb7100refstmmx $arg0
  end
end

################################################################################

define mb442stb7100bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100bypass $arg0 $arg1
  else
    mb442stb7100bypass $arg0
  end
end

define mb442stb7100stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100stmmx $arg0 $arg1
  else
    mb442stb7100stmmx $arg0
  end
end

################################################################################

define mb442bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442bypass $arg0 $arg1
  else
    mb442bypass $arg0
  end
end

define mb442stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stmmx $arg0 $arg1
  else
    mb442stmmx $arg0
  end
end

################################################################################

define stb7100refbypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refbypass $arg0 $arg1
  else
    stb7100refbypass $arg0
  end
end

define stb7100refstmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refstmmx $arg0 $arg1
  else
    stb7100refstmmx $arg0
  end
end

################################################################################

source sh4targets-mb442stb7100cutX.cmd

################################################################################

define mb442stb7100cut11bypass
  if ($argc > 1)
    mb442stb7100cutXbypass 11 $arg0 $arg1
  else
    mb442stb7100cutXbypass 11 $arg0
  end
end

define mb442stb7100cut11stmmx
  if ($argc > 1)
    mb442stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 11 $arg0
  end
end

define mb442stb7100cut13bypass
  if ($argc > 1)
    mb442stb7100cutXbypass 13 $arg0 $arg1
  else
    mb442stb7100cutXbypass 13 $arg0
  end
end

define mb442stb7100cut13stmmx
  if ($argc > 1)
    mb442stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 13 $arg0
  end
end

define mb442stb7100cut20bypass
  if ($argc > 1)
    mb442stb7100cutXbypass 20 $arg0 $arg1
  else
    mb442stb7100cutXbypass 20 $arg0
  end
end

define mb442stb7100cut20stmmx
  if ($argc > 1)
    mb442stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 20 $arg0
  end
end

define mb442stb7100cut31bypass
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb442cut11bypass
  if ($argc > 1)
    mb442cutXbypass 11 $arg0 $arg1
  else
    mb442cutXbypass 11 $arg0
  end
end

define mb442cut11stmmx
  if ($argc > 1)
    mb442cutXstmmx 11 $arg0 $arg1
  else
    mb442cutXstmmx 11 $arg0
  end
end

define mb442cut13bypass
  if ($argc > 1)
    mb442cutXbypass 13 $arg0 $arg1
  else
    mb442cutXbypass 13 $arg0
  end
end

define mb442cut13stmmx
  if ($argc > 1)
    mb442cutXstmmx 13 $arg0 $arg1
  else
    mb442cutXstmmx 13 $arg0
  end
end

define mb442cut20bypass
  if ($argc > 1)
    mb442cutXbypass 20 $arg0 $arg1
  else
    mb442cutXbypass 20 $arg0
  end
end

define mb442cut20stmmx
  if ($argc > 1)
    mb442cutXstmmx 20 $arg0 $arg1
  else
    mb442cutXstmmx 20 $arg0
  end
end

define mb442cut31bypass
  if ($argc > 1)
    mb442cutXbypass 31 $arg0 $arg1
  else
    mb442cutXbypass 31 $arg0
  end
end

define mb442cut31stmmx
  if ($argc > 1)
    mb442cutXstmmx 31 $arg0 $arg1
  else
    mb442cutXstmmx 31 $arg0
  end
end

################################################################################

define stb7100refcut11bypass
  if ($argc > 1)
    stb7100refcutXbypass 11 $arg0 $arg1
  else
    stb7100refcutXbypass 11 $arg0
  end
end

define stb7100refcut11stmmx
  if ($argc > 1)
    stb7100refcutXstmmx 11 $arg0 $arg1
  else
    stb7100refcutXstmmx 11 $arg0
  end
end

define stb7100refcut13bypass
  if ($argc > 1)
    stb7100refcutXbypass 13 $arg0 $arg1
  else
    stb7100refcutXbypass 13 $arg0
  end
end

define stb7100refcut13stmmx
  if ($argc > 1)
    stb7100refcutXstmmx 13 $arg0 $arg1
  else
    stb7100refcutXstmmx 13 $arg0
  end
end

define stb7100refcut20bypass
  if ($argc > 1)
    stb7100refcutXbypass 20 $arg0 $arg1
  else
    stb7100refcutXbypass 20 $arg0
  end
end

define stb7100refcut20stmmx
  if ($argc > 1)
    stb7100refcutXstmmx 20 $arg0 $arg1
  else
    stb7100refcutXstmmx 20 $arg0
  end
end

define stb7100refcut31bypass
  if ($argc > 1)
    stb7100refcutXbypass 31 $arg0 $arg1
  else
    stb7100refcutXbypass 31 $arg0
  end
end

define stb7100refcut31stmmx
  if ($argc > 1)
    stb7100refcutXstmmx 31 $arg0 $arg1
  else
    stb7100refcutXstmmx 31 $arg0
  end
end

################################################################################

define mb442stb7100cut11bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 11 $arg0 $arg1
  else
    mb442stb7100cutXbypass 11 $arg0
  end
end

define mb442stb7100cut11stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 11 $arg0
  end
end

define mb442stb7100cut13bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 13 $arg0 $arg1
  else
    mb442stb7100cutXbypass 13 $arg0
  end
end

define mb442stb7100cut13stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 13 $arg0
  end
end

define mb442stb7100cut20bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 20 $arg0 $arg1
  else
    mb442stb7100cutXbypass 20 $arg0
  end
end

define mb442stb7100cut20stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 20 $arg0
  end
end

define mb442stb7100cut31bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

define mb442stb7100cut31bypass27MHz128MB
  set $_mb442stb7100extclk = 27
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx27MHz128MB
  set $_mb442stb7100extclk = 27
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb442cut11bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 11 $arg0 $arg1
  else
    mb442cutXbypass 11 $arg0
  end
end

define mb442cut11stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 11 $arg0 $arg1
  else
    mb442cutXstmmx 11 $arg0
  end
end

define mb442cut13bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 13 $arg0 $arg1
  else
    mb442cutXbypass 13 $arg0
  end
end

define mb442cut13stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 13 $arg0 $arg1
  else
    mb442cutXstmmx 13 $arg0
  end
end

define mb442cut20bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 20 $arg0 $arg1
  else
    mb442cutXbypass 20 $arg0
  end
end

define mb442cut20stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 20 $arg0 $arg1
  else
    mb442cutXstmmx 20 $arg0
  end
end

define mb442cut31bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXbypass 31 $arg0 $arg1
  else
    mb442cutXbypass 31 $arg0
  end
end

define mb442cut31stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    mb442cutXstmmx 31 $arg0 $arg1
  else
    mb442cutXstmmx 31 $arg0
  end
end

################################################################################

define stb7100refcut11bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 11 $arg0 $arg1
  else
    stb7100refcutXbypass 11 $arg0
  end
end

define stb7100refcut11stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 11 $arg0 $arg1
  else
    stb7100refcutXstmmx 11 $arg0
  end
end

define stb7100refcut13bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 13 $arg0 $arg1
  else
    stb7100refcutXbypass 13 $arg0
  end
end

define stb7100refcut13stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 13 $arg0 $arg1
  else
    stb7100refcutXstmmx 13 $arg0
  end
end

define stb7100refcut20bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 20 $arg0 $arg1
  else
    stb7100refcutXbypass 20 $arg0
  end
end

define stb7100refcut20stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 20 $arg0 $arg1
  else
    stb7100refcutXstmmx 20 $arg0
  end
end

define stb7100refcut31bypass27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXbypass 31 $arg0 $arg1
  else
    stb7100refcutXbypass 31 $arg0
  end
end

define stb7100refcut31stmmx27MHz
  set $_mb442stb7100extclk = 27
  if ($argc > 1)
    stb7100refcutXstmmx 31 $arg0 $arg1
  else
    stb7100refcutXstmmx 31 $arg0
  end
end

################################################################################

define mb442stb7100cut11bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 11 $arg0 $arg1
  else
    mb442stb7100cutXbypass 11 $arg0
  end
end

define mb442stb7100cut11stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 11 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 11 $arg0
  end
end

define mb442stb7100cut13bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 13 $arg0 $arg1
  else
    mb442stb7100cutXbypass 13 $arg0
  end
end

define mb442stb7100cut13stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 13 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 13 $arg0
  end
end

define mb442stb7100cut20bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 20 $arg0 $arg1
  else
    mb442stb7100cutXbypass 20 $arg0
  end
end

define mb442stb7100cut20stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 20 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 20 $arg0
  end
end

define mb442stb7100cut31bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

define mb442stb7100cut31bypass30MHz128MB
  set $_mb442stb7100extclk = 30
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXbypass 31 $arg0 $arg1
  else
    mb442stb7100cutXbypass 31 $arg0
  end
end

define mb442stb7100cut31stmmx30MHz128MB
  set $_mb442stb7100extclk = 30
  set $_mb442stb7109sys128 = 1
  if ($argc > 1)
    mb442stb7100cutXstmmx 31 $arg0 $arg1
  else
    mb442stb7100cutXstmmx 31 $arg0
  end
end

################################################################################

define mb442cut11bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 11 $arg0 $arg1
  else
    mb442cutXbypass 11 $arg0
  end
end

define mb442cut11stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 11 $arg0 $arg1
  else
    mb442cutXstmmx 11 $arg0
  end
end

define mb442cut13bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 13 $arg0 $arg1
  else
    mb442cutXbypass 13 $arg0
  end
end

define mb442cut13stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 13 $arg0 $arg1
  else
    mb442cutXstmmx 13 $arg0
  end
end

define mb442cut20bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 20 $arg0 $arg1
  else
    mb442cutXbypass 20 $arg0
  end
end

define mb442cut20stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 20 $arg0 $arg1
  else
    mb442cutXstmmx 20 $arg0
  end
end

define mb442cut31bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXbypass 31 $arg0 $arg1
  else
    mb442cutXbypass 31 $arg0
  end
end

define mb442cut31stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    mb442cutXstmmx 31 $arg0 $arg1
  else
    mb442cutXstmmx 31 $arg0
  end
end

################################################################################

define stb7100refcut11bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 11 $arg0 $arg1
  else
    stb7100refcutXbypass 11 $arg0
  end
end

define stb7100refcut11stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 11 $arg0 $arg1
  else
    stb7100refcutXstmmx 11 $arg0
  end
end

define stb7100refcut13bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 13 $arg0 $arg1
  else
    stb7100refcutXbypass 13 $arg0
  end
end

define stb7100refcut13stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 13 $arg0 $arg1
  else
    stb7100refcutXstmmx 13 $arg0
  end
end

define stb7100refcut20bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 20 $arg0 $arg1
  else
    stb7100refcutXbypass 20 $arg0
  end
end

define stb7100refcut20stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 20 $arg0 $arg1
  else
    stb7100refcutXstmmx 20 $arg0
  end
end

define stb7100refcut31bypass30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXbypass 31 $arg0 $arg1
  else
    stb7100refcutXbypass 31 $arg0
  end
end

define stb7100refcut31stmmx30MHz
  set $_mb442stb7100extclk = 30
  if ($argc > 1)
    stb7100refcutXstmmx 31 $arg0 $arg1
  else
    stb7100refcutXstmmx 31 $arg0
  end
end

################################################################################
