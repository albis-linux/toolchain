################################################################################

define sh4202microdevbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh4202microdev_setup $arg1
  else
    connectsh4be $arg0 sh4202microdev_setup ""
  end
end

document sh4202microdevbe
Connect to and configure an SH4202 Micro Development SoC board (big endian)
Usage: sh4202microdevbe <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdevle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh4202microdev_setup $arg1
  else
    connectsh4le $arg0 sh4202microdev_setup ""
  end
end

document sh4202microdevle
Connect to and configure an SH4202 Micro Development SoC board (little endian)
Usage: sh4202microdevle <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdev
  if ($argc > 1)
    sh4202microdevle $arg0 $arg1
  else
    sh4202microdevle $arg0
  end
end

document sh4202microdev
Connect to and configure an SH4202 Micro Development SoC board
Usage: sh4202microdev <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh4202microdevsebe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh4202microdevse_setup $arg1
  else
    connectsh4be $arg0 sh4202microdevse_setup ""
  end
end

document sh4202microdevsebe
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, big endian)
Usage: sh4202microdevsebe <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdevsele
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh4202microdevse_setup $arg1
  else
    connectsh4le $arg0 sh4202microdevse_setup ""
  end
end

document sh4202microdevsele
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, little endian)
Usage: sh4202microdevsele <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdevse
  if ($argc > 1)
    sh4202microdevsele $arg0 $arg1
  else
    sh4202microdevsele $arg0
  end
end

document sh4202microdevse
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode)
Usage: sh4202microdevse <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh4202microdevseucbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh4202microdevseuc_setup $arg1
  else
    connectsh4be $arg0 sh4202microdevseuc_setup ""
  end
end

document sh4202microdevseucbe
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, big endian)
Usage: sh4202microdevseucbe <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdevseucle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh4202microdevseuc_setup $arg1
  else
    connectsh4le $arg0 sh4202microdevseuc_setup ""
  end
end

document sh4202microdevseucle
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, little endian)
Usage: sh4202microdevseucle <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdevseuc
  if ($argc > 1)
    sh4202microdevseucle $arg0 $arg1
  else
    sh4202microdevseucle $arg0
  end
end

document sh4202microdevseuc
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings)
Usage: sh4202microdevseuc <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh4202microdevse29be
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh4202microdevse29_setup $arg1
  else
    connectsh4be $arg0 sh4202microdevse29_setup ""
  end
end

document sh4202microdevse29be
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, big endian)
Usage: sh4202microdevse29be <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdevse29le
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh4202microdevse29_setup $arg1
  else
    connectsh4le $arg0 sh4202microdevse29_setup ""
  end
end

document sh4202microdevse29le
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, little endian)
Usage: sh4202microdevse29le <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202microdevse29
  if ($argc > 1)
    sh4202microdevse29le $arg0 $arg1
  else
    sh4202microdevse29le $arg0
  end
end

document sh4202microdevse29
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sh4202microdevse29 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh4202microdevusbbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh4202microdev_setup $arg1
  else
    connectsh4usbbe $arg0 sh4202microdev_setup ""
  end
end

document sh4202microdevusbbe
Connect to and configure an SH4202 Micro Development SoC board (big endian)
Usage: sh4202microdevusbbe <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusble
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh4202microdev_setup $arg1
  else
    connectsh4usble $arg0 sh4202microdev_setup ""
  end
end

document sh4202microdevusble
Connect to and configure an SH4202 Micro Development SoC board (little endian)
Usage: sh4202microdevusble <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusb
  if ($argc > 1)
    sh4202microdevusble $arg0 $arg1
  else
    sh4202microdevusble $arg0
  end
end

document sh4202microdevusb
Connect to and configure an SH4202 Micro Development SoC board
Usage: sh4202microdevusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh4202microdevusbsebe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh4202microdevse_setup $arg1
  else
    connectsh4usbbe $arg0 sh4202microdevse_setup ""
  end
end

document sh4202microdevusbsebe
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, big endian)
Usage: sh4202microdevusbsebe <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusbsele
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh4202microdevse_setup $arg1
  else
    connectsh4usble $arg0 sh4202microdevse_setup ""
  end
end

document sh4202microdevusbsele
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, little endian)
Usage: sh4202microdevusbsele <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusbse
  if ($argc > 1)
    sh4202microdevusbsele $arg0 $arg1
  else
    sh4202microdevusbsele $arg0
  end
end

document sh4202microdevusbse
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode)
Usage: sh4202microdevusbse <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh4202microdevusbseucbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh4202microdevseuc_setup $arg1
  else
    connectsh4usbbe $arg0 sh4202microdevseuc_setup ""
  end
end

document sh4202microdevusbseucbe
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, big endian)
Usage: sh4202microdevusbseucbe <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusbseucle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh4202microdevseuc_setup $arg1
  else
    connectsh4usble $arg0 sh4202microdevseuc_setup ""
  end
end

document sh4202microdevusbseucle
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, little endian)
Usage: sh4202microdevusbseucle <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusbseuc
  if ($argc > 1)
    sh4202microdevusbseucle $arg0 $arg1
  else
    sh4202microdevusbseucle $arg0
  end
end

document sh4202microdevusbseuc
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings)
Usage: sh4202microdevusbseuc <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh4202microdevusbse29be
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh4202microdevse29_setup $arg1
  else
    connectsh4usbbe $arg0 sh4202microdevse29_setup ""
  end
end

document sh4202microdevusbse29be
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, big endian)
Usage: sh4202microdevusbse29be <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusbse29le
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh4202microdevse29_setup $arg1
  else
    connectsh4usble $arg0 sh4202microdevse29_setup ""
  end
end

document sh4202microdevusbse29le
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, little endian)
Usage: sh4202microdevusbse29le <target>
where <target> is an ST Micro Connect USB name
end

define sh4202microdevusbse29
  if ($argc > 1)
    sh4202microdevusbse29le $arg0 $arg1
  else
    sh4202microdevusbse29le $arg0
  end
end

document sh4202microdevusbse29
Connect to and configure an SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sh4202microdevusbse29 <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh4202microdevsimbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsim_setup $arg0
  else
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsim_setup ""
  end
end

document sh4202microdevsimbe
Connect to and configure a simulated SH4202 Micro Development SoC board (big endian)
Usage: sh4202microdevsimbe
end

define sh4202microdevsimle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsim_setup $arg0
  else
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsim_setup ""
  end
end

document sh4202microdevsimle
Connect to and configure a simulated SH4202 Micro Development SoC board (little endian)
Usage: sh4202microdevsimle
end

define sh4202microdevsim
  if ($argc > 0)
    sh4202microdevsimle $arg0
  else
    sh4202microdevsimle
  end
end

document sh4202microdevsim
Connect to and configure a simulated SH4202 Micro Development SoC board
Usage: sh4202microdevsim
end

################################################################################

define sh4202microdevsimsebe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsimse_setup $arg0
  else
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsimse_setup ""
  end
end

document sh4202microdevsimsebe
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, big endian)
Usage: sh4202microdevsimsebe
end

define sh4202microdevsimsele
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsimse_setup $arg0
  else
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsimse_setup ""
  end
end

document sh4202microdevsimsele
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, little endian)
Usage: sh4202microdevsimsele
end

define sh4202microdevsimse
  if ($argc > 0)
    sh4202microdevsimsele $arg0
  else
    sh4202microdevsimsele
  end
end

document sh4202microdevsimse
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode)
Usage: sh4202microdevsimse
end

################################################################################

define sh4202microdevsimseucbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsimseuc_setup $arg0
  else
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsimseuc_setup ""
  end
end

document sh4202microdevsimseucbe
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, big endian)
Usage: sh4202microdevsimseucbe
end

define sh4202microdevsimseucle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsimseuc_setup $arg0
  else
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsimseuc_setup ""
  end
end

document sh4202microdevsimseucle
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, little endian)
Usage: sh4202microdevsimseucle
end

define sh4202microdevsimseuc
  if ($argc > 0)
    sh4202microdevsimseucle $arg0
  else
    sh4202microdevsimseucle
  end
end

document sh4202microdevsimseuc
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings)
Usage: sh4202microdevsimseuc
end

################################################################################

define sh4202microdevsimse29be
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsimse29_setup $arg0
  else
    connectsh4simbe sh4202microdev_fsim_setup sh4202microdevsimse29_setup ""
  end
end

document sh4202microdevsimse29be
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, big endian)
Usage: sh4202microdevsimse29be
end

define sh4202microdevsimse29le
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsimse29_setup $arg0
  else
    connectsh4simle sh4202microdev_fsim_setup sh4202microdevsimse29_setup ""
  end
end

document sh4202microdevsimse29le
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, little endian)
Usage: sh4202microdevsimse29le
end

define sh4202microdevsimse29
  if ($argc > 0)
    sh4202microdevsimse29le $arg0
  else
    sh4202microdevsimse29le
  end
end

document sh4202microdevsimse29
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sh4202microdevsimse29
end

################################################################################

define sh4202microdevpsimbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsim_setup $arg0
  else
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsim_setup ""
  end
end

document sh4202microdevpsimbe
Connect to and configure a simulated SH4202 Micro Development SoC board (big endian)
Usage: sh4202microdevpsimbe
end

define sh4202microdevpsimle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsim_setup $arg0
  else
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsim_setup ""
  end
end

document sh4202microdevpsimle
Connect to and configure a simulated SH4202 Micro Development SoC board (little endian)
Usage: sh4202microdevpsimle
end

define sh4202microdevpsim
  if ($argc > 0)
    sh4202microdevpsimle $arg0
  else
    sh4202microdevpsimle
  end
end

document sh4202microdevpsim
Connect to and configure a simulated SH4202 Micro Development SoC board
Usage: sh4202microdevpsim
end

################################################################################

define sh4202microdevpsimsebe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsimse_setup $arg0
  else
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsimse_setup ""
  end
end

document sh4202microdevpsimsebe
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, big endian)
Usage: sh4202microdevpsimsebe
end

define sh4202microdevpsimsele
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsimse_setup $arg0
  else
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsimse_setup ""
  end
end

document sh4202microdevpsimsele
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode, little endian)
Usage: sh4202microdevpsimsele
end

define sh4202microdevpsimse
  if ($argc > 0)
    sh4202microdevpsimsele $arg0
  else
    sh4202microdevpsimsele
  end
end

document sh4202microdevpsimse
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode)
Usage: sh4202microdevpsimse
end

################################################################################

define sh4202microdevpsimseucbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsimseuc_setup $arg0
  else
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsimseuc_setup ""
  end
end

document sh4202microdevpsimseucbe
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, big endian)
Usage: sh4202microdevpsimseucbe
end

define sh4202microdevpsimseucle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsimseuc_setup $arg0
  else
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsimseuc_setup ""
  end
end

document sh4202microdevpsimseucle
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings, little endian)
Usage: sh4202microdevpsimseucle
end

define sh4202microdevpsimseuc
  if ($argc > 0)
    sh4202microdevpsimseucle $arg0
  else
    sh4202microdevpsimseucle
  end
end

document sh4202microdevpsimseuc
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with uncached mappings)
Usage: sh4202microdevpsimseuc
end

################################################################################

define sh4202microdevpsimse29be
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsimse29_setup $arg0
  else
    connectsh4psimbe sh4202microdev_psim_setup sh4202microdevsimse29_setup ""
  end
end

document sh4202microdevpsimse29be
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, big endian)
Usage: sh4202microdevpsimse29be
end

define sh4202microdevpsimse29le
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202microdev.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsimse29_setup $arg0
  else
    connectsh4psimle sh4202microdev_psim_setup sh4202microdevsimse29_setup ""
  end
end

document sh4202microdevpsimse29le
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2, little endian)
Usage: sh4202microdevpsimse29le
end

define sh4202microdevpsimse29
  if ($argc > 0)
    sh4202microdevpsimse29le $arg0
  else
    sh4202microdevpsimse29le
  end
end

document sh4202microdevpsimse29
Connect to and configure a simulated SH4202 Micro Development SoC board (SH4202 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: sh4202microdevpsimse29
end

################################################################################
