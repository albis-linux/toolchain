################################################################################

define sh4stmc1tpbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4stmc1tpbe $arg0 $arg1
  else
    connectsh4stmc1tpbe $arg0 ""
  end
end

document sh4stmc1tpbe
Connect to an SH4 target (big endian)
Usage: sh4stmc1tpbe <target>
where <target> is an ST Micro Connect 1 target definition
end

define sh4stmc1tple
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4stmc1tple $arg0 $arg1
  else
    connectsh4stmc1tple $arg0 ""
  end
end

document sh4stmc1tple
Connect to an SH4 target (little endian)
Usage: sh4stmc1tple <target>
where <target> is an ST Micro Connect 1 target definition
end

define sh4stmc1tp
  if ($argc > 1)
    sh4stmc1tple $arg0 $arg1
  else
    sh4stmc1tple $arg0
  end
end

document sh4stmc1tp
Connect to an SH4 target
Usage: sh4stmc1tp <target>
where <target> is an ST Micro Connect 1 target definition
end

################################################################################

define st40300stmc1tpbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300stmc1tpbe $arg0 $arg1
  else
    connectst40300stmc1tpbe $arg0 ""
  end
end

document st40300stmc1tpbe
Connect to an ST40-300 target (big endian)
Usage: st40300stmc1tpbe <target>
where <target> is an ST Micro Connect 1 target definition
end

define st40300stmc1tple
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300stmc1tple $arg0 $arg1
  else
    connectst40300stmc1tple $arg0 ""
  end
end

document st40300stmc1tple
Connect to an ST40-300 target (little endian)
Usage: st40300stmc1tple <target>
where <target> is an ST Micro Connect 1 target definition
end

define st40300stmc1tp
  if ($argc > 1)
    st40300stmc1tple $arg0 $arg1
  else
    st40300stmc1tple $arg0
  end
end

document st40300stmc1tp
Connect to an ST40-300 target
Usage: st40300stmc1tp <target>
where <target> is an ST Micro Connect 1 target definition
end

################################################################################

define sh4stmc1tpusbbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4stmc1tpusbbe $arg0 $arg1
  else
    connectsh4stmc1tpusbbe $arg0 ""
  end
end

document sh4stmc1tpusbbe
Connect to an SH4 target (big endian)
Usage: sh4stmc1tpusbbe <target>
where <target> is an ST Micro Connect 1 USB target definition
end

define sh4stmc1tpusble
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4stmc1tpusble $arg0 $arg1
  else
    connectsh4stmc1tpusble $arg0 ""
  end
end

document sh4stmc1tpusble
Connect to an SH4 target (little endian)
Usage: sh4stmc1tpusble <target>
where <target> is an ST Micro Connect 1 USB target definition
end

define sh4stmc1tpusb
  if ($argc > 1)
    sh4stmc1tpusble $arg0 $arg1
  else
    sh4stmc1tpusble $arg0
  end
end

document sh4stmc1tpusb
Connect to an SH4 target
Usage: sh4stmc1tpusb <target>
where <target> is an ST Micro Connect 1 USB target definition
end

################################################################################

define st40300stmc1tpusbbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300stmc1tpusbbe $arg0 $arg1
  else
    connectst40300stmc1tpusbbe $arg0 ""
  end
end

document st40300stmc1tpusbbe
Connect to an ST40-300 target (big endian)
Usage: st40300stmc1tpusbbe <target>
where <target> is an ST Micro Connect 1 USB target definition
end

define st40300stmc1tpusble
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300stmc1tpusble $arg0 $arg1
  else
    connectst40300stmc1tpusble $arg0 ""
  end
end

document st40300stmc1tpusble
Connect to an ST40-300 target (little endian)
Usage: st40300stmc1tpusble <target>
where <target> is an ST Micro Connect 1 USB target definition
end

define st40300stmc1tpusb
  if ($argc > 1)
    st40300stmc1tpusble $arg0 $arg1
  else
    st40300stmc1tpusble $arg0
  end
end

document st40300stmc1tpusb
Connect to an ST40-300 target
Usage: st40300stmc1tpusb <target>
where <target> is an ST Micro Connect 1 USB target definition
end

################################################################################

define sh4stmc2tpbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4stmc2tpbe $arg0 $arg1
  else
    connectsh4stmc2tpbe $arg0 ""
  end
end

document sh4stmc2tpbe
Connect to an SH4 target (big endian)
Usage: sh4stmc2tpbe <target>
where <target> is an ST Micro Connect 2 target definition
end

define sh4stmc2tple
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4stmc2tple $arg0 $arg1
  else
    connectsh4stmc2tple $arg0 ""
  end
end

document sh4stmc2tple
Connect to an SH4 target (little endian)
Usage: sh4stmc2tple <target>
where <target> is an ST Micro Connect 2 target definition
end

define sh4stmc2tp
  if ($argc > 1)
    sh4stmc2tple $arg0 $arg1
  else
    sh4stmc2tple $arg0
  end
end

document sh4stmc2tp
Connect to an SH4 target
Usage: sh4stmc2tp <target>
where <target> is an ST Micro Connect 2 target definition
end

################################################################################

define st40300stmc2tpbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300stmc2tpbe $arg0 $arg1
  else
    connectst40300stmc2tpbe $arg0 ""
  end
end

document st40300stmc2tpbe
Connect to an ST40-300 target (big endian)
Usage: st40300stmc2tpbe <target>
where <target> is an ST Micro Connect 2 target definition
end

define st40300stmc2tple
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300stmc2tple $arg0 $arg1
  else
    connectst40300stmc2tple $arg0 ""
  end
end

document st40300stmc2tple
Connect to an ST40-300 target (little endian)
Usage: st40300stmc2tple <target>
where <target> is an ST Micro Connect 2 target definition
end

define st40300stmc2tp
  if ($argc > 1)
    st40300stmc2tple $arg0 $arg1
  else
    st40300stmc2tple $arg0
  end
end

document st40300stmc2tp
Connect to an ST40-300 target
Usage: st40300stmc2tp <target>
where <target> is an ST Micro Connect 2 target definition
end

################################################################################

define sh4tpbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4autotpbe $arg0 $arg1
  else
    connectsh4autotpbe $arg0 ""
  end
end

document sh4tpbe
Connect to an SH4 target (big endian)
Usage: sh4tpbe <target>
where <target> is an ST Micro Connect target definition
end

define sh4tple
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4autotple $arg0 $arg1
  else
    connectsh4autotple $arg0 ""
  end
end

document sh4tple
Connect to an SH4 target (little endian)
Usage: sh4tple <target>
where <target> is an ST Micro Connect target definition
end

define sh4tp
  if ($argc > 1)
    sh4tple $arg0 $arg1
  else
    sh4tple $arg0
  end
end

document sh4tp
Connect to an SH4 target
Usage: sh4tp <target>
where <target> is an ST Micro Connect target definition
end

################################################################################

define st40300tpbe
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300autotpbe $arg0 $arg1
  else
    connectst40300autotpbe $arg0 ""
  end
end

document st40300tpbe
Connect to an ST40-300 target (big endian)
Usage: st40300tpbe <target>
where <target> is an ST Micro Connect target definition
end

define st40300tple
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300autotple $arg0 $arg1
  else
    connectst40300autotple $arg0 ""
  end
end

document st40300tple
Connect to an ST40-300 target (little endian)
Usage: st40300tple <target>
where <target> is an ST Micro Connect target definition
end

define st40300tp
  if ($argc > 1)
    st40300tple $arg0 $arg1
  else
    st40300tple $arg0
  end
end

document st40300tp
Connect to an ST40-300 target
Usage: st40300tp <target>
where <target> is an ST Micro Connect target definition
end

################################################################################
