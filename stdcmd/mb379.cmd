##------------------------------------------------------------------------------
## mb379.cmd - STm8000-Demo Development Platform MB379
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB379 EMI Configuration
define mb379_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000002
  set *$EMI_BANK2_BASEADDRESS = 0x00000004
  set *$EMI_BANK3_BASEADDRESS = 0x0000000c
  set *$EMI_BANK4_BASEADDRESS = 0x00000014
  set *$EMI_BANK5_BASEADDRESS = 0x0000001c

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 8Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016f1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for on-board Flash 8Mb"

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x001016f1
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x9d200000
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 2 - On-board SDRAM
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for on-board SDRAM 32Mb"

  ## Strobe on falling edge, bus release width 1 cycle, 1 sub-bank of 256Mb, column address width 9, port size 16 bit
  set *$EMI_BANK2_EMICONFIGDATA0 = 0x04000452

  ## Page address mask
  set *$EMI_BANK2_EMICONFIGDATA1 = 0x0003fff8

  ## Pre-charge time 2 cycles
  set *$EMI_BANK2_EMICONFIGDATA2 = 0x00000010

  ## ModeSetDelay 3 cycles, RefreshTime 6 cycles, ActivtoRD 2 cycles, ActivtoWR 2 cycles, CAS Latency 2 cycles, 4 banks, WRRecovTime 2 cycles
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x0000a49a

##------------------------------------------------------------------------------
## Bank 3 - STEM (SMC91C111 card)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: STEM (SMC91C111) 32Mb"

  ## set *$EMI_BANK3_EMICONFIGDATA0 = 0x041086f1
  ## set *$EMI_BANK3_EMICONFIGDATA1 = 0x93001111
  ## set *$EMI_BANK3_EMICONFIGDATA2 = 0x91001111
  ## set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

  set *$EMI_BANK3_EMICONFIGDATA0 = 0x041086f1
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x0e024400
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x0e024400
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 32Mb"

##------------------------------------------------------------------------------
## Bank 5 - EPLD Registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for EPLD Registers 15Mb"

  set *$EMI_BANK5_EMICONFIGDATA0 = 0x001016f1
  set *$EMI_BANK5_EMICONFIGDATA1 = 0x8a000000
  set *$EMI_BANK5_EMICONFIGDATA2 = 0x8a220000
  set *$EMI_BANK5_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000006

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  ## _ST_display (_procname) "EMI SDRAM CLOCK @ 1/3 bus clock"
  set *$EMI_SDRAMCLKSEL = 0x00000002

  set *$EMI_CLKENABLE = 0x00000001
  set *$EMI_SDRAMNOPGEN = 0x00000001
  set *$EMI_REFRESHINIT = 0x00000100
  set *$EMI_SDRAMMODEREG = 0x00000022
  ## _ST_cfg_sleep 1000
  set *$EMI_SDRAMINIT = 0x00000001
end
##}}}

##{{{  MB379 LMI Configuration
define mb379_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM"

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  set *$SYSCONF_SYS_CON1_0 = 0x01000000
  set *$SYSCONF_SYS_CON2_1 = 0x0a800000

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x04100243

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x352d4345

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0c001903

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0c001903

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SDMR0 = 0x00000400
  set *$LMI_SDMR1 = 0x00000400
  set *$LMI_SDMR0 = 0x00000163
  set *$LMI_SDMR1 = 0x00000163
  ## _ST_cfg_sleep 1000
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SDMR0 = 0x00000063
  set *$LMI_SDMR1 = 0x00000063
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB379 Memory
define mb379_memory_define
  memory-add Flash     0x00000000 8 ROM
  memory-add Flash     0x00800000 8 ROM
  memory-add EMI_SDRAM 0x01000000 32 RAM
  memory-add EPLD_regs 0x07000000 15 RAM
  memory-add LMI_SDRAM 0x08000000 64 RAM
end

define mb379_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x00800000 8 ROM
  sim_addmemory 0x01000000 32 RAM
  sim_addmemory 0x08000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define stm8000_lmi_reset_dlls
  ## Soft reset DLL1 & DLL2 and wait to lock
  set *$LMI_COC_0 = *$LMI_COC_0 | (1 << 22)
  set *$LMI_COC_1 = *$LMI_COC_1 | (1 << 13)
  while ((*$LMI_CIC_0 & 0x00080200) != 0x00000000)
  end

  ## Return DLL1 & DLL2 to normal and wait to lock
  set *$LMI_COC_0 = *$LMI_COC_0 & ~(1 << 22)
  set *$LMI_COC_1 = *$LMI_COC_1 & ~(1 << 13)
  while ((*$LMI_CIC_0 & 0x00080200) != 0x00080200)
  end
end

define mb379_setup
  stm8000_define
  mb379_memory_define

  stm8000_si_regs

  linkspeed 1.25MHz

  ## Set SH4 core ratios to 1:1:1/3
  set *$CPG_FRQCR = 0x0e4a

  ## Set PLL1 to 400MHz
  stm8000_set_clockgen_a_pll1 0x04 0x24 0x0

  ## Set PLL2 to 266MHz
  stm8000_set_clockgen_a_pll2 0x04 0x30 0x1

  ## Set normal ratio mode (ST200=400MHz ST40=200MHz STBUS=133MHz ST40PER=66MHz)
  stm8000_set_clockgen_a_mode 1

  ## Set normal DDR mode (LMI=133MHz)
  stm8000_set_clockgen_a_ddr 0

  linkspeed 10MHz

  mb379_emi_configure
  mb379_lmi_configure

  ## LMI hack for reliable 133MHz
  stm8000_lmi_reset_dlls

  set *$CCN_CCR = 0x0000090d
end

document mb379_setup
Configure an STm8000-Demo board
Usage: mb379_setup
end

define mb379sim_setup
  stm8000_define
  mb379_memory_define

  stm8000_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb379sim_setup
Configure a simulated STm8000-Demo board
Usage: mb379sim_setup
end

define mb379_fsim_setup
  stm8000_fsim_core_setup
  mb379_sim_memory_define
end

document mb379_fsim_setup
Configure functional simulator for STm8000-Demo board
Usage: mb379_fsim_setup
end

define mb379_psim_setup
  stm8000_psim_core_setup
  mb379_sim_memory_define
end

document mb379_psim_setup
Configure performance simulator for STm8000-Demo board
Usage: mb379_psim_setup
end

define mb379_display_registers
  stm8000_display_si_regs
end

document mb379_display_registers
Display the STm8000 configuration registers
Usage: mb379_display_registers
end
