################################################################################

define mb360
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb360.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb360_setup $arg1
  else
    connectsh4le $arg0 mb360_setup "hardreset"
  end
end

document mb360
Connect to and configure an ST40RA-Eval board
Usage: mb360 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb360usb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb360.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb360_setup $arg1
  else
    connectsh4usble $arg0 mb360_setup "hardreset"
  end
end

document mb360usb
Connect to and configure an ST40RA-Eval board
Usage: mb360usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb360sim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb360.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb360_fsim_setup mb360sim_setup $arg0
  else
    connectsh4simle mb360_fsim_setup mb360sim_setup ""
  end
end

document mb360sim
Connect to and configure a simulated ST40RA-Eval board
Usage: mb360sim
end

################################################################################

define mb360psim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb360.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb360_psim_setup mb360sim_setup $arg0
  else
    connectsh4psimle mb360_psim_setup mb360sim_setup ""
  end
end

document mb360psim
Connect to and configure a simulated ST40RA-Eval board
Usage: mb360psim
end

################################################################################
