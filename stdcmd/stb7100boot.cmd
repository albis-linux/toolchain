################################################################################
#
# STb7100 ST231 Boot Commands
#
################################################################################

init-if-undefined $_stb7100_st231_audio_boot = 1
keep-variable $_stb7100_st231_audio_boot

init-if-undefined $_stb7100_st231_video_boot = 1
keep-variable $_stb7100_st231_video_boot

define stb7100_st231_audio_boot
  set *$SYSCONF_SYS_CFG09 |= 0x10000000
  set *$SYSCONF_SYS_CFG26 = ((*$SYSCONF_SYS_CFG27 & 0x00001ffe) << 19) | 0x00004001
  set *$SYSCONF_SYS_CFG27 |= 0x00000001
  set *$SYSCONF_SYS_CFG27 &= ~0x00000001
end

define stb7100_st231_video_boot
  set *$SYSCONF_SYS_CFG09 |= 0x10000000
  set *$SYSCONF_SYS_CFG28 = ((*$SYSCONF_SYS_CFG29 & 0x00001ffe) << 19) | 0x00004001
  set *$SYSCONF_SYS_CFG29 |= 0x00000001
  set *$SYSCONF_SYS_CFG29 &= ~0x00000001
end

define stb7100_st231_boot
  if ($_stb7100_st231_audio_boot)
    stb7100_st231_audio_boot
  end

  if ($_stb7100_st231_video_boot)
    stb7100_st231_video_boot
  end
end

################################################################################
