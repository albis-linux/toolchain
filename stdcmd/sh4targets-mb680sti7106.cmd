################################################################################

define mb680sti7106sim
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7106_fsim_setup mb680sti7106sim_setup $arg0
  else
    connectsh4simle mb680sti7106_fsim_setup mb680sti7106sim_setup ""
  end
end

document mb680sti7106sim
Connect to and configure a simulated STi7106-Mboard board
Usage: mb680sti7106sim
end

define mb680sti7106simse
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7106se_fsim_setup mb680sti7106simse_setup $arg0
  else
    connectsh4simle mb680sti7106se_fsim_setup mb680sti7106simse_setup ""
  end
end

document mb680sti7106simse
Connect to and configure a simulated STi7106-Mboard board (STi7106 in 32-bit SE mode)
Usage: mb680sti7106simse
end

define mb680sti7106simseuc
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7106se_fsim_setup mb680sti7106simseuc_setup $arg0
  else
    connectsh4simle mb680sti7106se_fsim_setup mb680sti7106simseuc_setup ""
  end
end

document mb680sti7106simseuc
Connect to and configure a simulated STi7106-Mboard board (STi7106 in 32-bit SE mode with uncached mappings)
Usage: mb680sti7106simseuc
end

define mb680sti7106simse29
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb680sti7106se_fsim_setup mb680sti7106simse29_setup $arg0
  else
    connectsh4simle mb680sti7106se_fsim_setup mb680sti7106simse29_setup ""
  end
end

document mb680sti7106simse29
Connect to and configure a simulated STi7106-Mboard board (STi7106 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb680sti7106simse29
end

################################################################################

define mb680sti7106psim
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7106_psim_setup mb680sti7106sim_setup $arg0
  else
    connectsh4psimle mb680sti7106_psim_setup mb680sti7106sim_setup ""
  end
end

document mb680sti7106psim
Connect to and configure a simulated STi7106-Mboard board
Usage: mb680sti7106psim
end

define mb680sti7106psimse
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7106se_psim_setup mb680sti7106simse_setup $arg0
  else
    connectsh4psimle mb680sti7106se_psim_setup mb680sti7106simse_setup ""
  end
end

document mb680sti7106psimse
Connect to and configure a simulated STi7106-Mboard board (STi7106 in 32-bit SE mode)
Usage: mb680sti7106psimse
end

define mb680sti7106psimseuc
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7106se_psim_setup mb680sti7106simseuc_setup $arg0
  else
    connectsh4psimle mb680sti7106se_psim_setup mb680sti7106simseuc_setup ""
  end
end

document mb680sti7106psimseuc
Connect to and configure a simulated STi7106-Mboard board (STi7106 in 32-bit SE mode with uncached mappings)
Usage: mb680sti7106psimseuc
end

define mb680sti7106psimse29
  source register40.cmd
  source display40.cmd
  source sti7106.cmd
  source mb680sti7106.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb680sti7106se_psim_setup mb680sti7106simse29_setup $arg0
  else
    connectsh4psimle mb680sti7106se_psim_setup mb680sti7106simse29_setup ""
  end
end

document mb680sti7106psimse29
Connect to and configure a simulated STi7106-Mboard board (STi7106 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb680sti7106psimse29
end

################################################################################
