##------------------------------------------------------------------------------
## fldb_gpd201.cmd - FLi7510 Development Platform FLDB_GPD201
##------------------------------------------------------------------------------

##{{{  FLDB_GPD201 PMB Configuration
define fldb_gpd201se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0xa0 0x80 128
  sh4_set_pmb 2 0xa8 0x88 128

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define fldb_gpd201seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  sh4_set_pmb 1 0xa0 0x80 128 0 0 1
  sh4_set_pmb 2 0xa8 0x88 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define fldb_gpd201se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x80 128
  sh4_set_pmb 2 0x90 0x88 128
  sh4_set_pmb 3 0xa0 0x40 128 0 0 1
  sh4_set_pmb 4 0xa8 0x80 128 0 0 1
  sh4_set_pmb 5 0xb0 0x88 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  FLDB_GPD201 Memory
define fldb_gpd201se_memory_define
  memory-add Flash        0x00000000 32 ROM
  memory-add LMICMP_SDRAM 0x40000000 128 RAM
  memory-add LMIVID_SDRAM 0x80000000 256 RAM
end

define fldb_gpd201se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x40000000 128 RAM
  sim_addmemory 0x80000000 256 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define fldb_gpd201simse_setup
  fli7510_define
  fldb_gpd201se_memory_define

  st40300_core_si_regs

  fldb_gpd201se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document fldb_gpd201simse_setup
Configure a simulated FLi7510 development board with the FLi7510 in 32-bit SE mode
Usage: fldb_gpd201simse_setup
end

define fldb_gpd201simseuc_setup
  fli7510_define
  fldb_gpd201se_memory_define

  st40300_core_si_regs

  fldb_gpd201seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document fldb_gpd201simseuc_setup
Configure a simulated FLi7510 development board with the FLi7510 in 32-bit SE mode with uncached RAM mappings
Usage: fldb_gpd201simseuc_setup
end

define fldb_gpd201simse29_setup
  fli7510_define
  fldb_gpd201se_memory_define

  st40300_core_si_regs

  fldb_gpd201se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document fldb_gpd201simse29_setup
Configure a simulated FLi7510 development board with the FLi7510 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: fldb_gpd201simse29_setup
end

define fldb_gpd201se_fsim_setup
  fli7510_fsim_core_setup
  fldb_gpd201se_sim_memory_define
end

document fldb_gpd201se_fsim_setup
Configure functional simulator for FLi7510 development board with the FLi7510 in 32-bit SE mode
Usage: fldb_gpd201se_fsim_setup
end

define fldb_gpd201se_psim_setup
  fli7510_psim_core_setup
  fldb_gpd201se_sim_memory_define
end

document fldb_gpd201se_psim_setup
Configure performance simulator for FLi7510 development board with the FLi7510 in 32-bit SE mode
Usage: fldb_gpd201se_psim_setup
end

define fldb_gpd201_display_registers
  st40300_display_core_si_regs
end

document fldb_gpd201_display_registers
Display the FLi7510 configuration registers
Usage: fldb_gpd201_display_registers
end
