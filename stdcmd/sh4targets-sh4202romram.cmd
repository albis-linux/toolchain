################################################################################

define sh4202romrambe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh4202romram_setup $arg1
  else
    connectsh4be $arg0 sh4202romram_setup "hardreset"
  end
end

document sh4202romrambe
Connect to and configure an SH4202 ROMRAM Evaluation board (big endian)
Usage: sh4202romrambe <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202romramle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh4202romram_setup $arg1
  else
    connectsh4le $arg0 sh4202romram_setup "hardreset"
  end
end

document sh4202romramle
Connect to and configure an SH4202 ROMRAM Evaluation board (little endian)
Usage: sh4202romramle <target>
where <target> is an ST Micro Connect name or IP address
end

define sh4202romram
  if ($argc > 1)
    sh4202romramle $arg0 $arg1
  else
    sh4202romramle $arg0
  end
end

document sh4202romram
Connect to and configure an SH4202 ROMRAM Evaluation board
Usage: sh4202romram <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh4202romramusbbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh4202romram_setup $arg1
  else
    connectsh4usbbe $arg0 sh4202romram_setup "hardreset"
  end
end

document sh4202romramusbbe
Connect to and configure an SH4202 ROMRAM Evaluation board (big endian)
Usage: sh4202romramusbbe <target>
where <target> is an ST Micro Connect USB name
end

define sh4202romramusble
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh4202romram_setup $arg1
  else
    connectsh4usble $arg0 sh4202romram_setup "hardreset"
  end
end

document sh4202romramusble
Connect to and configure an SH4202 ROMRAM Evaluation board (little endian)
Usage: sh4202romramusble <target>
where <target> is an ST Micro Connect USB name
end

define sh4202romramusb
  if ($argc > 1)
    sh4202romramusble $arg0 $arg1
  else
    sh4202romramusble $arg0
  end
end

document sh4202romramusb
Connect to and configure an SH4202 ROMRAM Evaluation board
Usage: sh4202romramusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh4202romramsimbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh4202romram_fsim_setup sh4202romramsim_setup $arg0
  else
    connectsh4simbe sh4202romram_fsim_setup sh4202romramsim_setup ""
  end
end

document sh4202romramsimbe
Connect to and configure a simulated SH4202 ROMRAM Evaluation board (big endian)
Usage: sh4202romramsimbe
end

define sh4202romramsimle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh4202romram_fsim_setup sh4202romramsim_setup $arg0
  else
    connectsh4simle sh4202romram_fsim_setup sh4202romramsim_setup ""
  end
end

document sh4202romramsimle
Connect to and configure a simulated SH4202 ROMRAM Evaluation board (little endian)
Usage: sh4202romramsimle
end

define sh4202romramsim
  if ($argc > 0)
    sh4202romramsimle $arg0
  else
    sh4202romramsimle
  end
end

document sh4202romramsim
Connect to and configure a simulated SH4202 ROMRAM Evaluation board
Usage: sh4202romramsim
end

################################################################################

define sh4202romrampsimbe
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh4202romram_psim_setup sh4202romramsim_setup $arg0
  else
    connectsh4psimbe sh4202romram_psim_setup sh4202romramsim_setup ""
  end
end

document sh4202romrampsimbe
Connect to and configure a simulated SH4202 ROMRAM Evaluation board (big endian)
Usage: sh4202romrampsimbe
end

define sh4202romrampsimle
  source register40.cmd
  source display40.cmd
  source sh4202.cmd
  source sh4202romram.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh4202romram_psim_setup sh4202romramsim_setup $arg0
  else
    connectsh4psimle sh4202romram_psim_setup sh4202romramsim_setup ""
  end
end

document sh4202romrampsimle
Connect to and configure a simulated SH4202 ROMRAM Evaluation board (little endian)
Usage: sh4202romrampsimle
end

define sh4202romrampsim
  if ($argc > 0)
    sh4202romrampsimle $arg0
  else
    sh4202romrampsimle
  end
end

document sh4202romrampsim
Connect to and configure a simulated SH4202 ROMRAM Evaluation board
Usage: sh4202romrampsim
end

################################################################################
