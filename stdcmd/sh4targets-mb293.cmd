################################################################################

define mb293
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb293_setup $arg1
  else
    connectsh4le $arg0 mb293_setup "hardreset"
  end
end

document mb293
Connect to and configure an ST40RA HARP board
Usage: mb293 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb293usb
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb293_setup $arg1
  else
    connectsh4usble $arg0 mb293_setup "hardreset"
  end
end

document mb293usb
Connect to and configure an ST40RA HARP board
Usage: mb293usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb293sim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb293_fsim_setup mb293sim_setup $arg0
  else
    connectsh4simle mb293_fsim_setup mb293sim_setup ""
  end
end

document mb293sim
Connect to and configure a simulated ST40RA HARP board
Usage: mb293sim
end

################################################################################

define mb293psim
  source register40.cmd
  source display40.cmd
  source st40clocks.cmd
  source st40ra.cmd
  source mb293.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb293_psim_setup mb293sim_setup $arg0
  else
    connectsh4psimle mb293_psim_setup mb293sim_setup ""
  end
end

document mb293psim
Connect to and configure a simulated ST40RA HARP board
Usage: mb293psim
end

################################################################################
