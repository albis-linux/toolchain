################################################################################

define adi7108simse
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source adi7108.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle adi7108se_fsim_setup adi7108simse_setup $arg0
  else
    connectsh4simle adi7108se_fsim_setup adi7108simse_setup ""
  end
end

document adi7108simse
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode)
Usage: adi7108simse
end

define adi7108simseuc
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source adi7108.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle adi7108se_fsim_setup adi7108simseuc_setup $arg0
  else
    connectsh4simle adi7108se_fsim_setup adi7108simseuc_setup ""
  end
end

document adi7108simseuc
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode with uncached mappings)
Usage: adi7108simseuc
end

define adi7108simse29
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source adi7108.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle adi7108se_fsim_setup adi7108simse29_setup $arg0
  else
    connectsh4simle adi7108se_fsim_setup adi7108simse29_setup ""
  end
end

document adi7108simse29
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: adi7108simse29
end

################################################################################

define adi7108sim
  if ($argc > 0)
    adi7108simse $arg0
  else
    adi7108simse
  end
end

document adi7108sim
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode)
Usage: adi7108sim
end

################################################################################

define adi7108psimse
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source adi7108.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle adi7108se_psim_setup adi7108simse_setup $arg0
  else
    connectsh4psimle adi7108se_psim_setup adi7108simse_setup ""
  end
end

document adi7108psimse
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode)
Usage: adi7108psimse
end

define adi7108psimseuc
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source adi7108.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle adi7108se_psim_setup adi7108simseuc_setup $arg0
  else
    connectsh4psimle adi7108se_psim_setup adi7108simseuc_setup ""
  end
end

document adi7108psimseuc
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode with uncached mappings)
Usage: adi7108psimseuc
end

define adi7108psimse29
  source register40.cmd
  source display40.cmd
  source sti7108.cmd
  source adi7108.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle adi7108se_psim_setup adi7108simse29_setup $arg0
  else
    connectsh4psimle adi7108se_psim_setup adi7108simse29_setup ""
  end
end

document adi7108psimse29
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: adi7108psimse29
end

################################################################################

define adi7108psim
  if ($argc > 0)
    adi7108psimse $arg0
  else
    adi7108psimse
  end
end

document adi7108psim
Connect to and configure a simulated STi7108-ADI board (STi7108 in 32-bit SE mode)
Usage: adi7108psim
end

################################################################################
