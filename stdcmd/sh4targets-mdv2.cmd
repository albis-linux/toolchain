################################################################################

define mdv2
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mdv2.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mdv2_setup $arg1
  else
    connectsh4le $arg0 mdv2_setup "jtagpinout=default hardreset"
  end
end

document mdv2
Connect to and configure a Sagem MDV-2 board
Usage: mdv2 <target>
where <target> is an ST Micro Connect name or IP address
end

define mdv2bypass
  if ($argc > 1)
    mdv2 $arg0 "jtagpinout=default jtagreset -inicommand $arg1"
  else
    mdv2 $arg0 "jtagpinout=default jtagreset -inicommand mdv2bypass_setup"
  end
end

document mdv2bypass
Connect to and configure a Sagem MDV-2 board bypassing to the ST40
Usage: mdv2bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mdv2stmmx
  if ($argc > 1)
    mdv2 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mdv2 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mdv2stmmx_setup"
  end
end

document mdv2stmmx
Connect to and configure a Sagem MDV-2 board via an ST MultiCore/Mux
Usage: mdv2stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mdv2usb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source mdv2.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mdv2_setup $arg1
  else
    connectsh4usble $arg0 mdv2_setup "jtagpinout=default hardreset"
  end
end

document mdv2usb
Connect to and configure a Sagem MDV-2 board
Usage: mdv2usb <target>
where <target> is an ST Micro Connect USB name
end

define mdv2bypassusb
  if ($argc > 1)
    mdv2usb $arg0 "jtagpinout=default jtagreset -inicommand $arg1"
  else
    mdv2usb $arg0 "jtagpinout=default jtagreset -inicommand mdv2bypass_setup"
  end
end

document mdv2bypassusb
Connect to and configure a Sagem MDV-2 board bypassing to the ST40
Usage: mdv2bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mdv2stmmxusb
  if ($argc > 1)
    mdv2usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mdv2usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mdv2stmmx_setup"
  end
end

document mdv2stmmxusb
Connect to and configure a Sagem MDV-2 board via an ST MultiCore/Mux
Usage: mdv2stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mdv2sim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source mdv2.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mdv2_fsim_setup mdv2sim_setup $arg0
  else
    connectsh4simle mdv2_fsim_setup mdv2sim_setup ""
  end
end

document mdv2sim
Connect to and configure a simulated Sagem MDV-2 board
Usage: mdv2sim
end

################################################################################

define mdv2psim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source mdv2.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mdv2_psim_setup mdv2sim_setup $arg0
  else
    connectsh4psimle mdv2_psim_setup mdv2sim_setup ""
  end
end

document mdv2psim
Connect to and configure a simulated Sagem MDV-2 board
Usage: mdv2psim
end

################################################################################
