##------------------------------------------------------------------------------
## tmmidr04.cmd - STm8000 TMM Development Platform
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  TMMIDR04 EMI Configuration
define tmmidr04_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000002
  set *$EMI_BANK2_BASEADDRESS = 0x00000004
  set *$EMI_BANK3_BASEADDRESS = 0x0000000c
  set *$EMI_BANK4_BASEADDRESS = 0x00000014
  set *$EMI_BANK5_BASEADDRESS = 0x0000001c

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 8Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016f1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Configured for on-board Flash 8Mb"

  set *$EMI_BANK1_EMICONFIGDATA0 = 0x001016f1
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x9d200000
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 2 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Undefined 32Mb"

##------------------------------------------------------------------------------
## Bank 3 - On-board IEEE 1394
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for on-board IEEE 1394 32Mb"
  set *$EMI_BANK3_EMICONFIGDATA0 = 0x045006d1
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x1e687900
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x1e687900
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - On-board ATAPI
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for on-board ATAPI 32Mb"
  set *$EMI_BANK4_EMICONFIGDATA0 = 0x00138791
  set *$EMI_BANK4_EMICONFIGDATA1 = 0x13123200
  set *$EMI_BANK4_EMICONFIGDATA2 = 0x1f123202
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 5 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Undefined 15Mb"

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000000

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  set *$EMI_CLKENABLE = 0x00000001
end
##}}}

##{{{  TMMIDR04 LMI Configuration
define tmmidr04_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM"

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  set *$SYSCONF_SYS_CON1_0 = 0x01000000
  set *$SYSCONF_SYS_CON2_1 = 0x0a800000

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x04100243

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x352d4345

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0c001903

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0c001903

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SDMR0 = 0x00000400
  set *$LMI_SDMR1 = 0x00000400
  set *$LMI_SDMR0 = 0x00000163
  set *$LMI_SDMR1 = 0x00000163
  ## _ST_cfg_sleep 1000
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SDMR0 = 0x00000063
  set *$LMI_SDMR1 = 0x00000063
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  TMMIDR04 Memory
define tmmidr04_memory_define
  memory-add Flash     0x00000000 8 ROM
  memory-add Flash     0x00800000 8 ROM
  memory-add LMI_SDRAM 0x08000000 64 RAM
end

define tmmidr04_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x00800000 8 ROM
  sim_addmemory 0x08000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define stm8000_lmi_reset_dlls
  ## Soft reset DLL1 & DLL2 and wait to lock
  set *$LMI_COC_0 = *$LMI_COC_0 | (1 << 22)
  set *$LMI_COC_1 = *$LMI_COC_1 | (1 << 13)
  while ((*$LMI_CIC_0 & 0x00080200) != 0x00000000)
  end

  ## Return DLL1 & DLL2 to normal and wait to lock
  set *$LMI_COC_0 = *$LMI_COC_0 & ~(1 << 22)
  set *$LMI_COC_1 = *$LMI_COC_1 & ~(1 << 13)
  while ((*$LMI_CIC_0 & 0x00080200) != 0x00080200)
  end
end

define tmmidr04_setup
  stm8000_define
  tmmidr04_memory_define

  stm8000_si_regs

  linkspeed 1.25MHz

  ## Set SH4 core ratios to 1:1:1/3
  set *$CPG_FRQCR = 0x0e4a

  ## Set PLL1 to 350MHz
  stm8000_set_clockgen_a_pll1 0x04 0x3f 0x1

  ## Set PLL2 to 200MHz
  stm8000_set_clockgen_a_pll2 0x04 0x24 0x1

  ## Set normal ratio mode (ST200=350MHz ST40=175MHz STBUS=116MHz ST40PER=58MHz)
  stm8000_set_clockgen_a_mode 1

  ## Set normal DDR mode (LMI=100MHz)
  stm8000_set_clockgen_a_ddr 0

  linkspeed 10MHz

  tmmidr04_emi_configure
  tmmidr04_lmi_configure

  ## LMI hack for reliable 100MHz
  stm8000_lmi_reset_dlls

  set *$CCN_CCR = 0x0000090d
end

document tmmidr04_setup
Configure an STm8000-TMM board
Usage: tmmidr04_setup
end

define tmmidr04sim_setup
  stm8000_define
  tmmidr04_memory_define

  stm8000_si_regs

  set *$CCN_CCR = 0x0000090d
end

document tmmidr04sim_setup
Configure a simulated STm8000-TMM board
Usage: tmmidr04sim_setup
end

define tmmidr04_fsim_setup
  stm8000_fsim_core_setup
  tmmidr04_sim_memory_define
end

document tmmidr04_fsim_setup
Configure functional simulator for STm8000-TMM board
Usage: tmmidr04_fsim_setup
end

define tmmidr04_psim_setup
  stm8000_psim_core_setup
  tmmidr04_sim_memory_define
end

document tmmidr04_psim_setup
Configure performance simulator for STm8000-TMM board
Usage: tmmidr04_psim_setup
end

define tmmidr04_display_registers
  stm8000_display_si_regs
end

document tmmidr04_display_registers
Display the STm8000 configuration registers
Usage: tmmidr04_display_registers
end
