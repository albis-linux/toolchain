source jtagcommon.cmd

################################################################################

define stmmxTestLogicReset
  ## Move to Test-Logic-Reset
  jtag stmmx=0 tms=(1*5)
end

################################################################################

define stmmxRunTestIdle
  ## Move to Run-Test-Idle
  stmmxTestLogicReset
  jtag stmmx=0 tms=0
end

################################################################################

define stmmxPauseIR
  ## Pause IR register (Run-Test-Idle initial and final state)
  jtag stmmx=0 tms=11010(0*$arg0)1100
end

################################################################################

define _stmmxReadIR
  ## Read IR register (Run-Test-Idle initial and final state)
  jtag stmmx=0 tms=1100
  jtag stmmx=0 tms=(0*4) tdo=1 tdi=$arg0
  jtag stmmx=0 tms=110
end

define stmmxReadIR
  if ($argc > 0)
    _stmmxReadIR $arg0
  else
    _stmmxReadIR $_ir
  end
end

################################################################################

define stmmxWriteIR
  ## Write IR register (Run-Test-Idle initial and final state)
  set $_size = 4
  set $_data = $arg0
  jtag stmmx=0 tms=1100
  while ($_size-- > 0)
    if ($_data & 1)
      jtag stmmx=0 tms=0 tdo=1
    else
      jtag stmmx=0 tms=0 tdo=0
    end
    set $_data >>= 1
  end
  jtag stmmx=0 tms=110
end

################################################################################

define stmmxWriteIR_IDCODE
  stmmxWriteIR 0x2
end

define stmmxWriteIR_STATUS
  stmmxWriteIR 0x3
end

define stmmxWriteIR_TAPMUX
  stmmxWriteIR 0x4
end

define stmmxWriteIR_ALLOC_RD
  stmmxWriteIR 0x5
end

define stmmxWriteIR_ALLOC_WR
  stmmxWriteIR 0x6
end

define stmmxWriteIR_SPEED
  stmmxWriteIR 0x7
end

define stmmxWriteIR_BYPASS
  stmmxWriteIR 0x8
end

define stmmxWriteIR_RESET_MASK
  stmmxWriteIR 0x9
end

define stmmxWriteIR_LED
  stmmxWriteIR 0xa
end

define stmmxWriteIR_ASEBRK
  stmmxWriteIR 0xb
end

define stmmxWriteIR_REV
  stmmxWriteIR 0xe
end

################################################################################

define stmmxPrintIR
  if ($argc > 0)
    stmmxReadIR $arg0
    _jtagPrintSignal $arg0
  else
    stmmxReadIR $stmmxir
    _jtagPrintSignal $stmmxir
  end
end

define stmmxPrintRestoreIR
  if ($argc > 0)
    stmmxReadIR $arg0
    stmmxWriteIR $arg0_0
    _jtagPrintSignal $arg0
  else
    stmmxReadIR $stmmxir
    stmmxWriteIR $stmmxir_0
    _jtagPrintSignal $stmmxir
  end
end

################################################################################

define stmmxPauseDR
  ## Pause DR register (Run-Test-Idle initial and final state)
  jtag stmmx=0 tms=1010(0*$arg0)1100
end

################################################################################

define _stmmxReadDR
  ## Read DR register (Run-Test-Idle initial and final state)
  jtag stmmx=0 tms=100
  jtag stmmx=0 tms=(0*$arg0) tdo=0 tdi=$arg1
  jtag stmmx=0 tms=110
end

define stmmxReadDR
  if ($argc > 1)
    _stmmxReadDR $arg0 $arg1
  else
    _stmmxReadDR $arg0 $_dr
  end
end

################################################################################

define stmmxWriteDR
  ## Write DR register (Run-Test-Idle initial and final state)
  set $_size = $arg0
  set $_data = $arg1
  jtag stmmx=0 tms=100
  while ($_size-- > 0)
    if ($_data & 1)
      jtag stmmx=0 tms=0 tdo=1
    else
      jtag stmmx=0 tms=0 tdo=0
    end
    set $_data >>= 1
  end
  jtag stmmx=0 tms=110
end

################################################################################

define stmmxPrintDR
  if ($argc > 1)
    stmmxReadDR $arg0 $arg1
    _jtagPrintSignal $arg1
  else
    stmmxReadDR $arg0 $stmmxdr
    _jtagPrintSignal $stmmxdr
  end
end

define stmmxPrintRestoreDR
  if ($argc > 1)
    stmmxReadDR $arg0 $arg1
    stmmxWriteDR $arg0 $arg1_0
    _jtagPrintSignal $arg1
  else
    stmmxReadDR $arg0 $stmmxdr
    stmmxWriteDR $arg0 $stmmxdr_0
    _jtagPrintSignal $stmmxdr
  end
end

################################################################################

define stmmxInitialState
  jtag mode=singleshot
  stmmxRunTestIdle
end

################################################################################

define stmmxPrintRegister
  stmmxWriteIR_$arg0
  stmmxPrintDR $arg1 $stmmx$arg0
end

define stmmxPrintRestoreRegister
  stmmxWriteIR_$arg0
  stmmxPrintRestoreDR $arg1 $stmmx$arg0
end

################################################################################

define stmmxPrintAllRegisters
  stmmxPrintRegister IDCODE 32
  stmmxPrintRegister STATUS 24
  stmmxPrintRegister ALLOC_RD 32
  stmmxPrintRegister REV 8

  stmmxPrintRestoreRegister TAPMUX 1
  stmmxPrintRestoreRegister SPEED 24
  stmmxPrintRestoreRegister BYPASS 4
  stmmxPrintRestoreRegister RESET 4
  stmmxPrintRestoreRegister LED 2
  stmmxPrintRestoreRegister ASEBRK 4
end

################################################################################

define stmmxWriteICS9161
  stmmxWriteIR_SPEED
  stmmxWriteDR 24 $arg0
  stmmxPauseDR 40
  _jtagSleep 100
end

################################################################################
