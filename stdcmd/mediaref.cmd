##------------------------------------------------------------------------------
## mediaref.cmd - ST40GX1+ST5512 MediaRef Platform
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MediaRef EMI Configuration
define mediaref_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000c
  set *$EMI_BANK4_BASEADDRESS = 0x00000010
  set *$EMI_BANK5_BASEADDRESS = 0x00000014

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d010000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d010000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 2 - MPX Mode
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for MPX Mode 16Mb"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x000030db
  set *$EMI_BANK2_EMICONFIGDATA1 = 0x00000000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0x00000000
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 4 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Undefined 16Mb"

##------------------------------------------------------------------------------
## Bank 5 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Undefined 47Mb"

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  ## _ST_display (_procname) "EMI MPX CLOCK @ 1/2 bus clock"
  set *$EMI_MPXCLKSEL = 0x00000001

  set *$EMI_CLKENABLE = 0x00000001
end
##}}}

##{{{  MediaRef LMI Configuration
define mediaref_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for DDR SDRAM"

##------------------------------------------------------------------------------
## Program clock generator registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM CLOCK @ 1/1 processor clock"
  set *$CLOCKGENA_CLK4CR = 0x00000008
  set *$CLOCKGENA_CLK4CR = (0x00000000|0x00000008)
  set *$CLOCKGENA_CLK4CR = 0x00000000

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  ## NOTE:
  ##   bit 8 = 0 for SSTL (DDR), 1 for LVTTL (PC-SDRAM)
  ##   bit 9 = 0 for internal ref voltage, 1 for external ref voltage
  ##   bit 10 = 1 to by pass ECLK180 retime
  ##   bit 11 = 1 for PC-SDRAM, 0 for DDR notLMICOMP25_EN
  ##   bit 12 = 1 for PC-SDRAM, 0 for DDR LMICOMP33_EN
  ##   bits [13,14] = LVTTL drive strength for data & data strobe pads
  ##   bits [15,16] = LVTTL drive strength for address & control pads

  set *$SYSCONF_SYS_CON2_0 = 0x00000000

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x04000283

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x00001a6b

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0f001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0f001900

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00002000/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00002000/sizeof(*$LMI_SDMR1))) = 0x0
  set *($LMI_SDMR0+(0x00000910/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000910/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *($LMI_SDMR0+(0x00000110/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000110/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  ST40GX1 IO Configuration
define st40gx1_io_configure
  ## _ST_display (_procname) "Configuring access to IO block"

  ## Control memory bridge 4 - I/O block initiator:
  ## - asynchronous, one cycle FIFO latency, software reset disabled, strobe required to latch the data
  set *$SYSCONF_SYS_CON1_0 = 0x8e000000

  ## Control memory bridge 5 - I/O block target:
  ## - asynchronous, one cycle FIFO latency, software reset disabled, strobe required to latch the data
  set *$SYSCONF_SYS_CON1_1 = 0x00000047
end
##}}}

##{{{  MediaRef Memory
define mediaref_memory_define
  memory-add Flash     0x00000000 16 ROM
  memory-add MPX       0x02000000 16 RAM
  memory-add LMI_SDRAM 0x08000000 112 RAM
end

define mediaref_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x08000000 112 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mediaref_setup
  st40gx1_define
  mediaref_memory_define

  st40gx1_si_regs

  mediaref_emi_configure
  mediaref_lmi_configure
  st40gx1_io_configure

  set *$CCN_CCR = 0x0000090d
end

document mediaref_setup
Configure an STMediaRef-Demo board
Usage: mediaref_setup
end

define mediarefsim_setup
  st40gx1_define
  mediaref_memory_define

  st40gx1_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mediarefsim_setup
Configure an STMediaRef-Demo board
Usage: mediarefsim_setup
end

define mediaref_fsim_setup
  st40gx1_fsim_core_setup
  mediaref_sim_memory_define
end

document mediaref_fsim_setup
Configure functional simulator for STMediaRef-Demo board
Usage: mediaref_fsim_setup
end

define mediaref_psim_setup
  st40gx1_psim_core_setup
  mediaref_sim_memory_define
end

document mediaref_psim_setup
Configure performance simulator for STMediaRef-Demo board
Usage: mediaref_psim_setup
end

define mediaref_display_registers
  st40gx1_display_si_regs
end

document mediaref_display_registers
Display the ST40GX1 configuration registers
Usage: mediaref_display_registers
end
