################################################################################

define adi7105sim
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle adi7105_fsim_setup adi7105sim_setup $arg0
  else
    connectsh4simle adi7105_fsim_setup adi7105sim_setup ""
  end
end

document adi7105sim
Connect to and configure a simulated STi7105-ADI board
Usage: adi7105sim
end

define adi7105simse
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle adi7105se_fsim_setup adi7105simse_setup $arg0
  else
    connectsh4simle adi7105se_fsim_setup adi7105simse_setup ""
  end
end

document adi7105simse
Connect to and configure a simulated STi7105-ADI board (STi7105 in 32-bit SE mode)
Usage: adi7105simse
end

define adi7105simseuc
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle adi7105se_fsim_setup adi7105simseuc_setup $arg0
  else
    connectsh4simle adi7105se_fsim_setup adi7105simseuc_setup ""
  end
end

document adi7105simseuc
Connect to and configure a simulated STi7105-ADI board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: adi7105simseuc
end

define adi7105simse29
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle adi7105se_fsim_setup adi7105simse29_setup $arg0
  else
    connectsh4simle adi7105se_fsim_setup adi7105simse29_setup ""
  end
end

document adi7105simse29
Connect to and configure a simulated STi7105-ADI board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: adi7105simse29
end

################################################################################

define adi7105psim
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle adi7105_psim_setup adi7105sim_setup $arg0
  else
    connectsh4psimle adi7105_psim_setup adi7105sim_setup ""
  end
end

document adi7105psim
Connect to and configure a simulated STi7105-ADI board
Usage: adi7105psim
end

define adi7105psimse
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle adi7105se_psim_setup adi7105simse_setup $arg0
  else
    connectsh4psimle adi7105se_psim_setup adi7105simse_setup ""
  end
end

document adi7105psimse
Connect to and configure a simulated STi7105-ADI board (STi7105 in 32-bit SE mode)
Usage: adi7105psimse
end

define adi7105psimseuc
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle adi7105se_psim_setup adi7105simseuc_setup $arg0
  else
    connectsh4psimle adi7105se_psim_setup adi7105simseuc_setup ""
  end
end

document adi7105psimseuc
Connect to and configure a simulated STi7105-ADI board (STi7105 in 32-bit SE mode with uncached mappings)
Usage: adi7105psimseuc
end

define adi7105psimse29
  source register40.cmd
  source display40.cmd
  source sti7105.cmd
  source adi7105.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle adi7105se_psim_setup adi7105simse29_setup $arg0
  else
    connectsh4psimle adi7105se_psim_setup adi7105simse29_setup ""
  end
end

document adi7105psimse29
Connect to and configure a simulated STi7105-ADI board (STi7105 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: adi7105psimse29
end

################################################################################
