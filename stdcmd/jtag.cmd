set $jtag_enabled = 0
keep-variable $jtag_enabled

define jtag
  if (!$jtag_enabled)
    printf "JTAG plugin not installed\n"
  else
    if ($argc == 0)
      callplugin jtag
    end
    if ($argc == 1)
      callplugin jtag $arg0
    end
    if ($argc == 2)
      callplugin jtag $arg0 $arg1
    end
    if ($argc == 3)
      callplugin jtag $arg0 $arg1 $arg2
    end
    if ($argc == 4)
      callplugin jtag $arg0 $arg1 $arg2 $arg3
    end
    if ($argc == 5)
      callplugin jtag $arg0 $arg1 $arg2 $arg3 $arg4
    end
    if ($argc == 6)
      callplugin jtag $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
    end
    if ($argc == 7)
      callplugin jtag $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6
    end
    if ($argc == 8)
      callplugin jtag $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7
    end
    if ($argc == 9)
      callplugin jtag $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8
    end
    if ($argc > 9)
      callplugin jtag help
    end
  end
end

document jtag
Use "jtag help"
end

define enable_jtag
  if (!$jtag_enabled)
    installplugin jtag libsh4jtag.so
    set $jtag_enabled = 1
    source jtaghudi.cmd
    source jtagstmmx.cmd
    source jtagtmc.cmd
  else
    printf "JTAG plugin already enabled\n"
  end
end

document enable_jtag
Enable the JTAG plugin
Usage: enable_jtag
end
