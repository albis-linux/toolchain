##------------------------------------------------------------------------------
## mb427.cmd - ST40-300 FPGA Board
##------------------------------------------------------------------------------
## Note that configuration registers must be accessed through P4.
##------------------------------------------------------------------------------

##{{{  MB427 Registers
define mb427_v320_regs
  __shx_reg_32 $V320_LB_ROM_BASE ($arg0+0x0074)
  __shx_reg_32 $V320_LB_DRAM_BASE ($arg0+0x0078)
  __shx_reg_32 $V320_LB_BUS_CFG ($arg0+0x007c)
  __shx_reg_32 $V320_SDRAM_MA_CMD ($arg0+0x0088)
  __shx_reg_32 $V320_DRAM_CFG ($arg0+0x008c)
  __shx_reg_32 $V320_DRAM_BLK0 ($arg0+0x0090)
  __shx_reg_32 $V320_DRAM_BLK1 ($arg0+0x0094)
  __shx_reg_32 $V320_DRAM_BLK2 ($arg0+0x0098)
  __shx_reg_32 $V320_DRAM_BLK3 ($arg0+0x009c)
  __shx_reg_32 $V320_PCU_SUB0 ($arg0+0x00a0)
  __shx_reg_32 $V320_PCU_SUB1 ($arg0+0x00a4)
  __shx_reg_32 $V320_PCU_SUB2 ($arg0+0x00a8)
  __shx_reg_32 $V320_PCU_TC_WR0 ($arg0+0x00b0)
  __shx_reg_32 $V320_PCU_TC_WR1 ($arg0+0x00b4)
  __shx_reg_32 $V320_PCU_TC_WR2 ($arg0+0x00b8)
  __shx_reg_32 $V320_PCU_TC_WR3 ($arg0+0x00bc)
  __shx_reg_32 $V320_PCU_TC_WR4 ($arg0+0x00c0)
  __shx_reg_32 $V320_PCU_TC_RD0 ($arg0+0x00c8)
  __shx_reg_32 $V320_PCU_TC_RD1 ($arg0+0x00cc)
  __shx_reg_32 $V320_PCU_TC_RD2 ($arg0+0x00d0)
  __shx_reg_32 $V320_PCU_TC_RD3 ($arg0+0x00d4)
  __shx_reg_32 $V320_PCU_TC_RD4 ($arg0+0x00d8)
end

define mb427_fpga_regs
  __shx_reg_32 $FPGA_IO_LED ($arg0+0x0000)
  __shx_reg_32 $FPGA_IO_BUTTON ($arg0+0x0008)
  __shx_reg_32 $FPGA_IO_SWITCH ($arg0+0x0010)
  __shx_reg_32 $FPGA_IO_STATUS ($arg0+0x0018)
  __shx_reg_32 $FPGA_CONF_VERSION ($arg0+0x0020)
  __shx_reg_32 $FPGA_CONF_MEMORY ($arg0+0x0028)
  __shx_reg_32 $FPGA_IO_HEARTBEAT ($arg0+0x0030)
  __shx_reg_32 $FPGA_SOFT_RESET ($arg0+0x0038)
  __shx_reg_32 $FPGA_INTERRUPT_SET ($arg0+0x0080)
  __shx_reg_32 $FPGA_INTERRUPT_CLEAR ($arg0+0x0088)

  # NOTE: [0:2] = buffer size, [3] = disable, [4:7] = latency, [8:31] = 0x5e7040
  __shx_reg_32 $FPGA_L2_CACHE 0xfda00000
end
##}}}

##{{{  MB427 Configure
define set_V320_SDRAM_MA_CMD
  set *$V320_SDRAM_MA_CMD = $arg0
  while (*$V320_SDRAM_MA_CMD & 0x00010000)
  end
end

define mb427_configure
  set *$V320_LB_DRAM_BASE = 0x08000011

  set_V320_SDRAM_MA_CMD 0x001f0000
  set_V320_SDRAM_MA_CMD 0x00bf0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00ff0030

  set *$V320_DRAM_CFG = 0x02401f82
  set *$V320_DRAM_BLK0 = 0x08006a81
  set *$V320_DRAM_BLK1 = 0x10006a80
  set *$V320_DRAM_BLK2 = 0x00000000
  set *$V320_DRAM_BLK3 = 0x00000000
  set *$V320_PCU_SUB0 = 0x08000060
  set *$V320_PCU_SUB1 = 0x0a000060
  set *$V320_PCU_SUB2 = 0x0c000060

  # Switch FPGA to map 0x08000000 to DRAM (via V320) instead of SRAM
  set *$FPGA_CONF_MEMORY = 0xcafca001

  # Enable L2 cache in FPGA
  set *$FPGA_L2_CACHE = 0x5e704003
end

define mb427se_configure
  set *$V320_LB_DRAM_BASE = 0x40000031

  set_V320_SDRAM_MA_CMD 0x001f0000
  set_V320_SDRAM_MA_CMD 0x00bf0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00ff0030

  set *$V320_DRAM_CFG = 0x02401f82
  set *$V320_DRAM_BLK0 = 0x00007c91
  set *$V320_DRAM_BLK1 = 0x00000000
  set *$V320_DRAM_BLK2 = 0x10007c91
  set *$V320_DRAM_BLK3 = 0x00000000
  set *$V320_PCU_SUB0 = 0x08000060
  set *$V320_PCU_SUB1 = 0x0a000060
  set *$V320_PCU_SUB2 = 0x0c000060

  # Switch FPGA to map 0x08000000 to DRAM (via V320) instead of SRAM
  set *$FPGA_CONF_MEMORY = 0xcafca001

  # Enable L2 cache in FPGA
  set *$FPGA_L2_CACHE = 0x5e704003
end
##}}}

##{{{  MB427 PMB Configuration
define mb427se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512
  sh4_set_pmb 1 0xa0 0x01 16

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb427seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 512 0 0 1
  sh4_set_pmb 1 0xa0 0x01 16 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb427se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x48 128
  sh4_set_pmb 2 0x90 0x50 128
  sh4_set_pmb 3 0x98 0x58 64
  sh4_set_pmb 4 0x9c 0x5c 32
  sh4_set_pmb 5 0x9e 0x5e 16
  sh4_set_pmb 6 0x9f 0x01 16
  sh4_set_pmb 7 0xa0 0x40 128 0 0 1
  sh4_set_pmb 8 0xa8 0x48 128 0 0 1
  sh4_set_pmb 9 0xb0 0x50 128 0 0 1
  sh4_set_pmb 10 0xb8 0x58 64 0 0 1
  sh4_set_pmb 11 0xbc 0x5c 32 0 0 1
  sh4_set_pmb 12 0xbe 0x5e 16 0 0 1
  sh4_set_pmb 13 0xbf 0x01 16 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB427 Memory
define mb427_memory_define
  memory-add SRAM  0x01000000 8 RAM
  memory-add Flash 0x04000000 64 ROM
  memory-add DRAM  0x08000000 128 RAM
end

define mb427se_memory_define
  memory-add SRAM  0x01000000 8 RAM
  memory-add Flash 0x04000000 64 ROM
  memory-add DRAM  0x40000000 512 RAM
end

define mb427_sim_memory_define
  sim_addmemory 0x01000000 8 RAM
  sim_addmemory 0x04000000 64 ROM
  sim_addmemory 0x08000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define mb427se_sim_memory_define
  sim_addmemory 0x01000000 8 RAM
  sim_addmemory 0x04000000 64 ROM
  sim_addmemory 0x40000000 512 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb427_setup
  st40300_define
  mb427_memory_define

  st40300_core_si_regs

  st40_l2_regs 0xfd130000

  mb427_v320_regs 0xfd000000
  mb427_fpga_regs 0xfd100000

  mb427_configure

  stmcconfigure l2cache=0xfd130000

  set *$CCN_CCR = 0x8000090d
end

document mb427_setup
Configure an ST40-300 FPGA board
Usage: mb427_setup
end

define mb427se_setup
  st40300_define
  mb427se_memory_define

  st40300_core_si_regs

  st40_l2_regs 0xfd130000

  mb427_v320_regs 0xfd000000
  mb427_fpga_regs 0xfd100000

  mb427se_configure

  mb427se_pmb_configure

  stmcconfigure l2cache=0xfd130000

  set *$CCN_CCR = 0x8000090d
end

document mb427se_setup
Configure an ST40-300 FPGA board with the ST40-300 in 32-bit SE mode
Usage: mb427se_setup
end

define mb427seuc_setup
  st40300_define
  mb427se_memory_define

  st40300_core_si_regs

  st40_l2_regs 0xfd130000

  mb427_v320_regs 0xfd000000
  mb427_fpga_regs 0xfd100000

  mb427se_configure

  mb427seuc_pmb_configure

  stmcconfigure l2cache=0xfd130000

  set *$CCN_CCR = 0x8000090d
end

document mb427seuc_setup
Configure an ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with uncached RAM mappings
Usage: mb427seuc_setup
end

define mb427se29_setup
  st40300_define
  mb427se_memory_define

  st40300_core_si_regs

  st40_l2_regs 0xfd130000

  mb427_v320_regs 0xfd000000
  mb427_fpga_regs 0xfd100000

  mb427se_configure

  mb427se29_pmb_configure

  stmcconfigure l2cache=0xfd130000

  set *$CCN_CCR = 0x8000090d
end

document mb427se29_setup
Configure an ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb427se29_setup
end

define mb427sim_setup
  st40300_define
  mb427_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb427sim_setup
Configure a simulated ST40-300 FPGA board
Usage: mb427sim_setup
end

define mb427simse_setup
  st40300_define
  mb427se_memory_define

  st40300_core_si_regs

  mb427se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb427simse_setup
Configure a simulated ST40-300 FPGA board with the ST40-300 in 32-bit SE mode
Usage: mb427simse_setup
end

define mb427simseuc_setup
  st40300_define
  mb427se_memory_define

  st40300_core_si_regs

  mb427seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb427simseuc_setup
Configure a simulated ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with uncached RAM mappings
Usage: mb427simseuc_setup
end

define mb427simse29_setup
  st40300_define
  mb427se_memory_define

  st40300_core_si_regs

  mb427se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb427simse29_setup
Configure a simulated ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb427simse29_setup
end

define mb427_fsim_setup
  st40300_fsim_core_setup
  mb427_sim_memory_define
end

document mb427_fsim_setup
Configure functional simulator for ST40-300 FPGA board
Usage: mb427_fsim_setup
end

define mb427se_fsim_setup
  st40300_fsim_core_setup
  mb427se_sim_memory_define
end

document mb427se_fsim_setup
Configure functional simulator for ST40-300 FPGA board with the ST40-300 in 32-bit SE mode
Usage: mb427se_fsim_setup
end

define mb427_psim_setup
  st40300_psim_core_setup
  mb427_sim_memory_define
end

document mb427_psim_setup
Configure performance simulator for ST40-300 FPGA board
Usage: mb427_psim_setup
end

define mb427se_psim_setup
  st40300_psim_core_setup
  mb427se_sim_memory_define
end

document mb427se_psim_setup
Configure performance simulator for ST40-300 FPGA board with the ST40-300 in 32-bit SE mode
Usage: mb427se_psim_setup
end

define mb427_display_registers
  st40300_display_core_si_regs
end

document mb427_display_registers
Display the ST40-300 configuration registers
Usage: mb427_display_registers
end
