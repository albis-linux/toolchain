##------------------------------------------------------------------------------
## mb388.cmd - ST40-300 FPGA Board
##------------------------------------------------------------------------------
## Note that configuration registers must be accessed through P4.
##------------------------------------------------------------------------------

##{{{  MB388 Registers
define mb388_v320_regs
  __shx_reg_32 $V320_LB_ROM_BASE ($arg0+0x0074)
  __shx_reg_32 $V320_LB_DRAM_BASE ($arg0+0x0078)
  __shx_reg_32 $V320_LB_BUS_CFG ($arg0+0x007c)
  __shx_reg_32 $V320_SDRAM_MA_CMD ($arg0+0x0088)
  __shx_reg_32 $V320_DRAM_CFG ($arg0+0x008c)
  __shx_reg_32 $V320_DRAM_BLK0 ($arg0+0x0090)
  __shx_reg_32 $V320_DRAM_BLK1 ($arg0+0x0094)
  __shx_reg_32 $V320_DRAM_BLK2 ($arg0+0x0098)
  __shx_reg_32 $V320_DRAM_BLK3 ($arg0+0x009c)
  __shx_reg_32 $V320_PCU_SUB0 ($arg0+0x00a0)
  __shx_reg_32 $V320_PCU_SUB1 ($arg0+0x00a4)
  __shx_reg_32 $V320_PCU_SUB2 ($arg0+0x00a8)
  __shx_reg_32 $V320_PCU_TC_WR0 ($arg0+0x00b0)
  __shx_reg_32 $V320_PCU_TC_WR1 ($arg0+0x00b4)
  __shx_reg_32 $V320_PCU_TC_WR2 ($arg0+0x00b8)
  __shx_reg_32 $V320_PCU_TC_WR3 ($arg0+0x00bc)
  __shx_reg_32 $V320_PCU_TC_WR4 ($arg0+0x00c0)
  __shx_reg_32 $V320_PCU_TC_RD0 ($arg0+0x00c8)
  __shx_reg_32 $V320_PCU_TC_RD1 ($arg0+0x00cc)
  __shx_reg_32 $V320_PCU_TC_RD2 ($arg0+0x00d0)
  __shx_reg_32 $V320_PCU_TC_RD3 ($arg0+0x00d4)
  __shx_reg_32 $V320_PCU_TC_RD4 ($arg0+0x00d8)
end

define mb388_fpga_regs
  __shx_reg_32 $FPGA_IO_LED ($arg0+0x0000)
  __shx_reg_32 $FPGA_IO_BUTTON ($arg0+0x0008)
  __shx_reg_32 $FPGA_IO_SWITCH ($arg0+0x0010)
  __shx_reg_32 $FPGA_IO_STATUS ($arg0+0x0018)
  __shx_reg_32 $FPGA_CONF_VERSION ($arg0+0x0020)
  __shx_reg_32 $FPGA_CONF_MEMORY ($arg0+0x0028)
  __shx_reg_32 $FPGA_IO_HEARTBEAT ($arg0+0x0030)
end
##}}}

##{{{  MB388 Configure
define set_V320_SDRAM_MA_CMD
  set *$V320_SDRAM_MA_CMD = $arg0
  while (*$V320_SDRAM_MA_CMD & 0x00010000)
  end
end

define mb388_configure
  set *$V320_LB_DRAM_BASE = 0x08000011

  set_V320_SDRAM_MA_CMD 0x001f0000
  set_V320_SDRAM_MA_CMD 0x00bf0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00df0400
  set_V320_SDRAM_MA_CMD 0x00ff0030

  set *$V320_DRAM_CFG = 0x02401942
  set *$V320_DRAM_BLK0 = 0x08005771
  set *$V320_DRAM_BLK1 = 0x0c005771
  set *$V320_DRAM_BLK2 = 0x00000000
  set *$V320_DRAM_BLK3 = 0x00000000
  set *$V320_PCU_SUB0 = 0x08000070
  set *$V320_PCU_SUB1 = 0x0c000070
  set *$V320_PCU_SUB2 = 0x00000070
end
##}}}

##{{{  MB388 PMB Configuration
define mb388se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x08 128
  sh4_set_pmb 1 0xa0 0x04 16

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb388seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x08 128 0 0 1
  sh4_set_pmb 1 0xa0 0x04 16 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb388se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x08 128
  sh4_set_pmb 1 0x88 0x04 16
  sh4_set_pmb 2 0xa0 0x08 128 0 0 1
  sh4_set_pmb 3 0xa8 0x04 16 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB388 Memory
define mb388_memory_define
  memory-add Flash 0x00000000 16 ROM
  memory-add SRAM  0x04000000 8 RAM
  memory-add DRAM  0x08000000 128 RAM
end

define mb388_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x04000000 8 RAM
  sim_addmemory 0x08000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb388_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  mb388_v320_regs 0xfd000000
  mb388_fpga_regs 0xfd100000

  mb388_configure

  set *$CCN_CCR = 0x8000090d
end

document mb388_setup
Configure an ST40-300 FPGA board
Usage: mb388_setup
end

define mb388se_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  mb388_v320_regs 0xfd000000
  mb388_fpga_regs 0xfd100000

  mb388_configure

  mb388se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb388se_setup
Configure an ST40-300 FPGA board with the ST40-300 in 32-bit SE mode
Usage: mb388se_setup
end

define mb388seuc_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  mb388_v320_regs 0xfd000000
  mb388_fpga_regs 0xfd100000

  mb388_configure

  mb388seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb388seuc_setup
Configure an ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with uncached RAM mappings
Usage: mb388seuc_setup
end

define mb388se29_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  mb388_v320_regs 0xfd000000
  mb388_fpga_regs 0xfd100000

  mb388_configure

  mb388se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb388se29_setup
Configure an ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb388se29_setup
end

define mb388sim_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb388sim_setup
Configure a simulated ST40-300 FPGA board
Usage: mb388sim_setup
end

define mb388simse_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  mb388se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb388simse_setup
Configure a simulated ST40-300 FPGA board with the ST40-300 in 32-bit SE mode
Usage: mb388simse_setup
end

define mb388simseuc_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  mb388seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb388simseuc_setup
Configure a simulated ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with uncached RAM mappings
Usage: mb388simseuc_setup
end

define mb388simse29_setup
  st40300_define
  mb388_memory_define

  st40300_core_si_regs

  mb388se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb388simse29_setup
Configure a simulated ST40-300 FPGA board with the ST40-300 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb388simse29_setup
end

define mb388_fsim_setup
  st40300_fsim_core_setup
  mb388_sim_memory_define
end

document mb388_fsim_setup
Configure functional simulator for ST40-300 FPGA board
Usage: mb388_fsim_setup
end

define mb388_psim_setup
  st40300_psim_core_setup
  mb388_sim_memory_define
end

document mb388_psim_setup
Configure performance simulator for ST40-300 FPGA board
Usage: mb388_psim_setup
end

define mb388_display_registers
  st40300_display_core_si_regs
end

document mb388_display_registers
Display the ST40-300 configuration registers
Usage: mb388_display_registers
end
