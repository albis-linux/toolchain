################################################################################
## Core SH4 control registers
################################################################################

##{{{  SH4 CCN
## Core control registers (common to all SH4 variants)
define sh4_display_ccn_x_regs
  __shx_display_reg $CCN_ASEEVT
  __shx_display_reg $CCN_BASRS
  __shx_display_reg $CCN_BASRT
  __shx_display_reg $CCN_BASRU
  __shx_display_reg $CCN_BASRV
  __shx_display_reg $CCN_IOBAR
  __shx_display_reg $CCN_IOBDR

  __shx_display_reg $CCN_PMCR1
  __shx_display_reg $CCN_PMCR2
  __shx_display_reg $CCN_PMCTR1H
  __shx_display_reg $CCN_PMCTR1L
  __shx_display_reg $CCN_PMCTR2H
  __shx_display_reg $CCN_PMCTR2L

  __shx_display_reg $CCN_TRMCR
  __shx_display_reg $CCN_TRMBA
  __shx_display_reg $CCN_TRMCTR
end
##}}}

##{{{  SH4 UBC
## User Break Controller control registers (common to all SH4 variants)
define sh4_display_ubc_x_regs
  __shx_display_reg $UBC_BRCRA
  __shx_display_reg $UBC_BRCRS
  __shx_display_reg $UBC_BARS
  __shx_display_reg $UBC_BAMRS
  __shx_display_reg $UBC_BBRS
  __shx_display_reg $UBC_BASRS
  __shx_display_reg $UBC_BART
  __shx_display_reg $UBC_BAMRT
  __shx_display_reg $UBC_BBRT
  __shx_display_reg $UBC_BASRT
  __shx_display_reg $UBC_BARU
  __shx_display_reg $UBC_BAMRU
  __shx_display_reg $UBC_BBRU
  __shx_display_reg $UBC_BASRU
  __shx_display_reg $UBC_BARV
  __shx_display_reg $UBC_BAMRV
  __shx_display_reg $UBC_BBRV
  __shx_display_reg $UBC_BASRV
  __shx_display_reg $UBC_BDRV
  __shx_display_reg $UBC_BDMRV
end
##}}}

##{{{  SH4 UDI
## User Debug Interface control registers (common to all SH4 variants)
define sh4_display_udi_x_regs
  __shx_display_reg $UDI_SDSR
  __shx_display_reg $UDI_SDAR
  __shx_display_reg $UDI_SDARE
end
##}}}

################################################################################

define sh4_display_x_regs
  sh4_display_ccn_x_regs
  sh4_display_ubc_x_regs
  sh4_display_udi_x_regs
end
