################################################################################

define mb411stb7109
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb411stb7109_setup $arg1
  else
    connectsh4le $arg0 mb411stb7109_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109
Connect to and configure an STb7109-Mboard board
Usage: mb411stb7109 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109se
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb411stb7109se_setup $arg1
  else
    connectsh4le $arg0 mb411stb7109se_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109se
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode)
Usage: mb411stb7109se <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109seuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb411stb7109seuc_setup $arg1
  else
    connectsh4le $arg0 mb411stb7109seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109seuc
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109seuc <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109se29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb411stb7109se29_setup $arg1
  else
    connectsh4le $arg0 mb411stb7109se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109se29
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109se29 <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109bypass
  if ($argc > 1)
    mb411stb7109 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109 $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109bypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40
Usage: mb411stb7109bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109sebypass
  if ($argc > 1)
    mb411stb7109se $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109se $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109sebypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb411stb7109sebypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109seucbypass
  if ($argc > 1)
    mb411stb7109seuc $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109seuc $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109seucbypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109seucbypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109se29bypass
  if ($argc > 1)
    mb411stb7109se29 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109se29 $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109se29bypass
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109se29bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109stmmx
  if ($argc > 1)
    mb411stb7109 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109stmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7109stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109sestmmx
  if ($argc > 1)
    mb411stb7109se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109se $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109sestmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb411stb7109sestmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109seucstmmx
  if ($argc > 1)
    mb411stb7109seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109seuc $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109seucstmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109seucstmmx <target>
where <target> is an ST Micro Connect name or IP address
end

define mb411stb7109se29stmmx
  if ($argc > 1)
    mb411stb7109se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109se29 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109se29stmmx
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109se29stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb411stb7109usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb411stb7109_setup $arg1
  else
    connectsh4usble $arg0 mb411stb7109_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109usb
Connect to and configure an STb7109-Mboard board
Usage: mb411stb7109usb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109seusb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb411stb7109se_setup $arg1
  else
    connectsh4usble $arg0 mb411stb7109se_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109seusb
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode)
Usage: mb411stb7109seusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109seucusb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb411stb7109seuc_setup $arg1
  else
    connectsh4usble $arg0 mb411stb7109seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109seucusb
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109seucusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109se29usb
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb411stb7109se29_setup $arg1
  else
    connectsh4usble $arg0 mb411stb7109se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb411stb7109se29usb
Connect to and configure an STb7109-Mboard board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109se29usb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109bypassusb
  if ($argc > 1)
    mb411stb7109usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109bypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40
Usage: mb411stb7109bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109sebypassusb
  if ($argc > 1)
    mb411stb7109seusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109seusb $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109sebypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: mb411stb7109sebypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109seucbypassusb
  if ($argc > 1)
    mb411stb7109seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109seucusb $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109seucbypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109seucbypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109se29bypassusb
  if ($argc > 1)
    mb411stb7109se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    mb411stb7109se29usb $arg0 "jtagpinout=st40 jtagreset -inicommand mb411stb7109bypass_setup"
  end
end

document mb411stb7109se29bypassusb
Connect to and configure an STb7109-Mboard board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109se29bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109stmmxusb
  if ($argc > 1)
    mb411stb7109usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109stmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux
Usage: mb411stb7109stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109sestmmxusb
  if ($argc > 1)
    mb411stb7109seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109seusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109sestmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: mb411stb7109sestmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109seucstmmxusb
  if ($argc > 1)
    mb411stb7109seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109seucusb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109seucstmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109seucstmmxusb <target>
where <target> is an ST Micro Connect USB name
end

define mb411stb7109se29stmmxusb
  if ($argc > 1)
    mb411stb7109se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    mb411stb7109se29usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand mb411stb7109stmmx_setup"
  end
end

document mb411stb7109se29stmmxusb
Connect to and configure an STb7109-Mboard board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109se29stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb411stb7109sim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb411stb7109_fsim_setup mb411stb7109sim_setup $arg0
  else
    connectsh4simle mb411stb7109_fsim_setup mb411stb7109sim_setup ""
  end
end

document mb411stb7109sim
Connect to and configure a simulated STb7109-Mboard board
Usage: mb411stb7109sim
end

define mb411stb7109simse
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb411stb7109se_fsim_setup mb411stb7109simse_setup $arg0
  else
    connectsh4simle mb411stb7109se_fsim_setup mb411stb7109simse_setup ""
  end
end

document mb411stb7109simse
Connect to and configure a simulated STb7109-Mboard board (STb7109 in 32-bit SE mode)
Usage: mb411stb7109simse
end

define mb411stb7109simseuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb411stb7109se_fsim_setup mb411stb7109simseuc_setup $arg0
  else
    connectsh4simle mb411stb7109se_fsim_setup mb411stb7109simseuc_setup ""
  end
end

document mb411stb7109simseuc
Connect to and configure a simulated STb7109-Mboard board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109simseuc
end

define mb411stb7109simse29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb411stb7109se_fsim_setup mb411stb7109simse29_setup $arg0
  else
    connectsh4simle mb411stb7109se_fsim_setup mb411stb7109simse29_setup ""
  end
end

document mb411stb7109simse29
Connect to and configure a simulated STb7109-Mboard board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109simse29
end

################################################################################

define mb411stb7109psim
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb411stb7109_psim_setup mb411stb7109sim_setup $arg0
  else
    connectsh4psimle mb411stb7109_psim_setup mb411stb7109sim_setup ""
  end
end

document mb411stb7109psim
Connect to and configure a simulated STb7109-Mboard board
Usage: mb411stb7109psim
end

define mb411stb7109psimse
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb411stb7109se_psim_setup mb411stb7109simse_setup $arg0
  else
    connectsh4psimle mb411stb7109se_psim_setup mb411stb7109simse_setup ""
  end
end

document mb411stb7109psimse
Connect to and configure a simulated STb7109-Mboard board (STb7109 in 32-bit SE mode)
Usage: mb411stb7109psimse
end

define mb411stb7109psimseuc
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb411stb7109se_psim_setup mb411stb7109simseuc_setup $arg0
  else
    connectsh4psimle mb411stb7109se_psim_setup mb411stb7109simseuc_setup ""
  end
end

document mb411stb7109psimseuc
Connect to and configure a simulated STb7109-Mboard board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: mb411stb7109psimseuc
end

define mb411stb7109psimse29
  source register40.cmd
  source display40.cmd
  source stb7100boot.cmd
  source stb7100clocks.cmd
  source stb7109.cmd
  source mb411stb7109.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb411stb7109se_psim_setup mb411stb7109simse29_setup $arg0
  else
    connectsh4psimle mb411stb7109se_psim_setup mb411stb7109simse29_setup ""
  end
end

document mb411stb7109psimse29
Connect to and configure a simulated STb7109-Mboard board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb411stb7109psimse29
end

################################################################################
