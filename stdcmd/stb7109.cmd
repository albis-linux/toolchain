##
## STb7109 device description (on-chip memory and registers)
##

init-if-undefined $_stb7109movelmiregs = 1
keep-variable $_stb7109movelmiregs

define stb7109_memory_define
  ## Configuration registers on SuperHyway (via P2)
  if (!$_stb7109movelmiregs)
  memory-add LMISYS_regs 0x0f000000 16 DEV
  memory-add LMIVID_regs 0x17000000 16 DEV
  end
  memory-add ST_regs     0x18000000 64 DEV

  ## Configuration registers in P4
  memory-add STOREQ_data   0xe0000000 64 DEV
  memory-add ICACHE_addr   0xf0000000 16 DEV
  memory-add ICACHE_data   0xf1000000 16 DEV
  memory-add ITLB_addr     0xf2000000 16 DEV
  memory-add ITLB_data     0xf3000000 16 DEV
  memory-add OCACHE_addr   0xf4000000 16 DEV
  memory-add OCACHE_data   0xf5000000 16 DEV
  memory-add UTLB_PMB_addr 0xf6000000 16 DEV
  memory-add UTLB_PMB_data 0xf7000000 16 DEV
  memory-add CORE_regs     0xfc000000 64 DEV
end

define stb7109_define
  ## processor ST40
  ## chip STb7109

  stb7109_memory_define
end

define stb7109_fsim_core_setup
  sim_command "config +cpu.sh4_family=fpu"
  sim_command "config +cpu.sh7751_features=false"
  sim_command "config +cpu.icache.nr_partitions=2"
  sim_command "config +cpu.dcache.nr_partitions=2"
  sim_reset
end

define stb7109_psim_core_setup
  sim_command "config +cpu.cache_store_on_fill=true"
  sim_command "config +use_tobu=false"
  stb7109_fsim_core_setup
end
