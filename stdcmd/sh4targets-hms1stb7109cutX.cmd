################################################################################

define hms1stb7109cutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hms1stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
  connectsh4le $arg1 hms1stb7109_setup $arg2
  else
  connectsh4le $arg1 hms1stb7109_setup "jtagpinout=st40 hardreset"
  end
end

document hms1stb7109cutX
Connect to and configure a Simple Devices HMS1 board
Usage: hms1stb7109cutX <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109secutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hms1stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
  connectsh4le $arg1 hms1stb7109se_setup $arg2
  else
  connectsh4le $arg1 hms1stb7109se_setup "jtagpinout=st40 hardreset"
  end
end

document hms1stb7109secutX
Connect to and configure a Simple Devices HMS1 board (STb7109 in 32-bit SE mode)
Usage: hms1stb7109secutX <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109seuccutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hms1stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
  connectsh4le $arg1 hms1stb7109seuc_setup $arg2
  else
  connectsh4le $arg1 hms1stb7109seuc_setup "jtagpinout=st40 hardreset"
  end
end

document hms1stb7109seuccutX
Connect to and configure a Simple Devices HMS1 board (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hms1stb7109seuccutX <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109se29cutX
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7109.cmd
  source hms1stb7109cut$arg0.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 2)
  connectsh4le $arg1 hms1stb7109se29_setup $arg2
  else
  connectsh4le $arg1 hms1stb7109se29_setup "jtagpinout=st40 hardreset"
  end
end

document hms1stb7109se29cutX
Connect to and configure a Simple Devices HMS1 board (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hms1stb7109se29cutX <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109cutXbypass
  if ($argc > 2)
  hms1stb7109cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
  hms1stb7109cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand hms1stb7109bypass_setup"
  end
end

document hms1stb7109cutXbypass
Connect to and configure a Simple Devices HMS1 board bypassing to the ST40
Usage: hms1stb7109cutXbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109secutXbypass
  if ($argc > 2)
  hms1stb7109secutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
  hms1stb7109secutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand hms1stb7109bypass_setup"
  end
end

document hms1stb7109secutXbypass
Connect to and configure a Simple Devices HMS1 board bypassing to the ST40 (STb7109 in 32-bit SE mode)
Usage: hms1stb7109secutXbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109seuccutXbypass
  if ($argc > 2)
  hms1stb7109seuccutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
  hms1stb7109seuccutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand hms1stb7109bypass_setup"
  end
end

document hms1stb7109seuccutXbypass
Connect to and configure a Simple Devices HMS1 board bypassing to the ST40 (STb7109 in 32-bit SE mode with uncached mappings)
Usage: hms1stb7109seuccutXbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109se29cutXbypass
  if ($argc > 2)
  hms1stb7109se29cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand $arg2"
  else
  hms1stb7109se29cutX $arg0 $arg1 "jtagpinout=st40 jtagreset -inicommand hms1stb7109bypass_setup"
  end
end

document hms1stb7109se29cutXbypass
Connect to and configure a Simple Devices HMS1 board bypassing to the ST40 (STb7109 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hms1stb7109se29cutXbypass <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109cutXstmmx
  if ($argc > 2)
  hms1stb7109cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
  hms1stb7109cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hms1stb7109stmmx_setup"
  end
end

document hms1stb7109cutXstmmx
Connect to and configure a Simple Devices HMS1 board via an ST MultiCore/Mux
Usage: hms1stb7109cutXstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109secutXstmmx
  if ($argc > 2)
  hms1stb7109secutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
  hms1stb7109secutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hms1stb7109stmmx_setup"
  end
end

document hms1stb7109secutXstmmx
Connect to and configure a Simple Devices HMS1 board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: hms1stb7109secutXstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109seuccutXstmmx
  if ($argc > 2)
  hms1stb7109seuccutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
  hms1stb7109seuccutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hms1stb7109stmmx_setup"
  end
end

document hms1stb7109seuccutXstmmx
Connect to and configure a Simple Devices HMS1 board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: hms1stb7109seuccutXstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

define hms1stb7109se29cutXstmmx
  if ($argc > 2)
  hms1stb7109se29cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg2"
  else
  hms1stb7109se29cutX $arg0 $arg1 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hms1stb7109stmmx_setup"
  end
end

document hms1stb7109se29cutXstmmx
Connect to and configure a Simple Devices HMS1 board via an ST MultiCore/Mux (STb7109 in 32-bit SE mode)
Usage: hms1stb7109se29cutXstmmx <cut> <target>
where <cut> is the STb7109 silicon cut version
+     <target> is an ST Micro Connect name or IP address
end

################################################################################
