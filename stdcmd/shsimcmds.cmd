# Simulator Instruction Tracing

define chess-insttrace
  monitor SuperH-Chess_InstTrace-$arg0
end

maintenance deprecate chess-insttrace "sim_insttrace"

define sim_insttrace
  monitor SuperH-Chess_InstTrace-$arg0
end

document sim_insttrace
Enable or disable the listing of executed instructions
within the simulation.

Takes one argument:
on - Turn instruction trace on.
off - Turn instruction trace off.
end

# Simulator Branch Tracing

define chess-branchtrace
  monitor SuperH-Chess_BranchTrace-$arg0
end

maintenance deprecate chess-branchtrace "sim_branchtrace"

define sim_branchtrace
  monitor SuperH-Chess_BranchTrace-$arg0
end

document sim_branchtrace
Enable or disable the listing of branch instructions
within the simulation.

Takes one argument:
on - Turn branch trace on.
off - Turn branch trace off.
end

# Simulation Census information

define chess-census
  monitor SuperH-Chess_Census-$arg0
end

maintenance deprecate chess-census "sim_census"

define sim_census
  monitor SuperH-Chess_Census-$arg0
end

document sim_census
Enable or disable the taking of census information
within the simulation.

Takes one argument:
on
off
'string' is replaced by a user defined string in these cases.
autosave:'string'
open:'string'
end

# Simulation Trace information

define chess-trace
  monitor SuperH-Chess_Trace-$arg0
end

maintenance deprecate chess-trace "sim_trace"

define sim_trace
  monitor SuperH-Chess_Trace-$arg0
end

document sim_trace
Enable or disable the taking of trace information
within the simulation.

Takes one argument:
on
off
'string' is replaced by a user defined string in these cases.
open:'string'
close
end

# Simulation Command Strings

define chess-command
  monitor SuperH-Chess_SiteCmd-$arg0
end

maintenance deprecate chess-command "sim_command"

define sim_command
  monitor SuperH-Chess_SiteCmd-$arg0
end

document sim_command
Pass a command to the simulator.
end

# Simulation Configuration

define chess-config
  monitor SuperH-Chess_Config-$arg0
end

maintenance deprecate chess-config "sim_config"

define sim_config
  monitor SuperH-Chess_Config-$arg0
end

document sim_config
Pass a configuration command to the simulator.
end

# Simulation Memory Configuration commands

define chess-addmemory
  monitor SuperH-Chess_AddMemRegion-$arg0,$arg1,$arg2
end

maintenance deprecate chess-addmemory "sim_addmemory"

define sim_addmemory
  monitor SuperH-Chess_AddMemRegion-$arg0,$arg1,$arg2
end

document sim_addmemory
Add a memory region to the simulator

arg0=address in hex
arg1=size in Mb (i.e 16=16 Mb)
arg2=type (RAM, ROM or DEV)
end

# Simulation Reset Command

define chess-reset
  monitor SuperH-Chess_Reset
end

maintenance deprecate chess-reset "sim_reset"

define sim_reset
  monitor SuperH-Chess_Reset
end

document sim_reset
Reset the simulator.
end
