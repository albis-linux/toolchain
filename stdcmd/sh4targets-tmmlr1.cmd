################################################################################

define tmmlr1
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr1.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 tmmlr1_setup $arg1
  else
    connectsh4le $arg0 tmmlr1_setup "hardreset"
  end
end

document tmmlr1
Connect to and configure an STm8000-TMM board
Usage: tmmlr1 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define tmmlr1usb
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr1.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 tmmlr1_setup $arg1
  else
    connectsh4usble $arg0 tmmlr1_setup "hardreset"
  end
end

document tmmlr1usb
Connect to and configure an STm8000-TMM board
Usage: tmmlr1usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define tmmlr1sim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr1.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle tmmlr1_fsim_setup tmmlr1sim_setup $arg0
  else
    connectsh4simle tmmlr1_fsim_setup tmmlr1sim_setup ""
  end
end

document tmmlr1sim
Connect to and configure a simulated STm8000-TMM board
Usage: tmmlr1sim
end

################################################################################

define tmmlr1psim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source tmmlr1.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle tmmlr1_psim_setup tmmlr1sim_setup $arg0
  else
    connectsh4psimle tmmlr1_psim_setup tmmlr1sim_setup ""
  end
end

document tmmlr1psim
Connect to and configure a simulated STm8000-TMM board
Usage: tmmlr1psim
end

################################################################################
