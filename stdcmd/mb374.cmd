##------------------------------------------------------------------------------
## mb374.cmd - ST40RA-Starter Board MB374
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB374 EMI Configuration
define mb374_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000006

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x0000000c
  set *$EMI_BANK3_BASEADDRESS = 0x00000014
  set *$EMI_BANK4_BASEADDRESS = 0x00000018
  set *$EMI_BANK5_BASEADDRESS = 0x0000001c

##------------------------------------------------------------------------------
## Program bank functions
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Bank 0 - On-board Flash
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 16Mb"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 32Mb"

##------------------------------------------------------------------------------
## Bank 2 - STEM Module (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Undefined 32Mb"

##------------------------------------------------------------------------------
## Bank 3 - MPX Mode (unused)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Configured for MPX Mode 16Mb (unused)"

  set *$EMI_BANK3_EMICONFIGDATA0 = 0x0400064b
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x00000000
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x00000000
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 4 - MPX Mode (unused)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for MPX Mode 16Mb (unused)"

  set *$EMI_BANK4_EMICONFIGDATA0 = 0x0400064b
  set *$EMI_BANK4_EMICONFIGDATA1 = 0x00000000
  set *$EMI_BANK4_EMICONFIGDATA2 = 0x00000000
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 5 - EPLD Registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 5: Configured for EPLD Registers 15Mb"

  set *$EMI_BANK5_EMICONFIGDATA0 = 0x001016e9
  set *$EMI_BANK5_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK5_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK5_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000000

  ## _ST_display (_procname) "EMI FLASH CLOCK @ 1/3 bus clock"
  set *$EMI_FLASHCLKSEL = 0x00000002

  ## _ST_display (_procname) "EMI MPX CLOCK @ 1/2 bus clock"
  set *$EMI_MPXCLKSEL = 0x00000001

  set *$EMI_CLKENABLE = 0x00000001
end
##}}}

##{{{  MB374 LMI Configuration
define mb374_lmi_configure
  ## _ST_display (_procname) "Configuring LMI for 64-bit PC266 DDR-SDRAM burst length 4"

##------------------------------------------------------------------------------
## Program system configuration registers
##------------------------------------------------------------------------------

  ## NOTE:
  ##   bit 8 = 0 for SSTL (DDR), 1 for LVTTL (PC-SDRAM)
  ##   bit 9 = 0 for internal ref voltage, 1 for external ref voltage
  ##   bit 10 = 1 to by pass ECLK180 retime
  ##   bit 11 = 1 for PC-SDRAM, 0 for DDR notLMICOMP25_EN
  ##   bit 12 = 1 for PC-SDRAM, 0 for DDR LMICOMP33_EN
  ##   bits [13,14] = LVTTL (DDR) drive strength for data & data strobe pads
  ##   bits [15,16] = LVTTL (DDR) drive strength for address & control pads

  set *$SYSCONF_SYS_CON2_0 = 0x00000200

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI_MIM_0 = 0x04000283

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI_STR_0 = 0x000011db

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI_SDRA0_0 = 0x0c001500

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI_SDRA1_0 = 0x0c001500

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000003
  set *$LMI_SCR_0 = 0x00000001
  set *$LMI_SCR_0 = 0x00000002
  set *($LMI_SDMR0+(0x00002000/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00002000/sizeof(*$LMI_SDMR1))) = 0x0
  set *($LMI_SDMR0+(0x00000b10/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000b10/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000002
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *$LMI_SCR_0 = 0x00000004
  set *($LMI_SDMR0+(0x00000310/sizeof(*$LMI_SDMR0))) = 0x0
  set *($LMI_SDMR1+(0x00000310/sizeof(*$LMI_SDMR1))) = 0x0
  set *$LMI_SCR_0 = 0x00000000
end
##}}}

##{{{  MB374 Memory
define mb374_memory_define
  memory-add Flash        0x00000000 16 ROM
  memory-add MPX_unused_0 0x05000000 16 RAM
  memory-add MPX_unused_1 0x06000000 16 RAM
  memory-add EPLD_regs    0x07000000 15 RAM
  memory-add LMI_SDRAM    0x08000000 64 RAM
end

define mb374_sim_memory_define
  sim_addmemory 0x00000000 16 ROM
  sim_addmemory 0x08000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb374_setup
  st40ra_define
  mb374_memory_define

  st40ra_si_regs

  mb374_emi_configure
  mb374_lmi_configure

  set *$CCN_CCR = 0x0000090d
end

document mb374_setup
Configure an ST40RA-Starter board
Usage: mb374_setup
end

define mb374sim_setup
  st40ra_define
  mb374_memory_define

  st40ra_si_regs

  set *$CCN_CCR = 0x0000090d
end

document mb374sim_setup
Configure a simulated ST40RA-Starter board
Usage: mb374sim_setup
end

define mb374_fsim_setup
  st40ra_fsim_core_setup
  mb374_sim_memory_define
end

document mb374_fsim_setup
Configure functional simulator for ST40RA-Starter board
Usage: mb374_fsim_setup
end

define mb374_psim_setup
  st40ra_psim_core_setup
  mb374_sim_memory_define
end

document mb374_psim_setup
Configure performance simulator for ST40RA-Starter board
Usage: mb374_psim_setup
end

define mb374_display_registers
  st40ra_display_si_regs
end

document mb374_display_registers
Display the ST40RA configuration registers
Usage: mb374_display_registers
end
