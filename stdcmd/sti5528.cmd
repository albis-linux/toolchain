##
## STi5528 device description (on-chip memory and registers)
##

define sti5528_memory_define
  ## Configuration registers on SuperHyway (via P2)
  memory-add LMI_regs    0x0f000000 16 DEV
  memory-add PCI_regs    0x17000000 16 DEV
  memory-add ST_regs     0x18000000 64 DEV

  ## Configuration registers in P4
  memory-add STOREQ_data 0xe0000000 64 DEV
  memory-add ICACHE_addr 0xf0000000 16 DEV
  memory-add ICACHE_data 0xf1000000 16 DEV
  memory-add ITLB_addr   0xf2000000 16 DEV
  memory-add ITLB_data   0xf3000000 16 DEV
  memory-add OCACHE_addr 0xf4000000 16 DEV
  memory-add OCACHE_data 0xf5000000 16 DEV
  memory-add UTLB_addr   0xf6000000 16 DEV
  memory-add UTLB_data   0xf7000000 16 DEV
  memory-add CORE_regs   0xfc000000 64 DEV
end

define sti5528_define
  ## processor ST40
  ## chip STi5528

  sti5528_memory_define
end

define sti5528_fsim_core_setup
  sim_command "config +cpu.sh4_family=fpu"
  sim_command "config +cpu.sh7751_features=true"
  sim_command "config +cpu.icache.nr_partitions=1"
  sim_command "config +cpu.dcache.nr_partitions=1"
  sim_reset
end

define sti5528_psim_core_setup
  sim_command "config +cpu.cache_store_on_fill=false"
  sti5528_fsim_core_setup
end
