##------------------------------------------------------------------------------
## db507.cmd - SDRAM STEM Module DB507 (32Mb)
##------------------------------------------------------------------------------

define db507_emi_configure
  set *$EMI_BANK$arg0_EMICONFIGDATA0 = 0x0400044a
  set *$EMI_BANK$arg0_EMICONFIGDATA1 = 0x0003fffc
  set *$EMI_BANK$arg0_EMICONFIGDATA2 = 0x00000020
  set *$EMI_BANK$arg0_EMICONFIGDATA3 = 0x00002929

  set *$EMI_SDRAMCLKSEL = 0x00000001
  set *$EMI_SDRAMNOPGEN = 0x00000001
  set *$EMI_REFRESHINIT = 0x000003e6
  set *$EMI_SDRAMMODEREG = 0x00000032
  set *$EMI_SDRAMINIT = 0x00000001
end
