################################################################################

define hdk7111sim
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7111_fsim_setup hdk7111sim_setup $arg0
  else
    connectsh4simle hdk7111_fsim_setup hdk7111sim_setup ""
  end
end

document hdk7111sim
Connect to and configure a simulated STi7111-HDK board
Usage: hdk7111sim
end

define hdk7111simse
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7111se_fsim_setup hdk7111simse_setup $arg0
  else
    connectsh4simle hdk7111se_fsim_setup hdk7111simse_setup ""
  end
end

document hdk7111simse
Connect to and configure a simulated STi7111-HDK board (STi7111 in 32-bit SE mode)
Usage: hdk7111simse
end

define hdk7111simseuc
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7111se_fsim_setup hdk7111simseuc_setup $arg0
  else
    connectsh4simle hdk7111se_fsim_setup hdk7111simseuc_setup ""
  end
end

document hdk7111simseuc
Connect to and configure a simulated STi7111-HDK board (STi7111 in 32-bit SE mode with uncached mappings)
Usage: hdk7111simseuc
end

define hdk7111simse29
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hdk7111se_fsim_setup hdk7111simse29_setup $arg0
  else
    connectsh4simle hdk7111se_fsim_setup hdk7111simse29_setup ""
  end
end

document hdk7111simse29
Connect to and configure a simulated STi7111-HDK board (STi7111 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk7111simse29
end

################################################################################

define hdk7111psim
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7111_psim_setup hdk7111sim_setup $arg0
  else
    connectsh4psimle hdk7111_psim_setup hdk7111sim_setup ""
  end
end

document hdk7111psim
Connect to and configure a simulated STi7111-HDK board
Usage: hdk7111psim
end

define hdk7111psimse
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7111se_psim_setup hdk7111simse_setup $arg0
  else
    connectsh4psimle hdk7111se_psim_setup hdk7111simse_setup ""
  end
end

document hdk7111psimse
Connect to and configure a simulated STi7111-HDK board (STi7111 in 32-bit SE mode)
Usage: hdk7111psimse
end

define hdk7111psimseuc
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7111se_psim_setup hdk7111simseuc_setup $arg0
  else
    connectsh4psimle hdk7111se_psim_setup hdk7111simseuc_setup ""
  end
end

document hdk7111psimseuc
Connect to and configure a simulated STi7111-HDK board (STi7111 in 32-bit SE mode with uncached mappings)
Usage: hdk7111psimseuc
end

define hdk7111psimse29
  source register40.cmd
  source display40.cmd
  source sti7111.cmd
  source hdk7111.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hdk7111se_psim_setup hdk7111simse29_setup $arg0
  else
    connectsh4psimle hdk7111se_psim_setup hdk7111simse29_setup ""
  end
end

document hdk7111psimse29
Connect to and configure a simulated STi7111-HDK board (STi7111 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: hdk7111psimse29
end

################################################################################
