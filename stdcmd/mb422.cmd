##------------------------------------------------------------------------------
## mb422.cmd - DTV100-DB Development Platform MB422
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  MB422 CLOCKGEN Configuration
define mb422_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  set *$CLOCKGEN_CTL_SEL = 0x00000005
  set *$CLOCKGEN_CTL_EN = 0x000ca000
  set *$CLOCKGEN_CTL_PLL2 = 0x30002506

  while ((*$CLOCKGEN_STATUS_PLL & 0x00000002) == 0)
  end

  set *$CLOCKGEN_CTL_EN = 0x000ca001

  while ((*$GENCONF_LMI1_STATUS & 0x00000001) == 0)
  end

  while ((*$GENCONF_LMI1_STATUS & 0x00000002) == 0)
  end

  while ((*$GENCONF_LMI2_STATUS & 0x00000001) == 0)
  end

  while ((*$GENCONF_LMI2_STATUS & 0x00000002) == 0)
  end
end
##}}}

##{{{  MB422 EMI Configuration
define mb422_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x04001691
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x0a000000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x0e004400
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000
end
##}}}

##{{{  MB422 LMI Configuration
define mb422_mixer_common_configure
  set *$MIXER$arg0_MODC = 0x00000000
  set *$MIXER$arg0_FILC = 0x0000003f
  set *$MIXER$arg0_BTAC = 0x0000070e
  set *$MIXER$arg0_HPC = 0x00000a01
  set *$MIXER$arg0_RDBSC = 0x0004060a
  set *$MIXER$arg0_WDBSC = 0x000f0f0f
  set *$MIXER$arg0_OBCLC = 0x00080010
  set *$MIXER$arg0_BKCLC = 0x00200018
  set *$MIXER$arg0_LPBWC = 0x0a1e04b0
  set *$MIXER$arg0_GENPC0 = 0x00000000
  set *$MIXER$arg0_GENPC1 = 0x00000000
end

define mb422_lmi1_configure
  ## _ST_display (_procname) "Configuring LMI-1 for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI-1 MIXER registers
##------------------------------------------------------------------------------

  mb422_mixer_common_configure 1

  set *$MIXER1_ATC = 0x00050706
  set *$MIXER1_LMIAC = 0x00080009
  set *$MIXER1_PMC = 0x1fffff00

##------------------------------------------------------------------------------
## Program LMI-1 registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI1_MIM_0 = 0x0514005f

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI1_STR_0 = 0x35ad4345

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI1_SDRA0_0 = 0xafe01a03

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI1_SDRA1_0 = 0xafe01a03

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI1_SCR_0 = 0x00000001
  set *$LMI1_SCR_0 = 0x00000003
  set *$LMI1_SCR_0 = 0x00000001
  set *$LMI1_SCR_0 = 0x00000002
  set *$LMI1_SCR_0 = 0x00000001
  set *$LMI1_SDMR0 = 0x00000400
  set *$LMI1_SDMR0 = 0x00000163
  sleep 0 200
  set *$LMI1_SCR_0 = 0x00000002
  set *$LMI1_SCR_0 = 0x00000004
  set *$LMI1_SDMR0 = 0x00000063
  set *$LMI1_MIM_0 |= 0x00000200
  set *$LMI1_SCR_0 = 0x00000000

  ## _ST_display (_procname) "LMI Delay Registers"
  set *$GENCONF_LMI1_PDL_0_3_MSB_CTL = 0xd0d0d0d0
  set *$GENCONF_LMI1_PDL_0_3_LSB_CTL = 0x0000000f
end

define mb422_lmi2_configure
  ## _ST_display (_procname) "Configuring LMI-2 for DDR SDRAM"

##------------------------------------------------------------------------------
## Program LMI-2 MIXER registers
##------------------------------------------------------------------------------

  mb422_mixer_common_configure 2

  set *$MIXER2_ATC = 0x00050706
  set *$MIXER2_LMIAC = 0x00170010
  set *$MIXER2_PMC = 0x1fffff00

##------------------------------------------------------------------------------
## Program LMI-2 registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  set *$LMI2_MIM_0 = 0x0514005f

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI2_STR_0 = 0x35ad4345

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMI2_SDRA0_0 = 0xb6e01a03

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMI2_SDRA1_0 = 0xb6e01a03

  ## _ST_display (_procname) "SDRAM Control Register"
  set *$LMI2_SCR_0 = 0x00000001
  set *$LMI2_SCR_0 = 0x00000003
  set *$LMI2_SCR_0 = 0x00000001
  set *$LMI2_SCR_0 = 0x00000002
  set *$LMI2_SCR_0 = 0x00000001
  set *$LMI2_SDMR0 = 0x00000400
  set *$LMI2_SDMR0 = 0x00000163
  sleep 0 200
  set *$LMI2_SCR_0 = 0x00000002
  set *$LMI2_SCR_0 = 0x00000004
  set *$LMI2_SDMR0 = 0x00000063
  set *$LMI2_MIM_0 |= 0x00000200
  set *$LMI2_SCR_0 = 0x00000000

  ## _ST_display (_procname) "LMI Delay Registers"
  set *$GENCONF_LMI2_PDL_0_3_MSB_CTL = 0xd0d0d0d0
  set *$GENCONF_LMI2_PDL_0_3_LSB_CTL = 0x0000000f
end
##}}}

##{{{  MB422 Memory
define mb422_memory_define
  memory-add Flash      0x00000000 8 ROM
  memory-add LMI1_SDRAM 0x09000000 112 RAM
  memory-add LMI2_SDRAM 0x10000000 112 RAM
end

define mb422_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x09000000 112 RAM
  sim_addmemory 0x10000000 112 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define mb422_setup
  std2000_define
  mb422_memory_define

  std2000_si_regs

  mb422_clockgen_configure
  mb422_emi_configure
  mb422_lmi1_configure
  mb422_lmi2_configure

  set *$CCN_CCR = 0x8000090d
end

document mb422_setup
Configure a DTV100-DB board
Usage: mb422_setup
end

define mb422sim_setup
  std2000_define
  mb422_memory_define

  std2000_si_regs

  set *$CCN_CCR = 0x8000090d
end

document mb422sim_setup
Configure a simulated DTV100-DB board
Usage: mb422sim_setup
end

define mb422_fsim_setup
  std2000_fsim_core_setup
  mb422_sim_memory_define
end

document mb422_fsim_setup
Configure functional simulator for DTV100-DB board
Usage: mb422_fsim_setup
end

define mb422_psim_setup
  std2000_psim_core_setup
  mb422_sim_memory_define
end

document mb422_psim_setup
Configure performance simulator for DTV100-DB board
Usage: mb422_psim_setup
end

define mb422_display_registers
  std2000_display_si_regs
end

document mb422_display_registers
Display the STd2000 configuration registers
Usage: mb422_display_registers
end
