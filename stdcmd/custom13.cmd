##################################################
##                                              ##
## STb7100-Custom13 Platform configuration file ##
##                                              ##
##################################################

##{{{  CUSTOM13 CLOCKGEN Configuration
############################
## Clockgen configuration ##
############################

define custom13_clockgen_configure
  linkspeed 2.5MHz

  ## Set PLL0 to 531MHz
  stb7100_set_clockgen_a_pll0 0x06 0x3b 0x0

  if ($_custom13_stb7100_version == 3)
    ## Cut 3.x or above - set PLL1 to 400MHz ##
    stb7100_set_clockgen_a_pll1 0x1b 0xc8 0x0
  end

  if ($_custom13_stb7100_version == 2)
    ## Cut 2.x - set PLL1 to 384MHz ##
    stb7100_set_clockgen_a_pll1 0x09 0x80 0x1
  end

  if ($_custom13_stb7100_version == 1)
    if 1
      ## Cut 1.3 - set PLL1 to 330MHz ##
      stb7100_set_clockgen_a_pll1 0x1b 0xa5 0x0
    else
      ## Cut 1.1 - set PLL1 to 330MHz ##
      stb7100_set_clockgen_a_pll1 0x1b 0xa5 0x0
    end
  end

  ## clockOUT is EMI clock
  set *$CLOCKGENA_OUT_CTRL = 0x0000000b

  ## Restore default link speed (5MHz maximum for TapMux)
  if ($_stmmxmode)
    linkspeed 5MHz
  else
    linkspeed 10MHz
  end
end
##}}}

##{{{  CUSTOM13 SYSCONF Configuration
##########################
## System configuration ##
##########################

define custom13_sysconf_configure
  if ($_custom13_stb7100_version > 1)
    ## Cut 2.0 or above ##
    set *$SYSCONF_SYS_CFG11 = 0x0807fec0
  else
    ## Cut 1.x ##
    set *$SYSCONF_SYS_CFG11 = 0x080780c0
  end

  while ((*$SYSCONF_SYS_STA12 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  while ((*$SYSCONF_SYS_STA13 & ((1 << 9) | (1 << 19))) != ((1 << 9) | (1 << 19)))
  end

  set *$SYSCONF_SYS_CFG12 = 0x4000000f | (0xf << 12) | (0xf << 23)
  set *$SYSCONF_SYS_CFG13 = 0x4000000f | (0xf << 12) | (0xf << 23)

  if ($_custom13_stb7100_version == 1)
    ## Cut 1.3 ##
    if 1
      set *$SYSCONF_SYS_CFG14 = (1 << 18) | (0x5a << 20)
      set *$SYSCONF_SYS_CFG15 = (1 << 19) | (0x79 << 20)
      set *$SYSCONF_SYS_CFG20 = (1 << 18) | (0x5a << 20)
      set *$SYSCONF_SYS_CFG21 = (1 << 19) | (0x79 << 20)
    ## Cut 1.1 ##
    else
      set *$SYSCONF_SYS_CFG14 = (1 << 18) | (0x5a << 20)
      set *$SYSCONF_SYS_CFG15 = (1 << 19) | (0x79 << 20)
      set *$SYSCONF_SYS_CFG20 = (1 << 18) | (0x5a << 20)
      set *$SYSCONF_SYS_CFG21 = (1 << 19) | (0x79 << 20)
    end
  end
end
##}}}

##{{{  CUSTOM13 EMI Configuration
#######################
## EMI Configuration ##
#######################

define custom13_emi_configure
  ## Re-program bank addresses
  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000004
  set *$EMI_BANK2_BASEADDRESS = 0x00000008
  set *$EMI_BANK3_BASEADDRESS = 0x0000000a
  set *$EMI_BANK4_BASEADDRESS = 0x0000000c

  ## Bank 0 - On-board Flash
  set *$EMI_BANK0_EMICONFIGDATA0 = 0x040016d1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x0e004444
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x0e004444
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000008

  ## Bank 1 - On-board Flash
  set *$EMI_BANK1_EMICONFIGDATA0 = 0x040016d1
  set *$EMI_BANK1_EMICONFIGDATA1 = 0x0e004444
  set *$EMI_BANK1_EMICONFIGDATA2 = 0x0e004444
  set *$EMI_BANK1_EMICONFIGDATA3 = 0x00000008

  ## Bank 2 - DVBCI
  set *$EMI_BANK2_EMICONFIGDATA0 = 0x002046f9
  set *$EMI_BANK2_EMICONFIGDATA1 = 0xa5a00000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0xa5a20000
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

  ## Bank 3 - ATAPI
  set *$EMI_BANK3_EMICONFIGDATA0 = 0x00021791
  set *$EMI_BANK3_EMICONFIGDATA1 = 0x10004141
  set *$EMI_BANK3_EMICONFIGDATA2 = 0x10004141
  set *$EMI_BANK3_EMICONFIGDATA3 = 0x00000000

  ## Bank 4 - EPLD Registers and LAN91C111
  set *$EMI_BANK4_EMICONFIGDATA0 = 0x041086f1
  set *$EMI_BANK4_EMICONFIGDATA1 = 0x93001111
  set *$EMI_BANK4_EMICONFIGDATA2 = 0x91001111
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

  set *$EMI_GENCFG = 0x00000010
end
##}}}

##{{{  CUSTOM13 LMI Configuration (200Mhz)
####################################
## LMI Sys Configuration (200Mhz) ##
####################################

define custom13_lmisys_200Mhz_configure
  ## _ST_display (_procname) "SDRAM Mode Register"
  ## set *$LMISYS_MIM_0 = 0x06100247
  ## set *$LMISYS_MIM_1 = 0x01010000
  ## Active bandwith limiter for SPDIF
  set *$LMISYS_MIM_0 = 0x86100247
  set *$LMISYS_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMISYS_STR_0 = 0x34f27d6a

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMISYS_SDRA0_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMISYS_SDRA1_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000003
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SDMR0 = 0x00000400
  set *$LMISYS_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SDMR0 = 0x00000033
  set *$LMISYS_SCR_0 = 0x00000000
end

####################################
## LMI Vid Configuration (200Mhz) ##
####################################

define custom13_lmivid_200Mhz_configure
  ## _ST_display (_procname) "SDRAM Mode Register"
  ## set *$LMIVID_MIM_0 = 0x06100247
  ## set *$LMIVID_MIM_1 = 0x01010000
  ## Active bandwith limiter for spdif
  set *$LMIVID_MIM_0 = 0x86100247
  set *$LMIVID_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMIVID_STR_0 = 0x34f27d6a

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMIVID_SDRA0_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMIVID_SDRA1_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000003
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SDMR0 = 0x00000402
  set *$LMIVID_SDMR0 = 0x00000133
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SDMR0 = 0x00000033
  set *$LMIVID_SCR_0 = 0x00000000
end
##}}}

##{{{  CUSTOM13 LMI Configuration (133Mhz)
####################################
## LMI Sys Configuration (133Mhz) ##
####################################

define custom13_lmisys_133Mhz_configure
  ## _ST_display (_procname) "SDRAM Mode Register"
  ## set *$LMISYS_MIM_0 = 0x04100247
  ## set *$LMISYS_MIM_1 = 0x01010000
  ## Active bandwith limiter for SPDIF
  set *$LMISYS_MIM_0 = 0x84100247
  set *$LMISYS_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMISYS_STR_0 = 0x35f04c55

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMISYS_SDRA0_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMISYS_SDRA1_0 = 0x08001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000003
  set *$LMISYS_SCR_0 = 0x00000001
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SDMR0 = 0x00000400
  set *$LMISYS_SDMR0 = 0x00000123
  sleep 0 200
  set *$LMISYS_SCR_0 = 0x00000002
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SCR_0 = 0x00000004
  set *$LMISYS_SDMR0 = 0x00000023
  set *$LMISYS_SCR_0 = 0x00000000
end

####################################
## LMI Vid Configuration (133Mhz) ##
####################################

define custom13_lmivid_133Mhz_configure
  ## _ST_display (_procname) "SDRAM Mode Register"
  ## set *$LMIVID_MIM_0 = 0x04100247
  ## set *$LMIVID_MIM_1 = 0x01010000
  ## Active bandwith limiter for spdif
  set *$LMIVID_MIM_0 = 0x84100247
  set *$LMIVID_MIM_1 = 0x01010022

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMIVID_STR_0 = 0x35f04c55

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  set *$LMIVID_SDRA0_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  set *$LMIVID_SDRA1_0 = 0x14001900

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000003
  set *$LMIVID_SCR_0 = 0x00000001
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SDMR0 = 0x00000400
  set *$LMIVID_SDMR0 = 0x00000123
  sleep 0 200
  set *$LMIVID_SCR_0 = 0x00000002
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SCR_0 = 0x00000004
  set *$LMIVID_SDMR0 = 0x00000023
  set *$LMIVID_SCR_0 = 0x00000000
end
##}}}

##{{{  CUSTOM13 Memory
####################
## Memory mapping ##
####################

define custom13_memory_define
  memory-add Flash        0x00000000 8 ROM
  memory-add LMISYS_SDRAM 0x04000000 64 RAM
  memory-add LMIVID_SDRAM 0x10000000 64 RAM
end

define custom13_sim_memory_define
  sim_addmemory 0x00000000 8 ROM
  sim_addmemory 0x04000000 64 RAM
  sim_addmemory 0x10000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  CUSTOM13 Bypass Configuration
define custom13bypass_setup
  if ($argc > 0)
    stb7100_bypass_setup $arg0
  else
    stb7100_bypass_setup
  end
end

define custom13bypass_setup_attach
  if ($argc > 0)
    stb7100_bypass_setup_attach $arg0
  else
    stb7100_bypass_setup_attach
  end
end
##}}}

##{{{  CUSTOM13 STMMX Configuration
define custom13stmmx_setup
  if ($argc > 0)
    stb7100_stmmx_setup $arg0
  else
    stb7100_stmmx_setup
  end
end

define custom13stmmx_setup_attach
  if ($argc > 0)
    stb7100_stmmx_setup_attach $arg0
  else
    stb7100_stmmx_setup_attach
  end
end
##}}}

define custom13_setup
  stb7100_define
  custom13_memory_define

  stb7100_si_regs

  set $_custom13_stb7100_version = ((*$SYSCONF_DEVICEID >> 28) & 0xf) + 1
  ##printf "STb7100 cut %d detected\n", $_custom13_stb7100_version

  custom13_clockgen_configure
  custom13_sysconf_configure
  custom13_emi_configure
  if ($_custom13_stb7100_version > 1)
    ## Cut 2.0 or above ##
    custom13_lmisys_200Mhz_configure
    custom13_lmivid_200Mhz_configure
  else
    ## Cut 1.x ##
    custom13_lmisys_133Mhz_configure
    custom13_lmivid_133Mhz_configure
  end

  set *$CCN_CCR = 0x8000090b
end

document custom13_setup
Configure a TMM Custom-13 board
Usage: custom13_setup
end

define custom13sim_setup
  stb7100_define
  custom13_memory_define

  stb7100_si_regs

  set *$CCN_CCR = 0x8000090b
end

document custom13sim_setup
Configure a simulated TMM Custom-13 board
Usage: custom13sim_setup
end

define custom13_fsim_setup
  stb7100_fsim_core_setup
  custom13_sim_memory_define
end

document custom13_fsim_setup
Configure functional simulator for TMM Custom-13 board
Usage: custom13_fsim_setup
end

define custom13_psim_setup
  stb7100_psim_core_setup
  custom13_sim_memory_define
end

document custom13_psim_setup
Configure performance simulator for TMM Custom-13 board
Usage: custom13_psim_setup
end

define custom13_display_registers
  stb7100_display_si_regs
end

document custom13_display_registers
Display the STb7100 configuration registers
Usage: custom13_display_registers
end
