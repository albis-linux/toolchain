##------------------------------------------------------------------------------
## sh4202microdev.cmd - SH4202 Micro Development SoC Board
##------------------------------------------------------------------------------
## Note that apart from the legacy Hitachi configuration registers (in P4), all
## the other configuration registers must be accessed through P2.
##------------------------------------------------------------------------------

##{{{  SH4202MICRODEV Peripherals
define sh4202microdev_general_regs
  __shx_reg_32 $SH4202MICRODEV_GENERAL_FPGAREV ($arg0+0x0008)
  __shx_reg_32 $SH4202MICRODEV_GENERAL_MEMMAP ($arg0+0x4d00)
  __shx_reg_32 $SH4202MICRODEV_GENERAL_CS_MAP ($arg0+0x4d10)
  __shx_reg_32 $SH4202MICRODEV_GENERAL_LED_CTRL ($arg0+0x4d20)
  __shx_reg_32 $SH4202MICRODEV_GENERAL_SWITCHES ($arg0+0x4d30)
  __shx_reg_32 $SH4202MICRODEV_GENERAL_RESET ($arg0+0x4d60)
  __shx_reg_32 $SH4202MICRODEV_GENERAL_SHOC ($arg0+0x4d70)
end

define sh4202microdev_intc_regs
  __shx_reg_32 $SH4202MICRODEV_INTC_INTENB ($arg0+0x00)
  __shx_reg_32 $SH4202MICRODEV_INTC_INTDSB ($arg0+0x08)
  __shx_reg_32 $SH4202MICRODEV_INTC_INTPRI_0 ($arg0+0x10)
  __shx_reg_32 $SH4202MICRODEV_INTC_INTPRI_1 ($arg0+0x18)
  __shx_reg_32 $SH4202MICRODEV_INTC_INTPRI_2 ($arg0+0x20)
  __shx_reg_32 $SH4202MICRODEV_INTC_INTPRI_3 ($arg0+0x28)
  __shx_reg_32 $SH4202MICRODEV_INTC_INTSRC ($arg0+0x30)
  __shx_reg_32 $SH4202MICRODEV_INTC_INTREQ ($arg0+0x38)
end

define sh4202microdev_display_general_regs
  __shx_display_reg $SH4202MICRODEV_GENERAL_FPGAREV
  __shx_display_reg $SH4202MICRODEV_GENERAL_MEMMAP
  __shx_display_reg $SH4202MICRODEV_GENERAL_CS_MAP
  __shx_display_reg $SH4202MICRODEV_GENERAL_LED_CTRL
  __shx_display_reg $SH4202MICRODEV_GENERAL_SWITCHES
  __shx_display_reg $SH4202MICRODEV_GENERAL_RESET
  __shx_display_reg $SH4202MICRODEV_GENERAL_SHOC
end

define sh4202microdev_display_intc_regs
  __shx_display_reg $SH4202MICRODEV_INTC_INTENB
  __shx_display_reg $SH4202MICRODEV_INTC_INTDSB
  __shx_display_reg $SH4202MICRODEV_INTC_INTPRI_0
  __shx_display_reg $SH4202MICRODEV_INTC_INTPRI_1
  __shx_display_reg $SH4202MICRODEV_INTC_INTPRI_2
  __shx_display_reg $SH4202MICRODEV_INTC_INTPRI_3
  __shx_display_reg $SH4202MICRODEV_INTC_INTSRC
  __shx_display_reg $SH4202MICRODEV_INTC_INTREQ
end
##}}}

##{{{  SH4202MICRODEV Configure
define sh4202microdev_configure
##------------------------------------------------------------------------------
## Reset board and wait (note that SoC reset is masked)
##------------------------------------------------------------------------------
  set *$SH4202MICRODEV_GENERAL_RESET = 0xa5525354
  sleep 0 100000

##------------------------------------------------------------------------------
## Configure FRQCR register
##------------------------------------------------------------------------------
  set *$CPG_WTCNT = 0x5a00
  set *$CPG_WTCSR = 0xa567
  set *$CPG_FRQCR = 0x0e0a

##------------------------------------------------------------------------------
## Program FEMI registers
##------------------------------------------------------------------------------
  set *$FEMI_A0MCR = 0x0b777110
  set *$FEMI_A1MCR = 0x0b777108
  set *$FEMI_A2MCR = 0x03777510
  set *$FEMI_A3MCR = 0x0b777110
  set *$FEMI_A4MCR = 0x0b777108

##------------------------------------------------------------------------------
## Program EMI registers
##------------------------------------------------------------------------------
  set *$CPG2_FRQCR3 = (*$CPG2_FRQCR3 & 0x000000f8) | 0x00000002

  set *$EMI_COC = 0x00000040
  sleep 0 10000
  set *$EMI_MIM = 0x00000021
  set *$EMI_STR = 0x0000116b
  set *$EMI_SDRA0 = 0x0a001900
  set *$EMI_SDRA1 = 0x0c001900
  set *$EMI_SCR = 0x00000003
  sleep 0 10000
  set *$EMI_SCR = 0x00000002
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *$EMI_SCR = 0x00000004
  set *($EMI_SDMR0+(0x0110/sizeof(*$EMI_SDMR0))) = 0x0
  set *($EMI_SDMR1+(0x0110/sizeof(*$EMI_SDMR1))) = 0x0
  set *$EMI_MIM = 0x00900281
  set *$EMI_SCR = 0x00000000
end
##}}}

##{{{  SH4202MICRODEV PMB Configuration
define sh4202microdevse_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x08 64

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define sh4202microdevseuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x08 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define sh4202microdevse29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x08 64
  sh4_set_pmb 1 0xa0 0x08 64 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  SH4202MICRODEV Memory
define sh4202microdev_memory_define
  memory-add Flash    0x00000000 32 ROM
  memory-add FEMI_RAM 0x04000000 8 RAM
  memory-add EMI_RAM  0x08000000 64 RAM
end

define sh4202microdev_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x04000000 8 RAM
  sim_addmemory 0x08000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define sh4202microdev_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs
  sh4202microdev_general_regs 0xa6100000
  sh4202microdev_intc_regs 0xa6110000

  sh4202microdev_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdev_setup
Configure an SH4202 Micro Development SoC board
Usage: sh4202microdev_setup
end

define sh4202microdevse_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs
  sh4202microdev_general_regs 0xa6100000
  sh4202microdev_intc_regs 0xa6110000

  sh4202microdev_configure

  sh4202microdevse_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdevse_setup
Configure an SH4202 Micro Development SoC board with the SH4202 in 32-bit SE mode
Usage: sh4202microdevse_setup
end

define sh4202microdevseuc_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs
  sh4202microdev_general_regs 0xa6100000
  sh4202microdev_intc_regs 0xa6110000

  sh4202microdev_configure

  sh4202microdevseuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdevseuc_setup
Configure an SH4202 Micro Development SoC board with the SH4202 in 32-bit SE mode with uncached RAM mappings
Usage: sh4202microdevseuc_setup
end

define sh4202microdevse29_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs
  sh4202microdev_general_regs 0xa6100000
  sh4202microdev_intc_regs 0xa6110000

  sh4202microdev_configure

  sh4202microdevse29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdevse29_setup
Configure an SH4202 Micro Development SoC board with the SH4202 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: sh4202microdevse29_setup
end

define sh4202microdevsim_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdevsim_setup
Configure a simulated SH4202 Micro Development SoC board
Usage: sh4202microdevsim_setup
end

define sh4202microdevsimse_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs

  sh4202microdevse_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdevsimse_setup
Configure a simulated SH4202 Micro Development SoC board with the SH4202 in 32-bit SE mode
Usage: sh4202microdevsimse_setup
end

define sh4202microdevsimseuc_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs

  sh4202microdevseuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdevsimseuc_setup
Configure a simulated SH4202 Micro Development SoC board with the SH4202 in 32-bit SE mode with uncached RAM mappings
Usage: sh4202microdevsimseuc_setup
end

define sh4202microdevsimse29_setup
  sh4202_define
  sh4202microdev_memory_define

  sh4202_si_regs

  sh4202microdevse29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document sh4202microdevsimse29_setup
Configure a simulated SH4202 Micro Development SoC board with the SH4202 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: sh4202microdevsimse29_setup
end

define sh4202microdev_fsim_setup
  sh4202_fsim_core_setup
  sh4202microdev_sim_memory_define
end

document sh4202microdev_fsim_setup
Configure functional simulator for SH4202 Micro Development SoC board
Usage: sh4202microdev_fsim_setup
end

define sh4202microdev_psim_setup
  sh4202_psim_core_setup
  sh4202microdev_sim_memory_define
end

document sh4202microdev_psim_setup
Configure performance simulator for SH4202 Micro Development SoC board
Usage: sh4202microdev_psim_setup
end

define sh4202microdev_display_registers
  sh4202_display_si_regs
end

document sh4202microdev_display_registers
Display the SH4202 configuration registers
Usage: sh4202microdev_display_registers
end
