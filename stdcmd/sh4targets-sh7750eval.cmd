################################################################################

define sh7750evalbe
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4be $arg0 sh7750eval_setup $arg1
  else
    connectsh4be $arg0 sh7750eval_setup ""
  end
end

document sh7750evalbe
Connect to and configure an SH7750 Evaluation board (big endian)
Usage: sh7750evalbe <target>
where <target> is an ST Micro Connect name or IP address
end

define sh7750evalle
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 sh7750eval_setup $arg1
  else
    connectsh4le $arg0 sh7750eval_setup ""
  end
end

document sh7750evalle
Connect to and configure an SH7750 Evaluation board (little endian)
Usage: sh7750evalle <target>
where <target> is an ST Micro Connect name or IP address
end

define sh7750eval
  if ($argc > 1)
    sh7750evalle $arg0 $arg1
  else
    sh7750evalle $arg0
  end
end

document sh7750eval
Connect to and configure an SH7750 Evaluation board
Usage: sh7750eval <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define sh7750evalusbbe
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usbbe $arg0 sh7750eval_setup $arg1
  else
    connectsh4usbbe $arg0 sh7750eval_setup ""
  end
end

document sh7750evalusbbe
Connect to and configure an SH7750 Evaluation board (big endian)
Usage: sh7750evalusbbe <target>
where <target> is an ST Micro Connect USB name
end

define sh7750evalusble
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 sh7750eval_setup $arg1
  else
    connectsh4usble $arg0 sh7750eval_setup ""
  end
end

document sh7750evalusble
Connect to and configure an SH7750 Evaluation board (little endian)
Usage: sh7750evalusble <target>
where <target> is an ST Micro Connect USB name
end

define sh7750evalusb
  if ($argc > 1)
    sh7750evalusble $arg0 $arg1
  else
    sh7750evalusble $arg0
  end
end

document sh7750evalusb
Connect to and configure an SH7750 Evaluation board
Usage: sh7750evalusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define sh7750evalsimbe
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simbe sh7750eval_fsim_setup sh7750evalsim_setup $arg0
  else
    connectsh4simbe sh7750eval_fsim_setup sh7750evalsim_setup ""
  end
end

document sh7750evalsimbe
Connect to and configure a simulated SH7750 Evaluation board (big endian)
Usage: sh7750evalsimbe
end

define sh7750evalsimle
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle sh7750eval_fsim_setup sh7750evalsim_setup $arg0
  else
    connectsh4simle sh7750eval_fsim_setup sh7750evalsim_setup ""
  end
end

document sh7750evalsimle
Connect to and configure a simulated SH7750 Evaluation board (little endian)
Usage: sh7750evalsimle
end

define sh7750evalsim
  if ($argc > 0)
    sh7750evalsimle $arg0
  else
    sh7750evalsimle
  end
end

document sh7750evalsim
Connect to and configure a simulated SH7750 Evaluation board
Usage: sh7750evalsim
end

################################################################################

define sh7750evalpsimbe
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimbe sh7750eval_psim_setup sh7750evalsim_setup $arg0
  else
    connectsh4psimbe sh7750eval_psim_setup sh7750evalsim_setup ""
  end
end

document sh7750evalpsimbe
Connect to and configure a simulated SH7750 Evaluation board (big endian)
Usage: sh7750evalpsimbe
end

define sh7750evalpsimle
  source register40.cmd
  source display40.cmd
  source sh7750.cmd
  source sh7750eval.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle sh7750eval_psim_setup sh7750evalsim_setup $arg0
  else
    connectsh4psimle sh7750eval_psim_setup sh7750evalsim_setup ""
  end
end

document sh7750evalpsimle
Connect to and configure a simulated SH7750 Evaluation board (little endian)
Usage: sh7750evalpsimle
end

define sh7750evalpsim
  if ($argc > 0)
    sh7750evalpsimle $arg0
  else
    sh7750evalpsimle
  end
end

document sh7750evalpsim
Connect to and configure a simulated SH7750 Evaluation board
Usage: sh7750evalpsim
end

################################################################################
