################################################################################

define eud7141sim
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle eud7141_fsim_setup eud7141sim_setup $arg0
  else
    connectsh4simle eud7141_fsim_setup eud7141sim_setup ""
  end
end

document eud7141sim
Connect to and configure a simulated STi7141-EUD board
Usage: eud7141sim
end

define eud7141simse
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle eud7141se_fsim_setup eud7141simse_setup $arg0
  else
    connectsh4simle eud7141se_fsim_setup eud7141simse_setup ""
  end
end

document eud7141simse
Connect to and configure a simulated STi7141-EUD board (STi7141 in 32-bit SE mode)
Usage: eud7141simse
end

define eud7141simseuc
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle eud7141se_fsim_setup eud7141simseuc_setup $arg0
  else
    connectsh4simle eud7141se_fsim_setup eud7141simseuc_setup ""
  end
end

document eud7141simseuc
Connect to and configure a simulated STi7141-EUD board (STi7141 in 32-bit SE mode with uncached mappings)
Usage: eud7141simseuc
end

define eud7141simse29
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle eud7141se_fsim_setup eud7141simse29_setup $arg0
  else
    connectsh4simle eud7141se_fsim_setup eud7141simse29_setup ""
  end
end

document eud7141simse29
Connect to and configure a simulated STi7141-EUD board (STi7141 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: eud7141simse29
end

################################################################################

define eud7141psim
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle eud7141_psim_setup eud7141sim_setup $arg0
  else
    connectsh4psimle eud7141_psim_setup eud7141sim_setup ""
  end
end

document eud7141psim
Connect to and configure a simulated STi7141-EUD board
Usage: eud7141psim
end

define eud7141psimse
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle eud7141se_psim_setup eud7141simse_setup $arg0
  else
    connectsh4psimle eud7141se_psim_setup eud7141simse_setup ""
  end
end

document eud7141psimse
Connect to and configure a simulated STi7141-EUD board (STi7141 in 32-bit SE mode)
Usage: eud7141psimse
end

define eud7141psimseuc
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle eud7141se_psim_setup eud7141simseuc_setup $arg0
  else
    connectsh4psimle eud7141se_psim_setup eud7141simseuc_setup ""
  end
end

document eud7141psimseuc
Connect to and configure a simulated STi7141-EUD board (STi7141 in 32-bit SE mode with uncached mappings)
Usage: eud7141psimseuc
end

define eud7141psimse29
  source register40.cmd
  source display40.cmd
  source sti7141.cmd
  source eud7141.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle eud7141se_psim_setup eud7141simse29_setup $arg0
  else
    connectsh4psimle eud7141se_psim_setup eud7141simse29_setup ""
  end
end

document eud7141psimse29
Connect to and configure a simulated STi7141-EUD board (STi7141 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: eud7141psimse29
end

################################################################################
