################################################################################

define custom13
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source custom13.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 custom13_setup $arg1
  else
    connectsh4le $arg0 custom13_setup "jtagpinout=default hardreset"
  end
end

document custom13
Connect to and configure a TMM Custom-13 board
Usage: custom13 <target>
where <target> is an ST Micro Connect name or IP address
end

define custom13bypass
  if ($argc > 1)
    custom13 $arg0 "jtagpinout=default jtagreset -inicommand $arg1"
  else
    custom13 $arg0 "jtagpinout=default jtagreset -inicommand custom13bypass_setup"
  end
end

document custom13bypass
Connect to and configure a TMM Custom-13 board bypassing to the ST40
Usage: custom13bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define custom13stmmx
  if ($argc > 1)
    custom13 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    custom13 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand custom13stmmx_setup"
  end
end

document custom13stmmx
Connect to and configure a TMM Custom-13 board via an ST MultiCore/Mux
Usage: custom13stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define custom13usb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source custom13.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 custom13_setup $arg1
  else
    connectsh4usble $arg0 custom13_setup "jtagpinout=default hardreset"
  end
end

document custom13usb
Connect to and configure a TMM Custom-13 board
Usage: custom13usb <target>
where <target> is an ST Micro Connect USB name
end

define custom13bypassusb
  if ($argc > 1)
    custom13usb $arg0 "jtagpinout=default jtagreset -inicommand $arg1"
  else
    custom13usb $arg0 "jtagpinout=default jtagreset -inicommand custom13bypass_setup"
  end
end

document custom13bypassusb
Connect to and configure a TMM Custom-13 board bypassing to the ST40
Usage: custom13bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define custom13stmmxusb
  if ($argc > 1)
    custom13usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    custom13usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand custom13stmmx_setup"
  end
end

document custom13stmmxusb
Connect to and configure a TMM Custom-13 board via an ST MultiCore/Mux
Usage: custom13stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define custom13sim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source custom13.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle custom13_fsim_setup custom13sim_setup $arg0
  else
    connectsh4simle custom13_fsim_setup custom13sim_setup ""
  end
end

document custom13sim
Connect to and configure a simulated TMM Custom-13 board
Usage: custom13sim
end

################################################################################

define custom13psim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source custom13.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle custom13_psim_setup custom13sim_setup $arg0
  else
    connectsh4psimle custom13_psim_setup custom13sim_setup ""
  end
end

document custom13psim
Connect to and configure a simulated TMM Custom-13 board
Usage: custom13psim
end

################################################################################
