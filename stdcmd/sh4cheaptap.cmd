init-if-undefined $SHTDIServerWriteSize = (512 * 1024)
keep-variable $SHTDIServerWriteSize

init-if-undefined $RTOSType = 1
keep-variable $RTOSType

init-if-undefined $_sh_simulator_target = 0
keep-variable $_sh_simulator_target

################################################################################
# SHx silicon connect commands
################################################################################

# arg0 = SDI server type
# arg1 = architecture type
# arg2 = endian type
# arg3 = target definition
# arg4 = configuration command
# arg5 = configuration options

define _connectshx
  set endian $arg2
  set $_sh_simulator_target = 0

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi libsh4sdi-server.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -server sh4sdi$arg0 -target $arg3 $arg5 endian=$arg2
  else
    target shtdi libsh4shdebug.so -sdi libsh4sdi-server.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -server sh4sdi$arg0 -target $arg3 $arg5 endian=$arg2
  end

  _init_all_plugins

  $arg4
end

define _connectshx-cheaptap
  set download-write-size $SHTDIServerWriteSize
  _connectshx cheaptap $arg0 $arg1 deviceid=$arg2 $arg3 $arg4
end

################################################################################

# arg0 = SDI server type
# arg1 = architecture type
# arg2 = endian type
# arg3 = target definition
# arg4 = configuration options

define _connectshxtp
  set endian $arg2
  set $_sh_simulator_target = 0

  if ($RTOSType == 1)
    target shtdi libsh4shdebug.so -sdi libsh4sdi-server.so -rtos libsh4rtos-os21.so -io libsh4io-posix.so -stmc libstmc1.so -server sh4sdi$arg0 -target $arg3 -targetpack jtaginitialise=off $arg4 endian=$arg2
  else
    target shtdi libsh4shdebug.so -sdi libsh4sdi-server.so -rtos libsh4rtos-bare.so -io libsh4io-posix.so -stmc libstmc1.so -server sh4sdi$arg0 -target $arg3 -targetpack jtaginitialise=off $arg4 endian=$arg2
  end

  _init_all_plugins

  enable_targetpack
  targetpack import $arg3
end

define _connectshxtp-cheaptap
  set download-write-size $SHTDIServerWriteSize
  _connectshxtp cheaptap $arg0 $arg1 deviceid=$arg2 $arg3
end

################################################################################
# SH4 little endian silicon connection wrapper commands
################################################################################

define connectsh4le
  _connectshx-cheaptap sh4 little $arg0 $arg1 $arg2
end

document connectsh4le
Connect to and configure a little endian SH4 silicon target
Usage: connectsh4le <target> <setup-command> <control-commands>
where <target> is an ST CheapTap name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectsh4tple
  _connectshxtp-cheaptap sh4 little $arg0 $arg1
end

document connectsh4tple
Connect to and configure a little endian SH4 silicon target
Usage: connectsh4tple <target> <control-commands>
where <target> is an ST CheapTap target definition
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectsh4autotple
  connectsh4tple $arg0 $arg1
end

################################################################################
# ST40-300 little endian silicon connection wrapper commands
################################################################################

define connectst40300le
  _connectshx-cheaptap st40-300 little $arg0 $arg1 $arg2
end

document connectst40300le
Connect to and configure a little endian ST40-300 silicon target
Usage: connectst40300le <target> <setup-command> <control-commands>
where <target> is an ST CheapTap name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectst40300tple
  _connectshxtp-cheaptap st40-300 little $arg0 $arg1
end

document connectst40300tple
Connect to and configure a little endian ST40-300 silicon target
Usage: connectst40300tple <target> <control-commands>
where <target> is an ST CheapTap target definition
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectst40300autotple
  connectst40300tple $arg0 $arg1
end

################################################################################
# SH4 big endian silicon connection wrapper commands
################################################################################

define connectsh4be
  _connectshx-cheaptap sh4 big $arg0 $arg1 $arg2
end

document connectsh4be
Connect to and configure a big endian SH4 silicon target
Usage: connectsh4be <target> <setup-command> <control-commands>
where <target> is an ST CheapTap name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectsh4tpbe
  _connectshxtp-cheaptap sh4 big $arg0 $arg1
end

document connectsh4tpbe
Connect to and configure a big endian SH4 silicon target
Usage: connectsh4tpbe <target> <control-commands>
where <target> is an ST CheapTap target definition
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectsh4autotpbe
  connectsh4tpbe $arg0 $arg1
end

################################################################################
# ST40-300 big endian silicon connection wrapper commands
################################################################################

define connectst40300be
  _connectshx-cheaptap st40-300 big $arg0 $arg1 $arg2
end

document connectst40300be
Connect to and configure a big endian ST40-300 silicon target
Usage: connectst40300be <target> <setup-command> <control-commands>
where <target> is an ST CheapTap name
+     <setup-command> is a command to configure the target
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectst40300tpbe
  _connectshxtp-cheaptap st40-300 big $arg0 $arg1
end

document connectst40300tpbe
Connect to and configure a big endian ST40-300 silicon target
Usage: connectst40300tpbe <target> <control-commands>
where <target> is an ST CheapTap target definition
+     <control-commands> are commands to configure the ST CheapTap
end

################################################################################

define connectst40300autotpbe
  connectst40300tpbe $arg0 $arg1
end

################################################################################
