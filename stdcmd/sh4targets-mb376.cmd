################################################################################

define mb376
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source mb376.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb376_setup $arg1
  else
    connectsh4le $arg0 mb376_setup "hardreset"
  end
end

document mb376
Connect to and configure an STi5528-Mboard board
Usage: mb376 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb376usb
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source mb376.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb376_setup $arg1
  else
    connectsh4usble $arg0 mb376_setup "hardreset"
  end
end

document mb376usb
Connect to and configure an STi5528-Mboard board
Usage: mb376usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb376sim
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source mb376.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb376_fsim_setup mb376sim_setup $arg0
  else
    connectsh4simle mb376_fsim_setup mb376sim_setup ""
  end
end

document mb376sim
Connect to and configure a simulated STi5528-Mboard board
Usage: mb376sim
end

################################################################################

define mb376psim
  source register40.cmd
  source display40.cmd
  source sti5528clocks.cmd
  source sti5528.cmd
  source mb376.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb376_psim_setup mb376sim_setup $arg0
  else
    connectsh4psimle mb376_psim_setup mb376sim_setup ""
  end
end

document mb376psim
Connect to and configure a simulated STi5528-Mboard board
Usage: mb376psim
end

################################################################################
