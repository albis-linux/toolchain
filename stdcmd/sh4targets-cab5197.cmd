################################################################################

define cab5197sim
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle cab5197_fsim_setup cab5197sim_setup $arg0
  else
    connectsh4simle cab5197_fsim_setup cab5197sim_setup ""
  end
end

document cab5197sim
Connect to and configure a simulated STi5197-CAB board
Usage: cab5197sim
end

define cab5197simse
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle cab5197se_fsim_setup cab5197simse_setup $arg0
  else
    connectsh4simle cab5197se_fsim_setup cab5197simse_setup ""
  end
end

document cab5197simse
Connect to and configure a simulated STi5197-CAB board (STi5197 in 32-bit SE mode)
Usage: cab5197simse
end

define cab5197simseuc
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle cab5197se_fsim_setup cab5197simseuc_setup $arg0
  else
    connectsh4simle cab5197se_fsim_setup cab5197simseuc_setup ""
  end
end

document cab5197simseuc
Connect to and configure a simulated STi5197-CAB board (STi5197 in 32-bit SE mode with uncached mappings)
Usage: cab5197simseuc
end

define cab5197simse29
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle cab5197se_fsim_setup cab5197simse29_setup $arg0
  else
    connectsh4simle cab5197se_fsim_setup cab5197simse29_setup ""
  end
end

document cab5197simse29
Connect to and configure a simulated STi5197-CAB board (STi5197 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: cab5197simse29
end

################################################################################

define cab5197psim
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle cab5197_psim_setup cab5197sim_setup $arg0
  else
    connectsh4psimle cab5197_psim_setup cab5197sim_setup ""
  end
end

document cab5197psim
Connect to and configure a simulated STi5197-CAB board
Usage: cab5197psim
end

define cab5197psimse
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle cab5197se_psim_setup cab5197simse_setup $arg0
  else
    connectsh4psimle cab5197se_psim_setup cab5197simse_setup ""
  end
end

document cab5197psimse
Connect to and configure a simulated STi5197-CAB board (STi5197 in 32-bit SE mode)
Usage: cab5197psimse
end

define cab5197psimseuc
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle cab5197se_psim_setup cab5197simseuc_setup $arg0
  else
    connectsh4psimle cab5197se_psim_setup cab5197simseuc_setup ""
  end
end

document cab5197psimseuc
Connect to and configure a simulated STi5197-CAB board (STi5197 in 32-bit SE mode with uncached mappings)
Usage: cab5197psimseuc
end

define cab5197psimse29
  source register40.cmd
  source display40.cmd
  source sti5197.cmd
  source cab5197.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle cab5197se_psim_setup cab5197simse29_setup $arg0
  else
    connectsh4psimle cab5197se_psim_setup cab5197simse29_setup ""
  end
end

document cab5197psimse29
Connect to and configure a simulated STi5197-CAB board (STi5197 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: cab5197psimse29
end

################################################################################
