source jtagcommon.cmd

################################################################################

define tmcTestLogicReset
  ## Move to Test-Logic-Reset
  jtag tms=(1*5)
end

################################################################################

define tmcRunTestIdle
  ## Move to Run-Test-Idle
  tmcTestLogicReset
  jtag tms=0
end

################################################################################

define _tmcReadIR
  ## Read IR register (Run-Test-Idle initial and final state)
  jtag tms=110
  jtag tms=(0*5) tdo=1 tdi=$arg0
  jtag tms=110
end

define tmcReadIR
  if ($argc > 0)
    _tmcReadIR $arg0
  else
    _tmcReadIR $_ir
  end
end

################################################################################

define tmcWriteIR
  ## Write IR register (Run-Test-Idle initial and final state)
  set $_size = 5
  set $_data = $arg0
  jtag tms=1100
  while ($_size-- > 1)
    if ($_data & 1)
      jtag tms=0 tdo=1
    else
      jtag tms=0 tdo=0
    end
    set $_data >>= 1
  end
  if ($_data & 1)
    jtag tms=1 tdo=1
  else
    jtag tms=1 tdo=0
  end
  jtag tms=10
end

################################################################################

define tmcWriteIR_IDCODE
  tmcWriteIR 0x2
end

define tmcWriteIR_TESTMODE
  tmcWriteIR 0x8
end

################################################################################

define tmcPrintIR
  if ($argc > 0)
    tmcReadIR $arg0
    _jtagPrintSignal $arg0
  else
    tmcReadIR $tmcir
    _jtagPrintSignal $tmcir
  end
end

define tmcPrintRestoreIR
  if ($argc > 0)
    tmcReadIR $arg0
    tmcWriteIR $arg0_0
    _jtagPrintSignal $arg0
  else
    tmcReadIR $tmcir
    tmcWriteIR $tmcir_0
    _jtagPrintSignal $tmcir
  end
end

################################################################################

define _tmcReadDR
  ## Read DR register (Run-Test-Idle initial and final state)
  jtag tms=10
  jtag tms=(0*$arg0) tdo=0 tdi=$arg1
  jtag tms=110
end

define tmcReadDR
  if ($argc > 1)
    _tmcReadDR $arg0 $arg1
  else
    _tmcReadDR $arg0 $_dr
  end
end

################################################################################

define tmcWriteDR
  ## Write DR register (Run-Test-Idle initial and final state)
  set $_size = $arg0
  set $_data = $arg1
  jtag tms=100
  while ($_size-- > 1)
    if ($_data & 1)
      jtag tms=0 tdo=1
    else
      jtag tms=0 tdo=0
    end
    set $_data >>= 1
  end
  if ($_data & 1)
    jtag tms=1 tdo=1
  else
    jtag tms=1 tdo=0
  end
  jtag tms=10
end

################################################################################

define tmcPrintDR
  if ($argc > 1)
    tmcReadDR $arg0 $arg1
    _jtagPrintSignal $arg1
  else
    tmcReadDR $arg0 $tmcdr
    _jtagPrintSignal $tmcdr
  end
end

define tmcPrintRestoreDR
  if ($argc > 1)
    tmcReadDR $arg0 $arg1
    tmcWriteDR $arg0 $arg1_0
    _jtagPrintSignal $arg1
  else
    tmcReadDR $arg0 $tmcdr
    tmcWriteDR $arg0 $tmcdr_0
    _jtagPrintSignal $tmcdr
  end
end

define tmcSelectDeviceId
  set $_tmc_select_device_id_argc = $argc

  tmcWriteIR_TESTMODE

  ## Zero TEST register (default is 512 bits) (Run-Test-Idle initial and final state)
  if ($_tmc_select_device_id_argc > 1)
    jtag tms=10(0*$arg1)110 tdo=0
  else
    jtag tms=10(0*512)110 tdo=0
  end

  tmcWriteDR 5 $arg0
end

################################################################################

define tmcInitialState
  jtag mode=singleshot
  tmcRunTestIdle
end

define tmcSelectDeviceId
  set $_tmc_select_device_id_argc = $argc

  tmcWriteIR_TESTMODE

  ## Zero TEST register (default is 512 bits) (Run-Test-Idle initial and final state)
  if ($_tmc_select_device_id_argc > 1)
    jtag tms=10(0*$arg1)110 tdo=0
  else
    jtag tms=10(0*512)110 tdo=0
  end

  tmcWriteDR 5 $arg0
end

################################################################################

define tmcPrintRegister
  tmcWriteIR_$arg0
  tmcPrintDR $arg1 $tmc$arg0
end

define tmcPrintRestoreRegister
  tmcWriteIR_$arg0
  tmcPrintRestoreDR $arg1 $tmc$arg0
end

################################################################################

define tmcPrintAllRegisters
  tmcPrintRegister IDCODE 32
end

################################################################################
