##------------------------------------------------------------------------------
## hdk5289sti5206.cmd - STi5206-HDK Reference Platform
##------------------------------------------------------------------------------

##{{{  HDK5289 (STi5206) PMB Configuration
define hdk5289sti5206se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk5289sti5206seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define hdk5289sti5206se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0xa0 0x40 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  HDK5289 (STi5206) Memory
define hdk5289sti5206_memory_define
  memory-add Flash     0x00000000 64 ROM
  memory-add LMI_SDRAM 0x0c000000 128 RAM
end

define hdk5289sti5206_sim_memory_define
  sim_addmemory 0x00000000 64 ROM
  sim_addmemory 0x0c000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define hdk5289sti5206se_memory_define
  memory-add Flash     0x00000000 64 ROM
  memory-add LMI_SDRAM 0x40000000 128 RAM
end

define hdk5289sti5206se_sim_memory_define
  sim_addmemory 0x00000000 64 ROM
  sim_addmemory 0x40000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

define hdk5289sti5206sim_setup
  sti5206_define
  hdk5289sti5206_memory_define

  st40300_core_si_regs

  set *$CCN_CCR = 0x8000090d
end

document hdk5289sti5206sim_setup
Configure a simulated STi5206-HDK board
Usage: hdk5289sti5206sim_setup
end

define hdk5289sti5206simse_setup
  sti5206_define
  hdk5289sti5206se_memory_define

  st40300_core_si_regs

  hdk5289sti5206se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk5289sti5206simse_setup
Configure a simulated STi5206-HDK board with the STi5206 in 32-bit SE mode
Usage: hdk5289sti5206simse_setup
end

define hdk5289sti5206simseuc_setup
  sti5206_define
  hdk5289sti5206se_memory_define

  st40300_core_si_regs

  hdk5289sti5206seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk5289sti5206simseuc_setup
Configure a simulated STi5206-HDK board with the STi5206 in 32-bit SE mode with uncached RAM mappings
Usage: hdk5289sti5206simseuc_setup
end

define hdk5289sti5206simse29_setup
  sti5206_define
  hdk5289sti5206se_memory_define

  st40300_core_si_regs

  hdk5289sti5206se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document hdk5289sti5206simse29_setup
Configure a simulated STi5206-HDK board with the STi5206 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: hdk5289sti5206simse29_setup
end

define hdk5289sti5206_fsim_setup
  sti5206_fsim_core_setup
  hdk5289sti5206_sim_memory_define
end

document hdk5289sti5206_fsim_setup
Configure functional simulator for STi5206-HDK board
Usage: hdk5289sti5206_fsim_setup
end

define hdk5289sti5206se_fsim_setup
  sti5206_fsim_core_setup
  hdk5289sti5206se_sim_memory_define
end

document hdk5289sti5206se_fsim_setup
Configure functional simulator for STi5206-HDK board with the STi5206 in 32-bit SE mode
Usage: hdk5289sti5206se_fsim_setup
end

define hdk5289sti5206_psim_setup
  sti5206_psim_core_setup
  hdk5289sti5206_sim_memory_define
end

document hdk5289sti5206_psim_setup
Configure performance simulator for STi5206-HDK board
Usage: hdk5289sti5206_psim_setup
end

define hdk5289sti5206se_psim_setup
  sti5206_psim_core_setup
  hdk5289sti5206se_sim_memory_define
end

document hdk5289sti5206se_psim_setup
Configure performance simulator for STi5206-HDK board with the STi5206 in 32-bit SE mode
Usage: hdk5289sti5206se_psim_setup
end

define hdk5289sti5206_display_registers
  st40300_display_core_si_regs
end

document hdk5289sti5206_display_registers
Display the STi5206 configuration registers
Usage: hdk5289sti5206_display_registers
end
