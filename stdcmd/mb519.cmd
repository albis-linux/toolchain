##------------------------------------------------------------------------------
## mb519.cmd - STi7200-Mboard Validation Platform MB519
##------------------------------------------------------------------------------

init-if-undefined $_mb519extclk = 30
keep-variable $_mb519extclk

init-if-undefined $_mb519sti7200cut = 1
keep-variable $_mb519sti7200cut

##{{{  MB519 SoC and board version detection
define mb519_version
  sti7200_version $arg0
  set $arg1 = *((unsigned char *) 0xa5020000)
end
##}}}

init-if-undefined $_mb519slowckgbpll0 = 0
keep-variable $_mb519slowckgbpll0

##{{{  MB519 CLOCKGEN Configuration
define mb519_clockgen_configure
  ## _ST_display (_procname) "Configuring CLOCKGEN"

  linkspeed 1.25MHz

  if ($_mb519extclk == 30)
    if ($arg0 == 0xa0)
      ## Set CKGA_PLL0 nominal to 700MHz
      sti7200_set_clockgen_a_pll0 0x03 0x23 0x0
    else
      if ($arg0 == 0xa1)
        ## Set CKGA_PLL0 nominal to 800MHz
        sti7200_set_clockgen_a_pll0 0x03 0x28 0x0
      else
        ## Set CKGA_PLL0 nominal to 900MHz
        sti7200_set_clockgen_a_pll0 0x01 0x0f 0x0
      end
    end

    ## Set CKGA_PLL1 nominal to 450MHz
    sti7200_set_clockgen_a_pll1 0x01 0x0f

    ## Set CKGA_PLL2 nominal to 800MHz
    sti7200_set_clockgen_a_pll2 0x03 0x28 0x0

    if ($_mb519slowckgbpll0)
      ## Set CKGB_PLL0 nominal to 600MHz
      sti7200_set_clockgen_b_pll0 0x03 0x1e 0x0
    else
      ## Set CKGB_PLL0 nominal to 800MHz
      sti7200_set_clockgen_b_pll0 0x03 0x28 0x0
    end

    if ($arg1 == 0xa)
      ## On PCB rev A, we have a limitation on LMI1 : Set LMI clock to 400MHz
      if (($arg0 == 0xa0) || ($arg0 == 0xa1))
        sti7200_set_clockgen_lmi_pll_cut1 0x03 0x28
      else
        sti7200_set_clockgen_lmi_pll_cut2 0x03 0x28
      end
    else
      if (($arg0 == 0xa0) || ($arg0 == 0xa1))
        ## Set LMI clock to 666MHz
        sti7200_set_clockgen_lmi_pll_cut1 0x05 0x6f
      else
        ## Set LMI clock to 800MHz
        sti7200_set_clockgen_lmi_pll_cut2 0x03 0x50
      end
    end
  else
    ## Unsupported external clock frequency
    printf "External clock frequencies other than 30MHz are not supported\n"
  end

  ## Restore default link speed (5MHz maximum for TapMux)
  if ($_stmmxmode)
    linkspeed 2.5MHz
  else
    linkspeed 10MHz
  end
end
##}}}

##{{{  MB519 SYSCONF Configuration
define mb519_sysconf_configure
  ## _ST_display (_procname) "Configuring SYSCONF"

  ## Exit from reset
  set *$SYSCONF_SYS_CFG11 = *$SYSCONF_SYS_CFG11 | 0x00000001
  set *$SYSCONF_SYS_CFG15 = *$SYSCONF_SYS_CFG15 | 0x00000001

  ## Check both DLL on LMI0 are locked
  while ((*$SYSCONF_SYS_STA03 & ((1 << 10) | (1 << 20))) != ((1 << 10) | (1 << 20)))
  end

  ## Check both DLL on LMI1 are locked
  while ((*$SYSCONF_SYS_STA05 & ((1 << 10) | (1 << 20))) != ((1 << 10) | (1 << 20)))
  end

  ## Adjust proga, progb, zoutproga, and receiver mode for LMI0
  set *$SYSCONF_SYS_CFG12 = 0xa2007801 | (0x0 << 1) | (0x7 << 4) | (0x7 << 7) | (0x0 << 10)

  ## Adjust proga, progb, zoutproga, and receiver mode for LMI1
  set *$SYSCONF_SYS_CFG16 = 0xa2007801 | (0x0 << 1) | (0x7 << 4) | (0x7 << 7) | (0x0 << 10)

  ## Enable AutoPrecharge
  set *$SYSCONF_SYS_CFG38 = *$SYSCONF_SYS_CFG38 | 0x00030000
  set *$SYSCONF_SYS_CFG39 = *$SYSCONF_SYS_CFG39 | 0x00030000

  ## Force DLL1 and DLL2 commands of LMI0
  set *$SYSCONF_SYS_CFG13 = 0x00000002
  set *$SYSCONF_SYS_CFG14 = 0x00000002

  ## Force DLL1 and DLL2 commands of LMI1
  set *$SYSCONF_SYS_CFG17 = 0x00000002
  set *$SYSCONF_SYS_CFG18 = 0x00000002
end

define mb519se_sysconf_configure
  mb519_sysconf_configure

  # Move LMI0 and LMI1 base addresses to their space enhanced mode addresses
  set *$SYSCONF_SYS_CFG38 = (*$SYSCONF_SYS_CFG38 & 0xffffff00) | 0x00000040
  set *$SYSCONF_SYS_CFG39 = (*$SYSCONF_SYS_CFG39 & 0xffffff00) | 0x00000080
end
##}}}

##{{{  MB519 EMI Configuration
define mb519_emi_configure
  ## _ST_display (_procname) "Configuring EMI"

##------------------------------------------------------------------------------
## Re-program bank addresses
##------------------------------------------------------------------------------

  set *$EMI_BANK_ENABLE = 0x00000005

  ## NOTE: bits [0,5] define bottom address bits [22,27] of bank
  set *$EMI_BANK0_BASEADDRESS = 0x00000000
  set *$EMI_BANK1_BASEADDRESS = 0x00000008
  set *$EMI_BANK2_BASEADDRESS = 0x0000000c
  set *$EMI_BANK3_BASEADDRESS = 0x00000010
  set *$EMI_BANK4_BASEADDRESS = 0x00000014

##------------------------------------------------------------------------------
## Bank 0 - On-board 32MBytes Flash at address 0x00000000 -> 0x01ffffff
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 0: Configured for on-board Flash 32MBytes"

  set *$EMI_BANK0_EMICONFIGDATA0 = 0x001016d1
  set *$EMI_BANK0_EMICONFIGDATA1 = 0x9d200000
  set *$EMI_BANK0_EMICONFIGDATA2 = 0x9d220000
  set *$EMI_BANK0_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 1 - Unused (reset configuration not changed)
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 1: Undefined 16MBytes"

##------------------------------------------------------------------------------
## Bank 2 - DVB-CI at address 0x03000000 -> 0x03FFFFFF
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 2: Configured for DVB-CI 16MBytes"

  set *$EMI_BANK2_EMICONFIGDATA0 = 0x002046f9
  set *$EMI_BANK2_EMICONFIGDATA1 = 0xa5a00000
  set *$EMI_BANK2_EMICONFIGDATA2 = 0xa5a20000
  set *$EMI_BANK2_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Bank 3 - ATAPI at address 0x04000000 -> 0x04FFFFFF
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 3: Undefined 16MBytes"

##------------------------------------------------------------------------------
## Bank 4 - EPLD Registers at address 0x05000000 -> 0x05FFFFFF
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "Bank 4: Configured for EPLD Registers 16MBytes"

  set *$EMI_BANK4_EMICONFIGDATA0 = 0x042086f1
  set *$EMI_BANK4_EMICONFIGDATA1 = 0x8a002200
  set *$EMI_BANK4_EMICONFIGDATA2 = 0x8a004200
  set *$EMI_BANK4_EMICONFIGDATA3 = 0x00000000

##------------------------------------------------------------------------------
## Program other EMI registers
##------------------------------------------------------------------------------

  set *$EMI_GENCFG = 0x00000050
end
##}}}

##{{{  MB519 LMI Configuration
define mb519_lmi0_configure
  ## _ST_display (_procname) "Configuring LMI0 for DDR2 SDRAM Elpida EDE5116AHBG-8E-E"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  ## "Refresh Interval" is set equal to 0xA28=2600 => 2600*3ns=7.8us
  set *$LMI0_MIM_0 = 0x0a28015b
  set *$LMI0_MIM_1 = 0x000000b0

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI0_STR_0 = 0xcb2cab5b
  set *$LMI0_STR_1 = 0x000002d6

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  ## lmi base address + 128Mbytes 13x10
  set *$LMI0_SDRA0_0 = (((*$SYSCONF_SYS_CFG38 & 0xff) + 0x08) << 24) | 0x00000a00

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  ## lmi base address + 128Mbytes 13x10
  set *$LMI0_SDRA1_0 = (((*$SYSCONF_SYS_CFG38 & 0xff) + 0x08) << 24) | 0x00000a00

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200

  ## Enable clock with NOP command
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020023
  ## Wait NOP command for 400 nsec
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021

  ## Precharge all
  set *$LMI0_SCR_0 = 0x00020022
  ## Wait NOP command for 400 nsec
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021

  ## Issue EMRS2
  set *$LMI0_SDMR0 = 0x00000800

  ## Issue EMRS3
  set *$LMI0_SDMR0 = 0x00000c00

  ## Issue EMRS1 to enable DLL
  set *$LMI0_SDMR0 = 0x00000400

  ## Issue MRS with DLL reset, CAS 5, Write recovery 6, Sequential, Burst lengh 8
  set *$LMI0_SDMR0 = 0x00002353

  ## Wait NOP command for 400 nsec
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021

  ## Precharge all
  set *$LMI0_SCR_0 = 0x00020022
  ## 2 CBR (auto refresh)
  set *$LMI0_SCR_0 = 0x00020024
  set *$LMI0_SCR_0 = 0x00020024

  ## Issue MRS with CAS 5, Write recovery 6, Sequential, Burst lengh 8
  set *$LMI0_SDMR0 = 0x00002253

  ## Issue EMRS1 for OCD calibration default
  set *$LMI0_SDMR0 = 0x000007c4

  ## Issue EMRS1 for OCD calibration exit
  set *$LMI0_SDMR0 = 0x00000404

  ## Enable auto refresh
  set *$LMI0_MIM_0 = 0x0a28035b
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021
  set *$LMI0_SCR_0 = 0x00020021

  set *$LMI0_GCC_0 = 0x00382003
end

define mb519_lmi1_configure
  ## _ST_display (_procname) "Configuring LMI1 for DDR2 SDRAM Elpida EDE5116AHBG-8E-E"

##------------------------------------------------------------------------------
## Program LMI registers
##------------------------------------------------------------------------------

  ## _ST_display (_procname) "SDRAM Mode Register"
  ## "Refresh Interval" is set equal to 0xA28=0d2600 => 2600*3ns=7.8us
  set *$LMI1_MIM_0 = 0x0a28015b
  set *$LMI1_MIM_1 = 0x000000b0

  ## _ST_display (_procname) "SDRAM Timing Register"
  set *$LMI1_STR_0 = 0xcb2cab5b
  set *$LMI1_STR_1 = 0x000002d6

  ## _ST_display (_procname) "SDRAM Row Attribute 0"
  if ((*$SYSCONF_SYS_CFG39 & 0xff) == 0x80)
    ## lmi base address + 128Mbytes 13x10
    set *$LMI1_SDRA0_0 = (((*$SYSCONF_SYS_CFG39 & 0xff) + 0x08) << 24) | 0x00000a00
  else
    ## lmi base address + 64Mbytes 13x10 (not all 128MBytes are visible in 29 bit mode)
    set *$LMI1_SDRA0_0 = (((*$SYSCONF_SYS_CFG39 & 0xff) + 0x04) << 24) | 0x00000a00
  end

  ## _ST_display (_procname) "SDRAM Row Attribute 1"
  if ((*$SYSCONF_SYS_CFG39 & 0xff) == 0x80)
    ## lmi base address + 128Mbytes 13x10
    set *$LMI1_SDRA1_0 = (((*$SYSCONF_SYS_CFG39 & 0xff) + 0x08) << 24) | 0x00000a00
  else
    ## lmi base address + 64Mbytes 13x10 (not all 128MBytes are visible in 29 bit mode)
    set *$LMI1_SDRA1_0 = (((*$SYSCONF_SYS_CFG39 & 0xff) + 0x04) << 24) | 0x00000a00
  end

  ## _ST_display (_procname) "SDRAM Control Register"
  sleep 0 200

  ## Enable clock with NOP command
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020023
  ## Wait NOP command for 400 nsec
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021

  ## Precharge all
  set *$LMI1_SCR_0 = 0x00020022
  ## Wait NOP command for 400 nsec
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021

  ## Issue EMRS2
  set *$LMI1_SDMR0 = 0x00000800

  ## Issue EMRS3
  set *$LMI1_SDMR0 = 0x00000c00

  ## Issue EMRS1 to enable DLL
  set *$LMI1_SDMR0 = 0x00000400

  ## Issue MRS with DLL reset, CAS 5, Write recovery 6, Sequential, Burst lengh 8
  set *$LMI1_SDMR0 = 0x00002353

  ## Wait NOP command for 400 nsec
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021

  ## Precharge all
  set *$LMI1_SCR_0 = 0x00020022
  ## 2 CBR (auto refresh)
  set *$LMI1_SCR_0 = 0x00020024
  set *$LMI1_SCR_0 = 0x00020024

  ## Issue MRS with CAS 5, Write recovery 6, Sequential, Burst lengh 8
  set *$LMI1_SDMR0 = 0x00002253

  ## Issue EMRS1 for OCD calibration default
  set *$LMI1_SDMR0 = 0x000007c4

  ## Issue EMRS1 for OCD calibration exit
  set *$LMI1_SDMR0 = 0x00000404

  ## Enable auto refresh
  set *$LMI1_MIM_0 = 0x0a28035b
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021
  set *$LMI1_SCR_0 = 0x00020021

  set *$LMI1_GCC_0 = 0x00382003
end
##}}}

##{{{  MB519 PMB Configuration
define mb519se_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0xa0 0x80 128

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb519seuc_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128 0 0 1
  sh4_set_pmb 1 0xa0 0x80 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end

define mb519se29_pmb_configure
  # Configure the PMBs
  sh4_clear_all_pmbs
  sh4_set_pmb 0 0x80 0x40 128
  sh4_set_pmb 1 0x88 0x80 128
  sh4_set_pmb 2 0xa0 0x40 128 0 0 1
  sh4_set_pmb 3 0xa8 0x80 128 0 0 1

  # Switch to 32-bit SE mode
  sh4_enhanced_mode 1
end
##}}}

##{{{  MB519 Memory
define mb519_memory_define
  memory-add Flash      0x00000000 32 ROM
  memory-add LMI0_SDRAM 0x08000000 128 RAM
  memory-add LMI1_SDRAM 0x18000000 64 RAM
end

define mb519_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x08000000 128 RAM
  sim_addmemory 0x18000000 64 RAM
  sim_addmemory 0xfc000000 64 DEV
end

define mb519se_memory_define
  memory-add Flash      0x00000000 32 ROM
  memory-add LMI0_SDRAM 0x40000000 128 RAM
  memory-add LMI1_SDRAM 0x80000000 128 RAM
end

define mb519se_sim_memory_define
  sim_addmemory 0x00000000 32 ROM
  sim_addmemory 0x40000000 128 RAM
  sim_addmemory 0x80000000 128 RAM
  sim_addmemory 0xfc000000 64 DEV
end
##}}}

##{{{  MB519 Bypass Configuration
define mb519bypass_setup
  if ($argc > 0)
    sti7200_bypass_setup $arg0
  else
    sti7200_bypass_setup
  end
end

define mb519bypass_setup_attach
  if ($argc > 0)
    sti7200_bypass_setup_attach $arg0
  else
    sti7200_bypass_setup_attach
  end
end
##}}}

##{{{  MB519 STMMX Configuration
define mb519stmmx_setup
  if ($argc > 0)
    sti7200_stmmx_setup $arg0
  else
    sti7200_stmmx_setup
  end
end

define mb519stmmx_setup_attach
  if ($argc > 0)
    sti7200_stmmx_setup_attach $arg0
  else
    sti7200_stmmx_setup_attach
  end
end
##}}}

define mb519_setup
  sti7200_define
  mb519_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  mb519_version $_chipver $_pcbver

  mb519_clockgen_configure $_chipver $_pcbver
  mb519_sysconf_configure
  mb519_emi_configure
  mb519_lmi0_configure
  mb519_lmi1_configure

  set *$CCN_CCR = 0x8000090d
end

document mb519_setup
Configure an STi7200-Mboard board
Usage: mb519_setup
end

define mb519se_setup
  sti7200_define
  mb519se_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  mb519_version $_chipver $_pcbver

  mb519_clockgen_configure $_chipver $_pcbver
  mb519se_sysconf_configure
  mb519_emi_configure
  mb519_lmi0_configure
  mb519_lmi1_configure

  mb519se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb519se_setup
Configure an STi7200-Mboard board with the STi7200 in 32-bit SE mode
Usage: mb519se_setup
end

define mb519seuc_setup
  sti7200_define
  mb519se_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  mb519_version $_chipver $_pcbver

  mb519_clockgen_configure $_chipver $_pcbver
  mb519se_sysconf_configure
  mb519_emi_configure
  mb519_lmi0_configure
  mb519_lmi1_configure

  mb519seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb519seuc_setup
Configure an STi7200-Mboard board with the STi7200 in 32-bit SE mode with uncached RAM mappings
Usage: mb519seuc_setup
end

define mb519se29_setup
  sti7200_define
  mb519se_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  mb519_version $_chipver $_pcbver

  mb519_clockgen_configure $_chipver $_pcbver
  mb519se_sysconf_configure
  mb519_emi_configure
  mb519_lmi0_configure
  mb519_lmi1_configure

  mb519se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb519se29_setup
Configure an STi7200-Mboard board with the STi7200 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb519se29_setup
end

define mb519sim_setup
  sti7200_define
  mb519_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  set *$CCN_CCR = 0x8000090d
end

document mb519sim_setup
Configure a simulated STi7200-Mboard board
Usage: mb519sim_setup
end

define mb519simse_setup
  sti7200_define
  mb519se_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  mb519se_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb519simse_setup
Configure a simulated STi7200-Mboard board with the STi7200 in 32-bit SE mode
Usage: mb519simse_setup
end

define mb519simseuc_setup
  sti7200_define
  mb519se_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  mb519seuc_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb519simseuc_setup
Configure a simulated STi7200-Mboard board with the STi7200 in 32-bit SE mode with uncached RAM mappings
Usage: mb519simseuc_setup
end

define mb519simse29_setup
  sti7200_define
  mb519se_memory_define

  if ($_mb519sti7200cut == 1)
    sti7200_si_regs st40200
  else
    sti7200_si_regs st40300
  end

  mb519se29_pmb_configure

  set *$CCN_CCR = 0x8000090d
end

document mb519simse29_setup
Configure a simulated STi7200-Mboard board with the STi7200 in 32-bit SE mode with 29-bit compatibility RAM mappings in P1 and P2
Usage: mb519simse29_setup
end

define mb519_fsim_setup
  if ($_mb519sti7200cut == 1)
    sti7200_fsim_core_setup st40200
  else
    sti7200_fsim_core_setup st40300
  end
  mb519_sim_memory_define
end

document mb519_fsim_setup
Configure functional simulator for STi7200-Mboard board
Usage: mb519_fsim_setup
end

define mb519se_fsim_setup
  if ($_mb519sti7200cut == 1)
    sti7200_fsim_core_setup st40200
  else
    sti7200_fsim_core_setup st40300
  end
  mb519se_sim_memory_define
end

document mb519se_fsim_setup
Configure functional simulator for STi7200-Mboard board with the STi7200 in 32-bit SE mode
Usage: mb519se_fsim_setup
end

define mb519_psim_setup
  if ($_mb519sti7200cut == 1)
    sti7200_psim_core_setup st40200
  else
    sti7200_psim_core_setup st40300
  end
  mb519_sim_memory_define
end

document mb519_psim_setup
Configure performance simulator for STi7200-Mboard board
Usage: mb519_psim_setup
end

define mb519se_psim_setup
  if ($_mb519sti7200cut == 1)
    sti7200_psim_core_setup st40200
  else
    sti7200_psim_core_setup st40300
  end
  mb519se_sim_memory_define
end

document mb519se_psim_setup
Configure performance simulator for STi7200-Mboard board with the STi7200 in 32-bit SE mode
Usage: mb519se_psim_setup
end

define mb519_display_registers
  if ($_mb519sti7200cut == 1)
    sti7200_display_si_regs st40200
  else
    sti7200_display_si_regs st40300
  end
end

document mb519_display_registers
Display the STi7200 configuration registers
Usage: mb519_display_registers
end
