################################################################################

set $UTLB_ADDR_ARRAY = (unsigned int *) 0xf6000000
set $UTLB_DATA_ARRAY = (unsigned int *) 0xf7000000

################################################################################

define sh4_set_utlb
  set $_utlbdata = 0x00000000

  # UTLB[n].SZ
  if ($arg3 == 1)
    set $_utlbdata |= 0x00000000
  else
  if ($arg3 == 4)
    set $_utlbdata |= 0x00000010
  else
  if ($arg3 == 64)
    set $_utlbdata |= 0x00000080
  else
  if ($arg3 == 1024)
    set $_utlbdata |= 0x00000090
  else
    ## Unsupported page size
  end
  end
  end
  end

  # UTLB[n].C (default is 1)
  if ($argc > 4)
    set $_utlbdata |= (($arg4 & 0x1) << 3)
  else
    set $_utlbdata |= 0x00000008
  end

  # UTLB[n].WT (default is 0)
  if ($argc > 5)
    set $_utlbdata |= (($arg5 & 0x1) << 0)
  end

  # UTLB[n].UB (default is 0)
  if ($argc > 6)
    set $_utlbdata |= (($arg6 & 0x1) << 9)
  end

  set $UTLB_ADDR_ARRAY[$arg0 * 64] = (($arg1 & 0xfffffc) << 10)
  set $UTLB_DATA_ARRAY[$arg0 * 64] = (($arg2 & 0xfffffc) << 10) | (1 << 8) | $_utlbdata
end

document sh4_set_utlb
Enable a UTLB entry
Usage: sh4_set_utlb <index> <virtual> <physical> <size> [<cache>=1 [<wt>=0 [<ub>=0]]]]
where <index> is the UTLB entry
+     <virtual> is the virtual page number
+     <physical> is the physical page number
+     <size> is the page size in KBytes
+     <cache> is optional and is the page cacheability (default: 1 [on])
+     <wt> is optional and is the page cache mode (default: 0 [copy-back])
+     <ub> is optional and is the page buffer mode (default: 0 [buffered])
end

################################################################################

define sh4_clear_utlb
  set $UTLB_DATA_ARRAY[$arg0 * 64] = 0x00000000
  set $UTLB_ADDR_ARRAY[$arg0 * 64] = 0x00000000
end

document sh4_clear_utlb
Disable a UTLB entry
Usage: sh4_clear_utlb <index>
where <index> is the UTLB entry
end

define sh4_clear_all_utlbs
  set $_utlb = 0
  while ($_utlb < 64)
    sh4_clear_utlb $_utlb
    set $_utlb++
  end
end

document sh4_clear_all_utlbs
Disable all UTLB entries
Usage: sh4_clear_all_utlbs
end

################################################################################

define sh4_display_utlb
  set $_utlbaddrp = &$UTLB_ADDR_ARRAY[$arg0 * 64]
  set $_utlbdatap = &$UTLB_DATA_ARRAY[$arg0 * 64]

  printf "UTLB[%02.2d] (0x%08x,0x%08x) = 0x%08x:0x%08x (Valid: %d)\n", $arg0, $_utlbaddrp, $_utlbdatap, *$_utlbaddrp, *$_utlbdatap, (*$_utlbaddrp >> 8) & 0x1
end

document sh4_display_utlb
Display the configuration of a UTLB entry
Usage: sh4_display_utlb <index>
where <index> is the UTLB entry
end

define sh4_display_all_utlbs
  set $_utlb = 0
  while ($_utlb < 64)
    sh4_display_utlb $_utlb
    set $_utlb++
  end
end

document sh4_display_all_utlbs
Display the configurations of all UTLB entries
Usage: sh4_display_all_utlbs
end

################################################################################

define sh4_display_utlb_mapping
  set $_utlbaddr = $UTLB_ADDR_ARRAY[$arg0 * 64]
  set $_utlbdata = $UTLB_DATA_ARRAY[$arg0 * 64]

  set $_utlbvalid = ($_utlbaddr >> 8) & 0x1
  set $_utlbdirty = ($_utlbaddr >> 9) & 0x1

  if ($_utlbvalid)
    set $_utlbwt = ($_utlbdata >> 0) & 0x1
    set $_utlbsh = ($_utlbdata >> 1) & 0x1
    set $_utlbcached = ($_utlbdata >> 3) & 0x1
    set $_utlbpr = ($_utlbdata >> 5) & 0x3
    set $_utlbub = ($_utlbdata >> 9) & 0x1
    set $_utlbsize = (($_utlbdata >> 6) & 0x2) | (($_utlbdata >> 4) & 0x1)

    printf "UTLB[%02d]: ", $arg0
    if ($_utlbsize == 0)
      set $_utlbvpn = $_utlbaddr & 0xfffffc00
      set $_utlbppn = $_utlbdata & 0xfffffc00
      printf " 1K VIRT 0x%08x -> PHYS 0x%08x\n", $_utlbvpn, $_utlbppn
    else
    if ($_utlbsize == 1)
      set $_utlbvpn = $_utlbaddr & 0xfffff000
      set $_utlbppn = $_utlbdata & 0xfffff000
      printf " 4K VIRT 0x%08x -> PHYS 0x%08x\n", $_utlbvpn, $_utlbppn
    else
    if ($_utlbsize == 2)
      set $_utlbvpn = $_utlbaddr & 0xffff0000
      set $_utlbppn = $_utlbdata & 0xffff0000
      printf "64K VIRT 0x%08x -> PHYS 0x%08x\n", $_utlbvpn, $_utlbppn
    else
      set $_utlbvpn = $_utlbaddr & 0xfff00000
      set $_utlbppn = $_utlbdata & 0xfff00000
      printf " 1M VIRT 0x%08x -> PHYS 0x%08x\n", $_utlbvpn, $_utlbppn
    end
    end
    end

    printf "("
    if ($_utlbcached)
      printf "CACHED, "
      if ($_utlbwt)
        printf "WRITE-THROUGH, "
      else
        printf "COPY-BACK, "
      end
    else
      printf "UNCACHED, "
      if ($_utlbub)
        printf "UNBUFFERED, "
      else
        printf "BUFFERED, "
      end
    end
    if ($_utlbdirty)
      printf "DIRTY, "
    else
      printf "CLEAN, "
    end
    if ($_utlbsh)
      printf "SHARED, "
    else
      printf "UNSHARED, "
    end
    if ($_utlbpr == 0)
      printf "PRIV: RO, USER: --"
    else
    if ($_utlbpr == 1)
      printf "PRIV: RW, USER: --"
    else
    if ($_utlbpr == 2)
      printf "PRIV: RO, USER: RO"
    else
      printf "PRIV: RW, USER: RW"
    end
    end
    end
    printf ")\n"
  end
end

document sh4_display_utlb_mapping
Display the mapping of a UTLB entry if valid
Usage: sh4_display_utlb_mapping <index>
where <index> is the UTLB entry
end

define sh4_display_all_utlb_mappings
  set $_utlb = 0
  while ($_utlb < 64)
    sh4_display_utlb_mapping $_utlb
    set $_utlb++
  end
end

document sh4_display_all_utlb_mappings
Display all the mappings of valid UTLB entries
Usage: sh4_display_all_utlb_mappings
end

################################################################################

define sh4_virtual_mode
  if ($argc)
    set *$CCN_MMUCR = (*$CCN_MMUCR & ~(1 << 0)) | (($arg0 & 0x1) << 0)
  else
    printf "Virtual translation mode = %d\n", (*$CCN_MMUCR >> 0) & 0x1
  end
end

document sh4_virtual_mode
Display or set virtual translation mode
Usage: sh4_virtual_mode [<mode>]
where <mode> is optional and enables or disables virtual translation mode
end

################################################################################
