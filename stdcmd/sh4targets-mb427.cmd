################################################################################

define mb427be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb427_setup $arg1
  else
    connectst40300be $arg0 mb427_setup "jtagpinout=st40 hardreset"
  end
end

document mb427be
Connect to and configure an ST40-300 FPGA board (big endian)
Usage: mb427be <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb427_setup $arg1
  else
    connectst40300le $arg0 mb427_setup "jtagpinout=st40 hardreset"
  end
end

document mb427le
Connect to and configure an ST40-300 FPGA board (little endian)
Usage: mb427le <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427
  if ($argc > 1)
    mb427le $arg0 $arg1
  else
    mb427le $arg0
  end
end

document mb427
Connect to and configure an ST40-300 FPGA board
Usage: mb427 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb427sebe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb427se_setup $arg1
  else
    connectst40300be $arg0 mb427se_setup "jtagpinout=st40 hardreset"
  end
end

document mb427sebe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb427sebe <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427sele
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb427se_setup $arg1
  else
    connectst40300le $arg0 mb427se_setup "jtagpinout=st40 hardreset"
  end
end

document mb427sele
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb427sele <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427se
  if ($argc > 1)
    mb427sele $arg0 $arg1
  else
    mb427sele $arg0
  end
end

document mb427se
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb427se <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb427seucbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb427seuc_setup $arg1
  else
    connectst40300be $arg0 mb427seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb427seucbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427seucbe <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427seucle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb427seuc_setup $arg1
  else
    connectst40300le $arg0 mb427seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb427seucle
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427seucle <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427seuc
  if ($argc > 1)
    mb427seucle $arg0 $arg1
  else
    mb427seucle $arg0
  end
end

document mb427seuc
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427seuc <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb427se29be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300be $arg0 mb427se29_setup $arg1
  else
    connectst40300be $arg0 mb427se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb427se29be
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427se29be <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427se29le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300le $arg0 mb427se29_setup $arg1
  else
    connectst40300le $arg0 mb427se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb427se29le
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427se29le <target>
where <target> is an ST Micro Connect name or IP address
end

define mb427se29
  if ($argc > 1)
    mb427se29le $arg0 $arg1
  else
    mb427se29le $arg0
  end
end

document mb427se29
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427se29 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb427usbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb427_setup $arg1
  else
    connectst40300usbbe $arg0 mb427_setup "jtagpinout=st40 hardreset"
  end
end

document mb427usbbe
Connect to and configure an ST40-300 FPGA board (big endian)
Usage: mb427usbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb427usble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb427_setup $arg1
  else
    connectst40300usble $arg0 mb427_setup "jtagpinout=st40 hardreset"
  end
end

document mb427usble
Connect to and configure an ST40-300 FPGA board (little endian)
Usage: mb427usble <target>
where <target> is an ST Micro Connect USB name
end

define mb427usb
  if ($argc > 1)
    mb427usble $arg0 $arg1
  else
    mb427usble $arg0
  end
end

document mb427usb
Connect to and configure an ST40-300 FPGA board
Usage: mb427usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb427seusbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb427se_setup $arg1
  else
    connectst40300usbbe $arg0 mb427se_setup "jtagpinout=st40 hardreset"
  end
end

document mb427seusbbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb427seusbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb427seusble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb427se_setup $arg1
  else
    connectst40300usble $arg0 mb427se_setup "jtagpinout=st40 hardreset"
  end
end

document mb427seusble
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb427seusble <target>
where <target> is an ST Micro Connect USB name
end

define mb427seusb
  if ($argc > 1)
    mb427seusble $arg0 $arg1
  else
    mb427seusble $arg0
  end
end

document mb427seusb
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb427seusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb427seucusbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb427seuc_setup $arg1
  else
    connectst40300usbbe $arg0 mb427seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb427seucusbbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427seucusbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb427seucusble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb427seuc_setup $arg1
  else
    connectst40300usble $arg0 mb427seuc_setup "jtagpinout=st40 hardreset"
  end
end

document mb427seucusble
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427seucusble <target>
where <target> is an ST Micro Connect USB name
end

define mb427seucusb
  if ($argc > 1)
    mb427seucusble $arg0 $arg1
  else
    mb427seucusble $arg0
  end
end

document mb427seucusb
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427seucusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb427se29usbbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usbbe $arg0 mb427se29_setup $arg1
  else
    connectst40300usbbe $arg0 mb427se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb427se29usbbe
Connect to and configure an ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427se29usbbe <target>
where <target> is an ST Micro Connect USB name
end

define mb427se29usble
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectst40300usble $arg0 mb427se29_setup $arg1
  else
    connectst40300usble $arg0 mb427se29_setup "jtagpinout=st40 hardreset"
  end
end

document mb427se29usble
Connect to and configure an ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427se29usble <target>
where <target> is an ST Micro Connect USB name
end

define mb427se29usb
  if ($argc > 1)
    mb427se29usble $arg0 $arg1
  else
    mb427se29usble $arg0
  end
end

document mb427se29usb
Connect to and configure an ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427se29usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb427simbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb427_fsim_setup mb427sim_setup $arg0
  else
    connectst40300simbe mb427_fsim_setup mb427sim_setup ""
  end
end

document mb427simbe
Connect to and configure a simulated ST40-300 FPGA board (big endian)
Usage: mb427simbe
end

define mb427simle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb427_fsim_setup mb427sim_setup $arg0
  else
    connectst40300simle mb427_fsim_setup mb427sim_setup ""
  end
end

document mb427simle
Connect to and configure a simulated ST40-300 FPGA board (little endian)
Usage: mb427simle
end

define mb427sim
  if ($argc > 0)
    mb427simle $arg0
  else
    mb427simle
  end
end

document mb427sim
Connect to and configure a simulated ST40-300 FPGA board
Usage: mb427sim
end

################################################################################

define mb427simsebe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb427se_fsim_setup mb427simse_setup $arg0
  else
    connectst40300simbe mb427se_fsim_setup mb427simse_setup ""
  end
end

document mb427simsebe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb427simsebe
end

define mb427simsele
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb427se_fsim_setup mb427simse_setup $arg0
  else
    connectst40300simle mb427se_fsim_setup mb427simse_setup ""
  end
end

document mb427simsele
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb427simsele
end

define mb427simse
  if ($argc > 0)
    mb427simsele $arg0
  else
    mb427simsele
  end
end

document mb427simse
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb427simse
end

################################################################################

define mb427simseucbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb427se_fsim_setup mb427simseuc_setup $arg0
  else
    connectst40300simbe mb427se_fsim_setup mb427simseuc_setup ""
  end
end

document mb427simseucbe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427simseucbe
end

define mb427simseucle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb427se_fsim_setup mb427simseuc_setup $arg0
  else
    connectst40300simle mb427se_fsim_setup mb427simseuc_setup ""
  end
end

document mb427simseucle
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427simseucle
end

define mb427simseuc
  if ($argc > 0)
    mb427simseucle $arg0
  else
    mb427simseucle
  end
end

document mb427simseuc
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427simseuc
end

################################################################################

define mb427simse29be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simbe mb427se_fsim_setup mb427simse29_setup $arg0
  else
    connectst40300simbe mb427se_fsim_setup mb427simse29_setup ""
  end
end

document mb427simse29be
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427simse29be
end

define mb427simse29le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300simle mb427se_fsim_setup mb427simse29_setup $arg0
  else
    connectst40300simle mb427se_fsim_setup mb427simse29_setup ""
  end
end

document mb427simse29le
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427simse29le
end

define mb427simse29
  if ($argc > 0)
    mb427simse29le $arg0
  else
    mb427simse29le
  end
end

document mb427simse29
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427simse29
end

################################################################################

define mb427psimbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb427_psim_setup mb427sim_setup $arg0
  else
    connectst40300psimbe mb427_psim_setup mb427sim_setup ""
  end
end

document mb427psimbe
Connect to and configure a simulated ST40-300 FPGA board (big endian)
Usage: mb427psimbe
end

define mb427psimle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb427_psim_setup mb427sim_setup $arg0
  else
    connectst40300psimle mb427_psim_setup mb427sim_setup ""
  end
end

document mb427psimle
Connect to and configure a simulated ST40-300 FPGA board (little endian)
Usage: mb427psimle
end

define mb427psim
  if ($argc > 0)
    mb427psimle $arg0
  else
    mb427psimle
  end
end

document mb427psim
Connect to and configure a simulated ST40-300 FPGA board
Usage: mb427psim
end

################################################################################

define mb427psimsebe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb427se_psim_setup mb427simse_setup $arg0
  else
    connectst40300psimbe mb427se_psim_setup mb427simse_setup ""
  end
end

document mb427psimsebe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode)
Usage: mb427psimsebe
end

define mb427psimsele
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb427se_psim_setup mb427simse_setup $arg0
  else
    connectst40300psimle mb427se_psim_setup mb427simse_setup ""
  end
end

document mb427psimsele
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode)
Usage: mb427psimsele
end

define mb427psimse
  if ($argc > 0)
    mb427psimsele $arg0
  else
    mb427psimsele
  end
end

document mb427psimse
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode)
Usage: mb427psimse
end

################################################################################

define mb427psimseucbe
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb427se_psim_setup mb427simseuc_setup $arg0
  else
    connectst40300psimbe mb427se_psim_setup mb427simseuc_setup ""
  end
end

document mb427psimseucbe
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427psimseucbe
end

define mb427psimseucle
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb427se_psim_setup mb427simseuc_setup $arg0
  else
    connectst40300psimle mb427se_psim_setup mb427simseuc_setup ""
  end
end

document mb427psimseucle
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427psimseucle
end

define mb427psimseuc
  if ($argc > 0)
    mb427psimseucle $arg0
  else
    mb427psimseucle
  end
end

document mb427psimseuc
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with uncached mappings)
Usage: mb427psimseuc
end

################################################################################

define mb427psimse29be
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimbe mb427se_psim_setup mb427simse29_setup $arg0
  else
    connectst40300psimbe mb427se_psim_setup mb427simse29_setup ""
  end
end

document mb427psimse29be
Connect to and configure a simulated ST40-300 FPGA board (big endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427psimse29be
end

define mb427psimse29le
  source register40.cmd
  source display40.cmd
  source st40300.cmd
  source mb427.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectst40300psimle mb427se_psim_setup mb427simse29_setup $arg0
  else
    connectst40300psimle mb427se_psim_setup mb427simse29_setup ""
  end
end

document mb427psimse29le
Connect to and configure a simulated ST40-300 FPGA board (little endian) (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427psimse29le
end

define mb427psimse29
  if ($argc > 0)
    mb427psimse29le $arg0
  else
    mb427psimse29le
  end
end

document mb427psimse29
Connect to and configure a simulated ST40-300 FPGA board (ST40-300 in 32-bit SE mode with 29-bit compatibility mappings in P1 and P2)
Usage: mb427psimse29
end

################################################################################
