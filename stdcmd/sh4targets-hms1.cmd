################################################################################

define hms1
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source hms1.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 hms1_setup $arg1
  else
    connectsh4le $arg0 hms1_setup "jtagpinout=st40 hardreset"
  end
end

document hms1
Connect to and configure a Simple Devices HMS-1 board
Usage: hms1 <target>
where <target> is an ST Micro Connect name or IP address
end

define hms1bypass
  if ($argc > 1)
    hms1 $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hms1 $arg0 "jtagpinout=st40 jtagreset -inicommand hms1bypass_setup"
  end
end

document hms1bypass
Connect to and configure a Simple Devices HMS-1 board bypassing to the ST40
Usage: hms1bypass <target>
where <target> is an ST Micro Connect name or IP address
end

define hms1stmmx
  if ($argc > 1)
    hms1 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hms1 $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hms1stmmx_setup"
  end
end

document hms1stmmx
Connect to and configure a Simple Devices HMS-1 board via an ST MultiCore/Mux
Usage: hms1stmmx <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define hms1usb
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100jtag.cmd
  source stb7100.cmd
  source hms1.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 hms1_setup $arg1
  else
    connectsh4usble $arg0 hms1_setup "jtagpinout=st40 hardreset"
  end
end

document hms1usb
Connect to and configure a Simple Devices HMS-1 board
Usage: hms1usb <target>
where <target> is an ST Micro Connect USB name
end

define hms1bypassusb
  if ($argc > 1)
    hms1usb $arg0 "jtagpinout=st40 jtagreset -inicommand $arg1"
  else
    hms1usb $arg0 "jtagpinout=st40 jtagreset -inicommand hms1bypass_setup"
  end
end

document hms1bypassusb
Connect to and configure a Simple Devices HMS-1 board bypassing to the ST40
Usage: hms1bypassusb <target>
where <target> is an ST Micro Connect USB name
end

define hms1stmmxusb
  if ($argc > 1)
    hms1usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand $arg1"
  else
    hms1usb $arg0 "jtagpinout=stmmx jtagreset tdidelay=1 -inicommand hms1stmmx_setup"
  end
end

document hms1stmmxusb
Connect to and configure a Simple Devices HMS-1 board via an ST MultiCore/Mux
Usage: hms1stmmxusb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define hms1sim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source hms1.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle hms1_fsim_setup hms1sim_setup $arg0
  else
    connectsh4simle hms1_fsim_setup hms1sim_setup ""
  end
end

document hms1sim
Connect to and configure a simulated Simple Devices HMS-1 board
Usage: hms1sim
end

################################################################################

define hms1psim
  source register40.cmd
  source display40.cmd
  source stb7100clocks.cmd
  source stb7100.cmd
  source hms1.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle hms1_psim_setup hms1sim_setup $arg0
  else
    connectsh4psimle hms1_psim_setup hms1sim_setup ""
  end
end

document hms1psim
Connect to and configure a simulated Simple Devices HMS-1 board
Usage: hms1psim
end

################################################################################
