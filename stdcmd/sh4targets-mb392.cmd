################################################################################

define mb392
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb392.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4le $arg0 mb392_setup $arg1
  else
    connectsh4le $arg0 mb392_setup "hardreset"
  end
end

document mb392
Connect to and configure an ST220-Eval board
Usage: mb392 <target>
where <target> is an ST Micro Connect name or IP address
end

################################################################################

define mb392usb
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb392.cmd
  source sh4connect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectsh4usble $arg0 mb392_setup $arg1
  else
    connectsh4usble $arg0 mb392_setup "hardreset"
  end
end

document mb392usb
Connect to and configure an ST220-Eval board
Usage: mb392usb <target>
where <target> is an ST Micro Connect USB name
end

################################################################################

define mb392sim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb392.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4simle mb392_fsim_setup mb392sim_setup $arg0
  else
    connectsh4simle mb392_fsim_setup mb392sim_setup ""
  end
end

document mb392sim
Connect to and configure a simulated ST220-Eval board
Usage: mb392sim
end

################################################################################

define mb392psim
  source register40.cmd
  source display40.cmd
  source stm8000clocks.cmd
  source stm8000.cmd
  source mb392.cmd
  source sh4connect.cmd
  source plugins.cmd
  source shsimcmds.cmd

  if ($argc > 0)
    connectsh4psimle mb392_psim_setup mb392sim_setup $arg0
  else
    connectsh4psimle mb392_psim_setup mb392sim_setup ""
  end
end

document mb392psim
Connect to and configure a simulated ST220-Eval board
Usage: mb392psim
end

################################################################################
