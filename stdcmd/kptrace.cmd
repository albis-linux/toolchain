###########################################################

define kptrace_dump
        dont-repeat
        set linux-awareness force_tlb_reprogramming on
        echo Dumping last kptrace subbuffer to $arg0...
        set $KPTRACE_SUBBUF_SIZE = chan->subbuf_size - 4
        set $KPTRACE_SUBBUF = chan->buf->data + 4
        dump binary memory /tmp/kptrace_dump $KPTRACE_SUBBUF $KPTRACE_SUBBUF+$KPTRACE_SUBBUF_SIZE
        shell tr -s '\000' < /tmp/kptrace_dump > $arg0
        shell rm /tmp/kptrace_dump
        echo done\n
end

document kptrace_dump
Dumps the last kptrace subbuffer to a file.
Usage: kptrace_dump <filename>
end

###########################################################
