# STLinux 2.4 sh4 toolchain

This is the GCC 4.3 toolchain taken from STLinux 2.4 distribution.  
It should be usable on any modern Linux distibution. Tested on Ubuntu 18.04.

### Usage

- Clone the repository to any location
- `export CROSS_COMPILE=/path/to/toolchain/sh4-linux-`
